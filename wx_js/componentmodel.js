/**
 * ComponentModel adalah gabungan dari beberapa model 
 */


class ShipperComponentModel {
    constructor() {
        this.m_shipper = new MShipper();
    }
}

class ChargeComponentModel {
    constructor() {
        this.c_charge = new CCharge();
    }
}


class LocatorComponentModel {
    constructor() {
        this.m_locator = new MLocator();
    }
}

class OrganizationComponentModel {
    constructor() {
        this.ad_org = new AdOrg();
        this.ad_orginfo = new AdOrginfo();
        this.c_location = new CLocation();

    }
}


class InvoiceComponentModel {
    constructor() {
        this.c_invoice = new CInvoice();
        this.c_bpartner = new CBpartner();
        this.c_bpartner_location = new CBpartnerLocation();
        this.c_order = new COrder();
        this.c_doctype = new CDoctype();

    }
}

class InvoicelineComponentModel {
    constructor() {
        this.c_invoiceline = new CInvoiceline();
        this.c_invoice = new CInvoice();
        this.m_product = new MProduct();
        this.c_uom = new CUom();
        this.c_uom_based = new CUom();
        this.c_orderline = new COrderline();
        this.c_tax = new CTax();

    }

}

class PaymentComponentModel {
    constructor() {
        this.c_payment = new CPayment();
        this.c_bpartner = new CBpartner();
        this.c_order = new COrder();
        this.c_bankaccount = new CBankaccount();
        this.c_invoice = new CInvoice();
        this.c_doctype = new CDoctype();

    }
}

class BankstatementlineComponentModel {
    constructor() {
        this.c_bankstatement = new CBankstatement();
        this.c_bankstatementline = new CBankstatementline();
        // this.c_invoice = new CInvoice();
        this.c_payment = new CPayment();

    }
}


class BankstatementComponentModel {
    constructor() {
        this.c_bankstatement = new CBankstatement();
        this.c_bankaccount = new CBankaccount();

    }
}

class RmaComponentModel {
    constructor() {
        this.c_invoice = new CInvoice();
        this.m_rma = new MRma();
        this.c_bpartner = new CBpartner();
        this.c_doctype = new CDoctype();
        this.m_warehouse = new MWarehouse();
    }
}

class RmalineComponentModel {
    constructor() {
        this.c_invoiceline = new CInvoiceline();
        this.m_rma = new MRma();
        this.m_product = new MProduct();
        this.c_uom = new CUom();
        this.m_rmaline = new MRmaline();
        this.c_tax = new CTax();
    }
}


class ExpenseComponentModel {
    constructor() {
        this.c_payment = new CPayment();
        this.c_bankaccount = new CBankaccount();
        this.c_charge = new CCharge();
        this.c_doctype = new CDoctype();

    }
}

class MovementComponentModel {
    constructor() {
        this.m_warehouse_from = new MWarehouse();
        this.m_warehouse_to = new MWarehouse();
        this.m_movement = new MMovement();
        this.c_doctype = new CDoctype();
    }
}

class MovementlineComponentModel {
    constructor() {
        this.m_movementline = new MMovementline();
        this.m_movement = new MMovement();
        this.m_product = new MProduct();
        this.c_uom = new CUom();
    }
}


class StorageOnHandComponentModel {
    constructor() {
        this.m_storageonhand = new MStorageonhand();
        this.m_locator = new MLocator();
        this.m_warehouse = new MWarehouse();
        this.m_product = new MProduct();
        this.c_uom = new CUom();
    }
}


class MaterialTransactionComponentModel {
    constructor() {
        this.m_transaction = new MTransaction();
        this.m_locator = new MLocator();
        this.m_warehouse = new MWarehouse();
        this.m_product = new MProduct();
        this.c_uom = new CUom();
    }
}

class InventoryComponentModel {
    constructor() {
        this.m_inventory = new MInventory();
        this.m_warehouse = new MWarehouse();
        this.c_doctype = new CDoctype();
    }
}


class InventorylineComponentModel {
    constructor() {
        this.m_inventory = new MInventory();
        this.m_inventoryline = new MInventoryline();
        this.m_product = new MProduct();
        this.c_uom = new CUom();
    }
}

class ProductionComponentModel {
    constructor() {
        this.m_production = new MProduction();
        this.m_product = new MProduct();
        this.m_warehouse = new MWarehouse();
        this.c_uom = new CUom();
    }
}


class ProductionlineComponentModel {
    constructor() {
        this.m_production = new MProduction();
        this.m_productionline = new MProductionline();
        this.m_product = new MProduct();
        this.c_uom = new CUom();
    }
}


class SearchPosProductComponentModel {
    constructor() {
        this.m_product = new MProduct();
        this.m_productprice = new MProductprice();
    }
}

class PosConfigComponentModel {
    constructor() {
        this.c_pos = new CPos();
        this.m_warehouse = new MWarehouse();
        this.m_pricelist_version = new MPricelistVersion();
        this.c_bankaccount = new CBankaccount();
        this.c_tax = new CTax();
        this.m_shipper = new MShipper();

    }
}


class CartComponentModel {
    constructor() {
        this.pos_c_order = new PosCOrder();
        this.c_bpartner = new CBpartner();
        this.c_bpartner_location = new CBpartnerLocation();
    }

}

class CartlineComponentModel {
    constructor() {

        this.pos_c_order = new PosCOrder();
        this.m_product = new MProduct();
        this.c_uom = new CUom();
        this.pos_c_orderline = new PosCOrderline();
    }
}

class UomComponentModel {


    constructor() {

        this.c_uom = new CUom();
    }
}

class WarehouseViewModel {
    constructor() {
        this.m_warehouse = new MWarehouse();
        this.c_location = new CLocation();
    }
}

class BusinesspartnerViewModel {


    constructor() {
        this.c_bpartner = new CBpartner();
        this.c_bpartner_location = new CBpartnerLocation();
        this.c_location = new CLocation();
    }

}


class BankaccountViewModel {

    constructor() {
        this.c_bankaccount = new CBankaccount();
    }
}

class ProductCategoryViewModel {

    constructor() {
        this.m_product_category = new MProductCategory();
    }
}

class PricelistVersionViewModel {

    constructor() {
        this.m_pricelist = new MPricelist();
        this.m_pricelist_version = new MPricelistVersion();
    }
}

class ProductViewModel {

    constructor() {
        this.m_product = new MProduct();
        this.m_product_category = new MProductCategory();
        this.c_uom = new CUom();
        this.wypex_producttype = new WypexProducttype();
    }

}

class ProductBomViewModel {


    constructor() {
        // this.productParent = new MProduct(); // merupakan produk untuk produksi atau parent
        this.m_product = new MProduct(); // merupakan product BOM atau child
        this.c_uom = new CUom();
        this.m_product_bom = new MProductBom();
    }
}

class ProductpriceViewModel {


    constructor() {
        // this.productParent = new MProduct(); // merupakan produk untuk produksi atau parent
        this.m_product = new MProduct(); // merupakan product BOM atau child
        this.m_pricelist_version = new MPricelistVersion();
        this.m_productprice = new MProductprice();
        this.m_pricelist = new MPricelist();
    }
}

class UomConversionViewModel {


    constructor() {

        this.c_uomfrom = new CUom();
        this.c_uomto = new CUom();
        this.c_uom_conversion = new CUomConversion();
    }
}

class OrderViewModel {
    constructor() {

        this.c_order = new COrder();
        this.c_bpartner = new CBpartner();
        this.c_bpartner_location = new CBpartnerLocation();
        this.m_warehouse = new MWarehouse();
        this.c_bankaccount = new CBankaccount();
        this.c_doctype = new CDoctype();
        this.m_shipper = new MShipper();
    }
}

class OrderlineViewModel {
    constructor() {

        this.c_order = new COrder();
        this.m_product = new MProduct();
        this.c_uom = new CUom();
        this.c_tax = new CTax();
        this.c_orderline = new COrderline();
    }
}

class InoutViewModel {
    constructor() {
        this.m_inout = new MInout();
        this.c_bpartner = new CBpartner();
        this.c_bpartner_location = new CBpartnerLocation();
        this.c_order = new COrder();
        this.c_doctype = new CDoctype();
        this.m_shipper = new MShipper();
        this.m_warehouse = new MWarehouse();
    }
}
class InoutlineViewModel {
    constructor() {
        this.m_inout = new MInout();
        this.m_inoutline = new MInoutline();
        this.c_uom = new CUom();
        this.c_uom_based = new CUom();
        this.c_orderline = new COrderline();
        this.m_locator = new MLocator();
        this.m_product = new MProduct();
    }

}

class CostViewModel {
    constructor() {
        this.m_product = new MProduct();
        this.m_cost = new MCost();
    }
}

class UserComponentModel {
    constructor() {
        this.ad_user = new AdUser();
        this.ad_user_roles = new AdUserRoles();
        this.ad_role = new AdRole();

    }
}

class PermissionComponentModel {
    constructor() {

        this.ad_user_roles = new AdUserRoles();
        this.wxt_user_access = new WxtUserAccess();
        this.ad_role = new AdRole();

    }
}

class RoleComponentModel {
    constructor() {
        this.ad_role = new AdRole();
    }
}

class RolelineComponentModel {
    constructor() {
        this.ad_role = new AdRole();
        this.wxt_roleline = new WxtRoleline();
    }
}


class TaxComponentModel {
    constructor() {
        this.c_tax = new CTax();
    }
}

class PrintProductbarcodeComponentModel {

    constructor() {

        this.wxt_paper_type = new WxtPaperType();
        this.wxt_paper_size = new WxtPaperSize();
        this.wxt_print_productbarcode = new WxtPrintProductbarcode();


    }

}
class PrintProductbarcodelineComponentModel {

    constructor() {

        this.m_product = new MProduct();
        this.wxt_print_productbarcode = new WxtPrintProductbarcode();
        this.wxt_print_productbarcodeline = new WxtPrintProductbarcodeline();


    }

}

class PaperSizeComponentModel{
    constructor(){

        this.wxt_paper_size = new WxtPaperSize();
        this.wxt_paper_type =  new WxtPaperType();
    }
}


class PosTransactionComponentModel{
    constructor(){

        this.pos_transaction = new PosTransaction();
        
        
    }
}


class PosTransactionlineComponentModel{
    constructor(){

        this.pos_transaction = new PosTransaction();
        this.pos_transactionline =  new PosTransactionline();
        this.c_bankaccount = new CBankaccount();
    }
}

class ElementvalueComponentModel{

    constructor(){

        this.c_elementvalue = new CElementvalue();
    }

}

class PeriodComponentModel{
    constructor(){

        this.c_period = new CPeriod();
    }
}
