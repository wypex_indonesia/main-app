class SideMenu {
    /**
     * 
     * @param {*} selectorFragmentSideMenu  selector yang langsung dimasukkan dalam jquery $(selectorFragmentSideMenu)
     */
    constructor(selectorFragmentSideMenu) {

        this.selectorFragmentSideMenu = selectorFragmentSideMenu;


    }
    Init() {


        try {
            let thisObject = this;
            let parentMenu = '<ul class="kt-menu__nav ">';

            this.FindAll().then(function (dataMenuCMTList) {

                try {
                    let dataMenu = thisObject.FilterMenuSection(dataMenuCMTList);

                    _.forEach(dataMenu, function (menu) {

                        if (menu.wypex_ad_menu_wx_parent_uu === '') {
                            parentMenu += thisObject.CreateMenuSection(menu.wypex_ad_menu_wx_name);
                        } else {
                            parentMenu += thisObject.CreateMenuItem(menu.wypex_ad_menu_wx_name, menu.wypex_ad_menu_wx_urlmenu);
                        }


                    });

                    parentMenu += '</ul>';
                    $(thisObject.selectorFragmentSideMenu).html(parentMenu);

                    thisObject.AttachEventHandler();
                } catch (error) {
                    apiInterface.Log(this.constructor.name, this.CreateSideMenu.name, err);
                }

            });



        } catch (err) {
            apiInterface.Log(this.constructor.name, this.CreateSideMenu.name, err);
        }






    }

    Clear() {
        $(this.selectorFragmentSideMenu).html('');


    }

    CreateMenuSection(name) {
        return `	<li class="kt-menu__section ">
        <h4 class="kt-menu__section-text">`+ name + `</h4>
        <i class="kt-menu__section-icon flaticon-more-v2"></i>
        </li>`;

    }

    CreateMenuItem(name, url) {

        return `	<li class="kt-menu__item " aria-haspopup="true"><a name="` + url + `"  class="wx-sidemenu kt-menu__link "><span class="kt-menu__link-icon"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
            <rect x="0" y="0" width="24" height="24" />
            <path d="M5,3 L6,3 C6.55228475,3 7,3.44771525 7,4 L7,20 C7,20.5522847 6.55228475,21 6,21 L5,21 C4.44771525,21 4,20.5522847 4,20 L4,4 C4,3.44771525 4.44771525,3 5,3 Z M10,3 L11,3 C11.5522847,3 12,3.44771525 12,4 L12,20 C12,20.5522847 11.5522847,21 11,21 L10,21 C9.44771525,21 9,20.5522847 9,20 L9,4 C9,3.44771525 9.44771525,3 10,3 Z" fill="#000000" />
            <rect fill="#000000" opacity="0.3" transform="translate(17.825568, 11.945519) rotate(-19.000000) translate(-17.825568, -11.945519) " x="16.3255682" y="2.94551858" width="3" height="18" rx="1" />
        </g>
    </svg></span><span class="kt-menu__link-text">`+ name + `</span></a></li>`;
    }



    FindAll() {


        let rolelineService = new RolelineService();

        return rolelineService.GetNotInClauseSql('SideMenu').then(function (notInClause) {
            //  let notInClause = rolelineService.GetNotInClauseSql('SideMenu');

            let filterRole = '';
            if (notInClause) {
                filterRole = ' WHERE wypex_ad_menu.wypex_ad_menu_uu NOT IN (' + notInClause + ')';
            }

            let sqlTemplate = ' FROM wypex_ad_menu ' + filterRole + ' ORDER BY wypex_ad_menu.sequence_order ASC ';

            const listMetaColumn = HelperService.ConvertViewModelToMetaColumn({ wypex_ad_menu: new WypexAdMenu() });
            let varMetaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);

            return apiInterface.Query(varMetaDataQuery);

        });

        /* return new Promise(function(resolve,reject){
 
             rolelineService.GetNotInClauseSql('SideMenu').then(function (notInClause) {
                 //  let notInClause = rolelineService.GetNotInClauseSql('SideMenu');
     
                 let filterRole = '';
                 if (notInClause) {
                     filterRole = ' WHERE wypex_ad_menu.wypex_ad_menu_uu NOT IN (' + notInClause + ')';
                 }
     
                 let sqlTemplate = ' FROM wypex_ad_menu ' + filterRole + ' ORDER BY wypex_ad_menu.sequence_order ASC ';
     
                 const listMetaColumn = HelperService.ConvertViewModelToMetaColumn({ wypex_ad_menu: new WypexAdMenu() });
                 let varMetaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, dumpGlobal.SideMenu);
     
                 resolve(apiInterface.Query(varMetaDataQuery));
     
             });
 
         });
 */


        /* let rolelineCMTList = rolelineService.FindRolelineCMTListByAdRoleUuAndClassName(localStorage.getItem('ad_role_uu'), 'SideMenu');
         let rolelineCMT = null;
         if (rolelineCMTList.length) {
             rolelineCMT = rolelineCMTList[0];
         }
 
         let filterRole = '';
         if (rolelineCMT) {
             let roleException = JSON.parse(rolelineCMT.wxt_roleline_wx_data);
             let notInArray = [];
             _.forEach(roleException, function (wypex_ad_menu_uu) {
                 notInArray.push('\'' + wypex_ad_menu_uu + '\'');
 
             });
 
             if (notInArray.length){
                 let notInString = notInArray.join(',');
                 filterRole = ' WHERE wypex_ad_menu.wypex_ad_menu_uu NOT IN (' + notInString + ')';
             }
            
         }*/




    }

    AttachEventHandler() {

        try {
            let thisObject = this;
            $('.wx-sidemenu').off('click');
            $('.wx-sidemenu').on('click', function () {

                switch ($(this).attr('name')) {
                    case 'warehouse':
                        let warehousePage = new WarehousePage();
                        warehousePage.Init();
                        thisObject.RemoveSubHeader();
                        break;
                    case 'shipper':
                        let shipperPage = new ShipperPage();
                        shipperPage.Init();
                        thisObject.RemoveSubHeader();
                        break;
                    case 'uom':
                        let uomPage = new UomPage();
                        uomPage.Init();
                        thisObject.RemoveSubHeader();
                        break;
                    case 'businesspartner':
                        let businesspartnerPage = new BusinesspartnerPage();
                        businesspartnerPage.Init();
                        thisObject.RemoveSubHeader();
                        break;
                    case 'bankaccount':
                        let bankaccountPage = new BankaccountPage('wx_fragment_content', 'C');
                        bankaccountPage.Init();
                        thisObject.RemoveSubHeader();
                        break;
                    case 'pettycash':
                        let pettyCashPage = new BankaccountPage('wx_fragment_content', 'B');
                        pettyCashPage.Init();
                        thisObject.RemoveSubHeader();
                        break;

                    case 'productcategory':
                        let productcategoryPage = new ProductCategoryPage();
                        productcategoryPage.Init();
                        thisObject.RemoveSubHeader();
                        break;
                    case 'pricelistversion':
                        let pricelistversionPage = new PricelistversionPage();
                        pricelistversionPage.Init();
                        thisObject.RemoveSubHeader();
                        break;

                    case 'product':
                        let productPage = new ProductPage();
                        productPage.Init();
                        thisObject.RemoveSubHeader();
                        break;
                    case 'material_transaction':
                        let materialTransactionPage = new MaterialTransactionsPage('wx_fragment_content');
                        materialTransactionPage.Init();
                        thisObject.RemoveSubHeader();
                        break;
                    case 'stock_report':
                        let storageOnHandPage = new StorageOnHandPage('wx_fragment_content');
                        storageOnHandPage.Init();
                        thisObject.RemoveSubHeader();
                        break;
                    case 'detail_journal':
                        let factacctTransactionPage = new FactAcctTransactionPage('wx_fragment_content');
                        factacctTransactionPage.Init();

                        break;
                    case 'salesorder':
                        let saleorderPage = new OrderPage('SO', 'wx_fragment_content');
                        saleorderPage.Init();
                        thisObject.RemoveSubHeader();
                        break;
                    case 'purchaseorder':
                        let purchaseorderPage = new OrderPage('PO', 'wx_fragment_content');
                        purchaseorderPage.Init();
                        thisObject.RemoveSubHeader();
                        break;
                    case 'material_receipt':
                        let materialReceiptPage = new InoutPage('MMR', 'wx_fragment_content');
                        thisObject.RemoveSubHeader();
                        break;
                    case 'shipping':
                        let shippingPage = new InoutPage('MMS', 'wx_fragment_content');
                        thisObject.RemoveSubHeader();
                        break;
                    case 'invoice_vendor':
                        let invoiceAPIPage = new InvoicePage('API', 'wx_fragment_content');
                        thisObject.RemoveSubHeader();
                        break;
                    case 'invoice':
                        let invoiceARIPage = new InvoicePage('ARI', 'wx_fragment_content');
                        thisObject.RemoveSubHeader();
                        break;
                    case 'ap_payment':
                        let apPaymentPage = new PaymentPage('APP', 'wx_fragment_content');
                        thisObject.RemoveSubHeader();
                        break;
                    case 'ar_receipt':
                        let arReceiptPage = new PaymentPage('ARR', 'wx_fragment_content');
                        thisObject.RemoveSubHeader();
                        break;
                    /* case 'customer_retur':
                         let rmaPage = new RmaPage('wx_fragment_content');
                         thisObject.RemoveSubHeader();
                         break;*/
                    case 'payment_expense':
                        let paymentPage = new ExpensePage('wx_fragment_content');
                        thisObject.RemoveSubHeader();
                        break;
                    case 'mutasi_barang':
                        let movementPage = new MovementPage('wx_fragment_content');
                        thisObject.RemoveSubHeader();
                        break;
                    case 'stock_opname':
                        let inventoryPage = new InventoryPage('wx_fragment_content');
                        thisObject.RemoveSubHeader();
                        break;
                    case 'proses_produksi':
                        let productionPage = new ProductionPage('wx_fragment_content');
                        thisObject.RemoveSubHeader();
                        break;
                    case 'pos':
                        let posPage = new PosPage('wx_fragment_content', 'wx_fragment_subheader');

                        break;
                    case 'manage_user':
                        let manageUsersPage = new ManageUsersPage('wx_fragment_content', 'wx_fragment_subheader');
                        break;
                    case 'manage_role':
                        let rolePage = new RolePage('wx_fragment_content');
                        thisObject.RemoveSubHeader();
                        break;
                    case 'dashboard':
                        let dashboardDefaultPage = new DashboardPage('wx_fragment_content');
                        dashboardDefaultPage.Init();
                        thisObject.RemoveSubHeader();
                        break;
                    case 'print_barcode':
                        let printbarcode = new PrintProductBarcodePage('wx_fragment_content');
                        thisObject.RemoveSubHeader();
                        break;
                    case 'company':
                        let company = new CompanyPage('wx_fragment_content');
                        thisObject.RemoveSubHeader();
                        break;
                    case 'financial_report':
                        let financial_report = new ReportFinancialPage('wx_fragment_content');
                        thisObject.RemoveSubHeader();
                        break;
                    case 'coa':
                        let coa = new CoaPage('wx_fragment_content');
                        coa.Init();
                        thisObject.RemoveSubHeader();
                        break;
                    case 'configure_pos':
                        let configure_pos = new DefaultPosConfigForm('wx_fragment_content');
                      
                        thisObject.RemoveSubHeader();
                        break;
                    default:
                        let dashboardPage = new DashboardPage('wx_fragment_content');
                        thisObject.RemoveSubHeader();

                }

            });

        } catch (error) {
            apiInterface.Log(this.constructor.name, this.AttachEventHandler.name, error);
        }
    }

    /**
     * Remove Sub Header , krn yang punya sub header hanya user page
     * maka page yang lain harus dibuang sub headernya
     */
    RemoveSubHeader() {
        $('#wx_fragment_subheader').html('');

    }


    FilterMenuSection(wypexAdMenuCMTList) {

        let sections = {};

        _.forEach(wypexAdMenuCMTList, function (sideMenuCMT) {

            let arrayMenu = sections[sideMenuCMT.wypex_ad_menu_wx_parent_uu];
            // let sideMenuCM = HelperService.ConvertRowDataToViewModel(sideMenuCMT);

            if (arrayMenu) {
                arrayMenu.push(sideMenuCMT);
            } else {
                arrayMenu = [];
                arrayMenu.push(sideMenuCMT);
                if (sideMenuCMT.wypex_ad_menu_wx_parent_uu) {
                    sections[sideMenuCMT.wypex_ad_menu_wx_parent_uu] = arrayMenu;
                } else {
                    sections[sideMenuCMT.wypex_ad_menu_wx_wypex_ad_menu_uu] = arrayMenu;
                }

            }
        });

        let arrayCompleted = [];
        _.forEach(sections, function (valueArray, key) {

            if (valueArray.length > 1) {
                arrayCompleted = _.union(arrayCompleted, valueArray);

            }

        });

        let arraySorted = _.orderBy(arrayCompleted, ['wypex_ad_menu_wx_sequence_order'], ['asc']);

        return arraySorted;


    }


}
