class MMovementline{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.confirmedqty = null;this.created = null;this.createdby = null;this.description = null;this.isactive = null;this.line = null;this.m_locator_uu = null;this.m_locatorto_uu = null;this.m_movement_uu = null;this.m_movementline_uu = null;this.m_product_uu = null;this.movementqty = null;this.process_date = null;this.sync_client = null;this.targetqty = null;this.updated = null;this.updatedby = null;this.wx_isdeleted = 'N';this.wx_updated_on_server = null  }
}
class FactAcctSummary{
  constructor() {
this.start_timestamp = null;this.value = null;this.sum_dr = null;this.sum_cr = null;this.fact_acct_summary_uu = null;this.end_timestamp = null;this.amount = null;this.name = null;this.parent_value = null;this.account_type = null;this.c_period_name = null  }
}
class COrderline{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.c_order_uu = null;this.c_orderline_uu = null;this.c_tax_uu = null;this.c_uom_uu = null;this.created = null;this.createdby = null;this.isactive = null;this.line = null;this.linenetamt = null;this.m_product_uu = null;this.m_shipper_uu = null;this.priceactual = null;this.pricecost = null;this.priceentered = null;this.pricelimit = null;this.pricelist = null;this.process_date = null;this.qtydelivered = null;this.qtyentered = null;this.qtyinvoiced = null;this.qtylostsales = null;this.qtyordered = null;this.qtyreserved = null;this.sync_client = null;this.updated = null;this.updatedby = null;this.wx_isdeleted = 'N';this.wx_updated_on_server = null;this.linetaxamt = null;this.isreturn = null  }
}
class MPricelistVersion{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.created = null;this.createdby = null;this.description = null;this.isactive = null;this.m_pricelist_uu = null;this.m_pricelist_version_uu = null;this.name = null;this.process_date = null;this.sync_client = null;this.updated = null;this.updatedby = null;this.wx_isdeleted = 'N';this.wx_updated_on_server = null  }
}
class CInvoiceline{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.c_invoice_uu = null;this.c_invoiceline_uu = null;this.c_orderline_uu = null;this.c_tax_uu = null;this.c_uom_uu = null;this.created = null;this.createdby = null;this.description = null;this.isactive = null;this.line = null;this.linenetamt = null;this.linetotalamt = null;this.m_inoutline_uu = null;this.m_product_uu = null;this.priceactual = null;this.priceentered = null;this.process_date = null;this.processed = null;this.qtyentered = null;this.qtyinvoiced = null;this.sync_client = null;this.taxamt = null;this.updated = null;this.updatedby = null;this.wx_isdeleted = 'N';this.wx_updated_on_server = null;this.isreturn = null  }
}
class COrder{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.c_bpartner_location_uu = null;this.c_bpartner_uu = null;this.c_doctype_id = null;this.c_order_id = null;this.c_order_uu = null;this.c_pos_uu = null;this.created = null;this.createdby = null;this.dateacct = null;this.dateordered = null;this.description = null;this.docaction = null;this.docstatus = null;this.documentno = null;this.grandtotal = null;this.isactive = null;this.issotrx = null;this.m_pricelist_uu = null;this.m_shipper_uu = null;this.m_warehouse_uu = null;this.poreference = null;this.process_date = null;this.sync_client = null;this.totallines = null;this.updated = null;this.updatedby = null;this.wx_isdeleted = 'N';this.wx_updated_on_server = null;this.totaltaxlines = null;this.c_bankaccount_uu = null;this.pos_charge_amount = null;this.pos_payment_amount = null;this.c_invoice_uu_retur = null;this.noinvoice_retur = null  }
}
class CPayment{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.c_bankaccount_uu = null;this.c_bpartner_uu = null;this.c_charge_uu = null;this.c_doctype_id = null;this.c_doctype_uu = null;this.c_invoice_uu = null;this.c_order_uu = null;this.c_payment_uu = null;this.created = null;this.createdby = null;this.dateacct = null;this.datetrx = null;this.description = null;this.docstatus = null;this.documentno = null;this.isactive = null;this.payamt = null;this.process_date = null;this.sync_client = null;this.taxamt = null;this.updated = null;this.updatedby = null;this.writeoffamt = null;this.wx_isdeleted = 'N';this.wx_updated_on_server = null  }
}
class WxPaymentrule{
  constructor() {
this.wx_paymentrule_uu = null;this.paymentrule = null;this.name = null  }
}
class CLocation{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.address1 = null;this.c_location_uu = null;this.city = null;this.created = null;this.createdby = null;this.isactive = null;this.postal = null;this.process_date = null;this.regionname = null;this.sync_client = null;this.updated = null;this.updatedby = null;this.wx_isdeleted = 'N';this.wx_updated_on_server = null  }
}
class CInvoice{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.c_bpartner_location_uu = null;this.c_bpartner_uu = null;this.c_doctype_id = null;this.c_doctype_uu = null;this.c_invoice_uu = null;this.c_order_uu = null;this.c_payment_uu = null;this.created = null;this.createdby = null;this.dateacct = null;this.dateinvoiced = null;this.dateordered = null;this.dateprinted = null;this.description = null;this.docstatus = null;this.documentno = null;this.grandtotal = null;this.isactive = null;this.paymentrule = null;this.poreference = null;this.process_date = null;this.sync_client = null;this.totallines = null;this.updated = null;this.updatedby = null;this.wx_isdeleted = 'N';this.wx_updated_on_server = null;this.invoicereference = null;this.totaltaxlines = null  }
}
class WypexProducttype{
  constructor() {
this.wypex_producttype_uu = null;this.producttype = null;this.name = null  }
}
class WypexSynchronizationTable{
  constructor() {
this.send_to_client = null;this.send_to_server = null;this.tablename = null  }
}
class WypexSynchronizationPointState{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.created = null;this.device_id = null;this.last_updated_on_client = null;this.last_updated_on_server = null;this.tablename = null;this.username = null;this.wx_updated_on_server = null;this.wypex_synchronization_point_state_uu = null  }
}
class CUom{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.c_uom_uu = null;this.created = null;this.createdby = null;this.description = null;this.isactive = null;this.isdefault = null;this.name = null;this.process_date = null;this.sync_client = null;this.uomsymbol = null;this.uomtype = null;this.updated = null;this.updatedby = null;this.wx_updated_on_server = null;this.wx_isdeleted = 'N'  }
}
class WxtPrintProductbarcodeline{
  constructor() {
this.wxt_print_productbarcodeline_uu = null;this.wxt_print_productbarcode_uu = null;this.m_product_uu = null;this.count_print = null;this.ad_client_uu = null;this.ad_org_uu = null;this.created = null;this.updated = null;this.createdby = null;this.updatedby = null;this.sync_client = null;this.process_date = null;this.wx_updated_on_server = null;this.isskuupc = null  }
}
class CBankstatement{
  constructor() {
this.c_bankstatement_uu = null;this.ad_client_uu = null;this.ad_org_uu = null;this.c_bankaccount_uu = null;this.created = null;this.createdby = null;this.dateacct = null;this.description = null;this.docstatus = null;this.isactive = null;this.updated = null;this.updatedby = null;this.sync_client = null;this.process_date = null;this.wx_isdeleted = 'N';this.wx_updated_on_server = null  }
}
class MLocator{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.created = null;this.createdby = null;this.isactive = null;this.isdefault = null;this.m_locator_uu = null;this.m_locatortype_uu = null;this.m_warehouse_uu = null;this.process_date = null;this.sync_client = null;this.updated = null;this.updatedby = null;this.value = null;this.wx_isdeleted = 'N';this.wx_updated_on_server = null  }
}
class MShipper{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.created = null;this.createdby = null;this.isactive = null;this.m_shipper_uu = null;this.name = null;this.process_date = null;this.sync_client = null;this.updated = null;this.updatedby = null;this.wx_updated_on_server = null;this.wx_isdeleted = 'N'  }
}
class CDoctype{
  constructor() {
this.c_doctype_uu = null;this.ad_client_uu = null;this.ad_org_uu = null;this.c_doctype_id = null;this.description = null;this.isactive = null;this.name = null;this.updated = null;this.updatedby = null;this.sync_client = null;this.process_date = null  }
}
class AdOrginfo{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.ad_orginfo_uu = null;this.c_location_uu = null;this.created = null;this.createdby = null;this.email = null;this.fax = null;this.isactive = null;this.logo_uu = null;this.phone = null;this.phone2 = null;this.process_date = null;this.sync_client = null;this.taxid = null;this.updated = null;this.updatedby = null;this.wx_isdeleted = 'N';this.wx_updated_on_server = null  }
}
class WxtPaperSize{
  constructor() {
this.wxt_paper_size_uu = null;this.paper_size = null;this.ad_org_uu = null;this.ad_client_uu = null;this.sync_client = null;this.process_date = null;this.created = null;this.updated = null;this.createdby = null;this.updatedby = null;this.wx_updated_on_server = null;this.wxt_paper_type_uu = null  }
}
class CElementvalue{
  constructor() {
this.c_elementvalue_uu = null;this.value = null;this.name = null;this.wx_updated_on_server = null;this.sync_client = null;this.process_date = null;this.description = null;this.ad_org_uu = null;this.ad_client_uu = null;this.created = null;this.updated = null;this.createdby = null;this.updatedby = null;this.parent_value = null;this.account_type = null;this.transaction_code = null  }
}
class MInventoryline{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.created = null;this.createdby = null;this.currentcostprice = null;this.description = null;this.isactive = null;this.line = null;this.m_inventory_uu = null;this.m_inventoryline_uu = null;this.m_product_uu = null;this.process_date = null;this.qtybook = null;this.qtycount = null;this.sync_client = null;this.updated = null;this.updatedby = null;this.wx_isdeleted = 'N';this.wx_updated_on_server = null  }
}
class MInoutline{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.c_activity_uu = null;this.c_orderline_uu = null;this.c_uom_uu = null;this.confirmedqty = null;this.created = null;this.createdby = null;this.description = null;this.isactive = null;this.isdescription = null;this.line = null;this.m_inout_uu = null;this.m_inoutline_uu = null;this.m_locator_uu = null;this.m_product_uu = null;this.movementqty = null;this.pickedqty = null;this.process_date = null;this.processed = null;this.qtyentered = null;this.qtyoverreceipt = null;this.scrappedqty = null;this.sync_client = null;this.updated = null;this.updatedby = null;this.wx_isdeleted = 'N';this.wx_updated_on_server = null;this.isreturn = null  }
}
class MInout{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.c_bpartner_location_uu = null;this.c_bpartner_uu = null;this.c_doctype_id = null;this.c_doctype_uu = null;this.c_invoice_uu = null;this.c_order_uu = null;this.created = null;this.createdby = null;this.dateacct = null;this.dateordered = null;this.dateprinted = null;this.datereceived = null;this.deliveryrule = null;this.deliveryviarule = null;this.description = null;this.docstatus = null;this.documentno = null;this.isactive = null;this.m_inout_uu = null;this.m_shipper_uu = null;this.m_warehouse_uu = null;this.movementdate = null;this.movementtype = null;this.nopackages = null;this.pickdate = null;this.poreference = null;this.process_date = null;this.processing = null;this.shipdate = null;this.shipperaccount = null;this.sync_client = null;this.trackingno = null;this.updated = null;this.updatedby = null;this.volume = null;this.weight = null;this.wx_isdeleted = 'N';this.wx_updated_on_server = null  }
}
class WypexCUomAdOrg{
  constructor() {
this.ad_org_uu = null;this.c_uom_uu = null;this.created = null;this.createdby = null;this.process_date = null;this.sync_client = null;this.updated = null;this.updatedby = null;this.wx_isdeleted = 'N';this.wx_updated_on_server = null;this.wypex_c_uom_ad_org_uu = null;this.ad_client_uu = null  }
}
class PosCOrderline{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.c_order_uu = null;this.c_orderline_uu = null;this.c_tax_uu = null;this.c_uom_uu = null;this.created = null;this.createdby = null;this.isactive = null;this.line = null;this.linenetamt = null;this.m_product_uu = null;this.m_shipper_uu = null;this.priceactual = null;this.pricecost = null;this.priceentered = null;this.pricelimit = null;this.pricelist = null;this.process_date = null;this.qtydelivered = null;this.qtyentered = null;this.qtyinvoiced = null;this.qtylostsales = null;this.qtyordered = null;this.qtyreserved = null;this.sync_client = null;this.updated = null;this.updatedby = null;this.wx_isdeleted = 'N';this.wx_updated_on_server = null;this.linetaxamt = null;this.isreturn = null  }
}
class PosCOrder{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.c_bpartner_location_uu = null;this.c_bpartner_uu = null;this.c_doctype_id = null;this.c_order_id = null;this.c_order_uu = null;this.c_pos_uu = null;this.created = null;this.createdby = null;this.dateacct = null;this.dateordered = null;this.description = null;this.docaction = null;this.docstatus = null;this.documentno = null;this.grandtotal = null;this.isactive = null;this.issotrx = null;this.m_pricelist_uu = null;this.m_shipper_uu = null;this.m_warehouse_uu = null;this.poreference = null;this.process_date = null;this.sync_client = null;this.totallines = null;this.updated = null;this.updatedby = null;this.wx_isdeleted = 'N';this.wx_updated_on_server = null;this.totaltaxlines = null;this.c_bankaccount_uu = null;this.pos_charge_amount = null;this.pos_payment_amount = null;this.c_invoice_uu_retur = null;this.noinvoice_retur = null  }
}
class AdUser{
  constructor() {
this.ad_client_uu = null;this.ad_image_uu = null;this.ad_org_uu = null;this.ad_user_uu = null;this.created = null;this.createdby = null;this.description = null;this.isactive = null;this.name = null;this.password = null;this.phone = null;this.phone2 = null;this.process_date = null;this.sync_client = null;this.updated = null;this.updatedby = null;this.wx_updated_on_server = null;this.wx_isdeleted = 'N'  }
}
class MProduction{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.created = null;this.createdby = null;this.description = null;this.docstatus = null;this.documentno = null;this.isactive = null;this.m_locator_uu = null;this.m_product_uu = null;this.m_production_uu = null;this.movementdate = null;this.name = null;this.process_date = null;this.productionqty = null;this.sync_client = null;this.updated = null;this.updatedby = null;this.wx_isdeleted = 'N';this.wx_updated_on_server = null;this.m_warehouse_uu = null;this.c_doctype_id = null  }
}
class MCost{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.created = null;this.createdby = null;this.currentcostprice = null;this.description = null;this.isactive = null;this.m_cost_uu = null;this.m_product_uu = null;this.process_date = null;this.sync_client = null;this.updated = null;this.updatedby = null;this.wx_updated_on_server = null;this.wx_isdeleted = 'N';this.m_costelement = null  }
}
class MProductCategory{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.created = null;this.createdby = null;this.description = null;this.isactive = null;this.isdefault = null;this.m_product_category_uu = null;this.name = null;this.process_date = null;this.sync_client = null;this.updated = null;this.updatedby = null;this.wx_isdeleted = 'N';this.wx_updated_on_server = null  }
}
class CBpartner{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.c_bpartner_uu = null;this.created = null;this.createdby = null;this.description = null;this.isactive = null;this.iscustomer = null;this.isemployee = null;this.ismanufacturer = null;this.isvendor = null;this.logo_uu = null;this.name = null;this.name2 = null;this.process_date = null;this.sync_client = null;this.taxid = null;this.updated = null;this.updatedby = null;this.url = null;this.wx_isdeleted = 'N';this.wx_updated_on_server = null  }
}
class MProductprice{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.created = null;this.createdby = null;this.isactive = null;this.m_pricelist_version_uu = null;this.m_product_uu = null;this.m_productprice_uu = null;this.pricelimit = null;this.pricelist = null;this.pricestd = null;this.process_date = null;this.sync_client = null;this.updated = null;this.updatedby = null;this.wx_isdeleted = 'N';this.wx_updated_on_server = null  }
}
class MInventory{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.c_doctype_id = null;this.c_doctype_uu = null;this.created = null;this.createdby = null;this.description = null;this.docstatus = null;this.documentno = null;this.isactive = null;this.m_inventory_uu = null;this.m_warehouse_uu = null;this.movementdate = null;this.process_date = null;this.sync_client = null;this.updated = null;this.updatedby = null;this.wx_isdeleted = 'N';this.wx_updated_on_server = null  }
}
class MMovement{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.c_doctype_id = null;this.c_doctype_uu = null;this.created = null;this.createdby = null;this.datereceived = null;this.description = null;this.docstatus = null;this.documentno = null;this.isactive = null;this.m_movement_uu = null;this.movementdate = null;this.process_date = null;this.sync_client = null;this.updated = null;this.updatedby = null;this.wx_isdeleted = 'N';this.wx_updated_on_server = null;this.m_warehouse_from_uu = null;this.m_warehouse_to_uu = null  }
}
class CBank{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.c_bank_id = null;this.c_bank_uu = null;this.created = null;this.createdby = null;this.description = null;this.isactive = null;this.isownbank = null;this.name = null;this.process_date = null;this.routingno = null;this.sync_client = null;this.updated = null;this.updatedby = null  }
}
class AdRole{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.ad_role_uu = null;this.created = null;this.createdby = null;this.description = null;this.isactive = null;this.name = null;this.process_date = null;this.sync_client = null;this.updated = null;this.updatedby = null;this.wx_isdeleted = 'N';this.wx_updated_on_server = null  }
}
class CBankaccount{
  constructor() {
this.accountno = null;this.ad_client_uu = null;this.ad_org_uu = null;this.bankaccounttype = null;this.c_bank_id = null;this.c_bank_uu = null;this.c_bankaccount_uu = null;this.created = null;this.createdby = null;this.currentbalance = null;this.description = null;this.isactive = null;this.name = null;this.process_date = null;this.sync_client = null;this.updated = null;this.updatedby = null;this.wx_isdeleted = 'N';this.wx_updated_on_server = null;this.asset_element = null;this.transit_element = null  }
}
class WxtPrintProductbarcode{
  constructor() {
this.wxt_print_productbarcode_uu = null;this.wxt_paper_type_uu = null;this.wxt_paper_size_uu = null;this.ad_client_uu = null;this.ad_org_uu = null;this.created = null;this.updated = null;this.createdby = null;this.updatedby = null;this.sync_client = null;this.process_date = null;this.wx_updated_on_server = null  }
}
class CCharge{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.c_charge_uu = null;this.chargeamt = null;this.created = null;this.createdby = null;this.description = null;this.isactive = null;this.name = null;this.process_date = null;this.sync_client = null;this.updated = null;this.updatedby = null;this.wx_updated_on_server = null  }
}
class MProductBom{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.bomqty = null;this.bomtype = null;this.created = null;this.createdby = null;this.description = null;this.isactive = null;this.line = null;this.m_product_bom_uu = null;this.m_product_uu = null;this.m_productbom_uu = null;this.process_date = null;this.sync_client = null;this.updated = null;this.updatedby = null;this.wx_isdeleted = 'N';this.wx_updated_on_server = null  }
}
class WypexAuditDeleteRow{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.object_uu = null;this.tablename = null;this.updated = null;this.wx_updated_on_server = null;this.wypex_audit_delete_row_id = null  }
}
class AdUserRoles{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.ad_role_uu = null;this.ad_user_roles_uu = null;this.ad_user_uu = null;this.created = null;this.createdby = null;this.isactive = null;this.process_date = null;this.sync_client = null;this.updated = null;this.updatedby = null;this.wx_isdeleted = 'N';this.wx_updated_on_server = null  }
}
class CBpartnerLocation{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.c_bpartner_location_uu = null;this.c_bpartner_uu = null;this.c_location_uu = null;this.created = null;this.createdby = null;this.fax = null;this.isactive = null;this.name = null;this.phone = null;this.phone2 = null;this.process_date = null;this.sync_client = null;this.updated = null;this.updatedby = null;this.wx_isdeleted = 'N';this.wx_updated_on_server = null  }
}
class MStorageonhand{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.created = null;this.createdby = null;this.isactive = null;this.m_locator_uu = null;this.m_product_uu = null;this.m_storageonhand_uu = null;this.process_date = null;this.qtyonhand = null;this.sync_client = null;this.updated = null;this.updatedby = null;this.wx_updated_on_server = null  }
}
class MProductionline{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.created = null;this.createdby = null;this.description = null;this.isactive = null;this.isendproduct = null;this.line = null;this.m_locator_uu = null;this.m_product_uu = null;this.m_production_uu = null;this.m_productionline_uu = null;this.movementqty = null;this.plannedqty = null;this.process_date = null;this.processed = null;this.qtyused = null;this.sync_client = null;this.updated = null;this.updatedby = null;this.wx_isdeleted = 'N';this.wx_updated_on_server = null  }
}
class CUomConversion{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.c_uom_conversion_uu = null;this.c_uom_to_uu = null;this.c_uom_uu = null;this.created = null;this.createdby = null;this.dividerate = null;this.isactive = null;this.m_product_uu = null;this.multiplyrate = null;this.process_date = null;this.sync_client = null;this.updated = null;this.updatedby = null;this.wx_isdeleted = 'N';this.wx_updated_on_server = null  }
}
class MTransaction{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.created = null;this.createdby = null;this.isactive = null;this.m_product_uu = null;this.m_transaction_uu = null;this.movementdate = null;this.movementqty = null;this.movementtype = null;this.process_date = null;this.sync_client = null;this.updated = null;this.updatedby = null;this.docstatus = null;this.tableuuid = null;this.tablename = null;this.wx_updated_on_server = null  }
}
class WxtPaperType{
  constructor() {
this.wxt_paper_type_uu = null;this.paper_type = null;this.ad_org_uu = null;this.ad_client_uu = null;this.sync_client = null;this.process_date = null;this.created = null;this.updated = null;this.createdby = null;this.updatedby = null;this.wx_updated_on_server = null  }
}
class CBankstatementline{
  constructor() {
this.c_bankstatementline_uu = null;this.ad_client_uu = null;this.ad_org_uu = null;this.c_bankstatement_uu = null;this.c_bpartner_uu = null;this.c_charge_uu = null;this.c_currency_uu = null;this.c_invoice_uu = null;this.c_payment_uu = null;this.chargeamt = null;this.created = null;this.createdby = null;this.dateacct = null;this.description = null;this.isactive = null;this.line = null;this.statementlinedate = null;this.stmtamt = null;this.updated = null;this.updatedby = null;this.sync_client = null;this.process_date = null;this.wx_isdeleted = 'N';this.c_order_uu = null;this.wx_updated_on_server = null;this.asset_element = null;this.transit_element = null  }
}
class MProduct{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.c_uom_uu = null;this.created = null;this.createdby = null;this.description = null;this.help = null;this.imageurl = null;this.isactive = null;this.isbom = null;this.ispurchased = null;this.issold = null;this.isstocked = null;this.m_product_category_uu = null;this.m_product_uu = null;this.name = null;this.process_date = null;this.producttype = null;this.shelfwidth = null;this.sku = null;this.sync_client = null;this.upc = null;this.updated = null;this.updatedby = null;this.value = null;this.volume = null;this.weight = null;this.wx_isdeleted = 'N';this.wx_updated_on_server = null  }
}
class MWarehouse{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.c_location_uu = null;this.created = null;this.createdby = null;this.description = null;this.isactive = null;this.m_reservelocator_uu = null;this.m_warehouse_uu = null;this.m_warehousesource_uu = null;this.name = null;this.process_date = null;this.sync_client = null;this.updated = null;this.updatedby = null;this.wx_isdeleted = 'N';this.wx_updated_on_server = null  }
}
class PosTransaction{
  constructor() {
this.pos_transaction_uu = null;this.ad_org_uu = null;this.ad_client_uu = null;this.open_date = null;this.close_date = null;this.documentno = null;this.created = null;this.updated = null;this.createdby = null;this.updatedby = null;this.sync_client = null;this.process_date = null;this.docstatus = null;this.c_pos_uu = null;this.m_warehouse_uu = null;this.wx_isdeleted = 'N'  }
}
class MPricelist{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.created = null;this.createdby = null;this.description = null;this.enforcepricelimit = null;this.isactive = null;this.isdefault = null;this.ismandatory = null;this.ispresentforproduct = null;this.issopricelist = null;this.istaxincluded = null;this.m_pricelist_uu = null;this.name = null;this.priceprecision = null;this.process_date = null;this.sync_client = null;this.updated = null;this.updatedby = null;this.wx_updated_on_server = null  }
}
class WypexAdMenu{
  constructor() {
this.created = null;this.createdby = null;this.description = null;this.isactive = null;this.name = null;this.parent_uu = null;this.process_date = null;this.sequence_order = null;this.sync_client = null;this.updated = null;this.updatedby = null;this.urlmenu = null;this.wx_updated_on_server = null;this.wypex_ad_menu_uu = null  }
}
class WypexVersion{
  constructor() {
this.version_id = null;this.version = null  }
}
class AdImage{
  constructor() {
this.ad_client_id = null;this.ad_client_uu = null;this.ad_image_id = null;this.ad_image_uu = null;this.ad_org_id = null;this.ad_org_uu = null;this.binarydata = null;this.created = null;this.createdby = null;this.description = null;this.entitytype = null;this.imageurl = null;this.isactive = null;this.name = null;this.process_date = null;this.reference_uuid_object = null;this.sync_client = null;this.updated = null;this.updatedby = null;this.wx_isdeleted = 'N';this.wx_updated_on_server = null  }
}
class CPos{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.c_bankaccount_uu = null;this.c_bpartnercashtrx_uu = null;this.c_cashbook_uu = null;this.c_pos_uu = null;this.c_tax_uu = null;this.created = null;this.createdby = null;this.description = null;this.device_id = null;this.isactive = null;this.ismodifyprice = null;this.m_pricelist_version_uu = null;this.m_warehouse_uu = null;this.name = null;this.printername = null;this.process_date = null;this.sync_client = null;this.updated = null;this.updatedby = null;this.wx_isdeleted = 'N';this.m_shipper_uu = null  }
}
class PosTransactionline{
  constructor() {
this.pos_transactionline_uu = null;this.pos_transaction_uu = null;this.ad_org_uu = null;this.ad_client_uu = null;this.c_bankaccount_uu = null;this.open_amount = null;this.close_amount = null;this.created = null;this.updated = null;this.createdby = null;this.updatedby = null;this.sync_client = null;this.process_date = null;this.actual_amount = null;this.wx_isdeleted = 'N'  }
}
class WxtRoleline{
  constructor() {
this.wxt_roleline_uu = null;this.class_name = null;this.ad_role_uu = null;this.sync_client = null;this.process_date = null;this.ad_client_uu = null;this.ad_org_uu = null;this.created = null;this.updated = null;this.createdby = null;this.updatedby = null;this.data = null;this.wx_isdeleted = 'N';this.wx_updated_on_server = null  }
}
class CTax{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.c_tax_uu = null;this.created = null;this.createdby = null;this.description = null;this.isactive = null;this.isdefault = null;this.name = null;this.process_date = null;this.rate = null;this.sync_client = null;this.updated = null;this.updatedby = null;this.wx_updated_on_server = null;this.wx_isdeleted = 'N'  }
}
class CPeriod{
  constructor() {
this.name = null;this.ad_org_uu = null;this.ad_client_uu = null;this.created = null;this.updated = null;this.isopen = null;this.createdby = null;this.updatedby = null;this.c_period_uu = null;this.start_timestamp = null;this.end_timestamp = null  }
}
class WypexDeviceRegistered{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.created = null;this.createdby = null;this.description = null;this.device_id = null;this.isregistered = null;this.process_date = null;this.sync_client = null;this.updated = null;this.updatedby = null;this.wx_isdeleted = 'N';this.wx_updated_on_server = null;this.wypex_device_registered_uu = null;this.name = null  }
}
class AdOrg{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.created = null;this.createdby = null;this.name = null;this.process_date = null;this.sync_client = null;this.updated = null;this.updatedby = null;this.wx_isdeleted = 'N';this.wx_updated_on_server = null  }
}
class FactAcct{
  constructor() {
this.ad_client_uu = null;this.ad_org_uu = null;this.amtacctcr = null;this.amtacctdr = null;this.created = null;this.createdby = null;this.dateacct = null;this.description = null;this.fact_acct_uu = null;this.isactive = null;this.process_date = null;this.sync_client = null;this.updated = null;this.updatedby = null;this.wx_updated_on_server = null;this.accounttype = null;this.c_elementvalue = null;this.tablename = null;this.tableuuid = null;this.docstatus = null;this.c_period_name = null  }
}

