class WarehouseSearchWidget {

    /**
    * 
    * @param {*} idFragmentContent 
   * @param {*} functionGetResult function untuk set variable dalam form yang lain setelah widget di click , ex: var getValue = function(value){   ....     }
      * 
    */
    constructor(idFragmentContent, functionGetResult) {

        this.idFragmentContent = idFragmentContent;
        this.inputWarehouseSearch = "input_search_mwarehouse_warehouse_search_widget";
        this.idWidget = "warehouse_search_widget";
        this.warehouseService = new WarehouseService();
        this.functionGetResult = functionGetResult;
    }

    Init() {
        let thisObject = this;

        try {
            let htmlSearchMWarehouse = `
            <div id="`+ thisObject.idWidget + `">
                <label>Cari Toko / Gudang</label>
                <div class="kt-input-icon kt-input-icon--left">
                    <input type="text" class="form-control" placeholder="Type  Toko / Gudang"
                        id="`+ thisObject.inputWarehouseSearch + `">
                    <span class="kt-input-icon__icon kt-input-icon__icon--left">
                        <span><i class="la la-search"></i></span>
                    </span>
                </div>    
                <div  id="search_result" class="kt-scroll kt-notification" data-scroll="true" data-height="245" data-mobile-height="200"
                style="height: 245px;overflow:hidden;">
              
                </div>
            </div>
                `;

            $('#' + thisObject.idFragmentContent).html(htmlSearchMWarehouse);
            thisObject.RenderListHtml('');
            thisObject.EventHandler();
            KTApp.initComponents();
        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.Init.name, error);
        }



    }

    EventHandler() {

        let thisObject = this;

        try {
            $('#' + thisObject.idWidget + ' #' + thisObject.inputWarehouseSearch).focus();
            $('#' + thisObject.idWidget + ' #' + thisObject.inputWarehouseSearch).off("input");
            $('#' + thisObject.idWidget + ' #' + thisObject.inputWarehouseSearch).on("input", function () {

                try {
                    let searchValue = $(this).val();
                    $('#' + thisObject.idWidget + ' #search_result').html('');
                    if (searchValue.length >= 3) {
                       
                        thisObject.RenderListHtml(searchValue);

                    }
                } catch (error) {
                    apiInterface.Log(thisObject.constructor.name, thisObject.EventHandler.name, error);
                }

            });

            $(document).off('click', '#' + thisObject.idWidget + ' .result-warehouse-search-item');
            $(document).on('click', '#' + thisObject.idWidget + ' .result-warehouse-search-item', function (e) {
                e.preventDefault();
                let mWarehouseUu = $(this).attr('id');
                thisObject.warehouseService.FindWarehouseViewModelByMWarehouseUu(mWarehouseUu).then(function (listWarehouse) {

                    try {
                        if (listWarehouse.length > 0) {

                            let mWarehouseSelected = listWarehouse[0];

                            if (thisObject.functionGetResult){

                                thisObject.functionGetResult(mWarehouseSelected);
                            }
                            //  clear widget
                            $('#' + thisObject.idFragmentContent).html('');

                        }
                    } catch (error) {
                        apiInterface.Log(thisObject.constructor.name, thisObject.EventHandler.name, error);
                    }

                });

            });
        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.EventHandler.name, error);
        }



    }

    RenderListHtml(searchValue){
        let thisObject = this;
        let metaDataTableSearch = new MetaDataTable();
        metaDataTableSearch.ad_org_uu = localStorage.getItem('ad_org_uu');

        if (searchValue){
            metaDataTableSearch.searchField = ' m_warehouse.name ';
            metaDataTableSearch.search = searchValue;
        }

        metaDataTableSearch.page = 0;
        metaDataTableSearch.perpage = 10;
      
        thisObject.warehouseService.FindAll(metaDataTableSearch).then(function (rows) {

            try {
                _.forEach(rows, function (row) {
                    let itemMWarehouse = `<a href="#" id='` + row.m_warehouse_wx_m_warehouse_uu + `' class="result-warehouse-search-item kt-notification__item">
                        <div class="kt-notification__item-icon">
                            <i class="flaticon2-box-1 kt-font-brand"></i>
                        </div>
                        <div class="kt-notification__item-details">
                            <div  class="kt-notification__item-title">
                                `+ row.m_warehouse_wx_name + `
                            </div>						
                        </div>
                    
                    </a>`;
                    $('#' + thisObject.idWidget + ' #search_result').append(itemMWarehouse);


                });

            } catch (error) {
                apiInterface.Log(thisObject.constructor.name, thisObject.EventHandler.name, error);
            }



        });

    }

}