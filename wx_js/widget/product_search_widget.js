class ProductSearchWidget {

    /**
     * 
     * @param {*} idFragmentContent 
     * @param {*} functionGetResult function untuk set variable dalam form yang lain setelah widget di click , ex: var getValue = function(value){   ....     }
     *            value dalam functionGetResult adalah productCMT
     */
    constructor(idFragmentContent, functionGetResult, searchValue) {
        this.idFragmentContent = idFragmentContent;
        this.inputSearch = "input_search_product_product_search_widget";
        this.idWidget = "product_search_widget";
        this.productService = new ProductService();
        this.functionGetResult = functionGetResult;
        this.searchValue = '';
        if (searchValue) {
            this.searchValue = searchValue;
        }
    }


    Init() {

        let thisObject = this;
        try {
            let htmlSearchMProduct = `
        <div id="`+ thisObject.idWidget + `">
            <label>Cari Produk</label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" placeholder="Ketik nama produk atau sku atau upc"
                id="`+ thisObject.inputSearch + `">
                <span class="kt-input-icon__icon kt-input-icon__icon--left">
                    <span><i class="la la-search"></i></span>
                </span>
            </div>
      
            <div  id="search_result" class="kt-scroll kt-notification" data-scroll="true" data-height="245" data-mobile-height="200"
                style="height: 245px;overflow:hidden;">
              
            </div>
        </div>   
            `;
            $('#' + thisObject.idFragmentContent).html(htmlSearchMProduct);

            thisObject.EventHandler();
            //   $('#' + thisObject.idFormValidation + ' #fragment_search_content').html(htmlSearchMProduct);
            $('#' + thisObject.idWidget + ' #' + thisObject.inputSearch).val(thisObject.searchValue);
            thisObject.RenderListHtml(thisObject.searchValue);
            KTApp.initComponents();

        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.Init.name, error);
        }


    }

    EventHandler() {

        let thisObject = this;

        $('#' + thisObject.idWidget + ' #' + thisObject.inputSearch).focus();
        $('#' + thisObject.idWidget + ' #' + thisObject.inputSearch).off("input");
        $('#' + thisObject.idWidget + ' #' + thisObject.inputSearch).on("input", function () {
            try {
                let searchValue = $(this).val();
                $('#' + thisObject.idWidget + ' #search_result').html('');
                if (searchValue.length >= 3) {

                    thisObject.RenderListHtml(searchValue);

                }
            } catch (error) {
                apiInterface.Log(thisObject.constructor.name, thisObject.EventHandler.name, error);
            }

        });


        $(document).off('click', '#' + thisObject.idWidget + ' .result-product-search-item');
        $(document).on('click', '#' + thisObject.idWidget + ' .result-product-search-item', function (e) {
            e.preventDefault();
            let mProductUu = $(this).attr('id');
            //let productViewModelJson = $('#' + thisObject.idFormValidation + ' #result-product-search-item-object-' + mProductUu).val();

            thisObject.productService.FindProductViewModelByMProductUu(mProductUu).then(function (listProduct) {

                if (listProduct.length > 0) {

                    thisObject.functionGetResult(listProduct[0]);
                    // $('#' + thisObject.idWidget + ' #c_bankaccount_wx_c_bankaccount_uu').val(bankaccountSelected.c_bankaccount_wx_c_bankaccount_uu);
                    //  clear widget
                    $('#' + thisObject.idFragmentContent).html('');


                }
            });


        });

    }

    RenderListHtml(searchValue) {

        let thisObject = this;
        let metaDataTableSearch = new MetaDataTable();
        metaDataTableSearch.ad_org_uu = localStorage.getItem('ad_org_uu');

        if (searchValue) {
            metaDataTableSearch.otherFilters = [' AND m_product.name LIKE  \'%' + searchValue + '%\'', ' OR m_product.sku LIKE  \'%' + searchValue + '%\'', ' OR m_product.upc LIKE  \'%' + searchValue + '%\''];
        }

        metaDataTableSearch.page = 0;
        metaDataTableSearch.perpage = 10;

        thisObject.productService.FindAll(metaDataTableSearch).then(function (rows) {
            _.forEach(rows, function (row) {
                let itemProduct = `<a href="#" id='` + row.m_product_wx_m_product_uu + `' class="result-product-search-item kt-notification__item">
                <div class="kt-notification__item-icon">
                    <i class="flaticon2-box-1 kt-font-brand"></i>
                </div>
                <div class="kt-notification__item-details">
                    <div  class="kt-notification__item-title">
                        `+ row.m_product_wx_name + `
                    </div>
                    <div class="kt-notification__item-time">
                        SKU : `+ row.m_product_wx_sku + `
                    </div>
                    <div class="kt-notification__item-time">
                        UPC : `+ row.m_product_wx_upc + `
                    </div>
                </div>
                
            </a>`;
                $('#' + thisObject.idWidget + ' #search_result').append(itemProduct);


            });

        });


    }

}