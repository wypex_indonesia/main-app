class DialogBoxStatusWidget {

    /**
     * 
     * @param {*} modalTitle Judul Dialog Box
     * @param {*} nameDeleted Nama yang akan dihapus
     * @param {*} processStatusDocumentFunction insert object function atau service dengan function run process document, dengan parameter docstatus
     */
    constructor(modalTitle, docStatus, processStatusDocumentFunction) {

        this.idFormModal = 'wx-dialogbox-status';
        this.processStatusDocumentFunction = processStatusDocumentFunction;
        this.docStatus = docStatus;

        this.idFormValidation = 'wx_dialogbox_status_form_validation';

        this.idRadioDocStatus = 'dialog_box_radio_status';
        this.modalTitle = 'Dialog Box';
        if (!modalTitle) {
            this.modalTitle = modalTitle;
        }



        this.Init();

    }



    Init() {

        let thisObject = this;

        let statusDialogHtml = thisObject.GenerateDocStatusDialogHtml();
        let formModalHtml = `
        <div class="modal fade" id="` + thisObject.idFormModal + `" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form id="`+ thisObject.idFormValidation + `"
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">`+ thisObject.modalTitle + `</h5>
                        <button type="button" class="close_modal close" data-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <div id="content_modal_info_box" class="modal-body">`+ statusDialogHtml + `                       
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="close_modal btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button id="ok_info_box" type="submit" class="btn btn-primary">OK</button>
                    </div>
                </div>
            </form>
        </div>
    </div>`;
        HelperService.InsertReplaceFormModal(thisObject.idFormModal, formModalHtml);

        this.EventHandler();

    }

    async  EventHandler() {

        let thisObject = this;

        try {
            $('#' + thisObject.idFormValidation).off('submit');
            $('#' + thisObject.idFormValidation).on('submit', function (e) {
                let docStatus = $("input[name='" + thisObject.idRadioDocStatus + "']:checked").val();
                $('#'+thisObject.idFormValidation + ' #ok_info_box').addClass(" kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light");
                $('#'+thisObject.idFormValidation + ' .close_modal').hide();
                thisObject.RunStatusDocument(docStatus);
                e.preventDefault();
            });



            $('#' + thisObject.idFormModal).modal({backdrop:'static', keyboard: false, show:true});

        } catch (error) {
            apiInterface.Log(this.constructor.name, this.EventHandler.name, error);
        }

    }

    async RunStatusDocument(docStatus) {
        let thisObject = this;

        let insertSuccess = await thisObject.processStatusDocumentFunction(docStatus);

        if (insertSuccess) {
            $('#' + thisObject.idFormModal).modal('hide');
        }

    }


    /**
	 * Generate document status html content
	 * @param {*} orderViewModelTable 
	 */
    GenerateDocStatusDialogHtml() {
        let thisObject = this;

        let statusDraftObject = { status: 'DRAFT', body: 'Simpan dokumen dengan status draft. Dokumen masih dapat dimodifikasi.' };
        let statusCompletedObject = { status: 'COMPLETED', body: 'Simpan dokumen dengan status completed. Dokumen tidak dapat dimodifikasi. Dan akan membuat transaksi record.' };
        let statusDeletedObject = { status: 'DELETED', body: 'Simpan dokumen dengan status deleted. Dokumen akan dihapus.' };
        let statusReversedObject = { status: 'REVERSED', body: 'Dokumen akan dibatalkan dan dibuatkan jurnal balik, jika ada transaksi sebelumnya yang terbentuk.' };
        let statusClosedObject = { status: 'CLOSED', body: 'Dokumen telah closed dan tidak dapat dibatalkan.' };

        let docStatusRow = [];
        switch (thisObject.docStatus) {

            case 'DRAFT':
                docStatusRow.push(statusDraftObject, statusCompletedObject, statusDeletedObject);
                break;
            default:
                docStatusRow.push(statusReversedObject, statusClosedObject);
                break;
        }

        let rowHtml = '';
        _.forEach(docStatusRow, function (objectStatus, index) {
            let checked = '';
            if (index === 0) {
                checked = 'checked';
            }
            let templateHtml = `
		<div class="row">
			<div class="col-lg-12">
				<label class="kt-option">
					<span class="kt-option__control">
						<span class="kt-radio kt-radio--bold kt-radio--brand">
							<input type="radio" name="`+ thisObject.idRadioDocStatus + `" value="` + objectStatus.status + `" ` + checked + `>
							<span></span>
						</span>
					</span>
					<span class="kt-option__label">
						<span class="kt-option__head">
							<span class="kt-option__title">
							`+ objectStatus.status + `
							</span>								
						</span>
						<span class="kt-option__body">
						`+ objectStatus.body + `
						</span>
					</span>
				</label>
			</div>
		</div>`;
            rowHtml += templateHtml;
        });


        return rowHtml;


    }



}