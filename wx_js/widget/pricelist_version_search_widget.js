class PricelistVersionSearchWidget {

    constructor(idFragmentContent, listFieldForm) {

        this.idFragmentContent = idFragmentContent;
        this.inputSearch = "input_search_pricelist_version_pricelist_version_search_widget";
        this.idWidget = "pricelist_version_search_widget";
        this.pricelistVersionService = new PricelistVersionService();
        this.listFieldForm = listFieldForm;
    }

    Init() {
        let thisObject = this;

        try {
            let htmlSearchMPricelistVersion = `
        <div id="`+ thisObject.idWidget + `">
            <label>Cari Versi Daftar Harga Jual </label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" placeholder="Cari Versi Daftar Harga Jual"
                    id="`+ thisObject.inputSearch + `">
                <span class="kt-input-icon__icon kt-input-icon__icon--left">
                    <span><i class="la la-search"></i></span>
                </span>
            </div>
	
            <div  id="search_result" class="kt-scroll kt-notification" data-scroll="true" data-height="245" data-mobile-height="200"
            style="height: 245px;overflow:hidden;">
          
            </div>
        </div>
        `;

            $('#' + thisObject.idFragmentContent).html(htmlSearchMPricelistVersion);
            thisObject.RenderListHtml('');
            thisObject.EventHandler();
            KTApp.initComponents();
        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.Init.name, error);
        }




    }

    EventHandler() {


        let thisObject = this;


        try {
            $('#' + thisObject.idWidget + ' #' + thisObject.inputSearch).focus();
            $('#' + thisObject.idWidget + ' #' + thisObject.inputSearch).off("input");
            $('#' + thisObject.idWidget + ' #' + thisObject.inputSearch).on("input", function () {

                try {
                    let searchValue = $(this).val();
                    $('#' + thisObject.idWidget + ' #search_result').html('');
                    if (searchValue.length >= 3) {
                        thisObject.RenderListHtml(searchValue);

                    }
                } catch (error) {
                    apiInterface.Log(thisObject.constructor.name, thisObject.EventHandler.name, error);
                }

            });




            $(document).off('click', '#' + thisObject.idWidget + ' .result-pricelist-version-search-item');
            $(document).on('click', '#' + thisObject.idWidget + ' .result-pricelist-version-search-item', function (e) {
                e.preventDefault();
                let m_pricelist_version_uu = $(this).attr('id');
                thisObject.pricelistVersionService.FindPricelistVersionViewModelByMPricelistVersionUu(m_pricelist_version_uu)
                    .then(function (listPricelistVersion) {
                        try {
                            if (listPricelistVersion.length > 0) {

                                let pricelistVersionSelected = listPricelistVersion[0];

                                _.forEach(thisObject.listFieldForm, function (field) {
                                    if ($('#' + field).length > 0) {
                                        $('#' + field).val(pricelistVersionSelected[field]);
                                    }

                                });


                                //  $('#' + thisObject.idFormValidation + ' #m_pricelist_version_wx_name').val(pricelistVersionSelected.m_pricelist_version_wx_name);
                                //  $('#' + thisObject.idFormValidation + ' #m_pricelist_version_wx_m_pricelist_version_uu').val(pricelistVersionSelected.m_pricelist_version_wx_m_pricelist_version_uu);
                                //  clear widget
                                $('#' + thisObject.idFragmentContent).html('');

                            }
                        } catch (error) {
                            apiInterface.Log(thisObject.constructor.name, thisObject.EventHandler.name, error);
                        }
                    });

            });

        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.EventHandler.name, error);
        }


    }

    RenderListHtml(searchValue) {
        let thisObject = this;
        let metaDataTableSearch = new MetaDataTable();
        metaDataTableSearch.ad_org_uu = localStorage.getItem('ad_org_uu');
        if (searchValue) {

            metaDataTableSearch.searchField = ' m_pricelist_version.name ';
            metaDataTableSearch.search = searchValue;
            metaDataTableSearch.otherFilters = [' AND m_pricelist.m_pricelist_uu = \'c90fbb80-589b-4943-b1eb-e22beb25178a\''];
        }

        metaDataTableSearch.page = 0;
        metaDataTableSearch.perpage = 10;
      

        thisObject.pricelistVersionService.FindAll(metaDataTableSearch).then(function (rows) {

            try {
                _.forEach(rows, function (row) {
                    let itemMPricelistVersion = `<a href="#" id='` + row.m_pricelist_version_wx_m_pricelist_version_uu + `' class="result-pricelist-version-search-item kt-notification__item">
                <div class="kt-notification__item-icon">
                    <i class="flaticon2-box-1 kt-font-brand"></i>
                </div>
                <div class="kt-notification__item-details">
                    <div  class="kt-notification__item-title">
                        `+ row.m_pricelist_version_wx_name + `
                    </div>						
                </div>
            
            </a>`;
                    $('#' + thisObject.idWidget + ' #search_result').append(itemMPricelistVersion);


                });
            } catch (error) {
                apiInterface.Log(thisObject.constructor.name, thisObject.RenderListHtml.name, error);
            }


        });

    }

}