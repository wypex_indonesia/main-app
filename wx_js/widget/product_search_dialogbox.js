class ProductSearchDialogBox {

	/**
	 * 
	 * @param {*} searchValue searchValue jika ada, jika tidak ada tinggalkan '' 
	 * @param {*} PostSelectedProductFunction function ketika produk dipilih
	 */
	constructor(searchValue, PostSelectedProductFunction) {


	
		this.PostSelectedProductFunction = PostSelectedProductFunction;

		
		this.searchValue = ''; //searchValue dari inventorylinePage, dimasukkan ke dalam search form

		if (searchValue){
			this.searchValue = searchValue;
		}


		this.inventorylineService = new InventorylineService();

		this.productService = new ProductService();

	
		this.selectedProduct = {}; // current product yang sedang dipilih, product_viewmodel_table
		this.idFormModal = 'product_search_dialog_box_modal_form';
		this.idFormValidation = 'product_search_dialog_form_validation_';
		this.classNameFormControl = 'product_search_dialog-form-control';
		this.selectorClassNameFormControl = '.' + this.classNameFormControl;

	
		this.idWidgetSearch = 'product_search_dialog_form_product_widget_search';

		this.Init();

	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery

		let allHtml = thisObject.GenerateFormInput();

		HelperService.InsertReplaceFormModal(thisObject.idFormModal, allHtml);
		$('#' + thisObject.idFormModal).modal('show');
		/*let postSelectedProduct = function (productCMT) {

			return thisObject.FunctionSelectProduct(productCMT);

		};*/
		
		let productSearchWidget = new ProductSearchWidget(thisObject.idWidgetSearch, thisObject.PostSelectedProductFunction, thisObject.searchValue);
		productSearchWidget.Init();
		
	}




	GenerateFormInput() {
		let thisObject = this;

		return `<div class="modal fade" id="` + thisObject.idFormModal + `" tabindex="-1" role="dialog" aria-labelledby="product-form-labelled"
		style="display: none;" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="product-form-labelled">Cari Produk</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div id="`+ thisObject.idWidgetSearch + `" class="modal-body"></div>			
			</div>
		</div>
	</div>`;
	}

	HideDialog(){
		let thisObject = this;
		$('#' + thisObject.idFormModal).modal('hide');
	}


	




}
