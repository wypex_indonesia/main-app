class BankaccountSearchWidget {

    /**
     * 
     * @param {*} idFragmentContent 
     * @param {*} listFieldForm  list field form yang harus diisi setelah widget di click, id field harus sesuai dengan component model table , c_bankaccount_wx_name
     * @param {*} typeBank C = Bank, B = Cash, jika null berarti semua bank dan petty cash
     */
    constructor(idFragmentContent, listFieldForm, typeBank) {
        this.idFragmentContent = idFragmentContent;
        this.inputSearch = "input_search_bankaccount_bankaccount_search_widget";
        this.idWidget = "bankaccount_search_widget";
        this.bankaccountService = new BankaccountService();
        this.listFieldForm = listFieldForm;
    }

    Init() {

        let thisObject = this;
        try {
            let htmlSearch = `
            <div id="`+ thisObject.idWidget + `">
                <label>Cari Akun Petty Cash</label>
                <div class="kt-input-icon kt-input-icon--left">
                    <input type="text" class="form-control" placeholder="Cari Akun Petty Cash"
                        id="`+ thisObject.inputSearch + `">
                    <span class="kt-input-icon__icon kt-input-icon__icon--left">
                        <span><i class="la la-search"></i></span>
                    </span>
                </div>
           
                <div  id="search_result" class="kt-scroll kt-notification" data-scroll="true" data-height="245" data-mobile-height="200"
                style="height: 245px;overflow:hidden;">
              
                </div>
            </div>
            `;

            $('#' + thisObject.idFragmentContent).html(htmlSearch);
            thisObject.RenderListHtml('');
            thisObject.EventHandler();
            KTApp.initComponents();
        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.Init.name, error);
        }






    }

    EventHandler() {

        let thisObject = this;
        try {
            $('#' + thisObject.idWidget + ' #' + thisObject.inputSearch).focus();
            $('#' + thisObject.idWidget + ' #' + thisObject.inputSearch).off("input");
            $('#' + thisObject.idWidget + ' #' + thisObject.inputSearch).on("input", function () {
                try {
                    let searchValue = $(this).val();
                    $('#' + thisObject.idWidget + ' #search_result').html('');
                    if (searchValue.length >= 3) {
                        thisObject.RenderListHtml(searchValue);

                    }
                } catch (error) {
                    apiInterface.Log(thisObject.constructor.name, thisObject.EventHandler.name, error);
                }


            });



            $(document).off('click', '#' + thisObject.idWidget + ' .result-bankaccount-search-item');
            $(document).on('click', '#' + thisObject.idWidget + ' .result-bankaccount-search-item', function (e) {
                e.preventDefault();
                try {
                    let c_bankaccount_uu = $(this).attr('id');
                    thisObject.bankaccountService.GetBankaccountCMTByCBankaccountUu(c_bankaccount_uu).then(function (listBankaccount) {

                        if (listBankaccount.length > 0) {

                            let bankaccountSelected = listBankaccount[0];


                            _.forEach(thisObject.listFieldForm, function (field) {
                                if ($('#' + field).length > 0) {
                                    $('#' + field).val(bankaccountSelected[field]);
                                }

                            });


                            // $('#' + thisObject.idWidget + ' #c_bankaccount_wx_c_bankaccount_uu').val(bankaccountSelected.c_bankaccount_wx_c_bankaccount_uu);
                            //  clear widget
                            $('#' + thisObject.idFragmentContent).html('');

                        }

                    });
                } catch (error) {
                    apiInterface.Log(thisObject.constructor.name, thisObject.EventHandler.name, error);
                }





            });






        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.EventHandler.name, error);
        }


    }

    RenderListHtml(searchValue) {
        let thisObject = this;
        let metaDataTableSearch = new MetaDataTable();
        metaDataTableSearch.ad_org_uu = localStorage.getItem('ad_org_uu');
        if (searchValue){
            metaDataTableSearch.searchField = ' c_bankaccount.name ';
            metaDataTableSearch.search = searchValue;

        }
        metaDataTableSearch.page = 0;
        metaDataTableSearch.perpage = 10;
      

        thisObject.bankaccountService.FindPettyCash(metaDataTableSearch).then(function (rows) {

            try {
                _.forEach(rows, function (row) {
                    let itemBankaccount = `<a href="#" id='` + row.c_bankaccount_wx_c_bankaccount_uu + `' class="result-bankaccount-search-item kt-notification__item">
                    <div class="kt-notification__item-icon">
                        <i class="flaticon2-box-1 kt-font-brand"></i>
                    </div>
                    <div class="kt-notification__item-details">
                        <div  class="kt-notification__item-title">
                            `+ row.c_bankaccount_wx_name + `
                        </div>						
                    </div>
                
                </a>`;
                    $('#' + thisObject.idWidget + ' #search_result').append(itemBankaccount);


                });
            } catch (error) {
                apiInterface.Log(thisObject.constructor.name, thisObject.EventHandler.name, error);
            }


        });

    }

}