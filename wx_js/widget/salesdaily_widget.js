class SalesDailyWidget{

    constructor(){        
    }

    Html(){

        let html =`
        <div class="col-xl-4 col-lg-6 order-lg-3 order-xl-1">

									<!--begin:: Widgets/New Users-->
									<div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
												10 Stock Minimum Produk
												</h3>
											</div>
											
										</div>
										<div class="kt-portlet__body">
                                             <div class="kt-widget4">
                                                <div class="kt-widget4__item">
                                                    <div class="kt-widget4__pic kt-widget4__pic--pic">
                                                        <img src="assets/media/users/100_4.jpg" alt="">
                                                    </div>
                                                    <div class="kt-widget4__info">
                                                        <a href="#" class="kt-widget4__username">
                                                            Anna Strong
                                                        </a>
                                                        <p class="kt-widget4__text">
                                                            Visual Designer,Google Inc
                                                        </p>
                                                    </div>
                                                    <a href="#" class="btn btn-sm btn-label-brand btn-bold">Follow</a>
                                                </div>
                                                <div class="kt-widget4__item">
                                                    <div class="kt-widget4__pic kt-widget4__pic--pic">
                                                        <img src="assets/media/users/100_14.jpg" alt="">
                                                    </div>
                                                    <div class="kt-widget4__info">
                                                        <a href="#" class="kt-widget4__username">
                                                            Milano Esco
                                                        </a>
                                                        <p class="kt-widget4__text">
                                                            Product Designer, Apple Inc
                                                        </p>
                                                    </div>
                                                    <a href="#" class="btn btn-sm btn-label-warning btn-bold">Follow</a>
                                                </div>
                                                <div class="kt-widget4__item">
                                                    <div class="kt-widget4__pic kt-widget4__pic--pic">
                                                        <img src="assets/media/users/100_11.jpg" alt="">
                                                    </div>
                                                    <div class="kt-widget4__info">
                                                        <a href="#" class="kt-widget4__username">
                                                            Nick Bold
                                                        </a>
                                                        <p class="kt-widget4__text">
                                                            Web Developer, Facebook Inc
                                                        </p>
                                                    </div>
                                                    <a href="#" class="btn btn-sm btn-label-danger btn-bold">Follow</a>
                                                </div>
                                                <div class="kt-widget4__item">
                                                    <div class="kt-widget4__pic kt-widget4__pic--pic">
                                                        <img src="assets/media/users/100_1.jpg" alt="">
                                                    </div>
                                                    <div class="kt-widget4__info">
                                                        <a href="#" class="kt-widget4__username">
                                                            Wiltor Delton
                                                        </a>
                                                        <p class="kt-widget4__text">
                                                            Project Manager, Amazon Inc
                                                        </p>
                                                    </div>
                                                    <a href="#" class="btn btn-sm btn-label-success btn-bold">Follow</a>
                                                </div>
                                                <div class="kt-widget4__item">
                                                    <div class="kt-widget4__pic kt-widget4__pic--pic">
                                                        <img src="assets/media/users/100_5.jpg" alt="">
                                                    </div>
                                                    <div class="kt-widget4__info">
                                                        <a href="#" class="kt-widget4__username">
                                                            Nick Stone
                                                        </a>
                                                        <p class="kt-widget4__text">
                                                            Visual Designer, Github Inc
                                                        </p>
                                                    </div>
                                                    <a href="#" class="btn btn-sm btn-label-primary btn-bold">Follow</a>
                                                </div>
                                            </div>
										</div>
									</div>

									<!--end:: Widgets/New Users-->
								</div>
        `;

        return html;
    }

}