class DialogBoxItemReturWidget {

    /**
     * 
     * @param {*} modalTitle Judul Dialog Box
     * @param {*} nameDeleted Nama yang akan dihapus
     * @param {*} functionPostItemSelected insert object function atau service dengan function run process document, dengan parameter docstatus
     */
    constructor(c_invoice_uu_retur, functionPostItemSelected, modalTitle) {

        this.idFormModal = 'wx-dialogbox-item-retur';
        this.functionPostItemSelected = functionPostItemSelected;
      

        this.idFormValidation = 'wx_dialogbox_item_retur_form_validation';

        this.classCheckboxItemRetur = 'dialog_box_checkbox_item_retur';
        this.modalTitle = 'Pilih Item';
        if (!modalTitle) {
            this.modalTitle = modalTitle;
        }
        this.c_invoice_uu_retur = c_invoice_uu_retur;


        this.Init();

    }



   async Init() {

        let thisObject = this;

        let formModalHtml = await thisObject.GenerateItemRetur();
        HelperService.InsertReplaceFormModal(thisObject.idFormModal, formModalHtml);

        this.EventHandler();

    }

    EventHandler() {

        let thisObject = this;

        try {
            $('#' + thisObject.idFormValidation).off('submit');
            $('#' + thisObject.idFormValidation).on('submit', function (e) {

                let listInvoicelineUu = [];
                $('#'+thisObject.idFormValidation + ' .'+thisObject.classCheckboxItemRetur + ':checked').each(function(){
                    let c_invoiceline_uu = $(this).attr('id');
                    listInvoicelineUu.push(c_invoiceline_uu);
                });

               
                $('#' + thisObject.idFormValidation + ' #ok_info_box').addClass(" kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light");
                $('#' + thisObject.idFormValidation + ' .close_modal').hide();
                thisObject.RunFunction(listInvoicelineUu);
                e.preventDefault();
            });



            $('#' + thisObject.idFormModal).modal({ backdrop: 'static', keyboard: false, show: true });

        } catch (error) {
            apiInterface.Log(this.constructor.name, this.EventHandler.name, error);
        }

    }

    async RunFunction(listInvoicelineUu) {
        let thisObject = this;

        let insertSuccess = await thisObject.functionPostItemSelected(listInvoicelineUu);

        if (insertSuccess) {
            $('#' + thisObject.idFormModal).modal('hide');
        }

    }


    /**
	 * Generate document status html content
	 * @param {*} orderViewModelTable 
	 */
    async GenerateItemRetur() {
        let thisObject = this;

        let invoicelineService = new InvoicelineService();

        let invoicelineCMTList = await invoicelineService.FindInvoicelineComponentModelByCInvoiceUu(thisObject.c_invoice_uu_retur);


        let listItemReturHtml = '';

        _.forEach(invoicelineCMTList, function (invoicelineCMT) {

            let linetotalamt = new Intl.NumberFormat().format(invoicelineCMT.c_invoiceline_wx_linetotalamt);
            listItemReturHtml +=
                `
            <div class="kt-mycart__item">
                <div class="kt-mycart__container">
                    <div class="kt-mycart__info">
                        <label class="kt-checkbox kt-checkbox--brand">
                            <input class="`+thisObject.classCheckboxItemRetur + `" id="`+ invoicelineCMT.c_invoiceline_wx_c_invoiceline_uu + `" type="checkbox">` + invoicelineCMT.m_product_wx_name + `
                            <span></span>
                        </label>
                        <span class="kt-mycart__desc">SKU : `+ invoicelineCMT.m_product_wx_sku + ` UPC :  ` + invoicelineCMT.m_product_wx_upc + `
                          
                        </span>
        
                    </div>
                    <span class="kt-mycart__pic">
                        <span class="kt-mycart__price">`+ invoicelineCMT.c_invoiceline_wx_qtyentered + `  ` + invoicelineCMT.c_uom_wx_name + `</span>
                        <span class="kt-mycart__price">`+ linetotalamt + ` </span>
                    </span>
                </div>
            </div>
            `
        });


        let modalHtml = `
<div class="modal fade" id="` + thisObject.idFormModal + `" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form id="`+ thisObject.idFormValidation + `"
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">`+ thisObject.modalTitle + `</h5>
                        <button type="button" class="close_modal close" data-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <div id="content_modal_info_box" class="modal-body">
                        <div class="kt-mycart">
                            <div class="kt-mycart__body kt-scroll" data-scroll="true" data-height="245" data-mobile-height="200">                    
                            `+ listItemReturHtml + `
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="close_modal btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button id="ok_info_box" type="submit" class="btn btn-primary">OK</button>
                    </div>
                </div>
            </form>
        </div>
</div>`;


        return modalHtml;


    }



}