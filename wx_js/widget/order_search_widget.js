class OrderSearchWidget {

    /**
     * 
     * @param {*} idFragmentContent 
     * @param {*} functionGetResult function untuk set variable dalam form yang lain setelah widget di click , ex: var getValue = function(value){   ....     }
     * 
     */
    constructor(idFragmentContent, functionGetResult, labelCOrder, doctypeId) {
        this.idFragmentContent = idFragmentContent;
        this.inputSearch = "input_search_order_order_search_widget";
        this.idWidget = "order_search_widget";
        this.orderService = new OrderService();
        this.functionGetResult = functionGetResult;
        this.labelCOrder = labelCOrder;
        this.doctypeId = doctypeId;
    }


    Init() {

        let thisObject = this;
        try {
            let htmlSearch = `
        <div id="`+ thisObject.idWidget + `">
            <label>`+ thisObject.labelCOrder + `</label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" placeholder="Ketik nomor order"
                id="`+ thisObject.inputSearch + `">
                <span class="kt-input-icon__icon kt-input-icon__icon--left">
                    <span><i class="la la-search"></i></span>
                </span>
            </div>
      
            <div class="kt-scroll" data-scroll="true" data-height="245" data-mobile-height="200"
                style="height: 245px;overflow:hidden;">
                <div id="search_result" class="kt-notification">				

                </div>
            </div>
        </div>   
            `;
            $('#' + thisObject.idFragmentContent).html(htmlSearch);
            thisObject.EventHandler();
        



        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.Init.name, error);
        }


    }

    EventHandler() {

        let thisObject = this;

        $('#' + thisObject.idWidget + ' #' + thisObject.inputSearch).focus();
        $('#' + thisObject.idWidget + ' #' + thisObject.inputSearch).off("input");
        $('#' + thisObject.idWidget + ' #' + thisObject.inputSearch).on("input", function () {
            try {
                let searchValue = $(this).val();
                $('#' + thisObject.idWidget + ' #' + thisObject.inputSearch).html('');

                if (searchValue.length >= 3) {
                    let metaDataTableSearch = new MetaDataTable();
                    metaDataTableSearch.ad_org_uu = localStorage.getItem('ad_org_uu');
                    metaDataTableSearch.otherFilters = [' AND c_order.documentno LIKE  \'%' + searchValue + '%\'', ' AND c_order.c_doctype_id = ' + thisObject.doctypeId, ' AND c_order.docstatus = \'COMPLETED\' OR c_order.docstatus= \'CLOSED\''];

                    thisObject.orderService.FindAll(metaDataTableSearch).then(function (rows) {

                        _.forEach(rows, function (row) {
                            let grandTotal = new Intl.NumberFormat().format(row.c_order_wx_grandtotal);
                            let itemOrder = `<a href="#" id='` + row.c_order_wx_c_order_uu + `' class="result-order-search-item kt-notification__item">
					<div class="kt-notification__item-icon">
						<i class="flaticon2-box-1 kt-font-brand"></i>
					</div>
					<div class="kt-notification__item-details">
						<div  class="kt-notification__item-title">
							`+ row.c_order_wx_documentno + `
						</div>
						<div class="kt-notification__item-time">
						`+ row.c_bpartner_wx_name + `
						</div>
						<div class="kt-notification__item-time">
							 Lokasi: `+ row.m_warehouse_wx_name + `
						</div>
						<div class="kt-notification__item-time">
							Grand Total : `+ grandTotal + `
						</div>
					</div>						
				</a>`;
                            $('#' + thisObject.idWidget + ' #search_result').append(itemOrder);


                        });

                    });


                }



            } catch (error) {
                apiInterface.Log(thisObject.constructor.name, thisObject.EventHandler.name, error);
            }




        });
        $(document).off('click', '#' + thisObject.idWidget + ' .result-order-search-item  ');
        $(document).on('click', '#' + thisObject.idWidget + ' .result-order-search-item  ', function (e) {
            e.preventDefault();
            try {
                let cOrderUu = $(this).attr('id');

             thisObject.orderService.FindOrderViewModelByCOrderUu(cOrderUu).then(function (listCOrder) {

                    if (listCOrder.length > 0) {
                        thisObject.functionGetResult(listCOrder[0]);
                        $('#' + thisObject.idFragmentContent).html('');
                    }

                });


            } catch (error) {
                apiInterface.Log(thisObject.constructor.name, thisObject.EventHandler.name, error);
            }


        });
    }

}