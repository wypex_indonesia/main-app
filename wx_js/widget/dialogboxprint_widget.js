class DialogBoxPrintWidget {

    /**
     * 
     * @param {*} modalTitle Judul Dialog Box
     * @param {*} nameDeleted Nama yang akan dihapus
     * @param {*} objectInterfaceDeletedFunction insert object function atau service dengan function delete
     */
    constructor(modalTitle, printPreviewFunction) {

        this.idFormModal = 'wx-dialogbox-print';
        this.printPreviewFunction = printPreviewFunction;
        this.idFormValidation = 'wx_dialogbox_print_form_validation';
        this.idContentPrint = 'content_modal_print_box';
        if (!modalTitle) {
            modalTitle = 'Dialog Box';
        }

        this.formModalHtml = `
        <div class="modal fade" id="` + this.idFormModal + `" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form id="`+ this.idFormValidation + `"
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">`+ modalTitle + `</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <div id="`+this.idContentPrint +`" class="modal-body">
                     
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    
                    </div>
                </div>
            </form>
        </div>
    </div>`;

        this.Init();

    }



    Init() {

        let thisObject = this;
        HelperService.InsertReplaceFormModal(thisObject.idFormModal, thisObject.formModalHtml);

        thisObject.EventHandler();

    



    }

    async  EventHandler() {

        let thisObject = this;

        try {
            $('#' + thisObject.idFormModal).on('shown.bs.modal', function (e) {

                thisObject.printPreviewFunction(thisObject.idContentPrint);
            });
            $('#' + thisObject.idFormModal).modal('show');
        } catch (error) {
            apiInterface.Log(this.constructor.name, this.EventHandler.name, error);
        }

    }
}