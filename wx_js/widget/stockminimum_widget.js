class StockMinimumWidget {

    constructor() {
      
        this.contentHtml = '';
    }


    async Html() {

        let thisObject = this;


        let sqlStatement = 'SELECT  p.name, s.qtyonhand FROM  m_storageonhand s   INNER JOIN   m_product p ON p.m_product_uu = s.m_product_uu ' +
            ' WHERE p.wx_isdeleted = \'N\' ORDER BY qtyonhand ASC LIMIT 0,10; ';


        let stockList = await apiInterface.DirectQuerySqlStatement(sqlStatement);

        try {
            _.forEach(stockList, function (stock) {
                let qtyonhand = new Intl.NumberFormat().format(stock.qtyonhand);
                let itemHtml = `
                    <div class="kt-widget4__item">
                        <div class="kt-widget4__info">
                            <span href="#" class="kt-widget4__username">`+ stock.name + `</span>
                    
                        </div>
                        <span class="btn btn-sm btn-label-brand btn-bold">`+ qtyonhand + `</span>
                    </div>                   
                                    
                        `
                    ;
                thisObject.contentHtml += itemHtml;
            });
        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.Html.name, error);
        }



        let html = `
        <div class="col-xl-4 col-lg-6 order-lg-3 order-xl-1">

            <!--begin:: Widgets/New Users-->
            <div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            10 Stock Minimum Produk
                        </h3>
                    </div>
        
                </div>
                <div class="kt-portlet__body">
                    <div class="kt-widget4">`+ this.contentHtml + `</div>
                </div>
            </div>
        
            <!--end:: Widgets/New Users-->
        </div>
        `;
        return html;
    }

}