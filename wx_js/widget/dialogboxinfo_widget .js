class DialogBoxInfoWidget {

    /**
     * 
     * @param {*} modalTitle Judul Dialog Box
     * @param {*} message  message info
     */
    constructor(modalTitle, message,showFooter) {

        this.idFormModal = 'wx-dialogbox-info';
        this.message = message;
        this.idFormValidation = 'wx_dialogbox_info_form_validation';
        this.showFooter = true;
        if (showFooter){
            this.showFooter = showFooter;
        }

        this.modalTitle = 'Dialog Box';
        if (!modalTitle) {
            this.modalTitle = modalTitle;
        }

        



        this.Init();

    }



    Init() {

        let thisObject = this;
        let modal_footer = `  <div class="modal-footer">
                    
        <button id="ok_info_box" type="submit" class="btn btn-primary">OK</button>
    </div>`;
        if (!this.showFooter){
            modal_footer = "";
        }
        
        let formModalHtml = `
        <div class="modal fade" id="` + thisObject.idFormModal + `"  data-backdrop="false" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form id="`+ thisObject.idFormValidation + `"
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">`+ thisObject.modalTitle + `</h5>
                        <button type="button" class="close_modal close" data-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <div id="content_modal_info_box" class="modal-body">`+ thisObject.message + `                       
                    </div>`+modal_footer+`                  
                </div>
            </form>
        </div>
    </div>`;
        HelperService.InsertReplaceFormModal(thisObject.idFormModal, formModalHtml);

        this.EventHandler();

    }

    async  EventHandler() {

        let thisObject = this;

        try {
            $('#' + thisObject.idFormValidation).off('submit');
            $('#' + thisObject.idFormValidation).on('submit', function (e) {
                $('#' + thisObject.idFormModal).modal('hide');
                e.preventDefault();
            });


        } catch (error) {
            apiInterface.Log(this.constructor.name, this.EventHandler.name, error);
        }

    }

    Hide(){
        let thisObject = this;
        $('#' + thisObject.idFormModal).modal('hide');

    }

 

}