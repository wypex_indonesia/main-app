class BusinesspartnerSearchWidget {

    /**
     * 
     * @param {*} idFragmentContent 
    * @param {*} functionGetResult function untuk set variable dalam form yang lain setelah widget di click , ex: var getValue = function(value){   ....     }
      */
    constructor(idFragmentContent, functionGetResult, labelVendorOrCustomer) {
        this.idFragmentContent = idFragmentContent;
        this.inputSearch = "input_search_business_partner_business_partner_search_widget";
        this.idWidget = "business_partner_search_widget";
        this.businesspartnerService = new BusinessPartnerService();
        this.functionGetResult = functionGetResult;
        this.labelVendorOrCustomer = labelVendorOrCustomer;
    }

    Init() {

        let thisObject = this;
        try {
            let htmlSearch = `
        <div id="`+ thisObject.idWidget + `">
            <label>Cari ` + thisObject.labelVendorOrCustomer + `</label>
			<div class="kt-input-icon kt-input-icon--left">
				<input type="text" class="form-control" placeholder="Search... `+ thisObject.labelVendorOrCustomer + `"
					id="`+ thisObject.inputSearch + `">
				<span class="kt-input-icon__icon kt-input-icon__icon--left">
					<span><i class="la la-search"></i></span>
				</span>
			</div>
		
            <div  id="search_result" class="kt-scroll kt-notification" data-scroll="true" data-height="245" data-mobile-height="200"
            style="height: 245px;overflow:hidden;">
          
            </div>
        </div>`;

            $('#' + thisObject.idFragmentContent).html(htmlSearch);
            thisObject.RenderListHtml('');
            thisObject.EventHandler();
            KTApp.initComponents();
        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.Init.name, error);
        }






    }

    EventHandler() {

        let thisObject = this;
        try {


            $('#' + thisObject.idWidget + ' #' + thisObject.inputSearch).focus();
            $('#' + thisObject.idWidget + ' #' + thisObject.inputSearch).off("input");
            $('#' + thisObject.idWidget + ' #' + thisObject.inputSearch).on("input", function () {
                try {
                    let searchValue = $(this).val();
                    $('#' + thisObject.idWidget + ' #search_result').html('');
                    if (searchValue.length >= 3) {
                       
                        thisObject.RenderListHtml(searchValue);


                    }
                } catch (error) {
                    apiInterface.Log(thisObject.constructor.name, thisObject.EventHandler.name, error);
                }


            });

            $(document).off('click', '#' + thisObject.idWidget + ' .result-bpartner-search-item');
            $(document).on('click', '#' + thisObject.idWidget + ' .result-bpartner-search-item', function (e) {
                e.preventDefault();
                let cBpartnerUu = $(this).attr('id');
                thisObject.businesspartnerService.FindBusinesspartnerViewModelByCBpartnerUu(cBpartnerUu).then(function (listBpartner) {
                    try {
                        if (listBpartner.length > 0) {
                           
                            apiInterface.Debug('Masuk business partner found');
                            thisObject.functionGetResult(listBpartner[0]);
                            apiInterface.Debug('sebelum clear widget');
                            //  clear widget
                          $('#' + thisObject.idFragmentContent).html('');


                        }
                    } catch (error) {
                        apiInterface.Log(thisObject.constructor.name, thisObject.EventHandler.name, error);
                    }


                });


            });



        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.EventHandler.name, error);
        }







    }

    RenderListHtml(searchValue){
        let thisObject = this;
        let metaDataTableSearch = new MetaDataTable();
        metaDataTableSearch.ad_org_uu = localStorage.getItem('ad_org_uu');

        if (searchValue){
            metaDataTableSearch.searchField = ' c_bpartner.name ';
            metaDataTableSearch.search = searchValue;

        }

        metaDataTableSearch.page = 0;
        metaDataTableSearch.perpage = 10;
      
       
        thisObject.businesspartnerService.FindAll(metaDataTableSearch).then(function (rows) {

            try {
                _.forEach(rows, function (row) {
                    let itemBPartner = `<a href="#" id='` + row.c_bpartner_wx_c_bpartner_uu + `' class="result-bpartner-search-item kt-notification__item">
                    <div class="kt-notification__item-icon">
                        <i class="flaticon2-box-1 kt-font-brand"></i>
                    </div>
                    <div class="kt-notification__item-details">
                        <div  class="kt-notification__item-title">
                            `+ row.c_bpartner_wx_name + `
                        </div>						
                    </div>
                    <input type="hidden" id="result-bpartner-search-item-object-`+ row.c_bpartner_wx_c_bpartner_uu + `" value='` + JSON.stringify(row) + `'>
                </a>`;
                    $('#' + thisObject.idWidget + ' #search_result').append(itemBPartner);


                });
            } catch (error) {
                apiInterface.Log(thisObject.constructor.name, thisObject.EventHandler.name, error);
            }



        });


    }

}