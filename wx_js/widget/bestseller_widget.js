class BestSellerWidget {

    constructor() {
       
        this.contentHtml = '';
    }


    async Html() {

        // date UTC Date.UTC(2020, 0, 1, 0, 0, 0);   tahun, bulan dimulai dari 0, tanggal, jam,menit,detik
        let currentDate = new Date();
        let year = currentDate.getFullYear();
        let month = currentDate.getUTCMonth(); // karena dimulai january = 0
        let firstDateCurrentMonth = Date.UTC(year, month, 1, 0, 0, 0);
        let thisObject = this;


        let sqlStatement = 'SELECT  p.name,  COUNT(m.movementqty * -1)  quantity FROM  m_transaction m   INNER JOIN   m_product p ON p.m_product_uu = m.m_product_uu ' +
            ' WHERE m.created > ' + firstDateCurrentMonth + '  GROUP BY  p.name  ORDER BY quantity DESC LIMIT 0,10; ';


        let bestSellerCMT = await apiInterface.DirectQuerySqlStatement(sqlStatement);

        try {
            _.forEach(bestSellerCMT, function (bestSeller) {
                let sales = new Intl.NumberFormat().format(bestSeller.quantity);
                let itemHtml = `
                    <div class="kt-widget5__item">
                        <div class="kt-widget5__content">
                    
                            <div class="kt-widget5__section">
                                <span href="#" class="kt-widget5__title">`+ bestSeller.name + `</span>
                            </div>
                        </div>
                        <div class="kt-widget5__content">
                            <div class="kt-widget5__stats">
                                <span class="kt-widget5__number">`+ sales + `</span>
                                <span class="kt-widget5__sales">sales</span>
                            </div>              
                        </div>
                    </div>                    
                        `
                    ;
                thisObject.contentHtml += itemHtml;


            });

            let html = `
    <div class="col-xl-8 col-lg-12 order-lg-3 order-xl-1">

        <!--begin:: Widgets/Best Sellers-->
        <div class="kt-portlet kt-portlet--height-fluid">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Best Sellers This Month
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="kt-widget5">`+ this.contentHtml + `</div>
            </div>
        </div>
    
        <!--end:: Widgets/Best Sellers-->
    </div>       
        `;

            return html;
        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.Init.name, error);
        }


    }

}