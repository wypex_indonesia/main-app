class DialogBoxDeleteWidget {

    /**
     * 
     * @param {*} modalTitle Judul Dialog Box
     * @param {*} nameDeleted Nama yang akan dihapus
     * @param {*} objectInterfaceDeletedFunction insert object function atau service dengan function delete
     */
    constructor(modalTitle, nameDeleted, uuidDeleted, objectDeletedFunction) {

        this.idFormModal = 'wx-dialogbox';
        this.uuidDeleted = uuidDeleted;
        this.objectDeletedFunction = objectDeletedFunction;
        this.idFormValidation = 'wx_dialogbox_delete_form_validation';
        if (!modalTitle) {
            modalTitle = 'Dialog Box';
        }

        this.formModalHtml = `
        <div class="modal fade" id="` + this.idFormModal + `" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form id="`+ this.idFormValidation + `"
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">`+ modalTitle + `</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <div id="content_modal_info_box" class="modal-body">
                        <p>Apakah anda akan menghapus `+ nameDeleted + ` ?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button id="ok_info_box" type="submit" class="btn btn-primary">OK</button>
                    </div>
                </div>
            </form>
        </div>
    </div>`;

        this.Init();

    }



    Init() {

        let thisObject = this;
        HelperService.InsertReplaceFormModal(thisObject.idFormModal, thisObject.formModalHtml);

        this.EventHandler();

    }

    async  EventHandler() {

        let thisObject = this;

        try {
            $('#' + thisObject.idFormValidation).off('submit');
            $('#' + thisObject.idFormValidation).on('submit', function (e) {

              
                thisObject.Delete();
                e.preventDefault();
            });



            $('#' + thisObject.idFormModal).modal('show');

        } catch (error) {
            apiInterface.Log(this.constructor.name, this.EventHandler.name, error);
        }

    }

    async Delete() {
        let thisObject = this;
      
        let insertSuccess = await thisObject.objectDeletedFunction.Delete(thisObject.uuidDeleted);
   
        if (insertSuccess) {
            $('#' + thisObject.idFormModal).modal('hide');
        }

        //   }




    }



}