class InvoiceSearchWidget {

    /**
     * 
     * @param {*} idFragmentContent 
     * @param {*} functionGetResult function untuk set variable dalam form yang lain setelah widget di click , ex: var getValue = function(value){   ....     }
     * 
     */
    constructor(idFragmentContent, functionGetResult, labelNoInvoice, doctypeId) {
        this.idFragmentContent = idFragmentContent;
        this.inputSearch = "input_search_invoice_invoice_search_widget";
        this.idWidget = "invoice_search_widget";
        this.invoiceService = new InvoiceService();
        this.functionGetResult = functionGetResult;
        this.labelNoInvoice = labelNoInvoice;
        this.doctypeId = doctypeId;
    }


    Init() {

        let thisObject = this;
        try {
            let htmlSearch = `
        <div id="`+ thisObject.idWidget + `">
            <label>`+ thisObject.labelNoInvoice + `</label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" placeholder="Ketik nomor dokumen"
                id="`+ thisObject.inputSearch + `">
                <span class="kt-input-icon__icon kt-input-icon__icon--left">
                    <span><i class="la la-search"></i></span>
                </span>
            </div>
      
            <div class="kt-scroll" data-scroll="true" data-height="245" data-mobile-height="200"
                style="height: 245px;overflow:hidden;">
                <div id="search_result" class="kt-notification">				

                </div>
            </div>
        </div>   
            `;
            $('#' + thisObject.idFragmentContent).html(htmlSearch);
            thisObject.EventHandler();



        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.Init.name, error);
        }


    }

    EventHandler() {

        let thisObject = this;

        $('#' + thisObject.idWidget + ' #' + thisObject.inputSearch).focus();
        $('#' + thisObject.idWidget + ' #' + thisObject.inputSearch).off("input");
        $('#' + thisObject.idWidget + ' #' + thisObject.inputSearch).on("input", function () {
            try {
                let searchValue = $(this).val();
                $('#' + thisObject.idWidget + ' #' + thisObject.inputSearch).html('');

                if (searchValue.length >= 3) {
                    let metaDataTableSearch = new MetaDataTable();
                    metaDataTableSearch.ad_org_uu = localStorage.getItem('ad_org_uu');

                    if (thisObject.doctypeId) {
                        metaDataTableSearch.otherFilters.push(' AND c_invoice.c_doctype_id = ' + thisObject.doctypeId);
                    }
                    metaDataTableSearch.otherFilters.push(' AND c_invoice.documentno LIKE  \'%' + searchValue + '%\'', ' AND c_invoice.docstatus = \'COMPLETED\' OR c_invoice.docstatus= \'CLOSED\'');

                    thisObject.invoiceService.FindAll(metaDataTableSearch).then(function (rows) {
                        _.forEach(rows, function (row) {
                            let grandTotal = new Intl.NumberFormat().format(row.c_invoice_wx_grandtotal);
                            let itemInvoice = `
                                <a href="#" id='` + row.c_invoice_wx_c_invoice_uu + `' class="result-invoice-search-item kt-notification__item">
                    <div class="kt-notification__item-icon">
                        <i class="flaticon2-box-1 kt-font-brand"></i>
                    </div>
                    <div class="kt-notification__item-details">
                        <div  class="kt-notification__item-title">
                            No Invoice : `+ row.c_invoice_wx_documentno + `
                        </div>
                        <div  class="kt-notification__item-title">
                            No Invoice Vendor : `+ row.c_invoice_wx_invoicereference + `
                        </div>
                        <div class="kt-notification__item-time">
                        `+ row.c_bpartner_wx_name + `
                        </div>
                        <div class="kt-notification__item-time">
                            No Order: `+ row.c_order_wx_documentno + `
                        </div>
                        <div class="kt-notification__item-time">
                            Grand Total : `+ grandTotal + `
                        </div>
                    </div>						
                </a>`;
                            $('#' + thisObject.idWidget + ' #search_result').append(itemInvoice);

                        });
                    });


                }



            } catch (error) {
                apiInterface.Log(thisObject.constructor.name, thisObject.EventHandler.name, error);
            }




        });
        $(document).off('click', '#' + thisObject.idWidget + ' .result-invoice-search-item  ');
        $(document).on('click', '#' + thisObject.idWidget + ' .result-invoice-search-item  ', function (e) {
            e.preventDefault();
            try {
                let c_invoice_uu = $(this).attr('id');

                thisObject.invoiceService.FindInvoiceComponentModelByCInvoiceUu(c_invoice_uu).then(function (listCInvoice) {

                    if (listCInvoice.length > 0) {
                        thisObject.functionGetResult(listCInvoice[0]);
                        $('#' + thisObject.idFragmentContent).html('');
                    }

                });


            } catch (error) {
                apiInterface.Log(thisObject.constructor.name, thisObject.EventHandler.name, error);
            }


        });
    }

}