$(document).ready(function () {


    /**
     * 1. Application dibedakan menjadi standalone dan wypex_cloud
     * Ketika standalone , maka software tidak akan upload ke server, dan transaksi stock, akan langsung dihandle di lokal PC
     * Wypex Cloud, maka software akan diupload ke server dan transaksi stock akan dilakukan di server, 
     * handphone_login_page.js
     * otp_page.js
     * create_account_admin_page.js
     * pin_device_registered_page.js
     * pin_login_page.js
     * choose_app_type.js
     * choose_app_for.js
     * 
     * 1. Standalone, handphone_login_page.js -> otp_page.js -> create_account_admin_page.js -> login_page -> main_page.js  
     * 2. Wypex Cloud, maka  dibuat pilihan apakah APP for staff or App for Owner
     *                   Pilihan APP for Owner -> handphone_login_page.js -> otp_page.js ->main_page.js
     *                   Pilihan APP for staff -> pin_device_registered_page.js -> pin_login_page.js -> main_page.js
     * 
     * 
     * 1. Standalone 
     *     FirstSetup Page :
     *      1. Administrator Form 
     *      2. Company Form
     *    
     *      
     * 
     * 
     * 
     * 
     *  */


    // sementara dump data adOrg dan adUser


    // jika belum ada tipe application maka show choose_app_type
    //  let appType = "";




    /* if (!appType){
         chooseAppType.Init();
     }else{
  
          if (appType === 'standalone'){
              // check account_admin sudah ada 
              // jika sudah ada account_admin
              let isAccountAdmin = true;
              if (isAccountAdmin){
                 // cek jika di config ada main_ad_user
                  // masuk ke main_page.js
                  let main_ad_user = "";
                  if (main_ad_user){
  
                      mainPage.Init();
                  }else{
  
                      loginPage.Init();
                  }          
  
              }else{
  
                  //createAccountAdmin.Init();
              }
  
          }
  
          if (appType === 'cloud'){
              // choose_app_for
  
              if (appFor){
                  if (appFor === 'APP_FOR_OWNER'){
                      // cek jika di config ada main_ad_user
                      let main_ad_user = "";
                      if (main_ad_user){
                          mainPage.Init();
                      }else{
  
                          // handphoneLoginPage.Init();
  
                      }
  
                  }
      
                  if (appFor === 'APP_FOR_STAFF'){
                      let main_ad_user = "";
                      if (main_ad_user){
                          mainPage.Init();
                      }else{
  
                          // pinLoginPage.Init();
  
                      }
  
                  }
              }else{
                  
                  
                  chooseAppFor.Init();
  
              }
           
  
  
  
          }
  
     }*/
    if (config.build === 'web' || config.build === 'electron') {
        let spinner = `<button class="btn btn-success btn-icon btn-circle kt-spinner kt-spinner--center kt-spinner--sm kt-spinner--light"></button>`;
        let dialogBoxInfo = new DialogBoxInfoWidget("Checking And Updating",spinner,false);

        var interval = setInterval(() => {
            $.get(config.urlNodeJs + "/checkingupdate", function( data ) {
                if (!data.isChecking){
                    clearInterval(interval);
                    dialogBoxInfo.Hide();
                }                
              });
        }, 3000);

        let mainPage = new MainPage();
    }

    if (config.build === 'desktop') {

        window.addEventListener('pywebviewready', function () {
            //   apiInterface.Debug('pywebview have been ready');
            let mainPage = new MainPage();
        });
    }

    if (config.build === 'android') {
        let mainPage = new MainPage();
    }

    if (config.build === 'dotnet') {

        CefSharp.BindObjectAsync("dotnetJs").then(function () {

            dotnetJs.showDevTools().then(function (result) {
                let mainPage = new MainPage();
            });

        });

    }





});