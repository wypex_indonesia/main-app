class MainPage {

    constructor() {
        this.Init();
    }

    Init() {
        let organizationService = new OrganizationService();
        let periodService = new PeriodService();
        let thisObject = this;
        apiInterface.Debug(" masuk main_page ");
        try {

            //hide profile menu page, karena tidak bisa menggunakan class profile menu page, karena harus di load pertama kali index.html di load
            let firstProfileMenuPage = new ProfileMenuPage('fragment_profile_menu_page');
            firstProfileMenuPage.Hide();


            organizationService.FindAdOrg().then(function (dataRowDbAdOrg) {

                try {
                    if (dataRowDbAdOrg.length > 0) {

                        localStorage.setItem('ad_org_uu', dataRowDbAdOrg[0]['ad_org_wx_ad_org_uu']);
                        localStorage.setItem('ad_client_uu', dataRowDbAdOrg[0]['ad_org_wx_ad_client_uu']);
                        let sidemenu = new SideMenu('div[name="wx_side_menu"]');
                        apiInterface.GetLocalStorage().then(function(objLocalStorage){
                            if (objLocalStorage){

                                localStorage.setItem('ad_user_uu', objLocalStorage['ad_user_uu']);

                            }
                            if (localStorage.getItem('ad_user_uu')) {
                                let userService = new UserService();
                                userService.FindUserCMTListByAdUserUu(localStorage.getItem('ad_user_uu')).then(function (userCMTList) {
    
                                    periodService.FindAndCreatePeriodOpen().then(function (c_period) {
                                        localStorage.setItem('current_period', c_period.name);
                                        localStorage.setItem('start_timestamp', c_period.start_timestamp);
                                        localStorage.setItem('end_timestamp', c_period.end_timestamp);
                                        let userCMT = userCMTList[0];
                                        let userCM = HelperService.ConvertRowDataToViewModel(userCMT);
                                        let profileMenuPage = new ProfileMenuPage('fragment_profile_menu_page', userCM);
                                        sidemenu.Init();
                                        //  profileMenuPage.Init();
                                        profileMenuPage.Show();
                                        thisObject.DefaultPage();
    
                                        let factacctService = new FactAcctService();
                                        factacctService.GetProcessClosingPeriod().then(function(arrayFunctionClosing){
    
                                            factacctService.RunProcessClosingPeriod(arrayFunctionClosing).then(function(result){
                                               
                                            });
                                        });
    
                                    });
    
    
                                });


                        } else {
                            let profileMenuPage = new ProfileMenuPage('fragment_profile_menu_page');
                            sidemenu.Clear();
                            //  profileMenuPage.Clear();
                            profileMenuPage.Hide();
                            let loginPage = new LoginPage('wx_fragment_content');
                        }
                    });

                    } else {

                        let firstSetupPage = new FirstSetupPage('wx_fragment_content', new OrganizationComponentModel(), new UserComponentModel());

                    }
                } catch (error) {
                    apiInterface.Log(thisObject.constructor.name, thisObject.Init.name, error);
                }





            });

        } catch (err) {

            apiInterface.Log(thisObject.constructor.name, thisObject.Init.name, err);
        }



    }

    DefaultPage() {

        // default page nanti bisa diconfigure tidak harus 
        let dashboardPage = new DashboardPage('wx_fragment_content');
        dashboardPage.Init();


    }


}
