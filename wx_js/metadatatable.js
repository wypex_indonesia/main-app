/*var metaDataTable = {
    ad_org_uu: '',

    page: 0, //halaman berapa
    perpage: 10, //jumlah item per halaman
    total: 0, //total jumlah item
    sort: 'asc', //sort asc/desc --- ini harus dihapus setelah dibetulin semua    
    isAscending: true,
    sortField: '',
    search: '',
    searchField: '',
    otherFilters: [] // dibutuhkan params untuk filter - filter pada saat query, merupakan filter query dalam format sql
}*/

class MetaDataTable {


    constructor() {
        this.ad_org_uu = '';

        this.page = null; //halaman berapa
        this.perpage = null; //jumlah item per halaman
        this.total = 0; //total jumlah item
        this.sort = 'asc'; //sort asc/desc --- ini harus dihapus setelah dibetulin semua    
        this.isAscending = true;
        this.sortField = '';
        this.search = '';
        this.searchField = '';
        this.otherFilters = []; // dibutuhkan params untuk filter - filter pada saat query, merupakan filter query dalam format sql
    }
}