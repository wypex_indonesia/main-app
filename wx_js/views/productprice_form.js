class ProductpriceForm {

	constructor(productpriceCM, datatableReference) {

		this.viewModel = productpriceCM;


		this.datatableReference = datatableReference; // buat reference datatable

		this.productPriceService = new ProductpriceService();

		this.idFormModal = 'm_productprice_form_wx_';
		this.idFormValidation = 'm_productprice_form_validation_';
		this.classNameFormControl = 'm_productprice-form-control';
		this.selectorClassNameFormControl = '.' + this.classNameFormControl;
		this.idButtonFormSubmit = 'm_productprice_form_submit_wx_';
		this.idWidgetSearch = 'm_productprice_form_widget_search';
		this.idBttnSearchProduct = 'm_productprice_form_search_product';
	
		this.validation = {
			id_form: this.idFormValidation,
			m_pricelist_version_wx_m_pricelist_version_uu: {
				required: true
			},
			m_productprice_wx_pricelist: {
				required: true
			},

		};

		this.Init();
	}


	async Init() {

		try {

			let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery
		
			let htmlForm = await thisObject.GenerateForm();
			HelperService.InsertReplaceFormModal(thisObject.idFormModal, htmlForm);

			HelperService.InjectViewModelToForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.viewModel);

		
			thisObject.EventHandler();



		} catch (error) {
			apiInterface.Log(this.constructor.name, this.Init.name, error);
		}


	}

	async GenerateForm() {
		let thisObject = this;

		let pricelistVersionOptions = new PricelistversionOptions(thisObject.classNameFormControl);
		let pricelistVersionOptionsHtml = await pricelistVersionOptions.Html();
		let allHtml = '';
		try {
			allHtml = `
<div class="modal fade" id="` + thisObject.idFormModal + `" tabindex="-1" role="dialog"
    aria-labelledby="product-form-labelled" style="display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="product-form-labelled">Produk Price Form</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
			</div>
			<form id="`+ thisObject.idFormValidation + `" class="kt-form">
            	<div class="modal-body">`+pricelistVersionOptionsHtml +`
                    <div class="form-group">
                        <label>Harga Maksimal</label>
                        <input id="m_productprice_wx_pricelist" type="text"
                            class="`+ thisObject.classNameFormControl + ` wx-format-money form-control"
                            aria-describedby="emailHelp" placeholder="Harga Maksimal">
                        <span class="form-text text-muted">Harga maksimal dalam pricelist</span>

                    </div>
                    <div class="form-group">
                        <label>Harga Standart</label>
                        <input id="m_productprice_wx_pricestd" type="text"
                            class="`+ thisObject.classNameFormControl + ` wx-format-money form-control"
                            aria-describedby="emailHelp" placeholder="Harga Standart">
                        <span class="form-text text-muted">Harga normal dalam pricelist</span>

                    </div>
                    <div class="form-group">
                        <label>Harga Minimal</label>
                        <input id="m_productprice_wx_pricelimit" type="text"
                            class="`+ thisObject.classNameFormControl + ` wx-format-money form-control"
                            aria-describedby="emailHelp" placeholder="Harga Minimal">
                        <span class="form-text text-muted">Harga minimal dalam pricelist</span>

                    </div>
				</div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="`+ thisObject.idButtonFormSubmit + `" type="submit" class="btn btn-primary">Save
                        changes</button>
                </div>		
			</form>
        </div>
    </div>
</div>		
				`;
		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.GenerateForm.name, error);
		}

		return allHtml;



	}

	EventHandler() {
		let thisObject = this;

		try {
		

			$('#' + thisObject.idFormValidation).off('submit');
			$('#' + thisObject.idFormValidation).on('submit', function (e) {
				e.preventDefault();
				thisObject.Save();

			});
			


			$('#' + thisObject.idFormModal).modal('show');
			$('.wx-format-money').mask('#,##0', { reverse: true });


		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.EventHandler.name, error);
		}

	}



	/**
		 *

		 */
	async Save() {
		let thisObject = this;

		try {

			let insertSuccess = 0;
			if (HelperService.CheckValidation(thisObject.validation)) {

				thisObject.viewModel = HelperService.GetViewModelFromForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.viewModel);



				insertSuccess += await thisObject.productPriceService.SaveUpdate(thisObject.viewModel, thisObject.datatableReference);
				// jika sukses tersimpan
				if (insertSuccess > 0) {

					$('#' + thisObject.idFormModal).modal('hide');


				}

			}
		} catch (err) {
			apiInterface.Log(this.constructor.name, this.Save.name, err);
		}



	}









}
