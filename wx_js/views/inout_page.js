class InoutPage {

	/**
	 * 
	 * @param {*} inoutType  // 'MMR' (MM Receipt)  MMS (MM Shipment)
	 * @param {*} idFragmentContent  // IdControl untuk meletakkan View Orderpage ex>. #fragmentContent
	 */
	constructor(inoutType, idFragmentContent) {

		this.titlePage = '';

		this.inoutType = inoutType;
		this.viewModel = new InoutViewModel();
		this.labelForm = '';
		this.labelNoDocument = '';
		this.labelVendorOrCustomer = '';
		this.labelCOrder = '';
		this.c_doctype_id = 0;
		this.idTable = 'wx_inout_table_';
		this.idFragmentContent = idFragmentContent;
		this.inoutService = new InoutService();
		this.metaDataTable = new MetaDataTable();
		this.datatableReference = {}; // buat reference datatable
		this.idShowForm = 'wx-btn-show-form-inout';
		this.idFormFragmentContent = "wx_inout_form_content";
		this.idButtonRowStatusDialog = "bttn_row_status";
		this.idButtonPrintDialog = "bttn_print_dialog";
		this.idDialogBox = 'status-dialog-box';
		this.idDialogPrint = 'print-dialog-box';
		this.labelPoReference = '';// No PO Vendor
		this.prefixContentChild = 'inoutline_page_';
		this.classDialogStatus = 'inout_page_bttn_show_dialog_status';
		this.classDialogPrint = 'inout_page_bttn_show_dialog_print';
		if (inoutType === 'MMR') {

			this.c_doctype_id = 4000;
			this.titlePage = 'Penerimaan Barang';
			this.labelNoDocument = 'No Dokumen Terima Barang';
			this.labelVendorOrCustomer = 'Vendor';
			this.labelCOrder = 'No Purchase Order';
			this.metaDataTable.otherFilters = [' AND c_doctype.docbasetype = \'MMR\''];
		}

		if (inoutType === 'MMS') {
			this.labelPoReference = 'No PO Vendor';// No PO Vendor
			this.c_doctype_id = 3000;
			this.titlePage = 'Keluar Barang';
			this.labelNoDocument = 'No Surat Jalan';
			this.labelVendorOrCustomer = 'Customer';
			this.labelCOrder = 'No Sales Order';
			this.metaDataTable.otherFilters = [' AND c_doctype.docbasetype = \'MMS\''];
		}

		this.columnsDataTable =
			[
				{
					"className": 'details-control',
					"orderable": false,
					"data": null,
					"defaultContent": '<button  type="button" class="btn btn-outline-hover-info btn-icon"><i class="la la-plus-square-o"></i></button>'
				},
				{ title: this.labelNoDocument, data: 'm_inout_wx_documentno' },
				{ title: this.labelCOrder, data: 'c_order_wx_documentno' },
				{ title: this.labelPoReference, data: 'm_inout_wx_poreference' },
				{ title: this.labelVendorOrCustomer, data: 'c_bpartner_wx_name' },
				{ title: 'Gudang/Toko', data: 'm_warehouse_wx_name' },
				{ title: "Status", data: 'm_inout_wx_docstatus' },
				{ title: "Tanggal", data: 'm_inout_wx_dateacct' },
				{ title: 'Action' }
			];

		if (inoutType === 'MMR') {

			// buang column labelPoReference, pada index 3
			this.columnsDataTable.splice(3, 1);
		}
		this.Init();

	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery


		let allHtml = `
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head kt-portlet__head--lg">
			<div  class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
			<i class="kt-font-brand flaticon2-line-chart"></i>
			</span>
			<h3 class="kt-portlet__head-title">
		 	`+ thisObject.titlePage + `
			</h3>
			</div>
			<div  class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-wrapper">
			<div class="kt-portlet__head-actions">
			<button type="button" class="` + thisObject.idShowForm + ` btn btn-bold btn-label-brand btn-sm"  name="new_record" >
			<i class="la la-plus"></i>New Record</button>	
			
			</div>
			</div>
			</div>
			</div>
			<div class="kt-portlet__body" >
			<table class="table table-striped- table-bordered table-hover table-checkable" id="`+ thisObject.idTable + `">
			
			</thead>
			</table>
			<div id="`+ thisObject.idFormFragmentContent + `"></div>
			</div>
		</div>
		`;


		this.metaDataTable.ad_org_uu = localStorage.getItem('ad_org_uu');

		//	allHtml += HelperService.DialogBox(thisObject.idDialogBox, 'Set Status Dokumen');
		allHtml += HelperService.DialogBox(thisObject.idDialogPrint, 'Print Preview');



		$('#' + thisObject.idFragmentContent).html(allHtml);


		var table = $('#' + thisObject.idTable);
		let columnDefDatatable = [
			{
				targets: -1,
				title: 'Actions',
				orderable: false,
				render: function (data, type, full, meta) {

					let buttonPrintHtml = `<button id="` + thisObject.idButtonPrintDialog + full.m_inout_wx_m_inout_uu + `" name="` + full.m_inout_wx_m_inout_uu + `" type="button" class="` + thisObject.classDialogPrint + ` btn btn-outline-hover-info btn-elevate btn-icon"> <i class="la la-print"></i></button>`;
					let buttonStatusHtml = `<button id="` + thisObject.idButtonRowStatusDialog + full.m_inout_wx_m_inout_uu + `" value="` + full.m_inout_wx_documentno + `" name="` + full.m_inout_wx_m_inout_uu + `" type="button"    class="` + thisObject.classDialogStatus + ` btn btn-outline-hover-info btn-elevate btn-icon"> <i class="la la-flag-o"></i></button>`;
					let buttonEditHtml = `<button id="bttn_row_edit` + full.m_inout_wx_m_inout_uu + `" name="` + full.m_inout_wx_m_inout_uu + `" type="button" class="` + thisObject.idShowForm + ` btn btn-outline-hover-info btn-elevate btn-icon"><i class="la la-edit"></i></button>`;


					// jika 
					let htmlAction = '';
					if (full.m_inout_wx_docstatus === 'CLOSED') {
						htmlAction = buttonPrintHtml;
					}


					if (full.m_inout_wx_docstatus === 'REVERSED') {
						htmlAction = '';
					}

					if (full.m_inout_wx_docstatus === 'COMPLETED') {
						htmlAction = buttonStatusHtml + buttonPrintHtml;
					}
					if (full.m_inout_wx_docstatus === 'DRAFT') {
						htmlAction = buttonEditHtml + buttonStatusHtml + buttonPrintHtml;
					}


					return htmlAction;
				},
			},
			{
				targets: -2,
				title: 'Tanggal',
				orderable: true,
				render: function (data, type, full, meta) {
					return HelperService.ConvertTimestampToLocateDateString(full.m_inout_wx_dateacct);
				},
			},

		];

		thisObject.inoutService.FindAll(thisObject.metaDataTable).then(function (inoutCMTList) {
			thisObject.datatableReference = table.DataTable({
				responsive: true,
				data: inoutCMTList,
				scrollX: true,
				columns: thisObject.columnsDataTable,
				columnDefs: columnDefDatatable,
				"order": [[1, 'asc']],
				createdRow: function (row, data, index) {
					if (data.extn === '') {
						var td = $(row).find("td:first");
						td.removeClass('details-control');
					}
				},
				rowCallback: function (row, data, index) {
					//console.log('rowCallback');
				}

			});
			thisObject.EventHandler();

		});



	}

	EventHandler() {
		let thisObject = this;
		// Add event listener for opening and closing details
		$('#' + thisObject.idTable + ' tbody').off('click', 'td.details-control');
		$('#' + thisObject.idTable + ' tbody').on('click', 'td.details-control', function () {
			var tr = $(this).closest('tr');
			var row = thisObject.datatableReference.row(tr);
			let inoutParent = row.data();
			if (row.child.isShown()) {
				// This row is already open - close it
				row.child.hide();
				$(this).html('<button  type="button" class="btn btn-outline-hover-info btn-icon"><i class="la la-plus-square-o"></i></button>');
			}
			else {
				let idInoutdetailContent = thisObject.prefixContentChild + inoutParent.m_inout_wx_m_inout_uu;
				let inoutlineFragment = `<div id="` + idInoutdetailContent + `"></div>`;
				row.child(inoutlineFragment).show();

				// generate orderlinePage
				let inoutlinePage = new InoutlinePage(inoutParent, idInoutdetailContent);
				inoutlinePage.Init();

				// add reference to listReferenceOrderlinePage, agar nanti bisa dipanggil lagi di orderpage, untuk membuat

				// change menjadi button minus
				$(this).html('<button  type="button" class="btn btn-outline-hover-info btn-icon"><i class="la la-minus-square-o"></i></button>');

			}

		});
		$('.' + thisObject.idShowForm).off("click");
		$('.' + thisObject.idShowForm).on("click", function (e) {

			let attrName = $(this).attr('name');
			let inoutViewModel = new InoutViewModel();
			inoutViewModel.m_inout.documentno = new Date().getTime();
			inoutViewModel.m_inout.docstatus = 'DRAFT';
			if (attrName !== 'new_record') {
				let dataEdited = thisObject.datatableReference.row($('#bttn_row_edit' + attrName).parents('tr')).data();
				inoutViewModel = HelperService.ConvertRowDataToViewModel(dataEdited);
			}
			let inoutFormPage = new InoutForm(thisObject.idFormFragmentContent, inoutViewModel, thisObject.datatableReference, thisObject.inoutType);



		});

		$('.' + thisObject.classDialogStatus).off('click');
		$('.' + thisObject.classDialogStatus).on('click', function (e) {
			let uuid = $(this).attr('name');
			let inoutCMT = thisObject.datatableReference.row($('#' + thisObject.idButtonRowStatusDialog + uuid).parents('tr')).data();
			let processDocumentFuction = function (docstatus) {

				return thisObject.UpdateStatusDialogBox(docstatus, uuid);
			};
			let dialogboxstatusWidget = new DialogBoxStatusWidget('Set Status InOut Material', inoutCMT.m_inout_wx_docstatus, processDocumentFuction);

		});

		/**
		* PRINT PREVIEW
		*/
		$('.' + thisObject.classDialogPrint).off('click');
		$('.' + thisObject.classDialogPrint).on('click', function (e) {

			let uuid = $(this).attr('name');
			let inoutCMT = thisObject.datatableReference.row($('#' + thisObject.idButtonRowStatusDialog + uuid).parents('tr')).data();

			let printPreviewFunction = function (idContentPrint) {
				let printpreviewService = new PrintPreviewService();
				return printpreviewService.PrintPreviewInout(idContentPrint, inoutCMT);
			};

			let dialogPrint = new DialogBoxPrintWidget('Cetak Surat Jalan', printPreviewFunction);

		});



		thisObject.datatableReference.off('draw');
		thisObject.datatableReference.on('draw', function () {

			thisObject.EventHandler();
		});



	}

	async	UpdateStatusDialogBox(docStatus, uuid) {
		let thisObject = this;
		let insertSuccess = 0;

		try {

			let inoutViewModelTable = thisObject.datatableReference.row($('#' + thisObject.idButtonRowStatusDialog + uuid).parents('tr')).data();
			inoutViewModelTable.m_inout_wx_docstatus = docStatus;
			insertSuccess += await thisObject.inoutService.UpdateStatus(inoutViewModelTable);

			if (insertSuccess) {
				let rowDataList = await thisObject.inoutService.FindInoutViewModelByMInoutUu(inoutViewModelTable.m_inout_wx_m_inout_uu);

				let rowData = {};

				if (rowDataList.length > 0) {
					rowData = rowDataList[0];
					if (docStatus === 'DELETED') {

						thisObject.datatableReference
							.row($('#' + thisObject.idButtonRowStatusDialog + uuid).parents('tr'))
							.remove()
							.draw();
					} else {

						if (docStatus === 'COMPLETED') {
							// cek jika ternyata child orderlinepage terbuka, maka create lagi tablenya
							if ($('#' + thisObject.prefixContentChild + rowData.m_inout_wx_m_inout_uu).length) {

								let inoutlinePage = new InoutlinePage(rowData, thisObject.prefixContentChild + rowData.m_inout_wx_m_inout_uu);
								inoutlinePage.Init();
							}
						}

						thisObject.datatableReference
							.row($('#' + thisObject.idButtonRowStatusDialog + uuid).parents('tr'))
							.data(rowData).draw();

					}
				}

			}
		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.UpdateStatusDialogBox.name, error);
			return insertSuccess;
		}



		return insertSuccess;




	}



}

