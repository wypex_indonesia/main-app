class WarehouseForm {

	/**
	 * 
	
	
	 * @param {WarehouseViewModel} warehouseCM
	 * @param {datatableReference} datatableReference data tabel pada role page, sebagai reference untuk mengupdate table tersebut	 
	 */
	constructor(warehouseCM, datatableReference) {

		this.titleForm = 'Add/Edit Warehouse/Toko';

		this.viewModel = warehouseCM;


		this.metaDataTable = new MetaDataTable();

		this.datatableReference = datatableReference; // buat reference datatable

		this.warehouseService = new WarehouseService();


		this.idFormModal = 'm_warehouse_form_wx_';
		this.idFormValidation = 'm_warehouse_form_validation_';
		this.classNameFormControl = 'm_warehouse-form-control';
		this.selectorClassNameFormControl = '.' + this.classNameFormControl;


		this.idButtonFormSubmit = 'm_warehouse_form_submit_wx_';


		this.validation = {

			id_form: this.idFormValidation,

			m_warehouse_wx_name: {
				required: true
			},


		};




		this.Init();




	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery

		try {

			HelperService.InsertReplaceFormModal(thisObject.idFormModal, thisObject.GenerateFormInput());

			HelperService.InjectViewModelToForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.viewModel);
			thisObject.PageEventHandler();
		} catch (error) {
			apiInterface.Log(this.constructor.name, this.Init.name, error);
		}


	}




	/**
	 * 
	
	 */
	async Save() {
		let thisObject = this;

		try {

			let insertSuccess = 0;
			if (HelperService.CheckValidation(thisObject.validation)) {

				thisObject.viewModel = HelperService.GetViewModelFromForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.viewModel);

				insertSuccess = await thisObject.warehouseService.SaveUpdate(thisObject.viewModel, thisObject.datatableReference);
				// jika sukses tersimpan 
				if (insertSuccess > 0) {

					$('#' + thisObject.idFormModal).modal('hide');


				}

			}
		} catch (err) {
			apiInterface.Log(this.constructor.name, this.Save.name, err);
		}



	}

	async GenerateFormInput() {
		let thisObject = this;

		let html = '';
		try {
			html = `
		<div class="modal fade" id="` + thisObject.idFormModal + `" tabindex="-1" role="dialog"
			aria-labelledby="warehouse-form-labelled" style="display: none;" aria-hidden="true">
		
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="warehouse-form-labelled">Toko/Gudang Form</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						</button>
					</div>
					<form id="`+ thisObject.idFormValidation + `" class="kt-form">
					<div class="modal-body">
							<div class="form-group">
								<label>Nama Toko / Gudang</label>
								<input id="m_warehouse_wx_name" type="text"
									class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp"
									placeholder="Masukkan nama toko / gudang">
								<span class="form-text text-muted">Masukkan nama toko atau gudang.</span>
							</div>
							<div class="form-group">
								<label>Alamat</label>
								<input id="c_location_wx_address1" type="text" value=""
									class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp"
									placeholder="Masukkan alamat">
								<span class="form-text text-muted">Masukkan alamat.</span>
							</div>
							<div class="form-group">
								<label>Kodepos</label>
								<input id="c_location_wx_postal" type="text"
									class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp"
									placeholder="Masukkan Kodepos">
								<span class="form-text text-muted">Masukkan kodepos.</span>
							</div>
							<div class="form-group">
								<label>Kota</label>
								<input id="c_location_wx_city" type="text"
									class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp"
									placeholder="Masukkan kota">
								<span class="form-text text-muted">Masukkan kota.</span>
							</div>
							<div class="form-group">
								<label>Propinsi</label>
								<input id="c_location_wx_regionname" type="text"
									class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp"
									placeholder="Propinsi">
								<span class="form-text text-muted">Masukkan propinsi.</span>
							</div>
		
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button id='warehouse-form-submit' type="submit" class="btn btn-primary">Save changes</button>
						</div>
					
					</form>
				</div>
			</div>
		</div>
`;

		} catch (error) {
			apiInterface.Log(this.constructor.name, this.GenerateFormInput.name, error);
		}

		return html;
	}


	PageEventHandler() {
		let thisObject = this;


		try {

			$('#' + thisObject.idFormValidation).off('submit');
			$('#' + thisObject.idFormValidation).on('submit', function (e) {
				thisObject.Save();
				e.preventDefault();


			});



			$('#' + thisObject.idFormModal).modal('show');


		} catch (err) {
			apiInterface.Log(this.constructor.name, this.PageEventHandler.name, err);
		}


	}



}
