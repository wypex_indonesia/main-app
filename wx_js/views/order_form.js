class OrderForm {

	constructor(orderCM, datatableReference, orderType) {

		this.viewModel = orderCM;



		this.datatableReference = datatableReference; // buat reference datatable

		this.orderService = new OrderService();

		this.orderType = orderType; // 'PO' // SO (SALES ORDER), (PURCHASE ORDER)

		this.labelForm = '';
		this.labelVendorOrCustomer = '';

		this.poReferenceHtml = ``;

		this.idFormModal = 'c_order_form_wx_';
		this.idFormValidation = 'c_order_form_validation_';
		this.classNameFormControl = 'm-product-form-control';
		this.selectorClassNameFormControl = '.' + this.classNameFormControl;
		this.idButtonFormSubmit = 'c_order_form_submit_wx_';
		this.idWidgetSearch = 'order_form_input_search';
		this.idDocTypeId = 'c_doctype_wx_c_doctype_id';
		this.idGroupBttnSearchPartner = 'order_form_group_bttn_search_bpartner';
		this.idFormGroupOptions = 'formGroupDocOptions';
		if (orderType === 'PO') {

			this.labelForm = 'Purchase Order Form';
			this.labelVendorOrCustomer = 'Vendor';

		} else {

			this.labelForm = 'Sales Order Form';
			this.labelVendorOrCustomer = 'Customer';

			this.poReferenceHtml = `<div class="form-group">
			<label>No PO Customer</label>
			<input id="c_order_wx_poreference" type="text"   class="` + this.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
			<span class="form-text text-muted">No PO dari customer</span>
		</div>`;
		}
		this.validation = {

			id_form: this.idFormValidation,


			c_bpartner_wx_name: {
				required: true
			},
			m_warehouse_wx_name: {
				required: true
			},
			c_bankaccount_wx_c_bankaccount_uu: {
				required: true
			},

			m_shipper_wx_m_shipper_uu: {
				required: true
			},

			c_doctype_wx_c_doctype_id: {
				required: true
			}


		};

		this.Init();

	}


	async	Init() {

		try {

			let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery


			let htmlForm = await thisObject.GenerateForm();
			HelperService.InsertReplaceFormModal(thisObject.idFormModal, htmlForm);

			thisObject.FilterShowFieldForm(thisObject.viewModel.c_order.c_doctype_id);

			HelperService.InjectViewModelToForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.viewModel);

			thisObject.EventHandler();

			// diletakkan disni formmodal show, karena eventHandler harus dipanggil oleh select doctype id, ketika form masih terbuka

			$('#' + thisObject.idFormModal).modal('show');

		} catch (error) {
			apiInterface.Log(this.constructor.name, this.Init.name, error);
		}


	}

	async GenerateForm() {
		let thisObject = this;
		let optionsBankAccount = new BankaccountOptions(thisObject.classNameFormControl);
		let selectOptionsBankAccountHtml = await optionsBankAccount.GenerateControlOptions();

		let docTypeOptions = new DoctypeOptions(thisObject.classNameFormControl, thisObject.idDocTypeId, thisObject.orderType);
		let controlDoctypeOptionsHtml = await docTypeOptions.GenerateControlOptions();

		let shipperOptions = new ShipperOptions(thisObject.classNameFormControl);
		let shipperOptionsHtml = await shipperOptions.Html();


		let allHtml = `				
	<div class="modal fade" id="` + thisObject.idFormModal + `" tabindex="-1" role="dialog"
		aria-labelledby="product-form-labelled" style="display: none;" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="product-form-labelled">`+ thisObject.labelForm + `</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<form id="`+ thisObject.idFormValidation + `" class="kt-form">
					<div class="modal-body">
				
						<div class="form-group">
							<label>No Dokumen</label>
							<input disabled="disabled" id="c_order_wx_documentno" type="text"
								class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp">
							<span class="form-text text-muted">No Dokumen dibuat otomatis</span>
						</div>`+ thisObject.poReferenceHtml + `
						<div id="`+ thisObject.idFormGroupOptions + `" class="form-group">
							<label>Pilih Tipe Dokumen</label>`+ controlDoctypeOptionsHtml + `							
						</div>						
						<div class="form-group form-group-marginless">
							<label>`+ thisObject.labelVendorOrCustomer + `</label>
	
							<div class="input-group">
								<input disabled="disabled" type="text"
									class="` + thisObject.classNameFormControl + ` form-control" id="c_bpartner_wx_name"
									placeholder="Cari ` + thisObject.labelVendorOrCustomer + ` ...">
								<div id ="`+ thisObject.idGroupBttnSearchPartner + `" class="input-group-append">
									
								</div>
							</div>
							<input type="hidden" class="` + thisObject.classNameFormControl + `"
								id="c_bpartner_wx_c_bpartner_uu">
						</div>
	
						<div class="form-group form-group-marginless">
							<label>Gudang / Toko</label>
							<div class="input-group">
								<input disabled="disabled" type="text"
									class="` + thisObject.classNameFormControl + ` form-control" id="m_warehouse_wx_name"
									placeholder="Cari Gudang / Toko ...">
								<div class="input-group-append">
									<span id="search_m_warehouse" class="input-group-text"><i
											class="la la-search"></i></span>
								</div>
								<input type="hidden" class="` + thisObject.classNameFormControl + `"
									id="m_warehouse_wx_m_warehouse_uu">
							</div>
						</div>						
						<div class="form-group ">
								<label>Pembayaran Via</label>
								<div class="input-group">`+ selectOptionsBankAccountHtml + `								
								</div>
								<span class="form-text text-muted">Pilih pembayaran via cash atau transfer</span>
						</div>`+ shipperOptionsHtml + `		
						<div class="form-group ">
							<label>Tanggal Transaksi</label>
							<input type="text" class="` + thisObject.classNameFormControl + ` datepicker form-control" id="c_order_wx_dateacct" readonly="" placeholder="Select date">
						</div>				
						<div class="form-group">
								<label>Dokumen Status</label>
								<input disabled="disabled" id="c_order_wx_docstatus" type="text"
									class="` + thisObject.classNameFormControl + ` form-control"
									aria-describedby="emailHelp">
								<span class="form-text text-muted">Status Dokumen DRAFT / COMPLETED / REVERSED</span>
						</div>
						<div class="form-group" id="`+ thisObject.idWidgetSearch + `">
	
	
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button id="`+ thisObject.idButtonFormSubmit + `" type="submit" class="btn btn-primary">Save
							changes</button>
					</div>
			
				</form>
			</div>
		</div>
	</div>	
				`;

		return allHtml;



	}

	EventHandler() {
		let thisObject = this;

		try {
			$('#' + thisObject.idFormValidation).off('submit');
			$('#' + thisObject.idFormValidation).on('submit', function (e) {
				e.preventDefault();
				thisObject.Save();

			});

			$('#' + thisObject.idFormValidation + ' #'+thisObject.idDocTypeId).off('change');
			$('#' + thisObject.idFormValidation + ' #'+thisObject.idDocTypeId).on('change', function(){

				let cDoctypeIdSelected = $(this).val();
				thisObject.FilterShowFieldForm(cDoctypeIdSelected);

			});




			/** BEGIN EVENT SEARCH C_BPARTNER **************************************/
			$('#' + thisObject.idFormValidation + ' #search_c_bpartner').off('click');
			$('#' + thisObject.idFormValidation + ' #search_c_bpartner').on('click', function () {


				let functionGetResult = function (bpartnerSelected) {

					$('#' + thisObject.idFormValidation + ' #c_bpartner_wx_name').val(bpartnerSelected.c_bpartner_wx_name);
					$('#' + thisObject.idFormValidation + ' #c_bpartner_wx_c_bpartner_uu').val(bpartnerSelected.c_bpartner_wx_c_bpartner_uu);

				};
				let business_partner_search_widget = new BusinesspartnerSearchWidget(thisObject.idWidgetSearch, functionGetResult, thisObject.labelVendorOrCustomer);
				business_partner_search_widget.Init();
			});




			/** END EVENT SEARCH C_BPARTNER **************************************/



			/** BEGIN EVENT SEARCH M_Warehouse **************************************/
			$('#' + thisObject.idFormValidation + ' #search_m_warehouse').off('click');
			$('#' + thisObject.idFormValidation + ' #search_m_warehouse').on('click', function () {

				let functionGetResult = function (warehouseSelected) {

					$('#' + thisObject.idFormValidation + ' #m_warehouse_wx_name').val(warehouseSelected.m_warehouse_wx_name);
					$('#' + thisObject.idFormValidation + ' #m_warehouse_wx_m_warehouse_uu').val(warehouseSelected.m_warehouse_wx_m_warehouse_uu);
				};

				let warehouse_search_widget = new WarehouseSearchWidget(thisObject.idWidgetSearch, functionGetResult);
				warehouse_search_widget.Init();
			});

			/** BEGIN EVENT SEARCH M_Product **************************************/
			$('#' + thisObject.idFormValidation + ' #search_c_invoice').off('click');
			$('#' + thisObject.idFormValidation + ' #search_c_invoice').on('click', function () {

				let functionGetResult = function (invoiceComponentModelTable) {
					$('#' + thisObject.idFormValidation + ' #c_order_wx_c_invoice_uu_retur').val(invoiceComponentModelTable.c_invoice_wx_c_invoice_uu);

					$('#' + thisObject.idFormValidation + ' #c_order_wx_noinvoice_retur').val(invoiceComponentModelTable.c_invoice_wx_documentno);

					$('#' + thisObject.idFormValidation + ' #c_bpartner_wx_name').val(invoiceComponentModelTable.c_bpartner_wx_name);

					$('#' + thisObject.idFormValidation + ' #c_bpartner_wx_c_bpartner_uu').val(invoiceComponentModelTable.c_bpartner_wx_c_bpartner_uu);


				};

				let invoice_search_widget = new InvoiceSearchWidget(thisObject.idWidgetSearch, functionGetResult, 'Invoice');
				invoice_search_widget.Init();

			});
			


		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.EventHandler.name, error);
		}

	}


	/**
		 *

		 */
	async Save() {
		let thisObject = this;

		try {

			let insertSuccess = 0;
			if (HelperService.CheckValidation(thisObject.validation)) {

				thisObject.viewModel = HelperService.GetViewModelFromForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.viewModel);

				if (thisObject.orderType === 'PO') {
					//thisObject.viewModel.c_order.m_pricelist_id = 1000001;
					thisObject.viewModel.c_order.m_pricelist_uu = 'fd61186c-6ade-4e33-9db8-fb81b82cef5b';
				} else {
					//thisObject.viewModel.c_order.m_pricelist_id = 1000002;
					thisObject.viewModel.c_order.m_pricelist_uu = 'c90fbb80-589b-4943-b1eb-e22beb25178a';
				}

				insertSuccess = await thisObject.orderService.SaveUpdate(thisObject.viewModel, thisObject.datatableReference);
				// jika sukses tersimpan
				if (insertSuccess > 0) {

					$('#' + thisObject.idFormModal).modal('hide');


				}

			}
		} catch (err) {
			apiInterface.Log(this.constructor.name, this.Save.name, err);
		}
	}


	//show field yang sesuai jika ada retur
	FilterShowFieldForm(cDoctypeIdSelected) {
		let thisObject = this;

		let bttnSearchBpartner = `<span id="search_c_bpartner" class="input-group-text"><i
		class="la la-search"></i></span>`;
		let fieldNoInvoiceRetur = `
		<div id="fieldNoInvoiceRetur" class="form-group form-group-marginless">
							<label>No Invoice Untuk Retur</label>
	
							<div class="input-group">
								<input disabled="disabled" type="text"
									class="` + thisObject.classNameFormControl + ` form-control"
									id="c_order_wx_noinvoice_retur" placeholder="Cari  no invoice retur">
								<div class="input-group-append">
									<span id="search_c_invoice" class="input-group-text"><i class="la la-search"></i></span>
								</div>
								<input type="hidden" class="` + thisObject.classNameFormControl + `"
								id="c_order_wx_c_invoice_uu_retur">
							</div>							
						</div>
		`;


		//PENJUALAN DENGAN RETUR
		if (cDoctypeIdSelected === "2002") {
			$('#' + thisObject.idGroupBttnSearchPartner).html('');// remove button search bpartner
			//show field no invoice retur
			$('#' + thisObject.idFormGroupOptions).after(fieldNoInvoiceRetur);
		} else {
			//PENJUALAN DENGAN bukan RETUR
			//show bttn 
			$('#' + thisObject.idGroupBttnSearchPartner).html(bttnSearchBpartner);
			// remove no invoice retur
			$('#fieldNoInvoiceRetur').remove();
		}
		thisObject.EventHandler();



	}

}
