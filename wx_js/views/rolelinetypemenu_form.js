class RolelineTypeMenuForm {

    /**
     * Widget untuk pemilihana menu mana yang akan muncul, tetapi yang disimpan adalah menu yang tidak contreng
     * karena akan dipergunakan sebagai filter
     */
    constructor(rolelineCM, dataTableReference) {
        this.idFormValidation = 'wxt_roleline_form_validation_';
        this.classNameFormControl = 'wxt_roleline-menu-form-control';
        this.idFormModal = 'wxt_roleline_menu_form_modal';
        this.idButtonFormSubmit = 'wxt_bttn_submit_roleline_form_validation';
        this.rolelineCM = rolelineCM;
        this.dataTableReference = dataTableReference;
        this.Init();
    }

    Init() {

        let thisObject = this;

        thisObject.GenerateForm();
    }





    GenerateForm() {
        let thisObject = this;
        let allHtml = '';
        try {
            let sideMenu = new SideMenu();


            sideMenu.FindAll().then(function (listSidemenu) {
                let sections = {};

                _.forEach(listSidemenu, function (sideMenuCMT) {

                    let arrayMenu = sections[sideMenuCMT.wypex_ad_menu_wx_parent_uu];
                    let sideMenuCM = HelperService.ConvertRowDataToViewModel(sideMenuCMT);

                    if (arrayMenu) {
                        arrayMenu.push(sideMenuCM);
                    } else {
                        arrayMenu = [];
                        arrayMenu.push(sideMenuCM);
                        if (sideMenuCMT.wypex_ad_menu_wx_parent_uu) {
                            sections[sideMenuCMT.wypex_ad_menu_wx_parent_uu] = arrayMenu;
                        } else {
                            sections[sideMenuCMT.wypex_ad_menu_wx_wypex_ad_menu_uu] = arrayMenu;
                        }

                    }
                });


                let htmlArray = [];
                _.forEach(sections, function (valueArray, key) {


                    let checkboxList = '';
                    let parentName = '';
                    _.forEach(valueArray, function (sideMenu) {
                        if (sideMenu.wypex_ad_menu.parent_uu) {
                            checkboxList += `
                            <div class="kt-checkbox-list">								
                                <label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand">
                                    <input id="`+ sideMenu.wypex_ad_menu.wypex_ad_menu_uu + `" class="` + thisObject.classNameFormControl + `" type="checkbox">` + sideMenu.wypex_ad_menu.name + `
                                    <span></span>
                                </label>
                              
                            </div>							
                            `;
                        } else {
                            parentName = sideMenu.wypex_ad_menu.name;
                        }


                    });
                    let html = '';

                    html += `<div class="col-lg-6">`;
                    html += `<label>` + parentName + `</label>`;
                    html += checkboxList;
                    html += `</div>`;

                    htmlArray.push(html);
                });

                let htmlArrayClone = JSON.parse(JSON.stringify(htmlArray));

                allHtml = '';
                _.forEach(htmlArray, function (sectionMenu, index) {


                    ++index;
                    if (index % 2 > 0) {
                        allHtml += `<div class="form-group row">` + sectionMenu;
                        if (htmlArrayClone.length === 1) {
                            allHtml += `</div>`;
                        }
                    } else {
                        allHtml += sectionMenu + '</div>';
                    }
                    htmlArrayClone.shift();




                });
                let formHtml = `
                <div class="modal fade" id="` + thisObject.idFormModal + `" tabindex="-1" role="dialog" aria-labelledby="product-form-labelled"
                style="display: none;" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="product-form-labelled">Hak Akses Menu</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                </button>
                            </div>
                            <form id="`+ thisObject.idFormValidation + `" class="kt-form">
                                <div class="modal-body">
                                    <div class="alert alert-secondary" role="alert">
                                        <div class="alert-icon"><i class="flaticon-warning kt-font-brand"></i></div>
                                        <div class="alert-text">
                                            Pilih Hak Akses Menu untuk role yang dipilih. Tanda centang, berarti menu akan muncul sesuai dengan role dari pengguna
                                        </div>
                                    </div>`+ allHtml + `
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button id="`+ thisObject.idButtonFormSubmit + `" type="submit" class="btn btn-primary">Save changes</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>`;
                HelperService.InsertReplaceFormModal(thisObject.idFormModal, formHtml);
                thisObject.InjectToForm(thisObject.rolelineCM.wxt_roleline.data);

                thisObject.EventHandler();


            });



        } catch (error) {
            apiInterface.Log(this.constructor.name, this.GenerateForm.name, error);
        }


    }



    SaveUpdate() {

        let thisObject = this;
        let menusUuid = [];
        let rolelineService = new RolelineService();
        let insertSuccess = 0;
        try {
            $('#' + thisObject.idFormValidation + ' .' + thisObject.classNameFormControl).each(function () {
                let wypex_ad_menu_uu = $(this).attr('id');
                if ($(this).attr('type') === "checkbox") {
                    if (!$(this).prop('checked')) {
                        menusUuid.push(wypex_ad_menu_uu);
                    }
                }
            });
            return new Promise(function (resolve, reject) {
                rolelineService.FindRolelineCMTListByAdRoleUuAndClassName(thisObject.rolelineCM.wxt_roleline.ad_role_uu, thisObject.rolelineCM.wxt_roleline.class_name)
                    .then(function (rolelineCMTList) {
                        if (rolelineCMTList.length) {
                            let rolelineCMT = rolelineCMTList[0];
                            thisObject.rolelineCM.wxt_roleline.wxt_roleline_uu = rolelineCMT.wxt_roleline_wx_wxt_roleline_uu;
                        }

                        thisObject.rolelineCM.wxt_roleline.data = JSON.stringify(menusUuid);


                        rolelineService.SaveUpdate(thisObject.rolelineCM, thisObject.dataTableReference)
                            .then(function (insertSuccess) {
                                if (insertSuccess) {
                                    $('#' + thisObject.idFormModal).modal('hide');
                                }

                                resolve(insertSuccess);
                            });



                    });



            });


        } catch (error) {
            apiInterface.Log(this.constructor.name, this.SaveUpdate.name, error);
        }



    }

    InjectToForm(dataJson) {

        let thisObject = this;
        try {
            if (dataJson) {

                let urlmenus = JSON.parse(dataJson);

                $('#' + thisObject.idFormValidation + ' .' + thisObject.classNameFormControl).each(function () {
                    let wypex_ad_menu_uu = $(this).attr('id');

                    if ($(this).attr('type') === "checkbox") {

                        let indexFound = _.findIndex(urlmenus, function (menu_uuid) { return menu_uuid === wypex_ad_menu_uu; });
                        if (indexFound >= 0) {
                            $(this).prop('checked', false);
                        } else {
                            $(this).prop('checked', true);
                        }
                    }


                    // masukkan value

                });
            }
        } catch (error) {
            apiInterface.Log(this.constructor.name, this.InjectToForm.name, error);
        }


    }

    EventHandler() {

        let thisObject = this;
        $('#' + thisObject.idFormValidation).off('submit');
        $('#' + thisObject.idFormValidation).on('submit', function () {

            thisObject.SaveUpdate();
        });

        $('#' + thisObject.idButtonFormSubmit).off('click');
        $('#' + thisObject.idButtonFormSubmit).on('click', function () {
            thisObject.SaveUpdate();

        });

        $('#' + thisObject.idFormModal).modal('show');

    }

}