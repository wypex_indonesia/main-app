class InoutForm {

	/**
	 * 
	
	 * @param {string} idFragmentContent name id fragment content untuk meletakkan html content dari form_html
	 * @param {InoutViewModel}
	 * @param {datatableReference} datatableReference data tabel pada inout page, sebagai reference untuk mengupdate table tersebut
	 * @param {c_doctype} inouttype MMS (MM Shipment), MMR (MM Receipt)
	 */
	constructor(idFragmentContent, inoutViewModel, datatableReference, inoutType) {

		this.titleForm = '';
		this.inoutType = inoutType;
		this.viewModel = inoutViewModel;

		this.labelNoDocument = '';
		this.labelVendorOrCustomer = '';
		this.labelCOrder = '';
		this.c_doctype_id = 0;
		this.c_order_wx_c_doctype_id = 0;
		//this.labelPoReference = 'No PO Vendor';// No PO Vendor
		this.poReferenceHtml = '';


		this.metaDataTable = new MetaDataTable();

		this.datatableReference = datatableReference; // buat reference datatable

		this.orderService = new OrderService();
		this.inoutService = new InoutService();

		this.idFragmentContent = idFragmentContent;
		this.idFormModal = 'm_inout_form_wx_';
		this.idFormValidation = 'm_inout_form_validation_';
		this.classNameFormControl = 'm-inout-form-control';
		this.selectorClassNameFormControl = '.' + this.classNameFormControl;
		this.idDialogBox = 'm_inout_dialog_box_wx_';

		this.idNewRecord = 'new_record_inout_wx_';
		this.idButtonFormSubmit = 'm_inout_form_submit_wx_';
		this.idButtonRowStatusDialog = 'bttn_row_status'; // button row status pada datatable order page
		this.idRadioDocStatus = '';// idGroupRadio Status pada document status dialog
		this.idButtonGenerateItemBarang = 'bttn_generate_inoutline';
		this.prefixContent = 'inoutline_page_'; // prefix content untuk child page 

		if (inoutType === 'MMR') {

			this.c_doctype_id = 4000;
			this.c_order_wx_c_doctype_id = 1001; // pembelian dengan PO
			this.titleForm = 'Penerimaan Barang';
			this.labelNoDocument = 'No Dokumen Terima Barang';
			this.labelVendorOrCustomer = 'Vendor';
			this.labelCOrder = 'No Purchase Order';
		}

		if (inoutType === 'MMS') {

			this.c_doctype_id = 3000;
			this.c_order_wx_c_doctype_id = 2001; // penjualan dengan SO
			this.titleForm = 'Keluar Barang';
			this.labelNoDocument = 'No Dokumen Keluar Barang';
			this.labelVendorOrCustomer = 'Customer';
			this.labelCOrder = 'No Sales Order';
			this.poReferenceHtml = `<div class="form-group">
			<label>No PO Customer</label>
			<input disabled="disabled" id="m_inout_wx_poreference" type="text"   class="` + this.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
			<span class="form-text text-muted">No PO dari customer</span>
		</div>`;
		}

		this.validation = {

			id_form: this.idFormValidation,


			c_order_wx_documentno: {
				required: true
			},
			m_warehouse_wx_name: {
				required: true
			},
			m_shipper_wx_name: {
				required: true
			}


		};




		this.Init();


		this.PageEventHandler();

	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery



		let allHtml = thisObject.GenerateFormInput();


		HelperService.InsertReplaceFormModal(thisObject.idFormModal, allHtml);

		HelperService.InjectViewModelToForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.viewModel);

	}




	/**
	 * 
	
	 */
	async Save() {
		let thisObject = this;

		let insertSuccess = 0;
		if (HelperService.CheckValidation(thisObject.validation)) {

			thisObject.viewModel = HelperService.GetViewModelFromForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.viewModel);
			thisObject.viewModel.c_doctype.c_doctype_id = thisObject.c_doctype_id;

			insertSuccess += await thisObject.inoutService.SaveUpdate(thisObject.viewModel, thisObject.datatableReference);
			// jika sukses tersimpan 
			if (insertSuccess > 0) {

				$('#' + thisObject.idFormModal).modal('hide');


			}

		}


	}

	GenerateFormInput() {
		let thisObject = this;

		return `<div class="modal fade" id="` + thisObject.idFormModal + `" tabindex="-1" role="dialog" aria-labelledby="product-form-labelled"
		style="display: none;" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="product-form-labelled">`+ thisObject.titleForm + `</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<form id="`+ thisObject.idFormValidation + `" class="kt-form">
					<div class="form-group">
						<label>No Dokumen</label>
						<input disabled="disabled" id="m_inout_wx_documentno" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
						<span class="form-text text-muted">No Dokumen dibuat otomatis</span>
					</div>
					<div class="form-group form-group-marginless">
						<label>`+ thisObject.labelCOrder + `</label>
						
						<div class="input-group">
							<input disabled="disabled" type="text" class="` + thisObject.classNameFormControl + ` form-control" id="c_order_wx_documentno" placeholder="Cari ` + thisObject.labelCOrder + ` ...">
							<div class="input-group-append">
							<span id="search_c_order" class="input-group-text"><i class="la la-search"></i></span>
							</div>
						</div>
						<input type="hidden" class="` + thisObject.classNameFormControl + `" id="c_order_wx_c_order_uu">
					</div>
					<div class="form-group">
						<label>`+ thisObject.labelVendorOrCustomer + `</label>
						<input disabled="disabled" id="c_bpartner_wx_name" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
						<span class="form-text text-muted"></span>
						<input type="hidden" class="` + thisObject.classNameFormControl + `" id="c_bpartner_wx_c_bpartner_uu">
						<input type="hidden" class="` + thisObject.classNameFormControl + `" id="c_bpartner_location_wx_c_bpartner_location_uu">
					</div>`+
			thisObject.poReferenceHtml +
			`
					<div class="form-group">
						<label>Kurir</label>
						<input disabled="disabled" id="m_shipper_wx_name" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
						<span class="form-text text-muted"></span>
						<input type="hidden" class="` + thisObject.classNameFormControl + `" id="m_shipper_wx_m_shipper_uu">
						
					</div>
					<div class="form-group">
					<label>Gudang / Toko</label>
					<input disabled="disabled" id="m_warehouse_wx_name" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
					<span class="form-text text-muted"></span>
					<input type="hidden" class="` + thisObject.classNameFormControl + `" id="m_warehouse_wx_m_warehouse_uu">
					
					</div>

					<div class="form-group">
					<label>Tracking No</label>
					<input  id="m_inout_wx_trackingno" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
					<span class="form-text text-muted">No Tracking </span>
					</div>
					<div class="form-group">
					<label>Deskripsi</label>
					<input  id="m_inout_wx_description" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
					<span class="form-text text-muted">Deskripsi</span>
					</div>		
					<div class="form-group ">
							<label>Tanggal Movement</label>
							<input type="text" class="` + thisObject.classNameFormControl + ` datepicker form-control" id="m_inout_wx_dateacct" readonly="" placeholder="Select date">
						</div>		
					<div class="form-group">
						<label>Dokumen Status</label>
						<input disabled="disabled" id="m_inout_wx_docstatus" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
						<span class="form-text text-muted">Status Dokumen DRAFT / COMPLETED / REVERSED</span>
					</div>
					<div class="form-group" id="fragment_search_content">
							
	
					</div>
					</form>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button id="`+ thisObject.idButtonFormSubmit + `" type="button" class="btn btn-primary">Save changes</button>
					</div>
				</div>
			</div>
		</div>
	</div>`;
	}


	PageEventHandler() {
		let thisObject = this;


		try {
			$('#' + thisObject.idButtonFormSubmit).off('click');
			$('#' + thisObject.idButtonFormSubmit).on('click', function () {
				thisObject.Save();

			});


			/** BEGIN EVENT SEARCH M_Product **************************************/
			$('#' + thisObject.idFormValidation + ' #search_c_order').off('click');
			$('#' + thisObject.idFormValidation + ' #search_c_order').on('click', function () {

				let htmlSearchCOrder = `<label>Cari ` + thisObject.labelCOrder + `</label>
					<div class="kt-input-icon kt-input-icon--left">
						<input type="text" class="form-control" placeholder="Ketik nomor dokumen"
							id="input_search_c_order">
						<span class="kt-input-icon__icon kt-input-icon__icon--left">
							<span><i class="la la-search"></i></span>
						</span>
					</div>
				</div>
				<div class="kt-scroll" data-scroll="true" data-height="245" data-mobile-height="200"
					style="height: 245px;overflow:hidden;">
					<div id="search_result" class="kt-notification">				
		
					</div>`;

				$('#' + thisObject.idFormValidation + ' #fragment_search_content').html(htmlSearchCOrder);

				$('#' + thisObject.idFormValidation + ' #input_search_c_order').focus();

				$('#' + thisObject.idFormValidation + ' #input_search_c_order').off("input");
				$('#' + thisObject.idFormValidation + ' #input_search_c_order').on("input", function () {

					try {
						let searchValue = $(this).val();
						$('#' + thisObject.idFormValidation + ' #search_result').html('');
						if (searchValue.length >= 3) {
							let metaDataTableSearch = new MetaDataTable();
							metaDataTableSearch.ad_org_uu = localStorage.getItem('ad_org_uu');
							metaDataTableSearch.otherFilters = [' AND c_order.documentno LIKE  \'%' + searchValue + '%\'', ' AND c_order.c_doctype_id = ' + thisObject.c_order_wx_c_doctype_id, ' AND c_order.docstatus = \'COMPLETED\' OR c_order.docstatus= \'CLOSED\''];

							thisObject.orderService.FindAll(metaDataTableSearch).then(function (rows) {

								try {
									_.forEach(rows, function (row) {
										let grandTotal = new Intl.NumberFormat().format(row.c_order_wx_grandtotal);
										let itemOrder = `<a href="#" id='` + row.c_order_wx_c_order_uu + `' class="result-order-search-item kt-notification__item">
										<div class="kt-notification__item-icon">
											<i class="flaticon2-box-1 kt-font-brand"></i>
										</div>
										<div class="kt-notification__item-details">
											<div  class="kt-notification__item-title">
												`+ row.c_order_wx_documentno + `
											</div>
											<div class="kt-notification__item-time">
											`+ row.c_bpartner_wx_name + `
											</div>
											<div class="kt-notification__item-time">
												 Lokasi: `+ row.m_warehouse_wx_name + `
											</div>
											<div class="kt-notification__item-time">
												Grand Total : `+ grandTotal + `
											</div>
										</div>						
									</a>`;
										$('#' + thisObject.idFormValidation + ' #search_result').append(itemOrder);


									});
								} catch (error) {
									apiInterface.Log(thisObject.constructor.name, thisObject.PageEventHandler.name, error);
								}




							});




						}
					} catch (error) {
						apiInterface.Log(thisObject.constructor.name, thisObject.PageEventHandler.name, error);
					}


				});
			});

			$(document).on('click', '#' + thisObject.idFormValidation + ' .result-order-search-item ', function () {

				let cOrderUu = $(this).attr('id');

				thisObject.orderService.FindOrderViewModelByCOrderUu(cOrderUu).then(function (listCOrder) {

					if (listCOrder.length > 0) {
						thisObject.orderViewModelTable = listCOrder[0];
						$('#' + thisObject.idFormValidation + ' #c_order_wx_c_order_uu').val(thisObject.orderViewModelTable.c_order_wx_c_order_uu);

						$('#' + thisObject.idFormValidation + ' #c_order_wx_documentno').val(thisObject.orderViewModelTable.c_order_wx_documentno);


						if ($('#' + thisObject.idFormValidation + ' #m_inout_wx_poreference')) {
							$('#' + thisObject.idFormValidation + ' #m_inout_wx_poreference').val(thisObject.orderViewModelTable.c_order_wx_poreference);

						}



						$('#' + thisObject.idFormValidation + ' #c_bpartner_wx_name').val(thisObject.orderViewModelTable.c_bpartner_wx_name);

						$('#' + thisObject.idFormValidation + ' #c_bpartner_wx_c_bpartner_uu').val(thisObject.orderViewModelTable.c_bpartner_wx_c_bpartner_uu);
						$('#' + thisObject.idFormValidation + ' #c_bpartner_wx_c_bpartner_location_uu').val(thisObject.orderViewModelTable.c_bpartner_wx_c_bpartner_location_uu);
						$('#' + thisObject.idFormValidation + ' #m_warehouse_wx_name').val(thisObject.orderViewModelTable.m_warehouse_wx_name);

						$('#' + thisObject.idFormValidation + ' #m_warehouse_wx_m_warehouse_uu').val(thisObject.orderViewModelTable.m_warehouse_wx_m_warehouse_uu);
						$('#' + thisObject.idFormValidation + ' #m_shipper_wx_name').val(thisObject.orderViewModelTable.m_shipper_wx_name);

						$('#' + thisObject.idFormValidation + ' #m_shipper_wx_m_shipper_uu').val(thisObject.orderViewModelTable.m_shipper_wx_m_shipper_uu);

					}


					$('#' + thisObject.idFormValidation + ' #fragment_search_content').html('');


				});



			});


			$('#' + thisObject.idFormModal).modal('show');


		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.PageEventHandler.name, error);
		}
	}




}
