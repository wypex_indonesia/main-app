class ReportBalanceSheetPortlet {


    constructor(idFragmentContent, periode) {

        this.idFragmentContent = idFragmentContent;
        this.periode = periode;
        this.titleReport = 'Laporan Neraca Saldo';
        this.idShowPrintPreview = 'report_neraca_saldo_bttn_show_print_preview';
        this.idBodyReport = 'report_neraca_saldo_portlet_body_report';

        this.rowsPdf = [];
    }

    async Init() {
        let thisObject = this;

        try {
            let html = await thisObject.Html();
            $('#' + thisObject.idFragmentContent).html(html);

            let factacctService = new FactAcctService();

            factacctService.FindFactAcctSummary(thisObject.periode).then(function (summaryTotalElementValue) {

                if (summaryTotalElementValue.length) {
                    let organizationService = new OrganizationService();

                    organizationService.FindOrganizationComponentModelByAdOrgUu(localStorage.getItem('ad_org_uu'))
                        .then(function (adOrgCMTList) {
                            let elementValueService = new ElementValueService();
                            elementValueService.FindAll().then(function (listElementvalue) {

                                thisObject.PdfReview(summaryTotalElementValue, adOrgCMTList, listElementvalue);

                            });

                        })


                } else {
                    //jika tidak ada data
                    $('#' + thisObject.idBodyReport).html('Tidak Ada Data');

                }

            });
        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.Init.name, error);
        }



    }

    PdfReview(summaryTotalElementValue, adOrgCMTList, listElementvalue) {
        let thisObject = this;
        try {
            let rowsPdf = [];

            let docPdf = new jsPDF();
            let printpreviewService = new PrintPreviewService();

            let adOrgCMT = adOrgCMTList[0];
            let labelDate = HelperService.ConvertPeriodeToLabel(thisObject.periode);
            let headerText = [thisObject.titleReport, adOrgCMT.ad_org_wx_name, labelDate]

            printpreviewService.HeaderReportFinancial(docPdf, headerText);
            let elementValueService = new ElementValueService();
            let treeSummary = elementValueService.CreateHierarchyElementValue(summaryTotalElementValue, 'value', 'parent_value', 'children');


            let elementAset = _.find(listElementvalue, function (element) { return element.transaction_code === "acct_aset"; });
            let elementKewajiban = _.find(listElementvalue, function (element) { return element.transaction_code === "acct_kewajiban"; });
            let elementEkuitas = _.find(listElementvalue, function (element) { return element.transaction_code === "acct_ekuitas"; });


            let objAsset = _.find(treeSummary, function (summary) { return summary.value === elementAset.value; });
            let rowsPdfAsset = printpreviewService.CreateHierarchyRowAkun(objAsset, [], 0);
            rowsPdf = rowsPdf.concat(rowsPdfAsset);
            rowsPdf.push(printpreviewService.CreateTotalSummaryRow(objAsset.amount, "TOTAL ASET"));


            let objLiability = _.find(treeSummary, function (summary) { return summary.value === elementKewajiban.value; });
            let rowsPdfLiability = printpreviewService.CreateHierarchyRowAkun(objLiability, [], 0);

            rowsPdf = rowsPdf.concat(rowsPdfLiability);
            rowsPdf.push(printpreviewService.CreateTotalSummaryRow(objLiability.amount, "TOTAL KEWAJIBAN"));

            let objEquity = _.find(treeSummary, function (summary) { return summary.value === elementEkuitas.value; });
            let rowsPdfEquity = printpreviewService.CreateHierarchyRowAkun(objEquity, [], 0);

            rowsPdf = rowsPdf.concat(rowsPdfEquity);
            rowsPdf.push(printpreviewService.CreateTotalSummaryRow(objEquity.amount, "TOTAL Ekuitas"));


            rowsPdf.push(printpreviewService.CreateTotalSummaryRow(objEquity.amount + objLiability.amount, "TOTAL Kewajiban + Modal"));

            let space = docPdf.lastAutoTable.finalY + 10;

            docPdf.autoTable({
                startY: space,
                head: [['Akun', { content: 'Rp', colspan: 2 }]],
                body: rowsPdf
            });

            printpreviewService.PrintPreviewReport(thisObject.idBodyReport, docPdf);
        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.PdfReview.name, error);
        }



    }

    async Html() {
        let thisObject = this;


        let html = `
    <div class="kt-portlet">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    `+ thisObject.titleReport + `
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                      
    
                    </div>
                </div>
            </div>
        </div>
        <div id="`+ thisObject.idBodyReport + `" class="kt-portlet__body kt-portlet__body--fit">
            <div class="kt-spinner kt-spinner--md kt-spinner--danger"></div>
        </div>
    </div>
        
        `;

        return html;


    }



    /**
     * 
     * @param {*} listAkun 
     * @param {*} mainAkun mainAkun atau parentAkun 
     * @param {*} labelTotalSummary 
     * return {total_amount: totalamount, row_html: rowhtml}
   
    CreateRowAkun(listAkun, mainAkun, labelTotalSummary) {

        let thisObject = this;
        let rowMainAkun = `
        <tr>
            <td colspan="3">`+ mainAkun.value + ' ' + mainAkun.name + `</td>    
        </tr>`;

        //to pdf

        let listRowHtml = '';
        let totalSummary = 0;
        _.forEach(listAkun, function (akun) {
            totalSummary += numeral(akun.amount).value();
            let amount = new Intl.NumberFormat().format(akun.amount);
            let row = `
            <tr>
                <td colspan="2">&nbsp;&nbsp;`+ akun.value + ' ' + akun.name + `</td>
                <td >`+ amount + `</td>
            </tr>            
            `;
            listRowHtml += row;
            thisObject.rowsPdf.push([{ content: '  ' + akun.value + ' ' + akun.name, colspan: 2 }, amount]);

        });

        let rowTotalSummary = this.CreateTotalSummaryRow(totalSummary, labelTotalSummary);
        let allRowHtml = rowMainAkun + listRowHtml + rowTotalSummary;
        let returnRow = { total_amount: totalSummary, row_html: allRowHtml };

        return returnRow;
    }

    CreateTotalSummaryRow(totalAmount, labelTotalSummary) {
        let thisObject = this;
        let formatTotalSummary = new Intl.NumberFormat().format(totalAmount);
        let rowTotalSummary = `
        <tr>
            <td></td>
            <td>`+ labelTotalSummary + `</td>
            <td>`+ formatTotalSummary + `</td>
        </tr>
        `;
        thisObject.rowsPdf.push([{ content: labelTotalSummary, colspan: 2, styles: { halign: 'center' } }, formatTotalSummary]);
        return rowTotalSummary;


    }
  */




}