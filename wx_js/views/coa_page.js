class CoaPage {

	constructor() {


	}





	Init() {

		try {

			let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery
			//	let productCategoryService = new ProductCategoryService();
			let metaDataResponseTableCategory = new MetaDataTable();
			metaDataResponseTableCategory.ad_org_uu = localStorage.getItem('ad_org_uu');

			let allHtml = `
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head kt-portlet__head--lg">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon">
						<i class="kt-font-brand flaticon2-line-chart"></i>
					</span>
					<h3 class="kt-portlet__head-title">
						Chart Of Accounts
					</h3>
				</div>
				<div class="kt-portlet__head-toolbar">
					<div class="kt-portlet__head-wrapper">
						<div class="kt-portlet__head-actions">
						</div>
					</div>
				</div>
			</div>
			<div id="coa" class="kt-portlet__body">
		
			</div>
		</div>
		`;
			/*
			[
			{
				"value": 10000000, "name": "AKTIVA", "text": "10000000 AKTIVA", "children": [
					{
						"value": 11000000, "name": "AKTIVA Lancar", "text": "11000000 AKTIVA Lancar", "children": [
							{ "value": 11110000, "name": "KAS DAN SETARA KAS", "text": "11110000 KAS DAN SETARA KAS", "children": [] }
						]
					},
					{ "value": 11110000, "name": "AKTIVA Lancar", "text": "11000000 AKTIVA Lancar" }
				]
			}
		]*/
			$('#wx_fragment_content').html(allHtml);




			let elementValueService = new ElementValueService();
			elementValueService.FindAll().then(function (flatlist) {
				let tree = elementValueService.CreateHierarchyElementValue(flatlist, "value",
					"parent_value", "children");

				$('#coa').jstree({
					"core": {
						"themes": {
							"responsive": false
						},
						"data": tree
					},
					"types": {
						"default": {
							"icon": "fa fa-folder"
						},
						"file": {
							"icon": "fa fa-file"
						}
					},
					"plugins": ["types"]
				});

				$('#coa').on("select_node.jstree", function (e, data) {
					$('#coa').jstree("toggle_node", data.node);
				});

			});


		} catch (error) {
			apiInterface.Log(this.constructor.name, this.Init.name, error);
		}


	}






}
