class FactAcctTransferBankaccountPortlet {

	constructor(idFragmentContent, posTransactionTablePortlet) {

		this.posTransactionCM = new PosTransactionComponentModel();
		this.posTransactionlineCashCM = new PosTransactionlineComponentModel();

		this.factAcctService = new FactAcctService();

		this.idFragmentContent = idFragmentContent;
		this.posTransactionTablePortlet = posTransactionTablePortlet;

		this.labelForm = 'Transfer Internal Akun Bank';


		this.idFormModal = 'transfer_internal_bank_form_wx_';
		this.idFormValidation = 'transfer_internal_bank_form_validation_';
		this.classNameFormControl = 'transfer_internal_bank_form-control';

		this.selectorClassNameFormControl = '.' + this.classNameFormControl;
		this.idButtonFormSubmit = 'transfer_internal_bank_form_submit_wx_';

		this.idBankaccountFrom = 'transfer_internal_bank_from_options';
		this.idBankaccountTo = 'transfer_internal_bank_to_options';
		this.idAmountTransfer = 'transfer_internal_bank_amount_transfer';
		this.idNotification = 'transfer_internal_bank_notification';
		this.idRefreshButton = 'transfer_internal_bank_refresh_form';
		this.validation = {
			id_form: this.idFormValidation,

		};
		this.validation[this.idAmountTransfer] = { required: true };
		this.validation[this.idBankaccountFrom] = { required: true };
		this.validation[this.idBankaccountTo] = { required: true };

		this.Init();

	}


	async	Init() {

		try {

			let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery

			let htmlForm = await thisObject.GeneratePortlet();

			$('#' + thisObject.idFragmentContent).html(htmlForm);

		
			thisObject.EventHandler();



		} catch (error) {
			apiInterface.Log(this.constructor.name, this.Init.name, error);
		}


	}



	async	GeneratePortlet() {
		let thisObject = this;

		let bankaccountFrom = new BankaccountOptions(thisObject.classNameFormControl, thisObject.idBankaccountFrom, null, 'Pilih Transfer dari Bank/Cash');
		let bankaccountFromHtml = await bankaccountFrom.Html();

		let bankaccountTo = new BankaccountOptions(thisObject.classNameFormControl, thisObject.idBankaccountTo, null, 'Pilih Transfer ke  Bank/Cash');
		let bankaccountToHtml = await bankaccountTo.Html();


		let portletHtml = `
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand flaticon2-line-chart"></i>
				</span>
				<h3 class="kt-portlet__head-title">
					`+ thisObject.labelForm + `
				</h3>
			</div>
			<div class="kt-portlet__head-toolbar">
				<div class="kt-portlet__head-wrapper">
					<div class="kt-portlet__head-actions">
					</div>
				</div>
			</div>
		</div>
		<form id="`+ thisObject.idFormValidation + `" class="kt-form">
			<div class="kt-portlet__body">`+ bankaccountFromHtml + bankaccountToHtml + `				
			
				<div class="form-group">
					<labelJumlah Transfer</label>
					<div class="input-group">
						<input id="`+ thisObject.idAmountTransfer + `" type="text"
							class="` + thisObject.classNameFormControl + ` wx-format-money  form-control"
							aria-describedby="emailHelp">
						
					</div>
				</div>
			</div>
			<div class="kt-portlet__foot">
				<div class="kt-form__actions">
					<button id="`+ thisObject.idButtonFormSubmit + `" type="submit" class="btn btn-primary">Transfer</button>
					<div id="`+thisObject.idNotification + `"></div>
				</div>
			</div>
		</form>
	</div>
		`;
		return portletHtml;

	}


	EventHandler() {
		let thisObject = this;

		try {
			$('#' + thisObject.idFormValidation).off('submit');
			$('#' + thisObject.idFormValidation).on('submit', function (e) {
				e.preventDefault();
				thisObject.Save();

			});
			$('#' + thisObject.idRefreshButton).off('click');
			$('#' + thisObject.idRefreshButton).on('click', function (e) {
				
				thisObject.Init();

			});
			$('.wx-format-money').mask('#,##0', { reverse: true });

		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.EventHandler.name, error);
		}

	}


	/**
		 *
		 * c_doctype_id =10002 --> transfer keluar
		 * c_doctype_id = 10001 --> transfer in
	
		 */
	async Save() {
		let thisObject = this;

		try {
			let bankstatementService = new BankstatementService();

			let insertSuccess = 0;
			if (HelperService.CheckValidation(thisObject.validation)) {

				let c_bankaccount_uu_from = $('#' + thisObject.idBankaccountFrom).val();
				let c_bankaccount_uu_to = $('#' + thisObject.idBankaccountTo).val();
				let amount_transfer = numeral($('#' + thisObject.idAmountTransfer).val()).value();

				let transfer_from_success = await bankstatementService.CreateBankStatement(c_bankaccount_uu_from,
					null, null, null, amount_transfer, 10002,'COMPLETED');
				let transfer_to_success = await bankstatementService.CreateBankStatement(c_bankaccount_uu_to,
					null, null, null, amount_transfer, 10001, 'COMPLETED');
				if ((transfer_from_success + transfer_to_success) > 0){
					let success = `
					<div class="alert alert-success" role="alert">
						<strong>Transfer sukses!</strong> Cek balance pada bank yang telah selesai ditransfer.
					</div>`;
					let bttnRefresh = `
					<button id="`+ thisObject.idRefreshButton + `" type="button" class="btn btn-primary">Refresh Form</button>
			
					
					`;
					$('#'+thisObject.idNotification).html(success);
					$('#'+thisObject.idButtonFormSubmit).replaceWith(bttnRefresh);
					thisObject.EventHandler();
				}
			}
		
		} catch (err) {
			apiInterface.Log(this.constructor.name, this.Save.name, err);
		}
	}



}
