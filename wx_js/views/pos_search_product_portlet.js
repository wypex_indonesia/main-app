/**
 * CartlinePortlet merupakan cartline portlet pada pos_page.
 * defaultCartCM akan selalu dibuat atau dicari apakah masih ada CartCM , jika tidak ada maka cartCM harus dibuat
 * 
 */
class SearchProductPortlet {

	constructor(idFragmentContent, pricelistVersionUu, toolbarSearchProductPortlet, cartlinePortlet) {


		this.idFragmentContent = idFragmentContent;
		this.posService = new PosService();

		this.cartDTRef = {}; // buat reference datatable


		this.toolbarSearchProductPortlet = toolbarSearchProductPortlet;
		this.metaDataTableSearch = new MetaDataTable();
		this.pricelistVersionUu = pricelistVersionUu;
		this.cartlinePortlet = cartlinePortlet;
		this.idSearchProductTable = 'pos_search_product_portlet_search_product_table';
		this.idCellProductPrice = 'post-search-product-cell-product-price';
	}

	async  Redraw(searchValue) {

		let thisObject = this;



		let htmlTable = `
		<div class="kt-portlet kt-portlet--mobile" >
				<div class="kt-portlet__head kt-portlet__head--lg">
					<div class="kt-portlet__head-label">
						<span class="kt-portlet__head-icon">
							<i class="kt-font-brand flaticon2-line-chart"></i>
						</span>
						<h3 class="kt-portlet__head-title">
						Daftar Produk
						</h3>
					</div>
					<div class="kt-portlet__head-toolbar">
						<div class="kt-portlet__head-wrapper">
							<div class="kt-portlet__head-actions">
							</div>
						</div>
					</div>
				</div>
				<div class="kt-portlet__body" >
				(ALT+F)
					<table class="table table-striped- table-bordered table-hover table-checkable" id="`+ thisObject.idSearchProductTable + `">
			
		
					</table>
			
			</div >
		</div >
			`;
		$('#' + thisObject.idFragmentContent).html(htmlTable);

		let table = $('#' + this.idSearchProductTable);


		let columnsSearchProductDT =
			[
				{ title: 'SKU/UPC' },
				{ title: 'Nama', data: 'm_product_wx_name' },
				{ title: 'Harga' }
			];

		let columnDefDatatable = [
			{
				targets: 0,
				title: 'SKU/UPC',
				orderable: false,
				render: function (data, type, full, meta) {

					let sku_upc = 'SKU :' + full.m_product_wx_sku;

					if (full.m_product_wx_upc) {
						sku_upc = '</br>UPC : ' + full.m_product_wx_upc;
					}
					return sku_upc;
				},
			},
			{
				targets: 2,
				title: 'Harga',
				orderable: false,
				render: function (data, type, full, meta) {

					return new Intl.NumberFormat().format(full.m_productprice_wx_pricestd);

				},
			},


		];
		thisObject.metaDataTableSearch.search = searchValue;
		thisObject.posService.FindSearchProductCMTList(thisObject.pricelistVersionUu, thisObject.metaDataTableSearch)
			.then(function (productCMTList) {

				thisObject.searchProductDTRef = table.DataTable({
					createdRow: function ( row, data, index ) {
						//angka 2 adalah index column
						if (index === 0){
							$('td', row).eq(2).attr('id', thisObject.idCellProductPrice);
						}
					
					 },
					paging: false,
					searching: false,
					responsive: true,
					data: productCMTList,
					scrollX: true,
					keys: true,
					columns: columnsSearchProductDT,
					columnDefs: columnDefDatatable,
					"order": [[1, 'asc']]

				});


				thisObject.searchProductDTRef.cell( '#'+thisObject.idCellProductPrice).focus();
				thisObject.EventHandler();

			});


	}

	ClearHtml() {
		$('#' + this.idFragmentContent).html('');
		
	}


	EventHandler() {
		let thisObject = this;

	
		
		// Add event listener for opening and closing details
		$('#' + thisObject.idSearchProductTable + ' tbody').off('click', 'tr');
		$('#' + thisObject.idSearchProductTable + ' tbody').on('click', 'tr', function () {
			//	var tr = $(this).closest('tr');
			var row = thisObject.searchProductDTRef.row(this);
			let productPriceCMT = row.data();
			thisObject.cartlinePortlet.AddCartline(productPriceCMT).then(function(inserted){
				thisObject.toolbarSearchProductPortlet.ClearValueAndFocus();
				thisObject.ClearHtml();
			})
			

		});

		hotkeys('alt+f', function (event, handler){

			if (thisObject.searchProductDTRef){

				if (thisObject.searchProductDTRef.cell( '#'+thisObject.idCellProductPrice)){

					thisObject.searchProductDTRef.cell( '#'+thisObject.idCellProductPrice).focus();

				}
			}
			
			event.preventDefault();
			return false;

		});
		
	}











}