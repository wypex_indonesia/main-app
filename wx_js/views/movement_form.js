class MovementForm {

	/**
	 * 
	
	 * @param {string} idFragmentContent name id fragment content untuk meletakkan html content dari form_html
	 * @param {MovementComponentModel}
	 * @param {datatableReference} datatableReference data tabel pada inout page, sebagai reference untuk mengupdate table tersebut
	 * @param {c_doctype} inouttype MMS (MM Shipment), MMR (MM Receipt)
	 */
	constructor(movementCM, datatableReference) {

		this.titleForm = 'Mutasi Barang';

		this.componentModel = movementCM;


		this.c_doctype_id = 8000;

		this.isWarehouseFrom = true; // buat flag apakah button search yang diclick adalah warehouseFrom atau warehouseTO
		this.metaDataTable = new MetaDataTable();

		this.datatableReference = datatableReference; // buat reference datatable

		this.movementService = new MovementService();

		this.warehouseService = new WarehouseService();
		this.bankaccountService = new BankaccountService();
	
		this.idFormModal = 'm_movement_form_wx_';
		this.idFormValidation = 'm_movement_form_validation_';
		this.classNameFormControl = 'm_movement-form-control';
		this.selectorClassNameFormControl = '.' + this.classNameFormControl;
		
		this.idWidgetSearch = 'movement_form_warehouse_search_widget';
	
		this.idButtonFormSubmit = 'm_movement_form_submit_wx_';
	

		this.prefixContent = 'movementline_page'; // prefix content untuk child page 



		this.validation = {

			id_form: this.idFormValidation,


			m_warehouse_from_wx_name: {
				required: true
			},

			m_warehouse_to_wx_name: {
				required: true
			}



		};




		this.Init();


	

	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery



		let allHtml = thisObject.GenerateFormInput();
		HelperService.InsertReplaceFormModal(thisObject.idFormModal, allHtml);
		
		// jika bukan new record, ambil data-data dibawah untuk select options

		HelperService.InjectViewModelToForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.componentModel);
		thisObject.EventHandler();
	}




	/**
	 * 
	
	 */
	async Save() {
		let thisObject = this;

		let insertSuccess = 0;
		if (HelperService.CheckValidation(thisObject.validation)) {

			thisObject.componentModel = HelperService.GetViewModelFromForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.componentModel);
			thisObject.componentModel.c_doctype.c_doctype_id = thisObject.c_doctype_id;

			insertSuccess = await thisObject.movementService.SaveUpdate(thisObject.componentModel, thisObject.datatableReference);
			// jika sukses tersimpan 
			if (insertSuccess > 0) {

				$('#' + thisObject.idFormModal).modal('hide');


			}

		}


	}

	GenerateFormInput() {
		let thisObject = this;


		return `<div class="modal fade" id="` + thisObject.idFormModal + `" tabindex="-1" role="dialog" aria-labelledby="product-form-labelled"
		style="display: none;" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="product-form-labelled">`+ thisObject.titleForm + `</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<form id="`+ thisObject.idFormValidation + `" class="kt-form">
				<div class="modal-body">
					
					<div class="form-group">
						<label>No Dokumen</label>
						<input disabled="disabled" id="m_movement_wx_documentno" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
						<span class="form-text text-muted">No Dokumen dibuat otomatis</span>
					</div>
				

					<div class="form-group form-group-marginless">
					<label>Dari Gudang / Toko</label>
					<div class="input-group">
						<input disabled="disabled" type="text" class="` + thisObject.classNameFormControl + ` form-control"  id="m_warehouse_from_wx_name" placeholder="Cari Gudang / Toko ...">
						<div class="input-group-append">
						<span id="warehouse_from" class="search_m_warehouse input-group-text"><i  class="la la-search"></i></span>
						</div>
						<input type="hidden" class="` + thisObject.classNameFormControl + `" id="m_warehouse_from_wx_m_warehouse_uu">
					</div>
				</div>		
				<div class="form-group form-group-marginless">
					<label>Ke Gudang / Toko</label>
					<div class="input-group">
						<input disabled="disabled" type="text" class="` + thisObject.classNameFormControl + ` form-control"  id="m_warehouse_to_wx_name" placeholder="Cari Gudang / Toko ...">
						<div class="input-group-append">
						<span id="warehouse_to"  class="search_m_warehouse input-group-text"><i  class="la la-search"></i></span>
						</div>
						<input type="hidden" class="` + thisObject.classNameFormControl + `" id="m_warehouse_to_wx_m_warehouse_uu">
					</div>
				</div>			
					<div class="form-group">
					<label>Deskripsi</label>
					<input  id="m_movement_wx_description" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
					<span class="form-text text-muted">Deskripsi</span>
				</div>		
				<div class="form-group ">
							<label>Tanggal Mutasi</label>
							<input type="text" class="` + thisObject.classNameFormControl + ` datepicker form-control" id="m_movement_wx_movementdate" readonly="" placeholder="Select date">
						</div>		
					<div class="form-group">
						<label>Dokumen Status</label>
						<input disabled="disabled" id="m_movement_wx_docstatus" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
						<span class="form-text text-muted">Status Dokumen DRAFT / COMPLETED / REVERSED</span>
					</div>
					<div class="form-group" id="`+thisObject.idWidgetSearch+`">
							
	
					</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button id="`+ thisObject.idButtonFormSubmit + `" type="button" class="btn btn-primary">Save changes</button>
					</div>
			
				</form>
			</div>
		</div>
	</div>`;
	}


	EventHandler() {
		let thisObject = this;

		$('#' + thisObject.idButtonFormSubmit).on('click', function () {
			thisObject.Save();

		});


		/** BEGIN EVENT SEARCH M_Warehouse **************************************/

		$('#' + thisObject.idFormValidation + ' .search_m_warehouse').on('click', function (e) {

			if (e.currentTarget.id === 'warehouse_from') {

				let functionGetResult = function(warehouseSelected){
					$('#' + thisObject.idFormValidation+ ' #m_warehouse_from_wx_name').val(warehouseSelected.m_warehouse_wx_name);
					$('#' + thisObject.idFormValidation+ ' #m_warehouse_from_wx_m_warehouse_uu').val(warehouseSelected.m_warehouse_wx_m_warehouse_uu);
				
				};
				let warehouse_search_widget = new WarehouseSearchWidget(thisObject.idWidgetSearch,functionGetResult );
				warehouse_search_widget.Init();
				thisObject.isWarehouseFrom = true;
			}

			if (e.currentTarget.id === 'warehouse_to') {
				let functionGetResult = function(warehouseSelected){
					$('#' + thisObject.idFormValidation+ ' #m_warehouse_to_wx_name').val(warehouseSelected.m_warehouse_wx_name);
					$('#' + thisObject.idFormValidation+ ' #m_warehouse_to_wx_m_warehouse_uu').val(warehouseSelected.m_warehouse_wx_m_warehouse_uu);
				
				};
				let warehouse_search_widget = new WarehouseSearchWidget(thisObject.idWidgetSearch,functionGetResult );
				warehouse_search_widget.Init();
				thisObject.isWarehouseFrom = false;
			}

			
		});



		/** END EVENT SEARCH M_Warehouse **************************************/



		$('#' + thisObject.idFormModal).modal('show');


	}



}
