class UomForm {

	/**
	 * 
	
	
	 * @param {UomComponentModel} uomCM
	 * @param {datatableReference} datatableReference data tabel pada role page, sebagai reference untuk mengupdate table tersebut	 
	 */
	constructor(uomCM, datatableReference) {

		this.titleForm = 'Add/Edit Satuan';

		this.viewModel = uomCM;


		this.metaDataTable = new MetaDataTable();

		this.datatableReference = datatableReference; // buat reference datatable

		this.uomService = new UomService();


		this.idFormModal = 'c_uom_form_wx_';
		this.idFormValidation = 'c_uom_form_validation_';
		this.classNameFormControl = 'c_uom-form-control';
		this.selectorClassNameFormControl = '.' + this.classNameFormControl;


		this.idButtonFormSubmit = 'c_uom_form_submit_wx_';


		this.validation = {

			id_form: this.idFormValidation,

			c_uom_wx_name: {
				required: true
			},
			c_uom_wx_uomsymbol: {
				required: true
			}

		};




		this.Init();


	

	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery

		try {

			HelperService.InsertReplaceFormModal(thisObject.idFormModal, thisObject.GenerateFormInput());

			HelperService.InjectViewModelToForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.viewModel);
			thisObject.PageEventHandler();
		} catch (error) {
			apiInterface.Log(this.constructor.name, this.Init.name, error);
		}


	}




	/**
	 * 
	
	 */
	async Save() {
		let thisObject = this;

		try {

			let insertSuccess = 0;
			if (HelperService.CheckValidation(thisObject.validation)) {

				thisObject.viewModel = HelperService.GetViewModelFromForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.viewModel);

				insertSuccess = await thisObject.uomService.SaveUpdate(thisObject.viewModel, thisObject.datatableReference);
				// jika sukses tersimpan 
				if (insertSuccess > 0) {

					$('#' + thisObject.idFormModal).modal('hide');


				}

			}
		} catch (err) {
			apiInterface.Log(this.constructor.name, this.Save.name, err);
		}



	}

	GenerateFormInput() {
		let thisObject = this;

		let html = '';
		try {
			html = `
<div class="modal fade" id="` + thisObject.idFormModal + `" tabindex="-1" role="dialog" aria-labelledby="uom-form-labelled"
    style="display: none;" aria-hidden="true">

    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="uom-form-labelled">`+ thisObject.titleForm + `</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form id="`+ thisObject.idFormValidation + `" class="kt-form">

                    <div class="form-group">
                        <label>Satuan</label>
                        <input id="c_uom_wx_name" type="text" class="` + thisObject.classNameFormControl + ` uom-form-control form-control"
                            aria-describedby="emailHelp" placeholder="Masukkan nama satuan">
                        <span class="form-text text-muted">Masukkan nama satuan.</span>
                    </div>
                    <div class="form-group">
                        <label>Simbol</label>
                        <input id="c_uom_wx_uomsymbol" type="text" value="" class="` + thisObject.classNameFormControl + ` uom-form-control form-control"
                            aria-describedby="emailHelp" placeholder="Masukkan simbol satuan">
                        <span class="form-text text-muted">Masukkan simbol satuan.</span>
                    </div>
              
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button id='uom-form-submit' type="submit" class="btn btn-primary">Save changes</button>
					</div>	
				</form>
            </div>
        </div>
    </div>
</div>
`;
			
		} catch (error) {
			apiInterface.Log(this.constructor.name, this.GenerateFormInput.name, error);
		}

		return html;
	}


	PageEventHandler() {
		let thisObject = this;


		try {

			$('#' + thisObject.idFormValidation).off('submit');
			$('#' + thisObject.idFormValidation).on('submit', function (e) {
				thisObject.Save();
				e.preventDefault();
				

			});



			$('#' + thisObject.idFormModal).modal('show');


		} catch (err) {
			apiInterface.Log(this.constructor.name, this.PageEventHandler.name, err);
		}


	}



}
