class ExpenseForm {

	/**
	 * 

	 * @param {ExpenseComponentModel}
	 * @param {datatableReference} datatableReference data tabel pada inout page, sebagai reference untuk mengupdate table tersebut
	 
	 */
	constructor(expenseComponentModel, datatableReference) {


		this.componentModel = expenseComponentModel;



		this.metaDataTable = new MetaDataTable();

		this.datatableReference = datatableReference; // buat reference datatable

		this.paymentService = new PaymentService();

		this.bankaccountService = new BankaccountService();
		this.chargeService = new ChargeService();


		this.idFormModal = 'c_payment_form_wx_';
		this.idFormValidation = 'c_payment_form_validation_';
		this.classNameFormControl = 'c-payment-form-control';
		this.selectorClassNameFormControl = '.' + this.classNameFormControl;

		this.idNewRecord = 'new_record_inout_wx_';
		this.idButtonFormSubmit = 'c_payment_form_submit_wx_';


		this.c_doctype_id = 5002;

		this.titleForm = 'Pembayaran Pengeluaran';


		this.validation = {

			id_form: this.idFormValidation,

			c_bankaccount_wx_c_bankaccount_uu: {
				required: true
			},
			c_charge_wx_c_charge_uu: {
				required: true
			},



		};




		this.Init();

	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery

		thisObject.GenerateFormInput();

	}




	/**
	 * 
	
	 */
	Save() {
		let thisObject = this;

		let insertSuccess = 0;
		if (HelperService.CheckValidation(thisObject.validation)) {

			thisObject.componentModel = HelperService.GetViewModelFromForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.componentModel);
			thisObject.componentModel.c_doctype.c_doctype_id = thisObject.c_doctype_id;

			thisObject.paymentService.SaveUpdateExpenseCM(thisObject.componentModel, thisObject.datatableReference)
				.then(function (rowChanged) {
					insertSuccess += rowChanged;
					// jika sukses tersimpan 
					if (insertSuccess > 0) {

						$('#' + thisObject.idFormModal).modal('hide');
					}

				});


		}


	}

	async GenerateFormInput() {
		let thisObject = this;
		try {

			let bankaccountOptions = new BankaccountOptions(thisObject.classNameFormControl);
			let bankaccountOptionsHtml = await bankaccountOptions.Html();

			let chargeOptions = new ChargeOptions(thisObject.classNameFormControl);
			let chargeOptionsHtml = await chargeOptions.Html();


			let allHtml = `
				<div class="modal fade" id="` + thisObject.idFormModal + `" tabindex="-1" role="dialog"
					aria-labelledby="product-form-labelled" style="display: none;" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="product-form-labelled">`+ thisObject.titleForm + `</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								</button>
							</div>
							<form id="`+ thisObject.idFormValidation + `" class="kt-form">
								<div class="modal-body">
				
									<div class="form-group">
										<label>No Dokumen</label>
										<input disabled="disabled" id="c_payment_wx_documentno" type="text"
											class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp">
										<span class="form-text text-muted">No Dokumen dibuat otomatis</span>
									</div>` + chargeOptionsHtml + bankaccountOptionsHtml+`
									<div class="form-group">
										<label>Jumlah Pengeluaran</label>
										<input id="c_payment_wx_payamt" type="text"
											class="` + thisObject.classNameFormControl + ` wx-format-money form-control"
											aria-describedby="emailHelp">
									</div>
									<div class="form-group">
										<label>Deskripsi</label>
										<input id="c_payment_wx_description" type="text"
											class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp">
										<span class="form-text text-muted">Deskripsi</span>
									</div>
									<div class="form-group">
										<label>Dokumen Status</label>
										<input disabled="disabled" id="c_payment_wx_docstatus" type="text"
											class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp">
										<span class="form-text text-muted">Status Dokumen DRAFT / COMPLETED / REVERSED</span>
									</div>
				
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
									<button id="`+ thisObject.idButtonFormSubmit + `" type="submit" class="btn btn-primary">Save
										changes</button>
								</div>
				
							</form>
						</div>
					</div>
				</div>
					`;



			HelperService.InsertReplaceFormModal(thisObject.idFormModal, allHtml);
			HelperService.InjectViewModelToForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.componentModel);


			thisObject.EventHandler();


		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.GenerateFormInput.name, error);
		}



	}


	EventHandler() {
		let thisObject = this;
		$('#' + thisObject.idButtonFormSubmit).off('click');
		$('#' + thisObject.idButtonFormSubmit).on('click', function () {
			thisObject.Save();

		});




		$('#' + thisObject.idFormModal).modal('show');

		$('.wx-format-money').mask('#,##0', { reverse: true });


	}



}
