class BusinesspartnerPage {

	constructor() {
		this.validation = {

			c_bpartner_wx_name: {
				required: true
			}

		};
		this.viewModel = new BusinesspartnerViewModel();

		this.metaDataTable = new MetaDataTable();
		this.metaDataTable.ad_org_uu = localStorage.getItem('ad_org_uu');

		this.datatableReference = {}; // buat reference datatable
		this.businessPartnerService = new BusinessPartnerService();
		this.classShowButtonForm = 'businesspartner_page_bttn_show_form';
		this.classDeleteRow = 'businesspartner_page_delete_row';

	}



	Init() {
		try {
			let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery
			let allHtml = `
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head kt-portlet__head--lg">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon">
						<i class="kt-font-brand flaticon2-line-chart"></i>
					</span>
					<h3 class="kt-portlet__head-title">
						Bisnis Partner
					</h3>
				</div>
				<div class="kt-portlet__head-toolbar">
					<div class="kt-portlet__head-wrapper">
						<div class="kt-portlet__head-actions">
							<button type="button"
								class="`+ thisObject.classShowButtonForm + ` btn btn-bold btn-label-brand btn-sm"
								name="new_record" data-toggle="modal" data-target="#uom-form">
								<i class="la la-plus"></i>New Record</button>
						</div>
					</div>
				</div>
			</div>
			<div class="kt-portlet__body">
				<table class="table table-striped- table-bordered table-hover table-checkable" id="wx_businesspartner_table">
		
					</thead>
				</table>
			</div>
		</div>	
					
		`;

			this.metaDataTable.ad_org_uu = localStorage.getItem('ad_org_uu');

			$('#wx_fragment_content').html(allHtml);


			var table = $('#wx_businesspartner_table');

			thisObject.businessPartnerService.FindAll(thisObject.metaDataTable).then(function (businesspartnerCMTList) {
				// begin first table
				thisObject.datatableReference = table.DataTable({
					responsive: true,
					data: businesspartnerCMTList,
					scrollX: true,
					columns: [
						{ title: 'Nama', data: 'c_bpartner_wx_name' },
						{ title: 'HP', data: 'c_bpartner_location_wx_phone' },
						{ title: 'Alamat', data: 'c_location_wx_address1' },
						{ title: 'Kota', data: 'c_location_wx_city' },
						{ title: 'Propinsi', data: 'c_location_wx_regionname' },
						{ title: 'Kodepos', data: 'c_location_wx_postal' },
						{ title: 'Action' }
					],
					columnDefs: [
						{
							targets: -1,
							title: 'Actions',
							orderable: false,
							render: function (data, type, full, meta) {
								return `
			<button id="bttn_row_edit`+ full.c_bpartner_wx_c_bpartner_uu + `" name="` + full.c_bpartner_wx_c_bpartner_uu + `" type="button" class="` + thisObject.classShowButtonForm + ` btn btn-outline-hover-info btn-elevate btn-icon"><i class="la la-edit"></i></button>
			<button id="bttn_row_delete`+ full.c_bpartner_wx_c_bpartner_uu + `" value="` + full.c_bpartner_wx_name + `" name="` + full.c_bpartner_wx_c_bpartner_uu + `" type="button"  class="` + thisObject.classDeleteRow + ` btn btn-outline-hover-info btn-elevate btn-icon"> <i class="la la-trash"></i></button>
			`;
							},
						},
					],

				});
				thisObject.EventHandler();

			});



		} catch (error) {
			apiInterface.Log(this.constructor.name, this.Init.name, error);
		}

	}

	EventHandler() {
		let thisObject = this;
		$('.' + thisObject.classDeleteRow).off('click');
		$('.' + thisObject.classDeleteRow).on('click', function () {
			let nameDeleted = $(this).val();
			let uuid = $(this).attr('name');
			let deleteWidget = new DialogBoxDeleteWidget('Hapus Data', nameDeleted, uuid, thisObject);

		});
		$('.' + thisObject.classShowButtonForm).off('click');
		$('.' + thisObject.classShowButtonForm).on('click', function (e) {

			try {
				let attrName = $(this).attr('name');

				if (attrName !== 'new_record') {

					let dataEdited = thisObject.datatableReference.row($('#bttn_row_edit' + attrName).parents('tr')).data();


					thisObject.componentModel = HelperService.ConvertRowDataToViewModel(dataEdited);

				} else {

					thisObject.componentModel = new BusinesspartnerViewModel();


				}
				let businesspartnerForm = new BusinesspartnerForm(thisObject.componentModel, thisObject.datatableReference);

			} catch (error) {
				apiInterface.Log(thisObject.constructor.name, thisObject.EventHandler.name, error)
			}

		});
		thisObject.datatableReference.off('draw');
		thisObject.datatableReference.on('draw', function () {

			thisObject.EventHandler();
		});




	}

	/**
	 * 

	SaveUpdate() {
		let thisObject = this;

		try {

			if (HelperService.CheckValidation(thisObject.validation)) {
				$(".businesspartner-form-control").each(function () {
					let id = $(this).attr('id');
					let arrayId = id.split("_wx_");
					thisObject.viewModel[arrayId[0]][arrayId[1]] = $(this).val();	// masukkan value

				});


				const timeNow = new Date().getTime();
				// jika new record maka buat mlocator 	
				// harus dibuat default locator, karena locator akan dipergunakan di inventory movement
				let isNewRecord = false;
				if (!thisObject.viewModel.c_bpartner.c_bpartner_uu) { //new record
					isNewRecord = true;
					thisObject.viewModel.c_bpartner.c_bpartner_uu = HelperService.UUID();
					thisObject.viewModel.c_bpartner.ad_client_uu = localStorage.getItem('ad_client_uu');
					thisObject.viewModel.c_bpartner.ad_org_uu = localStorage.getItem('ad_org_uu');

					thisObject.viewModel.c_bpartner.createdby = localStorage.getItem('ad_user_uu');
					thisObject.viewModel.c_bpartner.updatedby = localStorage.getItem('ad_user_uu');

					thisObject.viewModel.c_bpartner.created = timeNow;
					thisObject.viewModel.c_bpartner.updated = timeNow;
				} else { //update record


					thisObject.viewModel.c_bpartner.updatedby = localStorage.getItem('ad_user_uu');

					thisObject.viewModel.c_bpartner.updated = timeNow;
				}

				thisObject.viewModel.c_bpartner.sync_client = null;
				thisObject.viewModel.c_bpartner.process_date = null;

				if (!thisObject.viewModel.c_location.c_location_uu) {
					thisObject.viewModel.c_location.c_location_uu = HelperService.UUID();
					thisObject.viewModel.c_location.ad_client_uu = localStorage.getItem('ad_client_uu');
					thisObject.viewModel.c_location.ad_org_uu = localStorage.getItem('ad_org_uu');

					thisObject.viewModel.c_location.createdby = localStorage.getItem('ad_user_uu');
					thisObject.viewModel.c_location.updatedby = localStorage.getItem('ad_user_uu');

					thisObject.viewModel.c_location.created = timeNow;
					thisObject.viewModel.c_location.updated = timeNow;
				} else {


					thisObject.viewModel.c_location.updatedby = localStorage.getItem('ad_user_uu');

					thisObject.viewModel.c_location.updated = timeNow;
				}

				thisObject.viewModel.c_location.sync_client = null;
				thisObject.viewModel.c_location.process_date = null;

				if (!thisObject.viewModel.c_bpartner_location.c_bpartner_location_uu) {
					thisObject.viewModel.c_bpartner_location.c_bpartner_location_uu = HelperService.UUID();
					thisObject.viewModel.c_bpartner_location.ad_client_uu = localStorage.getItem('ad_client_uu');
					thisObject.viewModel.c_bpartner_location.ad_org_uu = localStorage.getItem('ad_org_uu');

					thisObject.viewModel.c_bpartner_location.createdby = localStorage.getItem('ad_user_uu');
					thisObject.viewModel.c_bpartner_location.updatedby = localStorage.getItem('ad_user_uu');

					thisObject.viewModel.c_bpartner_location.created = timeNow;
					thisObject.viewModel.c_bpartner_location.updated = timeNow;
				} else {


					thisObject.viewModel.c_bpartner_location.updatedby = localStorage.getItem('ad_user_uu');

					thisObject.viewModel.c_bpartner_location.updated = timeNow;
				}

				if (thisObject.viewModel.c_bpartner.iscustomer === '') {
					thisObject.viewModel.c_bpartner.iscustomer = 'Y';
				}

				if (thisObject.viewModel.c_bpartner.isvendor === '') {
					thisObject.viewModel.c_bpartner.isvendor = 'Y';
				}

				thisObject.viewModel.c_bpartner_location.c_bpartner_uu = thisObject.viewModel.c_bpartner.c_bpartner_uu;
				thisObject.viewModel.c_bpartner_location.c_location_uu = thisObject.viewModel.c_location.c_location_uu;


				thisObject.viewModel.c_bpartner_location.phone = HelperService.ConvertPhoneWithAreaCode(thisObject.viewModel.c_bpartner_location.phone, '62');
				thisObject.viewModel.c_bpartner_location.name = thisObject.viewModel.c_bpartner.name;
				thisObject.viewModel.c_bpartner_location.sync_client = null;
				thisObject.viewModel.c_bpartner_location.process_date = null;
				let insertSuccess = 0;
				const sqlInsertBusinesspartner = HelperService.SqlInsertOrReplaceStatement('c_bpartner', thisObject.viewModel.c_bpartner);

				insertSuccess += apiInterface.ExecuteSqlStatement(sqlInsertBusinesspartner);

				const sqlInsertBusinesspartnerlocation = HelperService.SqlInsertOrReplaceStatement('c_bpartner_location', thisObject.viewModel.c_bpartner_location);

				insertSuccess += apiInterface.ExecuteSqlStatement(sqlInsertBusinesspartnerlocation);

				const sqlInsertLocation = HelperService.SqlInsertOrReplaceStatement('c_location', thisObject.viewModel.c_location);
				insertSuccess += apiInterface.ExecuteSqlStatement(sqlInsertLocation);

				// jika sukses tersimpan 
				if (insertSuccess > 0) {

					let rowData = HelperService.ConvertViewModelToRow(thisObject.viewModel);

					if (!isNewRecord) {

						thisObject.datatableReference.row($('#bttn_row_edit' + rowData['c_bpartner_wx_c_bpartner_uu']).parents('tr')).data(rowData).draw();
					} else {

						thisObject.datatableReference.row.add(rowData).draw();
					}


				}
				$('#businesspartner-form').modal('hide');

			}
		} catch (error) {
			apiInterface.Log(this.constructor.name, this.SaveUpdate.name, error);
		}





	}

	 */
	/**
	 * Delete pada table, sebenarnya hanya menandai wx_isdeleted = 'Y'
	 * @param {*} dataObject dataObject pada datatable 
	 */
	async Delete(uuidDeleted) {
		let insertSuccess = 0;
		let thisObject = this;
		try {
			let  businesspartnerCMTList = await thisObject.businessPartnerService.FindBusinesspartnerViewModelByCBpartnerUu(uuidDeleted);
			if (businesspartnerCMTList.length){
				let businesspartnerCMT = businesspartnerCMTList[0];
				let sqlstatementDeleteBusinesspartner = HelperService.SqlDeleteStatement('c_bpartner', 'c_bpartner_uu', businesspartnerCMT.c_bpartner_wx_c_bpartner_uu);
				insertSuccess += await apiInterface.ExecuteSqlStatement(sqlstatementDeleteBusinesspartner);
				let sqlstatementDeleteC_location = HelperService.SqlDeleteStatement('c_location', 'c_location_uu', businesspartnerCMT.c_bpartner_wx_c_location_uu);
				insertSuccess += await apiInterface.ExecuteSqlStatement(sqlstatementDeleteC_location);
				let sqlstatementDeleteBusinesspartnerLocation = HelperService.SqlDeleteStatement('c_bpartner_location', 'c_bpartner_uu', businesspartnerCMT.c_bpartner_wx_c_bpartner_uu);
				insertSuccess += await apiInterface.ExecuteSqlStatement(sqlstatementDeleteBusinesspartnerLocation);
				if (insertSuccess) {
					// delete row pada datatable
					thisObject.datatableReference
						.row($('#bttn_row_delete' + uuidDeleted).parents('tr'))
						.remove()
						.draw();
					$('#modal_info_box').modal('hide');
	
				}
			}
			
		} catch (error) {
			apiInterface.Log(this.constructor.name, this.Delete.name, error);
		}


		return insertSuccess;
	}

}
