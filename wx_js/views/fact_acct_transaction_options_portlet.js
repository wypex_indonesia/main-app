class FactAcctTransactionOptionsPortlet {

	/**
	 * 
	 * @param {*} idFragmentContent fragment content untuk FactAcctTransactionOptionsPortlet (this Portlet)
	 * @param {*} factacctTablePortlet 
	 * @param {*} idFragmentFactTransactionPortlet fragment content untuk idFragmentFactTransactionPortlet (untuk portlet yang dipilih)
	 */
	constructor(idFragmentContent, factacctTablePortlet, idFragmentFactTransactionPortlet) {

		this.idFragmentContent = idFragmentContent;
		this.factacctTablePortlet = factacctTablePortlet;
		this.idFragmentFactTransactionPortlet = idFragmentFactTransactionPortlet;
		this.idTransactionTypesOptions = 'transaction_types_options';
		this.idFormValidation = 'fact_acct_transaction_options_portlet_form_validation';
		this.idButtonFormSubmit = 'fact_acc_transaction_options_portlet_button_form_submit';
		this.classNameControl = 'fact_acc_transaction_options_portlet_class_name_control';
		this.idTransactionTypesOptions = 'fact_acc_transaction_options_portlet_types_options';
		this.Init();

	}


	async	Init() {

		try {

			let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery

			let htmlForm = await thisObject.GeneratePortlet();

			$('#' + thisObject.idFragmentContent).html(htmlForm);

			thisObject.CreatePortletByTransactionTypes($('#' + thisObject.idTransactionTypesOptions).val());
			thisObject.EventHandler();



		} catch (error) {
			apiInterface.Log(this.constructor.name, this.Init.name, error);
		}


	}



	async	GeneratePortlet() {
		let thisObject = this;
		let transactionTypesOptions = new TransactionTypesOptions(thisObject.classNameControl, thisObject.idTransactionTypesOptions, 'Pilih Jenis Transaksi');

		let transactionTypesOptionsHtml = await transactionTypesOptions.Html();
		let portletHtml = `
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand flaticon2-line-chart"></i>
				</span>
				<h3 class="kt-portlet__head-title">
					Jenis Transaksi
				</h3>
			</div>
			<div class="kt-portlet__head-toolbar">
				<div class="kt-portlet__head-wrapper">
					<div class="kt-portlet__head-actions">
					</div>
				</div>
			</div>
		</div>
		<form id="`+ thisObject.idFormValidation + `" class="kt-form">
			<div class="kt-portlet__body">
			`+ transactionTypesOptionsHtml + `
			</div>			
		</form>
	</div>
		`;
		return portletHtml;

	}


	EventHandler() {
		let thisObject = this;

		try {


			$('#' + thisObject.idTransactionTypesOptions).off('change');
			$('#' + thisObject.idTransactionTypesOptions).on('change', function () {

				let transactionTypes = $(this).val();
				thisObject.CreatePortletByTransactionTypes(transactionTypes);
			});



		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.EventHandler.name, error);
		}

	}

	CreatePortletByTransactionTypes(transaction_types) {
		let thisObject = this;
		switch (transaction_types) {

			case 'transfer_inter_bankaccount':
				let factAcctTransferBankaccountPortlet = new FactAcctTransferBankaccountPortlet(thisObject.idFragmentFactTransactionPortlet
					, thisObject.factacctTablePortlet);
				break;
			case 'manual_journal':
				let fact_acct_manual_journal_portlet = new FactAcctManualJournalPortlet(thisObject.idFragmentFactTransactionPortlet
					, thisObject.factacctTablePortlet);
				break;
			case 'modal_transaction':
				let fact_acct_modal_transactions_portlet = new FactAcctModalTransactionsPortlet(thisObject.idFragmentFactTransactionPortlet
					, thisObject.factacctTablePortlet);
				break;


		}


	}




}
