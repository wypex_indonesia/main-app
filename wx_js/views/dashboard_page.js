class DashboardPage {

    constructor(idFragmentContent) {
        this.idFragmentContent = idFragmentContent;

    }

    async Init() {

        let thisObject = this;

        let templateRowHtml = await thisObject.TemplateRow();

        $('#' + thisObject.idFragmentContent).html(templateRowHtml);

    }

    async  TemplateRow() {

        let stockMinimumWidget = new StockMinimumWidget();
        let salesDailyWidget = new SalesDailyWidget();
        let outstandingPOWidget = new OutstandingPOWidget();
        let outstandingSOWidget = new OutstandingSOWidget();
        let bestSellerWidget = new BestSellerWidget();

        let stockMinimumContent = await stockMinimumWidget.Html();
        let bestSellerContent = await bestSellerWidget.Html();
        let template1 = `
        <div class="row">`+
            bestSellerContent +
            stockMinimumContent +

            `</div>`;

        /* let templateRow2 = `
         <div class="row">`+
         salesDailyWidget.Html() +    
            
             outstandingPOWidget.Html()+
             outstandingSOWidget.Html()+
         `</div>`
         ;*/

        //return template1 + templateRow2;
        return template1;
    }


}