class InvoiceForm {

	/**
	 * 
	
	 * @param {string} idFragmentContent name id fragment content untuk meletakkan html content dari form_html
	 * @param {InvoiceComponentModel}
	 * @param {datatableReference} datatableReference data tabel pada inout page, sebagai reference untuk mengupdate table tersebut
	 * @param {c_doctype} inouttype MMS (MM Shipment), MMR (MM Receipt)
	 */
	constructor(invoiceComponentModel, datatableReference, invoiceType) {

		this.titleForm = '';
		this.invoiceType = invoiceType;
		this.componentModel = invoiceComponentModel;

		this.labelNoDocument = '';
		this.labelVendorOrCustomer = '';

		this.c_doctype_id = 0;
		this.c_order_wx_c_doctype_id = 0;
		//this.labelPoReference = 'No PO Vendor';// No PO Vendor
		this.invoiceReferenceHtml = '';
		this.labelNoDocument = 'No Dokumen';
		this.labelNoOrder = '';


		this.metaDataTable = new MetaDataTable();

		this.datatableReference = datatableReference; // buat reference datatable

		this.orderService = new OrderService();
		this.inoutService = new InoutService();
		this.invoiceService = new InvoiceService();


		this.idFormModal = 'c_invoice_form_wx_';
		this.idFormValidation = 'c_invoice_form_validation_';
		this.classNameFormControl = 'c-invoice-form-control';
		this.selectorClassNameFormControl = '.' + this.classNameFormControl;
		
		this.idWidgetSearch = 'order_form_input_search';
		this.idNewRecord = 'new_record_inout_wx_';
		this.idButtonFormSubmit = 'c_invoice_form_submit_wx_';
		
	
		if (invoiceType === 'API') {

			this.c_doctype_id = 5000;
			this.c_order_wx_c_doctype_id = 1001; // pembelian dengan PO
			this.titleForm = 'Invoice Vendor';

			this.labelVendorOrCustomer = 'Vendor';
			this.labelNoOrder = 'No Purchase Order';
			//	this.labelNoInout = 'No Material Receipt';
			this.invoiceReferenceHtml = `<div class="form-group">
	<label>No Invoice Dari Vendor</label>
	<input id="c_invoice_wx_invoicereference" type="text"   class="` + this.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
	<span class="form-text text-muted">nomor dokumen invoice dari vendor</span>
</div>`;
		}

		if (invoiceType === 'ARI') {

			this.c_doctype_id = 6000;
			this.c_order_wx_c_doctype_id = 2001; // penjualan dengan SO
			this.titleForm = 'Keluar Barang';
			//	this.labelNoInout = 'No Surat Jalan';
			this.labelVendorOrCustomer = 'Customer';
			this.labelNoOrder = 'No Sales Order';

		}

		this.validation = {

			id_form: this.idFormValidation,


			c_order_wx_documentno: {
				required: true
			},



		};




		this.Init();


		this.PageEventHandler();

	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery


		HelperService.InsertReplaceFormModal(thisObject.idFormModal,  thisObject.GenerateFormInput());

		HelperService.InjectViewModelToForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.componentModel);

	}






	GenerateFormInput() {
		let thisObject = this;

		return `<div class="modal fade" id="` + thisObject.idFormModal + `" tabindex="-1" role="dialog" aria-labelledby="product-form-labelled"
		style="display: none;" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="product-form-labelled">`+ thisObject.titleForm + `</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<form id="`+ thisObject.idFormValidation + `" class="kt-form">
				<div class="modal-body">
				
					<div class="form-group">
						<label>No Dokumen</label>
						<input disabled="disabled" id="c_invoice_wx_documentno" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
						<span class="form-text text-muted">No Dokumen dibuat otomatis</span>
					</div>
					<div class="form-group form-group-marginless">
						<label>`+ thisObject.labelNoOrder + `</label>
						
						<div class="input-group">
							<input disabled="disabled" type="text" class="` + thisObject.classNameFormControl + ` form-control" id="c_order_wx_documentno" placeholder="Cari ` + thisObject.labelNoOrder + `">
							<div class="input-group-append">
							<span id="search_c_order" class="input-group-text"><i class="la la-search"></i></span>
							</div>
						</div>
						<input type="hidden" class="` + thisObject.classNameFormControl + `" id="c_order_wx_c_order_uu">
					</div>
				
					<div class="form-group">
						<label>`+ thisObject.labelVendorOrCustomer + `</label>
						<input disabled="disabled" id="c_bpartner_wx_name" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
						<span class="form-text text-muted"></span>
						<input type="hidden" class="` + thisObject.classNameFormControl + `" id="c_bpartner_wx_c_bpartner_uu">
						<input type="hidden" class="` + thisObject.classNameFormControl + `" id="c_bpartner_location_wx_c_bpartner_location_uu">
					</div>`+
			thisObject.invoiceReferenceHtml + `
					
					<div class="form-group">
					<label>Deskripsi</label>
					<input  id="c_invoice_wx_description" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
					<span class="form-text text-muted">Deskripsi</span>
				</div>			
				<div class="form-group ">
							<label>Tanggal Transaksi</label>
							<input type="text" class="` + thisObject.classNameFormControl + ` datepicker form-control" id="c_invoice_wx_dateacct" readonly="" placeholder="Select date">
						</div>	
					<div class="form-group">
						<label>Dokumen Status</label>
						<input disabled="disabled" id="c_invoice_wx_docstatus" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
						<span class="form-text text-muted">Status Dokumen DRAFT / COMPLETED / REVERSED</span>
					</div>
					<div class="form-group"id="`+ thisObject.idWidgetSearch + `">
							
	
					</div>
					</div>	
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button id="`+ thisObject.idButtonFormSubmit + `" type="submit" class="btn btn-primary">Save changes</button>
					</div>
				
				</form>
			</div>
		</div>
	</div>`;
	}


	PageEventHandler() {
		let thisObject = this;
		$('#' + thisObject.idFormValidation).on('submit');
		$('#' + thisObject.idFormValidation).on('submit', function (e) {
			thisObject.Save();
			e.preventDefault();

		});
		$('#' + thisObject.idFormValidation + ' #search_c_order').off('click');
		$('#' + thisObject.idFormValidation + ' #search_c_order').on('click', function () {
			let functionGetResult = function (orderSelected) {
				$('#' + thisObject.idFormValidation + ' #c_order_wx_c_order_uu').val(orderSelected.c_order_wx_c_order_uu);

				$('#' + thisObject.idFormValidation + ' #c_order_wx_documentno').val(orderSelected.c_order_wx_documentno);

				$('#' + thisObject.idFormValidation + ' #c_bpartner_wx_name').val(orderSelected.c_bpartner_wx_name);

				$('#' + thisObject.idFormValidation + ' #c_bpartner_wx_c_bpartner_uu').val(orderSelected.c_bpartner_wx_c_bpartner_uu);
				$('#' + thisObject.idFormValidation + ' #c_bpartner_wx_c_bpartner_location_uu').val(orderSelected.c_bpartner_wx_c_bpartner_location_uu);

			};
			let order_search_widget = new OrderSearchWidget(thisObject.idWidgetSearch, functionGetResult, thisObject.labelNoOrder, thisObject.c_order_wx_c_doctype_id);
			order_search_widget.Init();
		});

		/** BEGIN EVENT SEARCH M_Product *************************************
		$('#' + thisObject.idFormValidation + ' #search_c_order').off('click');
		$('#' + thisObject.idFormValidation + ' #search_c_order').on('click', function () {

			let htmlSearchCOrder = `<label>Cari ` + thisObject.labelCOrder + `</label>
		<div class="kt-input-icon kt-input-icon--left">
			<input type="text" class="form-control" placeholder="Ketik nomor dokumen"
				id="input_search_c_order">
			<span class="kt-input-icon__icon kt-input-icon__icon--left">
				<span><i class="la la-search"></i></span>
			</span>
		</div>
	</div>
	<div class="kt-scroll" data-scroll="true" data-height="245" data-mobile-height="200"
		style="height: 245px;overflow:hidden;">
		<div id="search_result" class="kt-notification">				

		</div>`;

			$('#' + thisObject.idFormValidation + ' #fragment_search_content').html(htmlSearchCOrder);

			$('#' + thisObject.idFormValidation + ' #input_search_c_order').focus();

			$('#' + thisObject.idFormValidation + ' #input_search_c_order').on("keyup", function () {
				let searchValue = $(this).val();
				$('#' + thisObject.idFormValidation + ' #search_result').html('');
				if (searchValue.length >= 3) {
					let metaDataTableSearch = new MetaDataTable();
					metaDataTableSearch.ad_org_uu = localStorage.getItem('ad_org_uu');
					metaDataTableSearch.otherFilters = [' AND c_order.documentno LIKE  \'%' + searchValue + '%\'', ' AND c_order.c_doctype_id = ' + thisObject.c_order_wx_c_doctype_id, ' AND c_order.docstatus = \'COMPLETED\' OR c_order.docstatus= \'CLOSED\''];

					thisObject.orderService.FindAll(metaDataTableSearch).then(function (rows) {

						_.forEach(rows, function (row) {
							let grandTotal = new Intl.NumberFormat().format(row.c_order_wx_grandtotal);
							let itemOrder = `<a href="#" id='` + row.c_order_wx_c_order_uu + `' class="result-order-search-item kt-notification__item">
					<div class="kt-notification__item-icon">
						<i class="flaticon2-box-1 kt-font-brand"></i>
					</div>
					<div class="kt-notification__item-details">
						<div  class="kt-notification__item-title">
							`+ row.c_order_wx_documentno + `
						</div>
						<div class="kt-notification__item-time">
						`+ row.c_bpartner_wx_name + `
						</div>
						<div class="kt-notification__item-time">
							 Lokasi: `+ row.m_warehouse_wx_name + `
						</div>
						<div class="kt-notification__item-time">
							Grand Total : `+ grandTotal + `
						</div>
					</div>						
				</a>`;
							$('#' + thisObject.idFormValidation + ' #search_result').append(itemOrder);


						});

					});



				}
			});
		});

		$(document).on('click', '#' + thisObject.idFormValidation + ' .result-order-search-item ', function () {

			let cOrderUu = $(this).attr('id');

			let listCOrder = thisObject.orderService.FindOrderViewModelByCOrderUu(cOrderUu);

			if (listCOrder.length > 0) {
				thisObject.orderViewModelTable = listCOrder[0];
				$('#' + thisObject.idFormValidation + ' #c_order_wx_c_order_uu').val(thisObject.orderViewModelTable.c_order_wx_c_order_uu);

				$('#' + thisObject.idFormValidation + ' #c_order_wx_documentno').val(thisObject.orderViewModelTable.c_order_wx_documentno);

				$('#' + thisObject.idFormValidation + ' #c_bpartner_wx_name').val(thisObject.orderViewModelTable.c_bpartner_wx_name);

				$('#' + thisObject.idFormValidation + ' #c_bpartner_wx_c_bpartner_uu').val(thisObject.orderViewModelTable.c_bpartner_wx_c_bpartner_uu);
				$('#' + thisObject.idFormValidation + ' #c_bpartner_wx_c_bpartner_location_uu').val(thisObject.orderViewModelTable.c_bpartner_wx_c_bpartner_location_uu);

			}


			$('#' + thisObject.idFormValidation + ' #fragment_search_content').html('');

		});

*/


		$('#' + thisObject.idFormModal).modal('show');


	}


	/**
	 * 
	 
	 */
	async Save() {
		let thisObject = this;

		let insertSuccess = 0;
		try {
			if (HelperService.CheckValidation(thisObject.validation)) {

				thisObject.componentModel = HelperService.GetViewModelFromForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.componentModel);
				thisObject.componentModel.c_doctype.c_doctype_id = thisObject.c_doctype_id;

				insertSuccess = await thisObject.invoiceService.SaveUpdate(thisObject.componentModel, thisObject.datatableReference);
				// jika sukses tersimpan 
				if (insertSuccess > 0) {

					$('#' + thisObject.idFormModal).modal('hide');


				}

			}

		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.Save.name, error);
		}


	}



}
