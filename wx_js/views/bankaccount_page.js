class BankaccountPage {

	constructor(idFragmentContent, bankaccountType) {

		this.viewModel = new BankaccountViewModel();
		this.bankaccountType = bankaccountType;
		this.idFragmentContent = idFragmentContent;
		this.metaDataTable = new MetaDataTable();
		this.metaDataTable.ad_org_uu = localStorage.getItem('ad_org_uu');
		this.metaDataTable.otherFilters.push('AND bankaccounttype = \'' + bankaccountType + '\'');
		this.datatableReference = {}; // buat reference datatable
		this.classShowButtonForm = 'bankaccount_page_show_bankaccount_form';
		this.classDeleteRow = 'bankaccount_page_delete_row';
		if (bankaccountType === 'C') {
			this.titlePage = 'Akun Bank';
			this.c_bank_id = 1000001;
		}

		if (bankaccountType === 'B') {
			this.titlePage = 'Akun Petty Cash';
			this.c_bank_id = 1000000;
		}


		this.bankaccountService = new BankaccountService();

	}



	Init() {
		try {
			let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery
			let allHtml = `
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__head kt-portlet__head--lg">
				<div  class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
				<i class="kt-font-brand flaticon2-line-chart"></i>
				</span>
				<h3 class="kt-portlet__head-title">`+ thisObject.titlePage + `
				</h3>
				</div>
				<div  class="kt-portlet__head-toolbar">
				<div class="kt-portlet__head-wrapper">
				<div class="kt-portlet__head-actions">
				<button type="button" class="`+ thisObject.classShowButtonForm + ` btn btn-bold btn-label-brand btn-sm" name="new_record" >
				<i class="la la-plus"></i>New Record</button>			
				</div>
				</div>
				</div>
				</div>
				<div class="kt-portlet__body" >
				<table class="table table-striped- table-bordered table-hover table-checkable" id="wx_bankaccount_table">
				
				</thead>
				</table>
				</div>
			</div>
				
			`;


			$('#' + thisObject.idFragmentContent).html(allHtml);


			var table = $('#wx_bankaccount_table');

			thisObject.bankaccountService.FindAll(thisObject.metaDataTable).then(function (bankaccountCMTList) {
				// begin first table
				thisObject.datatableReference = table.DataTable({
					responsive: true,
					data: bankaccountCMTList,
					scrollX: true,
					columns: [
						{ title: thisObject.titlePage, data: 'c_bankaccount_wx_name' },
						{ title: 'Nomor Akun', data: 'c_bankaccount_wx_accountno' },
						{ title: 'Asset Element Value', data: 'c_bankaccount_wx_asset_element' },
						{ title: 'Transit Element Value', data: 'c_bankaccount_wx_transit_element' },
						{ title: 'Action' }
					],
					columnDefs: [
						{
							targets: -1,
							title: 'Actions',
							orderable: false,
							render: function (data, type, full, meta) {
								return `
					<button id="bttn_row_edit`+ full.c_bankaccount_wx_c_bankaccount_uu + `" name="` + full.c_bankaccount_wx_c_bankaccount_uu + `" type="button" class="` + thisObject.classShowButtonForm + ` btn btn-outline-hover-info btn-elevate btn-icon"><i class="la la-edit"></i></button>
					<button id="bttn_row_delete`+ full.c_bankaccount_wx_c_bankaccount_uu + `" value="` + full.c_bankaccount_wx_name + `" name="` + full.c_bankaccount_wx_c_bankaccount_uu + `" type="button"   class="` + thisObject.classDeleteRow + ` btn btn-outline-hover-info btn-elevate btn-icon"> <i class="la la-trash"></i></button>
					`;
							},
						},
						/*{
							targets: -2,
							title: 'Current Balance',
							orderable: false,
							render: function (data, type, full, meta) {
								let linenetamt = new Intl.NumberFormat().format(full.c_bankaccount_wx_currentbalance);

								return linenetamt;
							},
						}*/

					],

				});

				thisObject.EventHandler();


			});





		} catch (err) {
			apiInterface.Log(this.constructor.name, this.Init.name, err);

		}




	}

	EventHandler() {
		let thisObject = this;


		$('.' + thisObject.classDeleteRow).off('click');
		$('.' + thisObject.classDeleteRow).on('click', function () {
			let nameDeleted = $(this).val();
			let uuid = $(this).attr('name');
			let deleteWidget = new DialogBoxDeleteWidget('Hapus Data', nameDeleted, uuid, thisObject);

		});

		$('.' + thisObject.classShowButtonForm).off('click');
		$('.' + thisObject.classShowButtonForm).on('click', function (e) {

			try {
				let attrName = $(this).attr('name');

				if (attrName !== 'new_record') {

					let dataEdited = thisObject.datatableReference.row($('#bttn_row_edit' + attrName).parents('tr')).data();


					thisObject.componentModel = HelperService.ConvertRowDataToViewModel(dataEdited);

				} else {

					thisObject.componentModel = new BankaccountViewModel();
					thisObject.componentModel.c_bankaccount.bankaccounttype = thisObject.bankaccountType;
					thisObject.componentModel.c_bankaccount.c_bank_id = thisObject.c_bank_id;

				}
				let Form = new BankaccountForm(thisObject.componentModel, thisObject.datatableReference);

			} catch (error) {
				apiInterface.Log(thisObject.constructor.name, thisObject.EventHandler.name, error)
			}

		});

		thisObject.datatableReference.off('draw');
		thisObject.datatableReference.on('draw', function () {

			thisObject.EventHandler();
		});



	}


	/**
	 * Delete pada table, sebenarnya hanya menandai wx_isdeleted = 'Y'
	 * @param {*} dataObject dataObject pada datatable 
	 */
	async Delete(uuidDeleted) {
		let insertSuccess = 0;
		let thisObject = this;
		try {
			let sqlstatementDeleteBankaccount = HelperService.SqlDeleteStatement('c_bankaccount', 'c_bankaccount_uu', uuidDeleted);
			insertSuccess += await apiInterface.ExecuteSqlStatement(sqlstatementDeleteBankaccount);
			if (insertSuccess) {
				// delete row pada datatable
				thisObject.datatableReference
					.row($('#bttn_row_delete' + uuidDeleted).parents('tr'))
					.remove()
					.draw();


			}
		} catch (err) {
			apiInterface.Log(this.constructor.name, this.Delete.name, err);

		}

		return insertSuccess;
	}

}
