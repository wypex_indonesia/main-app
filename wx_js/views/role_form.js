class RoleForm {

	/**
	 * 
	
	
	 * @param {RoleComponentModel} roleCM
	 * @param {datatableReference} datatableReference data tabel pada role page, sebagai reference untuk mengupdate table tersebut	 
	 */
	constructor(roleCM, datatableReference) {

		this.titleForm = 'Add/Edit Role';

		this.viewModel = roleCM;


		this.metaDataTable = new MetaDataTable();

		this.datatableReference = datatableReference; // buat reference datatable

		this.roleService = new RoleService();


		this.idFormModal = 'ad_role_form_wx_';
		this.idFormValidation = 'ad_role_form_validation_';
		this.classNameFormControl = 'ad_role-form-control';
		this.selectorClassNameFormControl = '.' + this.classNameFormControl;


		this.idButtonFormSubmit = 'ad_role_form_submit_wx_';


		this.validation = {

			id_form: this.idFormValidation,

			ad_role_wx_name: {
				required: true
			},

		};




		this.Init();


		this.PageEventHandler();

	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery

		try {

			HelperService.InsertReplaceFormModal(thisObject.idFormModal, thisObject.GenerateFormInput());

			HelperService.InjectViewModelToForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.viewModel);

		} catch (error) {
			apiInterface.Log(this.constructor.name, this.Init.name, error);
		}


	}




	/**
	 * 
	
	 */
	async Save() {
		let thisObject = this;

		try {

			let insertSuccess = 0;
			if (HelperService.CheckValidation(thisObject.validation)) {

				thisObject.viewModel = HelperService.GetViewModelFromForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.viewModel);

				insertSuccess = await thisObject.roleService.SaveUpdate(thisObject.viewModel, thisObject.datatableReference);
				// jika sukses tersimpan 
				if (insertSuccess > 0) {

					$('#' + thisObject.idFormModal).modal('hide');


				}

			}
		} catch (err) {
			apiInterface.Log(this.constructor.name, this.Save.name, err);
		}



	}

	GenerateFormInput() {
		let thisObject = this;

		let html = '';
		try {
			html = `<div class="modal fade" id="` + thisObject.idFormModal + `" tabindex="-1" role="dialog" aria-labelledby="product-form-labelled"
		style="display: none;" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="product-form-labelled">`+ thisObject.titleForm + `</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<form id="`+ thisObject.idFormValidation + `" class="kt-form">
					<div class="form-group">
						<label>Nama Role</label>
						<input id="ad_role_wx_name" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
						<span class="form-text text-muted">Nama Role untuk profile user </span>
					</div>
					
					</form>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button id="`+ thisObject.idButtonFormSubmit + `" type="button" class="btn btn-primary">Save changes</button>
					</div>
				</div>
			</div>
		</div>
	</div>`;
		} catch (error) {
			apiInterface.Log(this.constructor.name, this.GenerateFormInput.name, error);
		}

		return html;
	}


	PageEventHandler() {
		let thisObject = this;


		try {

			$('#' + thisObject.idButtonFormSubmit).off('click');
			$('#' + thisObject.idButtonFormSubmit).on('click', function () {
				thisObject.Save();

			});



			$('#' + thisObject.idFormModal).modal('show');


		} catch (err) {
			apiInterface.Log(this.constructor.name, this.PageEventHandler.name, err);
		}


	}



}
