class ShipperPage {

	constructor() {
		
		this.metaDataTable = new MetaDataTable();
		this.classShowButtonForm = 'shipper_page_bttn_show_form';
		this.classDeleteRow = 'shipper_page_delete_row';

		this.datatableReference = {}; // buat reference datatable

		this.shipperService = new ShipperService();

	}



	Init() {
		try {
			let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery
			let allHtml = `
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head kt-portlet__head--lg">
			<div  class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
			<i class="kt-font-brand flaticon2-line-chart"></i>
			</span>
			<h3 class="kt-portlet__head-title">
			Kurir
			</h3>
			</div>
			<div  class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-wrapper">
			<div class="kt-portlet__head-actions">
			<button type="button" class="`+thisObject.classShowButtonForm + ` btn btn-bold btn-label-brand btn-sm" name="new_record" >
			<i class="la la-plus"></i>New Record</button>			
			</div>
			</div>
			</div>
			</div>
			<div class="kt-portlet__body" >
			<table class="table table-striped- table-bordered table-hover table-checkable" id="wx_shipper_table">
			
			</thead>
			</table>
			</div>
		</div>
				
		`;


			this.metaDataTable.ad_org_uu = localStorage.getItem('ad_org_uu');



			$('#wx_fragment_content').html(allHtml);


			var table = $('#wx_shipper_table');

			thisObject.shipperService.FindAll(thisObject.metaDataTable).then(function (shipperCMTList) {

				// begin first table
				thisObject.datatableReference = table.DataTable({
					responsive: true,
					data: shipperCMTList,
					scrollX: true,
					columns: [
						{ title: 'Nama', data: 'm_shipper_wx_name' },
						{ title: 'Action' }
					],
					columnDefs: [
						{
							targets: -1,
							title: 'Actions',
							orderable: false,
							render: function (data, type, full, meta) {
								return `
				<button id="bttn_row_edit`+ full.m_shipper_wx_m_shipper_uu + `" name="` + full.m_shipper_wx_m_shipper_uu + `" type="button" class="`+thisObject.classShowButtonForm +` btn btn-outline-hover-info btn-elevate btn-icon"><i class="la la-edit"></i></button>
				<button id="bttn_row_delete`+ full.m_shipper_wx_m_shipper_uu + `"   value="` + full.m_shipper_wx_name + `" name="` + full.m_shipper_wx_m_shipper_uu + `" type="button"   class="` + thisObject.classDeleteRow + ` btn btn-outline-hover-info btn-elevate btn-icon"> <i class="la la-trash"></i></button>
				`;
							},
						},
					],

				});

				thisObject.EventHandler();
			});


		} catch (error) {
			apiInterface.Log(this.constructor.name, this.Init.name, error);
		}





	}

	EventHandler() {
		let thisObject = this;
	/*	$('#shipper-form-submit').off('click');
		$('#shipper-form-submit').on('click', function () {
			thisObject.SaveUpdate();

		});*/

		$('.' + thisObject.classDeleteRow).off('click');
		$('.' + thisObject.classDeleteRow).on('click', function () {
			let nameDeleted = $(this).val();
			let uuid  = $(this).attr('name');
			let deleteWidget = new DialogBoxDeleteWidget('Hapus Data', nameDeleted, uuid, thisObject);

		});

		$('.'+thisObject.classShowButtonForm).off('click');
		$('.'+thisObject.classShowButtonForm).on('click', function (e) {

			try {
				let attrName = $(this).attr('name');
			
			if (attrName !== 'new_record') {

				let dataEdited = thisObject.datatableReference.row($('#bttn_row_edit' + attrName).parents('tr')).data();


				thisObject.componentModel = HelperService.ConvertRowDataToViewModel(dataEdited);			

			} else {

				thisObject.componentModel = new ShipperComponentModel();
				

			}
			let shipperForm = new ShipperForm(thisObject.componentModel,thisObject.datatableReference);
		
			} catch (error) {
				apiInterface.Log(thisObject.constructor.name, thisObject.EventHandler.name,error)
			}
			
		});

		thisObject.datatableReference.off('draw');
		thisObject.datatableReference.on('draw', function(){

			thisObject.EventHandler();
		});
		
	}




	/**
	 * Delete pada table, sebenarnya hanya menandai wx_isdeleted = 'Y'
	 * @param {*} dataObject dataObject pada datatable 
	 */
	async Delete(uuidDeleted) {
		let insertSuccess = 0;
		let thisObject = this;
		try {
			let sqlstatementDeleteShipper = HelperService.SqlDeleteStatement('m_shipper', 'm_shipper_uu', uuidDeleted);
			insertSuccess += await apiInterface.ExecuteSqlStatement(sqlstatementDeleteShipper);

		//	let dataDeleted = thisObject.datatableReference.row($('#bttn_row_delete' + uuidDeleted).parents('tr')).data();


			if (insertSuccess) {
				// delete row pada datatable
				thisObject.datatableReference
					.row($('#bttn_row_delete' + uuidDeleted).parents('tr'))
					.remove()
					.draw();
				$('#modal_info_box').modal('hide');

			}
		} catch (error) {
			apiInterface.Log(this.constructor.name, this.Delete.name, error);
		}

		return insertSuccess;
	}

}
