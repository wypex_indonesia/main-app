class OrderlinePage {

	/**
	 * 
	 * @param {OrderViewModelTable} cOrderParent product yang akan diproduksi
	 * @param {string} idFragmentContent name id fragment content untuk meletakkan html content dari productbom_page
	 */
	constructor(cOrderParent, idFragmentContent, parentDatatableReference, orderPage) {

		this.cOrderParent = cOrderParent;
		this.parentDatatableReference = parentDatatableReference;
		this.orderPage = orderPage;



		this.viewModel = new OrderlineViewModel();

		this.metaDataTable = new MetaDataTable();


		this.datatableReference = {}; // buat reference datatable

		this.orderlineService = new OrderlineService();


		this.idFragmentContent = idFragmentContent;


		this.idTable = 'wx_orderline_table_' + cOrderParent.c_order_wx_c_order_uu;
		this.idNewRecord = 'new_record_orderline_wx_' + cOrderParent.c_order_wx_c_order_uu;
		this.idGenerateItemRetur = 'orderline_page_generate_item_retur' + cOrderParent.c_order_wx_c_order_uu;
		this.classGenerateItemRetur = 'orderline_page_generate_item';

		this.idShowFormOrderline = 'wx-btn-show-form-orderline';

		this.classDeleteRow = 'orderline_page_delete_row';
	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery


		thisObject.metaDataTable.ad_org_uu = localStorage.getItem('ad_org_uu');

		let buttonNewRecordHtml = '';
		let buttonGenerateRetur = '';
		if (thisObject.cOrderParent.c_order_wx_docstatus === 'DRAFT') {
			buttonNewRecordHtml = `<button type="button" class="` + thisObject.idShowFormOrderline + ` btn btn-bold btn-label-brand btn-sm"  id="` + thisObject.idNewRecord + `" name="new_record"> <i class="la la-plus"></i>New Record</button>`;

			//penjualan langsung retur
			if (thisObject.cOrderParent.c_order_wx_c_doctype_id === 2002) {
				buttonGenerateRetur = `<button type="button"  class="` + thisObject.classGenerateItemRetur + ` btn btn-bold btn-label-brand btn-sm" name="` + thisObject.cOrderParent.c_order_wx_c_invoice_uu_retur + `">  <i class="la la-plus"></i>Pilih Item Retur</button>`;
			}
		}

		let allHtml =
			buttonNewRecordHtml + buttonGenerateRetur +
			`<table class="table table-striped- table-bordered table-hover table-checkable" id="` + thisObject.idTable + `">			
			</table>
		`;




		$('#' + thisObject.idFragmentContent).html(allHtml);


		thisObject.CreateDataTable();


	}


	PageEventHandler() {
		let thisObject = this;

		$('.' + thisObject.idShowFormOrderline).off("click");
		$('.' + thisObject.idShowFormOrderline).on("click", function (e) {

			let attrName = $(this).attr('name');
			let orderlineViewModel = new OrderlineViewModel();
			orderlineViewModel.c_orderline.c_order_uu = thisObject.cOrderParent.c_order_wx_c_order_uu;
			if (attrName !== 'new_record') {
				let dataEdited = thisObject.datatableReference.row($('#bttn_row_edit' + attrName).parents('tr')).data();
				orderlineViewModel = HelperService.ConvertRowDataToViewModel(dataEdited);
			}
			let orderFormPage = new OrderlineForm(thisObject.cOrderParent, thisObject.idFormFragmentContent, orderlineViewModel, thisObject.datatableReference);
		});


		$('.' + thisObject.classDeleteRow).off('click');
		$('.' + thisObject.classDeleteRow).on('click', function () {
			let nameDeleted = $(this).val();
			let uuid = $(this).attr('name');
			let deleteWidget = new DialogBoxDeleteWidget('Hapus Data', nameDeleted, uuid, thisObject);

		});

		$('.' + thisObject.classGenerateItemRetur).off('click');
		$('.' + thisObject.classGenerateItemRetur).on('click', function () {
			let c_invoice_uu_retur = $(this).attr('name');

			let functionPostItemSelected = function (listCInvoicelineUuSelected) {

				return thisObject.PostItemSelected(listCInvoicelineUuSelected);
			};

			let dialogBoxItemRetur = new DialogBoxItemReturWidget(c_invoice_uu_retur, functionPostItemSelected);
		});


		thisObject.datatableReference.off('draw');
		thisObject.datatableReference.on('draw', function () {
			let orderService = new OrderService();
			orderService.UpdateTableRowWithoutDraw(thisObject.parentDatatableReference, thisObject.cOrderParent.c_order_wx_c_order_uu, thisObject.orderPage);
			thisObject.PageEventHandler();

		});
	}


	/**
	 * Retur item akan dimasukkan ke datatables orderline
	 * @param {*} listCInvoicelineUuSelected 
	 * return length orderlineCMTLIst sebagai insertSuccess
	 */
	PostItemSelected(listCInvoicelineUuSelected) {
		let thisObject = this;


		return new Promise(function (resolve, reject) {
			let orderlineService = new OrderlineService();
			orderlineService.ConvertCInvoicelineUuReturToOrderline(listCInvoicelineUuSelected,
				thisObject.cOrderParent.c_order_wx_c_order_uu,
				thisObject.datatableReference.data().length).then(function (orderlineCMTList) {

					resolve(orderlineCMTList);

				});

		}).then(function (orderlineCMTList) {

			return new Promise(function (resolve, reject) {

				_.forEach(orderlineCMTList, function (orderlineCMT) {
					thisObject.datatableReference.row.add(orderlineCMT).draw();

				});
				//finish
				resolve(1);

			});
		});


	}

	CreateDataTable() {

		let thisObject = this;

		thisObject.metaDataTable.otherFilters = [' AND c_orderline.c_order_uu = \'' + thisObject.cOrderParent.c_order_wx_c_order_uu + '\''];

		var table = $('#' + thisObject.idTable);

		let columnDefDatatable = [];
		let columnsTable = [
			{ title: 'No', data: 'c_orderline_wx_line' },
			{ title: 'Produk', data: 'm_product_wx_name' },
			{ title: 'Satuan', data: 'c_uom_wx_name' },
			{ title: 'Jumlah', data: 'c_orderline_wx_qtyentered' },
			{ title: 'Harga' },
			{ title: 'Total Net' }
		];
		let indexColumnTotalNet = -1;
		let indexColumnHarga = -2;

		if (thisObject.cOrderParent.c_order_wx_docstatus === 'DRAFT') {

			columnDefDatatable.push({
				targets: -1,
				title: 'Actions',
				orderable: false,
				render: function (data, type, full, meta) {
					let htmlButton = '';
					//	if (full.c_orderline_wx_isreturn !== 'Y') {
					htmlButton = `
						<button id="bttn_row_edit`+ full.c_orderline_wx_c_orderline_uu + `" name="` + full.c_orderline_wx_c_orderline_uu + `" type="button"  class="` + thisObject.idShowFormOrderline + ` btn btn-outline-hover-info btn-elevate btn-icon"><i class="la la-edit"></i></button>
						<button id="bttn_row_delete`+ full.c_orderline_wx_c_orderline_uu + `" value="` + full.m_product_wx_name + `" name="` + full.c_orderline_wx_c_orderline_uu + `" type="button" class="` + thisObject.classDeleteRow + ` btn btn-outline-hover-info btn-elevate btn-icon"> <i class="la la-trash"></i></button>
						`;
					//	}
					return htmlButton;
				},
			});

			columnsTable.push({ title: 'Action' });
			indexColumnTotalNet = -2;
			indexColumnHarga = -3;
		}

		columnDefDatatable.push({
			targets: indexColumnTotalNet,
			title: 'Total Net',
			orderable: false,
			render: function (data, type, full, meta) {
				let retur = '';
				if (full.c_orderline_wx_isreturn === 'Y') {
					retur = ' (R)';
				}

				let returnString = new Intl.NumberFormat().format(full.c_orderline_wx_linenetamt) + retur;
				return returnString;
			},
		});

		columnDefDatatable.push({
			targets: indexColumnHarga,
			title: 'Harga',
			orderable: false,
			render: function (data, type, full, meta) {
				return new Intl.NumberFormat().format(full.c_orderline_wx_priceentered);
			},
		});

		thisObject.orderlineService.FindAll(thisObject.metaDataTable).then(function (orderlineCMTList) {
			// begin first table
			if (!$.fn.dataTable.isDataTable('#' + thisObject.idTable)) {
				thisObject.datatableReference = table.DataTable({
					searching: false,
					paging: false,
					responsive: true,
					data: orderlineCMTList,
					scrollX: true,
					columns: columnsTable,
					columnDefs: columnDefDatatable,
					"order": [[1, 'asc']],

				});

				thisObject.PageEventHandler();

			}


		});



	}

	/**
 * Delete pada table, sebenarnya hanya menandai wx_isdeleted = 'Y'
 * @param {*} dataObject dataObject pada datatable 
 */
	async Delete(uuiddeleted) {
		let thisObject = this;
		let insertSuccess = 0;
		try {
			let sqlstatementDeleteBankaccount = HelperService.SqlDeleteStatement('c_orderline', 'c_orderline_uu', uuiddeleted);

			insertSuccess += await thisObject.orderlineService.ExecuteAndUpdateOrderSql(sqlstatementDeleteBankaccount,
				thisObject.cOrderParent.c_order_wx_c_order_uu);

			if (insertSuccess) {
			
				thisObject.datatableReference
					.row($('#bttn_row_delete' + uuiddeleted).parents('tr'))
					.remove()
					.draw();
			}
		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.Delete.name, error);
		}

		return insertSuccess;
	}




}
