/**
 * Movementqty = masuknya barang sesuai dengan jumlah basedUom
 * Qtyentered = masuknya barang sesuai dengan uom yang tertulis
 */
class RolelinePage {

	/**
	 * 
	 * @param {RoleComponentModelTable} roleCMT parent inoutViewModel
	 * @param {string} idFragmentContent name id fragment content untuk meletakkan html content dari productbom_page
	 */
	constructor(roleCMT, idFragmentContent) {

		this.roleCMT = roleCMT;


		this.metaDataTable = new MetaDataTable();


		this.datatableReference = {}; // buat reference datatable

		this.rolelineService = new RolelineService();

		this.idFragmentContent = idFragmentContent;
		this.idFormModal = 'wxt_roleline_form_wx_' + roleCMT.ad_role_wx_ad_role_uu;
		this.idFormValidation = 'wxt_roleline_form_validation_' + roleCMT.ad_role_wx_ad_role_uu;
		this.idDialogBox = 'wxt_roleline_dialog_box_wx_' + roleCMT.ad_role_wx_ad_role_uu;
		this.idTable = 'wx_roleline_table_' + roleCMT.ad_role_wx_ad_role_uu;

		this.idButtonFormSubmit = 'wxt_roleline_form_submit_wx_' + roleCMT.ad_role_wx_ad_role_uu;
		this.idBtnNewRecord = 'wxt_roleline_new_record_' + roleCMT.ad_role_wx_ad_role_uu;

		this.idShowFormRoleline = 'wx-btn-show-form-roleline';
	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery


		try {
			thisObject.metaDataTable.ad_org_uu = localStorage.getItem('ad_org_uu');

			let buttonNewRecordHtml = `
			<div class="btn-group" role="group">
				<button id="btnGroupDrop1" type="button" class="btn btn-bold btn-label-brand btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<i class="la la-plus"></i>	Pilih Hak Akses
				</button>
				<div class="dropdown-menu" aria-labelledby="btnGroupDrop1" style="">
					<a name="new_record" data-classname="SideMenu" class="` + thisObject.idShowFormRoleline + ` dropdown-item" href="#">Menu</a>
					<a name="new_record" data-classname="MPricelist" class="` + thisObject.idShowFormRoleline + ` dropdown-item" href="#">Harga Jual Beli Produk</a>
				
				</div>
			</div>
		`;


			let allHtml =
				buttonNewRecordHtml +
				`<table class="table table-striped- table-bordered table-hover table-checkable" id="` + thisObject.idTable + `">			
				</table>`;

			$('#' + thisObject.idFragmentContent).html(allHtml);


			thisObject.CreateDataTable();
			// attach event handler on the page

			thisObject.PageEventHandler();

		} catch (error) {
			apiInterface.Log(this.constructor.name, this.Init.name, error);
		}


	}



	CreateDataTable() {

		let thisObject = this;


		var table = $('#' + thisObject.idTable);

		let columnDefDatatable = [];
		let columnsTable = [
			{ title: 'Tipe Hak Akses', data: 'wxt_roleline_wx_class_name' },

		];



		columnsTable.push({ title: 'Action' });
		columnDefDatatable.push({
			targets: -1,
			title: 'Actions',
			orderable: false,
			render: function (data, type, full, meta) {
				return `
					<button id="bttn_row_edit`+ full.wxt_roleline_wx_wxt_roleline_uu + `" name="` + full.wxt_roleline_wx_wxt_roleline_uu + `" type="button"  class="` + thisObject.idShowFormRoleline + ` btn btn-outline-hover-info btn-elevate btn-icon"><i class="la la-edit"></i></button>
					<button id="bttn_row_delete`+ full.wxt_roleline_wx_wxt_roleline_uu + `" value="` + full.wxt_roleline_wx_class_name + `" name="` + full.wxt_roleline_wx_wxt_roleline_uu + `" type="button" data-toggle="modal"   class="` + thisObject.idDialogBox + ` wx-row-delete btn btn-outline-hover-info btn-elevate btn-icon"> <i class="la la-trash"></i></button>
					`;
			},
		});




		thisObject.rolelineService.FindRolelineCMTListByAdRoleUu(thisObject.roleCMT.ad_role_wx_ad_role_uu)
			.then(function (rolelineCMTList) {

				// begin first table
				if (!$.fn.dataTable.isDataTable('#' + thisObject.idTable)) {
					this.datatableReference = table.DataTable({
						searching: false,
						paging: false,
						responsive: true,
						data: rolelineCMTList,
						scrollX: true,
						columns: columnsTable,
						columnDefs: columnDefDatatable,
						"order": [[1, 'asc']],

					});


				}

			});


	}



	PageEventHandler() {
		let thisObject = this;

		try {


			$('.' + thisObject.idShowFormRoleline).off("click");
			$('.' + thisObject.idShowFormRoleline).on("click", function (e) {

				let attrName = $(this).attr('name');
				let className = $(this).data('classname');
				let rolelineCM = new RolelineComponentModel();
				if (attrName !== 'new_record') {
					let dataEdited = thisObject.datatableReference.row($('#bttn_row_edit' + attrName).parents('tr')).data();
					rolelineCM = HelperService.ConvertRowDataToViewModel(dataEdited);
				} else {
					let roleCM = HelperService.ConvertRowDataToViewModel(thisObject.roleCMT);
					rolelineCM.ad_role = roleCM.ad_role;
					rolelineCM.wxt_roleline.class_name = className;

				}

				if (rolelineCM.wxt_roleline.class_name === "SideMenu") {
					let rolelineFormPage = new RolelineTypeMenuForm(rolelineCM, thisObject.datatableReference);
				}

				if (rolelineCM.wxt_roleline.class_name === "MPricelist") {
					let rolelineFormPage = new RolelineTypePricelistForm(rolelineCM, thisObject.datatableReference);
				}

				e.preventDefault();


			});


			$('.' + thisObject.idDialogBox).off('click');
			$('.' + thisObject.idDialogBox).on('click', function (e) {
				let uuid = $(this).attr('name');
				let nameDeleted = $(this).val();
				let dialogDelete = new DialogBoxDeleteWidget('Notifikasi', nameDeleted, uuid, thisObject);
			});

			/*$('#' + thisObject.idDialogBox + ' #ok_info_box').off('click');
			$('#' + thisObject.idDialogBox + ' #ok_info_box').on('click', function () {
				let uuid = $(this).attr('name');
				

			});*/


			thisObject.datatableReference.off('draw');
			thisObject.datatableReference.on('draw', function () {
				thisObject.PageEventHandler();
			});
		} catch (error) {
			apiInterface.Log(this.constructor.name, this.PageEventHandler.name, error);
		}



	}

	Delete(uuid) {
		let thisObject = this;

		let insertSuccess = 0;
		try {
			let sqlstatementDeleteBankaccount = HelperService.SqlDeleteStatement('wxt_roleline', 'wxt_roleline_uu', uuid);
			insertSuccess += apiInterface.ExecuteSqlStatement(sqlstatementDeleteBankaccount);
			thisObject.datatableReference
				.row($('#bttn_row_delete' + uuid).parents('tr'))
				.remove()
				.draw();

		} catch (error) {
			apiInterface.Log(this.constructor.name, this.Delete.name, error);
		}

		return insertSuccess;

	}

}
