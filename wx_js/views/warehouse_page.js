class WarehousePage {

	constructor() {
	
		this.viewModel = new WarehouseViewModel();

		this.metaDataTable = new MetaDataTable();


		this.datatableReference = {}; // buat reference datatable

		this.warehouseService = new WarehouseService();

		this.classShowButtonForm = 'warehouse_page_bttn_show_form';
		this.classDeleteRow = 'warehouse_page_delete_row';


	}



	Init() {

		try {
			let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery
			let allHtml = `
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head kt-portlet__head--lg">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon">
						<i class="kt-font-brand flaticon2-line-chart"></i>
					</span>
					<h3 class="kt-portlet__head-title">
						Toko/Gudang
					</h3>
				</div>
				<div class="kt-portlet__head-toolbar">
					<div class="kt-portlet__head-wrapper">
						<div class="kt-portlet__head-actions">
							<button type="button" class="`+ thisObject.classShowButtonForm + ` btn btn-bold btn-label-brand btn-sm" name="new_record">
								<i class="la la-plus"></i>New Record</button>
						</div>
					</div>
				</div>
			</div>
			<div class="kt-portlet__body">
				<table class="table table-striped- table-bordered table-hover table-checkable" id="wx_warehouse_table">
		
				</table>
			</div>
		</div>
					
			`;


			this.metaDataTable.ad_org_uu = localStorage.getItem('ad_org_uu');

		
			$('#wx_fragment_content').html(allHtml);


			var table = $('#wx_warehouse_table');
			thisObject.warehouseService.FindAll(thisObject.metaDataTable).then(function(warehouseCMTList){

				thisObject.datatableReference = table.DataTable({
					responsive: true,
					data: warehouseCMTList ,
					scrollX: true,
					columns: [
						{ title: 'Nama', data: 'm_warehouse_wx_name' },
						{ title: 'Alamat', data: 'c_location_wx_address1' },
						{ title: 'Kota', data: 'c_location_wx_city' },
						{ title: 'Propinsi', data: 'c_location_wx_regionname' },
						{ title: 'Kodepos', data: 'c_location_wx_postal' },
						{ title: 'Action' }
					],
					columnDefs: [
						{
							targets: -1,
							title: 'Actions',
							orderable: false,
							render: function (data, type, full, meta) {
								return `
								<button id="bttn_row_edit`+ full.m_warehouse_wx_m_warehouse_uu + `" name="` + full.m_warehouse_wx_m_warehouse_uu + `" type="button" class="` + thisObject.classShowButtonForm + ` btn btn-outline-hover-info btn-elevate btn-icon"><i class="la la-edit"></i></button>
								<button id="bttn_row_delete`+ full.m_warehouse_wx_m_warehouse_uu + `" value="` + full.m_warehouse_wx_name + `" name="` + full.m_warehouse_wx_m_warehouse_uu + `" type="button" class="` + thisObject.classDeleteRow + ` btn btn-outline-hover-info btn-elevate btn-icon"> <i class="la la-trash"></i></button>
								`;
							},
						},
					],
	
				});

				thisObject.EventHandler();
			});
			// begin first table
			

			



		} catch (error) {
			apiInterface.Log(this.constructor.name, this.Init.name, error);
		}


	}

	EventHandler(){
		let thisObject = this;
		
		$('.' + thisObject.classDeleteRow).off('click');
		$('.' + thisObject.classDeleteRow).on('click', function () {
			let nameDeleted = $(this).val();
			let uuid = $(this).attr('name');
			let deleteWidget = new DialogBoxDeleteWidget('Hapus Data', nameDeleted, uuid, thisObject);

		});

		$('.' + thisObject.classShowButtonForm).off('click');
		$('.' + thisObject.classShowButtonForm).on('click', function (e) {

			try {
				let attrName = $(this).attr('name');

				if (attrName !== 'new_record') {

					let dataEdited = thisObject.datatableReference.row($('#bttn_row_edit' + attrName).parents('tr')).data();


					thisObject.componentModel = HelperService.ConvertRowDataToViewModel(dataEdited);

				} else {

					thisObject.componentModel = new WarehouseViewModel();


				}
				let uomForm = new WarehouseForm(thisObject.componentModel, thisObject.datatableReference);

			} catch (error) {
				apiInterface.Log(thisObject.constructor.name, thisObject.EventHandler.name, error)
			}

		});

		thisObject.datatableReference.off('draw');
		thisObject.datatableReference.on('draw', function () {

			thisObject.EventHandler();
		});

	}


	/**
	 * 
	
	 
	SaveUpdate() {
		let thisObject = this;

		try {
			if (HelperService.CheckValidation(thisObject.validation)) {
				$(".warehouse-form-control").each(function () {
					let id = $(this).attr('id');
					let arrayId = id.split("_wx_");
					thisObject.viewModel[arrayId[0]][arrayId[1]] = $(this).val();	// masukkan value

				});

				//const currentAdOrg = JSON.parse(localStorage.getItem('currentAdOrg'));
				//const currentAdUser = JSON.parse(localStorage.getItem('currentAdUser'));

				const timeNow = new Date().getTime();
				// jika new record maka buat mlocator 	
				// harus dibuat default locator, karena locator akan dipergunakan di inventory movement
				let isNewRecord = false;
				let mLocator;
				if (!thisObject.viewModel.m_warehouse.m_warehouse_uu) {
					isNewRecord = true;
					thisObject.viewModel.m_warehouse.m_warehouse_uu = HelperService.UUID();
					thisObject.viewModel.m_warehouse.ad_client_uu = localStorage.getItem('ad_client_uu');
					thisObject.viewModel.m_warehouse.ad_org_uu = localStorage.getItem('ad_org_uu');

					mLocator = new MLocator();
					mLocator.m_locator_uu = HelperService.UUID();
					mLocator.value = thisObject.viewModel.m_warehouse.name;
					mLocator.ad_client_uu = localStorage.getItem('ad_client_uu');
					mLocator.ad_org_uu = localStorage.getItem('ad_org_uu');
					mLocator.m_warehouse_uu = thisObject.viewModel.m_warehouse.m_warehouse_uu;
					mLocator.isdefault = 'Y';
					mLocator.sync_client = null;
					mLocator.process_date = null;
					mLocator.created = timeNow;
					mLocator.updated = timeNow;
					mLocator.createdby = localStorage.getItem('ad_user_uu');
					mLocator.updatedby = localStorage.getItem('ad_user_uu');



					thisObject.viewModel.m_warehouse.createdby = localStorage.getItem('ad_user_uu');
					thisObject.viewModel.m_warehouse.updatedby = localStorage.getItem('ad_user_uu');


					thisObject.viewModel.m_warehouse.created = timeNow;
					thisObject.viewModel.m_warehouse.updated = timeNow;
				} else { //update record

					thisObject.viewModel.m_warehouse.updatedby = localStorage.getItem('ad_user_uu');


					thisObject.viewModel.m_warehouse.updated = timeNow;
				}

				thisObject.viewModel.m_warehouse.sync_client = null;
				thisObject.viewModel.m_warehouse.process_date = null;


				if (!thisObject.viewModel.c_location.c_location_uu) {
					thisObject.viewModel.c_location.c_location_uu = HelperService.UUID();
					thisObject.viewModel.c_location.ad_client_uu = localStorage.getItem('ad_client_uu');
					thisObject.viewModel.c_location.ad_org_uu = localStorage.getItem('ad_org_uu');

					thisObject.viewModel.c_location.createdby = localStorage.getItem('ad_user_uu');
					thisObject.viewModel.c_location.updatedby = localStorage.getItem('ad_user_uu');

					thisObject.viewModel.c_location.created = timeNow;
					thisObject.viewModel.c_location.updated = timeNow;
				} else {


					thisObject.viewModel.c_location.updatedby = localStorage.getItem('ad_user_uu');

					thisObject.viewModel.c_location.updated = timeNow;
				}

				thisObject.viewModel.c_location.sync_client = null;
				thisObject.viewModel.c_location.process_date = null;


				thisObject.viewModel.m_warehouse.c_location_uu = thisObject.viewModel.c_location.c_location_uu;

				let insertSuccess = 0;
				const sqlInsertWarehouse = HelperService.SqlInsertOrReplaceStatement('m_warehouse', thisObject.viewModel.m_warehouse);

				insertSuccess += apiInterface.ExecuteSqlStatement(sqlInsertWarehouse);

				const sqlInsertLocation = HelperService.SqlInsertOrReplaceStatement('c_location', thisObject.viewModel.c_location);
				insertSuccess += apiInterface.ExecuteSqlStatement(sqlInsertLocation);




				if (mLocator) {

					const sqlInsertLocator = HelperService.SqlInsertOrReplaceStatement('m_locator', mLocator);

					insertSuccess += apiInterface.ExecuteSqlStatement(sqlInsertLocator);
				}

				// jika sukses tersimpan 
				if (insertSuccess > 0) {

					let rowData = HelperService.ConvertViewModelToRow(thisObject.viewModel);

					if (!isNewRecord) {

						thisObject.datatableReference.row($('#bttn_row_edit' + rowData['m_warehouse_wx_m_warehouse_uu']).parents('tr')).data(rowData).draw();
					} else {

						thisObject.datatableReference.row.add(rowData).draw();
					}


				}
				$('#warehouse-form').modal('hide');

			}

		} catch (error) {
			apiInterface.Log(this.constructor.name, this.SaveUpdate.name, error);
		}




	}
*/
	/**
	 * Delete pada table, sebenarnya hanya menandai wx_isdeleted = 'Y'
	 * @param {*} dataObject dataObject pada datatable 
	 */
	async Delete(uuidDeleted) {
		let insertSuccess = 0;
		let thisObject = this;
		try {

			let warehouseCMTList = await thisObject.warehouseService.FindWarehouseViewModelByMWarehouseUu(uuidDeleted);
			if (warehouseCMTList.length){
				let rowData = warehouseCMTList[0];
				let sqlstatementDeleteWarehouse = HelperService.SqlDeleteStatement('m_warehouse', 'm_warehouse_uu', rowData.m_warehouse_wx_m_warehouse_uu);
				insertSuccess += await apiInterface.ExecuteSqlStatement(sqlstatementDeleteWarehouse);
				let sqlstatementDeleteC_location = HelperService.SqlDeleteStatement('c_location', 'c_location_uu', rowData.m_warehouse_wx_c_location_uu);
				insertSuccess += await apiInterface.ExecuteSqlStatement(sqlstatementDeleteC_location);
				let sqlstatementDeleteM_locator = HelperService.SqlDeleteStatement('m_locator', 'm_warehouse_uu', rowData.m_warehouse_wx_m_warehouse_uu);
				insertSuccess += await apiInterface.ExecuteSqlStatement(sqlstatementDeleteM_locator);
				if (insertSuccess) {
					// delete row pada datatable
					thisObject.datatableReference
						.row($('#bttn_row_delete' + uuidDeleted).parents('tr'))
						.remove()
						.draw();
					$('#modal_info_box').modal('hide');
	
				}
			}

		
		} catch (error) {
			apiInterface.Log(this.constructor.name, this.Delete.name, error);
		}

		return insertSuccess;
	}

}
