class FactAcctTablePortlet {

	constructor(idFragmentContent) {

	

		this.metaDataTable = new MetaDataTable();

		this.datatableReference = {}; // buat reference datatable

		this.factacctService = new FactAcctService();

		this.idTable = 'fact_acct_transaction_table_portlet_fact_acct_transaction_table';
		this.idFragmentContent = idFragmentContent;
		this.Init();
	}



	Init() {
		let thisObject = this;
		try {
			// buat reference, karena this dalam jquery berarti element dalam jquery
			let allHtml = `
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head kt-portlet__head--lg">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon">
						<i class="kt-font-brand flaticon2-line-chart"></i>
					</span>
					<h3 class="kt-portlet__head-title">
						Detail Jurnal
					</h3>
				</div>			
			</div>
			<div class="kt-portlet__body">
				<table class="table table-striped- table-bordered table-hover table-checkable" id="`+ thisObject.idTable + `">
		
				</table>
			</div>
		</div>
					
			`;


			this.metaDataTable.ad_org_uu = localStorage.getItem('ad_org_uu');
			$('#' + thisObject.idFragmentContent).html(allHtml);
			thisObject.CreateTable();



		} catch (error) {
			apiInterface.Log(this.constructor.name, this.Init.name, error);
		}


	}

	EventHandler() {
		let thisObject = this;

		thisObject.datatableReference.off('draw');
		thisObject.datatableReference.on('draw', function () {
			thisObject.factacctService.FeedRows(thisObject.datatableReference);

		});


	}


	CreateTable() {
		let thisObject = this;
		var table = $('#' + thisObject.idTable);

		thisObject.factacctService.FindAll(thisObject.metaDataTable).then(function (factacctCMTList) {

			thisObject.datatableReference = table.DataTable({
				responsive: true,
				rowId: 'fact_acct_wx_fact_acct_uu',
				data: factacctCMTList,
				scrollX: true,
				"order": [[7, 'desc']],
				columns: [
					{ title: 'Tipe Akun', data: 'fact_acct_wx_accounttype' },
					{ title: 'Element', data: 'fact_acct_wx_c_elementvalue' },
					{ title: 'Debit' },
					{ title: 'Kredit' },
					{ title: 'Dokumen', data: 'document_type' },
					{ title: 'No Dokumen', data: 'document_no' },
					{ title: 'Status', data: 'fact_acct_wx_docstatus' },
					{ title: 'Tanggal', data: 'fact_acct_wx_updated' }
				],
				columnDefs: [
					{
						targets: 2,
						title: 'Debit',
						orderable: false,
						render: function (data, type, full, meta) {
							return new Intl.NumberFormat().format(full.fact_acct_wx_amtacctdr);
						},
					},
					{
						targets: 3,
						title: 'Kredit',
						orderable: false,
						render: function (data, type, full, meta) {
							return new Intl.NumberFormat().format(full.fact_acct_wx_amtacctcr);
						},
					},

					{
						targets: -1,
						title: 'Tanggal',
						orderable: true,
						render: function (data, type, full, meta) {
							return HelperService.ConvertTimestampToLocateDateString(full.fact_acct_wx_updated);
						},
					},

				]

			});
			//	thisObject.EventHandler() 
			//thisObject.factacctService.FeedRows(thisObject.metaDataTable, thisObject.datatableReference);
			thisObject.factacctService.FeedRows(thisObject.datatableReference);
		});







	}

	RunFeedRows() {
		let thisObject = this;
		thisObject.factacctService.FeedRows(thisObject.datatableReference);
	}

	UpdateRow(rowData){

		let thisObject = this;

		thisObject.datatableReference.row.add(rowData).draw();
	}

}
