class ProductpricePage {

	/**
	 * 
	 * @param {m_product_uu} mProductParentUu product yang akan diproduksi
	 * @param {string} idFragmentContent name id fragment content untuk meletakkan html content dari productbom_page
	 */
	constructor(mProductParentUu, idFragmentContent) {


		this.mProductParentUu = mProductParentUu;

		this.viewModel = new ProductpriceViewModel();

		this.metaDataTable = new MetaDataTable();


		this.datatableReference = {}; // buat reference datatable

		this.productpriceService = new ProductpriceService();
		
		this.idFragmentContent = idFragmentContent;
	/*	this.idFormModal = 'm_productprice_form_wx_' + mProductParentUu;
		this.idFormValidation = 'm_productprice_form_validation_' + mProductParentUu;
		this.classNameFormControl = 'm-productprice-form-control';*/
		this.classShowForm = 'productprice_page_productprice_form';
	
		this.idTable = 'wx_productprice_table_' + mProductParentUu;
		this.idNewRecord = 'new_record_productprice_wx_' + mProductParentUu;
	
	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery


		thisObject.metaDataTable.ad_org_uu = localStorage.getItem('ad_org_uu');


		let allHtml = `
		<button type="button" class="`+ thisObject.classShowForm + ` btn btn-bold btn-label-brand btn-sm"  id="` + thisObject.idNewRecord + `" name="new_record">
		<i class="la la-plus"></i>New Record</button>			
			<table class="table table-striped- table-bordered table-hover table-checkable" id="`+ thisObject.idTable + `">			
			</table>
		`;


		$('#' + thisObject.idFragmentContent).html(allHtml);



		thisObject.metaDataTable.otherFilters = [' AND m_productprice.m_product_uu = \'' + thisObject.mProductParentUu + '\'']

		var table = $('#' + thisObject.idTable);

		let columnDefDatatable = [
			{
				targets: -1,
				title: 'Actions',
				orderable: false,
				render: function (data, type, full, meta) {
					return `
					<button id="bttn_row_edit`+ full.m_productprice_wx_m_productprice_uu + `" name="` + full.m_productprice_wx_m_productprice_uu + `" type="button"  class="` + thisObject.classShowForm + ` btn btn-outline-hover-info btn-elevate btn-icon"><i class="la la-edit"></i></button>
					<button id="bttn_row_delete`+ full.m_productprice_wx_m_productprice_uu + `" value="` + full.m_pricelist_version_wx_name + `" name="` + full.m_productprice_wx_m_productprice_uu + `" type="button"   class="` + thisObject.classDeleteRow + ` btn btn-outline-hover-info btn-elevate btn-icon"> <i class="la la-trash"></i></button>
					`;
				},
			},
			{
				targets: -2,
				title: 'Harga Minimal',
				orderable: false,
				render: function (data, type, full, meta) {
					return new Intl.NumberFormat().format(full.m_productprice_wx_pricelimit);


				},
			},
			{
				targets: -3,
				title: 'Harga Standard',
				orderable: false,
				render: function (data, type, full, meta) {
					return new Intl.NumberFormat().format(full.m_productprice_wx_pricestd);


				},
			},
			{
				targets: -4,
				title: 'Harga Maksimal',
				orderable: false,
				render: function (data, type, full, meta) {
					return new Intl.NumberFormat().format(full.m_productprice_wx_pricelist);


				},
			},


		];

		thisObject.productpriceService.FindAll(thisObject.metaDataTable).then(function (productpriceCMTList) {
			// begin first table
			if (!$.fn.dataTable.isDataTable('#' + thisObject.idTable)) {
				thisObject.datatableReference = table.DataTable({
					searching: false,
					paging: false,
					responsive: true,
					data: productpriceCMTList,
					scrollX: true,
					columns: [
						{ title: 'Tipe Pricelist', data: 'm_pricelist_wx_name' },
						{ title: 'Nama', data: 'm_pricelist_version_wx_name' },
						{ title: 'Harga Maksimal', data: 'm_productprice_wx_pricelist' },
						{ title: 'Harga Standart', data: 'm_productprice_wx_pricestd' },
						{ title: 'Harga Minimal', data: 'm_productprice_wx_pricelimit' },
						{ title: 'Action' }
					],
					columnDefs: columnDefDatatable,
					"order": [[1, 'asc']],

				});


			}

			// attach event handler on the page

			thisObject.EventHandler();



		});



	}







	EventHandler() {
		let thisObject = this;

		try {

			$('.' + thisObject.classDeleteRow).off('click');
			$('.' + thisObject.classDeleteRow).on('click', function () {
				let nameDeleted = $(this).val();
				let uuid = $(this).attr('name');
				let deleteWidget = new DialogBoxDeleteWidget('Hapus Data', nameDeleted, uuid, thisObject);

			});

			$('.' + thisObject.classShowForm).off('click');
			$('.' + thisObject.classShowForm).on('click', function (e) {
				let name = $(this).attr('name');

				if (name !== 'new_record') {

					let dataEdited = thisObject.datatableReference.row($('#bttn_row_edit' + name).parents('tr')).data();

					thisObject.viewModel = HelperService.ConvertRowDataToViewModel(dataEdited);

				} else {

					thisObject.viewModel = new ProductpriceViewModel();
					thisObject.viewModel.m_productprice.m_product_uu = thisObject.mProductParentUu;
				}
				let productPriceForm = new ProductpriceForm(thisObject.viewModel, thisObject.datatableReference);



			});
			thisObject.datatableReference.off('draw');
			thisObject.datatableReference.on('draw', function () {

				thisObject.EventHandler();
			});

		} catch (error) {
			apiInterface.Log(this.constructor.name, this.EventHandler.name, error);
		}




	}

	/**
	 * Delete pada table, sebenarnya hanya menandai wx_isdeleted = 'Y'
	 * @param {*} dataObject dataObject pada datatable 
	 */
	async Delete(uuidDeleted) {
		let insertSuccess = 0;
		let thisObject = this;
		try {
			let sqlstatementDeleteBankaccount = HelperService.SqlDeleteStatement('m_productprice', 'm_productprice_uu', uuidDeleted);
			insertSuccess += await apiInterface.ExecuteSqlStatement(sqlstatementDeleteBankaccount);

			if (insertSuccess) {
				// delete row pada datatable
				thisObject.datatableReference
					.row($('#bttn_row_delete' + uuidDeleted).parents('tr'))
					.remove()
					.draw();


			}
		} catch (error) {
			apiInterface.Log(this.constructor.name, this.Delete.name, error);
		}

		return insertSuccess;
	}





}
