/**
 * Movementqty = masuknya barang sesuai dengan jumlah basedUom
 * Qtyentered = masuknya barang sesuai dengan uom yang tertulis
 */
class ProductionlinePage {

	/**
	 * 
	 * @param {ProductionCMT} productionCMT parent productionViewModel
	 * @param {string} idFragmentContent name id fragment content untuk meletakkan html content dari productbom_page
	 */
	constructor(productionCMT, idFragmentContent) {

		this.productionCMT = productionCMT;


		this.viewModel = new ProductionComponentModel();

		this.metaDataTable = new MetaDataTable();


		this.datatableReference = {}; // buat reference datatable

		this.productionlineService = new ProductionlineService();

		this.idFragmentContent = idFragmentContent;
		this.idFormModal = 'm_productionline_form_wx_';
		this.idFormValidation = 'm_productionline_form_validation_';

		
		this.idTable = 'wx_productionline_table_' + productionCMT.m_production_wx_m_production_uu;
		this.idBtnGenerateProductionline = 'generate_record_productionline_wx_' + productionCMT.m_production_wx_m_production_uu;
		this.idBtnNewRecord = 'new_record_productionline_wx_' + productionCMT.m_production_wx_m_production_uu;
	
		this.idShowFormProductionline = 'wx-btn-show-form-productionline';
		this.classDeleteRow = 'productionline_page_bttn_delete_row';
	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery


		thisObject.metaDataTable.ad_org_uu = localStorage.getItem('ad_org_uu');

		let buttonNewRecordHtml = '';
		if (thisObject.productionCMT.m_production_wx_docstatus === 'DRAFT') {
			buttonNewRecordHtml += `<button type="button" name="new_record" class="` + thisObject.idShowFormProductionline + ` btn btn-bold btn-label-brand btn-sm"  id="` + thisObject.idBtnNewRecord + `"> <i class="la la-plus"></i>New Record</button>`;
			buttonNewRecordHtml += `<button type="button" class="btn btn-bold btn-label-brand btn-sm"  id="` + thisObject.idBtnGenerateProductionline + `"> <i class="la la-plus"></i>Generate Item Barang</button>`;
		}

		let allHtml =
			buttonNewRecordHtml +
			`<table class="table table-striped- table-bordered table-hover table-checkable" id="` + thisObject.idTable + `">			
			</table>
	`;

		$('#' + thisObject.idFragmentContent).html(allHtml);


		thisObject.CreateDataTable();
		// attach event handler on the page

	

	}


	/**
	 * Delete pada table, sebenarnya hanya menandai wx_isdeleted = 'Y'
	 * @param {*} dataObject dataObject pada datatable 
	 */
	async Delete(uuidDeleted) {
		let thisObject = this;
		let insertSuccess = 0;
		try {
			let sqlstatementDeleteBankaccount = HelperService.SqlDeleteStatement('m_productionline', 'm_productionline_uu', uuidDeleted);
			insertSuccess += await apiInterface.ExecuteSqlStatement(sqlstatementDeleteBankaccount);

			if (insertSuccess) {
				// delete row pada datatable
				thisObject.datatableReference
					.row($('#bttn_row_delete' + uuidDeleted).parents('tr'))
					.remove()
					.draw();

			}
		} catch (error) {
			apiInterface.Log(this.constructor.name, this.Delete.name, error);
		}

		return insertSuccess;
	}





	EventHandler() {
		let thisObject = this;


		$('#' + thisObject.idBtnGenerateProductionline).off("click");
		$('#' + thisObject.idBtnGenerateProductionline).on("click", function (e) {
			let lengthRow = thisObject.datatableReference.data().length;
			thisObject.productionlineService.GenerateProductionline(thisObject.productionCMT, lengthRow)
				.then(function () {

					thisObject.Init();

				});

		});

		$('.' + thisObject.idShowFormProductionline).off("click");
		$('.' + thisObject.idShowFormProductionline).on("click", function (e) {

			let attrName = $(this).attr('name');
			let productionlineViewModel = new ProductionlineComponentModel();
			if (attrName !== 'new_record') {
				let dataEdited = thisObject.datatableReference.row($('#bttn_row_edit' + attrName).parents('tr')).data();
				productionlineViewModel = HelperService.ConvertRowDataToViewModel(dataEdited);
			}
			let productionlineFormPage = new ProductionlineForm(productionlineViewModel, thisObject);



		});

		$('.' + thisObject.classDeleteRow).off('click');
		$('.' + thisObject.classDeleteRow).on('click', function () {
			let nameDeleted = $(this).val();
			let uuid = $(this).attr('name');
			let deleteWidget = new DialogBoxDeleteWidget('Hapus Data', nameDeleted, uuid, thisObject);

		});




		$('.wx-format-money').mask('#,##0', { reverse: true });

		thisObject.datatableReference.off('draw');
		thisObject.datatableReference.on('draw', function () {
			thisObject.EventHandler();
		});

	}

	CreateDataTable() {

		let thisObject = this;

		thisObject.metaDataTable.otherFilters = [' AND m_productionline.m_production_uu = \'' + thisObject.productionCMT.m_production_wx_m_production_uu + '\'']

		var table = $('#' + thisObject.idTable);

		let columnDefDatatable = [];
		let columnsTable = [
			{ title: 'No', data: 'm_productionline_wx_line' },
			{ title: 'Produk', data: 'm_product_wx_name' },
			{ title: 'Satuan', data: 'c_uom_wx_name' },
			{ title: 'Jumlah', data: 'm_productionline_wx_movementqty' }
		];


		if (thisObject.productionCMT.m_production_wx_docstatus === 'DRAFT') {
			columnsTable.push({ title: 'Action' });
			columnDefDatatable.push({
				targets: -1,
				title: 'Actions',
				orderable: false,
				render: function (data, type, full, meta) {
					return `
					<button id="bttn_row_edit`+ full.m_productionline_wx_m_productionline_uu + `" name="` + full.m_productionline_wx_m_productionline_uu + `" type="button"  class="` + thisObject.idShowFormProductionline + ` btn btn-outline-hover-info btn-elevate btn-icon"><i class="la la-edit"></i></button>
					<button id="bttn_row_delete`+ full.m_productionline_wx_m_productionline_uu + `" value="` + full.m_product_wx_name + `" name="` + full.m_productionline_wx_m_productionline_uu + `" type="button" class="`+thisObject.classDeleteRow + ` btn btn-outline-hover-info btn-elevate btn-icon"> <i class="la la-trash"></i></button>
					`;
				},
			});



		}




		thisObject.productionlineService.FindProductionlineCMTListByMProductionUu(thisObject.productionCMT.m_production_wx_m_production_uu)
			.then(function (productionCMTList) {

				// begin first table
				if (!$.fn.dataTable.isDataTable('#' + thisObject.idTable)) {
					thisObject.datatableReference = table.DataTable({
						searching: false,
						paging: false,
						responsive: true,
						data: productionCMTList,
						scrollX: true,
						columns: columnsTable,
						columnDefs: columnDefDatatable,
						"order": [[0, 'asc']],

					});


				}
				thisObject.EventHandler();

			});


	}



}
