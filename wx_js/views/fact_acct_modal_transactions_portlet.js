class FactAcctModalTransactionsPortlet {

	constructor(idFragmentContent, factacctTablePortlet) {

		this.factAcctService = new FactAcctService();
		this.bankaccountService = new BankaccountService();
		this.idFragmentContent = idFragmentContent;
		this.factacctTablePortlet = factacctTablePortlet;

		this.labelForm = 'Pengambilan / Penyetoran Modal Usaha';


		this.idFormModal = 'modal_transaction_form_wx_';
		this.idFormValidation = 'modal_transaction_form_validation_';
		this.classNameFormControl = 'modal_transaction_form-control';

		this.selectorClassNameFormControl = '.' + this.classNameFormControl;
		this.idButtonFormSubmit = 'modal_transaction_form_submit_wx_';

		this.idAmountDebetKredit = 'modal_transaction_amount_debet_kredit';
		this.idPengambilanPenyetoran = 'modal_transaction_debet_credit_button';
		this.classDebetKreditOption = 'modal_transaction_debet_credit_options';
		this.idBankaccountOptions = 'modal_transaction_bankaccount_options';
		this.validation = {
			id_form: this.idFormValidation,

		};
		this.validation[this.idBankaccountOptions] = { required: true };
		this.validation[this.idAmountDebetKredit] = { required: true };


		this.Init();

	}


	async	Init() {

		try {

			let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery
			let metadataTable = new MetaDataTable()
			metadataTable.ad_org_uu = localStorage.getItem('ad_org_uu');
			let htmlForm = await thisObject.GeneratePortlet();

			$('#' + thisObject.idFragmentContent).html(htmlForm);


			thisObject.EventHandler();

			let bankaccountCMTList = await thisObject.bankaccountService.FindAll(metadataTable);

			_.forEach(bankaccountCMTList, function (bankaccountCMT) {
				let text = bankaccountCMT.c_bankaccount_wx_name + " - " + bankaccountCMT.c_bankaccount_wx_accountno;
				let id = bankaccountCMT.c_bankaccount_wx_c_bankaccount_uu;
				let newOption = new Option(text, id, false, false);
				// Append it to the select
				$('#' + thisObject.idBankaccountOptions).append(newOption).trigger('change');

			});


		} catch (error) {
			apiInterface.Log(this.constructor.name, this.Init.name, error);
		}


	}



	async	GeneratePortlet() {
		let thisObject = this;




		let portletHtml = `
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand flaticon2-line-chart"></i>
				</span>
				<h3 class="kt-portlet__head-title">
					`+ thisObject.labelForm + `
				</h3>
			</div>
			<div class="kt-portlet__head-toolbar">
				<div class="kt-portlet__head-wrapper">
					<div class="kt-portlet__head-actions">
					</div>
				</div>
			</div>
		</div>
		<form id="`+ thisObject.idFormValidation + `" class="kt-form">
			<div class="kt-portlet__body">
				<div class="form-group row">
					<label class="col-form-label col-lg-3 col-sm-12">Pilih Bank/Cash</label>
					<div class=" col-lg-4 col-md-9 col-sm-12">
						<select class="form-control kt-select2" id="`+ thisObject.idBankaccountOptions + `" name="param">
							
						</select>
					</div>
				</div>	
				<div class="form-group ">
				
					<div class="input-group">
						<div class="input-group-prepend">
							<button type="button" name="Debet" id="`+ thisObject.idPengambilanPenyetoran + `" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Penyetoran Modal
							</button>
							<div class="dropdown-menu">
								<a class="`+ thisObject.classDebetKreditOption + ` dropdown-item" name="Debet" dropdown-item" href="#">Penyetoran Modal</a>
								<a class="`+ thisObject.classDebetKreditOption + ` dropdown-item" name="Kredit" dropdown-item" href="#">Pengambilan Modal</a>
							</div>
						</div>
						<input id="`+ thisObject.idAmountDebetKredit + `" type="text"
						class="` + thisObject.classNameFormControl + ` wx-format-money  form-control"
						aria-describedby="emailHelp">					
					</div>
					
				</div>	
			</div>
			<div class="kt-portlet__foot">
				<div class="kt-form__actions">
					<button id="`+ thisObject.idButtonFormSubmit + `" type="submit" class="btn btn-primary">Submit</button>
					<div id="`+ thisObject.idNotification + `"></div>
				</div>
			</div>
		</form>
	</div>
		`;
		return portletHtml;

	}


	EventHandler() {
		let thisObject = this;

		try {
			$('#' + thisObject.idFormValidation).off('submit');
			$('#' + thisObject.idFormValidation).on('submit', function (e) {
				e.preventDefault();
				thisObject.Save();

			});
			$('#' + thisObject.idRefreshButton).off('click');
			$('#' + thisObject.idRefreshButton).on('click', function (e) {

				thisObject.Init();

			});

			$('#' + thisObject.idFormValidation + ' .' + thisObject.classDebetKreditOption).off('click');
			$('#' + thisObject.idFormValidation + ' .' + thisObject.classDebetKreditOption).on('click', function (e) {
				e.preventDefault();

				let debetOrKredit = $(this).attr('name');
				let text = $(this).text();

				$('#' + thisObject.idPengambilanPenyetoran).attr('name', debetOrKredit);
				$('#' + thisObject.idPengambilanPenyetoran).text(text);

			});


			$('#' + thisObject.idBankaccountOptions).select2();


			$('.wx-format-money').mask('#,##0', { reverse: true });

		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.EventHandler.name, error);
		}

	}





	/**
		 *
		 * c_doctype_id =10003 --> Penyetoran Modal Usaha
		 * c_doctype_id = 10004 --> Pengambilan Modal Usaha
	
		 */
	async Save() {
		let thisObject = this;

		try {
			let bankstatementService = new BankstatementService();
			let factAcctService = new FactAcctService();

			let insertSuccess = 0;
			if (HelperService.CheckValidation(thisObject.validation)) {

				let dataSelect = $('#' + thisObject.idBankaccountOptions).select2('data');
				let c_bankaccount_uu = dataSelect[0].id;
				let db_cr = $('#' + thisObject.idPengambilanPenyetoran).attr('name');

				let amount = numeral($('#' + thisObject.idAmountDebetKredit).val()).value();
				let c_doctype_id = null;
				if (db_cr === 'Debet') {
					c_doctype_id = 10003;
					//DR
					//11112112 - Bank IDR Dalam Transit
					insertSuccess += await factAcctService.SqlInsertFactAcct('11112112', '', '', amount, null);

					//CR
					//Equity 
					insertSuccess += await factAcctService.SqlInsertFactAcct('31011010', '', '', null, amount);

				} else {
					c_doctype_id = 10004;
					//DR
					//Equity 
					insertSuccess += await factAcctService.SqlInsertFactAcct('31011010', '', '', amount, null);

					//CR
					//11112112 - Bank IDR Dalam Transit
					insertSuccess += await factAcctService.SqlInsertFactAcct('11112112', '', '', null, amount);


				}



				insertSuccess += await bankstatementService.CreateBankStatement(c_bankaccount_uu, null, null, null, amount, c_doctype_id, 'COMPLETED');



				if (insertSuccess > 0) {
					let success = `
					<div class="alert alert-success" role="alert">
						<strong>Pengambilan/Penyetoran Modal berhasil</strong>Check Current Balance Bank, atau laporan neraca.
					</div>`;
					let bttnRefresh = `
					<button id="`+ thisObject.idRefreshButton + `" type="button" class="btn btn-primary">Refresh Form</button>
			
					`;
					$('#' + thisObject.idNotification).html(success);
					$('#' + thisObject.idButtonFormSubmit).replaceWith(bttnRefresh);
					thisObject.EventHandler();

					//let fact_acct = await factAcctService.FindFactAcctUu(fact_acct_uu);
					//thisObject.factacctTablePortlet.UpdateRow(fact_acct);

				}
			}

		} catch (err) {
			apiInterface.Log(this.constructor.name, this.Save.name, err);
		}
	}



}
