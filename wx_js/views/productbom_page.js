class ProductBomPage {

	/**
	 * 
	 * @param {m_product_uu} mProductParentUu product yang akan diproduksi
	 * @param {string} idFragmentContent name id fragment content untuk meletakkan html content dari productbom_page
	 */
	constructor(mProductParentUu, idFragmentContent) {



		this.mProductParentUu = mProductParentUu;

		this.viewModel = new ProductBomViewModel();

		this.metaDataTable = new MetaDataTable();


		this.datatableReference = {}; // buat reference datatable

		this.productBomService = new ProductBomService();
		

		this.idFragmentContent = idFragmentContent;
	
		this.classButtonShowForm = 'm_product_bom_page_bttn_show_form';
	

	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery

	
		let idTable = 'wx_product_bom_table_' + thisObject.mProductParentUu;

		let idNewRecord = 'new_record_product_bom_wx_' + thisObject.mProductParentUu;

		let allHtml = `
		<button type="button" class="`+ thisObject.classButtonShowForm + `  btn btn-bold btn-label-brand btn-sm"  id="` + idNewRecord + `" name="new_record">
		<i class="la la-plus"></i>New Record</button>			
			<table class="table table-striped- table-bordered table-hover table-checkable" id="`+ idTable + `">			
			</table>
		`;

		$('#' + thisObject.idFragmentContent).html(allHtml);


		//thisObject.EventHandler();
		this.metaDataTable.ad_org_uu = localStorage.getItem('ad_org_uu');

		this.metaDataTable.otherFilters = [' AND m_product_bom.m_product_uu = \'' + thisObject.mProductParentUu + '\'']



		var table = $('#' + idTable);

		thisObject.productBomService.FindAll(thisObject.metaDataTable).then(function (productBomCMTList) {
			// begin first table
			if (!$.fn.dataTable.isDataTable('#' + idTable)) {
				thisObject.datatableReference = table.DataTable({
					searching: false,
					paging: false,
					responsive: true,
					data: productBomCMTList,
					scrollX: true,
					columns: [
						{ title: 'No', data: 'm_product_bom_wx_line' },
						{ title: 'Nama', data: 'm_product_wx_name' },
						{ title: 'Sku', data: 'm_product_wx_sku' },
						{ title: 'Upc', data: 'm_product_wx_upc' },
						{ title: 'Satuan', data: 'c_uom_wx_name' },
						{ title: 'Jumlah', data: 'm_product_bom_wx_bomqty' },
						{ title: 'Action' }
					],
					columnDefs: [
						{
							targets: -1,
							title: 'Actions',
							orderable: false,
							render: function (data, type, full, meta) {
								return `
					<button id="bttn_row_edit`+ full.m_product_bom_wx_m_product_bom_uu + `" name="` + full.m_product_bom_wx_m_product_bom_uu + `" type="button"    class="` + thisObject.classButtonShowForm + `   btn btn-outline-hover-info btn-elevate btn-icon"><i class="la la-edit"></i></button>
					<button id="bttn_row_delete`+ full.m_product_bom_wx_m_product_bom_uu + `" value="` + full.m_product_wx_name + `" name="` + full.m_product_bom_wx_m_product_bom_uu + `" type="button"    class="` + thisObject.classDeleteRow + ` btn btn-outline-hover-info btn-elevate btn-icon"> <i class="la la-trash"></i></button>
					`;
							},
						},
					],
					"order": [[0, 'asc']],

				});


			}

			thisObject.EventHandler();

		});







	}





	

	EventHandler() {

		try {
			let thisObject = this;

			$('.' + thisObject.classDeleteRow).off('click');
			$('.' + thisObject.classDeleteRow).on('click', function () {
				let nameDeleted = $(this).val();
				let uuid = $(this).attr('name');
				let deleteWidget = new DialogBoxDeleteWidget('Hapus Data', nameDeleted, uuid, thisObject);
	
			});


			$('.' + thisObject.classButtonShowForm).off('click');
			$('.' + thisObject.classButtonShowForm).on('click', function (e) {
				let name = $(this).attr('name');
				let lengthRow = thisObject.datatableReference.data().length;
				if (name !== 'new_record') {

					
					let dataEdited = thisObject.datatableReference.row($('#bttn_row_edit' + name).parents('tr')).data();
					thisObject.viewModel = HelperService.ConvertRowDataToViewModel(dataEdited);

				} else {

					thisObject.viewModel = new ProductBomViewModel();
					thisObject.viewModel.m_product_bom.line = ++lengthRow;
					thisObject.viewModel.m_product_bom.m_product_uu = thisObject.mProductParentUu;
				}
				let productBomForm = new ProductBomForm(thisObject.viewModel,thisObject.datatableReference);

			});

			thisObject.datatableReference.off('draw');
			thisObject.datatableReference.on('draw', function () {
	
				thisObject.EventHandler();
			});
	
			

		} catch (error) {
			apiInterface.Log(this.constructor.name, this.EventHandler.name, error);
		}

	}

/**
	 * Delete pada table, sebenarnya hanya menandai wx_isdeleted = 'Y'
	 * @param {*} dataObject dataObject pada datatable 
	 */
	async Delete(uuidDeleted) {
		let insertSuccess = 0;
		let thisObject = this;
		try {
			let sqlstatementDeleteBankaccount = HelperService.SqlDeleteStatement('m_product_bom', 'm_product_bom_uu', uuidDeleted);
			insertSuccess += await apiInterface.ExecuteSqlStatement(sqlstatementDeleteBankaccount);

			if (insertSuccess) {
				// delete row pada datatable
				thisObject.datatableReference
					.row($('#bttn_row_delete' + uuidDeleted).parents('tr'))
					.remove()
					.draw();


			}
		} catch (error) {
			apiInterface.Log(this.constructor.name, this.Delete.name, error);
		}

		return insertSuccess;
	}


}
