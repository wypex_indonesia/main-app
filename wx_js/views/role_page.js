class RolePage {

	/**
	 * 
	
	 * @param {*} idFragmentContent  // IdControl untuk meletakkan View Orderpage ex>. #fragmentContent
	 */
	constructor(idFragmentContent) {

		this.titlePage = 'Hak Akses Role';

		this.viewModel = new RoleComponentModel();

		this.idTable = 'wx_ad_role_table_';
		this.idFragmentContent = idFragmentContent;
		this.roleService = new RoleService();
		this.metaDataTable = new MetaDataTable();
		this.datatableReference = {}; // buat reference datatable
		this.idShowForm = 'wx-btn-show-form-role';
		this.idFormFragmentContent = "wx_role_form_content";


		this.columnsDataTable =
			[
				{
					"className": 'details-control',
					"orderable": false,
					"data": null,
					"defaultContent": '<button  type="button" class="btn btn-outline-hover-info btn-icon"><i class="la la-plus-square-o"></i></button>'
				},
				{ title: 'Nama Role', data: 'ad_role_wx_name' },
				{ title: 'Action' }
			];

		this.Init();

	}





	Init() {

		// buat reference, karena this dalam jquery berarti element dalam jquery
		let thisObject = this;

		try {
			let allHtml = `
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head kt-portlet__head--lg">
			<div  class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
			<i class="kt-font-brand flaticon2-line-chart"></i>
			</span>
			<h3 class="kt-portlet__head-title">
		 	`+ thisObject.titlePage + `
			</h3>
			</div>
			<div  class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-wrapper">
			<div class="kt-portlet__head-actions">
			<button type="button" class="` + thisObject.idShowForm + ` btn btn-bold btn-label-brand btn-sm"  name="new_record" >
			<i class="la la-plus"></i>New Record</button>	
			
			</div>
			</div>
			</div>
			</div>
			<div class="kt-portlet__body" >
			<table class="table table-striped- table-bordered table-hover table-checkable" id="`+ thisObject.idTable + `">
			
			</table>
		</div>
		`;


			this.metaDataTable.ad_org_uu = localStorage.getItem('ad_org_uu');

			$('#' + thisObject.idFragmentContent).html(allHtml);


			var table = $('#' + thisObject.idTable);
			let columnDefDatatable = [
				{
					targets: -1,
					title: 'Actions',
					orderable: false,
					render: function (data, type, full, meta) {

						let htmlAction = `<button id="bttn_row_edit` + full.ad_role_wx_ad_role_uu + `" name="` + full.ad_role_wx_ad_role_uu + `" type="button" class="` + thisObject.idShowForm + ` btn btn-outline-hover-info btn-elevate btn-icon"><i class="la la-edit"></i></button>`;

						return htmlAction;
					},
				},

			];

			thisObject.roleService.FindAll(thisObject.metaDataTable).then(function (roleCMTList) {

				// begin first table
				thisObject.datatableReference = table.DataTable({
					responsive: true,
					data: roleCMTList,
					scrollX: true,
					columns: thisObject.columnsDataTable,
					columnDefs: columnDefDatatable,
					"order": [[1, 'asc']],
					createdRow: function (row, data, index) {
						if (data.extn === '') {
							var td = $(row).find("td:first");
							td.removeClass('details-control');
						}
					},
					rowCallback: function (row, data, index) {
						//console.log('rowCallback');
					}

				});
				// attach event handler on the page

				thisObject.PageEventHandler();
			});


		} catch (error) {
			apiInterface.Log(this.constructor.name, this.Init.name, error);
		}


	}

	PageEventHandler() {
		let thisObject = this;
		try {
			// Add event listener for opening and closing details
			$('#' + thisObject.idTable + ' tbody').off('click');
			$('#' + thisObject.idTable + ' tbody').on('click', 'td.details-control', function () {
				var tr = $(this).closest('tr');
				var row = thisObject.datatableReference.row(tr);
				let roleParent = row.data();
				if (row.child.isShown()) {
					// This row is already open - close it
					row.child.hide();
					$(this).html('<button  type="button" class="btn btn-outline-hover-info btn-icon"><i class="la la-plus-square-o"></i></button>');
				}
				else {
					let idRoleline = thisObject.prefixContentChild + roleParent.ad_role_wx_ad_role_uu;
					let rolelineFragment = `<div id="` + idRoleline + `"></div>`;
					row.child(rolelineFragment).show();

					// generate orderlinePage
					let rolelinePage = new RolelinePage(roleParent, idRoleline);
					rolelinePage.Init();

					// add reference to listReferenceOrderlinePage, agar nanti bisa dipanggil lagi di orderpage, untuk membuat

					// change menjadi button minus
					$(this).html('<button  type="button" class="btn btn-outline-hover-info btn-icon"><i class="la la-minus-square-o"></i></button>');

				}

			});

			$('.' + thisObject.idShowForm).off("click");
			$('.' + thisObject.idShowForm).on("click", function (e) {

				let attrName = $(this).attr('name');
				let roleCM = new RoleComponentModel();

				if (attrName !== 'new_record') {
					let dataEdited = thisObject.datatableReference.row($('#bttn_row_edit' + attrName).parents('tr')).data();
					roleCM = HelperService.ConvertRowDataToViewModel(dataEdited);
				}
				let roleFormPage = new RoleForm(roleCM, thisObject.datatableReference);



			});

			thisObject.datatableReference.off('draw');
			thisObject.datatableReference.on('draw', function () {
				thisObject.PageEventHandler();
			});



		} catch (err) {
			apiInterface.Log(this.constructor.name, this.PageEventHandler.name, err);
		}


	}




}

