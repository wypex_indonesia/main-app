class CartlineForm {

	/**
	 * 

	
	 * @param {CartlineComponentModel} cartlineCM
	 * @param {CartlineDataTable} datatableReference
	 * @param {IndexRow} indexRow // row tr yang di click dari cartlineDataTable
	 * 
	 */
	constructor(cartlineCM, datatableReference, indexRow) {
		this.validation = {
			pos_c_orderline_wx_qtyentered: {
				required: true
			},

			pos_c_orderline_wx_priceentered: {
				required: true
			},
		};




		this.indexRow = indexRow;

		this.viewModel = cartlineCM;

		this.posService = new PosService();

		this.idFragmentContent = 'fragment_cartline_form';
		this.selectedProduct = {}; // current product yang sedang dipilih, product_viewmodel_table
		this.idFormModal = 'c_orderline_form_wx_';
		this.idFormValidation = 'c_orderline_form_validation_';
		this.classNameFormControl = 'c-orderline-form-control';
		this.selectorClassNameFormControl = '.' + this.classNameFormControl;


		this.idButtonFormSubmit = 'c_orderline_form_submit_wx_';
		this.idButtonFormDelete = 'c_orderline_form_delete_wx_';

		this.datatableReference = datatableReference;


		this.Init();


		
	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery

		//set idfragmentcontent
		

		let allHtml = thisObject.GenerateFormInput();

		HelperService.InsertReplaceFormModal(thisObject.idFormModal, allHtml);

		HelperService.InjectViewModelToForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.viewModel);
		thisObject.PageEventHandler();

	}



	/**
	 * 

	 */
	async	SaveUpdate() {
		let thisObject = this;


		if (HelperService.CheckValidation(thisObject.validation)) {

			let qtyentered = numeral($('#' + thisObject.idFormValidation + ' #pos_c_orderline_wx_qtyentered').val()).value();
			let priceentered = numeral($('#' + thisObject.idFormValidation + ' #pos_c_orderline_wx_priceentered').val()).value();
			thisObject.viewModel.pos_c_orderline.qtyentered = qtyentered;
			thisObject.viewModel.pos_c_orderline.priceentered = priceentered;
			thisObject.viewModel.pos_c_orderline.qtyordered = qtyentered;
			thisObject.viewModel.pos_c_orderline.priceactual = priceentered;
			thisObject.viewModel.pos_c_orderline.linenetamt = priceentered * qtyentered;

			const timeNow = new Date().getTime();

			thisObject.viewModel.pos_c_orderline.updatedby = localStorage.getItem('ad_user_uu');

			thisObject.viewModel.pos_c_orderline.updated = timeNow;

			thisObject.viewModel.pos_c_orderline.sync_client = null;
			thisObject.viewModel.pos_c_orderline.process_date = null;

			const sqlInsert = HelperService.SqlInsertOrReplaceStatement('pos_c_orderline', thisObject.viewModel.pos_c_orderline);
			let insertSuccess = 0;
			insertSuccess += await apiInterface.ExecuteSqlStatement(sqlInsert);

			// jika sukses tersimpan 
			if (insertSuccess > 0) {


				let rowDataList = await thisObject.posService.FindCartlineCMTByCOrderlineUu(thisObject.viewModel.pos_c_orderline.c_orderline_uu);

				let rowData = {};

				if (rowDataList.length > 0) {
					rowData = rowDataList[0];
				}

				thisObject.datatableReference.row(thisObject.indexRow).data(rowData).draw();

			}
			$('#' + thisObject.idFormModal).modal('hide');

		}




	}

	GenerateFormInput() {
		let thisObject = this;

		return `
<div class="modal fade" id="` + thisObject.idFormModal + `" tabindex="-1" role="dialog"
    aria-labelledby="product-form-labelled" style="display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="product-form-labelled">Orderline Form</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <form id="`+ thisObject.idFormValidation + `" class="kt-form">
                <div class="modal-body">

                    <div class="form-group">
                        <label>Jumlah</label>
                        <input id="pos_c_orderline_wx_qtyentered" type="text"
                            class="` + thisObject.classNameFormControl + `  wx-format-money form-control" aria-describedby="emailHelp">
                        <span class="form-text text-muted">Jumlah</span>
                    </div>
                    <div class="form-group">
                        <label>Harga</label>
                        <input id="pos_c_orderline_wx_priceentered" type="text"
                            class="` + thisObject.classNameFormControl + ` wx-format-money form-control" aria-describedby="emailHelp">
                        <span class="form-text text-muted">Harga</span>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="`+ thisObject.idButtonFormDelete + `"
                        name="` + thisObject.viewModel.pos_c_orderline.c_orderline_uu + `" type="button"
                        class="btn btn-secondary">Delete</button>
                    <button id="`+ thisObject.idButtonFormSubmit + `" type="submit" class="btn btn-primary">Save
                        changes</button>
                </div>
            </form>
        </div>
    </div>
</div>		
		`;
	}

	PageEventHandler() {
		let thisObject = this;



		$('#' + thisObject.idFormValidation).off('submit');
		$('#' + thisObject.idFormValidation).on('submit', function (e) {
			thisObject.SaveUpdate();
			e.preventDefault();

		});
		$('#' + thisObject.idButtonFormDelete).off('click');
		$('#' + thisObject.idButtonFormDelete).on('click', function () {
			let uuid = $(this).attr('name');
			thisObject.Delete(uuid);

		});

		$('#'+thisObject.idFormModal).off('shown.bs.modal');
		$('#'+thisObject.idFormModal).on('shown.bs.modal', function(){

			$('#pos_c_orderline_wx_qtyentered').focus();
		});


		$('#' + thisObject.idFormModal).modal('show');
		$('.wx-format-money').mask('#,##0', { reverse: true });
		
	}

	async	Delete(uuid) {
		let thisObject = this;

		try {
			let sqlDeleteStatement = "DELETE FROM pos_c_orderline WHERE pos_c_orderline.c_orderline_uu = '" + uuid + "'";

			let insertSuccess = 0;
			insertSuccess += await apiInterface.ExecuteSqlStatement(sqlDeleteStatement);
	
			if (insertSuccess) {
				// delete row pada datatable
				thisObject.datatableReference.row(thisObject.indexRow).remove().draw();
			}
			$('#' + thisObject.idFormModal).modal('hide');
		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.Delete.name, error);
		}
	

	}








}
