/**
 * StockOpname

 */
class CompanyPage {

	/**
	 * 	
	 * @param {*} idFragmentContent  // IdControl untuk meletakkan View Orderpage ex>. #fragmentContent
	 */
	constructor(idFragmentContent) {
		this.validation = {

			ad_org_wx_name: {
				required: true
			},
			c_location_wx_address1: {
				required: true
			},
			c_location_wx_city: {
				required: true
			},
			c_location_wx_postal: {
				required: true
			},
			c_location_wx_regionname: {
				required: true
			},

		};



		this.organizationService = new OrganizationService();
		this.organizationCM = {};
		this.classNameFormControl = 'wx-company-page-control';

		this.idButtonFormSubmit = 'wx-company-page-submit';
		this.idFragmentContent = idFragmentContent;



		this.Init();

	}


	GetValidation() {

		return this.validation();

	}




	/**
	 * Cek default m_warehouse_uu, m_pricelist_version_uu, 
	 * jika tidak ada show form untuk memilih default m_warehouse_uu, m_pricelist_version_uu
	 */
	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery

		thisObject.organizationService.FindOrganizationComponentModelByAdOrgUu(localStorage.getItem('ad_org_uu'))
			.then(function (adOrgCMTList) {
				let organizationCMT = adOrgCMTList[0];
				thisObject.organizationCM = HelperService.ConvertRowDataToViewModel(organizationCMT);
				let layoutHtml = thisObject.GenerateForm();

				$('#' + thisObject.idFragmentContent).html(layoutHtml);

				HelperService.InjectViewModelToForm('.' + thisObject.classNameFormControl, thisObject.organizationCM);

				thisObject.PageEventHandler();

			})
			;




	}

	Save() {
		let thisObject = this;


		if (HelperService.CheckValidation(thisObject.validation)) {

			thisObject.organizationCM = HelperService.GetViewModelFromForm('#' + thisObject.idFormValidation + ' .' + thisObject.classNameFormControl, thisObject.organizationCM);


			thisObject.organizationService.SaveUpdate(thisObject.organizationCM).then(function (insertSuccess) {


			});



		}


	}

	GenerateForm() {
		let thisObject = this;

		let html = `
		<div class="row">
			<div class="col-xl-12">		
			
			<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
					Data Perusahaan
					</h3>
				</div>
			</div>

			<!--begin::Form-->
			<form id="`+ thisObject.idFormValidation + `" class="kt-form">
				<div class="kt-portlet__body">
					<div class="kt-section kt-section--first">
				<h3 class="kt-section__title">Detail Perusahaan:</h3>
						<div class="kt-section__body">
							<div class="form-group">
								<label>Nama Perusahaan</label>
								<input id="ad_org_wx_name" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
								<span class="form-text text-muted">Nama Perusahaan</span>						
							</div>
							<div class="form-group">
								<label>NPWP</label>
								<input id="ad_orginfo_wx_taxid" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
								<span class="form-text text-muted">NPWP jika ada</span>						
							</div>
							<div class="form-group">
								<label>Alamat</label>
								<input id="c_location_wx_address1" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
								
							</div>
							<div class="form-group">
								<label>Kota</label>
								<input id="c_location_wx_city" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
							</div>
							<div class="form-group">
								<label>Kode pos</label>
								<input id="c_location_wx_postal" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
							</div>
							<div class="form-group">
								<label>Propinsi</label>
								<input id="c_location_wx_regionname" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
							</div>
						</div>							
						
					</div>
				</div>
				<div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<div class="row">
							<div class="col-lg-3"></div>
							<div class="col-lg-6">
								<button id="`+ thisObject.idButtonFormSubmit + `" type="submit" class="btn btn-success">Submit</button>
							
							</div>
						</div>
					</div>
				</div>
			</form>

			<!--end::Form-->
			</div>
			</div>		
		</div>`;

		return html;

	}

	PageEventHandler() {
		let thisObject = this;

		$('#' + thisObject.idButtonFormSubmit).off('click');
		$('#' + thisObject.idButtonFormSubmit).on('click', function () {
			thisObject.Save();

		});

		$('#' + thisObject.idFormValidation).off('submit');
		$('#' + thisObject.idFormValidation).on('submit', function (e) {
			thisObject.Save();
			e.preventDefault();

		});
	}





}

