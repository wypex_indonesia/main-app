/**
 * CartlinePortlet merupakan cartline portlet pada pos_page.
 * defaultCartCM akan selalu dibuat atau dicari apakah masih ada CartCM , jika tidak ada maka cartCM harus dibuat
 * 
 */
class ToolbarSearchProductPortlet {

    constructor(idFragmentContent, configPosCMT, cartlinePortlet, redrawSearchProductTable) {

        this.idFragmentContent = idFragmentContent;
        this.posService = new PosService();



        this.defaultCartCM = null;
        this.defaultPosCMT = configPosCMT;

        this.idInputSearchProduct = 'input_pos_search_product';
        this.idShowCatalog = 'bttn_show_catalog';
        this.cartlinePortlet = cartlinePortlet;
        this.redrawSearchProductTable = redrawSearchProductTable;
    }

    async  Init() {

        let thisObject = this;


        let htmlTable = `
		<div class="kt-portlet">
			<div class="kt-portlet__body">
				<div class="form-group ">			
				<div class="input-group input-group-lg">				
					<input type="text" id="`+ thisObject.idInputSearchProduct + `" class="form-control" placeholder="Scan barcode / ketik sku/upc atau nama produk (ALT+Q)" aria-describedby="basic-addon1">
					<div class="input-group-prepend">
						<span class="input-group-text" id="`+ thisObject.idShowCatalog + `"><i class="la la-list kt-font-brand"></i>(ALT+W)</span>
					</div>
				</div>
				<span class="form-text text-muted">Scan barcode sku/upc atau ketik nama produk</span>
				</div>			
			</div>
		</div>
		`;
        $('#' + thisObject.idFragmentContent).html(htmlTable);


        this.EventHandler();

    }


    ClearValueAndFocus() {
        let thisObject = this;
        $('#' + thisObject.idInputSearchProduct).val('');
        $('#' + thisObject.idInputSearchProduct).focus();

    }


    EventHandler() {
        let thisObject = this;
        $('#' + thisObject.idShowCatalog).off('click');
        $('#' + thisObject.idShowCatalog).on('click', function () {

            thisObject.redrawSearchProductTable('');

        });
        $('#' + thisObject.idInputSearchProduct).off('input');
        $('#' + thisObject.idInputSearchProduct).on('input', function () {
            let searchValue = $(this).val().trim();

            if (searchValue.length >= 3) {
                let metaDataTableSearch = new MetaDataTable();
                metaDataTableSearch.ad_org_uu = localStorage.getItem('ad_org_uu');
                metaDataTableSearch.search = searchValue;

                thisObject.posService.FindSearchProductCMTList(thisObject.defaultPosCMT.m_pricelist_version_wx_m_pricelist_version_uu, metaDataTableSearch)
                    .then(function (productCMTList) {
                        if (productCMTList.length > 0) {
                            // lebih besar dari satu munculkan dalam search product table
                            if (productCMTList.length > 1) {
                                thisObject.redrawSearchProductTable(searchValue);
                            }

                            //langsung masuk dalam cart
                            if (productCMTList.length === 1) {
                                let productPriceCMTSelected = productCMTList[0];
                                thisObject.cartlinePortlet.AddCartline(productPriceCMTSelected).then(function (inserted) {
                                    thisObject.ClearValueAndFocus();
                                });


                            }

                        }


                    });


            }

        });

        thisObject.HotKeysEvent();
    }




    HotKeysEvent() {
        let thisObject = this;



        hotkeys('alt+q,alt+w', function (event, handler) {
            // Prevent the default refresh event under WINDOWS system
            switch (handler.key) {
                case 'alt+q':
                    thisObject.ClearValueAndFocus();
                    event.preventDefault();
                    return false;
                    break;
                case 'alt+w':
                    thisObject.redrawSearchProductTable('');
                    event.preventDefault();
                    return false;
                    break;
                default:
                    break;
            }

        });

        hotkeys.filter = function(event){
            return true;
          }
    }







}