/**
 * StockOpname

 */
class LoginPage {

	/**
	 * 	
	 * @param {*} idFragmentContent  // IdControl untuk meletakkan View Orderpage ex>. #fragmentContent
	 */
    constructor(idFragmentContent) {
        this.validation = {

            ad_user_wx_phone: {
                required: true
            },

            ad_user_wx_password: {
                required: true
            },


        };


        this.userCM = new UserComponentModel();

        this.userService = new UserService();
        this.securityService = new SecurityService();

        this.classNameFormControl = 'wx-login-form-control';
        this.idFormValidation = 'wx-login-form';
        this.idButtonFormSubmit = 'wx-login-form-submit';
        this.idFragmentContent = idFragmentContent;
        this.idAlertLogin = 'wx-login-form-alert';

        this.Init();

    }




	/**
	 * Cek default m_warehouse_uu, m_pricelist_version_uu, 
	 * jika tidak ada show form untuk memilih default m_warehouse_uu, m_pricelist_version_uu
	 */
    Init() {
        let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery


        let layoutHtml = this.GenerateForm();

        $('#' + thisObject.idFragmentContent).html(layoutHtml);

        thisObject.PageEventHandler();
        //let posConfig = new DefaultPosConfigForm(thisObject.idFragmentPos,new PosConfigComponentModel());

    }

    Login() {
        let thisObject = this;

        if (HelperService.CheckValidation(thisObject.validation)) {

            thisObject.userCM = HelperService.GetViewModelFromForm('#' + thisObject.idFormValidation + ' .' + thisObject.classNameFormControl, thisObject.userCM);

            // cek jika login benar
            thisObject.securityService.Login(thisObject.userCM.ad_user.phone,
                thisObject.userCM.ad_user.password).then(function (userCMT) {

                    if (userCMT) {
                        localStorage.setItem('ad_user_uu', userCMT.ad_user_wx_ad_user_uu);
                        localStorage.setItem('ad_role_uu', userCMT.ad_role_wx_ad_role_uu);
                        apiInterface.SetLocalStorage().then(function(result){});
                        let mainPage = new MainPage();

                    } else {

                        let alertHtml = `
				<div  class="alert alert-danger" role="alert">
					<strong>Login Gagal!</strong> Check password atau no handphone. Hubungi administrator untuk reset password.
				</div>`;
                        //set alert login gagal
                        $('#' + thisObject.idAlertLogin).html(alertHtml);

                    }


                });



        }


    }

    GenerateForm() {
        let thisObject = this;
        let html = `
		<div class="row">
			<div class="col-xl-12">		
			
			<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
					Login
					</h3>
				</div>
			</div>

			<!--begin::Form-->
			<form id="`+ thisObject.idFormValidation + `" class="kt-form">
				<div class="kt-portlet__body">
					<div class="kt-section kt-section--first">					
						<div class="kt-section__body">							
							<div class="form-group">
								<label>No Handphone</label>
								<input id="ad_user_wx_phone" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
																
							</div>
							<div class="form-group">
								<label>Password</label>
								<input id="ad_user_wx_password" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
							</div>
						</div>
					</div>
				</div>
				<div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<div class="row">
							<div class="col-lg-3"></div>
							<div class="col-lg-6">
								<button id="`+ thisObject.idButtonFormSubmit + `" type="submit" class="btn btn-success">Login</button>
							
							</div>
						</div>
					</div>
				</div>
				<div id="`+ thisObject.idAlertLogin + `">
				
				</div> 				
			</form>

			<!--end::Form-->
			</div>
			</div>		
		</div>`;

        return html;

    }

    PageEventHandler() {
        let thisObject = this;

        $('#' + thisObject.idFormValidation).off('submit');
        $('#' + thisObject.idFormValidation).on('submit', function (e) {
            thisObject.Login();
            e.preventDefault();

        });

        $('#' + thisObject.idButtonFormSubmit).off('click');
        $('#' + thisObject.idButtonFormSubmit).on('click', function () {
            thisObject.Login();

        });
    }





}

