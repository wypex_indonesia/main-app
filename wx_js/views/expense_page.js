class ExpensePage {

	/**
	 * 
	
	 * @param {*} idFragmentContent  // IdControl untuk meletakkan View Payment ex>. #fragmentContent
	 */
	constructor(idFragmentContent) {



		this.componentModel = new ExpenseComponentModel();

		this.idTable = 'wx_payment_table_';
		this.idFragmentContent = idFragmentContent;
		this.paymentService = new PaymentService();
		this.metaDataTable = new MetaDataTable();
		this.datatableReference = {}; // buat reference datatable
		this.idShowForm = 'wx-btn-show-form-payment';
		this.idFormFragmentContent = "wx_payment_form_content";
		this.idButtonRowStatusDialog = "bttn_row_status";
		this.idButtonPrintDialog = "bttn_print_dialog";
		this.idDialogBox = 'status-dialog-box';

		this.labelNoDocument = 'No Dokumen';

		this.labelAkunBank = 'Akun Pembayaran';
		this.titlePage = 'Daftar Pengeluaran';


		this.metaDataTable.otherFilters.push(' AND c_doctype.c_doctype_id = 5002');

		this.classDialogStatus = 'expense_page_bttn_show_dialog_status';
		this.classDialogPrint = 'expense_page_bttn_show_dialog_print';


		this.columnsDataTable =
			[

				{ title: this.labelNoDocument, data: 'c_payment_wx_documentno' },
				{ title: 'Jenis Pengeluaran', data: 'c_charge_wx_name' },
				{ title: this.labelAkunBank, data: 'c_bankaccount_wx_name' },
				{ title: 'Jumlah' },
				{ title: "Status", data: 'c_payment_wx_docstatus' },
				{ title: 'Action' }
			];


		this.Init();

	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery


		let allHtml = `
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head kt-portlet__head--lg">
			<div  class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
			<i class="kt-font-brand flaticon2-line-chart"></i>
			</span>
			<h3 class="kt-portlet__head-title">
		 	`+ thisObject.titlePage + `
			</h3>
			</div>
			<div  class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-wrapper">
			<div class="kt-portlet__head-actions">
			<button type="button" class="` + thisObject.idShowForm + ` btn btn-bold btn-label-brand btn-sm"  name="new_record" >
			<i class="la la-plus"></i>New Record</button>	
			
			</div>
			</div>
			</div>
			</div>
			<div class="kt-portlet__body" >
			<table class="table table-striped- table-bordered table-hover table-checkable" id="`+ thisObject.idTable + `">
			
			</thead>
			</table>
		
		</div>
		`;


		this.metaDataTable.ad_org_uu = localStorage.getItem('ad_org_uu');


		$('#' + thisObject.idFragmentContent).html(allHtml);


		var table = $('#' + thisObject.idTable);
		let columnDefDatatable = [
			{
				targets: -1,
				title: 'Actions',
				orderable: false,
				render: function (data, type, full, meta) {


					let buttonEdit = `<button id="bttn_row_edit` + full.c_payment_wx_c_payment_uu + `" name="` + full.c_payment_wx_c_payment_uu + `" type="button"  class="` + thisObject.idShowForm + ` btn btn-outline-hover-info btn-elevate btn-icon"><i class="la la-edit"></i></button>`;
					let buttonStatus = `<button id="` + thisObject.idButtonRowStatusDialog + full.c_payment_wx_c_payment_uu + `" value="` + full.c_payment_wx_documentno + `" name="` + full.c_payment_wx_c_payment_uu + `" type="button" class="` + thisObject.classDialogStatus + ` btn btn-outline-hover-info btn-elevate btn-icon"> <i class="la la-flag-o"></i></button>`;

					let htmlAction = '';
						// jika 

						if (full.c_payment_wx_docstatus === 'CLOSED') {
							htmlAction = '';
						}


						if (full.c_payment_wx_docstatus === 'REVERSED') {
							htmlAction = '';
						}

						if (full.c_payment_wx_docstatus === 'COMPLETED') {
							htmlAction = buttonStatus;
						}
						if (full.c_payment_wx_docstatus === 'DRAFT') {
							htmlAction = buttonEdit + buttonStatus;
						}



				



					return htmlAction;
				},
			},
			{
				targets: 3,
				title: 'Jumlah',
				orderable: false,
				render: function (data, type, full, meta) {


					return new Intl.NumberFormat().format(full.c_payment_wx_payamt);
				},
			},
			{
				targets: 2,
				title: thisObject.labelAkunBank,
				orderable: false,
				render: function (data, type, full, meta) {


					return full.c_bankaccount_wx_name + ' - ' + full.c_bankaccount_wx_accountno;
				},
			},

		];


		thisObject.paymentService.FindExpenseCMT(thisObject.metaDataTable)
			.then(function (paymentCMTList) {
				// begin first table
				thisObject.datatableReference = table.DataTable({
					responsive: true,
					data: paymentCMTList,
					scrollX: true,
					columns: thisObject.columnsDataTable,
					columnDefs: columnDefDatatable,
					"order": [[1, 'asc']],
					rowCallback: function (row, data, index) {
						//console.log('rowCallback');
					}

				});

				// attach event handler on the page

				thisObject.EventHandler();


			});

	}

	EventHandler() {
		let thisObject = this;

		$('.' + thisObject.idShowForm).off("click"); //diremove sebelumnya kalau, ada , karena bisa double
		$('.' + thisObject.idShowForm).on("click", function (e) {

			let attrName = $(this).attr('name');
			let expenseCM = new ExpenseComponentModel();
			expenseCM.c_payment.documentno = new Date().getTime();
			expenseCM.c_payment.docstatus = 'DRAFT';
			if (attrName !== 'new_record') {
				let dataEdited = thisObject.datatableReference.row($('#bttn_row_edit' + attrName).parents('tr')).data();
				expenseCM = HelperService.ConvertRowDataToViewModel(dataEdited);
			}
			let paymentFormPage = new ExpenseForm(expenseCM, thisObject.datatableReference);

			//$('#' + thisObject.idFormModal).modal('show');

		});

		$('.' + thisObject.classDialogStatus).off('click');
		$('.' + thisObject.classDialogStatus).on('click', function (e) {
			let uuid = $(this).attr('name');
			let expenseCMT = thisObject.datatableReference.row($('#' + thisObject.idButtonRowStatusDialog + uuid).parents('tr')).data();
			let processDocumentFuction = function (docstatus) {

				return thisObject.UpdateStatusDialogBox(docstatus, uuid);
			};
			let dialogboxstatusWidget = new DialogBoxStatusWidget('Set Status Pengeluaran', expenseCMT.c_payment_wx_docstatus, processDocumentFuction);

		});





		thisObject.datatableReference.off('draw');
		thisObject.datatableReference.on('draw', function () {
			thisObject.EventHandler();

		});



	}

	async	UpdateStatusDialogBox(docStatus, uuid) {
		let thisObject = this;
		let insertSuccess = 0;

		try {

			let expenseCMT = thisObject.datatableReference.row($('#' + thisObject.idButtonRowStatusDialog + uuid).parents('tr')).data();
			expenseCMT.c_payment_wx_docstatus = docStatus;
			insertSuccess += await thisObject.paymentService.UpdateStatus(expenseCMT);

			if (insertSuccess) {
				let rowDataList = await thisObject.paymentService.FindExpenseCMByCPaymentUu(expenseCMT.c_payment_wx_c_payment_uu);

				let rowData = {};

				if (rowDataList.length > 0) {
					rowData = rowDataList[0];
					if (docStatus === 'DELETED') {

						thisObject.datatableReference
							.row($('#' + thisObject.idButtonRowStatusDialog + uuid).parents('tr'))
							.remove()
							.draw();
					} else {

						thisObject.datatableReference
							.row($('#' + thisObject.idButtonRowStatusDialog + uuid).parents('tr'))
							.data(rowData).draw();

					}
				}

			}
		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.UpdateStatusDialogBox.name, error);
			return insertSuccess;
		}



		return insertSuccess;




	}

}

