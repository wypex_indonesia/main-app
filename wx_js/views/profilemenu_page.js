class ProfileMenuPage {

	constructor(idFragmentContent, userCM) {

		this.userCM = userCM;

		this.idFragmentContent = idFragmentContent;

		this.idMyProfileMenu = 'my_profile_menu';

		this.idSignOut = 'signout';

	}



	Init() {
		try {
			let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery


			if (thisObject.userCM) {
				let firstLetter = thisObject.userCM.ad_user.name.charAt(0);
				let firstWordArray = thisObject.userCM.ad_user.name.split(' ');

				let topbarUser = `
					<span class="kt-header__topbar-welcome kt-hidden-mobile">Hi,</span>
					<span class="kt-header__topbar-username kt-hidden-mobile">`+ firstWordArray[0] + `</span>
					<!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
					<span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold">`+ firstLetter + `</span>
				`;

				let profileMenu = `
				<!--begin: Head -->
					<div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x" style="background-image: url(assets/media/misc/bg-1.jpg)">
						<div class="kt-user-card__avatar">					
							<!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
							<span class="kt-badge kt-badge--lg kt-badge--rounded kt-badge--bold kt-font-success">`+ firstLetter + `</span>
						</div>
						<div class="kt-user-card__name">`+
					thisObject.userCM.ad_user.name +
					`</div>					
					</div>
					<!--end: Head -->
	
					<!--begin: Navigation -->
					<div class="kt-notification">
						
						<div class="kt-notification__custom kt-space-between">
							<a href="#" id="`+ thisObject.idSignOut + `" 	class="btn btn-label btn-label-brand btn-sm btn-bold">Sign Out</a>
											
						</div>
					</div>
	
					<!--end: Navigation -->
				`;


				$('#' + thisObject.idFragmentContent + ' #topbar_user').html(topbarUser);

				$('#' + thisObject.idFragmentContent + ' #profile_menu_list').html(profileMenu);

			}

			this.EventHandler();


		} catch (err) {
			apiInterface.Log(this.constructor.name, this.Init.name, err);

		}




	}

	EventHandler() {

		try {
			let thisObject = this;
			/*	$('#'+thisObject.idMyProfileMenu).off('click');
				$('#'+thisObject.idMyProfileMenu).on('click', function (e) {
					e.preventdefault();
	
				});*/

			$('#' + thisObject.idSignOut).off('click');
			$('#' + thisObject.idSignOut).on('click', function (e) {
				e.preventDefault();
				localStorage.setItem('ad_user_uu', '');
				localStorage.setItem('ad_role_uu', '');
				apiInterface.SetLocalStorage({ad_user_uu:''}).then(function(result){});
				let mainPage = new MainPage();

			});


		} catch (err) {
			apiInterface.Log(this.constructor.name, this.EventHandler.name, err);
		}



	}

	/**
	 * Menggunakan fungsi ini karena profile menu page harus di load pertama kali ketika index di load
	 */
	Show() {
		let thisObject = this;
		if (thisObject.userCM) {
			let firstLetter = thisObject.userCM.ad_user.name.charAt(0);
			let firstWordArray = thisObject.userCM.ad_user.name.split(' ');

		
			$('#' + thisObject.idFragmentContent + ' #greeting').html('Hi,');
			$('#' + thisObject.idFragmentContent + ' .name').html(firstWordArray[0]);
			$('#' + thisObject.idFragmentContent + ' .firstletter').html(firstLetter);
			$('#signout').html('Sign Out');

		}
		//	$('#'+thisObject.idFragmentContent).show();
		thisObject.EventHandler();
	}
	Hide() {
		let thisObject = this;
		//	$('#fragment_profile_menu_page').hide();
		$('#' + thisObject.idFragmentContent + ' .name').html('Sign In');
		$('#' + thisObject.idFragmentContent + ' #greeting').html('');
		$('#' + thisObject.idFragmentContent + ' .firstletter').html('');
		$('#signout').html('Sign In');
		thisObject.EventHandler();
	}

	Clear() {
		let thisObject = this;

		$('#' + thisObject.idFragmentContent + ' #topbar_user').html('');

		$('#' + thisObject.idFragmentContent + ' #profile_menu_list').html('');
	}


}
