class ProductForm {

	constructor(productCM, datatableReference) {
		
		this.viewModel = productCM;

		this.metaDataTable = new MetaDataTable();

		this.datatableReference = datatableReference; // buat reference datatable

		this.productService = new ProductService();

		this.idFormModal = 'm_product_form_wx_';
		this.idFormValidation = 'm_product_form_validation_';
		this.classNameFormControl = 'm-product-form-control';
		this.selectorClassNameFormControl = '.' + this.classNameFormControl;
		this.idButtonFormSubmit = 'm_product_form_submit_wx_';
		this.Init();
		this.validation = {
			id_form: this.idFormValidation,
			m_product_wx_name: {
				required: true
			},
			m_product_category_wx_m_product_category_uu: {
				required: true
			},
			c_uom_wx_c_uom_uu: {
				required: true
			},
			wypex_producttype_wx_producttype: {
				required: true
			}


		};
	}


	async	Init() {

		try {

			let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery

			if (!thisObject.viewModel.m_product.sku) {
				thisObject.viewModel.m_product.sku = new Date().getTime();
			}

			let htmlForm = await thisObject.GenerateForm();
			HelperService.InsertReplaceFormModal(thisObject.idFormModal, htmlForm);

			HelperService.InjectViewModelToForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.viewModel);

			thisObject.FilterCheckBoxBasedOnProductType(thisObject.viewModel.m_product.producttype);

			thisObject.EventHandler();
			this.metaDataTable.ad_org_uu = localStorage.getItem('ad_org_uu');



		} catch (error) {
			apiInterface.Log(this.constructor.name, this.Init.name, error);
		}


	}

	async GenerateForm() {
		let thisObject = this;
		let productCategoryOptions = new ProductCategoryOptions(thisObject.classNameFormControl);
		let productCategoryOptionsHtml = await productCategoryOptions.Html();

		let uomOptions = new UomOptions(thisObject.classNameFormControl);
		let uomOptionsHtml = await uomOptions.Html();

		let productTypeOptions = new ProductTypeOptions(thisObject.classNameFormControl);
		let productTypeOptionsHtml = await productTypeOptions.Html();


		let allHtml = `				
				<div class="modal fade" id="`+ thisObject.idFormModal + `" tabindex="-1" role="dialog"
				aria-labelledby="product-form-labelled" style="display: none;" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="product-form-labelled">Produk Form</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								</button>
							</div>
							<form id="`+ thisObject.idFormValidation + `" class="kt-form">
								<div class="modal-body">`+ productCategoryOptionsHtml + `
									<div class="form-group">
										<label>Nama Produk</label>
										<input id="m_product_wx_name" type="text" class="`+ thisObject.classNameFormControl + ` form-control"
											aria-describedby="emailHelp" placeholder="Masukkan nama produk">
										<span class="form-text text-muted">Masukkan Nama Produk</span>
									</div>`+ uomOptionsHtml + `
									
									<div class="form-group">
										<label>SKU (Stock Keeping Unit)</label>
										<input id="m_product_wx_sku" type="text" disabled="disabled"
										class="`+ thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp"
											placeholder="sku (stock keeping unit) dibuat otomatis oleh sistem">
										<span class="form-text text-muted">SKU Stock Keeping Unit dibuat otomatis oleh sistem</span>
									</div>
									<div class="form-group">
										<label>UPC (Universal Product Code)</label>
										<input id="m_product_wx_upc" type="text" class="`+ thisObject.classNameFormControl + ` form-control"
											aria-describedby="emailHelp" placeholder="Masukkan UPC jika ada">
										<span class="form-text text-muted">Kode Produk yang berlaku secara internasional, biasanya sudah
											ada dalam produk manufacturing</span>
									</div>`+ productTypeOptionsHtml + `
									
									<div class="form-group">
										<label></label>
										<div class="kt-checkbox-list">
				
											<label id="checkbox_m_product_wx_issold" class="kt-checkbox kt-checkbox--bold kt-checkbox--brand">
												<input id="m_product_wx_issold" class="`+ thisObject.classNameFormControl + ` form-control" type="checkbox" checked>
												Apakah produk ini dijual ?
												<span></span>
											</label>
											<label id="checkbox_m_product_wx_ispurchased" class="kt-checkbox kt-checkbox--bold kt-checkbox--brand">
												<input id="m_product_wx_ispurchased" class="`+ thisObject.classNameFormControl + ` form-control" type="checkbox"
													checked> Apakah dilakukan pembelian pada produk ini ?
												<span></span>
											</label>
											<label id="checkbox_m_product_wx_isstocked" class="kt-checkbox kt-checkbox--bold kt-checkbox--brand">
												<input id="m_product_wx_isstocked" class="`+ thisObject.classNameFormControl + ` form-control" type="checkbox" checked>
												Apakah stock produk ini akan di kontrol ?
												<span></span>
											</label>
											<label id="checkbox_m_product_wx_isbom" class="kt-checkbox kt-checkbox--bold kt-checkbox--brand">
												<input id="m_product_wx_isbom" class="`+ thisObject.classNameFormControl + ` form-control" type="checkbox"> Apakah
												produk ini diproduksi ?
												<span></span>
											</label>
										</div>
				
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
									<button id='product-form-submit' type="submit" class="btn btn-primary">Save changes</button>
								</div>
							</form>
						</div>
					</div>
				</div>
							
				`;

		return allHtml;



	}

	EventHandler() {
		let thisObject = this;

		try {
			$('#' + thisObject.idFormValidation).off('submit');
			$('#' + thisObject.idFormValidation).on('submit', function (e) {
				e.preventDefault();
				thisObject.Save();

			});
			$('#' + thisObject.idFormValidation + ' #wypex_producttype_wx_producttype').off('change');
			$('#' + thisObject.idFormValidation + ' #wypex_producttype_wx_producttype').on('change', function (e) {
				let productType = $(this).val();
				thisObject.FilterCheckBoxBasedOnProductType(productType);

			});


			$('#' + thisObject.idFormModal).modal('show');

		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.EventHandler.name, error);
		}

	}

	/**
	 * jika I (item munculkan semua checkbox)
	 * jika E (biaya maka hilangkan semua checkbox)
	 * jika S (service maka hilangkan semua checkbox kecuali m_product_wx_issold)
	 */
	FilterCheckBoxBasedOnProductType(producttype) {

		switch (producttype) {

			case "E":
				$('#checkbox_m_product_wx_issold').hide();
				$('#checkbox_m_product_wx_ispurchased').hide();
				$('#checkbox_m_product_wx_isstocked').hide();
				$('#checkbox_m_product_wx_isbom').hide();
				break;
			case "S":
				$('#checkbox_m_product_wx_issold').show();
				$('#checkbox_m_product_wx_ispurchased').hide();
				$('#checkbox_m_product_wx_isstocked').hide();
				$('#checkbox_m_product_wx_isbom').hide();
				break;
			default:
				$('#checkbox_m_product_wx_issold').show();
				$('#checkbox_m_product_wx_ispurchased').show();
				$('#checkbox_m_product_wx_isstocked').show();
				$('#checkbox_m_product_wx_isbom').show();

		}


	}

	/**
		 *

		 */
	async Save() {
		let thisObject = this;

		try {

			let insertSuccess = 0;
			if (HelperService.CheckValidation(thisObject.validation)) {

				thisObject.viewModel = HelperService.GetViewModelFromForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.viewModel);



				insertSuccess = await thisObject.productService.SaveUpdate(thisObject.viewModel, thisObject.datatableReference);
				// jika sukses tersimpan
				if (insertSuccess > 0) {

					$('#' + thisObject.idFormModal).modal('hide');


				}

			}
		} catch (err) {
			apiInterface.Log(this.constructor.name, this.Save.name, err);
		}



	}









}
