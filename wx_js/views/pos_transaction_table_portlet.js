class PosTransactionTablePortlet {

	/**
	 * 	 
	 * @param {*} idFragmentContent  // IdControl untuk meletakkan View pos_transactionpage ex>. #fragmentContent
	 */
	constructor(idFragmentContent) {


		this.titlePage = 'Daftar Open Close POS';

		this.labelVendorOrCustomer = '';
		this.viewModel = new PosTransactionComponentModel();

		this.metaDataTable = new MetaDataTable();

		this.datatableReference = {}; // buat reference datatable

		this.posTransactionService = new PosTransactionService();

		this.idFragmentContent = idFragmentContent;

		this.idTable = 'wx_pos_transaction_table_';
		this.idNewRecord = 'new_record_pos_transaction_wx_';

		this.idButtonRowStatusDialog = 'bttn_row_status'; // button row status pada datatable pos_transaction page

		this.prefixContentPosTransactionlinePage = 'detail_pos_transaction_';

		this.classButtonShowForm = 'pos_transaction_page_bttn_show_form';
		this.classDialogStatus = 'pos_transaction_page_bttn_show_dialog_status';
		this.Init();

	}


	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery

		thisObject.metaDataTable.ad_org_uu = localStorage.getItem('ad_org_uu');

		let allHtml = `
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head kt-portlet__head--lg">
				<div  class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
				<i class="kt-font-brand flaticon2-line-chart"></i>
				</span>
				<h3 class="kt-portlet__head-title">
				`+ thisObject.titlePage + `
				</h3>
				</div>
				<div  class="kt-portlet__head-toolbar">
					<div class="kt-portlet__head-wrapper">
						<div class="kt-portlet__head-actions">
						</div>
					</div>
				</div>
			</div>
			<div class="kt-portlet__body" >
			<table class="table table-striped- table-bpos_transactioned table-hover table-checkable" id="`+ thisObject.idTable + `">
			
			</table>
			</div>
		</div>
		`;


		$('#' + thisObject.idFragmentContent).html(allHtml);


		var table = $('#' + thisObject.idTable);
		let columnDefDatatable = [

			{
				targets: -2,
				title: 'Tanggal Open',
				orderable: true,
				render: function (data, type, full, meta) {
					return HelperService.ConvertTimestampToLocateDateTimeString(full.pos_transaction_wx_open_date);
				},
			},
			{
				targets: -1,
				title: 'Tanggal Close',
				orderable: true,
				render: function (data, type, full, meta) {
					return HelperService.ConvertTimestampToLocateDateTimeString(full.pos_transaction_wx_close_date);
				},
			},

		];


		thisObject.posTransactionService.FindAll(thisObject.metaDataTable).then(function (pos_transactionCMTList) {
			// begin first table
			thisObject.datatableReference = table.DataTable({
				responsive: true,
				data: pos_transactionCMTList,
				scrollX: true,
				rowId: 'pos_transaction_wx_pos_transaction_uu',
				columns: [
					{
						"className": 'details-control',
						"orderable": false,
						"data": null,
						"defaultContent": '<button  type="button" class="btn btn-outline-hover-info btn-icon"><i class="la la-plus-square-o"></i></button>'
					},
					{ title: 'No Dokumen', data: 'pos_transaction_wx_documentno' },
					{ title: 'Open Date', data: 'pos_transaction_wx_open_date' },
					{ title: 'Close Date', data: 'pos_transaction_wx_close_date' }
				],
				columnDefs: columnDefDatatable,
				"orderable": [[0, 'desc']],
				createdRow: function (row, data, index) {
					if (data.extn === '') {
						var td = $(row).find("td:first");
						td.removeClass('details-control');
					}
				},
				rowCallback: function (row, data, index) {
					//console.log('rowCallback');
				}

			});

			// attach event handler on the page

			thisObject.EventHandler();


		});

	}

	EventHandler() {
		let thisObject = this;
		// Add event listener for opening and closing details
		$('#' + thisObject.idTable + ' tbody').off('click', 'td.details-control');
		$('#' + thisObject.idTable + ' tbody').on('click', 'td.details-control', function () {
			var tr = $(this).closest('tr');
			var row = thisObject.datatableReference.row(tr);
			let cpos_transactionParent = row.data();
			if (row.child.isShown()) {
				// This row is already open - close it
				row.child.hide();
				$(this).html('<button  type="button" class="btn btn-outline-hover-info btn-icon"><i class="la la-plus-square-o"></i></button>');
			}
			else {
				let idpos_transactiondetailContent = thisObject.prefixContentPosTransactionlinePage + cpos_transactionParent.pos_transaction_wx_pos_transaction_uu;
				let pos_transactionlineFragment = `<div id="` + idpos_transactiondetailContent + `"></div>`;
				row.child(pos_transactionlineFragment).show();

				// generate pos_transactionlinePage
				let pos_transactionlinePage = new PosTransactionlineTable(cpos_transactionParent, idpos_transactiondetailContent, thisObject.datatableReference, thisObject);
				pos_transactionlinePage.Init();


				// change menjadi button minus
				$(this).html('<button  type="button" class="btn btn-outline-hover-info btn-icon"><i class="la la-minus-square-o"></i></button>');

			}

		});



		thisObject.datatableReference.off('draw');
		thisObject.datatableReference.on('draw', function () {

			thisObject.EventHandler();
		});

	}

	UpdateTable(uuid) {
		let thisObject = this;

		try {
			thisObject.posTransactionService.FindPosTransactionCMTListByPosTransactionUu(uuid).then(function (pos_transaction_cmt_list) {

				let pos_transaction_cmt = pos_transaction_cmt_list[0];

				if ($('#'+uuid).length){
					thisObject.datatableReference.row($('#' + uuid)).data(pos_transaction_cmt).draw();
				}else{
					thisObject.datatableReference.row.add(pos_transaction_cmt).draw();
					
				}

				

			});
		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.UpdateTable.name, error);
		}



	}




}

