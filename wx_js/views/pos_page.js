/**
 * StockOpname

 */
class PosPage {

	/**
	 * 	
	 * @param {*} idFragmentContent  // IdControl untuk meletakkan View Orderpage ex>. #fragmentContent
	 */
	constructor(idFragmentContent, idFragmentSubHeader) {

		this.titlePage = 'Point Of Sales';
		this.posService = new PosService();

		this.idFragmentSubHeader = idFragmentSubHeader;
		this.idFragmentContent = idFragmentContent;

		this.idPosTab = 'pos_page_pos_tab';
		this.idPosTransactionTab = 'pos_page_pos_transaction_tab';

		this.Init();

	}




	/**
	 * Cek default m_warehouse_uu, m_pricelist_version_uu, 
	 * jika tidak ada show form untuk memilih default m_warehouse_uu, m_pricelist_version_uu
	 */
	async Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery
		thisObject.CreateSubHeader();
		try {

			let defaultPosCMTList = await thisObject.posService.GetDefaultCPosCMT();

			// jika tidak ada default pos m_warehouse_uu, open configure POS
			if (defaultPosCMTList.length) {

				thisObject.defaultPosCMT = defaultPosCMTList[0];

				//cek jika pos sudah ada opened, jika belum opened maka buka pos_transaction_tab
				let posTransactionService = new PosTransactionService();
				let outstandingPosTransactionCMTList = await posTransactionService.FindOutstandingPosTransaction();

				if (outstandingPosTransactionCMTList.length){
					let posTab = new PosTab(thisObject.idFragmentContent);
				}else{

					//open pos_transactiontab
					let posTab = new PosTransactionTab(thisObject.idFragmentContent, thisObject.defaultPosCMT);
				}
				
			} else {
				let setDefaultConfigForm = new DefaultPosConfigForm(thisObject.idFragmentContent, new PosConfigComponentModel());
			}

		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.Init.name, error);
		}


	


	}


	CreateSubHeader() {
		let thisObject = this;
		let html = `
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	 <div class="kt-container  kt-container--fluid ">
		 <div class="kt-subheader__main">
			<a href="#" id="`+thisObject.idPosTab + `" class="btn btn-label-primary btn-bold">
				POS
			</a>
			<a href="#" id="`+thisObject.idPosTransactionTab + `" class="btn btn-label-success btn-bold">
				OPEN / CLOSE POS
			</a>
						
		 </div>		 
	 </div>
 </div>
	 `;

		$('#' + thisObject.idFragmentSubHeader).html(html);
		thisObject.EventHandler();
	}

	EventHandler() {
		let thisObject = this;
		
		$('#'+thisObject.idPosTab).off('click');
		$('#'+thisObject.idPosTab).on('click', function(e){

			let posTab = new PosTab(thisObject.idFragmentContent);
			e.preventDefault();
		})

		$('#'+thisObject.idPosTransactionTab).off('click');
		$('#'+thisObject.idPosTransactionTab).on('click', function(e){

			let posTab = new PosTransactionTab(thisObject.idFragmentContent, thisObject.defaultPosCMT);
			e.preventDefault();
		})


	}


}

