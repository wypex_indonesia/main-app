class UomConversionForm {

	
	constructor(uomConversionCM, datatableReference) {


		this.viewModel = uomConversionCM;

		this.metaDataTable = new MetaDataTable();


		this.datatableReference = datatableReference; // buat reference datatable

		this.uomConversionService = new UomConversionService();
	
		this.idFormModal = 'c_uom_conversion_form_wx_';
		this.idFormValidation = 'c_uom_conversion_form_validation_';
		this.classNameFormControl = 'c-uom-conversion-form-control';
		this.selectorClassNameFormControl = '.' + this.classNameFormControl;
		
		this.idButtonFormSubmit = 'c_uom_conversion_form_submit_wx_';
		this.validation = {
			id_form: this.idFormValidation,
			c_uom_conversion_wx_dividerate: {
				required: true
			},
			c_uomfrom_wx_c_uom_uu: {
				required: true
			},

			c_uomto_wx_c_uom_uu: {
				required: true
			},

		}
		this.Init();
	}





	async Init() {
		try {
			let thisObject = this;

			let htmlForm = await thisObject.GenerateForm();
			HelperService.InsertReplaceFormModal(thisObject.idFormModal, htmlForm);

			HelperService.InjectViewModelToForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.viewModel);

			thisObject.EventHandler();
		} catch (error) {
			apiInterface.Log()
		}


	}




	async GenerateForm() {
		let thisObject = this;
		let uomOptionsTo = new UomOptions(thisObject.classNameFormControl, 'c_uomto_wx_c_uom_uu');
		let uomOptionsToHtml = await uomOptionsTo.GenerateControlOptions();

		let uomOptionsFrom = new UomOptions(thisObject.classNameFormControl, 'c_uomfrom_wx_c_uom_uu');
		let uomOptionsFromHtml = await uomOptionsFrom.GenerateControlOptions();

		let html = `
			<div class="modal fade" id="` + thisObject.idFormModal + `" tabindex="-1" role="dialog"
			aria-labelledby="product-form-labelled" style="display: none;" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="product-form-labelled">Produk Bom Form</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							</button>
						</div>
						<form id="`+ thisObject.idFormValidation + `" class="kt-form">
							<div class="modal-body">
			
								<div class="form-group ">
									<label>Konversi Satuan Dari ke Satuan</label>
									<div class="input-group">
										<div class="input-group-prepend"><span class="input-group-text">1</span></div>`+ uomOptionsToHtml + `
										<div class="input-group-prepend"><span class="input-group-text">=</span></div>
										<input class="` + thisObject.classNameFormControl + ` form-control" placeholder="Jumlah"
											type="number" id="c_uom_conversion_wx_dividerate">`+ uomOptionsFromHtml + `
									</div>
									<span class="form-text text-muted">Masukkan konversi satuan dengan 1 [satuan] = [jumlah]
										[satuan]</span>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								<button id="`+ thisObject.idButtonFormSubmit + `" type="submit" class="btn btn-primary">Save
									changes</button>
							</div>
						</form>
					</div>
				</div>
			</div>				
			`;

		return html;

	}

	EventHandler() {
		let thisObject = this;

		try {
			$('#' + thisObject.idFormValidation).off('submit');
			$('#' + thisObject.idFormValidation).on('submit', function (e) {
				e.preventDefault();
				thisObject.Save();

			});


			$('#' + thisObject.idFormModal).modal('show');


			$('.wx-format-money').mask('#,##0', { reverse: true });

		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.EventHandler.name, error);
		}






	}
/**
		 *

		 */
		async Save() {
			let thisObject = this;
	
			try {
	
				let insertSuccess = 0;
				if (HelperService.CheckValidation(thisObject.validation)) {
	
					thisObject.viewModel = HelperService.GetViewModelFromForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.viewModel);
	
	
	
					insertSuccess = await thisObject.uomConversionService.SaveUpdate(thisObject.viewModel, thisObject.datatableReference);
					// jika sukses tersimpan
					if (insertSuccess > 0) {
	
						$('#' + thisObject.idFormModal).modal('hide');
	
	
					}
	
				}
			} catch (err) {
				apiInterface.Log(this.constructor.name, this.Save.name, err);
			}
	
	
	
		}
	




}
