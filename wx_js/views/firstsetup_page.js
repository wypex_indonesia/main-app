/**
 * StockOpname

 */
class FirstSetupPage {

	/**
	 * 	
	 * @param {*} idFragmentContent  // IdControl untuk meletakkan View Orderpage ex>. #fragmentContent
	 */
	constructor(idFragmentContent, organizationCM, userCM) {
		this.validation = {

			ad_org_wx_name: {
				required: true
			},
			ad_user_wx_phone: {
				required: true
			},
			ad_user_wx_name: {
				required: true
			},
			ad_user_wx_password: {
				required: true
			},
			c_location_wx_address1: {
				required: true
			},
			c_location_wx_city: {
				required: true
			},
			c_location_wx_postal: {
				required: true
			},
			c_location_wx_regionname: {
				required: true
			},

		};



		this.firstsetupService = new FirstSetupService();
		this.organizationCM = organizationCM;
		this.userCM = userCM;
		this.classNameFormControl = 'wx-firstsetup-form-control';

		this.idButtonFormSubmit = 'wx-firstsetup-form-submit';
		this.idFragmentContent = idFragmentContent;


		this.Init();

	}




	/**
	 * Cek default m_warehouse_uu, m_pricelist_version_uu, 
	 * jika tidak ada show form untuk memilih default m_warehouse_uu, m_pricelist_version_uu
	 */
	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery


		let layoutHtml = this.GenerateForm();


		$('#' + thisObject.idFragmentContent).html(layoutHtml);

		thisObject.PageEventHandler();

	}

	async Save() {
		let thisObject = this;
		let insertSuccess = 0;
		try {
			if (HelperService.CheckValidation(thisObject.validation)) {

				thisObject.organizationCM = HelperService.GetViewModelFromForm('#' + thisObject.idFormValidation + ' .' + thisObject.classNameFormControl, thisObject.organizationCM);


				thisObject.userCM = HelperService.GetViewModelFromForm('#' + thisObject.idFormValidation + ' .' + thisObject.classNameFormControl, thisObject.userCM);

				thisObject.userCM.ad_user.password = CryptoJS.MD5(thisObject.userCM.ad_user.password).toString();

				let insertSuccess = await thisObject.firstsetupService.SaveUpdate(thisObject.organizationCM, thisObject.userCM);
				apiInterface.Debug( "insert success " + insertSuccess);
				if (insertSuccess) {
				apiInterface.Debug( "insert success after " + insertSuccess);
					let mainPage = new MainPage();
				}

				/*		thisObject.firstsetupService.SaveUpdate(thisObject.organizationCM, thisObject.userCM).then(function (rowChanged) {
							insertSuccess += rowChanged;
							if (insertSuccess) {
								let mainPage = new MainPage();
							}
			
			
						});
			*/

			}
		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.Save.name, error);
		}




	}

	GenerateForm() {
		let thisObject = this;

		let htmlFormAdministrator = ``;

		htmlFormAdministrator = `
						<h3 class="kt-section__title">Administrator User:</h3>
						<div class="kt-section__body">
							<div class="form-group">
								<label>Nama lengkap</label>
								<input id="ad_user_wx_name" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
								<span class="form-text text-muted">Masukkan nama lengkap </span>								
							</div>
							<div class="form-group">
								<label>No Handphone</label>
								<input id="ad_user_wx_phone" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
								<span class="form-text text-muted">Masukkan no handphone dengan benar, karena akan dipergunakan ketika Anda lupa password </span>								
							</div>
							<div class="form-group">
								<label>Password</label>
								<input id="ad_user_wx_password" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
							</div>
						</div>
		`;

		let html = `
		<div class="row">
			<div class="col-xl-12">		
			
			<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
					Konfigurasi
					</h3>
				</div>
			</div>

			<!--begin::Form-->
			<form id="`+ thisObject.idFormValidation + `" class="kt-form">
				<div class="kt-portlet__body">
					<div class="kt-section kt-section--first">`+ htmlFormAdministrator +
			`<h3 class="kt-section__title">Detail Perusahaan:</h3>
						<div class="kt-section__body">
							<div class="form-group">
								<label>Nama Perusahaan</label>
								<input id="ad_org_wx_name" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
								<span class="form-text text-muted">Nama Perusahaan</span>						
							</div>
							<div class="form-group">
								<label>NPWP</label>
								<input id="ad_orginfo_wx_taxid" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
								<span class="form-text text-muted">NPWP jika ada</span>						
							</div>
							<div class="form-group">
								<label>Alamat</label>
								<input id="c_location_wx_address1" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
								
							</div>
							<div class="form-group">
								<label>Kota</label>
								<input id="c_location_wx_city" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
							</div>
							<div class="form-group">
								<label>Kode pos</label>
								<input id="c_location_wx_postal" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
							</div>
							<div class="form-group">
								<label>Propinsi</label>
								<input id="c_location_wx_regionname" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
							</div>
						</div>							
						
					</div>
				</div>
				<div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<div class="row">
							<div class="col-lg-3"></div>
							<div class="col-lg-6">
								<button id="`+ thisObject.idButtonFormSubmit + `" type="submit" class="btn btn-success">Submit</button>
							
							</div>
						</div>
					</div>
				</div>
			</form>

			<!--end::Form-->
			</div>
			</div>		
		</div>`;

		return html;

	}

	PageEventHandler() {
		let thisObject = this;

		/*	$('#' + thisObject.idButtonFormSubmit).off('click');
			$('#' + thisObject.idButtonFormSubmit).on('click', function () {
				thisObject.Save();
	
			});*/

		$('#' + thisObject.idFormValidation).off('submit');
		$('#' + thisObject.idFormValidation).on('submit', function (e) {
			thisObject.Save();
			e.preventDefault();

		});
	}





}

