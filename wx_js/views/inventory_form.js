class InventoryForm {

	/**
	 * 
	
	
	 * @param {InventoryComponentModel}
	 * @param {datatableReference} datatableReference data tabel pada inout page, sebagai reference untuk mengupdate table tersebut
	
	 */
	constructor(inventoryCM, datatableReference) {

		this.titleForm = 'Stock Opname';

		this.componentModel = inventoryCM;


		this.c_doctype_id = 8001;

		this.isWarehouseFrom = true; // buat flag apakah button search yang diclick adalah warehouseFrom atau warehouseTO
		this.metaDataTable = new MetaDataTable();

		this.datatableReference = datatableReference; // buat reference datatable

		this.inventoryService = new InventoryService();

		this.warehouseService = new WarehouseService();

		this.idWidgetSearch  = 'inventory_form_widget_search';
	//	this.idFragmentContent = idFragmentContent;
		this.idFormModal = 'm_inventory_form_wx_';
		this.idFormValidation = 'm_inventory_form_validation_';
		this.classNameFormControl = 'm-inventory-form-control';
		this.selectorClassNameFormControl = '.' + this.classNameFormControl;
	//	this.idDialogBox = 'm_inventory_dialog_box_wx_';

	//	this.idNewRecord = 'new_record_inventory_wx_';
		this.idButtonFormSubmit = 'm_inventory_form_submit_wx_';
	//	this.idButtonRowStatusDialog = 'bttn_row_status'; // button row status pada datatable order page
	//	this.idRadioDocStatus = '';// idGroupRadio Status pada document status dialog

	//	this.prefixContent = 'inventoryline_page'; // prefix content untuk child page 



		this.validation = {

			id_form: this.idFormValidation,


			m_warehouse_wx_name: {
				required: true
			},




		};




		this.Init();


	//	this.EventHandler();

	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery



		let allHtml = thisObject.GenerateFormInput();
		HelperService.InsertReplaceFormModal(thisObject.idFormModal, allHtml);
	//	$('#' + thisObject.idFragmentContent).html(allHtml);

		// jika bukan new record, ambil data-data dibawah untuk select options

		HelperService.InjectViewModelToForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.componentModel);
		thisObject.EventHandler();
	}




	/**
	 * 
	
	 */
	async Save() {
		let thisObject = this;

		let insertSuccess = 0;
		if (HelperService.CheckValidation(thisObject.validation)) {

			thisObject.componentModel = HelperService.GetViewModelFromForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.componentModel);
			thisObject.componentModel.c_doctype.c_doctype_id = thisObject.c_doctype_id;

			insertSuccess = await thisObject.inventoryService.SaveUpdate(thisObject.componentModel, thisObject.datatableReference);
			// jika sukses tersimpan 
			if (insertSuccess > 0) {

				$('#' + thisObject.idFormModal).modal('hide');


			}

		}


	}

	GenerateFormInput() {
		let thisObject = this;


		return `<div class="modal fade" id="` + thisObject.idFormModal + `" tabindex="-1" role="dialog" aria-labelledby="product-form-labelled"
		style="display: none;" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="product-form-labelled">`+ thisObject.titleForm + `</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<form id="`+ thisObject.idFormValidation + `" class="kt-form">
				<div class="modal-body">
					
					<div class="form-group">
						<label>No Dokumen</label>
						<input disabled="disabled" id="m_inventory_wx_documentno" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
						<span class="form-text text-muted">No Dokumen dibuat otomatis</span>
					</div>
				<div class="form-group form-group-marginless">
					<label>Dari Gudang / Toko</label>
					<div class="input-group">
						<input disabled="disabled" type="text" class="` + thisObject.classNameFormControl + ` form-control"  id="m_warehouse_wx_name" placeholder="Cari Gudang / Toko ...">
						<div class="input-group-append">
						<span id="warehouse_from" class="search_m_warehouse input-group-text"><i  class="la la-search"></i></span>
						</div>
						<input type="hidden" class="` + thisObject.classNameFormControl + `" id="m_warehouse_wx_m_warehouse_uu">
					</div>
				</div>	
				<div class="form-group">
					<label>Deskripsi</label>
					<input  id="m_inventory_wx_description" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
					<span class="form-text text-muted">Deskripsi</span>
				</div>			
				<div class="form-group ">
							<label>Tanggal Stock Opname</label>
							<input type="text" class="` + thisObject.classNameFormControl + ` datepicker form-control" id="m_inventory_wx_movementdate" readonly="" placeholder="Select date">
						</div>	
					<div class="form-group">
						<label>Dokumen Status</label>
						<input disabled="disabled" id="m_inventory_wx_docstatus" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
						<span class="form-text text-muted">Status Dokumen DRAFT / COMPLETED / REVERSED</span>
					</div>
					<div class="form-group" id="`+thisObject.idWidgetSearch + `">
							
	
					</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button id="`+ thisObject.idButtonFormSubmit + `" type="submit" class="btn btn-primary">Save changes</button>
					</div>
					</form>
			
			</div>
		</div>
	</div>`;
	}


	EventHandler() {
		let thisObject = this;
		$('#' + thisObject.idFormValidation).on('submit');
		$('#' + thisObject.idFormValidation).on('submit', function (e) {
			thisObject.Save();
			e.preventDefault();

		});


		/** BEGIN EVENT SEARCH M_Warehouse **************************************/
		$('#' + thisObject.idFormValidation + ' .search_m_warehouse').off('click');
		$('#' + thisObject.idFormValidation + ' .search_m_warehouse').on('click', function (e) {
			let functionGetResult = function(warehouseSelected){
				$('#' + thisObject.idFormValidation+ ' #m_warehouse_wx_name').val(warehouseSelected.m_warehouse_wx_name);
				$('#' + thisObject.idFormValidation+ ' #m_warehouse_wx_m_warehouse_uu').val(warehouseSelected.m_warehouse_wx_m_warehouse_uu);
			
			};
			let warehouse_search_widget = new WarehouseSearchWidget(thisObject.idWidgetSearch,functionGetResult );
			warehouse_search_widget.Init();
		});

		

		/** END EVENT SEARCH M_Warehouse **************************************/



		$('#' + thisObject.idFormModal).modal('show');


	}



}
