class PrintProductBarcodelineForm {

	/**
	 * 
	 * @param {PrintProductBarcodeComponentModel} printProductBarcodeCM
	 * @param {DatatableReference} datatableReference 
	 */
	constructor(printProductBarcodeCM, printProductBarcodelineCM, datatableReference) {
		this.validation = {

			m_product_wx_name: {
				required: true
			},

			wxt_print_productbarcodeline_wx_count_print: {
				required: true
			},

		};



		this.printProductBarcodeCM = printProductBarcodeCM;


		this.viewModel = printProductBarcodelineCM;

		this.printProductBarcodelineService = new PrintProductBarcodelineService();

		this.productService = new ProductService();

		this.selectedProduct = {}; // current product yang sedang dipilih, product_viewmodel_table
		this.idFormModal = 'wxt_print_productbarcodeline_form_wx';
		this.idFormValidation = 'wxt_print_productbarcodeline_form_validation';
		this.classNameFormControl = 'c-orderline-form-control';
		this.selectorClassNameFormControl = '.' + this.classNameFormControl;

		this.idWidgetSearch = 'wxt_print_productbarcodeline_form_wx_widget_search';
		this.idButtonFormSubmit = 'wxt_print_productbarcodeline_form_submit_wx';

		this.datatableReference = datatableReference;

		this.Init();


		this.PageEventHandler();
	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery


		HelperService.InsertReplaceFormModal(thisObject.idFormModal, thisObject.GenerateFormInput());


		HelperService.InjectViewModelToForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.viewModel);
		if (!thisObject.viewModel.wxt_print_productbarcodeline.isskuupc) {
			thisObject.viewModel.wxt_print_productbarcodeline.isskuupc = 'sku';
		}
		$("input[name='wxt_print_productbarcodeline_wx_isskuupc']").each(function () {
			if ($(this).val() === thisObject.viewModel.wxt_print_productbarcodeline.isskuupc) {
				$(this).prop('checked', true);
			}
		});



	}




	/**
	 * 
	
	 */
	SaveUpdate() {
		let thisObject = this;

		try {
			if (HelperService.CheckValidation(thisObject.validation)) {

				thisObject.viewModel = HelperService.GetViewModelFromForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.viewModel);
				thisObject.viewModel.wxt_print_productbarcodeline.isskuupc = $("input[name='wxt_print_productbarcodeline_wx_isskuupc']:checked").val();
				const timeNow = new Date().getTime();
				// jika new record maka buat mlocator 	
				// harus dibuat default locator, karena locator akan dipergunakan di inventory movement
				let isNewRecord = false;
				if (!thisObject.viewModel.wxt_print_productbarcodeline.wxt_print_productbarcodeline_uu) { //new record
					isNewRecord = true;
					thisObject.viewModel.wxt_print_productbarcodeline.wxt_print_productbarcodeline_uu = HelperService.UUID();
					thisObject.viewModel.wxt_print_productbarcodeline.ad_client_uu = localStorage.getItem('ad_client_uu');
					thisObject.viewModel.wxt_print_productbarcodeline.ad_org_uu = localStorage.getItem('ad_org_uu');

					thisObject.viewModel.wxt_print_productbarcodeline.createdby = localStorage.getItem('ad_user_uu');
					thisObject.viewModel.wxt_print_productbarcodeline.updatedby = localStorage.getItem('ad_user_uu');

					thisObject.viewModel.wxt_print_productbarcodeline.created = timeNow;
					thisObject.viewModel.wxt_print_productbarcodeline.updated = timeNow;
				} else { //update record


					thisObject.viewModel.wxt_print_productbarcodeline.updatedby = localStorage.getItem('ad_user_uu');

					thisObject.viewModel.wxt_print_productbarcodeline.updated = timeNow;
				}

				thisObject.viewModel.wxt_print_productbarcodeline.wxt_print_productbarcode_uu = thisObject.printProductBarcodeCM.wxt_print_productbarcode.wxt_print_productbarcode_uu;
				thisObject.viewModel.wxt_print_productbarcodeline.m_product_uu = thisObject.viewModel.m_product.m_product_uu;
				thisObject.viewModel.wxt_print_productbarcodeline.sync_client = null;
				thisObject.viewModel.wxt_print_productbarcodeline.process_date = null;

				let insertSuccess = 0;
				const sqlInsert = HelperService.SqlInsertOrReplaceStatement('wxt_print_productbarcodeline', thisObject.viewModel.wxt_print_productbarcodeline);

				return new Promise(function (resolve, reject) {
					apiInterface.ExecuteSqlStatement(sqlInsert)
						.then(function (rowChanged) {

							insertSuccess += rowChanged;
							// jika sukses tersimpan 
							if (insertSuccess > 0) {


								thisObject.printProductBarcodelineService.FindPrintProductBarcodelineCMTListByWxtPrintProductBarcodelineUu(thisObject.viewModel.wxt_print_productbarcodeline.wxt_print_productbarcodeline_uu)
									.then(function (rowDataList) {
										let rowData = {};

										if (rowDataList.length > 0) {
											rowData = rowDataList[0];
										}

										if (!isNewRecord) {

											thisObject.datatableReference.row($('#bttn_row_edit' + rowData['wxt_print_productbarcodeline_wx_wxt_print_productbarcodeline_uu']).parents('tr')).data(rowData).draw();
										} else {

											thisObject.datatableReference.row.add(rowData).draw();
										}
									
										$('#' + thisObject.idFormModal).modal('hide');

										resolve(insertSuccess)

									});

							}else{
								resolve(insertSuccess);
							}
						
						});

				});


			}
		} catch (error) {
			apiInterface.Log(this.constructor.name, this.SaveUpdate.name, error);
		}






	}

	GenerateFormInput() {
		let thisObject = this;


		return `
		<div class="modal fade" id="` + thisObject.idFormModal + `" tabindex="-1" role="dialog"
      aria-labelledby="product-form-labelled" style="display: none;" aria-hidden="true">
      <div class="modal-dialog" role="document">
            <div class="modal-content">
                  <div class="modal-header">
                        <h5 class="modal-title" id="product-form-labelled">Print Barcode Produk Line</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        </button>
                  </div>
                  <form id="`+ thisObject.idFormValidation + `" class="kt-form">
                        <div class="modal-body">

                              <div class="form-group form-group-marginless">
                                    <label>Produk</label>
                                    <div class="input-group">
                                          <input disabled="disabled" type="text"
                                                class="` + thisObject.classNameFormControl + ` form-control"
                                                id="m_product_wx_name" placeholder="Cari produk">
                                          <div class="input-group-append">
                                                <span id="search_m_product" class="input-group-text"><i class="la la-search"></i></span>
                                          </div>
                                    </div>
                                    <input type="hidden" class="` + thisObject.classNameFormControl + `"  id="m_product_wx_m_product_uu">
                              </div>
                              <div class="form-group">
                                    <label>Pilih Cetak Barcode SKU atau UPC</label>
                                    <div class="kt-radio-list">
                                          <label class="kt-radio kt-radio--solid kt-radio--brand">
                                                <input value="sku" type="radio" name="wxt_print_productbarcodeline_wx_isskuupc"> Cetak Barcode SKU
                                                <span></span>
                                          </label>
                                          <label class="kt-radio kt-radio--solid kt-radio--brand">
                                                <input value="upc" type="radio" name="wxt_print_productbarcodeline_wx_isskuupc">
                                                Cetak Barcode UPC
                                                <span></span>
                                          </label>
                                    </div>

                              </div>

                              <div class="form-group">
                                    <label>Jumlah yang dicetak</label>

                                    <input id="wxt_print_productbarcodeline_wx_count_print" type="text"   class="` + thisObject.classNameFormControl + ` form-control"     aria-describedby="emailHelp">

                              </div>


                              <div class="form-group" id="`+ thisObject.idWidgetSearch + `">


                              </div>
                        </div>
                        <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                              <button id="`+ thisObject.idButtonFormSubmit + `" type="submit"
                                    class="btn btn-primary">Save changes</button>
                        </div>
                  </form>
            </div>
      </div>
</div>`;
	}

	PageEventHandler() {
		let thisObject = this;



		$('#' + thisObject.idFormValidation).off('submit');
		$('#' + thisObject.idFormValidation).on('submit', function (e) {
			e.preventDefault();
			thisObject.SaveUpdate();

		});


		/** BEGIN EVENT SEARCH M_Product **************************************/
		$('#' + thisObject.idFormValidation + ' #search_m_product').off('click');
		$('#' + thisObject.idFormValidation + ' #search_m_product').on('click', function () {

			let functionGetResult = function (selectedProduct) {
				$('#' + thisObject.idFormValidation + ' #m_product_wx_name').val(selectedProduct.m_product_wx_name);

				$('#' + thisObject.idFormValidation + ' #m_product_wx_m_product_uu').val(selectedProduct.m_product_wx_m_product_uu);


			};
			let product_search_widget = new ProductSearchWidget(thisObject.idWidgetSearch, functionGetResult);
			product_search_widget.Init();

		});



		/** END EVENT SEARCH M_PRODUCT **************************************/

		$('#' + thisObject.idFormModal).modal('show');

	}


}
