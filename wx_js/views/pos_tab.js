/**
 * StockOpname

 */
class PosTab {

	/**
	 * 	
	 * @param {*} idFragmentContent  // IdControl untuk meletakkan View Orderpage ex>. #fragmentContent
	 */
	constructor(idFragmentContent) {

		this.titlePage = 'Point Of Sales';


		this.viewModel = new SearchPosProductComponentModel();
		this.labelForm = '';


		this.idFragmentContent = idFragmentContent;
		this.posService = new PosService();
		this.metaSearchProduct = new MetaDataTable();
		this.metaCart = new MetaDataTable();
		this.searchProductDTRef = {}; // buat reference datatable

		this.idShowCheckout = 'wx-btn-show-checkout';
		this.idCheckoutFragmentContent = "wx_checkout_form_content";
		this.idButtonRowStatusDialog = "bttn_row_status";
		this.idFragmentToolbarSearchProduct = 'wx-fragment-toolbar-search-product';
		this.idFragmentSearchProductTable = 'wx-fragment-search-product';
		this.idFragmentTotalCart = 'wx-fragment-total-cart';
		this.idFragmentCartline = 'wx-fragment-list-cart';
		this.idFragmentPosConfigForm = 'wx-fragment-pos-config-form';
		this.idFragmentCartlineForm = 'wx-fragment-cartline-form';
		this.defaultPosCMT = null;
		this.toolbarSearchProductPortlet = null;
		this.searchProductPortlet = null;
		this.cartlinePortlet = null;
		this.Init();

	}




	/**
	 * Cek default m_warehouse_uu, m_pricelist_version_uu, 
	 * jika tidak ada show form untuk memilih default m_warehouse_uu, m_pricelist_version_uu
	 */
	async Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery

		thisObject.CreatePosPage();

	}

	RedrawSearchProductFunction() {
		let thisObject = this;
		let redrawSearchProduct = function (searchValue) {
			thisObject.searchProductPortlet.Redraw(searchValue);
		}

		return redrawSearchProduct;

	}

	async CreatePosPage() {

		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery

		try {

			let defaultPosCMTList = await thisObject.posService.GetDefaultCPosCMT();

			// jika tidak ada default pos m_warehouse_uu
			if (defaultPosCMTList.length) {

				thisObject.defaultPosCMT = defaultPosCMTList[0];

				let layoutHtml = `
		<div class="row">
			<div class="col-xl-6">		
				<div id="`+ thisObject.idFragmentToolbarSearchProduct + `"></div>
				<div id="`+ thisObject.idFragmentSearchProductTable + `"></div>
			</div>
			<div class="col-xl-6">
				<div id="`+ thisObject.idFragmentTotalCart + `"></div>
				<div id="`+ thisObject.idFragmentCartline + `"></div>
			</div>
		</div>`;


				this.metaSearchProduct.ad_org_uu = localStorage.getItem('ad_org_uu');


				$('#' + thisObject.idFragmentContent).html(layoutHtml);

				thisObject.cartlinePortlet = new CartlinePortlet(thisObject.idFragmentCartline, thisObject.defaultPosCMT);
				thisObject.toolbarSearchProductPortlet = new ToolbarSearchProductPortlet(thisObject.idFragmentToolbarSearchProduct,
					thisObject.defaultPosCMT, thisObject.cartlinePortlet, thisObject.RedrawSearchProductFunction());
				thisObject.searchProductPortlet = new SearchProductPortlet(thisObject.idFragmentSearchProductTable,
					thisObject.defaultPosCMT.c_pos_wx_m_pricelist_version_uu,
					thisObject.toolbarSearchProductPortlet, thisObject.cartlinePortlet);

				thisObject.toolbarSearchProductPortlet.Init();
				thisObject.toolbarSearchProductPortlet.ClearValueAndFocus();

				thisObject.EventHandler();

			} else {
				let setDefaultConfigForm = new DefaultPosConfigForm(thisObject.idFragmentContent, new PosConfigComponentModel());
			}

		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.CreatePosPage.name, error);
		}



	}

	EventHandler() {
		let thisObject = this;
		// untuk enter keypress di table searchProduct dan table cartline
		$(document).off('keypress');
		$(document).on('keypress', function (e) {
			if (e.which == 13) {

				if (thisObject.searchProductPortlet) {

					if (thisObject.searchProductPortlet.searchProductDTRef) {

						let rowColSearchProduct = thisObject.searchProductPortlet.searchProductDTRef.cell({ focused: true })[0][0];
						if (rowColSearchProduct) {

							let data = thisObject.searchProductPortlet.searchProductDTRef.row(rowColSearchProduct.row).data();
							thisObject.cartlinePortlet.AddCartline(data).then(function (inserted) {
								thisObject.searchProductPortlet.ClearHtml();
								thisObject.toolbarSearchProductPortlet.ClearValueAndFocus();

							})
						}

					}

				}

				if (thisObject.cartlinePortlet) {

					if (thisObject.cartlinePortlet.cartDTRef) {

						let rowCartline = thisObject.cartlinePortlet.cartDTRef.cell({ focused: true })[0][0];
						if (rowCartline) {

							let indexRow = rowCartline.row;
							let orderline = thisObject.cartlinePortlet.cartDTRef.row(rowCartline.row).data();
							let cartlineCM = HelperService.ConvertRowDataToViewModel(orderline);

							let cartlineForm = new CartlineForm(cartlineCM, thisObject.cartlinePortlet.cartDTRef, indexRow);

						}

					}

				}


			}


		})




	}


}

