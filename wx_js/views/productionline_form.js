class ProductionlineForm {

	/**
	 * 
	 * @param {ProductionlinePage} productionlinePage productionlinePageReference

	 */
	constructor(productionlineCM, productionlinePage) {
		this.validation = {
			m_productionline_wx_movementqty: {
				required: true
			},
			m_product_wx_name: {
				required: true
			}


		};


		this.productionlinePage = productionlinePage;

		this.productionCMT = productionlinePage.productionCMT;


		this.componentModel = productionlineCM;

		this.productionlineService = new ProductionlineService();
		
		this.productService = new ProductService();

		this.idFormFragmentContent = productionlinePage.idFormFragmentContent;
		this.selectedProduct = {}; // current product yang sedang dipilih, product_viewmodel_table
		this.idFormModal = 'm_productionline_form_wx_' + this.productionCMT.m_production_wx_m_production_uu;
		this.idFormValidation = 'm_productionline_form_validation_' + this.productionCMT.m_production_wx_m_production_uu;
		this.classNameFormControl = 'm-productionline-form-control';
		this.selectorClassNameFormControl = '.' + this.classNameFormControl;


		this.idButtonFormSubmit = 'm_productionline_form_submit_wx_' + this.productionCMT.m_production_wx_m_production_uu;

		this.datatableReference = productionlinePage.datatableReference;
		this.productionlinePage = productionlinePage;

		this.idWidgetSearch = 'productionline_form_widget_search';
		this.ctrlBaseUom;
		let lengthRow = this.datatableReference.data().length;
		if (!this.componentModel.m_productionline.m_productionline_uu) {
			this.componentModel.m_productionline.line = ++lengthRow;
		}
		this.Init();

	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery



		HelperService.InsertReplaceFormModal(thisObject.idFormModal, thisObject.GenerateFormInput());

		
		HelperService.InjectViewModelToForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.componentModel);

		thisObject.ctrlBaseUom = $('#' + this.idFormValidation + ' .based-uom');

		thisObject.ctrlBaseUom.text(thisObject.componentModel.c_uom.name);

		thisObject.EventHandler();
	}




	/**
	 * 
	
	 */
 async	SaveUpdate() {
		let thisObject = this;


		if (HelperService.CheckValidation(thisObject.validation)) {

			thisObject.componentModel = HelperService.GetViewModelFromForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.componentModel);

			const timeNow = new Date().getTime();
			// jika new record maka buat mlocator 	
			// harus dibuat default locator, karena locator akan dipergunakan di inventory production
			let isNewRecord = false;
			if (!thisObject.componentModel.m_productionline.m_productionline_uu) { //new record
				isNewRecord = true;
				thisObject.componentModel.m_productionline.m_productionline_uu = HelperService.UUID();
				thisObject.componentModel.m_productionline.ad_client_uu = localStorage.getItem('ad_client_uu');
				thisObject.componentModel.m_productionline.ad_org_uu = localStorage.getItem('ad_org_uu');

				thisObject.componentModel.m_productionline.createdby = localStorage.getItem('ad_user_uu');
				thisObject.componentModel.m_productionline.updatedby = localStorage.getItem('ad_user_uu');

				thisObject.componentModel.m_productionline.created = timeNow;
				thisObject.componentModel.m_productionline.updated = timeNow;
			} else { //update record


				thisObject.componentModel.m_productionline.updatedby = localStorage.getItem('ad_user_uu');

				thisObject.componentModel.m_productionline.updated = timeNow;
			}


			thisObject.componentModel.m_productionline.m_production_uu = thisObject.productionCMT.m_production_wx_m_production_uu;
			thisObject.componentModel.m_productionline.m_product_uu = thisObject.componentModel.m_product.m_product_uu;
			thisObject.componentModel.m_productionline.sync_client = null;
			thisObject.componentModel.m_productionline.process_date = null;

			let insertSuccess = 0;
			const sqlInsert = HelperService.SqlInsertOrReplaceStatement('m_productionline', thisObject.componentModel.m_productionline);

			insertSuccess += await apiInterface.ExecuteSqlStatement(sqlInsert);

			// jika sukses tersimpan 
			if (insertSuccess > 0) {


				let rowDataList = await thisObject.productionlineService.FindProductionlineCMTListByMProductionlineUu(thisObject.componentModel.m_productionline.m_productionline_uu);

				let rowData = {};

				if (rowDataList.length > 0) {
					rowData = rowDataList[0];
				}
				if (!isNewRecord) {
					thisObject.productionlinePage.datatableReference.row($('#bttn_row_edit' + rowData['m_productionline_wx_m_productionline_uu']).parents('tr')).data(rowData).draw();
				} else {
					thisObject.productionlinePage.datatableReference.row.add(rowData).draw();
				}

			}
			$('#' + thisObject.idFormModal).modal('hide');

		}




	}

	GenerateFormInput() {
		let thisObject = this;

		return `
	<div class="modal fade" id="` + thisObject.idFormModal + `" tabindex="-1" role="dialog"
		aria-labelledby="product-form-labelled" style="display: none;" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="product-form-labelled">Production Line Form</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<form id="` + thisObject.idFormValidation + `" class="kt-form">
					<div class="modal-body">
						<div class="form-group">
							<label>No</label>
							<input id="m_productionline_wx_line" type="text"
								class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp">
							<span class="form-text text-muted">Nomor urut</span>
						</div>
						<div class="form-group form-group-marginless">
							<label>Produk</label>
							<div class="input-group">
								<input disabled="disabled" type="text"
									class="` + thisObject.classNameFormControl + ` form-control" id="m_product_wx_name"
									placeholder="Cari produk">
								<div class="input-group-append">
									<span id="search_m_product" class="input-group-text"><i class="la la-search"></i></span>
								</div>
							</div>
							<input type="hidden" class="` + thisObject.classNameFormControl + `"
								id="m_product_wx_m_product_uu">
						</div>
						<div class="form-group">
							<label>Jumlah Produk</label>
							<div class="input-group">
								<input id="m_productionline_wx_movementqty" type="number"
									class="` + thisObject.classNameFormControl + ` form-control"
									aria-describedby="emailHelp">
	
								<div class="input-group-prepend"><span class="input-group-text based-uom"></span></div>
							</div>
						</div>
						<div class="form-group" id="`+thisObject.idWidgetSearch+`">
	
	
						</div>
	
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button id="`+ thisObject.idButtonFormSubmit + `" type="submit" class="btn btn-primary">Save
							changes</button>
					</div>
	
				</form>
			</div>
		</div>
	</div>
		`;
	}

	EventHandler() {
		let thisObject = this;
		$('#' + thisObject.idFormValidation).off('submit');
		$('#' + thisObject.idFormValidation).on('submit', function (e) {
			thisObject.SaveUpdate();
			e.preventDefault();
		});

		/** BEGIN EVENT SEARCH M_Product **************************************/
		$('#' + thisObject.idFormValidation + ' #search_m_product').off('click');
		$('#' + thisObject.idFormValidation + ' #search_m_product').on('click', function () {

			let functionGetResult = function(selectedProduct){
				thisObject.selectedProduct = selectedProduct;
				$('#' + thisObject.idFormValidation + ' #m_product_wx_name').val(thisObject.selectedProduct.m_product_wx_name);
				thisObject.ctrlBaseUom.text(thisObject.selectedProduct.c_uom_wx_name);
				$('#' + thisObject.idFormValidation + ' #m_product_wx_m_product_uu').val(thisObject.selectedProduct.m_product_wx_m_product_uu);
			};

			let productSearchWidget = new ProductSearchWidget(thisObject.idWidgetSearch, functionGetResult);
			productSearchWidget.Init();
		});



		/** END EVENT SEARCH M_PRODUCT **************************************/

		$('.wx-foproductiont-money').mask('#,##0', { reverse: true });
		$('#' + thisObject.idFormModal).modal('show');
	}





}
