class ReportLabaRugiPortlet {


    constructor(idFragmentContent, periode) {

        this.idFragmentContent = idFragmentContent;
        this.periode = periode;

    }

    async Init() {
        let thisObject = this;

        let html = await thisObject.Html();
        $('#' + thisObject.idFragmentContent).html(html);


    }


    async Html() {
        let thisObject = this;
        let factacctService = new FactAcctService();
        let summaryTotalElementValue = await factacctService.GetSummaryTotalElementValue(thisObject.periode);
        let labelDate = HelperService.ConvertPeriodeToLabel(thisObject.periode);
        let akunPendapatan4 = _.find(summaryTotalElementValue, function (summary) { return summary.value === '40000000'; });
        let akunHpp5 = _.find(summaryTotalElementValue, function (summary) { return summary.value === '50000000'; });
        let akunExpenseUsaha6 = _.find(summaryTotalElementValue, function (summary) { return summary.value === '60000000'; });
        let akunPendapatanLain7 = _.find(summaryTotalElementValue, function (summary) { return summary.value === '70000000'; });
        let akunPajakPenghasilan8 = _.find(summaryTotalElementValue, function (summary) { return summary.value === '80000000'; });

        let akunChildPendapatan = HelperService.GetListObjectInSummaryTotalElement('parent_value', '40000000', 'Revenue', summaryTotalElementValue);
        let akunChildHPP = HelperService.GetListObjectInSummaryTotalElement('parent_value', '50000000', 'Expense', summaryTotalElementValue);
        let akunChildExpenseUsaha = HelperService.GetListObjectInSummaryTotalElement('parent_value', '60000000', 'Expense', summaryTotalElementValue);
        let akunChildPendapatanLain = HelperService.GetListObjectInSummaryTotalElement('parent_value', '70000000', 'Revenue', summaryTotalElementValue);
        let akunChildBiayaLain = HelperService.GetListObjectInSummaryTotalElement('parent_value', '70000000', 'Expense', summaryTotalElementValue);
        let akunChildPajakPenghasilan = HelperService.GetListObjectInSummaryTotalElement('parent_value', '80000000', 'Expense', summaryTotalElementValue);

        //sortOrder
        akunChildPendapatan = _.orderBy(akunChildPendapatan, ['value'], ['asc']);
        akunChildHPP = _.orderBy(akunChildHPP, ['value'], ['asc']);
        akunChildExpenseUsaha = _.orderBy(akunChildExpenseUsaha, ['value'], ['asc']);
        akunChildPendapatanLain = _.orderBy(akunChildPendapatanLain, ['value'], ['asc']);
        akunChildBiayaLain = _.orderBy(akunChildBiayaLain, ['value'], ['asc']);
        akunChildPajakPenghasilan = _.orderBy(akunChildPajakPenghasilan, ['value'], ['asc']);


        let returnRowPendapatan = thisObject.CreateRowAkun(akunChildPendapatan, akunPendapatan4, 'TOTAL PENDAPATAN');

        let returnRowHpp = thisObject.CreateRowAkun(akunChildHPP, akunHpp5, 'TOTAL HARGA POKOK PENJUALAN');

        let amountPendapatanMinusHpp   = returnRowPendapatan.total_amount - returnRowHpp.total_amount;
        let returnRowPendapatanMinusHppHtml = thisObject.CreateTotalSummaryRow(amountPendapatanMinusHpp,"LABA KOTOR<br>(TOTAL PENDAPATAN - TOTAL HARGA POKOK PENJUALAN)");

        let returnRowBiaya = thisObject.CreateRowAkun(akunChildExpenseUsaha, akunExpenseUsaha6, 'TOTAL BIAYA');

        let amountLabaKotorMinusBiaya = amountPendapatanMinusHpp - returnRowBiaya.total_amount;

        let returnRowLabaKotorMinusBiayaHtml = thisObject.CreateTotalSummaryRow(amountLabaKotorMinusBiaya,"TOTAL PENDAPATAN USAHA<br>(TOTAL LABA KOTOR - TOTAL BIAYA)");

        let returnRowPendapatanLainnya = thisObject.CreateRowAkun(akunChildPendapatanLain, akunPendapatanLain7, 'TOTAL PENDAPATAN LAINNYA');

        let returnRowBiayaLainnya = thisObject.CreateRowAkun(akunChildBiayaLain, akunPendapatanLain7, 'TOTAL BIAYA LAINNYA');

        let returnRowBiayaPajakPenghasilan = thisObject.CreateRowAkun(akunChildPajakPenghasilan, akunPajakPenghasilan8, 'TOTAL BIAYA PAJAK PENGHASILAN');

        let amountPendapatanLainnyaMinusBiayaLainnya  = returnRowPendapatanLainnya.total_amount - returnRowBiayaLainnya.total_amount - returnRowBiayaPajakPenghasilan.total_amount;
        
        let rowTotalPendapatanLainnyaHtml = thisObject.CreateTotalSummaryRow(amountPendapatanLainnyaMinusBiayaLainnya,"TOTAL PENDAPATAN DILUAR USAHA<br>(TOTAL PENDAPATAN LAINNYA - TOTAL BIAYA LAINNYA - TOTAL BIAYA PAJAK PENGHASILAN)");

        let amountLabaRugiBersih = amountLabaKotorMinusBiaya + amountPendapatanLainnyaMinusBiayaLainnya;

        let amountLabaRugiBersihHtml = thisObject.CreateTotalSummaryRow(amountLabaRugiBersih,"LABA/RUGI BERSIH<br>(TOTAL PENDAPATAN USAHA + TOTAL PENDAPATAN DILUAR USAHA)");


      
        let html = `
    <div class="kt-portlet">
        <div class="kt-portlet__body kt-portlet__body--fit">
            <div class="kt-invoice-2">
                <div class="kt-invoice__head">
                    <div class="kt-invoice__container">
                        <div class="kt-invoice__brand">
                            <div href="#" class="kt-invoice__logo">
                                
                                <span class="kt-invoice__desc">
                                    <span><h3>Laporan Laba Rugi</h3></span>
                                    <span>`+ labelDate + `</span>
                                </span>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="kt-invoice__body">
                    <div class="kt-invoice__container">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th colspan="2">Akun</th>
                                        <th >Rp</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    `+ returnRowPendapatan.row_html +
                                    returnRowHpp.row_html +
                                    returnRowPendapatanMinusHppHtml +
                                    returnRowBiaya.row_html + 
                                    returnRowLabaKotorMinusBiayaHtml +
                                    returnRowPendapatanLainnya.row_html+
                                    returnRowBiayaLainnya.row_html +
                                    returnRowBiayaPajakPenghasilan.row_html +
                                    rowTotalPendapatanLainnyaHtml +
                                    amountLabaRugiBersihHtml +
            `
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        
        `;

        return html;


    }


    /**
     * 
     * @param {*} listAkun 
     * @param {*} mainAkun mainAkun atau parentAkun 
     * @param {*} labelTotalSummary 
     * return {total_amount: totalamount, row_html: rowhtml}
     */
    CreateRowAkun(listAkun, mainAkun, labelTotalSummary) {

        let rowMainAkun = `
        <tr>
            <td colspan="3">`+ mainAkun.value + ' ' + mainAkun.name + `</td>    
        </tr>`;

        let listRowHtml = '';
        let totalSummary = 0;
        _.forEach(listAkun, function (akun) {
            totalSummary += numeral(akun.amount).value();
            let amount = new Intl.NumberFormat().format(akun.amount);
            let row = `
            <tr>
                <td colspan="2">&nbsp;&nbsp;`+ akun.value + ' ' + akun.name + `</td>
                <td >`+ amount + `</td>
            </tr>            
            `;
            listRowHtml += row;
        });

        let rowTotalSummary = this.CreateTotalSummaryRow(totalSummary, labelTotalSummary);
        let allRowHtml = rowMainAkun + listRowHtml + rowTotalSummary;
        let returnRow = {total_amount: totalSummary, row_html: allRowHtml};

        return returnRow;
    }

    CreateTotalSummaryRow(totalAmount, labelTotalSummary) {

        let formatTotalSummary = new Intl.NumberFormat().format(totalAmount);
        let rowTotalSummary = `
        <tr>
            <td></td>
            <td>`+ labelTotalSummary + `</td>
            <td>`+ formatTotalSummary + `</td>
        </tr>
        `;

        return rowTotalSummary;


    }



}