class ReportFinancialOptionsPortlet {

	/**	
	 * @param {*} idFragmentContent fragment content untuk ReportFinancialOptionsPortlet (this Portlet)
	
	 * @param {*} idFragmentReportContent fragment content untuk idFragmentReportContent (untuk portlet yang dipilih)
	 */
	constructor(idFragmentContent, idFragmentReportContent) {

		this.idFragmentContent = idFragmentContent;
		this.idFragmentReportContent = idFragmentReportContent;

		this.idFormValidation = 'report_financial_options_options_portlet_form_validation';
		this.idButtonFormSubmit = 'report_financial_options_options_portlet_button_form_submit';
		this.classNameControl = 'report_financial_options_options_portlet_class_name_control';
		this.idReportFinancialTypesOptions = 'report_financial_options_options_portlet_types_options';
		this.idSelectPeriode = 'report_financial_options_select_periode';
		this.Init();

	}


	async	Init() {

		try {

			let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery

			let htmlForm = await thisObject.GeneratePortlet();

			$('#' + thisObject.idFragmentContent).html(htmlForm);


			thisObject.EventHandler();



		} catch (error) {
			apiInterface.Log(this.constructor.name, this.Init.name, error);
		}


	}



	async	GeneratePortlet() {
		let thisObject = this;
		let reportFinancialTypesOptions = new ReportFinancialTypesOptions(thisObject.classNameControl, thisObject.idReportFinancialTypesOptions, 'Pilih Tipe Laporan');

		let reportFinancialTypesOptionsHtml = await reportFinancialTypesOptions.Html();
		let portletHtml = `
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand flaticon2-line-chart"></i>
				</span>
				<h3 class="kt-portlet__head-title">
					Tipe Laporan
				</h3>
			</div>
			<div class="kt-portlet__head-toolbar">
				<div class="kt-portlet__head-wrapper">
					<div class="kt-portlet__head-actions">
					</div>
				</div>
			</div>
		</div>
		<form id="`+ thisObject.idFormValidation + `" class="kt-form">
			<div class="kt-portlet__body">`+ reportFinancialTypesOptionsHtml + `
				<div class="form-group ">
					<label>Pilih Periode</label>
					<input type="text" class="` + thisObject.classNameControl + ` datepicker form-control" id="` + thisObject.idSelectPeriode + `" readonly="" placeholder="Select date">
				</div>
			</div>	
			<div class="modal-footer">
				
				<button id="`+ thisObject.idButtonFormSubmit + `" type="submit" class="btn btn-primary">Buat Laporan</button>
			</div>		
		</form>
	</div>
		`;
		return portletHtml;

	}


	EventHandler() {
		let thisObject = this;

	

		try {

			$('#' + thisObject.idFormValidation).off('submit');
			$('#' + thisObject.idFormValidation).on('submit', function (e) {

				e.preventDefault();
				// show report content
				let report_type = $('#' + thisObject.idReportFinancialTypesOptions).val();
				let periode = $('#'+thisObject.idSelectPeriode).val();
				switch (report_type) {

					case 'rugi_laba':
						let rugi_laba_report = new ReportLabaRugiPortlet(thisObject.idFragmentReportContent, periode);
						rugi_laba_report.Init();
						break;
					case 'balance_sheet':
						let balance_sheet_report = new ReportBalanceSheetPortlet(thisObject.idFragmentReportContent, periode);
						balance_sheet_report.Init();
						break;
					case 'cash_flow':
						break;



				}
			});




			$("#" + thisObject.idFormValidation + " #" + thisObject.idSelectPeriode).datepicker({
				format: "mm-yyyy",
				startView: "months",
				minViewMode: "months"
			}).on('changeMonth', function () {
				$("#" + thisObject.idFormValidation + " #" + thisObject.idSelectPeriode).datepicker('hide');

			});



		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.EventHandler.name, error);
		}

	}






}
