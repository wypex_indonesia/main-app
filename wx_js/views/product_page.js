class ProductPage {

	constructor() {
	
		this.viewModel = new ProductViewModel();

		this.metaDataTable = new MetaDataTable();


		this.classDeleteRow = 'product_page_delete_row';

		this.classShowFormProduct = 'product_page_show_product_form';
		this.datatableReference = {}; // buat reference datatable

		this.productService = new ProductService();
	}





	Init() {

		try {

			let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery
		//	let productCategoryService = new ProductCategoryService();
			let metaDataResponseTableCategory = new MetaDataTable();
			metaDataResponseTableCategory.ad_org_uu = localStorage.getItem('ad_org_uu');

			let allHtml = `
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head kt-portlet__head--lg">
			<div  class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
			<i class="kt-font-brand flaticon2-line-chart"></i>
			</span>
			<h3 class="kt-portlet__head-title">
			Produk
			</h3>
			</div>
			<div  class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-wrapper">
			<div class="kt-portlet__head-actions">
			<button type="button" class="`+ thisObject.classShowFormProduct + ` btn btn-bold btn-label-brand btn-sm" name="new_record" >
			<i class="la la-plus"></i>New Record</button>	
		
			</div>
			</div>
			</div>
			</div>
			<div class="kt-portlet__body" >
			<table class="table table-striped- table-bordered table-hover table-checkable" id="wx_product_table">
			
			</thead>
			</table>
			</div>
		</div>	
		`;


			this.metaDataTable.ad_org_uu = localStorage.getItem('ad_org_uu');

		

			$('#wx_fragment_content').html(allHtml);

			//this.ProductBomFormEventHandler();

			var table = $('#wx_product_table');

			// begin first table
			thisObject.productService.FindAll(thisObject.metaDataTable).then(function (productCMTList) {
				thisObject.datatableReference = table.DataTable({
					responsive: true,
					data: productCMTList,
					scrollX: true,
					columns: [
						{
							"className": 'details-control',
							"orderable": false,
							"data": null,
							"defaultContent": '<button  type="button" class="btn btn-outline-hover-info btn-icon"><i class="la la-plus-square-o"></i></button>'
						},
						{ title: 'SKU', data: 'm_product_wx_sku' },
						{ title: 'Nama', data: 'm_product_wx_name' },
						{ title: 'Kategori', data: 'm_product_category_wx_name' },
						{ title: 'Tipe', data: 'wypex_producttype_wx_name' },
						{ title: 'Action' }
					],
					columnDefs: [
						{
							targets: -1,
							title: 'Actions',
							orderable: false,
							render: function (data, type, full, meta) {
								return `
							<button id="bttn_row_edit`+ full.m_product_wx_m_product_uu + `" name="` + full.m_product_wx_m_product_uu + `" type="button"  class="` + thisObject.classShowFormProduct + ` btn btn-outline-hover-info btn-elevate btn-icon"><i class="la la-edit"></i></button>
							<button id="bttn_row_delete`+ full.m_product_wx_m_product_uu + `" value="` + full.m_product_wx_name + `" name="` + full.m_product_wx_m_product_uu + `" type="button"    class="` + thisObject.classDeleteRow + `  btn btn-outline-hover-info btn-elevate btn-icon"> <i class="la la-trash"></i></button>
							`;
							},
						},
					],
					"order": [[1, 'asc']],
					createdRow: function (row, data, index) {
						if (data.extn === '') {
							var td = $(row).find("td:first");
							td.removeClass('details-control');
						}
					},
					rowCallback: function (row, data, index) {
						//console.log('rowCallback');
					}

				});

				thisObject.EventHandler();


			});

		} catch (error) {
			apiInterface.Log(this.constructor.name, this.Init.name, error);
		}


	}

	EventHandler() {

		let thisObject = this;


		$('.' + thisObject.classDeleteRow).off('click');
		$('.' + thisObject.classDeleteRow).on('click', function () {
			let nameDeleted = $(this).val();
			let uuid = $(this).attr('name');
			let deleteWidget = new DialogBoxDeleteWidget('Hapus Data', nameDeleted, uuid, thisObject);

		});

		$('.' + thisObject.classShowFormProduct).off('click');
		$('.' + thisObject.classShowFormProduct).on('click', function (e) {

			try {
				let attrName = $(this).attr('name');

				if (attrName !== 'new_record') {

					let dataEdited = thisObject.datatableReference.row($('#bttn_row_edit' + attrName).parents('tr')).data();


					thisObject.componentModel = HelperService.ConvertRowDataToViewModel(dataEdited);

				} else {

					thisObject.componentModel = new ProductViewModel();
					

				}
				let uomForm = new ProductForm(thisObject.componentModel, thisObject.datatableReference);

			} catch (error) {
				apiInterface.Log(thisObject.constructor.name, thisObject.EventHandler.name, error)
			}

		});

		thisObject.datatableReference.off('draw');
		thisObject.datatableReference.on('draw', function () {

			thisObject.EventHandler();
		});

		// Add event listener for opening and closing details
		$('#wx_product_table tbody').off('click');
		$('#wx_product_table tbody').on('click', 'td.details-control', function () {
			var tr = $(this).closest('tr');
			var row = thisObject.datatableReference.row(tr);
			let mProductParent = row.data();
			if (row.child.isShown()) {
				// This row is already open - close it
				row.child.hide();
				$(this).html('<button  type="button" class="btn btn-outline-hover-info btn-icon"><i class="la la-plus-square-o"></i></button>');

			}
			else {

				let tabHtml = `<ul class="nav nav-tabs  nav-tabs-line" role="tablist">` +
					thisObject.GenerateTabPanel(mProductParent) +
					`</ul>
							<div class="tab-content">`+
					thisObject.GenerateContentPanel(mProductParent) +
					`</div>`;
				row.child(tabHtml).show();

				// change menjadi button minus
				$(this).html('<button  type="button" class="btn btn-outline-hover-info btn-icon"><i class="la la-minus-square-o"></i></button>');

				$('a[data-toggle="tab"]').off('shown.bs.tab');
				$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {

					if (e.target.hash === '#tab_c_uom_conversion_' + mProductParent.m_product_wx_m_product_uu) {
						let uomConversionPage = new UomConversionPage(mProductParent.m_product_wx_m_product_uu, 'tab_c_uom_conversion_' + mProductParent.m_product_wx_m_product_uu);
						uomConversionPage.Init();

					}

					if (e.target.hash === '#tab_m_product_bom_' + mProductParent.m_product_wx_m_product_uu) {
						let productBomPage = new ProductBomPage(mProductParent.m_product_wx_m_product_uu, 'tab_m_product_bom_' + mProductParent.m_product_wx_m_product_uu);
						productBomPage.Init();

					}

					if (e.target.hash === '#tab_m_productprice_' + mProductParent.m_product_wx_m_product_uu) {
						let productpricePage = new ProductpricePage(mProductParent.m_product_wx_m_product_uu, 'tab_m_productprice_' + mProductParent.m_product_wx_m_product_uu);
						productpricePage.Init();

					}



				});

				/// set awal tab yang pertama terbuka harus di set tablenya

				let tabUomConversion = $('a[href="#' + 'tab_c_uom_conversion_' + mProductParent.m_product_wx_m_product_uu + '"]');
				if (tabUomConversion.length > 0) {

					if ($(tabUomConversion[0]).hasClass('active')) {
						let uomConversionPage = new UomConversionPage(mProductParent.m_product_wx_m_product_uu, 'tab_c_uom_conversion_' + mProductParent.m_product_wx_m_product_uu);
						uomConversionPage.Init();

					}

				}

				let tabProductBom = $('a[href="#' + 'tab_m_product_bom_' + mProductParent.m_product_wx_m_product_uu + '"]');
				if (tabProductBom.length > 0) {

					if ($(tabProductBom[0]).hasClass('active')) {
						let productBomPage = new ProductBomPage(mProductParent.m_product_wx_m_product_uu, 'tab_m_product_bom_' + mProductParent.m_product_wx_m_product_uu);
						productBomPage.Init();

					}

				}

				let tabProductPrice = $('a[href="#' + 'tab_m_productprice_' + mProductParent.m_product_wx_m_product_uu + '"]');
				if (tabProductPrice.length > 0) {

					if ($(tabProductPrice[0]).hasClass('active')) {
						let productpricePage = new ProductpricePage(mProductParent.m_product_wx_m_product_uu, 'tab_m_productprice_' + mProductParent.m_product_wx_m_product_uu);
						productpricePage.Init();

					}

				}



			}



		});



	}

	/**
	 * Delete pada table, sebenarnya hanya menandai wx_isdeleted = 'Y'
	 * @param {*} dataObject dataObject pada datatable 
	 */
	async Delete(uuidDeleted) {
		let insertSuccess = 0;
		let thisObject = this;
		try {
			let sqlstatementDeleteProduct = HelperService.SqlDeleteStatement('m_product', 'm_product_uu', uuidDeleted);
			let sqlstatementDeleteProductBom =HelperService.SqlDeleteStatement('m_product_bom', 'm_product_uu', uuidDeleted);
			let sqlStatementDeleteProductPrice = HelperService.SqlDeleteStatement('m_productprice', 'm_product_uu', uuidDeleted);
			let sqlstatementConversion =HelperService.SqlDeleteStatement('c_uom_conversion', 'm_product_uu', uuidDeleted);

			insertSuccess += await apiInterface.ExecuteSqlStatement(sqlstatementDeleteProduct);
			insertSuccess += await apiInterface.ExecuteSqlStatement(sqlstatementDeleteProductBom);
			insertSuccess += await apiInterface.ExecuteSqlStatement(sqlStatementDeleteProductPrice);

			insertSuccess += await apiInterface.ExecuteSqlStatement(sqlstatementConversion);


			if (insertSuccess) {
				// delete row pada datatable
				thisObject.datatableReference
					.row($('#bttn_row_delete' + uuidDeleted).parents('tr'))
					.remove()
					.draw();


			}
		} catch (error) {
			apiInterface.Log(this.constructor.name, this.Delete.name, error);
		}

		return insertSuccess;
	}



	/**
	 * Generate tab panel, ketika ternyata product adalam bom maka munculkan tabnya
	 * @param {*} mProductParent 
	 */
	GenerateTabPanel(mProductParent) {
		let tabPanelHtml = '';

		try {
			let arrayTabPanel = [{ link: 'tab_m_productprice_' + mProductParent.m_product_wx_m_product_uu, title: 'Harga Pricelist' }, { link: 'tab_c_uom_conversion_' + mProductParent.m_product_wx_m_product_uu, title: 'Konversi Satuan' }];

			if (mProductParent.m_product_wx_isbom === 'Y') {
				arrayTabPanel.unshift({ link: 'tab_m_product_bom_' + mProductParent.m_product_wx_m_product_uu, title: 'Produk BOM' });
			}

			let index = 0;
			_.forEach(arrayTabPanel, function (tab) {
				let activeClass = "";
				if (index === 0) {
					activeClass = " active";
				}
				tabPanelHtml += `<li class="nav-item ">
									<a class="nav-link` + activeClass + `" data-toggle="tab" href="#` + tab.link + `" role="tab">` + tab.title + `</a>
								</li>`;
				index++;

			});

		} catch (error) {
			apiInterface.Log(this.constructor.name, this.GenerateTabPanel.name, error);
		}

		return tabPanelHtml;
	}

	/**
	 * Generate content panel , ketika ternyata product adalam bom maka munculkan contentpanelnya
	 * @param {*} mProductParent 
	 */
	GenerateContentPanel(mProductParent) {
		let tabPanelHtml = '';

		try {
			let arrayTabPanel = [{ link: 'tab_m_productprice_' + mProductParent.m_product_wx_m_product_uu, title: 'Harga Pricelist' }, { link: 'tab_c_uom_conversion_' + mProductParent.m_product_wx_m_product_uu, title: 'Konversi Satuan' }];

			if (mProductParent.m_product_wx_isbom === 'Y') {
				arrayTabPanel.unshift({ link: 'tab_m_product_bom_' + mProductParent.m_product_wx_m_product_uu, title: 'Produk BOM' });
			}
			let index = 0;
			_.forEach(arrayTabPanel, function (tab) {
				let activeClass = "";
				if (index === 0) {
					activeClass = " active";
				}
				tabPanelHtml += `<div class="tab-pane` + activeClass + `" id="` + tab.link + `" role="tabpanel">		
								</div>`;
				index++;

			});

		} catch (error) {
			apiInterface.Log(this.constructor.name, this.GenerateContentPanel.name, error);
		}

		return tabPanelHtml;

	}




}
