class InoutlineForm {

	/**
	 * 
	
	 * @param {InoutViewModel}
	 * @param {datatableReference} datatableReference data tabel pada inout page, sebagai reference untuk mengupdate table tersebut
	 * @param {c_doctype} c_doctype_id 4000 ->MMR, 3000 -> MMS 
	 * 
	 */
	constructor( inoutlineViewModel,  datatableReference, c_doctype_id) {
	
		this.titleForm = '';
	
		this.viewModel = inoutlineViewModel;
	
		this.labelJumlahProductReceiptOrOutBasedUom = '';
		this.labelJumlahProductReceiptOrOut = '';
	
		this.c_doctype_id = 0;
		this.c_order_wx_c_doctype_id = 0;
		//this.labelPoReference = 'No PO Vendor';// No PO Vendor
		this.poReferenceHtml = '';
		if (c_doctype_id === 4000) {

			this.c_doctype_id = c_doctype_id;
		
			this.titleForm = 'Penerimaan Barang';
			this.labelJumlahProductReceiptOrOutBasedUom = 'Jumlah Produk yang diterima dengan satuan dasar';
			this.labelJumlahProductReceiptOrOut = 'Jumlah Produk Yang Diterima';
			
		}

		if (c_doctype_id === 3000) {

			this.c_doctype_id = c_doctype_id;
		
			this.titleForm = 'Keluar Barang';
			this.labelJumlahProductReceiptOrOutBasedUom = 'Jumlah Produk yang keluar dengan satuan dasar';
			this.labelJumlahProductReceiptOrOut = 'Jumlah Produk Yang Keluar';
		
		}
	

		this.metaDataTable = new MetaDataTable();

		this.datatableReference = datatableReference; // buat reference datatable

	
		this.inoutlineService = new InoutlineService();
		this.orderlineService = new OrderlineService();
		this.uomconversionService = new UomConversionService();

	
		this.idFormModal = 'm_inoutline_form_wx_';
		this.idFormValidation = 'm_inoutline_form_validation_';
		this.classNameFormControl = 'm-inoutline-form-control';
		this.selectorClassNameFormControl = '.' + this.classNameFormControl;
	
		this.idButtonFormSubmit = 'm_inoutline_form_submit_wx_';
		
		this.idButtonGenerateItemBarang = 'bttn_generate_inoutline';
		this.prefixContent = 'inoutline_page_'; // prefix content untuk child page 

		this.validation = {

			id_form: this.idFormValidation,


			
			m_inoutline_wx_qtyentered: {
				required: true
			},


		};

		this.uomConversionCMTList = [];


		this.Init();


	
		
	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery



		HelperService.InsertReplaceFormModal(thisObject.idFormModal, thisObject.GenerateFormInput());

		HelperService.InjectViewModelToForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.viewModel);
	
		thisObject.uomconversionService.FindUomConversionViewModelByMProductUuAndCUomUuAndCUomToUu(thisObject.viewModel.m_product.m_product_uu, thisObject.viewModel.c_uom_based.c_uom_uu,thisObject.viewModel.c_uom.c_uom_uu )
		.then(function(uomConversionCMTList){
			thisObject.uomConversionCMTList = uomConversionCMTList;

		});


		//set text uom name , basedUomname
		$('#' + thisObject.idFormValidation + ' #c_uom_wx_name').text(thisObject.viewModel.c_uom.name);
		$('#' + thisObject.idFormValidation + ' #c_uom_based_wx_name').text(thisObject.viewModel.c_uom_based.name);

		this.EventHandler();
	}




	/**
	 * 

	 */
	async Save() {
		let thisObject = this;

		let insertSuccess = 0;
		if (HelperService.CheckValidation(thisObject.validation)) {

			thisObject.viewModel = HelperService.GetViewModelFromForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.viewModel);
			//thisObject.viewModel.m_inoutline.qtyentered = $('#' + thisObject.idFormValidation + ' ' + '#m_inoutline_wx_qtyentered').val();
			//thisObject.viewModel.m_inoutline.movementqty = $('#' + thisObject.idFormValidation + ' ' + '#m_inoutline_wx_movementqty').val();
		
			insertSuccess = await thisObject.inoutlineService.SaveUpdate(thisObject.viewModel.m_inoutline, thisObject.datatableReference);
			// jika sukses tersimpan 
			if (insertSuccess > 0) {

				$('#' + thisObject.idFormModal).modal('hide');


			}
		
		}


	}

	GenerateFormInput() {
		let thisObject = this;

		return `
	<div class="modal fade" id="` + thisObject.idFormModal + `" tabindex="-1" role="dialog"
		aria-labelledby="product-form-labelled" style="display: none;" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="product-form-labelled">`+ thisObject.titleForm + `</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<form id="`+ thisObject.idFormValidation + `" class="kt-form">
					<div class="modal-body">
	
						<div class="form-group">
							<label>No</label>
							<input id="m_inoutline_wx_line" type="text"
								class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp">
	
						</div>
	
						<div class="form-group">
							<label>Produk</label>
							<input disabled="disabled" id="m_product_wx_name" type="text"
								class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp">
							<span class="form-text text-muted"></span>
							<input type="hidden" class="` + thisObject.classNameFormControl + `"
								id="m_product_wx_m_product_uu">
						</div>
						<div class="form-group">
							<label>`+ thisObject.labelJumlahProductReceiptOrOut +`</label>
							<div class="input-group">
								<input id="m_inoutline_wx_qtyentered" type="text"
									class="` + thisObject.classNameFormControl + ` form-control"
									aria-describedby="emailHelp">
								<div class="input-group-prepend"><span id="c_uom_wx_name" class="input-group-text"></span>
								</div>
	
							</div>
						</div>
						<div class="form-group">
							<label>`+ thisObject.labelJumlahProductReceiptOrOutBasedUom +`</label>
							<div class="input-group">
								<input disabled="disabled" id="m_inoutline_wx_movementqty" type="text"
									class="` + thisObject.classNameFormControl + ` form-control"
									aria-describedby="emailHelp">
								<div class="input-group-prepend"><span id="c_uom_based_wx_name"
										class="input-group-text"></span></div>
	
							</div>
						</div>
						<input hidden id="m_inoutline_wx_isreturn">
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button id="`+ thisObject.idButtonFormSubmit + `" type="submit" class="btn btn-primary">Save
							changes</button>
					</div>
	
				</form>
			</div>
		</div>
	</div>
	
	`;
	}


	EventHandler() {
		let thisObject = this;

		$('#' + thisObject.idFormValidation).on('submit', function (e) {
			thisObject.Save();
			e.preventDefault();

		});


		$('#' + thisObject.idFormValidation + ' #m_inoutline_wx_qtyentered').off('input');
		$('#' + thisObject.idFormValidation + ' #m_inoutline_wx_qtyentered').on('input', function () {

			let qtyentered = $(this).val();

			let movementQty = thisObject.orderlineService.CalculateQtyBasedUomConversion(thisObject.viewModel.c_uom.c_uom_uu,qtyentered,thisObject.uomConversionCMTList);
			
			$('#' + thisObject.idFormValidation + ' #m_inoutline_wx_movementqty').val(movementQty);

		});




		$('#' + thisObject.idFormModal).modal('show');


	}



}
