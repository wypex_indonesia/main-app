class PaymentForm {

	/**
	 * 
	
	 * @param {string} idFragmentContent name id fragment content untuk meletakkan html content dari form_html
	 * @param {InvoiceComponentModel}
	 * @param {datatableReference} datatableReference data tabel pada inout page, sebagai reference untuk mengupdate table tersebut
	 * @param {c_doctype} inouttype MMS (MM Shipment), MMR (MM Receipt)
	 */
	constructor(paymentCM, datatableReference, paymentType) {

		this.titleForm = '';
		this.paymentType = paymentType;
		this.componentModel = paymentCM;

		this.labelNoDocument = '';
		this.labelVendorOrCustomer = '';

		this.c_doctype_id = 0;
		this.c_invoice_wx_c_doctype_id = 0;
		//this.labelPoReference = 'No PO Vendor';// No PO Vendor
		this.paymentReferenceHtml = '';
		this.labelNoDocument = 'No Dokumen';
		this.labelNoOrder = '';
		this.labelNoInvoice = '';


		this.metaDataTable = new MetaDataTable();

		this.datatableReference = datatableReference; // buat reference datatable

		this.paymentService = new PaymentService();
		this.paymentRuleService = new WxPaymentruleService();
		this.bankaccountService = new BankaccountService();
		this.invoiceService = new InvoiceService();


		this.idFormModal = 'c_payment_form_wx_';
		this.idFormValidation = 'c_payment_form_validation_';
		this.classNameFormControl = 'c-payment-form-control';
		this.selectorClassNameFormControl = '.' + this.classNameFormControl;
		this.idDialogBox = 'c_payment_dialog_box_wx_';

		this.idNewRecord = 'new_record_inout_wx_';
		this.idButtonFormSubmit = 'c_payment_form_submit_wx_';
		this.idButtonRowStatusDialog = 'bttn_row_status'; // button row status pada datatable order page
		this.idRadioDocStatus = '';// idGroupRadio Status pada document status dialog
		this.idButtonGenerateItemBarang = 'bttn_generate_paymentline';
		this.prefixContent = 'paymentline_page_'; // prefix content untuk child page 
		this.idWidgetSearch = 'payment_form_input_search';
		//this.labelNoInout = '';
		if (paymentType === 'APP') {

			this.c_doctype_id = 5001;
			this.c_invoice_wx_c_doctype_id = 5000;
			this.titleForm = 'Pembayaran Vendor';

			this.labelVendorOrCustomer = 'Vendor';
			this.labelNoOrder = 'No Purchase Order';
			this.labelNoInvoice = 'No Invoice';
			//	this.labelNoInout = 'No Material Receipt';
			this.paymentReferenceHtml = `<div class="form-group">
	<label>No Invoice Dari Vendor</label>
	<input disabled="disabled" id="c_invoice_wx_invoicereference" type="text"   class="` + this.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
	<span class="form-text text-muted">nomor dokumen invoice dari vendor</span>
</div>`;
		}

		if (paymentType === 'ARR') {

			this.c_doctype_id = 6001;
			this.c_invoice_wx_c_doctype_id = 6000;
			this.titleForm = 'Penerimaan';
			//	this.labelNoInout = 'No Surat Jalan';
			this.labelVendorOrCustomer = 'Customer';
			this.labelNoOrder = 'No Sales Order';
			this.labelNoInvoice = 'No Invoice';
		}

		this.validation = {

			id_form: this.idFormValidation,


			c_invoice_wx_documentno: {
				required: true
			},
			c_bankaccount_wx_c_bankaccount_uu: {
				required: true
			},



		};




		this.Init();

	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery



		thisObject.GenerateFormInput();

	}






	async	GenerateFormInput() {
		let thisObject = this;
		let metadataBankaccount = new MetaDataTable();
		metadataBankaccount.ad_org_uu = localStorage.getItem('ad_org_uu');

		try {
			let bankAccountOptions = new BankaccountOptions(thisObject.classNameFormControl);
			let optionsBankHtml = await bankAccountOptions.Html();


			let html = `
	<div class="modal fade" id="` + thisObject.idFormModal + `" tabindex="-1" role="dialog"
		aria-labelledby="product-form-labelled" style="display: none;" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="product-form-labelled">`+ thisObject.titleForm + `</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<form id="`+ thisObject.idFormValidation + `" class="kt-form">
					<div class="modal-body">
	
						<div class="form-group">
							<label>No Dokumen</label>
							<input disabled="disabled" id="c_payment_wx_documentno" type="text"
								class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp">
							<span class="form-text text-muted">No Dokumen dibuat otomatis</span>
						</div>
						<div class="form-group form-group-marginless">
							<label>`+ thisObject.labelNoInvoice + `</label>
	
							<div class="input-group">
								<input disabled="disabled" type="text"
									class="` + thisObject.classNameFormControl + ` form-control"
									id="c_invoice_wx_documentno" placeholder="Cari ` + thisObject.labelNoInvoice + `">
								<div class="input-group-append">
									<span id="search_c_invoice" class="input-group-text"><i class="la la-search"></i></span>
								</div>
							</div>
							<input type="hidden" class="` + thisObject.classNameFormControl + `"
								id="c_invoice_wx_c_invoice_uu">
						</div>
						<div class="form-group form-group-marginless">
							<label>`+ thisObject.labelNoOrder + `</label>
							<input disabled="disabled" id="c_order_wx_documentno" type="text"
								class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp">
							<span class="form-text text-muted"></span>
							<input type="hidden" class="` + thisObject.classNameFormControl + `" id="c_order_wx_c_order_uu">
						</div>
						<div class="form-group">
							<label>`+ thisObject.labelVendorOrCustomer + `</label>
							<input disabled="disabled" id="c_bpartner_wx_name" type="text"
								class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp">
							<span class="form-text text-muted"></span>
							<input type="hidden" class="` + thisObject.classNameFormControl + `"
								id="c_bpartner_wx_c_bpartner_uu">
	
						</div>`+
				thisObject.paymentReferenceHtml + `
						<div class="form-group">
							<label>Grand Total </label>
							<input id="c_invoice_wx_grandtotal" type="text"
								class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp">
						</div>`+ optionsBankHtml + `
	
	
						<div class="form-group">
							<label>Deskripsi</label>
							<input id="c_payment_wx_description" type="text"
								class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp">
							<span class="form-text text-muted">Deskripsi</span>
						</div>
						<div class="form-group ">
							<label>Tanggal Transaksi</label>
							<input type="text" class="` + thisObject.classNameFormControl + ` datepicker form-control" id="c_payment_wx_dateacct" readonly="" placeholder="Select date">
						</div>	
						<div class="form-group">
							<label>Dokumen Status</label>
							<input disabled="disabled" id="c_payment_wx_docstatus" type="text"
								class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp">
							<span class="form-text text-muted">Status Dokumen DRAFT / COMPLETED / REVERSED</span>
						</div>
						<div class="form-group"  id="`+ thisObject.idWidgetSearch + `">
	
	
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button id="`+ thisObject.idButtonFormSubmit + `" type="submit" class="btn btn-primary">Save
							changes</button>
					</div>
				</form>
			</div>
		</div>
	</div>

		`;

			HelperService.InsertReplaceFormModal(thisObject.idFormModal, html);
			HelperService.InjectViewModelToForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.componentModel);

			thisObject.EventHandler();

		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.GenerateFormInput.name, error);
		}


	}


	EventHandler() {
		let thisObject = this;

		try {
			$('#' + thisObject.idFormValidation).off('submit');
			$('#' + thisObject.idFormValidation).on('submit', function (e) {
				thisObject.Save();
				e.preventDefault();

			});

			/** BEGIN EVENT SEARCH M_Product **************************************/
			$('#' + thisObject.idFormValidation + ' #search_c_invoice').off('click');
			$('#' + thisObject.idFormValidation + ' #search_c_invoice').on('click', function () {

				let functionGetResult = function (selectedInvoice) {
					$('#' + thisObject.idFormValidation + ' #c_order_wx_c_order_uu').val(selectedInvoice.c_order_wx_c_order_uu);

					$('#' + thisObject.idFormValidation + ' #c_order_wx_documentno').val(selectedInvoice.c_order_wx_documentno);

					$('#' + thisObject.idFormValidation + ' #c_bpartner_wx_name').val(selectedInvoice.c_bpartner_wx_name);

					$('#' + thisObject.idFormValidation + ' #c_bpartner_wx_c_bpartner_uu').val(selectedInvoice.c_bpartner_wx_c_bpartner_uu);
					$('#' + thisObject.idFormValidation + ' #c_invoice_wx_c_invoice_uu').val(selectedInvoice.c_invoice_wx_c_invoice_uu);

					$('#' + thisObject.idFormValidation + ' #c_invoice_wx_documentno').val(selectedInvoice.c_invoice_wx_documentno);
					$('#' + thisObject.idFormValidation + ' #c_invoice_wx_grandtotal').val(selectedInvoice.c_invoice_wx_grandtotal);
					$('#' + thisObject.idFormValidation + ' #c_invoice_wx_invoicereference').val(selectedInvoice.c_invoice_wx_invoicereference);

				}

				let invoiceSearchWidget = new InvoiceSearchWidget(thisObject.idWidgetSearch, functionGetResult,thisObject.labelNoInvoice, thisObject.c_invoice_wx_c_doctype_id);
				invoiceSearchWidget.Init();


			});

			$('#' + thisObject.idFormModal).modal('show');

		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.EventHandler.name, error);
		}


	}

	/**
		 * 
		
		 */
	async Save() {
		let thisObject = this;
		try {
			let insertSuccess = 0;
			if (HelperService.CheckValidation(thisObject.validation)) {

				thisObject.componentModel = HelperService.GetViewModelFromForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.componentModel);
				thisObject.componentModel.c_doctype.c_doctype_id = thisObject.c_doctype_id;

				insertSuccess += await thisObject.paymentService.SaveUpdate(thisObject.componentModel, thisObject.datatableReference);
				// jika sukses tersimpan 
				if (insertSuccess > 0) {

					$('#' + thisObject.idFormModal).modal('hide');


				}

			}

		} catch (err) {
			apiInterface.Log(this.constructor.name, this.Save.name, err);
		}
	}

}
