class OrderlineForm {

	/**
	 * 
	 * @param {c_order} cOrderParent orderParent
	 * @param {string} idFragmentContent name id fragment content untuk meletakkan html content dari form_html
	 * @param {OrderlineViewModel}
	 */
	constructor(cOrderParent, idFragmentContent, orderlineViewModel, datatableReference) {



		this.cOrderParent = cOrderParent;


		this.viewModel = orderlineViewModel;

		this.orderlineService = new OrderlineService();
		this.uomService = new UomService();
		this.productService = new ProductService();
		this.taxService = new TaxService();
		this.idFragmentContent = idFragmentContent;
		this.selectedProduct = {}; // current product yang sedang dipilih, product_viewmodel_table
		this.idFormModal = 'c_orderline_form_wx_';
		this.idFormValidation = 'c_orderline_form_validation_' + cOrderParent.c_order_wx_c_order_uu;
		this.classNameFormControl = 'c-orderline-form-control';
		this.selectorClassNameFormControl = '.' + this.classNameFormControl;
		this.idWidgetSearch = 'orderline_form_widget_search';

		this.idButtonFormSubmit = 'c_orderline_form_submit_wx_' + cOrderParent.c_order_wx_c_order_uu;
		this.table_productprice_viewmodel = [];
		this.table_uomconversion_viewmodel = [];
		this.table_tax_viewmodel = [];
		this.datatableReference = datatableReference;
		let lengthRow = this.datatableReference.data().length;
		if (!this.viewModel.c_orderline.c_orderline_uu) {
			this.viewModel.c_orderline.line = ++lengthRow;
		}
		this.inputPriceactual;
		this.inputPriceentered;
		this.inputLinenetamt;
		this.ctrlSelectedUom;
		this.ctrlBaseUom;
		this.validation = {
			id_form: this.idFormValidation,
			m_product_wx_name: {
				required: true
			},
			c_uom_wx_c_uom_uu: {
				required: true
			},

			c_tax_wx_c_tax_uu: {
				required: true
			},

			c_orderline_wx_qtyordered: {
				required: true
			},

			c_orderline_wx_priceactual: {
				required: true
			},


		};

		this.Init();

	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery

		thisObject.GenerateFormInput();
	
	}




	/**
	 * 
	
	 */
	async SaveUpdate() {
		let thisObject = this;
		let insertSuccess = 0;

		try {
			if (HelperService.CheckValidation(thisObject.validation)) {
				thisObject.calculatePriceactual();
				thisObject.calculateLinenetamt();

				thisObject.calculateLinetaxamt();

				thisObject.viewModel = HelperService.GetViewModelFromForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.viewModel);

				insertSuccess += await thisObject.orderlineService.SaveUpdate(thisObject.viewModel, thisObject.datatableReference);
				if (insertSuccess > 0) {
					$('#' + thisObject.idFormModal).modal('hide');
				}
			}
		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.SaveUpdate.name, error);
		}





	}

	async GenerateFormInput() {
		let thisObject = this;

		let metaDataTableTax = new MetaDataTable();
		metaDataTableTax.ad_org_uu = localStorage.getItem('ad_org_uu');

		let taxtOptionsWidget = new TaxOptions(thisObject.classNameFormControl);
		let taxOptionsHtml = await taxtOptionsWidget.Html();

		try {
			let html = `
			<div class="modal fade" id="` + thisObject.idFormModal + `" tabindex="-1" role="dialog"
				aria-labelledby="product-form-labelled" style="display: none;" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="product-form-labelled">Orderline Form</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							</button>
						</div>
						<form id="`+ thisObject.idFormValidation + `" class="kt-form">
							<div class="modal-body">
			
								<div class="form-group">
									<label>No</label>
									<input id="c_orderline_wx_line" type="text"
										class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp">
									<span class="form-text text-muted">Nomor urut</span>
								</div>
								<div class="form-group form-group-marginless">
									<label>Produk</label>
									<div class="input-group">
										<input disabled="disabled" type="text"
											class="` + thisObject.classNameFormControl + ` form-control" id="m_product_wx_name"
											placeholder="Cari produk">
										<div class="input-group-append">
											<span id="search_m_product" class="input-group-text"><i class="la la-search"></i></span>
										</div>
									</div>
									<input type="hidden" class="` + thisObject.classNameFormControl + `"
										id="m_product_wx_m_product_uu">
								</div>
								<div class="form-group ">
									<label>Satuan</label>
									<select class="` + thisObject.classNameFormControl + ` form-control"
										id="c_uom_wx_c_uom_uu"></select>
								</div>
								<div class="form-group ">
									<label>Dafta Harga</label>
			
									<select class="form-control" id="m_pricelist_version_wx_m_pricelist_version_uu"></select>
								</div>
								<div class="form-group">
									<label>Jumlah Produk Yang Di Order</label>
									<div class="input-group">
										<input id="c_orderline_wx_qtyentered" type="text"
											class="` + thisObject.classNameFormControl + ` form-control"
											aria-describedby="emailHelp">
										<div class="input-group-prepend"><span class="input-group-text selected-uom"></span></div>
									</div>
								</div>
								<div class="form-group">
									<label>Jumlah Produk Dengan Satuan Dasar</label>
									<div class="input-group">
										<input disabled="disabled" class="` + thisObject.classNameFormControl + ` form-control"
											placeholder="Jumlah Produk Dengan Satuan Dasar" type="number"
											id="c_orderline_wx_qtyordered">
										<div class="input-group-prepend"><span class="input-group-text base-uom"></span></div>
									</div>
								</div>
			
								<div class="form-group">
									<label>Harga Produk</label>
									<div class="input-group">
										<input id="c_orderline_wx_priceentered" type="text"
											class="` + thisObject.classNameFormControl + ` wx-format-money  form-control"
											aria-describedby="emailHelp">
										<div class="input-group-prepend"><span class="input-group-text">/</span><span
												class="input-group-text selected-uom"></span></div>
									</div>
								</div>
								<div class="form-group">
									<label>Harga Produk Dengan Satuan Dasar</label>
									<div class="input-group">
										<input disabled="disabled" id="c_orderline_wx_priceactual" type="text"
											class="` + thisObject.classNameFormControl + ` wx-format-money form-control"
											aria-describedby="emailHelp">
										<div class="input-group-prepend"><span class="input-group-text">/</span><span
												class="input-group-text base-uom"></span></div>
									</div>
								</div>`+ taxOptionsHtml + `
			
								<div class="form-group">
									<label>Harga Net Total</label>
									<input disabled="disabled" id="c_orderline_wx_linenetamt" type="text"
										class="` + thisObject.classNameFormControl + ` wx-format-money form-control"
										aria-describedby="emailHelp">
									<span class="form-text text-muted">Harga Net total</span>
								</div>
								<div class="form-group">
									<label>Tax Amount</label>
									<input disabled="disabled" id="c_orderline_wx_linetaxamt" type="text"
										class="` + thisObject.classNameFormControl + ` wx-format-money form-control"
										aria-describedby="emailHelp">
									<span class="form-text text-muted">Jumlah Tax Amount</span>
								</div>
			
								<input hidden id="c_orderline_wx_isreturn">
								<div class="form-group" id="`+ thisObject.idWidgetSearch + `">
			
			
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								<button id="`+ thisObject.idButtonFormSubmit + `" type="submit" class="btn btn-primary">Save
									changes</button>
							</div>
			
						</form>
					</div>
				</div>
			</div>
		`;

			HelperService.InsertReplaceFormModal(thisObject.idFormModal, html);

			thisObject.inputPriceactual = $('#' + thisObject.idFormValidation + ' #c_orderline_wx_priceactual');
			thisObject.inputPriceentered = $('#' + thisObject.idFormValidation + ' #c_orderline_wx_priceentered');
			thisObject.inputLinenetamt = $('#' + thisObject.idFormValidation + ' #c_orderline_wx_linenetamt');
			thisObject.ctrlSelectedUom = $('#' + thisObject.idFormValidation + ' .selected-uom');
			thisObject.ctrlBaseUom = $('#' + thisObject.idFormValidation + ' .base-uom');



			// jika bukan new record, ambil data-data dibawah untuk select options
			if (thisObject.viewModel.c_orderline.c_orderline_uu) {
				let productCMTList = await thisObject.productService.FindProductViewModelByMProductUu(thisObject.viewModel.m_product.m_product_uu);


				thisObject.selectedProduct = productCMTList[0];
				await thisObject.GeneratePricelistVersionByProduct(thisObject.viewModel.m_product.m_product_uu);
				await thisObject.GenerateUomBasedOnProduct(thisObject.selectedProduct);

				// masukkan nama satuan di class base-uom dan selected-uom
				thisObject.ctrlSelectedUom.text(thisObject.viewModel.c_uom.name);
				thisObject.ctrlBaseUom.text(thisObject.selectedProduct.c_uom_wx_name);

				HelperService.InjectViewModelToForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.viewModel);



			} else {


				HelperService.InjectViewModelToForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.viewModel);


			}
			thisObject.DisableFieldIsReturn();


		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.GenerateFormInput.name, error);
		}


		thisObject.PageEventHandler();



	}

	PageEventHandler() {
		let thisObject = this;



		$('#' + thisObject.idFormValidation).off('submit');
		$('#' + thisObject.idFormValidation).on('submit', function (e) {
			thisObject.SaveUpdate();
			e.preventDefault();

		});


		/** BEGIN EVENT SEARCH M_Product **************************************/
		$('#' + thisObject.idFormValidation + ' #search_m_product').off('click');
		$('#' + thisObject.idFormValidation + ' #search_m_product').on('click', function () {

			let functionGetResult = function (selectedProduct) {

				return thisObject.functionCallbackProductSearchWidget(selectedProduct);

			}
			/*let functionGetResult = function (selectedProduct) {
				thisObject.selectedProduct = selectedProduct;
				thisObject.GenerateUomBasedOnProduct(thisObject.selectedProduct);
				thisObject.GeneratePricelistVersionByProduct(thisObject.selectedProduct);
				thisObject.SetPriceBasedOnPricelistVersion();

				$('#' + thisObject.idFormValidation + ' #m_product_wx_name').val(thisObject.selectedProduct.m_product_wx_name);
				thisObject.ctrlBaseUom.text(thisObject.selectedProduct.c_uom_wx_name);
				$('#' + thisObject.idFormValidation + ' #m_product_wx_m_product_uu').val(thisObject.selectedProduct.m_product_wx_m_product_uu);

				thisObject.ctrlSelectedUom.text(thisObject.selectedProduct.c_uom_wx_name);
			};*/
			let product_search_widget = new ProductSearchWidget(thisObject.idWidgetSearch, functionGetResult);
			product_search_widget.Init();
		});


		/** END EVENT SEARCH M_PRODUCT **************************************/

		/*** EVENT CHANGE C_UOM ********/
		/** mengconversi harga produk, jumlah produk */

		$('#' + thisObject.idFormValidation + ' #c_uom_wx_c_uom_uu').off('click');
		$('#' + thisObject.idFormValidation + ' #c_uom_wx_c_uom_uu').on('click', function () {

			thisObject.calculateQtyBasedUomConversion();

			thisObject.calculatePriceactual();
			thisObject.calculateLinenetamt();

			thisObject.calculateLinetaxamt();
			let textUomName = $('#' + thisObject.idFormValidation + ' #c_uom_wx_c_uom_uu  option:selected').text();
			thisObject.ctrlSelectedUom.text(textUomName);
		});

		$('#' + thisObject.idFormValidation + ' #c_orderline_wx_qtyentered').off('keyup');
		$('#' + thisObject.idFormValidation + ' #c_orderline_wx_qtyentered').on('keyup', function () {
			thisObject.calculateQtyBasedUomConversion();

			thisObject.calculatePriceactual();

			thisObject.calculateLinenetamt();

			thisObject.calculateLinetaxamt();

		});

		thisObject.inputPriceentered.off('keyup');
		thisObject.inputPriceentered.on('keyup', function () {

			thisObject.calculatePriceactual();

			thisObject.calculateLinenetamt();

			thisObject.calculateLinetaxamt();
		});

		$('#' + thisObject.idFormValidation + ' #c_tax_wx_c_tax_uu').off('change');
		$('#' + thisObject.idFormValidation + ' #c_tax_wx_c_tax_uu').on('change', function () {
			thisObject.calculateLinetaxamt();
		});
		/**
		 * pricelist version di click, maka akan mengubah data pada priceactual, 
		 * karena productprice hanya berdasarkan baseUom di product
	 */
		$('#' + thisObject.idFormValidation + ' #m_pricelist_version_wx_ m_pricelist_version_uu').off('change');
		$('#' + thisObject.idFormValidation + ' #m_pricelist_version_wx_ m_pricelist_version_uu').on('change', function () {
			thisObject.SetPriceBasedOnPricelistVersion();
			thisObject.calculatePriceactual();
			thisObject.calculateLinenetamt();
			thisObject.calculateLinetaxamt();

		});

		$('.wx-format-money').mask('#,##0', { reverse: true });

		$('#' + thisObject.idFormModal).modal('show');

	}

	/*calculatePriceentered() {
		let thisObject = this;
		let cUomUu = $('#' + thisObject.idFormValidation + ' #c_uom_wx_c_uom_uu').val();
		let priceActual = numeral(thisObject.inputPriceactual.val());

		if (thisObject.table_uomconversion_viewmodel) {
			const cUomConversionFound = _.find(thisObject.table_uomconversion_viewmodel, (cUomConversion) => {
				return cUomConversion.c_uomto_wx_c_uom_uu === cUomUu;
			});

			let priceEntered = 0;
			if (cUomConversionFound) {

				priceEntered = cUomConversionFound.dividerate * priceActual;
			} else {
				// tslint:disable-next-line:max-line-length
				priceEntered = priceActual;
			}
			$('#' + thisObject.idFormValidation + ' #c_orderline_wx_priceentered').val(priceEntered);
		}
	}*/


	async functionCallbackProductSearchWidget(selectedProduct) {
		let thisObject = this;
		thisObject.selectedProduct = selectedProduct;
		await thisObject.GenerateUomBasedOnProduct(selectedProduct);

		await thisObject.GeneratePricelistVersionByProduct(thisObject.selectedProduct);
		thisObject.SetPriceBasedOnPricelistVersion();

		$('#' + thisObject.idFormValidation + ' #m_product_wx_name').val(thisObject.selectedProduct.m_product_wx_name);
		thisObject.ctrlBaseUom.text(thisObject.selectedProduct.c_uom_wx_name);
		$('#' + thisObject.idFormValidation + ' #m_product_wx_m_product_uu').val(thisObject.selectedProduct.m_product_wx_m_product_uu);

		thisObject.ctrlSelectedUom.text(thisObject.selectedProduct.c_uom_wx_name);



	}

	calculatePriceactual() {
		let thisObject = this;
		let cUomUu = $('#' + thisObject.idFormValidation + ' #c_uom_wx_c_uom_uu').val();
		let priceEntered = numeral(thisObject.inputPriceentered.val()).value();

		if (thisObject.selectedProduct) {

			if (thisObject.selectedProduct.c_uom_wx_c_uom_uu === cUomUu) {
				thisObject.inputPriceactual.val(priceEntered);
			} else {
				let priceActual = 0;
				if (thisObject.table_uomconversion_viewmodel) {
					const cUomConversionFound = _.find(thisObject.table_uomconversion_viewmodel, (cUomConversion) => {
						return cUomConversion.c_uomto_wx_c_uom_uu === cUomUu;
					});

					if (cUomConversionFound) {

						priceActual = priceEntered / cUomConversionFound.c_uom_conversion_wx_dividerate;
					}
					thisObject.inputPriceactual.val(priceActual);
				}
			}

		}

	}
	/**
	 * Menghitung total jumlah, 
	 * dan set negatif jika retur
	 */
	calculateLinenetamt() {
		let thisObject = this;
	
		let priceActual = numeral(thisObject.inputPriceactual.val()).value();
		let qtyOrdered = $('#' + thisObject.idFormValidation + ' #c_orderline_wx_qtyordered').val();
		let linenetamt = priceActual * qtyOrdered;
		thisObject.inputLinenetamt.val(linenetamt);

	}


	/**
 * Menghitung total jumlah, 
 */
	calculateLinetaxamt() {
		let thisObject = this;
		let linenetamt = numeral(thisObject.inputLinenetamt.val()).value();
		let selectedTaxUu = $('#' + thisObject.idFormValidation + ' #c_tax_wx_c_tax_uu').val();
		let rate = $('#' + thisObject.idFormValidation + ' #c_tax_wx_c_tax_uu').find(':selected').data('rate');
		/*	let taxFound = _.find(thisObject.table_tax_viewmodel, (tableTax) => {
				return tableTax.c_tax_wx_c_tax_uu === selectedTaxUu;
			});*/

		let linetaxamt = 0;
		if (rate) {
			linetaxamt = rate / 100 * linenetamt;

		}

		$('#' + thisObject.idFormValidation + ' #c_orderline_wx_linetaxamt').val(linetaxamt);




	}


	/**
	 * Menghitung quantity berdasarkan selectedUom dan cUomConversion.
	 *
	 */
	calculateQtyBasedUomConversion() {

		let thisObject = this;
		let orderlineService = new OrderlineService();
		let cUomToUu = $('#' + thisObject.idFormValidation + ' #c_uom_wx_c_uom_uu').val();
		let qtyentered = $('#' + thisObject.idFormValidation + ' #c_orderline_wx_qtyentered').val();

		let qtyOrdered = orderlineService.CalculateQtyBasedUomConversion(cUomToUu, qtyentered, this.table_uomconversion_viewmodel);

		$('#' + thisObject.idFormValidation + ' #c_orderline_wx_qtyordered').val(qtyOrdered);

	}

	async GenerateUomBasedOnProduct(productViewModelTable) {
		// GENERATE UOM
		let thisObject = this;
		try {
			let uomConversionService = new UomConversionService();
			let metaDataResponseTableUom = new MetaDataTable();
			metaDataResponseTableUom.ad_org_uu = localStorage.getItem('ad_org_uu');
			metaDataResponseTableUom.otherFilters = [' AND c_uom_conversion.m_product_uu = \'' + productViewModelTable.m_product_wx_m_product_uu + '\''];

			let uomconversionCMTList = await uomConversionService.FindAll(metaDataResponseTableUom);
			thisObject.table_uomconversion_viewmodel = uomconversionCMTList;
			// extract uomtable pada productViewModelTable 

			let baseUomObject = {};
			_.forEach(productViewModelTable, function (value, key) {

				let tableAndColumn = key;
				let tableAndColumnArray = key.split('_wx_');
				let table = tableAndColumnArray[0];
				let column = tableAndColumnArray[1];
				if (table === 'c_uom') {

					tableAndColumn = table + '_wx_' + column;
					baseUomObject[tableAndColumn] = value;
				}

			});




			// karena optionsUom masih menggunakan uomFrom dan uomto, maka harus diconversi dulu, menjadi c_uom_wx_xxxxxx
			// dan table c_uom_conversion
			let newOptionsUom = [];
			newOptionsUom.push(baseUomObject);
			_.forEach(thisObject.table_uomconversion_viewmodel, function (uomConversionViewModel) {
				let newUomToObject = {};
				let newUomFromObject = {};
				_.forEach(uomConversionViewModel, function (value, key) {
					let tableAndColumn = key;
					let tableAndColumnArray = key.split('_wx_');
					let table = tableAndColumnArray[0];
					let column = tableAndColumnArray[1];
					if (table !== 'c_uom_conversion') {

						if (table === 'c_uomfrom') {
							let tableUomFrom = 'c_uom';

							tableAndColumn = tableUomFrom + '_wx_' + column;
							newUomFromObject[tableAndColumn] = value;
						}

						if (table === 'c_uomto') {
							let tableUomTo = 'c_uom';
							tableAndColumn = tableUomTo + '_wx_' + column;
							newUomToObject[tableAndColumn] = value;
						}

					}

				});
				newOptionsUom.push(newUomToObject);
				newOptionsUom.push(newUomFromObject);
			});

			let uniqueNewOptionsUom = _.uniqBy(newOptionsUom, 'c_uom_wx_c_uom_uu');

			let htmlOptionsUom = ''
			_.forEach(uniqueNewOptionsUom, function (uomObject) {
				// jika sama dengan baseUom dari product
				// set selected
				let selected = '';
				if (productViewModelTable.m_product_wx_c_uom_uu === uomObject.c_uom_wx_c_uom_uu) {
					selected = 'selected';
				}
				htmlOptionsUom += '<option value="' + uomObject.c_uom_wx_c_uom_uu + '"' + selected + '>' + uomObject.c_uom_wx_name + '</option>';
			});
			$('#' + thisObject.idFormValidation + ' #c_uom_wx_c_uom_uu').html(htmlOptionsUom);
			//HelperService.InjectViewModelToForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.viewModel);



		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.GenerateUomBasedOnProduct.name, error);
		}




	}

	DisableFieldIsReturn(){
		let thisObject = this;
		if(thisObject.viewModel.c_orderline.isreturn === 'Y'){
			$('#'+thisObject.idFormValidation + ' #c_orderline_wx_priceentered').prop('disabled', true);
			$('#'+thisObject.idFormValidation + ' #c_tax_wx_c_tax_uu').prop('disabled', true);

		}
	}



	/**
	 * Generate Uom, dengan baseUom produk, conversion Uom 
	 * @param {*} productViewModelTable  viewmodel seperti table
	 * 
	
	async GenerateUomBasedOnProduct(productViewModelTable) {
		// GENERATE UOM
		let thisObject = this;
		let uomConversionService = new UomConversionService();
		let metaDataResponseTableUom = new MetaDataTable();
		metaDataResponseTableUom.ad_org_uu = localStorage.getItem('ad_org_uu');
		metaDataResponseTableUom.otherFilters = [' AND c_uom_conversion.m_product_uu = \'' + productViewModelTable.m_product_wx_m_product_uu + '\''];

		uomConversionService.FindAll(metaDataResponseTableUom).then(function (uomconversionCMTList) {
			thisObject.table_uomconversion_viewmodel = uomconversionCMTList;
			// extract uomtable pada productViewModelTable 

			let baseUomObject = {};
			_.forEach(productViewModelTable, function (value, key) {

				let tableAndColumn = key;
				let tableAndColumnArray = key.split('_wx_');
				let table = tableAndColumnArray[0];
				let column = tableAndColumnArray[1];
				if (table === 'c_uom') {

					tableAndColumn = table + '_wx_' + column;
					baseUomObject[tableAndColumn] = value;
				}

			});




			// karena optionsUom masih menggunakan uomFrom dan uomto, maka harus diconversi dulu, menjadi c_uom_wx_xxxxxx
			// dan table c_uom_conversion
			let newOptionsUom = [];
			newOptionsUom.push(baseUomObject);
			_.forEach(thisObject.table_uomconversion_viewmodel, function (uomConversionViewModel) {
				let newUomToObject = {};
				let newUomFromObject = {};
				_.forEach(uomConversionViewModel, function (value, key) {
					let tableAndColumn = key;
					let tableAndColumnArray = key.split('_wx_');
					let table = tableAndColumnArray[0];
					let column = tableAndColumnArray[1];
					if (table !== 'c_uom_conversion') {

						if (table === 'c_uomfrom') {
							let tableUomFrom = 'c_uom';

							tableAndColumn = tableUomFrom + '_wx_' + column;
							newUomFromObject[tableAndColumn] = value;
						}

						if (table === 'c_uomto') {
							let tableUomTo = 'c_uom';
							tableAndColumn = tableUomTo + '_wx_' + column;
							newUomToObject[tableAndColumn] = value;
						}

					}

				});
				newOptionsUom.push(newUomToObject);
				newOptionsUom.push(newUomFromObject);
			});

			let uniqueNewOptionsUom = _.uniqBy(newOptionsUom, 'c_uom_wx_c_uom_uu');

			let htmlOptionsUom = ''
			_.forEach(uniqueNewOptionsUom, function (uomObject) {
				// jika sama dengan baseUom dari product
				// set selected
				let selected = '';
				if (productViewModelTable.m_product_wx_c_uom_uu === uomObject.c_uom_wx_c_uom_uu) {
					selected = 'selected';
				}
				htmlOptionsUom += '<option value="' + uomObject.c_uom_wx_c_uom_uu + '"' + selected + '>' + uomObject.c_uom_wx_name + '</option>';
			});
			$('#' + thisObject.idFormValidation + ' #c_uom_wx_c_uom_uu').html(htmlOptionsUom);
			//HelperService.InjectViewModelToForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.viewModel);


		});



	} */

	async	GeneratePricelistVersionByProduct(selectedProduct) {
		let thisObject = this;
		let mProductpriceService = new ProductpriceService();
		let metaDataTable = new MetaDataTable();
		metaDataTable.ad_org_uu = localStorage.getItem('ad_org_uu');
		metaDataTable.otherFilters = [' AND m_product.m_product_uu = \'' + selectedProduct.m_product_wx_m_product_uu + '\'',
		' AND m_pricelist.m_pricelist_uu = \'' + thisObject.cOrderParent.c_order_wx_m_pricelist_uu + '\''];

		let productPriceCMTList = await mProductpriceService.FindAll(metaDataTable);
		thisObject.table_productprice_viewmodel = productPriceCMTList;
		let htmlOptionsPricelistVersion = '';
		_.forEach(thisObject.table_productprice_viewmodel, function (productprice, index) {
			let selected = '';
			if (index === 0) {
				selected = 'selected';
			}
			htmlOptionsPricelistVersion += '<option value="' + productprice.m_pricelist_version_wx_m_pricelist_version_uu + '" ' + selected + ' >' + productprice.m_pricelist_version_wx_name + '</option>';
		});
		$('#' + thisObject.idFormValidation + ' #m_pricelist_version_wx_m_pricelist_version_uu').html(htmlOptionsPricelistVersion);
		thisObject.SetPriceBasedOnPricelistVersion();





	}


	SetPriceBasedOnPricelistVersion() {
		let thisObject = this;
		let mPricelistVersionUu = $('#' + thisObject.idFormValidation + ' #m_pricelist_version_wx_m_pricelist_version_uu').val();
		let productPriceFound = _.find(thisObject.table_productprice_viewmodel, (productprice) => {
			return productprice.m_pricelist_version_wx_m_pricelist_version_uu === mPricelistVersionUu;
		});


		let selectedUom = $('#' + thisObject.idFormValidation + ' #c_uom_wx_c_uom_uu').val();
		// jika baseUom === dengan uom yang dipilih set priceentered harga dari pricelist_version
		if (productPriceFound) {
			if (selectedUom === thisObject.selectedProduct.c_uom_wx_c_uom_uu) {
				thisObject.inputPriceentered.val(productPriceFound.m_productprice_wx_pricestd);
				thisObject.inputPriceactual.val(productPriceFound.m_productprice_wx_pricestd);
			}
		}

	}

}
