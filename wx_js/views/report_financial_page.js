class ReportFinancialPage {

	/**
	 * 	 
	 * @param {*} idFragmentContent  // IdControl untuk meletakkan View financial_reportpage ex>. #fragmentContent
	 */
	constructor(idFragmentContent) {


		this.titlePage = 'Laporan Keuangan';

		this.idFragmentContent = idFragmentContent;
	
		this.idFragmentReportTypeOptions = 'financial_report_tab_financial_report_options';

		this.idFragmentReportContent = 'financial_report_tab_financial_report_table'; // button row status pada datatable financial_report page

		

	
		this.Init();


	}


	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery

	
		let layoutHtml = `
		<div class="row">
			<div class="col-xl-12">		
				<div id="`+ thisObject.idFragmentReportTypeOptions + `"></div>			
				<div id="`+ thisObject.idFragmentReportContent + `"></div>
			</div>		
		</div>`;


		$('#' + thisObject.idFragmentContent).html(layoutHtml);
		let reportTypeOptionsPortlet = new ReportFinancialOptionsPortlet(thisObject.idFragmentReportTypeOptions, thisObject.idFragmentReportContent);
		
	}



}

