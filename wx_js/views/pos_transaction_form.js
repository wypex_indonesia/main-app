class PosTransactionForm {

	constructor(idFragmentContent, posTransactionTablePortlet, defaultPosCMT) {

		this.posTransactionCM = new PosTransactionComponentModel();
		this.posTransactionlineCashCM = new PosTransactionlineComponentModel();
		this.defaultPosCMT = defaultPosCMT;
		this.defaultPosCM = HelperService.ConvertRowDataToViewModel(defaultPosCMT, new PosConfigComponentModel());


		this.posTransactionService = new PosTransactionService();
		this.posTransactionlineService = new PosTransactionlineService();
		this.idFragmentContent = idFragmentContent;
		this.posTransactionTablePortlet = posTransactionTablePortlet;

		this.labelForm = 'POS Open Close Transaksi';
		this.labelVendorOrCustomer = '';

		this.poReferenceHtml = ``;

		this.idFormModal = 'pos_transaction_form_wx_';
		this.idFormValidation = 'pos_transaction_form_validation_';
		this.classNameFormControl = 'pos_transaction_form-control';
		this.classNameTransactionLineControl = 'pos_transaction_form-pos-transactionline-control';
		this.selectorClassNameFormControl = '.' + this.classNameFormControl;
		this.idButtonFormSubmit = 'pos_transaction_form_submit_wx_';
		this.classDateTimePicker = 'pos_transaction_form_datetime_picker';
		this.idRadioDocStatus = 'pos_transaction_form_pos_transaction_docstatus';
		this.validation = {
			id_form: this.idFormValidation,
		};
		this.idCashOpenAmount = 'pos_transaction_form_cash_open_amount';
		this.idCashCloseAmount = 'pos_transaction_form_cash_close_amount';
		this.idCashActualAmount = 'pos_transaction_form_cash_actual_amount';
		this.idDisplayOpenDate = 'pos_transaction_form_display_open_date';
		this.idDisplayCloseDate = 'pos_transaction_form_display_close_date';
		this.Init();

	}


	async	Init() {

		try {

			let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery

			let fact_acct_service = new FactAcctService();
			let fact_acct_summary = await fact_acct_service.FindFactAcctSummary(localStorage.getItem('current_period'));

			let asset_element_object = _.find(fact_acct_summary, function(summary){ return summary.value === thisObject.defaultPosCMT.c_bankaccount_wx_asset_element;});
			thisObject.defaultPosCM.c_bankaccount.currentbalance =  asset_element_object.amount;

			await thisObject.RunProcessData();
			let htmlForm = await thisObject.GeneratePortlet();

			$('#' + thisObject.idFragmentContent).html(htmlForm);

			HelperService.InjectViewModelToForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.posTransactionCM);

			thisObject.CustomDisplayDate();

			thisObject.FilterFieldForm();
			thisObject.EventHandler();



		} catch (error) {
			apiInterface.Log(this.constructor.name, this.Init.name, error);
		}


	}

	/**
	 * FactAcct=> GetCurrentBalance()
	 * amount cash atau bank akan dihitung dari fact_acct_summary, dan perhitungan fact_acct yang sedang berjalan
	 * 
	 * 1. Cek jika ada outstanding POS TRansaction, jika tidak ada
	 * 	pos_transaction.docstatus = 'DRAFT';
	 *  pos_transaction.open_date = new Date().getTime();
	 *  pos_transactionline.open_amount = GetCurrentBalance;
	 *  pos_transaction.documentno = new Date().getTime();
	 * 2. Jika ada outstanding pos transaction
	 *  --- DocStatus = 'DRAFT';
	 * 		 pos_transaction.open_date = new Date().getTime();
	 * 	--- DocStatus = 'OPENED';
	 * 		
	 *  --- DocStatus = 'PROCESSED';
	 * 		pos_transaction.closed_date = new Date().getTime();
	 * 
	 * 		pos_transactionline.close_amount = GetCurrentBalance;
	 * 		
	 * 		//bankaccount yang lain dijumlahkan semua berdasarkan order grandtotal
	 *  --- DocStatus = 'CLOSED';
	 * 		--- CREATE NEW PosTransaction
	 * 		
	 */
	async RunProcessData() {
		let thisObject = this;
		let outstandingPosTransactionCMTList = await thisObject.posTransactionService.FindOutstandingPosTransaction();

		if (outstandingPosTransactionCMTList.length) {
			let posTransactionCMT = outstandingPosTransactionCMTList[0];

			thisObject.posTransactionCM = HelperService.ConvertRowDataToViewModel(posTransactionCMT);

			let posTransactionlineCashCMTList = await thisObject.posTransactionlineService
				.FindPosTransactionlineCashAccountCMTList(posTransactionCMT.pos_transaction_wx_pos_transaction_uu,
					thisObject.defaultPosCMT.c_pos_wx_c_bankaccount_uu);

			if (posTransactionlineCashCMTList.length) {
				let posTransactionlineCashCMT = posTransactionlineCashCMTList[0];
				thisObject.posTransactionlineCashCM = HelperService.ConvertRowDataToViewModel(posTransactionlineCashCMT);
			}

			if (thisObject.posTransactionCM.pos_transaction.docstatus === 'DRAFT') {
				thisObject.posTransactionCM.pos_transaction.open_date = new Date().getTime();
			}

			if (thisObject.posTransactionCM.pos_transaction.docstatus === 'PROCESSED') {
				thisObject.posTransactionCM.pos_transaction.close_date = new Date().getTime();
				thisObject.posTransactionlineCashCM.pos_transactionline.close_amount = thisObject.defaultPosCM.c_bankaccount.currentbalance;
			}

		} else {

			let pos_transaction = new PosTransaction();
			pos_transaction.docstatus = 'DRAFT';
			pos_transaction.open_date = new Date().getTime();
			pos_transaction.documentno = new Date().getTime();

			thisObject.posTransactionCM.pos_transaction = pos_transaction;

			let pos_transactionline_cash = new PosTransactionline();

			pos_transactionline_cash.open_amount = numeral(thisObject.defaultPosCM.c_bankaccount.currentbalance).value();
			pos_transactionline_cash.c_bankaccount_uu = thisObject.defaultPosCM.c_bankaccount.c_bankaccount_uu;

			let posTransactionlineCashCM = new PosTransactionlineComponentModel();
			posTransactionlineCashCM.pos_transactionline = pos_transactionline_cash;
			posTransactionlineCashCM.pos_transaction = pos_transaction;
			posTransactionlineCashCM.c_bankaccount = thisObject.defaultPosCM.c_bankaccount;

			thisObject.posTransactionlineCashCM = posTransactionlineCashCM;
		}



	}

	CustomDisplayDate() {

		let thisObject = this;
		try {
			if (thisObject.posTransactionCM.pos_transaction.open_date) {
				let displayOpenDate = HelperService.ConvertTimestampToLocateDateTimeString(thisObject.posTransactionCM.pos_transaction.open_date);
				$('#' + thisObject.idDisplayOpenDate).val(displayOpenDate);
			}

			if (thisObject.posTransactionCM.pos_transaction.close_date) {
				let displayCloseDate = HelperService.ConvertTimestampToLocateDateTimeString(thisObject.posTransactionCM.pos_transaction.close_date);
				$('#' + thisObject.idDisplayCloseDate).val(displayCloseDate);
			}
		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.CustomDisplayDate.name, error);
		}

	}

	async	GeneratePortlet() {
		let thisObject = this;
		let optionsStatusDocument = thisObject.CreateOptionsStatusDocument();

		let bankTotalSalesHtml = '';
		if (thisObject.posTransactionCM.pos_transaction.docstatus === 'PROCESSED') {
			bankTotalSalesHtml = await thisObject.CreateCurrentSalesAkunBank();
		}

		let portletHtml = `
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand flaticon2-line-chart"></i>
				</span>
				<h3 class="kt-portlet__head-title">
					`+ thisObject.labelForm + `
				</h3>
			</div>
			<div class="kt-portlet__head-toolbar">
				<div class="kt-portlet__head-wrapper">
					<div class="kt-portlet__head-actions">
					</div>
				</div>
			</div>
		</div>
		<form id="`+ thisObject.idFormValidation + `" class="kt-form">
			<div class="kt-portlet__body">
	
				<div class="form-group">
					<label>No Dokumen</label>
					<input disabled="disabled" id="pos_transaction_wx_documentno" type="text"
						class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp">
					<span class="form-text text-muted">No Dokumen dibuat otomatis</span>
				</div>				
				<div class="form-group ">
					<label>Tanggal  Open</label>
					<input type="text" class="form-control"	id="`+ thisObject.idDisplayOpenDate + `" readonly="" placeholder="">
					<input hidden type="text" class="` + thisObject.classNameFormControl + `  form-control"
						id="pos_transaction_wx_open_date"  placeholder="">
				</div>
				<div class="form-group ">
					<label>Tanggal Close</label>
					<input type="text" class=" form-control"	id="`+ thisObject.idDisplayCloseDate + `" readonly="" placeholder="">
					<input hidden type="text" class="` + thisObject.classNameFormControl + ` form-control"
						id="pos_transaction_wx_close_date" readonly="" placeholder="">
				</div>
				<div class="form-group">
					<label>`+ thisObject.defaultPosCMT.c_bankaccount_wx_name + `</label>
					<input hidden data-row-id="line-cash" class="` + thisObject.classNameTransactionLineControl + `" name ="c_bankaccount_uu" value="` + thisObject.posTransactionlineCashCM.pos_transactionline.c_bankaccount_uu + `">
				
					<div class="lg-4">
						<div class="input-group">
							<div class="input-group-prepend"><span class="input-group-text">Open Cash</span></div>
							<input disabled type="text" id="`+ thisObject.idCashOpenAmount + `" data-row-id="line-cash" value="` + thisObject.posTransactionlineCashCM.pos_transactionline.open_amount + `" name="open_amount" class="` + thisObject.classNameTransactionLineControl + ` wx-format-money form-control" placeholder="Jumlah Open Cash" aria-describedby="basic-addon1">
							<input hidden data-row-id="line-cash" class="` + thisObject.classNameTransactionLineControl + `" name ="pos_transactionline_uu" value="` + thisObject.posTransactionlineCashCM.pos_transactionline.pos_transactionline_uu + `">
		
						</div>
					</div>
					<div class="lg-4">

						<div class="input-group">
							<div class="input-group-prepend"><span class="input-group-text">Close Cash</span></div>
							<input disabled type="text" id="`+ thisObject.idCashCloseAmount + `" data-row-id="line-cash" value="` + thisObject.posTransactionlineCashCM.pos_transactionline.close_amount + `" name="close_amount" class="` + thisObject.classNameTransactionLineControl + ` wx-format-money form-control" placeholder="Jumlah Close Cash" aria-describedby="basic-addon1">
						</div>

					</div>
					<div class="lg-4">

						<div class="input-group">
							<div class="input-group-prepend"><span class="input-group-text">Aktual Cash</span></div>
							<input type="text" id="`+ thisObject.idCashActualAmount + `" data-row-id="line-cash" value="` + thisObject.posTransactionlineCashCM.pos_transactionline.actual_amount + `" name="actual_amount" class="` + thisObject.classNameTransactionLineControl + ` wx-format-money form-control" placeholder="Jumlah Aktual Cash" aria-describedby="basic-addon1">
						</div>

					</div>
					<span class="form-text text-muted">Bandingkan jumlah Open Cash, Close Cash dan Aktual Cash</span>
				</div>
				
				`+ bankTotalSalesHtml + optionsStatusDocument + `
			</div>
			<div class="kt-portlet__foot">
				<div class="kt-form__actions">
					<button id="`+ thisObject.idButtonFormSubmit + `" type="submit" class="btn btn-primary">Submit</button>
	
				</div>
			</div>
		</form>
	</div>
		`;
		return portletHtml;

	}

	async CreateCurrentSalesAkunBank() {
		let thisObject = this;
		let orderService = new OrderService();

		let allHtml = '';
		try {
			let bankaccountSalesList = await orderService.FindCurrentSalesBankaccountByCPosUu(thisObject.defaultPosCMT.c_pos_wx_c_pos_uu,
				thisObject.posTransactionCM.pos_transaction.open_date, thisObject.posTransactionCM.pos_transaction.close_date);

			_.forEach(bankaccountSalesList, function (bankaccountsales, index) {

				let htmlRow = `
			<div class="form-group row">
			<label>`+ bankaccountsales.bankaccount + `</label>
			<input hidden data-row-id="line-`+ index + `" class="` + thisObject.classNameTransactionLineControl + `" name ="c_bankaccount_uu" value="` + bankaccountsales.c_bankaccount_uu + `">
			<div class="lg-6">

				<div class="input-group">
					<div class="input-group-prepend"><span class="input-group-text">Close </span></div>
					<input data-row-id="line-`+ index + `" name="close_amount" value="` + bankaccountsales.totalsales + `" disabled type="text" class="` + thisObject.classNameTransactionLineControl + ` wx-format-money form-control" placeholder="Jumlah Close Cash" aria-describedby="basic-addon1">
				</div>

			</div>
			<div class="lg-6">

				<div class="input-group">
					<div class="input-group-prepend"><span class="input-group-text">Aktual</span></div>
					<input type="text" data-row-id="line-`+ index + `" name="actual_amount" class="` + thisObject.classNameTransactionLineControl + ` wx-format-money form-control" placeholder="Jumlah Aktual (setelah settle)" aria-describedby="basic-addon1">
				</div>

			</div>
			<span class="form-text text-muted">Bandingkan jumlah Close  dan Aktual (setelah settlement) </span>
		</div>
			`;
				allHtml += htmlRow;

			})
		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.CreateCurrentSalesAkunBank.name, error);
		}

		return allHtml;

	}

	CreateOptionsStatusDocument() {

		let thisObject = this;

		let statusDraftObject = { status: 'DRAFT', body: 'Simpan dokumen dengan status draft.' };
		let statusOpenedObject = { status: 'OPENED', body: 'Simpan dokumen dengan status opened. POS siap melakukan transaksi' };
		let statusProcessedObject = { status: 'PROCESSED', body: 'Dokumen processed dilakukan untuk menghitung semua penjualan dan tidak ada lagi transaksi ' };

		let statusClosedObject = { status: 'CLOSED', body: 'Dokumen closed, jumlah cash dan akun yang lain telah selesai diperiksa' };
		let statusDeletedObject = { status: 'DELETED', body: 'Delete dokumen' };

		let docStatusRow = [];

		let allHtml = '';

		try {
			switch (thisObject.posTransactionCM.pos_transaction.docstatus) {

				case 'DRAFT':
					docStatusRow.push(statusDraftObject, statusOpenedObject, statusDeletedObject);
					break;
				case 'OPENED':
					docStatusRow.push(statusProcessedObject);
					break;
				case 'PROCESSED':
					docStatusRow.push(statusClosedObject);
					break;
				default:
					docStatusRow.push(statusClosedObject);
					break;
			}

			let rowHtml = '';
			_.forEach(docStatusRow, function (objectStatus, index) {
				let checked = '';
				if (index === 0) {
					checked = 'checked';
				}
				let templateHtml = `
			<div class="row">
				<div class="col-lg-12">
					<label class="kt-option">
						<span class="kt-option__control">
							<span class="kt-radio kt-radio--bold kt-radio--brand">
								<input type="radio" name="`+ thisObject.idRadioDocStatus + `" value="` + objectStatus.status + `" ` + checked + `>
								<span></span>
							</span>
						</span>
						<span class="kt-option__label">
							<span class="kt-option__head">
								<span class="kt-option__title">
								`+ objectStatus.status + `
								</span>								
							</span>
							<span class="kt-option__body">
							`+ objectStatus.body + `
							</span>
						</span>
					</label>
				</div>
			</div>`;
				rowHtml += templateHtml;
			});

			allHtml = `<div class="form-group "> ` + rowHtml + `</div>`;
		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.CreateOptionsStatusDocument.name, error);
		}

		return allHtml;

	}

	EventHandler() {
		let thisObject = this;

		try {
			$('#' + thisObject.idFormValidation).off('submit');
			$('#' + thisObject.idFormValidation).on('submit', function (e) {
				e.preventDefault();
				thisObject.Save();

			});
			$('.wx-format-money').mask('#,##0', { reverse: true });

		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.EventHandler.name, error);
		}

	}


	/**
		 *

		 */
	async Save() {
		let thisObject = this;

		try {
			let posTransactionlineService = new PosTransactionlineService();

			let insertSuccess = 0;
			if (HelperService.CheckValidation(thisObject.validation)) {

				thisObject.posTransactionCM = HelperService.GetViewModelFromForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.posTransactionCM);


				let docStatus = $("input[name='" + thisObject.idRadioDocStatus + "']:checked").val();

				thisObject.posTransactionCM.pos_transaction.docstatus = docStatus;
				$('#' + thisObject.idFormValidation + ' #' + thisObject.idButtonFormSubmit).addClass(" kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light");

				insertSuccess += await thisObject.posTransactionService.SaveUpdate(thisObject.posTransactionCM);

				// SAVE POS TRANSACTION LINE

				let objectTransactionList = {};
				//get pos_transactionline 
				$('.' + thisObject.classNameTransactionLineControl).each(function () {
					let rowId = $(this).data('row-id');
					let name = $(this).attr('name');
					let value = $(this).val();

					if (!objectTransactionList[rowId]) {
						objectTransactionList[rowId] = {};
					}

					objectTransactionList[rowId][name] = value;

				});

				let insertSqlPosTransactionline = [];
				_.forEach(objectTransactionList, function (modelPosTransactioneline) {
					modelPosTransactioneline.pos_transaction_uu = thisObject.posTransactionCM.pos_transaction.pos_transaction_uu;
					let insertSql = posTransactionlineService.GetSqlInsertStatement(modelPosTransactioneline);
					insertSqlPosTransactionline.push(insertSql);
				});

				insertSuccess += await apiInterface.ExecuteBulkSqlStatement(insertSqlPosTransactionline);



				// jika sukses tersimpan
				if (insertSuccess > 0) {
					this.posTransactionTablePortlet.UpdateTable(thisObject.posTransactionCM.pos_transaction.pos_transaction_uu);
					thisObject.Init();
				}

			}
		} catch (err) {
			apiInterface.Log(this.constructor.name, this.Save.name, err);
		}
	}

	/**
	 * docstatus = DRAFT => 
	 * -- OPEN AMOUNT DISABLR
	 * -- CLOSE AMOUNT DISABLE
	 * -- ACTUAL AMOUNT DISABLE
	 * docstatus = OPENED
	 * -- OPEN AMOUNT DISABLE
	 * -- CLOSE AMOUNT DISABLE
	 * -- ACTUAL AMOUNT DISABLE
	 *  docstatus = PROCESSED
	 * -- OPEN AMOUNT DISABLE
	 * -- CLOSE AMOUNT DISABLE
	 * -- ACTUAL AMOUNT ENABLE
	 *  docstatus = PROCESSED
	 * -- OPEN AMOUNT DISABLE
	 * -- CLOSE AMOUNT DISABLE
	 * -- ACTUAL AMOUNT DISABLE
	 */
	FilterFieldForm() {
		let thisObject = this;
		$('#' + thisObject.idCashOpenAmount).prop('disabled', true);
		$('#' + thisObject.idCashCloseAmount).prop('disabled', true);
		$('#' + thisObject.idCashActualAmount).prop('disabled', true);


		if (thisObject.posTransactionCM.pos_transaction.docstatus === 'PROCESSED') {
			$('#' + thisObject.idCashActualAmount).prop('disabled', false);
		}
	}


}
