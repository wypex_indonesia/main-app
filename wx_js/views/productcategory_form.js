class ProductCategoryForm {

	/**
	 * 
	
	
	 * @param {ProductCategoryViewModel} productcategoryCM
	 * @param {datatableReference} datatableReference data tabel pada role page, sebagai reference untuk mengupdate table tersebut	 
	 */
	constructor(productcategoryCM, datatableReference) {

		this.titleForm = 'Add/Edit Kategori Produk';

		this.viewModel = productcategoryCM;


		this.metaDataTable = new MetaDataTable();

		this.datatableReference = datatableReference; // buat reference datatable

		this.productCategoryService = new ProductCategoryService();


		this.idFormModal = 'm_product_category_form_wx_';
		this.idFormValidation = 'm_product_category_form_validation_';
		this.classNameFormControl = 'm_product_category-form-control';
		this.selectorClassNameFormControl = '.' + this.classNameFormControl;


		this.idButtonFormSubmit = 'm_product_category_form_submit_wx_';


		this.validation = {

			id_form: this.idFormValidation,

			m_product_category_wx_name: {
				required: true
			},
		

		};




		this.Init();


	

	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery

		try {

			HelperService.InsertReplaceFormModal(thisObject.idFormModal, thisObject.GenerateFormInput());

			HelperService.InjectViewModelToForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.viewModel);
			thisObject.PageEventHandler();
		} catch (error) {
			apiInterface.Log(this.constructor.name, this.Init.name, error);
		}


	}




	/**
	 * 
	
	 */
	async Save() {
		let thisObject = this;

		try {

			let insertSuccess = 0;
			if (HelperService.CheckValidation(thisObject.validation)) {

				thisObject.viewModel = HelperService.GetViewModelFromForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.viewModel);

				insertSuccess = await thisObject.productCategoryService.SaveUpdate(thisObject.viewModel, thisObject.datatableReference);
				// jika sukses tersimpan 
				if (insertSuccess > 0) {
					$('#' + thisObject.idFormModal).modal('hide');
				}

			}
		} catch (err) {
			apiInterface.Log(this.constructor.name, this.Save.name, err);
		}



	}

	GenerateFormInput() {
		let thisObject = this;

		let html = '';
		try {
			html = `
		<div class="modal fade" id="` + thisObject.idFormModal + `" tabindex="-1" role="dialog"
			aria-labelledby="productcategory-form-labelled" style="display: none;" aria-hidden="true">
		
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="productcategory-form-labelled">Kategori Produk</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						</button>
					</div>
					<form id="`+ thisObject.idFormValidation + `" class="kt-form">
						<div class="modal-body">
		
							<div class="form-group">
								<label>Nama Kategori</label>
								<input id="m_product_category_wx_name" type="text"
								class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp"
									placeholder="Masukkan nama kategori">
								<span class="form-text text-muted">Masukkan kategori</span>
							</div>
						</div>
		
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button id='productcategory-form-submit' type="submit" class="btn btn-primary">Save changes</button>
						</div>
					</form>
				</div>
			</div>
		</div>
`;
			
		} catch (error) {
			apiInterface.Log(this.constructor.name, this.GenerateFormInput.name, error);
		}

		return html;
	}


	PageEventHandler() {
		let thisObject = this;


		try {

			$('#' + thisObject.idFormValidation).off('submit');
			$('#' + thisObject.idFormValidation).on('submit', function (e) {
				thisObject.Save();
				e.preventDefault();
				

			});



			$('#' + thisObject.idFormModal).modal('show');


		} catch (err) {
			apiInterface.Log(this.constructor.name, this.PageEventHandler.name, err);
		}


	}



}
