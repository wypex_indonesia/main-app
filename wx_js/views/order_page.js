class OrderPage {

	/**
	 * 
	 * @param {*} orderType  // 'PO' // SO (SALES ORDER), (PURCHASE ORDER)
	 * @param {*} idFragmentContent  // IdControl untuk meletakkan View Orderpage ex>. #fragmentContent
	 */
	constructor(orderType, idFragmentContent) {


		this.titlePage = '';
		this.orderType = orderType; // 'PO' // SO (SALES ORDER), (PURCHASE ORDER)
		this.labelVendorOrCustomer = '';
		this.viewModel = new OrderViewModel();

		this.metaDataTable = new MetaDataTable();

		this.datatableReference = {}; // buat reference datatable

		this.orderService = new OrderService();
		this.bpartnerService = new BusinessPartnerService();
		this.warehouseService = new WarehouseService();
		this.bankaccountService = new BankaccountService();
		this.idWidgetSearch = 'order_page_fragment_widget_search';
		this.idFragmentContent = idFragmentContent;
		this.idFormModal = 'c_order_form_wx_';
		this.idFormValidation = 'c_order_form_validation_';
		this.classNameFormControl = 'c-order-form-control';
		this.selectorClassNameFormControl = '.' + this.classNameFormControl;
		this.idDialogBox = 'c_order_dialog_box_wx_';
		this.idDialogPrint = 'print-dialog-box';
		this.idTable = 'wx_order_table_';
		this.idNewRecord = 'new_record_order_wx_';
		this.idButtonFormSubmit = 'c_order_form_submit_wx_';
		this.idButtonRowStatusDialog = 'bttn_row_status'; // button row status pada datatable order page
		this.idRadioDocStatus = '';// idGroupRadio Status pada document status dialog

		this.idButtonPrintDialog = "bttn_print_dialog";
		this.prefixContentOrderlinePage = 'detail_order_';

		this.classButtonShowForm = 'c_order_page_bttn_show_form';
		this.classDialogStatus = 'c_order_page_bttn_show_dialog_status';
		this.classDialogPrint = 'c_order_page_bttn_show_dialog_print';

		if (orderType === 'PO') {
			this.titlePage = 'Daftar Pembelian';

			this.metaDataTable.otherFilters.push(' AND c_doctype.docbasetype = \'PO\'');
			this.labelVendorOrCustomer = 'Vendor';
		} else {
			this.titlePage = 'Daftar Penjualan';

			this.metaDataTable.otherFilters.push(' AND c_doctype.docbasetype = \'SO\'');
			this.labelVendorOrCustomer = 'Customer';

		}



	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery

		thisObject.metaDataTable.ad_org_uu = localStorage.getItem('ad_org_uu');

		let allHtml = `
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head kt-portlet__head--lg">
			<div  class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
			<i class="kt-font-brand flaticon2-line-chart"></i>
			</span>
			<h3 class="kt-portlet__head-title">
		 	`+ thisObject.titlePage + `
			</h3>
			</div>
			<div  class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-wrapper">
			<div class="kt-portlet__head-actions">
			<button type="button" class="`+ thisObject.classButtonShowForm + ` btn btn-bold btn-label-brand btn-sm" name="new_record" >
			<i class="la la-plus"></i>New Record</button>	
		
			</div>
			</div>
			</div>
			</div>
			<div class="kt-portlet__body" >
			<table class="table table-striped- table-bordered table-hover table-checkable" id="`+ thisObject.idTable + `">
			
		
			</table>
			</div>
		</div>
		`;




		//allHtml += HelperService.DialogBox(thisObject.idDialogPrint, 'Print Preview');


		$('#' + thisObject.idFragmentContent).html(allHtml);


		var table = $('#' + thisObject.idTable);
		let columnDefDatatable = [
			{
				targets: -1,
				title: 'Actions',
				orderable: false,
				render: function (data, type, full, meta) {

					let buttonPrintHtml = `<button id="` + thisObject.idButtonPrintDialog + full.c_order_wx_c_order_uu + `" name="` + full.c_order_wx_c_order_uu + `" type="button"   class="` + thisObject.classDialogPrint + ` btn btn-outline-hover-info btn-elevate btn-icon"> <i class="la la-print"></i></button>`;
					let buttonStatusHtml = `<button id="` + thisObject.idButtonRowStatusDialog + full.c_order_wx_c_order_uu + `" value="` + full.c_order_wx_documentno + `" name="` + full.c_order_wx_c_order_uu + `" type="button"   class="` + thisObject.classDialogStatus + ` btn btn-outline-hover-info btn-elevate btn-icon"> <i class="la la-flag-o"></i></button>`;
					let buttonEditHtml = `<button id="bttn_row_edit` + full.c_order_wx_c_order_uu + `" name="` + full.c_order_wx_c_order_uu + `" type="button" class="` + thisObject.classButtonShowForm + ` btn btn-outline-hover-info btn-elevate btn-icon"><i class="la la-edit"></i></button>`;
					// jika 
					let htmlAction = '';
					if (full.c_order_wx_docstatus === 'CLOSED') {
						htmlAction += buttonPrintHtml;
					}


					if (full.c_order_wx_docstatus === 'REVERSED') {
						htmlAction += '';
					}

					if (full.c_order_wx_docstatus === 'COMPLETED') {
						// customer retur statushtml dihapus, karena tidak action untuk reversed
						if (full.c_order_wx_c_doctype_id !== 2002) {
							htmlAction += buttonStatusHtml;
						}
						htmlAction += buttonPrintHtml;
					}
					if (full.c_order_wx_docstatus === 'DRAFT') {
						htmlAction += buttonEditHtml + buttonStatusHtml + buttonPrintHtml;
					}


					return htmlAction;
				},
			},
			{
				targets: 9,
				title: 'Tanggal',
				orderable: true,
				render: function (data, type, full, meta) {
					return HelperService.ConvertTimestampToLocateDateString(full.c_order_wx_dateacct);
				},
			},
			{
				targets: 7,
				title: 'Grand total',
				orderable: false,
				render: function (data, type, full, meta) {

					return new Intl.NumberFormat().format(full.c_order_wx_grandtotal);
				},
			},
			{
				targets: 6,
				title: 'Total Tax',
				orderable: false,
				render: function (data, type, full, meta) {

					return new Intl.NumberFormat().format(full.c_order_wx_totaltaxlines);
				},
			},
			{
				targets: 5,
				title: 'Total Orderline',
				orderable: false,
				render: function (data, type, full, meta) {
					return new Intl.NumberFormat().format(full.c_order_wx_totallines);
				},
			},
		];


		thisObject.orderService.FindAll(thisObject.metaDataTable).then(function (orderCMTList) {
			// begin first table
			thisObject.datatableReference = table.DataTable({
				responsive: true,
				data: orderCMTList,
				scrollX: true,
				rowId: 'c_order_wx_c_order_uu',
				columns: [
					{
						"className": 'details-control',
						"orderable": false,
						"data": null,
						"defaultContent": '<button  type="button" class="btn btn-outline-hover-info btn-icon"><i class="la la-plus-square-o"></i></button>'
					},
					{ title: 'No Dokumen', data: 'c_order_wx_documentno' },
					{ title: thisObject.labelVendorOrCustomer, data: 'c_bpartner_wx_name' },
					{ title: 'Gudang/Toko', data: 'm_warehouse_wx_name' },
					{ title: 'Tipe Order', data: 'c_doctype_wx_name' },
					{
						title: "Total Orderline",
						data: 'c_order_wx_totallines'
					},
					{
						title: "Total Tax",
						data: 'c_order_wx_totaltaxlines'
					},
					{
						title: "Grand total",
						data: 'c_order_wx_grandtotal'
					},
					{
						title: "Status",
						data: 'c_order_wx_docstatus'
					},
					{
						title: "Tanggal",
						data: 'c_order_wx_docacct'
					},
					{ title: 'Action' }
				],
				columnDefs: columnDefDatatable,
				"order": [[0, 'desc']],
				createdRow: function (row, data, index) {
					if (data.extn === '') {
						var td = $(row).find("td:first");
						td.removeClass('details-control');
					}
				},
				rowCallback: function (row, data, index) {
					//console.log('rowCallback');
				}

			});

			// attach event handler on the page

			thisObject.EventHandler();


		});

	}

	EventHandler() {
		let thisObject = this;
		// Add event listener for opening and closing details
		$('#' + thisObject.idTable + ' tbody').off('click', 'td.details-control');
		$('#' + thisObject.idTable + ' tbody').on('click', 'td.details-control', function () {
			var tr = $(this).closest('tr');
			var row = thisObject.datatableReference.row(tr);
			let cOrderParent = row.data();
			if (row.child.isShown()) {
				// This row is already open - close it
				row.child.hide();
				$(this).html('<button  type="button" class="btn btn-outline-hover-info btn-icon"><i class="la la-plus-square-o"></i></button>');
			}
			else {
				let idOrderdetailContent = thisObject.prefixContentOrderlinePage + cOrderParent.c_order_wx_c_order_uu;
				let orderlineFragment = `<div id="` + idOrderdetailContent + `"></div>`;
				row.child(orderlineFragment).show();

				// generate orderlinePage
				let orderlinePage = new OrderlinePage(cOrderParent, idOrderdetailContent, thisObject.datatableReference, thisObject);
				orderlinePage.Init();

				// add reference to listReferenceOrderlinePage, agar nanti bisa dipanggil lagi di orderpage, untuk membuat

				// change menjadi button minus
				$(this).html('<button  type="button" class="btn btn-outline-hover-info btn-icon"><i class="la la-minus-square-o"></i></button>');

			}

		});



		$('.' + thisObject.classButtonShowForm).off('click');
		$('.' + thisObject.classButtonShowForm).on('click', function (e) {
			try {
				let attrName = $(this).attr('name');

				if (attrName !== 'new_record') {

					let dataEdited = thisObject.datatableReference.row($('#bttn_row_edit' + attrName).parents('tr')).data();


					thisObject.componentModel = HelperService.ConvertRowDataToViewModel(dataEdited);

				} else {

					thisObject.componentModel = new OrderViewModel();
					thisObject.componentModel.c_order.documentno = new Date().getTime();
					thisObject.componentModel.c_order.docstatus = 'DRAFT';

				}
				let uomForm = new OrderForm(thisObject.componentModel, thisObject.datatableReference, thisObject.orderType);

			} catch (error) {
				apiInterface.Log(thisObject.constructor.name, thisObject.EventHandler.name, error)
			}

		});




		$('.' + thisObject.classDialogStatus).off('click');
		$('.' + thisObject.classDialogStatus).on('click', function (e) {
			let uuid = $(this).attr('name');
			let orderViewModelTable = thisObject.datatableReference.row($('#' + thisObject.idButtonRowStatusDialog + uuid).parents('tr')).data();

			let processDocumentFuction = function (docstatus) {

				return thisObject.UpdateStatusDialogBox(docstatus, uuid);
			};

			let dialogboxstatusWidget = new DialogBoxStatusWidget('Set Status Order', orderViewModelTable.c_order_wx_docstatus, processDocumentFuction);


		});





		/**
		* PRINT PREVIEW
		*/
		$('.' + thisObject.classDialogPrint).off('click');
		$('.' + thisObject.classDialogPrint).on('click', function (e) {

			let uuid = $(this).attr('name');
			let orderCMT = thisObject.datatableReference.row($('#' + thisObject.idButtonRowStatusDialog + uuid).parents('tr')).data();

			let printPreviewFunction = function (idContentPrint) {
				let printpreviewService = new PrintPreviewService();
				return printpreviewService.PrintPreviewOrder(idContentPrint, orderCMT);
			};

			let dialogPrint = new DialogBoxPrintWidget('Cetak Order', printPreviewFunction);

		});
		thisObject.datatableReference.off('draw');
		thisObject.datatableReference.on('draw', function () {

			thisObject.EventHandler();
		});


		$('.wx-format-money').mask('#,##0', { reverse: true });
	}

	async	UpdateStatusDialogBox(docStatus, uuid) {
		let thisObject = this;
		let insertSuccess = 0;

		try {

			let orderViewModelTable = thisObject.datatableReference.row($('#' + thisObject.idButtonRowStatusDialog + uuid).parents('tr')).data();
			orderViewModelTable.c_order_wx_docstatus = docStatus;
			insertSuccess += await thisObject.orderService.UpdateStatus(orderViewModelTable);

			if (insertSuccess) {
				let rowDataList = await thisObject.orderService.FindOrderViewModelByCOrderUu(orderViewModelTable.c_order_wx_c_order_uu);

				let rowData = {};

				if (rowDataList.length > 0) {
					rowData = rowDataList[0];
					if (docStatus === 'DELETED') {

						thisObject.datatableReference
							.row($('#' + thisObject.idButtonRowStatusDialog + uuid).parents('tr'))
							.remove()
							.draw();
					} else {

						if (docStatus === 'COMPLETED') {
							// cek jika ternyata child orderlinepage terbuka, maka create lagi tablenya
							if ($('#' + thisObject.prefixContentOrderlinePage + rowData.c_order_wx_c_order_uu).length) {

								let orderlinePage = new OrderlinePage(rowData, thisObject.prefixContentOrderlinePage + rowData.c_order_wx_c_order_uu);
								orderlinePage.Init();
							}
						}

						thisObject.datatableReference
							.row($('#' + thisObject.idButtonRowStatusDialog + uuid).parents('tr'))
							.data(rowData).draw();

					}
				}

			}
		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.UpdateStatusDialogBox.name, error);
			return insertSuccess;
		}
		return insertSuccess;
	}


}

