class FactAcctTransactionPage {

	/**
	 * 	 
	 * @param {*} idFragmentContent  // IdControl untuk meletakkan View fact_acct_transactionpage ex>. #fragmentContent
	 */
	constructor(idFragmentContent) {


		this.titlePage = 'Buat Transaksi';

		this.idFragmentContent = idFragmentContent;
	

		this.idFragmentFactTransationTable = 'fact_acct_transaction_tab_fact_acct_transaction_table'; // button row status pada datatable fact_acct_transaction page

		this.idFragmentFactTransactionPortlet = 'fact_acct_transaction_tab_transaction_portlet';

		this.idFragmentFactTransactionOptions = 'fact_acct_transaction_tab_fact_acct_transaction_options';

		this.Init();


	}


	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery

	
		let layoutHtml = `
		<div class="row">
			<div class="col-xl-12">		
				<div id="`+ thisObject.idFragmentFactTransactionOptions + `"></div>
				<div id="`+ thisObject.idFragmentFactTransactionPortlet + `"></div>
				<div id="`+ thisObject.idFragmentFactTransationTable + `"></div>
			</div>		
		</div>`;


		$('#' + thisObject.idFragmentContent).html(layoutHtml);
		let factacctTablePortlet = new FactAcctTablePortlet(thisObject.idFragmentFactTransationTable);
		let factAcctTransactionOptions = new FactAcctTransactionOptionsPortlet(thisObject.idFragmentFactTransactionOptions,
			 factacctTablePortlet, thisObject.idFragmentFactTransactionPortlet);
	}



}

