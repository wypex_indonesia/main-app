class ProductBomForm {

	constructor(productBomCM, datatableReference) {

		this.viewModel = productBomCM;


		this.datatableReference = datatableReference; // buat reference datatable

		this.productBomService = new ProductBomService();

		this.idFormModal = 'm_product_bom_form_wx_';
		this.idFormValidation = 'm_product_bom_form_validation_';
		this.classNameFormControl = 'm-product-bom-form-control';
		this.selectorClassNameFormControl = '.' + this.classNameFormControl;
		this.idButtonFormSubmit = 'm_product_bom_form_submit_wx_';
		this.idWidgetSearch = 'm_product_bom_form_widget_search';
		this.idBttnSearchProduct = 'm_product_bom_form_search_product';
		this.validation = {

			id_form: this.idFormValidation,

			m_product_wx_name: {
				required: true
			},
			m_product_bom_wx_bomqty: {
				required: true
			},

		}

		this.Init();
	}


	Init() {

		try {

			let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery

			let htmlForm = thisObject.GenerateForm();
			HelperService.InsertReplaceFormModal(thisObject.idFormModal, htmlForm);

			HelperService.InjectViewModelToForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.viewModel);

			// khusus untuk nama satuan karena bukan sebuah input, hanya text
			if (thisObject.viewModel.c_uom.name) {
				$('#' + thisObject.idFormValidation + ' #c_uom_wx_name').text(thisObject.viewModel.c_uom.name);
			}


			thisObject.EventHandler();



		} catch (error) {
			apiInterface.Log(this.constructor.name, this.Init.name, error);
		}


	}

	GenerateForm() {
		let thisObject = this;
		let allHtml = '';
		try {
			allHtml = `
			<div class="modal fade" id="` + thisObject.idFormModal + `" tabindex="-1" role="dialog"
				aria-labelledby="product-form-labelled" style="display: none;" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="product-form-labelled">Produk Bom Form</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							</button>
						</div>
						<form id="`+ thisObject.idFormValidation + `" class="kt-form">
							<div class="modal-body">
						
								<div class="form-group row">
									<div class="col-lg-6">
										<label>No</label>
										<input id="m_product_bom_wx_line" type="text"	class="`+ thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp"
											placeholder="Nomor">
										<span class="form-text text-muted">No urut</span>
			
									</div>
									<div class="col-lg-6">
										<label>Produk</label>					
										<div class="input-group">
											<input disabled="disabled" type="text" class="` + thisObject.classNameFormControl + ` form-control" id="m_product_wx_name" placeholder="Cari produk">
											<div class="input-group-append">
											<span id="`+ thisObject.idBttnSearchProduct + `" class="input-group-text"><i class="la la-search"></i></span>
											</div>
										</div>
										<input type="hidden" class="` + thisObject.classNameFormControl + `" id="m_product_wx_m_product_uu">
										
										<span class="form-text text-muted">Nama produk</span>
			
									</div>
								</div>
								<div class="form-group form-group-marginless">
									<label>Jumlah Produk</label>
									<div class="input-group">
										<input id="m_product_bom_wx_bomqty" type="text"
										class="`+ thisObject.classNameFormControl + ` form-control" placeholder="Jumlah Produk"
											aria-describedby="basic-addon2">
										<div class="input-group-append"><span class="input-group-text" id="c_uom_wx_name"></span>
										</div>
									</div>
								</div>
									<div id="`+ thisObject.idWidgetSearch + `" class="form-group ">
			
									</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								<button id="`+ thisObject.idButtonFormSubmit + `" type="submit" class="btn btn-primary">Save
									changes</button>
							</div>
						
						</form>
					</div>
				</div>
			</div>		
				`;
		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.GenerateForm.name, error);
		}

		return allHtml;



	}

	EventHandler() {
		let thisObject = this;

		try {
			$('#' + thisObject.idFormValidation + ' #' + thisObject.idBttnSearchProduct).off('click');
			$('#' + thisObject.idFormValidation + ' #' + thisObject.idBttnSearchProduct).on('click', function (e) {
				let functionGetResult = function (mProduct) {
					$('#' + thisObject.idFormValidation + ' #m_product_wx_name').val(mProduct.m_product_wx_name);
					$('#' + thisObject.idFormValidation + ' #c_uom_wx_name').text(mProduct.c_uom_wx_name);

					$('#' + thisObject.idFormValidation + ' #m_product_wx_m_product_uu').val(mProduct.m_product_wx_m_product_uu);


				};
				let product_search_widget = new ProductSearchWidget(thisObject.idWidgetSearch, functionGetResult);
				product_search_widget.Init();


			});

			$('#' + thisObject.idFormValidation).off('submit');
			$('#' + thisObject.idFormValidation).on('submit', function (e) {
				e.preventDefault();
				thisObject.Save();

			});
			


			$('#' + thisObject.idFormModal).modal('show');

		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.EventHandler.name, error);
		}

	}



	/**
		 *

		 */
	async Save() {
		let thisObject = this;

		try {

			let insertSuccess = 0;
			if (HelperService.CheckValidation(thisObject.validation)) {

				thisObject.viewModel = HelperService.GetViewModelFromForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.viewModel);



				insertSuccess += await thisObject.productBomService.SaveUpdate(thisObject.viewModel, thisObject.datatableReference);
				// jika sukses tersimpan
				if (insertSuccess > 0) {

					$('#' + thisObject.idFormModal).modal('hide');


				}

			}
		} catch (err) {
			apiInterface.Log(this.constructor.name, this.Save.name, err);
		}



	}









}
