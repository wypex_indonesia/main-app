/**
 * qty = barang sesuai dengan uom yang tertulis
 * movementqty = barang sesuai dengan uom based uom
 */
class MovementlinePage {

	/**
	 * 
	 * @param {movementCMT} movementCMT parent InvoiceComponentModelTable
	 * @param {string} idFragmentContent name id fragment content untuk meletakkan html content dari productbom_page
	 */
	constructor(movementCMT, idFragmentContent) {

		this.movementCMT = movementCMT;


		this.viewModel = new MovementlineComponentModel();

		this.metaDataTable = new MetaDataTable();


		this.datatableReference = {}; // buat reference datatable

		this.movementlineService = new MovementlineService();

		//	this.movementlineForm;

		this.idFragmentContent = idFragmentContent;
		this.idFormModal = 'm_movementline_form_wx_' + movementCMT.m_movement_wx_m_movement_uu;
		this.idFormValidation = 'm_movementline_form_validation_' + movementCMT.m_movement_wx_m_movement_uu;

		this.idDialogBox = 'm_movementline_dialog_box_wx_' + movementCMT.m_movement_wx_m_movement_uu;
		this.idTable = 'wx_movementline_table_' + movementCMT.m_movement_wx_m_movement_uu;
		this.idBtnNewMovementline = 'new_record_movementline_wx_' + movementCMT.m_movement_wx_m_movement_uu;
		this.idButtonFormSubmit = 'm_movementline_form_submit_wx_' + movementCMT.m_movement_wx_m_movement_uu;

		this.idFormFragmentContent = "wx_movementline_form_content";
		this.idShowFormMovementline = 'wx-btn-show-form-movementline';
		this.classDeleteRow = 'movementline-page-wx-btn-delete';
	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery


		thisObject.metaDataTable.ad_org_uu = localStorage.getItem('ad_org_uu');

		let buttonNewRecordHtml = '';
		if (thisObject.movementCMT.m_movement_wx_docstatus === 'DRAFT') {
			buttonNewRecordHtml = `<button type="button" name="new_record" class="btn btn-bold btn-label-brand btn-sm ` + thisObject.idShowFormMovementline + `"  id="` + thisObject.idBtnNewMovementline + `"> <i class="la la-plus"></i>New record</button>`;
		}

		let allHtml =
			buttonNewRecordHtml +
			`<table class="table table-striped- table-bordered table-hover table-checkable" id="` + thisObject.idTable + `">			
			</table>
	`;


		$('#' + thisObject.idFragmentContent).html(allHtml);


		//	thisObject.movementlineForm = new MovementlineForm(thisObject);

		thisObject.CreateDataTable();
		// attach event handler on the page

	}






/**
	 * Delete pada table, sebenarnya hanya menandai wx_isdeleted = 'Y'
	 * @param {*} dataObject dataObject pada datatable 
	 */
	async Delete(uuidDeleted) {
		let thisObject = this;
		let insertSuccess = 0;
		try {
			let sqlstatementDeleteBankaccount = HelperService.SqlDeleteStatement('m_movementline', 'm_movementline_uu', uuidDeleted);
			insertSuccess += await apiInterface.ExecuteSqlStatement(sqlstatementDeleteBankaccount);

			if (insertSuccess) {
				// delete row pada datatable
				thisObject.datatableReference
					.row($('#bttn_row_delete' + uuidDeleted).parents('tr'))
					.remove()
					.draw();

			}
		} catch (error) {
			apiInterface.Log(this.constructor.name, this.Delete.name, error);
		}

		return insertSuccess;
	}



	EventHandler() {
		let thisObject = this;



		$('.' + thisObject.idShowFormMovementline).off("click");
		$('.' + thisObject.idShowFormMovementline).on("click", function (e) {

			let attrName = $(this).attr('name');
			let movementlineCM = new MovementlineComponentModel();
			if (attrName !== 'new_record') {
				let dataEdited = thisObject.datatableReference.row($('#bttn_row_edit' + attrName).parents('tr')).data();
				movementlineCM = HelperService.ConvertRowDataToViewModel(dataEdited);
			}
			let movementlineFormPage = new MovementlineForm(movementlineCM, thisObject);

			//$('#' + thisObject.idFormModal).modal('show');

		});

		$('.' + thisObject.classDeleteRow).off('click');
		$('.' + thisObject.classDeleteRow).on('click', function () {
			let nameDeleted = $(this).val();
			let uuid = $(this).attr('name');
			let deleteWidget = new DialogBoxDeleteWidget('Hapus Data', nameDeleted, uuid, thisObject);

		});

		$('#' + thisObject.idDialogBox + ' #ok_info_box').off("click");
		$('#' + thisObject.idDialogBox + ' #ok_info_box').on('click', function () {
			let uuid = $(this).attr('name');
			let dataDeleted = thisObject.datatableReference.row($('#bttn_row_delete' + uuid).parents('tr')).data();

			let insertSuccess = thisObject.Delete(dataDeleted);

			if (insertSuccess) {
				// delete row pada datatable
				thisObject.datatableReference
					.row($('#bttn_row_delete' + $(this).attr('name')).parents('tr'))
					.remove()
					.draw();
				$('#' + thisObject.idDialogBox).modal('hide');

			}

		});


		$('.wx-format-money').mask('#,##0', { reverse: true });

		thisObject.datatableReference.off('draw');
		thisObject.datatableReference.on('draw', function () {
			thisObject.EventHandler();

		});



	}

	CreateDataTable() {

		let thisObject = this;

		thisObject.metaDataTable.otherFilters = [' AND m_movementline.m_movement_uu = \'' + thisObject.movementCMT.m_movement_wx_m_movement_uu + '\''];

		var table = $('#' + thisObject.idTable);

		let columnDefDatatable = [];
		let columnsTable = [
			{ title: 'No', data: 'm_movementline_wx_line' },
			{ title: 'Produk', data: 'm_product_wx_name' },
			{ title: 'Satuan', data: 'c_uom_wx_name' },
			{ title: 'Jumlah Mutasi', data: 'm_movementline_wx_movementqty' }

		];

		if (thisObject.movementCMT.m_movement_wx_docstatus === 'DRAFT') {
			columnsTable.push({ title: 'Action' });
			columnDefDatatable.push({
				targets: -1,
				title: 'Actions',
				orderable: false,
				render: function (data, type, full, meta) {
					return `
					<button id="bttn_row_edit`+ full.m_movementline_wx_m_movementline_uu + `" name="` + full.m_movementline_wx_m_movementline_uu + `" type="button"   class="` + thisObject.idShowFormMovementline + `   btn btn-outline-hover-info btn-elevate btn-icon" ><i class="la la-edit"></i></button>
					<button id="bttn_row_delete`+ full.m_movementline_wx_m_movementline_uu + `" value="` + full.m_product_wx_name + `" name="` + full.m_movementline_wx_m_movementline_uu + `" type="button" class="`+thisObject.classDeleteRow + ` btn btn-outline-hover-info btn-elevate btn-icon"> <i class="la la-trash"></i></button>
					`;
				},
			});



		}



		thisObject.movementlineService.FindMovementlineCMTByMMovementUu(thisObject.movementCMT.m_movement_wx_m_movement_uu)
			.then(function (movementCMTList) {

				// begin first table
				if (!$.fn.dataTable.isDataTable('#' + thisObject.idTable)) {
					thisObject.datatableReference = table.DataTable({
						searching: false,
						paging: false,
						responsive: true,
						data: movementCMTList,
						scrollX: true,
						columns: columnsTable,
						columnDefs: columnDefDatatable,
						"order": [[1, 'asc']],

					});


					thisObject.EventHandler();


				}

			});



	}



}
