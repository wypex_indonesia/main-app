/**
 * qty = barang sesuai dengan uom yang tertulis
 * inventoryqty = barang sesuai dengan uom based uom
 */
class MovementlinePage {

	/**
	 * 
	 * @param {movementCMT} movementCMT parent InvoiceComponentModelTable
	 * @param {string} idFragmentContent name id fragment content untuk meletakkan html content dari productbom_page
	 */
	constructor(movementCMT, idFragmentContent) {

		this.movementCMT = movementCMT;


		this.viewModel = new MovementlineComponentModel();
		this.metaDataTable = new MetaDataTable();
		this.productService = new ProductService();
		this.storageOnHandService = new StorageOnHandService();

		this.datatableReference = {}; // buat reference datatable

		this.movementlineService = new MovementlineService();
		//	this.movementlineForm;

		this.idFragmentContent = idFragmentContent;

		this.idTable = 'wx_movementline_table_' + movementCMT.m_movement_wx_m_movement_uu;
		this.idInputNewmovementline = 'new_record_movementline_wx_' + movementCMT.m_movement_wx_m_movement_uu;

		this.idFormFragmentContent = "wx_movementline_form_content";
		this.idShowFormMovementline = 'wx-btn-show-form-movementline';
		this.classNameFormControl = 'm-movementline-form-control' + movementCMT.m_movement_wx_m_movement_uu;
		this.classDeleteRow = 'movementline_page_delete_row';
		this.productSearchDialogBox = null;
	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery


		thisObject.metaDataTable.ad_org_uu = localStorage.getItem('ad_org_uu');

		let inputSearchNewRecordHtml = '';
		if (thisObject.movementCMT.m_movement_wx_docstatus === 'DRAFT') {
			inputSearchNewRecordHtml = `
			<div class="form-group ">
					<label>Cari Produk Untuk Mutasi</label>
					 <div class="kt-input-icon kt-input-icon--left">
						<input type="text" class="form-control" placeholder="Search..." id="`+ thisObject.idInputNewmovementline + `">
							<span class="kt-input-icon__icon kt-input-icon__icon--left">
								<span><i class="la la-search"></i></span>
							</span>
						</div>
				<span class="form-text text-muted">Ketik sku/upc/ nama produk yang akan di mutasi</span>
			</div>
			
			`;
		}

		let allHtml =
			inputSearchNewRecordHtml +
			`<table class="table table-striped- table-bordered table-hover table-checkable" id="` + thisObject.idTable + `">			
			</table>`;


		$('#' + thisObject.idFragmentContent).html(allHtml);


		thisObject.CreateDataTable();
		// attach event handler on the page

	}







	/**
 * Delete pada table, sebenarnya hanya menandai wx_isdeleted = 'Y'
 * @param {*} dataObject dataObject pada datatable 
 */
	async Delete(uuiddeleted) {
		let thisObject = this;
		let insertSuccess = 0;
		try {
			let sqlstatementDeleteBankaccount = HelperService.SqlDeleteStatement('m_movementline', 'm_movementline_uu', uuiddeleted);

			insertSuccess += await apiInterface.ExecuteSqlStatement(sqlstatementDeleteBankaccount);

			if (insertSuccess) {

				thisObject.datatableReference
					.row($('#bttn_row_delete' + uuiddeleted).parents('tr'))
					.remove()
					.draw();
			}
		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.Delete.name, error);
		}

		return insertSuccess;
	}


	EventHandler() {
		let thisObject = this;



		/**
		 * 1. Cari di product Table 
		 *  1.1 jika ditemukan hanya 1 maka
		 * 	 	cari di m_storageonhand,
		 * 		1.1.1 Jika ditermukan di m_storage onhand, maka ambil storageonhand
		 * 			  dan masukkan ke dalam qtybook pada new movementline
		 * 		1.2.1 Jika tidak ditemukan di m_storage onhand, 
		 * 			 create new movementline 
		 *  1.2 Jika ditemukan lebih dari 1 mka
		 *  show form list product
		 */
		$('#' + thisObject.idInputNewmovementline).off("input");
		$('#' + thisObject.idInputNewmovementline).on("input", function (e) {
			let searchValue = $(this).val();
			if (searchValue.length >= 3) {
				let isSearching = true;

				if (isSearching) {
					let metaDataTableSearch = new MetaDataTable();
					metaDataTableSearch.ad_org_uu = localStorage.getItem('ad_org_uu');
					metaDataTableSearch.otherFilters = [' AND m_product.name LIKE  \'%' + searchValue + '%\'', ' OR m_product.sku LIKE  \'%' + searchValue + '%\'', ' OR m_product.upc LIKE  \'%' + searchValue + '%\''];
					let rows = thisObject.productService.FindAll(metaDataTableSearch).then(function (rows) {
						if (rows.length > 0) {

							if (rows.length === 1) {

								let productCMT = rows[0];
								thisObject.movementlineService.CreateNewmovementline(productCMT.m_product_wx_m_product_uu,
									thisObject.movementCMT, thisObject.datatableReference).then(function (inserted) {
										$('#' + thisObject.idInputNewmovementline).val('');


									});


							} else {
								searchValue = $('#' + thisObject.idInputNewmovementline).val();
								let functionPostSelectedProduct = function (productCMT) {

									return thisObject.PostSelectedProductFunction(productCMT);
								};
								thisObject.productSearchDialogBox = new ProductSearchDialogBox(searchValue, functionPostSelectedProduct);


							}

						}


					});

					isSearching = false;

				}
			}



		});

		$('.' + thisObject.classNameFormControl).off("input");
		$('.' + thisObject.classNameFormControl).on("input", function (e) {
			let m_movementline_uu = $(this).attr('name');
			thisObject.movementlineService.UpdateQtyCount(m_movementline_uu, $(this).val()).then(function (inserted) {


			});
		});



		$('.' + thisObject.classDeleteRow).off('click');
		$('.' + thisObject.classDeleteRow).on('click', function () {
			let nameDeleted = $(this).val();
			let uuid = $(this).attr('name');
			let deleteWidget = new DialogBoxDeleteWidget('Hapus Data', nameDeleted, uuid, thisObject);

		});



		thisObject.datatableReference.off('draw');
		thisObject.datatableReference.on('draw', function () {
			thisObject.EventHandler();

		});



	}

	CreateDataTable() {

		let thisObject = this;

		thisObject.metaDataTable.otherFilters = [' AND m_movementline.m_movement_uu = \'' + thisObject.movementCMT.m_movement_wx_m_movement_uu + '\''];

		var table = $('#' + thisObject.idTable);

		let columnDefDatatable = [];
		let columnsTable = [
			{ title: 'No', data: 'm_movementline_wx_line' },
			{ title: 'Produk', data: 'm_product_wx_name' },
			{ title: 'Satuan', data: 'c_uom_wx_name' },
			{ title: 'Jumlah Mutasi', data: 'm_movementline_wx_movementqty' }
		];

		if (thisObject.movementCMT.m_movement_wx_docstatus === 'DRAFT') {
			columnsTable.push({ title: 'Action' });
			columnDefDatatable.push({
				targets: -1,
				title: 'Action',
				orderable: false,
				render: function (data, type, full, meta) {

					let htmlbuttonDelete = '';

					htmlbuttonDelete = `					
						<button id="bttn_row_delete`+ full.m_movementline_wx_m_movementline_uu + `" value="` + full.m_product_wx_name + `" name="` + full.m_movementline_wx_m_movementline_uu + `" type="button" class="` + thisObject.classDeleteRow + ` btn btn-outline-hover-info btn-elevate btn-icon"> <i class="la la-trash"></i></button>
						`;



					return htmlbuttonDelete;
				},
			});

			columnDefDatatable.push({
				targets: 3,
				title: 'Jumlah Mutasi',
				orderable: false,
				render: function (data, type, full, meta) {
					let movementQty = 0;
					if (full.m_movementline_wx_movementqty) {
						movementQty = numeral(full.m_movementline_wx_movementqty).value();
					}

					let inputMovementQty = new Intl.NumberFormat().format(movementQty);

					if (thisObject.movementCMT.m_movement_wx_docstatus === 'DRAFT') {
						inputMovementQty = `
						<input id="input_`+ full.m_movementline_wx_m_movementline_uu + `" name="` + full.m_movementline_wx_m_movementline_uu + `" type="number"   class="` +
							thisObject.classNameFormControl + `" value="` + movementQty + `" form-control" aria-describedby="emailHelp" >
						`;

					}


					return inputMovementQty;
				},
			});



		}





		thisObject.movementlineService.FindMovementlineCMTByMMovementUu(thisObject.movementCMT.m_movement_wx_m_movement_uu)
			.then(function (movementlineCMTList) {
				// begin first table
				if (!$.fn.dataTable.isDataTable('#' + thisObject.idTable)) {
					thisObject.datatableReference = table.DataTable({
						searching: false,
						paging: false,
						responsive: true,
						data: movementlineCMTList,
						scrollX: true,
						columns: columnsTable,
						columnDefs: columnDefDatatable,
						"order": [[0, 'asc']],

					});


				}
				thisObject.EventHandler();
			})


	}

	PostSelectedProductFunction(productCMT) {
		let thisObject = this;

		thisObject.movementlineService.CreateNewmovementline(productCMT.m_product_wx_m_product_uu,
			thisObject.movementCMT,
			thisObject.datatableReference).then(function (insertSuccess) {

				if (insertSuccess) {
					//$('#' + thisObject.idFormModal).modal('hide');
					thisObject.productSearchDialogBox.HideDialog();
					//$('#input_' + newMovementline.m_movementline_uu).focus();
				}

			});



	}

}
