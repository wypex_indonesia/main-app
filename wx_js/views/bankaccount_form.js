class BankaccountForm {

	/**
	 * 
	
	
	 * @param {BankaccountViewModel} bankaccountCM
	 * @param {datatableReference} datatableReference data tabel pada role page, sebagai reference untuk mengupdate table tersebut	 
	 */
	constructor(bankaccountCM, datatableReference) {

		this.validation = {

			id_form: this.idFormValidation,

			c_bankaccount_wx_name: {
				required: true
			}

		};
		//bank account
		if(bankaccountCM.c_bankaccount.bankaccounttype === 'C'){
			this.titleForm = 'Add/Edit Bank Akun';
			this.labelNamaAkun = 'Nama Bank';
			this.labelNoAccount = 'Nomor Rekening';
			this.labelDescNoAccount = 'Masukkan No Rekening Bank';
			this.validation['c_bankaccount_wx_accountno'] = {
				required: true
			};
		}

		// PETTY CASH
		if(bankaccountCM.c_bankaccount.bankaccounttype === 'B'){
			this.titleForm = 'Add/Edit Petty Cash Akun';
			this.labelNamaAkun = 'Nama Petty Cash';
			this.labelNoAccount = 'Nomor Petty Cash';
			this.labelDescNoAccount = 'Masukkan No  Petty Cash jika ada';
		}

		

		this.viewModel = bankaccountCM;


		this.metaDataTable = new MetaDataTable();

		this.datatableReference = datatableReference; // buat reference datatable

		this.bankaccountService = new BankaccountService();


		this.idFormModal = 'c_bankaccount_form_wx_';
		this.idFormValidation = 'c_bankaccount_form_validation_';
		this.classNameFormControl = 'c_bankaccount-form-control';
		this.selectorClassNameFormControl = '.' + this.classNameFormControl;


		this.idButtonFormSubmit = 'c_bankaccount_form_submit_wx_';


	




		this.Init();


	

	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery

		try {

			HelperService.InsertReplaceFormModal(thisObject.idFormModal, thisObject.GenerateFormInput());

			HelperService.InjectViewModelToForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.viewModel);
			thisObject.PageEventHandler();
		} catch (error) {
			apiInterface.Log(this.constructor.name, this.Init.name, error);
		}


	}




	/**
	 * 
	
	 */
	async Save() {
		let thisObject = this;

		try {

			let insertSuccess = 0;
			if (HelperService.CheckValidation(thisObject.validation)) {

				thisObject.viewModel = HelperService.GetViewModelFromForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.viewModel);

				insertSuccess = await thisObject.bankaccountService.SaveUpdate(thisObject.viewModel, thisObject.datatableReference);
				// jika sukses tersimpan 
				if (insertSuccess > 0) {

					$('#' + thisObject.idFormModal).modal('hide');


				}

			}
		} catch (err) {
			apiInterface.Log(this.constructor.name, this.Save.name, err);
		}



	}

	GenerateFormInput() {
		let thisObject = this;

		let html = '';
		try {
			html = `
		<div class="modal fade" id="` + thisObject.idFormModal + `" tabindex="-1" role="dialog"
			aria-labelledby="bankaccount-form-labelled" style="display: none;" aria-hidden="true">
		
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="bankaccount-form-labelled">`+ thisObject.titleForm + `</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						</button>
					</div>
					<form id="`+ thisObject.idFormValidation + `" class="kt-form">
						<div class="modal-body">
		
							<div class="form-group">
								<label>`+thisObject.labelNamaAkun +`</label>
								<input id="c_bankaccount_wx_name" type="text"
									class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp"
									placeholder="`+thisObject.labelNamaAkun+`">
								
							</div>
							<div class="form-group">
								<label>`+thisObject.labelNoAccount+`</label>
								<input id="c_bankaccount_wx_accountno" type="text" value=""
									class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp"
									placeholder="`+thisObject.labelNoAccount+`">
								<span class="form-text text-muted">`+thisObject.labelDescNoAccount+`</span>
							</div>
		
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button id='bankaccount-form-submit' type="submit" class="btn btn-primary">Save changes</button>
						</div>
		
					</form>
				</div>
			</div>
		</div>
`;
			
		} catch (error) {
			apiInterface.Log(this.constructor.name, this.GenerateFormInput.name, error);
		}

		return html;
	}


	PageEventHandler() {
		let thisObject = this;


		try {

			$('#' + thisObject.idFormValidation).off('submit');
			$('#' + thisObject.idFormValidation).on('submit', function (e) {
				thisObject.Save();
				e.preventDefault();
				

			});



			$('#' + thisObject.idFormModal).modal('show');


		} catch (err) {
			apiInterface.Log(this.constructor.name, this.PageEventHandler.name, err);
		}


	}



}
