class InvoicelineForm {

	/**
	 * 
	 * @param {string} idFormFragmentContent name id fragment content untuk meletakkan html content dari form_html
	 * @param {InvoicelineComponentModel}
	 *  @param {InvoicelineDataTable} datatableReference
	 */
	constructor(invoicelineCM, datatableReference) {
		
		this.componentModel = invoicelineCM;

		this.orderlineService = new OrderlineService();
		this.uomService = new UomService();
		this.productService = new ProductService();
		this.taxService = new TaxService();
		this.invoicelineService = new InvoicelineService();
	
		this.selectedProduct = {}; // current product yang sedang dipilih, product_viewmodel_table
		this.idFormModal = 'c_invoiceline_form_wx_';
		this.idFormValidation = 'c_invoiceline_form_validation_' ;
		this.classNameFormControl = 'c-orderline-form-control';
		this.selectorClassNameFormControl = '.' + this.classNameFormControl;


		this.idButtonFormSubmit = 'c_invoiceline_form_submit_wx_' ;

		this.table_uomconversion_viewmodel = [];
		this.table_tax_viewmodel = [];
		this.datatableReference = datatableReference;
	
		this.inputPriceactual;
		this.inputPriceentered;
		this.inputLinenetamt;
		this.ctrlSelectedUom;
		this.ctrlBaseUom;
		this.validation = {
			id_form: this.idFormValidation,
			m_product_wx_name: {
				required: true
			},
			c_uom_wx_c_uom_uu: {
				required: true
			},

			c_tax_wx_c_tax_uu: {
				required: true
			},

			c_invoiceline_wx_qtyinvoiced: {
				required: true
			},

			c_invoiceline_wx_priceentered: {
				required: true
			},


		};


		this.Init();

	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery

		HelperService.InsertReplaceFormModal(thisObject.idFormModal, thisObject.GenerateFormInput());

		thisObject.inputPriceactual = $('#' + this.idFormValidation + ' #c_invoiceline_wx_priceactual');
		thisObject.inputPriceentered = $('#' + this.idFormValidation + ' #c_invoiceline_wx_priceentered');
		thisObject.inputLinenetamt = $('#' + this.idFormValidation + ' #c_invoiceline_wx_linenetamt');
		thisObject.ctrlSelectedUom = $('#' + this.idFormValidation + ' .selected-uom');
		thisObject.ctrlBaseUom = $('#' + this.idFormValidation + ' .base-uom');

		thisObject.productService.FindProductViewModelByMProductUu(thisObject.componentModel.m_product.m_product_uu)
		.then(function(selectedProductList){
			thisObject.selectedProduct = selectedProductList[0];

			// masukkan nama satuan di class base-uom dan selected-uom
			thisObject.ctrlSelectedUom.text(thisObject.componentModel.c_uom.name);
			thisObject.ctrlBaseUom.text(thisObject.selectedProduct.c_uom_wx_name);
			thisObject.GenerateUomConversionOnProduct(thisObject.selectedProduct.m_product_wx_m_product_uu);
	
	
			HelperService.InjectViewModelToForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.componentModel);
	


		});

		this.EventHandler();


	}

	

	/**
	 * 
	
	 */
	async SaveUpdate() {
		let thisObject = this;
		let insertSuccess = 0;

		if (HelperService.CheckValidation(thisObject.validation)) {
			thisObject.calculatePriceactual();
			thisObject.calculateLinenetamt();

			thisObject.calculateLinetaxamt();

			thisObject.componentModel = HelperService.GetViewModelFromForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.componentModel);

			insertSuccess = await thisObject.invoicelineService.SaveUpdate(thisObject.componentModel.c_invoiceline, thisObject.datatableReference);
			// jika sukses tersimpan 
			if (insertSuccess > 0) {

				$('#' + thisObject.idFormModal).modal('hide');


			}

		/*	const timeNow = new Date().getTime();
			// jika new record maka buat mlocator 	
			// harus dibuat default locator, karena locator akan dipergunakan di inventory movement
			let isNewRecord = false;
			if (!thisObject.componentModel.c_invoiceline.c_invoiceline_uu) { //new record
				isNewRecord = true;
				thisObject.componentModel.c_invoiceline.c_invoiceline_uu = HelperService.UUID();
				thisObject.componentModel.c_invoiceline.ad_client_uu = localStorage.getItem('ad_client_uu');
				thisObject.componentModel.c_invoiceline.ad_org_uu = localStorage.getItem('ad_org_uu');

				thisObject.componentModel.c_invoiceline.createdby = localStorage.getItem('ad_user_uu');
				thisObject.componentModel.c_invoiceline.updatedby = localStorage.getItem('ad_user_uu');

				thisObject.componentModel.c_invoiceline.created = timeNow;
				thisObject.componentModel.c_invoiceline.updated = timeNow;
			} else { //update record


				thisObject.componentModel.c_invoiceline.updatedby = localStorage.getItem('ad_user_uu');

				thisObject.componentModel.c_invoiceline.updated = timeNow;
			}

		//	thisObject.componentModel.c_invoiceline.c_invoice_uu = thisObject.invoiceComponentModelTable.c_invoice_wx_c_invoice_uu;
			thisObject.componentModel.c_invoiceline.m_product_uu = thisObject.componentModel.m_product.m_product_uu;
			thisObject.componentModel.c_invoiceline.c_uom_uu = thisObject.componentModel.c_uom.c_uom_uu;
			thisObject.componentModel.c_invoiceline.c_tax_uu = thisObject.componentModel.c_tax.c_tax_uu;
			thisObject.componentModel.c_invoiceline.sync_client = null;
			thisObject.componentModel.c_invoiceline.process_date = null;

			let insertSuccess = 0;
			const sqlInsert = HelperService.SqlInsertOrReplaceStatement('c_invoiceline', thisObject.componentModel.c_invoiceline);

			return new Promise(function (resolve, reject) {
				apiInterface.ExecuteSqlStatement(sqlInsert).then(function (rowChanged) {

					insertSuccess += rowChanged;

					if (insertSuccess > 0) {


						thisObject.invoicelineService.FindInvoicelineComponentModelByCInvoicelineUu(thisObject.componentModel.c_invoiceline.c_invoiceline_uu)
						.then(function(rowDataList){
							let rowData = {};

							if (rowDataList.length > 0) {
								rowData = rowDataList[0];
							}
	
							thisObject.datatableReference.row($('#bttn_row_edit' + rowData['c_invoiceline_wx_c_invoiceline_uu']).parents('tr')).data(rowData).draw();
						
							resolve(insertSuccess);
							$('#' + thisObject.idFormModal).modal('hide');
						});

					
					}else{

						resolve(insertSuccess);
					}
					


				});





			});

			// jika sukses tersimpan 

		}


	*/}

	}

	GenerateFormInput() {
		let thisObject = this;

		let metaDataTableTax = new MetaDataTable();
		metaDataTableTax.ad_org_uu = localStorage.getItem('ad_org_uu');



		return `
	<div class="modal fade" id="` + thisObject.idFormModal + `" tabindex="-1" role="dialog"
		aria-labelledby="product-form-labelled" style="display: none;" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="product-form-labelled">Orderline Form</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<form id="` + thisObject.idFormValidation + `" class="kt-form">
					<div class="modal-body">
						<div class="form-group">
							<label>No</label>
							<input id="c_invoiceline_wx_line" type="text"
								class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp">
							<span class="form-text text-muted">Nomor urut</span>
						</div>
						<div class="form-group form-group-marginless">
							<label>Produk</label>
							<input disabled="disabled" type="text"
								class="` + thisObject.classNameFormControl + ` form-control" id="m_product_wx_name">
	
							<input type="hidden" class="` + thisObject.classNameFormControl + `"
								id="m_product_wx_m_product_uu">
						</div>
	
						<div class="form-group">
							<label>Jumlah Produk Yang Di Order</label>
							<div class="input-group">
								<input id="c_invoiceline_wx_qtyentered" type="text"
									class="` + thisObject.classNameFormControl + ` form-control"
									aria-describedby="emailHelp">
								<input type="hidden" class="` + thisObject.classNameFormControl + `" id="c_uom_wx_c_uom_uu">
								<div class="input-group-prepend"><span class="input-group-text selected-uom"></span></div>
							</div>
						</div>
						<div class="form-group">
							<label>Jumlah Produk Dengan Satuan Dasar</label>
							<div class="input-group">
								<input disabled="disabled" class="` + thisObject.classNameFormControl + ` form-control"
									placeholder="Jumlah Produk Dengan Satuan Dasar" type="number"
									id="c_invoiceline_wx_qtyinvoiced">
								<div class="input-group-prepend"><span class="input-group-text base-uom"></span></div>
							</div>
						</div>
	
						<div class="form-group">
							<label>Harga Produk</label>
							<div class="input-group">
								<input id="c_invoiceline_wx_priceentered" type="text"
									class="` + thisObject.classNameFormControl + ` wx-format-money  form-control"
									aria-describedby="emailHelp">
								<div class="input-group-prepend"><span class="input-group-text">/</span><span
										class="input-group-text selected-uom"></span></div>
							</div>
						</div>
						<div class="form-group">
							<label>Harga Produk Dengan Satuan Dasar</label>
							<div class="input-group">
								<input disabled="disabled" id="c_invoiceline_wx_priceactual" type="text"
									class="` + thisObject.classNameFormControl + ` wx-format-money form-control"
									aria-describedby="emailHelp">
								<div class="input-group-prepend"><span class="input-group-text">/</span><span
										class="input-group-text base-uom"></span></div>
							</div>
						</div>
						<div class="form-group ">
							<label>Pajak</label>
							<input disabled="disabled" type="text"
								class="` + thisObject.classNameFormControl + ` form-control" id="c_tax_wx_name">
	
							<input type="hidden" class="` + thisObject.classNameFormControl + `" id="c_tax_wx_c_tax_uu">
	
						</div>
						<div class="form-group">
							<label>Harga Net Total</label>
							<input disabled="disabled" id="c_invoiceline_wx_linenetamt" type="text"
								class="` + thisObject.classNameFormControl + ` wx-format-money form-control"
								aria-describedby="emailHelp">
							<span class="form-text text-muted">Harga Net total</span>
						</div>
						<div class="form-group">
							<label>Tax Amount</label>
							<input disabled="disabled" id="c_invoiceline_wx_taxamt" type="text"
								class="` + thisObject.classNameFormControl + ` wx-format-money form-control"
								aria-describedby="emailHelp">
							<span class="form-text text-muted">Jumlah Tax Amount</span>
						</div>
						<div class="form-group">
							<label>Total</label>
							<input disabled="disabled" id="c_invoiceline_wx_linetotalamt" type="text"
								class="` + thisObject.classNameFormControl + ` wx-format-money form-control"
								aria-describedby="emailHelp">
							<span class="form-text text-muted">Jumlah Total</span>
						</div>
	
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button id="`+ thisObject.idButtonFormSubmit + `" type="submit" class="btn btn-primary">Save
							changes</button>
					</div>
				</form>
			</div>
		</div>
	</div>
		
		`;
	}

	EventHandler() {
		let thisObject = this;
		$('#' + thisObject.idFormValidation).on('submit');
		$('#' + thisObject.idFormValidation).on('submit', function (e) {
			
			thisObject.SaveUpdate();
			e.preventDefault();
		});


		$('#' + thisObject.idFormValidation + ' #c_invoiceline_wx_qtyentered').off('input');
		$('#' + thisObject.idFormValidation + ' #c_invoiceline_wx_qtyentered').on('input', function () {
			thisObject.calculateQtyBasedUomConversion();

			thisObject.calculatePriceactual();

			thisObject.calculateLinenetamt();

			thisObject.calculateLinetaxamt();

			thisObject.calculateLinetotalamt();

		});

		thisObject.inputPriceentered.off('input');
		thisObject.inputPriceentered.on('input', function () {

			thisObject.calculatePriceactual();

			thisObject.calculateLinenetamt();

			thisObject.calculateLinetaxamt();
			thisObject.calculateLinetotalamt();
		});


		$('#' + thisObject.idFormModal).modal('show');

		$('.wx-format-money').mask('#,##0', { reverse: true });

	}

	calculatePriceactual() {
		let thisObject = this;
		let cUomUu = $('#' + thisObject.idFormValidation + ' #c_uom_wx_c_uom_uu').val();
		let priceEntered = numeral(thisObject.inputPriceentered.val()).value();

		if (thisObject.selectedProduct) {

			if (thisObject.selectedProduct.c_uom_wx_c_uom_uu === cUomUu) {
				thisObject.inputPriceactual.val(priceEntered);
			} else {
				let priceActual = 0;
				if (thisObject.table_uomconversion_viewmodel) {
					const cUomConversionFound = _.find(thisObject.table_uomconversion_viewmodel, (cUomConversion) => {
						return cUomConversion.c_uomto_wx_c_uom_uu === cUomUu;
					});

					if (cUomConversionFound) {

						priceActual = priceEntered / cUomConversionFound.c_uom_conversion_wx_dividerate;
					}
					thisObject.inputPriceactual.val(priceActual);
				}
			}

		}

	}
	/**
	 * Menghitung total jumlah, 
	 */
	calculateLinenetamt() {
		let thisObject = this;
		let priceActual = numeral(thisObject.inputPriceactual.val()).value();
		let qtyInvoiced = $('#' + thisObject.idFormValidation + ' #c_invoiceline_wx_qtyinvoiced').val();
		let linenetamt = priceActual * qtyInvoiced;
		thisObject.inputLinenetamt.val(linenetamt);

	}


	/**
 * Menghitung total jumlah, 
 */
	calculateLinetaxamt() {
		let thisObject = this;
		let linenetamt = numeral(thisObject.inputLinenetamt.val()).value();
		let selectedTaxUu = $('#' + thisObject.idFormValidation + ' #c_tax_wx_c_tax_uu').val();
		let taxFound = _.find(thisObject.table_tax_viewmodel, (tableTax) => {
			return tableTax.c_tax_wx_c_tax_uu === selectedTaxUu;
		});

		if (taxFound) {
			let rate = taxFound.c_tax_wx_rate;
			let linetaxamt = 0;
			if (rate) {
				linetaxamt = rate / 100 * linenetamt;

			}

			$('#' + thisObject.idFormValidation + ' #c_invoiceline_wx_taxamt').val(linetaxamt);
		}



	}

	calculateLinetotalamt() {
		let thisObject = this;
		let linenetamt = numeral(thisObject.inputLinenetamt.val()).value();
		let taxamt = numeral($('#' + thisObject.idFormValidation + ' #c_invoiceline_wx_taxamt').val()).value();

		let linetotalamt = linenetamt + taxamt;
		$('#' + thisObject.idFormValidation + ' #c_invoiceline_wx_linetotalamt').val(linetotalamt);




	}


	/**
	 * Menghitung quantity berdasarkan selectedUom dan cUomConversion.
	 *
	 */
	calculateQtyBasedUomConversion() {

		let thisObject = this;
		let orderlineService = new OrderlineService();
		let cUomToUu = $('#' + thisObject.idFormValidation + ' #c_uom_wx_c_uom_uu').val();
		let qtyentered = $('#' + thisObject.idFormValidation + ' #c_invoiceline_wx_qtyentered').val();

		let qtyOrdered = orderlineService.CalculateQtyBasedUomConversion(cUomToUu, qtyentered, this.table_uomconversion_viewmodel);

		$('#' + thisObject.idFormValidation + ' #c_invoiceline_wx_qtyinvoiced').val(qtyOrdered);

	}

	/**
	 * Generate Uom, dengan baseUom produk, conversion Uom 
	 * @param {*} productViewModelTable  viewmodel seperti table
	 * 
	 */
	GenerateUomConversionOnProduct(m_product_uu) {
		// GENERATE UOM
		let thisObject = this;
		let uomConversionService = new UomConversionService();
		let metaDataResponseTableUom = new MetaDataTable();
		metaDataResponseTableUom.ad_org_uu = localStorage.getItem('ad_org_uu');
		metaDataResponseTableUom.otherFilters = [' AND c_uom_conversion.m_product_uu = \'' + m_product_uu + '\''];

		uomConversionService.FindAll(metaDataResponseTableUom).then(function (uomConversionCMTList) {

			thisObject.table_uomconversion_viewmodel = uomConversionCMTList;

		});

	}




	GenerateForm() {
		let thisObject = this;
		return ``;


	}




}
