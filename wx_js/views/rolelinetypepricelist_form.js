class RolelineTypePricelistForm {

    /**
     * Widget untuk pemilihan pricelist
     * 
     * pricelist harga pembelian  m_pricelist_uu = 'fd61186c-6ade-4e33-9db8-fb81b82cef5b'
     * pricelist harga penjualan m_pricelist_uu = 'c90fbb80-589b-4943-b1eb-e22beb25178a
     
     */
    constructor(rolelineCM, dataTableReference) {
        this.idFormValidation = 'wxt_roleline_pricelist_form_validation_';
        this.classNameFormControl = 'wxt_roleline-pricelist-form-control';
        this.idFormModal = 'wxt_roleline_pricelist_form_modal';
        this.idButtonFormSubmit = 'wxt_bttn_submit_roleline_form_pricelist_validation';
        this.rolelineCM = rolelineCM;
        this.dataTableReference = dataTableReference;

        this.Init();
    }

    Init() {

        let thisObject = this;
        HelperService.InsertReplaceFormModal(thisObject.idFormModal, thisObject.GenerateForm());
        thisObject.InjectToForm(thisObject.rolelineCM.wxt_roleline.data);

        thisObject.EventHandler();

    }





    GenerateForm() {
        let thisObject = this;
        let allHtml = '';
        try {

            allHtml = `
            <div class="form-group">
				<label>Hak Akses View Harga Jual Beli</label>
				<div class="kt-checkbox-list">
					<label class="kt-checkbox kt-checkbox--solid">
						<input id="c90fbb80-589b-4943-b1eb-e22beb25178a" class="`+ thisObject.classNameFormControl + `" type="checkbox">View Harga Jual Produk
						<span></span>
					</label>
					<label class="kt-checkbox kt-checkbox--solid">
						<input id="fd61186c-6ade-4e33-9db8-fb81b82cef5b"  class="`+ thisObject.classNameFormControl + `" type="checkbox">View Harga Beli Produk
						<span></span>
					</label>
			    </div>				
			</div>
            `;



        } catch (error) {
            apiInterface.Log(this.constructor.name, this.GenerateForm.name, error);
        }

        return `
        <div class="modal fade" id="` + thisObject.idFormModal + `" tabindex="-1" role="dialog" aria-labelledby="product-form-labelled"
		style="display: none;" aria-hidden="true">
		    <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="product-form-labelled">Hak Akses Menu</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <form id="`+ thisObject.idFormValidation + `" class="kt-form">
                        <div class="modal-body">
                            <div class="alert alert-secondary" role="alert">
                                <div class="alert-icon"><i class="flaticon-warning kt-font-brand"></i></div>
                                <div class="alert-text">
                                    Pilih Hak Akses Harga Jual atau Harga Beli untuk role yang dipilih. 
                                    User akan memiliki hak akses melihat harga jual atau harga beli di produk.
                                 </div>
                            </div>`+ allHtml + `
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button id="`+ thisObject.idButtonFormSubmit + `" type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>`;
    }



    SaveUpdate() {

        let thisObject = this;
        let pricelistUuidList = [];
        let rolelineService = new RolelineService();
        let insertSuccess = 0;
        try {
            $('#' + thisObject.idFormValidation + ' .' + thisObject.classNameFormControl).each(function () {
                let m_pricelist_uu = $(this).attr('id');
                if ($(this).attr('type') === "checkbox") {
                    if (!$(this).prop('checked')) {
                        pricelistUuidList.push(m_pricelist_uu);
                    }
                }
            });


            rolelineService.FindRolelineCMTListByAdRoleUuAndClassName(thisObject.rolelineCM.wxt_roleline.ad_role_uu, thisObject.rolelineCM.wxt_roleline.class_name)
                .then(function (rolelineCMTList) {
                    if (rolelineCMTList.length) {
                        let rolelineCMT = rolelineCMTList[0];
                        thisObject.rolelineCM.wxt_roleline.wxt_roleline_uu = rolelineCMT.wxt_roleline_wx_wxt_roleline_uu;
                    }

                    thisObject.rolelineCM.wxt_roleline.data = JSON.stringify(pricelistUuidList);
                    rolelineService.SaveUpdate(thisObject.rolelineCM, thisObject.dataTableReference)
                        .then(function (rowChanged) {

                            insertSuccess += rowChanged;
                            if (insertSuccess) {
                                $('#' + thisObject.idFormModal).modal('hide');
                            }

                            resolve(insertSuccess);
                        });



                });


        } catch (error) {
            apiInterface.Log(this.constructor.name, this.SaveUpdate.name, error);
        }



    }

    InjectToForm(dataJson) {

        let thisObject = this;
        try {
            if (dataJson) {

                let m_pricelist_uuList = JSON.parse(dataJson);

                $('#' + thisObject.idFormValidation + ' .' + thisObject.classNameFormControl).each(function () {
                    let m_pricelist_uu = $(this).attr('id');

                    if ($(this).attr('type') === "checkbox") {

                        let indexFound = _.findIndex(m_pricelist_uuList, function (pricelistUu) { return pricelistUu === m_pricelist_uu; });
                        if (indexFound >= 0) {
                            $(this).prop('checked', false);
                        } else {
                            $(this).prop('checked', true);
                        }
                    }


                    // masukkan value

                });
            }
        } catch (error) {
            apiInterface.Log(this.constructor.name, this.InjectToForm.name, error);
        }


    }

    EventHandler() {

        let thisObject = this;
        $('#' + thisObject.idFormValidation).off('submit');
        $('#' + thisObject.idFormValidation).on('submit', function () {

            thisObject.SaveUpdate();
        });

        $('#' + thisObject.idButtonFormSubmit).off('click');
        $('#' + thisObject.idButtonFormSubmit).on('click', function () {
            thisObject.SaveUpdate();

        });

        $('#' + thisObject.idFormModal).modal('show');

    }

}