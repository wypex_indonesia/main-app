/**
 * Movementqty = masuknya barang sesuai dengan jumlah basedUom
 * Qtyentered = masuknya barang sesuai dengan uom yang tertulis
 */
class InoutlinePage {

	/**
	 * 
	 * @param {InoutViewModelTable} inoutViewModelTable parent inoutViewModel
	 * @param {string} idFragmentContent name id fragment content untuk meletakkan html content dari productbom_page
	 */
	constructor(inoutViewModelTable, idFragmentContent) {

		this.inoutViewModelTable = inoutViewModelTable;


		this.viewModel = new InoutlineViewModel();

		this.metaDataTable = new MetaDataTable();


		this.datatableReference = {}; // buat reference datatable

		this.inoutlineService = new InoutlineService();

		this.idFragmentContent = idFragmentContent;
		this.idFormModal = 'm_inoutline_form_wx_' + inoutViewModelTable.m_inout_wx_m_inout_uu;
		this.idFormValidation = 'm_inoutline_form_validation_' + inoutViewModelTable.m_inout_wx_m_inout_uu;

		this.idDialogBox = 'm_inoutline_dialog_box_wx_' + inoutViewModelTable.m_inout_wx_m_inout_uu;
		this.idTable = 'wx_inoutline_table_' + inoutViewModelTable.m_inout_wx_m_inout_uu;
		this.idBtnGenerateInoutline = 'generate_record_inoutline_wx_' + inoutViewModelTable.m_inout_wx_m_inout_uu;
		this.idButtonFormSubmit = 'm_inoutline_form_submit_wx_' + inoutViewModelTable.m_inout_wx_m_inout_uu;

		this.idFormFragmentContent = "wx_inoutline_form_content";
		this.idShowFormInoutline = 'wx-btn-show-form-inoutline';
		this.classDeleteRow = 'inoutline_page_delete_row';

	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery


		thisObject.metaDataTable.ad_org_uu = localStorage.getItem('ad_org_uu');

		let buttonNewRecordHtml = '';
		if (thisObject.inoutViewModelTable.m_inout_wx_docstatus === 'DRAFT') {
			buttonNewRecordHtml = `<button type="button" class="btn btn-bold btn-label-brand btn-sm"  id="` + thisObject.idBtnGenerateInoutline + `"> <i class="la la-plus"></i>Generate Item Barang Dari No ` + thisObject.inoutViewModelTable.c_order_wx_documentno + `</button>`;
		}

		let allHtml =
			buttonNewRecordHtml +
			`<table class="table table-striped- table-bordered table-hover table-checkable" id="` + thisObject.idTable + `">			
			</table>
		<div id="`+ thisObject.idFormFragmentContent + `"></div>`;





		$('#' + thisObject.idFragmentContent).html(allHtml);


		thisObject.CreateDataTable();
		// attach event handler on the page




	}






	/**
	 * Delete pada table, sebenarnya hanya menandai wx_isdeleted = 'Y'
	 * @param {*} dataObject dataObject pada datatable 
	 */
	async Delete(uuidDeleted) {
		let thisObject = this;
		let insertSuccess = 0;
		try {
			let sqlstatementDeleteBankaccount = HelperService.SqlDeleteStatement('m_inoutline', 'm_inoutline_uu', uuidDeleted);
			insertSuccess += await apiInterface.ExecuteSqlStatement(sqlstatementDeleteBankaccount);

			if (insertSuccess) {
				// delete row pada datatable
				thisObject.datatableReference
					.row($('#bttn_row_delete' + uuidDeleted).parents('tr'))
					.remove()
					.draw();

			}
		} catch (error) {
			apiInterface.Log(this.constructor.name, this.Delete.name, error);
		}

		return insertSuccess;
	}




	CreateDataTable() {

		let thisObject = this;

		thisObject.metaDataTable.otherFilters = [' AND m_inoutline.m_inout_uu = \'' + thisObject.inoutViewModelTable.m_inout_wx_m_inout_uu + '\'']

		var table = $('#' + thisObject.idTable);

		let columnDefDatatable = [];
		let columnsTable = [
			{ title: 'No', data: 'm_inoutline_wx_line' },
			{ title: 'Produk', data: 'm_product_wx_name' },
			{ title: 'Jumlah' },
			{ title: 'Jumlah Aktual' }
		];


		if (thisObject.inoutViewModelTable.m_inout_wx_docstatus === 'DRAFT') {
			columnsTable.push({ title: 'Action' });
			columnDefDatatable.push({
				targets: -1,
				title: 'Actions',
				orderable: false,
				render: function (data, type, full, meta) {
					return `
					<button id="bttn_row_edit`+ full.m_inoutline_wx_m_inoutline_uu + `" name="` + full.m_inoutline_wx_m_inoutline_uu + `" type="button"  class="` + thisObject.idShowFormInoutline + ` btn btn-outline-hover-info btn-elevate btn-icon"><i class="la la-edit"></i></button>
					<button id="bttn_row_delete`+ full.m_inoutline_wx_m_inoutline_uu + `" value="` + full.m_product_wx_name + `" name="` + full.m_inoutline_wx_m_inoutline_uu + `" type="button" class="` + thisObject.classDeleteRow + ` btn btn-outline-hover-info btn-elevate btn-icon"> <i class="la la-trash"></i></button>
					`;
				},
			});



		}

		columnDefDatatable.push({
			targets: 2,
			title: 'Jumlah',
			orderable: false,
			render: function (data, type, full, meta) {
				let qtyentered = new Intl.NumberFormat().format(full.m_inoutline_wx_qtyentered);
				let uomSelected = full.c_uom_wx_name;
				return qtyentered + ' ' + uomSelected;
			},
		});

		columnDefDatatable.push({
			targets: 3,
			title: 'Jumlah Aktual',
			orderable: false,
			render: function (data, type, full, meta) {
				let movementqty = new Intl.NumberFormat().format(full.m_inoutline_wx_movementqty);
				let basedUom = full.c_uom_based_wx_name;
				return movementqty + ' ' + basedUom;
			},
		});


		thisObject.inoutlineService.FindInoutlineViewModelByMInoutUu(thisObject.inoutViewModelTable.m_inout_wx_m_inout_uu)
			.then(function (inoutCMTList) {
				// begin first table
				if (!$.fn.dataTable.isDataTable('#' + thisObject.idTable)) {
					thisObject.datatableReference = table.DataTable({
						searching: false,
						paging: false,
						responsive: true,
						data: inoutCMTList,
						scrollX: true,
						columns: columnsTable,
						columnDefs: columnDefDatatable,
						"order": [[1, 'asc']],

					});

					thisObject.EventHandler();

				}


			});


	}


	EventHandler() {
		let thisObject = this;


		$('#' + thisObject.idBtnGenerateInoutline).off("click");
		$('#' + thisObject.idBtnGenerateInoutline).on("click", function (e) {
			thisObject.inoutlineService.GenerateInoutlineFromCOrderUu(thisObject.inoutViewModelTable).then(function (insertSuccess) {

				thisObject.Init();
			});

		});

		$('.' + thisObject.idShowFormInoutline).off("click");
		$('.' + thisObject.idShowFormInoutline).on("click", function (e) {

			let attrName = $(this).attr('name');
			let inoutlineViewModel = new InoutlineViewModel();

			let dataEdited = thisObject.datatableReference.row($('#bttn_row_edit' + attrName).parents('tr')).data();
			inoutlineViewModel = HelperService.ConvertRowDataToViewModel(dataEdited);

			let inoutlineFormPage = new InoutlineForm( inoutlineViewModel, thisObject.datatableReference, thisObject.inoutViewModelTable.m_inout_wx_c_doctype_id);



		});


		$('.' + thisObject.classDeleteRow).off('click');
		$('.' + thisObject.classDeleteRow).on('click', function () {
			let nameDeleted = $(this).val();
			let uuid = $(this).attr('name');
			let deleteWidget = new DialogBoxDeleteWidget('Hapus Data', nameDeleted, uuid, thisObject);

		});





		$('.wx-format-money').mask('#,##0', { reverse: true });

		thisObject.datatableReference.off('draw');
		thisObject.datatableReference.on('draw', function () {
			thisObject.EventHandler();
		});

	}


}
