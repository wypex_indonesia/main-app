class InvoicePage {

	/**
	 * 
	 * @param {*} invoiceType  // 'API' (AP Invoice)  ARI (AR Invoice)
	 * @param {*} idFragmentContent  // IdControl untuk meletakkan View Orderpage ex>. #fragmentContent
	 */
	constructor(invoiceType, idFragmentContent) {

		this.titlePage = '';

		this.invoiceType = invoiceType;
		this.viewModel = new InvoiceComponentModel();
		this.labelForm = '';
		this.labelNoDocument = '';
		this.labelVendorOrCustomer = '';
		this.labelNoOrder = '';
		this.labelNoInout = '';

		this.idTable = 'wx_invoice_table_';
		this.idFragmentContent = idFragmentContent;
		this.invoiceService = new InvoiceService();
		this.metaDataTable = new MetaDataTable();
		this.datatableReference = {}; // buat reference datatable
		this.idShowForm = 'wx-btn-show-form-invoice';
	
		this.idButtonRowStatusDialog = "bttn_row_status";
		this.idButtonPrintDialog = "bttn_print_dialog";
	
		this.idDialogPrint = 'print-dialog-box';
		this.idDialogForm = 'c_invoice_form_wx_';
		this.labelInvoiceReference = 'No Invoice Vendor';// No PO Vendor
		this.labelNoDocument = 'No Dokumen';
		this.prefixContentChild = 'invoiceline_page_';

		this.classDialogStatus = 'invoice_page_bttn_show_dialog_status';
		this.classDialogPrint = 'invoice_page_bttn_show_dialog_print';

		if (invoiceType === 'API') {


			this.titlePage = 'Invoice Vendor';

			this.labelVendorOrCustomer = 'Vendor';
			this.labelNoOrder = 'No Purchase Order';
			this.labelNoInout = 'No Material Receipt';
			this.metaDataTable.otherFilters.push(' AND c_doctype.docbasetype = \'API\'');
		}

		if (invoiceType === 'ARI') {

			//	this.c_order_wx_c_doctype_id = 2001; // SO
			this.titlePage = 'Invoice Customer';

			this.labelVendorOrCustomer = 'Customer';
			this.labelNoOrder = 'No Sales Order';
			this.labelNoInout = 'No Surat Jalan';
			this.metaDataTable.otherFilters.push(' AND c_doctype.docbasetype = \'ARI\'');

		}

		this.columnsDataTable =
			[
				{
					"className": 'details-control',
					"orderable": false,
					"data": null,
					"defaultContent": '<button  type="button" class="btn btn-outline-hover-info btn-icon"><i class="la la-plus-square-o"></i></button>'
				},
				{ title: this.labelNoDocument, data: 'c_invoice_wx_documentno' },
				{ title: this.labelNoOrder, data: 'c_order_wx_documentno' },
				{ title: this.labelInvoiceReference, data: 'c_invoice_wx_invoicereference' },
				{ title: this.labelVendorOrCustomer, data: 'c_bpartner_wx_name' },
				{ title: 'Total Lines' },
				{ title: 'Total Tax' },
				{ title: 'Grand Total' },
				{ title: "Status", data: 'c_invoice_wx_docstatus' },
				{ title: "Tanggal", data: 'c_invoice_wx_dateacct' },
				{ title: 'Action' }
			];

		if (invoiceType === 'ARI') {

			// buang column labelInvoiceReference, pada index 3
			this.columnsDataTable.splice(3, 1);
		}
		this.Init();

	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery


		let allHtml = `
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head kt-portlet__head--lg">
			<div  class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
			<i class="kt-font-brand flaticon2-line-chart"></i>
			</span>
			<h3 class="kt-portlet__head-title">
		 	`+ thisObject.titlePage + `
			</h3>
			</div>
			<div  class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-wrapper">
			<div class="kt-portlet__head-actions">
			<button type="button" class="` + thisObject.idShowForm + ` btn btn-bold btn-label-brand btn-sm"  name="new_record" >
			<i class="la la-plus"></i>New Record</button>	
			
			</div>
			</div>
			</div>
			</div>
			<div class="kt-portlet__body" >
			<table class="table table-striped- table-bordered table-hover table-checkable" id="`+ thisObject.idTable + `">
			
		
			</table>
		
		</div>
		`;


		this.metaDataTable.ad_org_uu = localStorage.getItem('ad_org_uu');

			$('#' + thisObject.idFragmentContent).html(allHtml);


		var table = $('#' + thisObject.idTable);
		let columnDefDatatable = [
			{
				targets: -1,
				title: 'Actions',
				orderable: false,
				render: function (data, type, full, meta) {


					let buttonEdit = `<button id="bttn_row_edit` + full.c_invoice_wx_c_invoice_uu + `" name="` + full.c_invoice_wx_c_invoice_uu + `" type="button"  class="` + thisObject.idShowForm + ` btn btn-outline-hover-info btn-elevate btn-icon"><i class="la la-edit"></i></button>`;
					let buttonStatus = `<button id="` + thisObject.idButtonRowStatusDialog + full.c_invoice_wx_c_invoice_uu + `" value="` + full.c_invoice_wx_documentno + `" name="` + full.c_invoice_wx_c_invoice_uu + `" type="button" class="` + thisObject.classDialogStatus + ` btn btn-outline-hover-info btn-elevate btn-icon"> <i class="la la-flag-o"></i></button>`;
					let buttonPrint = `<button id="` + thisObject.idButtonPrintDialog + full.c_invoice_wx_c_invoice_uu + `" name="` + full.c_invoice_wx_c_invoice_uu + `" type="button" class="` + thisObject.classDialogPrint + ` btn btn-outline-hover-info btn-elevate btn-icon"> <i class="la la-print"></i></button>`;
					// jika 
					let htmlAction = '';
					if (full.c_invoice_wx_docstatus === 'CLOSED') {
						htmlAction = buttonPrint;
					}


					if (full.c_invoice_wx_docstatus === 'REVERSED') {
						htmlAction = '';
					}

					if (full.c_invoice_wx_docstatus === 'COMPLETED') {
						htmlAction = buttonStatus + buttonPrint;
					}
					if (full.c_invoice_wx_docstatus === 'DRAFT') {
						htmlAction = buttonEdit + buttonStatus + buttonPrint;
					}


					return htmlAction;
				},
			},
			{
				targets: 7,
				title: 'Grand total',
				orderable: false,
				render: function (data, type, full, meta) {


					return new Intl.NumberFormat().format(full.c_invoice_wx_grandtotal);
				},
			},
			{
				targets: 6,
				title: 'Total Tax',
				orderable: false,
				render: function (data, type, full, meta) {

					return new Intl.NumberFormat().format(full.c_invoice_wx_totaltaxlines);
				},
			},
			{
				targets: 5,
				title: 'Total Invoice line',
				orderable: false,
				render: function (data, type, full, meta) {
					return new Intl.NumberFormat().format(full.c_invoice_wx_totallines);
				},
			},
			{
				targets: -2,
				title: 'Tanggal',
				orderable: true,
				render: function (data, type, full, meta) {
					return HelperService.ConvertTimestampToLocateDateString(full.c_invoice_wx_dateacct);
				},
			},

		];

		thisObject.invoiceService.FindAll(thisObject.metaDataTable).then(function (invoiceCMTList) {
			// begin first table
			thisObject.datatableReference = table.DataTable({
				responsive: true,
				data: invoiceCMTList,
				scrollX: true,
				columns: thisObject.columnsDataTable,
				columnDefs: columnDefDatatable,
				"order": [[1, 'asc']],
				createdRow: function (row, data, index) {
					if (data.extn === '') {
						var td = $(row).find("td:first");
						td.removeClass('details-control');
					}
				},
				rowCallback: function (row, data, index) {
					//console.log('rowCallback');
				}

			});

			thisObject.EventHandler();


		});

	}

	EventHandler() {
		let thisObject = this;
		// Add event listener for opening and closing details
		$('#' + thisObject.idTable + ' tbody').off('click', 'td.details-control');
		$('#' + thisObject.idTable + ' tbody').on('click', 'td.details-control', function () {
			var tr = $(this).closest('tr');
			var row = thisObject.datatableReference.row(tr);
			let invoiceParent = row.data();
			if (row.child.isShown()) {
				// This row is already open - close it
				row.child.hide();
				$(this).html('<button  type="button" class="btn btn-outline-hover-info btn-icon"><i class="la la-plus-square-o"></i></button>');
			}
			else {
				let idInvoicedetailContent = thisObject.prefixContentChild + invoiceParent.c_invoice_wx_c_invoice_uu;
				let invoicelineFragment = `<div id="` + idInvoicedetailContent + `"></div>`;
				row.child(invoicelineFragment).show();

				// generate orderlinePage
				let invoicelinePage = new InvoicelinePage(invoiceParent, idInvoicedetailContent);
				invoicelinePage.Init();


				// add reference to listReferenceOrderlinePage, agar nanti bisa dipanggil lagi di orderpage, untuk membuat

				// change menjadi button minus
				$(this).html('<button  type="button" class="btn btn-outline-hover-info btn-icon"><i class="la la-minus-square-o"></i></button>');

			}

		});




		/*$('#' + thisObject.idButtonFormSubmit).on('click', function () {
			thisObject.SaveUpdate();

		});*/
		$('.' + thisObject.idShowForm).off("click");
		$('.' + thisObject.idShowForm).on("click", function (e) {

			let attrName = $(this).attr('name');
			let invoiceComponentModel = new InvoiceComponentModel();
			invoiceComponentModel.c_invoice.documentno = new Date().getTime();
			invoiceComponentModel.c_invoice.docstatus = 'DRAFT';
			if (attrName !== 'new_record') {
				let dataEdited = thisObject.datatableReference.row($('#bttn_row_edit' + attrName).parents('tr')).data();
				invoiceComponentModel = HelperService.ConvertRowDataToViewModel(dataEdited);
			}
			let invoiceFormPage = new InvoiceForm(invoiceComponentModel, thisObject.datatableReference, thisObject.invoiceType);

			

		});

		$('.' + thisObject.classDialogStatus).off('click');
		$('.' + thisObject.classDialogStatus).on('click', function (e) {
			let uuid = $(this).attr('name');
			let invoiceCMT = thisObject.datatableReference.row($('#' + thisObject.idButtonRowStatusDialog + uuid).parents('tr')).data();
			let processDocumentFuction = function (docstatus) {

				return thisObject.UpdateStatusDialogBox(docstatus, uuid);
			};
			let dialogboxstatusWidget = new DialogBoxStatusWidget('Set Status Invoice', invoiceCMT.c_invoice_wx_docstatus, processDocumentFuction);

		});
		

	 	/**
		* PRINT PREVIEW
		*/
		$('.' + thisObject.classDialogPrint).off('click');
		$('.' + thisObject.classDialogPrint).on('click', function (e) {

			let uuid = $(this).attr('name');
			let invoiceCMT = thisObject.datatableReference.row($('#' + thisObject.idButtonRowStatusDialog + uuid).parents('tr')).data();

			let printPreviewFunction = function (idContentPrint) {
				let printpreviewService = new PrintPreviewService();
				return printpreviewService.PrintPreviewInvoice(idContentPrint, invoiceCMT);
			};

			let dialogPrint = new DialogBoxPrintWidget('Cetak Invoice', printPreviewFunction);

		});
	
	
		thisObject.datatableReference.off('draw');
		thisObject.datatableReference.on('draw', function () {
			thisObject.EventHandler();
		});

	}

	async	UpdateStatusDialogBox(docStatus, uuid) {
		let thisObject = this;
		let insertSuccess = 0;

		try {

			let invoiceCMT = thisObject.datatableReference.row($('#' + thisObject.idButtonRowStatusDialog + uuid).parents('tr')).data();
			invoiceCMT.c_invoice_wx_docstatus = docStatus;
			insertSuccess += await thisObject.invoiceService.UpdateStatus(invoiceCMT);

			if (insertSuccess) {
				let rowDataList = await thisObject.invoiceService.FindInvoiceComponentModelByCInvoiceUu(invoiceCMT.c_invoice_wx_c_invoice_uu);

				let rowData = {};

				if (rowDataList.length > 0) {
					rowData = rowDataList[0];
					if (docStatus === 'DELETED') {

						thisObject.datatableReference
							.row($('#' + thisObject.idButtonRowStatusDialog + uuid).parents('tr'))
							.remove()
							.draw();
					} else {

						if (docStatus === 'COMPLETED') {
							// cek jika ternyata child orderlinepage terbuka, maka create lagi tablenya
							if ($('#' + thisObject.prefixContentChild +  rowData.c_invoice_wx_c_invoice_uu).length) {

								let invoicelinePage = new InvoicelinePage(rowData, thisObject.prefixContentChild +  rowData.c_invoice_wx_c_invoice_uu);
								invoicelinePage.Init();
							}
						}

						thisObject.datatableReference
							.row($('#' + thisObject.idButtonRowStatusDialog + uuid).parents('tr'))
							.data(rowData).draw();

					}
				}

			}
		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.UpdateStatusDialogBox.name, error);
			return insertSuccess;
		}

		return insertSuccess;

	}






}

