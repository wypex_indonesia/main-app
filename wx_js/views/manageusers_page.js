class ManageUsersPage {
    constructor(idFragmentContent, idFragmentSubHeader) {

        this.idFragmentContent = idFragmentContent;
        this.idFragmentSubHeader = idFragmentSubHeader;
        this.idButtonAddUser = 'button-add-user-form';
        this.userService = new UserService();
        this.classEditUserForm = 'wx_manage_user_edit';
        this.classLinkDetailUser = 'wx_detail_user_link';

        this.idUserSearch = 'wx_user_search';



        this.Init();

    }


    Init() {

        try {
            let thisObject = this;
            let metadatatable = new MetaDataTable();
            metadatatable.ad_org_uu = localStorage.getItem('ad_org_uu');
            thisObject.CreateListItemUser(metadatatable);
            thisObject.CreateSubheader();

        } catch (err) {
            apiInterface.Log(this.constructor.name, this.Init.name, err);
        }



    }

    AddUpdate(ad_user_uu, isNewRecord) {
        try {
            let thisObject = this;
            let userCMTList = thisObject.userService.FindUserCMTListByAdUserUu(ad_user_uu);
            let userCMT = userCMTList[0];


            if (!isNewRecord) {
                $('#' + ad_user_uu).remove();
            }

            thisObject.CreateItemUser(userCMT);

        } catch (err) {
            apiInterface.Log(this.constructor.name, this.AddUpdate.name, err);
        }

    }

    CreateListItemUser(metadatatable) {
        let thisObject = this;

        try {
            $('#' + thisObject.idFragmentContent).html('');
            thisObject.userService.FindAll(metadatatable).then(function (userCMTList) {
                _.forEach(userCMTList, function (userCMT) {

                    thisObject.CreateItemUser(userCMT);

                });

            });
        } catch (error) {
            apiInterface.Log(this.constructor.name, this.AddUpdate.name, error);
        }



    }




    CreateItemUser(userCMT) {
        let thisObject = this;
        let html = '';
        try {
            let initial = HelperService.GetInitialName(userCMT.ad_user_wx_name);

            html = `
    <div id="`+ userCMT.ad_user_wx_ad_user_uu + `" class="kt-portlet">
        <div class="kt-portlet__body">
            <div class="kt-widget kt-widget--user-profile-3">
                <div class="kt-widget__top">
                  
                    <div  class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden-">
                        `+ initial + `</div>
                    <div class="kt-widget__content">
                        <div class="kt-widget__head">
                            <span class="kt-widget__username">
                                `+ userCMT.ad_user_wx_name + `</span>
                            <div class="kt-widget__action">							
                                <button id="`+ userCMT.ad_user_wx_ad_user_uu + `" type="button" class="` + thisObject.classEditUserForm + ` btn btn-brand btn-sm btn-upper">edit</button>
                            </div>
                        </div>
                        <div class="kt-widget__subhead">
                            <a class="`+ thisObject.classLinkDetailUser + `" href="#"><i class="flaticon2-new-email"></i>` + userCMT.ad_user_wx_phone + `</a>						
                            <a class="`+ thisObject.classLinkDetailUser + `" href="#"><i class="flaticon2-calendar-3"></i>` + userCMT.ad_role_wx_name + ` </a>
                        </div>
                        <div class="kt-widget__info">
                            <div class="kt-widget__desc">							
                            </div>						
                        </div>
                    </div>
                </div>
              
            </div>
        </div>
    </div>`;
        } catch (err) {
            apiInterface.Log(this.constructor.name, this.CreateItemUser.name, err);

        }


        $('#' + this.idFragmentContent).prepend(html);

        thisObject.EventHandlerItemUser();

    }

    EventHandlerItemUser() {

        try {
            let thisObject = this;

            $('.' + thisObject.classEditUserForm).off('click');
            $('.' + thisObject.classEditUserForm).on('click', function () {
                let ad_user_uu = $(this).attr('id');
             thisObject.userService.FindUserCMTListByAdUserUu(ad_user_uu).then(function(userCMTList){
                    let userCMT = userCMTList[0];
                    let userCM = HelperService.ConvertRowDataToViewModel(userCMT);
                    let userForm = new UserForm(userCM, thisObject);
                });
                
              


            });


            $('.' + thisObject.classLinkDetailUser).off('click');
            $('.' + thisObject.classLinkDetailUser).on('click', function (e) {
                e.preventDefault();
            });
        } catch (err) {

            apiInterface.Log(this.constructor.name, this.EventHandler.name, err);
        }
    }

    CreateSubheader() {

        let thisObject = this;
        let html = `
    <div class="kt-subheader kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    Users
                </h3>
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                <div class="kt-subheader__group" id="kt_subheader_search">
                    <span class="kt-subheader__desc" id="kt_subheader_total">
                      </span>
                    <form class="kt-margin-l-20" id="kt_subheader_search_form">
                        <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                            <input type="text" class="form-control" placeholder="Search..." id="`+ thisObject.idUserSearch + `">
                            <span class="kt-input-icon__icon kt-input-icon__icon--right">
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                        width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect x="0" y="0" width="24" height="24"></rect>
                                            <path
                                                d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z"
                                                fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                            <path
                                                d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z"
                                                fill="#000000" fill-rule="nonzero"></path>
                                        </g>
                                    </svg>
    
                                    <!--<i class="flaticon2-search-1"></i>-->
                                </span>
                            </span>
                        </div>
                    </form>
                </div>               
            </div>
            <div class="kt-subheader__toolbar">
                <a href="#" class="">
                </a>
                <a href="#" id="`+ this.idButtonAddUser + `" type="button" class="btn btn-label-brand btn-bold">
                    Add User </a>
                
            </div>
        </div>
    </div>`;

        $('#' + this.idFragmentSubHeader).html(html);
        thisObject.EventHandlerSubHeader();

    }

    EventHandlerSubHeader() {

        try {
            let thisObject = this;



            $('#' + thisObject.idButtonAddUser).off('click');
            $('#' + thisObject.idButtonAddUser).on('click', function () {
                let userCM = new UserComponentModel();
                let userForm = new UserForm(userCM, thisObject);

            });

            $('#' + thisObject.idUserSearch).off('input');
            $('#' + thisObject.idUserSearch).on('input', function () {
                let value = $(this).val().trim();

                if (value.length > 3) {
                    let metadatatable = new MetaDataTable();
                    metadatatable.ad_org_uu = localStorage.getItem('ad_org_uu');
                    metadatatable.search = value;
                    metadatatable.searchField = 'ad_user.name';

                    thisObject.CreateListItemUser(metadatatable);

                }

            });

        } catch (err) {

            apiInterface.Log(this.constructor.name, this.EventHandler.name, err);
        }
    }




}