class PosTransactionTab {

	/**
	 * 	 
	 * @param {*} idFragmentContent  // IdControl untuk meletakkan View pos_transactionpage ex>. #fragmentContent
	 */
	constructor(idFragmentContent, defaultPosCMT) {


		this.titlePage = 'Open & Close POS Transaksi';


		this.metaDataTable = new MetaDataTable();

		this.idFragmentContent = idFragmentContent;
		this.defaultPosCMT = defaultPosCMT;

		this.idFragmentPosTransationTable = 'pos_transaction_tab_pos_transaction_table'; // button row status pada datatable pos_transaction page

		this.idFragmentPosTransactionForm = 'pos_transaction_tab_pos_transaction_form';

		this.Init();


	}


	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery

		thisObject.metaDataTable.ad_org_uu = localStorage.getItem('ad_org_uu');


		let layoutHtml = `
		<div class="row">
			<div class="col-xl-12">		
				<div id="`+ thisObject.idFragmentPosTransactionForm + `"></div>
				<div id="`+ thisObject.idFragmentPosTransationTable + `"></div>
			</div>		
		</div>`;


		$('#' + thisObject.idFragmentContent).html(layoutHtml);
		let posTransactionTablePortlet = new PosTransactionTablePortlet(thisObject.idFragmentPosTransationTable);
		let posTransactionForm = new PosTransactionForm(thisObject.idFragmentPosTransactionForm, posTransactionTablePortlet, thisObject.defaultPosCMT);
	}



}

