class DefaultPosConfigForm {

	/**
	 * 
	 * @param {string} idFragmentContent name id fragment content untuk meletakkan html content dari form_html
	 */
	constructor(idFragmentContent, posConfigCM) {
		this.validation = {

			m_warehouse_wx_name: {
				required: true
			},
			c_bankaccount_wx_name: {
				required: true
			},
			m_pricelist_version_wx_name: {
				required: true
			},
			c_tax_wx_c_tax_uu: {
				required: true
			},
			m_shipper_wx_m_shipper_uu: {
				required: true
			},



		};

		this.viewModel = posConfigCM;

		this.posService = new PosService();
		this.warehouseService = new WarehouseService();
		this.bankaccountService = new BankaccountService();
		this.pricelistversionService = new PricelistVersionService();

		this.idFragmentContent = idFragmentContent;
		this.selectedProduct = {}; // current product yang sedang dipilih, product_viewmodel_table
	
		this.idFormValidation = 'c_pos_form_validation_';
		this.classNameFormControl = 'c-pos-form-control';
		this.selectorClassNameFormControl = '.' + this.classNameFormControl;
		this.idWidgetSearch = 'default_post_config_page_fragment_search_content';

		this.idButtonFormSubmit = 'c_pos_form_submit_wx_';
		this.table_productprice_viewmodel = [];
		this.table_uomconversion_viewmodel = [];
		this.table_tax_viewmodel = [];
		this.table_shipper_viewmodel = [];


		this.Init();



	}





	async Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery
		let posService = new PosService();
		if (!thisObject.viewModel){
			let defaultPosCMTList = await posService.GetDefaultCPosCMT();
			thisObject.viewModel = new PosConfigComponentModel();
			if (defaultPosCMTList.length){
				let defaultPosCMT = defaultPosCMTList[0];
				thisObject.viewModel =  HelperService.ConvertRowDataToViewModel(defaultPosCMT, new PosConfigComponentModel());

			}
		}
		

		thisObject.GenerateFormInput();

	}




	/**
	 * 
	
	 */
	async SaveUpdate() {
		let thisObject = this;


		if (HelperService.CheckValidation(thisObject.validation)) {

			thisObject.viewModel = HelperService.GetViewModelFromForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.viewModel);

			const timeNow = new Date().getTime();
			// jika new record maka buat mlocator 	
			// harus dibuat default locator, karena locator akan dipergunakan di inventory movement
			let isNewRecord = false;
			if (!thisObject.viewModel.c_pos.c_pos_uu) { //new record
				isNewRecord = true;
				thisObject.viewModel.c_pos.c_pos_uu = HelperService.UUID();
				thisObject.viewModel.c_pos.ad_client_uu = localStorage.getItem('ad_client_uu');
				thisObject.viewModel.c_pos.ad_org_uu = localStorage.getItem('ad_org_uu');

				thisObject.viewModel.c_pos.createdby = localStorage.getItem('ad_user_uu');
				thisObject.viewModel.c_pos.updatedby = localStorage.getItem('ad_user_uu');

				thisObject.viewModel.c_pos.created = timeNow;
				thisObject.viewModel.c_pos.updated = timeNow;
			} else { //update record


				thisObject.viewModel.c_pos.updatedby = localStorage.getItem('ad_user_uu');

				thisObject.viewModel.c_pos.updated = timeNow;
			}

			thisObject.viewModel.c_pos.m_warehouse_uu = thisObject.viewModel.m_warehouse.m_warehouse_uu;
			thisObject.viewModel.c_pos.c_bankaccount_uu = thisObject.viewModel.c_bankaccount.c_bankaccount_uu;
			thisObject.viewModel.c_pos.m_pricelist_version_uu = thisObject.viewModel.m_pricelist_version.m_pricelist_version_uu;
			thisObject.viewModel.c_pos.c_tax_uu = thisObject.viewModel.c_tax.c_tax_uu;
			thisObject.viewModel.c_pos.m_shipper_uu = thisObject.viewModel.m_shipper.m_shipper_uu;
			thisObject.viewModel.c_pos.sync_client = null;
			thisObject.viewModel.c_pos.process_date = null;

			let insertSuccess = 0;
			const sqlInsert = HelperService.SqlInsertOrReplaceStatement('c_pos', thisObject.viewModel.c_pos);

			insertSuccess += await apiInterface.ExecuteSqlStatement(sqlInsert);

			// jika sukses tersimpan 
			if (insertSuccess > 0) {

				let posPage = new PosPage(thisObject.idFragmentContent);
			}


		}




	}

	GenerateFormInput() {
		let thisObject = this;

		try {
			let metaDataTableTax = new MetaDataTable();
			metaDataTableTax.ad_org_uu = localStorage.getItem('ad_org_uu');
			let taxService = new TaxService();
			let shipperService = new ShipperService();
			Promise.all([taxService.FindAll(metaDataTableTax), shipperService.FindAll(metaDataTableTax)]).then(function (arrayResult) {

				try {
					thisObject.table_tax_viewmodel = arrayResult[0];
					let htmlOptionsTax = '';
					_.forEach(thisObject.table_tax_viewmodel, function (taxObject, index) {
						let selected = '';
						if (index === 0) {
							selected = 'selected';
						}
						htmlOptionsTax += '<option value="' + taxObject.c_tax_wx_c_tax_uu + '"  ' + selected + '>' + taxObject.c_tax_wx_name + '</option>';
					});

					thisObject.table_shipper_viewmodel = arrayResult[1]; // shipperCMTList;

					let htmlOptionsShipper = '';
					_.forEach(thisObject.table_shipper_viewmodel, function (shipperObject, index) {
						let selected = '';
						if (index === 0) {
							selected = 'selected';
						}
						htmlOptionsShipper += '<option value="' + shipperObject.m_shipper_wx_m_shipper_uu + '"  ' + selected + '>' + shipperObject.m_shipper_wx_name + '</option>';
					});


					let htmlForm = `
					
				<div class="row">
				<div class="col-lg-12">
		
					<!--begin::Portlet-->
					<div class="kt-portlet">
						<div class="kt-portlet__head">
							<div class="kt-portlet__head-label">
								<h3 class="kt-portlet__head-title">
									Set Default Konfigurasi POS
								</h3>
							</div>
						</div>
		
						<!--begin::Form-->
						<form class="kt-form kt-form--label-right" id="`+ thisObject.idFormValidation + `" novalidate="novalidate">
							<div class="kt-portlet__body">
							<div class="form-group">
							<label>Nama</label>
							<input id="c_pos_wx_name" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
							<span class="form-text text-muted">Nama Pos sebagai pembeda dengan pos yang lain</span>
						</div>
						<div class="form-group form-group-marginless">
							<label>Lokasi POS</label>
							<div class="input-group">
								<input disabled="disabled" type="text" class="` + thisObject.classNameFormControl + ` form-control"  id="m_warehouse_wx_name" placeholder="Cari Gudang / Toko ...">
								<div class="input-group-append">
								<span id="search_m_warehouse" class="input-group-text"><i class="la la-search"></i></span>
								</div>
								<input type="hidden" class="` + thisObject.classNameFormControl + `" id="m_warehouse_wx_m_warehouse_uu">
							</div>
						</div>
						<div class="form-group form-group-marginless">
							<label>Akun Cash</label>
							<div class="input-group">
								<input disabled="disabled" type="text" class="` + thisObject.classNameFormControl + ` form-control"  id="c_bankaccount_wx_name" placeholder="Cari Akun Petty Cash ...">
								<div class="input-group-append">
								<span id="search_c_bankaccount" class="input-group-text"><i class="la la-search"></i></span>
								</div>
								<input type="hidden" class="` + thisObject.classNameFormControl + `" id="c_bankaccount_wx_c_bankaccount_uu">
							</div>
						</div>
						<div class="form-group form-group-marginless">
							<label>Versi Harga Jual</label>
							<div class="input-group">
								<input disabled="disabled" type="text" class="` + thisObject.classNameFormControl + ` form-control"  id="m_pricelist_version_wx_name" placeholder="Cari Pricelist version ...">
								<div class="input-group-append">
								<span id="search_m_pricelist_version" class="input-group-text"><i class="la la-search"></i></span>
								</div>
								<input type="hidden" class="` + thisObject.classNameFormControl + `" id="m_pricelist_version_wx_m_pricelist_version_uu">
							</div>
						</div>	
						<div class="form-group ">
						<label>Pajak Yang Dipergunakan</label>				
						<select class="` + thisObject.classNameFormControl + ` form-control" id="c_tax_wx_c_tax_uu">` +
						htmlOptionsTax +
						`</select>
						</div>
		
						<div class="form-group ">
						<label>Default Pickup</label>				
						<select class="` + thisObject.classNameFormControl + ` form-control" id="m_shipper_wx_m_shipper_uu">` +
						htmlOptionsShipper +
						`</select>
						</div>
					
						<div class="form-group" id="`+thisObject.idWidgetSearch+`">
								
		
						</div>
							</div>
							<div class="kt-portlet__foot">
								<div class="kt-form__actions">
									<div class="row">
										<div class="col-lg-9 ml-lg-auto">
										<button id="`+ thisObject.idButtonFormSubmit + `" type="submit" class="btn btn-primary">Save changes</button>
										
										</div>
									</div>
								</div>
							</div>
						</form>
		
						<!--end::Form-->
					</div>
		
					<!--end::Portlet-->
				</div>
				
			</div>`;

					$('#'+thisObject.idFragmentContent).html(htmlForm);

					HelperService.InjectViewModelToForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.viewModel);
					thisObject.PageEventHandler();

				} catch (error) {
					apiInterface.Log(thisObject.constructor.name, thisObject.GenerateFormInput.name, error);
				}



			});
		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.GenerateFormInput.name, error);
		}



	}

	PageEventHandler() {
		let thisObject = this;

		try {
			$('#' + thisObject.idFormValidation).off('submit');
			$('#' + thisObject.idFormValidation).on('submit', function (e) {
				thisObject.SaveUpdate();
				e.preventDefault();
			});


			/** BEGIN EVENT SEARCH M_Warehouse **************************************/
			$('#' + thisObject.idFormValidation + ' #search_m_warehouse').off('click');
			$('#' + thisObject.idFormValidation + ' #search_m_warehouse').on('click', function () {

				let functionGetResult = function(warehouseSelected){
					$('#' + thisObject.idFormValidation+ ' #m_warehouse_wx_name').val(warehouseSelected.m_warehouse_wx_name);
					$('#' + thisObject.idFormValidation+ ' #m_warehouse_wx_m_warehouse_uu').val(warehouseSelected.m_warehouse_wx_m_warehouse_uu);
				
				};
				let warehouse_search_widget = new WarehouseSearchWidget(thisObject.idWidgetSearch,functionGetResult );
				warehouse_search_widget.Init();
			});




			/** END EVENT SEARCH M_Warehouse **************************************/

			/** BEGIN EVENT SEARCH C_Bankaccount **************************************/
			$('#' + thisObject.idFormValidation + ' #search_c_bankaccount').off('click');
			$('#' + thisObject.idFormValidation + ' #search_c_bankaccount').on('click', function () {
				let bankaccount_search_widget = new BankaccountSearchWidget(thisObject.idWidgetSearch, ['c_bankaccount_wx_name','c_bankaccount_wx_c_bankaccount_uu'], 'petty_cash');
			bankaccount_search_widget.Init();
			});



			/** END EVENT SEARCH C_bankaccount **************************************/

			/** BEGIN EVENT SEARCH m_pricelist_version **************************************/
			$('#' + thisObject.idFormValidation + ' #search_m_pricelist_version').off('click');
			$('#' + thisObject.idFormValidation + ' #search_m_pricelist_version').on('click', function () {
				let pricelist_version_widget = new PricelistVersionSearchWidget(thisObject.idWidgetSearch,
					['m_pricelist_version_wx_name', 'm_pricelist_version_wx_m_pricelist_version_uu']);
			pricelist_version_widget.Init();
			});




		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.PageEventHandler.name, error);
		}



		/** END EVENT SEARCH m_pricelist_version **************************************/
	}


}
