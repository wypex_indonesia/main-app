class PricelistVersionForm {

	/**
	 * 
	 * @param {PricelistVersionViewModel} pricelistVersionCM
	 * @param {datatableReference} datatableReference data tabel pada role page, sebagai reference untuk mengupdate table tersebut	 
	 */
	constructor(pricelistVersionCM, datatableReference) {

		this.titleForm = 'Add/Edit Price List Version';

		this.viewModel = pricelistVersionCM;


		this.metaDataTable = new MetaDataTable();

		this.datatableReference = datatableReference; // buat reference datatable

		this.pricelistVersionService = new PricelistVersionService();


		this.idFormModal = 'm_pricelist_version_form_wx_';
		this.idFormValidation = 'm_pricelist_version_form_validation_';
		this.classNameFormControl = 'm_pricelist_version-form-control';
		this.selectorClassNameFormControl = '.' + this.classNameFormControl;


		this.idButtonFormSubmit = 'm_pricelist_version_form_submit_wx_';


		this.validation = {

			id_form: this.idFormValidation,

			m_pricelist_version_wx_name: {
				required: true
			},
			m_pricelist_wx_m_pricelist_uu: {
				required: true
			},


		};




		this.Init();




	}





	async Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery

		try {

			let htmlForm = await thisObject.GenerateFormInput();
			HelperService.InsertReplaceFormModal(thisObject.idFormModal, htmlForm);

			HelperService.InjectViewModelToForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.viewModel);
			thisObject.PageEventHandler();
		} catch (error) {
			apiInterface.Log(this.constructor.name, this.Init.name, error);
		}


	}




	/**
	 * 
	
	 */
	async Save() {
		let thisObject = this;

		try {

			let insertSuccess = 0;
			if (HelperService.CheckValidation(thisObject.validation)) {

				thisObject.viewModel = HelperService.GetViewModelFromForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.viewModel);

				insertSuccess = await thisObject.pricelistVersionService.SaveUpdate(thisObject.viewModel, thisObject.datatableReference);
				// jika sukses tersimpan 
				if (insertSuccess > 0) {
					$('#' + thisObject.idFormModal).modal('hide');
				}

			}
		} catch (err) {
			apiInterface.Log(this.constructor.name, this.Save.name, err);
		}



	}

	async GenerateFormInput() {
		let thisObject = this;

		let html = '';
		try {
			let pricelistOptions = new PricelistOptions(thisObject.classNameFormControl);
			let pricelistOptionsHtml = await pricelistOptions.Html();
			html = `
		<div class="modal fade" id="`+ thisObject.idFormModal + `" tabindex="-1" role="dialog" aria-labelledby="pricelistversion-form-labelled" style="display: none;" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="pricelistversion-form-labelled">Daftar Versi Pricelist</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						</button>
					</div>
					<form id="`+ thisObject.idFormValidation + `" class="kt-form">
						<div class="modal-body">
							<div class="form-group">
								<label>Nama Pricelist</label>
								<input id="m_pricelist_version_wx_name" type="text"
									class="`+ thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp"
									placeholder="Masukkan nama pricelist">
								<span class="form-text text-muted">Masukkan Nama Pricelist</span>
							</div>`+ pricelistOptionsHtml + `		
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button id="`+ thisObject.idButtonFormSubmit + `" type="submit" class="btn btn-primary">Save
								changes</button>
						</div>
					</form>
				</div>
			</div>
		</div>
`;

		} catch (error) {
			apiInterface.Log(this.constructor.name, this.GenerateFormInput.name, error);
		}

		return html;
	}


	PageEventHandler() {
		let thisObject = this;


		try {

			$('#' + thisObject.idFormValidation).off('submit');
			$('#' + thisObject.idFormValidation).on('submit', function (e) {
				thisObject.Save();
				e.preventDefault();


			});



			$('#' + thisObject.idFormModal).modal('show');


		} catch (err) {
			apiInterface.Log(this.constructor.name, this.PageEventHandler.name, err);
		}


	}



}
