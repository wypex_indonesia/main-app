class CheckoutForm {

	/**
	 * 

	
	 * @param {CartlineComponentModel} cartlineCM
	 * @param {DefaultPosCMT} defaultPosCMT
	
	 * 
	 */
	constructor(cartCM, defaultPosCMT, cartlinePortlet) {
		this.validation = {
			pos_c_order_wx_pos_payment_amount: {
				required: true
			},

		};


		this.cartlinePortlet = cartlinePortlet;
		this.cartCM = cartCM;

		this.defaultPosCMT = defaultPosCMT;


		this.posService = new PosService();

		this.selectedProduct = {}; // current product yang sedang dipilih, product_viewmodel_table
		this.idFormModal = 'c_orderline_form_wx_';
		this.idFormValidation = 'c_orderline_form_validation_';
		this.classNameFormControl = 'c-orderline-form-control';
		this.selectorClassNameFormControl = '.' + this.classNameFormControl;


		this.idButtonFormSubmit = 'c_orderline_form_submit_wx_';


		this.idPaymentAmount = 'pos_c_order_wx_pos_payment_amount';
		this.idChargeAmount = 'pos_c_order_wx_pos_charge_amount';
		this.idGrandTotal = 'pos_c_order_wx_grandtotal';
		this.idBankaccountUu = 'pos_c_order_wx_c_bankaccount_uu';
		this.classCashMoney = 'cash-money';
		this.classBankAccount = 'pos-bankaccount';
		this.grandtotalFormat = new Intl.NumberFormat().format(this.cartCM.pos_c_order.grandtotal);
		this.idTableTypePayment = 'checkout_form_table_type_payment';
		this.fragmentContentCash = 'checkout_form_fragment_cash';
		this.fragmentContentBank = 'checkout_form_fragment_bank';
		this.tableBankaccount = null;
		this.idTableGenerateMoney = 'checkout_form_table_generate_money';
		this.idTableBankaccount = 'checkout_form_table_bankaccount';

		this.idBttnBank = 'checkout-form-bttn-bank';
		this.idBttnCash = 'checkout-form-bttn-cash';
		this.classBttnTypePayment = 'checkout-form-bttn-type-payment';
		this.idOptionsBank = 'c_bankaccount_wx_c_bankaccount_uu';
		this.Init();
	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery
		thisObject.GenerateFormInput();


	}



	/**
	 * paymentAmount
	 * 
	 * 
	
	 */
	SaveUpdate() {
		let thisObject = this;
		let bpartnerService = new BusinessPartnerService();
		let insertSuccess = 0;
		if (HelperService.CheckValidation(thisObject.validation)) {



			let pos_c_order_wx_pos_payment_amount = numeral($('#' + thisObject.idFormValidation + ' #' + thisObject.idPaymentAmount).val()).value();
			let pos_c_order_wx_pos_charge_amount = numeral($('#' + thisObject.idFormValidation + ' #' + thisObject.idChargeAmount).val()).value();
			
			let c_bankaccount_uu = $('#' + thisObject.idFormValidation + ' #' + thisObject.idBankaccountUu).val();

			thisObject.cartCM.pos_c_order.c_bankaccount_uu = c_bankaccount_uu;
			thisObject.cartCM.pos_c_order.pos_payment_amount = pos_c_order_wx_pos_payment_amount;
			thisObject.cartCM.pos_c_order.pos_charge_amount = pos_c_order_wx_pos_charge_amount;

			thisObject.cartCM.c_bpartner_location.phone = HelperService.ConvertPhoneWithAreaCode(thisObject.cartCM.c_bpartner_location.phone, '62');
			// check phone

			thisObject.SetBusinessPartner().then(function () {

				thisObject.cartCM.pos_c_order.ad_client_uu = localStorage.getItem('ad_client_uu');
				thisObject.cartCM.pos_c_order.ad_org_uu = localStorage.getItem('ad_org_uu');
				thisObject.cartCM.pos_c_order.c_doctype_id = 2000; // penjualan langsung
				thisObject.cartCM.pos_c_order.m_warehouse_uu = thisObject.defaultPosCMT.c_pos_wx_m_warehouse_uu;
				thisObject.cartCM.pos_c_order.c_pos_uu = thisObject.defaultPosCMT.c_pos_wx_c_pos_uu;
				thisObject.cartCM.pos_c_order.m_shipper_uu = thisObject.defaultPosCMT.c_pos_wx_m_shipper_uu; // pickup shipper

				const sqlInsert = HelperService.SqlInsertOrReplaceStatement('pos_c_order', thisObject.cartCM.pos_c_order);

				apiInterface.ExecuteSqlStatement(sqlInsert).then(function (rowChanged) {
					insertSuccess += rowChanged;

					// jika sukses tersimpan 
					if (insertSuccess > 0) {
						$('#' + thisObject.idFormModal).modal('hide');
						thisObject.posService.RunPosPayment(thisObject.cartCM.pos_c_order.c_order_uu, thisObject.cartlinePortlet).then(function (inserted) {

						});
					}

				});

			});




		}
	}

	SetBusinessPartner() {
		let thisObject = this;
		let bpartnerService = new BusinessPartnerService();

		return new Promise(function (resolve, reject) {
			if (thisObject.cartCM.c_bpartner_location.phone) {

				bpartnerService.FindBusinessPartnerCMTListByPhone(thisObject.cartCM.c_bpartner_location.phone).then(function (bpartnerCMTList) {
					if (bpartnerCMTList.length) {
						let bpartnerCMT = bpartnerCMTList[0];
						thisObject.cartCM.pos_c_order.c_bpartner_uu = bpartnerCMT.c_bpartner_wx_c_bpartner_uu;
						thisObject.cartCM.pos_c_order.c_bpartner_location_uu = bpartnerCMT.c_bpartner_location_wx_c_bpartner_location_uu;
						resolve();
					} else {
						//jika belum teregister maka simpan businesspartner, dan business_partnerlocation
						let newBpartnerViewModel = new BusinesspartnerViewModel();
						newBpartnerViewModel.c_bpartner.name = thisObject.cartCM.c_bpartner.name;
						newBpartnerViewModel.c_bpartner.c_bpartner_uu = HelperService.UUID();
						newBpartnerViewModel.c_bpartner_location.phone = thisObject.cartCM.c_bpartner_location.phone;
						newBpartnerViewModel.c_bpartner_location_uu = HelperService.UUID();

						bpartnerService.SaveUpdate(newBpartnerViewModel).then(function (inserted) {
							thisObject.cartCM.pos_c_order.c_bpartner_uu = newBpartnerViewModel.c_bpartner.c_bpartner_uu;
							thisObject.cartCM.pos_c_order.c_bpartner_location_uu = newBpartnerViewModel.c_bpartner_location_uu;
							resolve();
						});


					}



				});
				// check jika phone sudah pernah teregister masukkan ke dalam cartCM


			} else {
				// jika phone tidak terisi, maka set default customer
				bpartnerService.GetDefaultCustomerPos().then(function (defaultCustomerBpartnerCM) {
					thisObject.cartCM.pos_c_order.c_bpartner_uu = defaultCustomerBpartnerCM.c_bpartner.c_bpartner_uu;
					thisObject.cartCM.pos_c_order.c_bpartner_location_uu = defaultCustomerBpartnerCM.c_bpartner_location.c_bpartner_location_uu;
					resolve();
				});

			}


		});

	}

	GenerateFormInput() {
		let thisObject = this;
		let allHtml =
			`
			<div class="modal fade" id="` + thisObject.idFormModal + `" tabindex="-1" role="dialog"
				aria-labelledby="product-form-labelled" style="display: none;" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="product-form-labelled">Checkout Form</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							</button>
						</div>
						<form id="`+ thisObject.idFormValidation + `" class="kt-form">
							<div class="modal-body">
						
						<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
							<li class="nav-item">
							  <a class="nav-link active" id="`+ thisObject.idBttnCash + `" data-toggle="pill" href="#` + thisObject.fragmentContentCash + `" role="tab" aria-controls="pills-home" aria-selected="true">CASH (ALT+C)</a>
							</li>
							<li class="nav-item">
							  <a class="nav-link" id="`+ thisObject.idBttnBank + `" data-toggle="pill" href="#` + thisObject.fragmentContentBank + `" role="tab" aria-controls="pills-profile" aria-selected="false">BANK (ALT+X)</a>
							</li>
						
						 </ul>
						  <div class="tab-content" id="pills-tabContent">
							<div class="tab-pane fade show active" id="`+ thisObject.fragmentContentCash + `" role="tabpanel" aria-labelledby="pills-home-tab">
							`+ thisObject.CreateTableGenerateMoney() + `
							</div>
							<div class="tab-pane fade" id="`+ thisObject.fragmentContentBank + `" role="tabpanel" aria-labelledby="pills-profile-tab">
							
							</div>
						
						  </div>
								<div class="form-group row">
									<label class="col-form-label col-lg-3 col-sm-12">
										<h3>TOTAL</h3>
									</label>
									<label class="col-form-label col-lg-9 col-sm-12">
										<h3 id="`+ thisObject.idGrandTotal + `"></h3>
									</label>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-lg-3 col-sm-12">
										<h3>BAYAR (ALT+Z)</h3>
									</label>
									<div class="col-lg-9 col-sm-12">
										<input id="`+ thisObject.idPaymentAmount + `" type="text" style="font-size:2rem"
											class="` + thisObject.classNameFormControl + ` wx-format-money c-orderline-form-control form-control form-control-lg"
											aria-describedby="emailHelp">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-lg-3 col-sm-12">
										<h3>SISA</h3>
									</label>
									<label class="col-form-label col-lg-9 col-sm-12">
										<h3 id="`+ thisObject.idChargeAmount + `"></h3>
									</label>
								</div>
								<input class="`+ thisObject.classNameFormControl + `" hidden id="`+thisObject.idBankaccountUu + `">
			
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								<button id="`+ thisObject.idButtonFormSubmit + `" type="submit" class="btn btn-primary">Save
									changes</button>
							</div>
						</form>
					</div>
				</div>
			</div>	
	`;

		HelperService.InsertReplaceFormModal(thisObject.idFormModal, allHtml);
		$('#' + thisObject.idFormValidation + ' #' + thisObject.idGrandTotal).html(thisObject.grandtotalFormat);
		$('#'+ thisObject.idFormValidation + ' #' + thisObject.idBankaccountUu).val(thisObject.defaultPosCMT.c_pos_wx_c_bankaccount_uu);
		thisObject.CreateTableBankaccount();
		thisObject.EventHandler();

	}


	CreateTableGenerateMoney() {
		let thisObject = this;

		let html = `
<div class="kt-grid-nav kt-grid-nav--skin-light">
    <div class="kt-grid-nav__row">
        <a href="#" class="`+ thisObject.classCashMoney + ` kt-grid-nav__item">
            <span name="500" class="kt-grid-nav__title">500<br>(ALT+1)</span>
        </a>
        <a href="#" class="`+ thisObject.classCashMoney + ` kt-grid-nav__item">
            <span name="1000" class="kt-grid-nav__title">1.000<br>(ALT+2)</span>
        </a>
        <a href="#" class="`+ thisObject.classCashMoney + ` kt-grid-nav__item">
            <span name="2000" class="kt-grid-nav__title">2.000<br>(ALT+3)</span>
        </a>
        <a href="#" name="5000" class="`+ thisObject.classCashMoney + ` kt-grid-nav__item">
            <span class="kt-grid-nav__title">5.000<br>(ALT+4)</span>
        </a>
    </div>
    <div class="kt-grid-nav__row">
        <a href="#" name="10000" class="`+ thisObject.classCashMoney + ` kt-grid-nav__item">
            <span class="kt-grid-nav__title">10.000<br>(ALT+5)</span>
        </a>
        <a href="#" name="20000" class="`+ thisObject.classCashMoney + ` kt-grid-nav__item">
            <span class="kt-grid-nav__title">20.000<br>(ALT+6)</span>
        </a>
        <a href="#" name="50000" class="`+ thisObject.classCashMoney + ` kt-grid-nav__item">
            <span class="kt-grid-nav__title">50.000<br>(ALT+7)</span>
        </a>
        <a href="#" name="100000" class="`+ thisObject.classCashMoney + ` kt-grid-nav__item">
            <span class="kt-grid-nav__title">100.000<br>(ALT+8)</span>
        </a>
    </div>
</div>
		`;

		return html;


	}



	CreateTableBankaccount() {
		let thisObject = this;

		let bankaccountOptions = new BankaccountOptions(thisObject.classNameFormControl, thisObject.idOptionsBank, 'C', 'Pilih Bank (ALT+B)');
		bankaccountOptions.Html().then(function (htmlBank) {

			$('#' + thisObject.fragmentContentBank).html(htmlBank);


			hotkeys.unbind('alt+b');
			hotkeys('alt+b', function () {
				$('#' + idOptionsBank).focus();

			});
			$('.' + thisObject.idOptionsBank).off('change');
			$('.' + thisObject.idOptionsBank).on('change', function () {
				let c_bankaccount_uu = $(this).val();
				$('#'+thisObject.idBankaccountUu).val(c_bankaccount_uu);
			});

			$('#' + thisObject.idBttnBank).off('shown.bs.tab');
			$('#' + thisObject.idBttnBank).on('shown.bs.tab', function () {

				thisObject.ShowTabBank();
			});


		});


	}


	EventHandler() {
		let thisObject = this;

		$('#' + thisObject.idPaymentAmount).off('input');
		$('#' + thisObject.idPaymentAmount).on('input', function () {
		
			thisObject.CalculatePayment();
		});

		$('.' + thisObject.classCashMoney).off('click');
		$('.' + thisObject.classCashMoney).on('click', function (e) {
			e.preventDefault();
		
			let cashAmount = numeral($(this).attr('name')).value();
			thisObject.AddMoneyCash(cashAmount);
		});

		
		$('#' + thisObject.idFormValidation).off('submit');
		$('#' + thisObject.idFormValidation).on('submit', function (e) {
			if (thisObject.CalculatePayment() >= 0) {
				thisObject.SaveUpdate();
			}
			e.preventDefault();
		});


		$('#' + thisObject.idFormModal).off('shown.bs.modal');
		$('#' + thisObject.idFormModal).on('shown.bs.modal', function () {

			$('#' + thisObject.idPaymentAmount).focus();
		
		});



		$('#' + thisObject.idFormModal).modal('show');
		$('.wx-format-money').mask('#,##0', { reverse: true });

		this.HotkeysEvent();

	}

	AddMoneyCash(cashAmount) {
		let thisObject = this;
		let paymentAmount = numeral($('#' + thisObject.idPaymentAmount).val()).value();

		let incrementPaymentAmount = cashAmount + paymentAmount;
		$('#' + thisObject.idPaymentAmount).val(incrementPaymentAmount);
		thisObject.CalculatePayment();

	}

	CalculatePayment() {
		let thisObject = this;

		let paymentAmount = numeral($('#' + thisObject.idPaymentAmount).val()).value();

		let chargeAmount = paymentAmount - thisObject.cartCM.pos_c_order.grandtotal;
		let stringChargeAmount = new Intl.NumberFormat().format(chargeAmount);

		$('#' + thisObject.idChargeAmount).html(stringChargeAmount);

		thisObject.cartCM.pos_c_order.pos_payment_amount = paymentAmount;
		thisObject.cartCM.pos_c_order.pos_charge_amount = chargeAmount;
		return chargeAmount;

	}

	ShowTabBank(){
		let thisObject = this;
		
		$('#' + thisObject.idPaymentAmount).val(thisObject.cartCM.pos_c_order.grandtotal);
		$('#' + thisObject.idPaymentAmount).prop('disabled', true);
		let c_bankaccount_uu = $('#'+thisObject.idOptionsBank).val();
		$('#'+thisObject.idBankaccountUu).val(c_bankaccount_uu);
		thisObject.CalculatePayment();
		$('#' + thisObject.idOptionsBank).focus();
	}
	ShowTabCash(){
		let thisObject = this;
		$('#'+thisObject.idBankaccountUu).val(thisObject.defaultPosCMT.c_pos_wx_c_bankaccount_uu);
		$('#' + thisObject.idPaymentAmount).prop('disabled', false);
		$('#' + thisObject.idPaymentAmount).focus();
		
	}
	

	/*ActiveTypePayment(selectorElementJquery) {
		let thisObject = this;

		try {
			// remove class bttn-primary btn-secondary btn-hover-brand active type payment
			$('.' + thisObject.classBttnTypePayment).each(function (index) {
				$(this).removeClass('btn-primary btn-secondary btn-hover-brand');
				if ($(this).attr('id') === selectorElementJquery.attr('id')) {
					$(this).addClass('btn-primary');
				} else {
					$(this).addClass('btn-secondary btn-hover-brand');

				}

			});

			if (selectorElementJquery.attr('id') === thisObject.idBttnBank) {
				thisObject.CreateTableBankaccount();
			}

			if (selectorElementJquery.attr('id') === thisObject.idBttnCash) {
				$('#pos_c_order_wx_c_bankaccount_uu').val(thisObject.defaultPosCMT.c_pos_wx_c_bankaccount_uu);
				thisObject.CreateTableGenerateMoney();
			}

		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.ActiveTypePayment.name, error);
		}

	}*/


	HotkeysEvent() {
		let thisObject = this;
		hotkeys('alt+1,alt+2,alt+3,alt+4,alt+5,alt+6,alt+7,alt+8,alt+c,alt+x,alt+z', function (event, handler) {
			// Prevent the default refresh event under WINDOWS system
			switch (handler.key) {
				case 'alt+1':
					thisObject.AddMoneyCash(500);
					event.preventDefault();
					return false;
					break;
				case 'alt+2':
					thisObject.AddMoneyCash(1000);
					event.preventDefault();
					return false;
					break;
				case 'alt+3':
					thisObject.AddMoneyCash(2000);
					event.preventDefault();
					return false;
					break;
				case 'alt+4':
					thisObject.AddMoneyCash(5000);
					event.preventDefault();
					return false;
					break;
				case 'alt+5':
					thisObject.AddMoneyCash(10000);
					event.preventDefault();
					return false;
					break;
				case 'alt+6':
					thisObject.AddMoneyCash(20000);
					event.preventDefault();
					return false;
					break;
				case 'alt+7':
					thisObject.AddMoneyCash(50000);
					event.preventDefault();
					return false;
					break;
				case 'alt+8':
					thisObject.AddMoneyCash(100000);
					event.preventDefault();
					return false;
					break;
				case 'alt+c':
					$('#' + thisObject.idBttnCash).tab('show') // Select tab by name
					//thisObject.ActiveTypePayment($('#' + thisObject.idBttnCash));
					event.preventDefault();
					return false;
					break;
				case 'alt+x':
					$('#' + thisObject.idBttnBank).tab('show');
					//	thisObject.ActiveTypePayment($('#'+thisObject.idBttnBank));
					event.preventDefault();
					return false;
					break;
				case 'alt+z':
					$('#' + thisObject.idPaymentAmount).focus();
					event.preventDefault();
					return false;
					break;
			}

		});
		hotkeys.filter = function (event) {
			return true;
		}

	}





}
