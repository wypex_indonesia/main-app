class FactAcctManualJournalPortlet {

	constructor(idFragmentContent, factacctTablePortlet) {

		this.factAcctService = new FactAcctService();
		this.elementValueService = new ElementValueService();
		this.idFragmentContent = idFragmentContent;
		this.factacctTablePortlet = factacctTablePortlet;

		this.labelForm = 'Manual Journal';


		this.idFormModal = 'manual_journal_form_wx_';
		this.idFormValidation = 'manual_journal_form_validation_';
		this.classNameFormControl = 'manual_journal_form-control';

		this.selectorClassNameFormControl = '.' + this.classNameFormControl;
		this.idButtonFormSubmit = 'manual_journal_form_submit_wx_';

		this.idAmountDebetKredit = 'manual_journal_amount_debet_kredit';
		this.idDebetKreditButton = 'manual_journal_debet_credit_button';
		this.classDebetKreditOption = 'manual_journal_debet_credit_options';
		this.idElementValue = 'element_value_option';
		this.validation = {
			id_form: this.idFormValidation,

		};
		this.validation[this.idElementValue] = { required: true };
		this.validation[this.idAmountDebetKredit] = { required: true };


		this.Init();

	}


	async	Init() {

		try {

			let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery

			let htmlForm = await thisObject.GeneratePortlet();

			$('#' + thisObject.idFragmentContent).html(htmlForm);


			thisObject.EventHandler();

			let elementValueCMTList = await thisObject.elementValueService.FindAll();

			_.forEach(elementValueCMTList, function (elementValueCMT) {
				let text = elementValueCMT.c_elementvalue_wx_value + " - " + elementValueCMT.c_elementvalue_wx_name;
				let id = elementValueCMT.c_elementvalue_wx_value + "_" + elementValueCMT.c_elementvalue_wx_account_type;
				let newOption = new Option(text, id, false, false);
				// Append it to the select
				$('#' + thisObject.idElementValue).append(newOption).trigger('change');

			});


		} catch (error) {
			apiInterface.Log(this.constructor.name, this.Init.name, error);
		}


	}



	async	GeneratePortlet() {
		let thisObject = this;




		let portletHtml = `
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand flaticon2-line-chart"></i>
				</span>
				<h3 class="kt-portlet__head-title">
					`+ thisObject.labelForm + `
				</h3>
			</div>
			<div class="kt-portlet__head-toolbar">
				<div class="kt-portlet__head-wrapper">
					<div class="kt-portlet__head-actions">
					</div>
				</div>
			</div>
		</div>
		<form id="`+ thisObject.idFormValidation + `" class="kt-form">
			<div class="kt-portlet__body">
				<div class="form-group row">
					<label class="col-form-label col-lg-3 col-sm-12">Pilih Element</label>
					<div class=" col-lg-4 col-md-9 col-sm-12">
						<select class="form-control kt-select2" id="`+ thisObject.idElementValue + `" name="param">
							
						</select>
					</div>
				</div>	
				<div class="form-group ">
			
					<div class="input-group">
						<div class="input-group-prepend">
							<button type="button" name="Debet" id="`+ thisObject.idDebetKreditButton + `" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Debet
							</button>
							<div class="dropdown-menu">
								<a class="`+ thisObject.classDebetKreditOption + ` dropdown-item" name="Debet" dropdown-item" href="#">Debet</a>
								<a class="`+ thisObject.classDebetKreditOption + ` dropdown-item" name="Kredit" dropdown-item" href="#">Kredit</a>
							</div>
						</div>
						<input id="`+ thisObject.idAmountDebetKredit + `" type="text"
						class="` + thisObject.classNameFormControl + ` wx-format-money  form-control"
						aria-describedby="emailHelp">					
					</div>
					
				</div>	
			</div>
			<div class="kt-portlet__foot">
				<div class="kt-form__actions">
					<button id="`+ thisObject.idButtonFormSubmit + `" type="submit" class="btn btn-primary">Submit</button>
					<div id="`+ thisObject.idNotification + `"></div>
				</div>
			</div>
		</form>
	</div>
		`;
		return portletHtml;

	}


	EventHandler() {
		let thisObject = this;

		try {
			$('#' + thisObject.idFormValidation).off('submit');
			$('#' + thisObject.idFormValidation).on('submit', function (e) {
				e.preventDefault();
				thisObject.Save();

			});
			$('#' + thisObject.idRefreshButton).off('click');
			$('#' + thisObject.idRefreshButton).on('click', function (e) {

				thisObject.Init();

			});

			$('#' + thisObject.idFormValidation + ' .' + thisObject.classDebetKreditOption).off('click');
			$('#' + thisObject.idFormValidation + ' .' + thisObject.classDebetKreditOption).on('click', function (e) {
				e.preventDefault();

				let debetOrKredit = $(this).attr('name');

				$('#' + thisObject.idDebetKreditButton).attr('name', debetOrKredit);
				$('#' + thisObject.idDebetKreditButton).text(debetOrKredit);

			});

			let matchCustom = function (params, data) {
				return thisObject.MatchCustom(params, data);

			};

			$('#' + thisObject.idElementValue).select2({
				matcher: thisObject.MatchCustom
			});


			$('.wx-format-money').mask('#,##0', { reverse: true });

		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.EventHandler.name, error);
		}

	}

	MatchCustom(params, data) {
		// If there are no search terms, return all of the data
		if ($.trim(params.term) === '') {
			return data;
		}

		// Do not display the item if there is no 'text' property
		if (typeof data.text === 'undefined') {
			return null;
		}

		// `params.term` should be the term that is used for searching
		// `data.text` is the text that is displayed for the data object
		if (data.text.indexOf(params.term) > -1) {
			var modifiedData = $.extend({}, data, true);
			modifiedData.text += ' (matched)';

			// You can return modified objects from here
			// This includes matching the `children` how you want in nested data sets
			return modifiedData;
		}

		// Return `null` if the term should not be displayed
		return null;
	}




	/**
		 *
		 * c_doctype_id =10002 --> transfer keluar
		 * c_doctype_id = 10001 --> transfer in
	
		 */
	async Save() {
		let thisObject = this;

		try {
			let factAcctService = new FactAcctService();

			let insertSuccess = 0;
			if (HelperService.CheckValidation(thisObject.validation)) {

				let value_accountType = $('#' + thisObject.idElementValue).select2('data');
				let valueArrayAccountType = value_accountType[0].id.split("_");
				let elementvalue = { value: valueArrayAccountType[0], accountType: valueArrayAccountType[1] };
				let db_cr = $('#' + thisObject.idDebetKreditButton).attr('name');
				let amount_db = null;
				let amount_cr = null;
				if (db_cr === 'Debet') {
					amount_db = numeral($('#' + thisObject.idAmountDebetKredit).val()).value();
				} else {
					amount_cr = numeral($('#' + thisObject.idAmountDebetKredit).val()).value();
				}

				let fact_acct_uu = HelperService.UUID();
				insertSuccess = await factAcctService.sqlInsertFactAcct(elementvalue, '', '', amount_db, amount_cr, new Date().getTime(), fact_acct_uu);

				if (insertSuccess > 0) {
					let success = `
					<div class="alert alert-success" role="alert">
						<strong>Manual jurnal tersimpan!</strong>
					</div>`;
					let bttnRefresh = `
					<button id="`+ thisObject.idRefreshButton + `" type="button" class="btn btn-primary">Refresh Form</button>
			
					`;
					$('#' + thisObject.idNotification).html(success);
					$('#' + thisObject.idButtonFormSubmit).replaceWith(bttnRefresh);
					thisObject.EventHandler();

					let fact_acct = await factAcctService.FindFactAcctUu(fact_acct_uu);
					thisObject.factacctTablePortlet.UpdateRow(fact_acct);

				}
			}

		} catch (err) {
			apiInterface.Log(this.constructor.name, this.Save.name, err);
		}
	}



}
