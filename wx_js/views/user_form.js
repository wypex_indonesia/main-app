class UserForm {

	/**
	 *	
	 * @param {UserComponentModel}
	 * @param {datatableReference} datatableReference data tabel pada inout page, sebagai reference untuk mengupdate table tersebut
	 
	 */
	constructor(userCM, userPage) {

		this.userPage = userPage;

		this.titleForm = 'Add/Edit User';

		this.userCM = userCM;
		this.isNewRecord = true;
		this.tempPassword = ''; //temporary password, utk penyimpanan sementara, ketika edit user, karena tidak ikut diedit

		this.userService = new UserService();
		this.roleService = new RoleService();
		this.idFragmentContent = 'wx_fragment_ad_user_form';
		this.idFormModal = 'ad_user_form_wx_';
		this.idFormValidation = 'ad_user_form_validation_';
		this.classNameFormControl = 'ad-user-form-control';
		this.selectorClassNameFormControl = '.' + this.classNameFormControl;

		this.idNewRecord = 'new_record_ad_user_wx_';
		this.idButtonFormSubmit = 'ad_user_form_submit_wx_';

		this.validation = {

			id_form: this.idFormValidation,

			ad_user_wx_name: {
				required: true
			},
			ad_user_wx_phone: {
				required: true
			},
			ad_role_wx_ad_role_uu: {
				required: true
			},
		};

		this.Init();

	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery


		if (this.userCM.ad_user.ad_user_uu) {
			this.isNewRecord = false;
			this.tempPassword = this.userCM.ad_user.password;
			this.userCM.ad_user.password = '';
		} else {
			this.isNewRecord = true;
			this.validation['ad_user_wx_password'] = { required: true };

		}

		thisObject.GenerateFormInput();


	}




	/**
	 * 
	
	 */
	Save() {
		let thisObject = this;

		let insertSuccess = 0;
		if (HelperService.CheckValidation(thisObject.validation)) {

			thisObject.userCM = HelperService.GetViewModelFromForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.userCM);

			if (thisObject.userCM.ad_user.password) {
				thisObject.userCM.ad_user.password = CryptoJS.MD5(thisObject.userCM.ad_user.password);
			} else {
				thisObject.userCM.ad_user.password = thisObject.tempPassword;
			}

			// dicreate disini uuid karena akan dipergunakan di userpage
			if (thisObject.isNewRecord) {
				thisObject.userCM.ad_user.ad_user_uu = HelperService.UUID();
				thisObject.userCM.ad_user.ad_org_uu = localStorage.getItem('ad_org_uu');
				thisObject.userCM.ad_user.ad_client_uu = localStorage.getItem('ad_client_uu');
				thisObject.userCM.ad_user.createdby = localStorage.getItem('ad_user_uu');
				thisObject.userCM.ad_user.created = new Date().getTime();
			}

			insertSuccess = thisObject.userService.SaveUpdate(thisObject.userCM);
			// jika sukses tersimpan 
			if (insertSuccess > 0) {
				// update userPage
				thisObject.userPage.AddUpdate(thisObject.userCM.ad_user.ad_user_uu);

				$('#' + thisObject.idFormModal).modal('hide');


			}

		}


	}

	GenerateFormInput() {
		let thisObject = this;
		let metaDataResponseTableRole = new MetaDataTable();
		metaDataResponseTableRole.ad_org_uu = localStorage.getItem('ad_org_uu');


		Promise.all([thisObject.roleService.FindAll(metaDataResponseTableRole)]).then(function (resultArray) {
			let optionsRole = resultArray[0];
			let htmlOptionsRole = '';
			_.forEach(optionsRole, function (role) {
				htmlOptionsRole += '<option value="' + role.ad_role_wx_ad_role_uu + '">' + role.ad_role_wx_name + '</option>';
			});
			let html = `<div class="modal fade" id="` + thisObject.idFormModal + `" tabindex="-1" role="dialog" aria-labelledby="product-form-labelled"
			style="display: none;" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="product-form-labelled">`+ thisObject.titleForm + `</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						</button>
					</div>
					<div class="modal-body">
						<form id="`+ thisObject.idFormValidation + `" class="kt-form">
						<div class="form-group">
							<label>Nama Lengkap</label>
							<input  id="ad_user_wx_name" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >						
						</div>
						<div class="form-group">
							<label>Handphone</label>
							<input  id="ad_user_wx_phone" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >						
						</div>
						<div class="form-group">
							<label>Password</label>
							<input  id="ad_user_wx_password" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
							<span class="form-text text-muted">Password</span>
						</div>
						<div class="form-group">
						<label for="exampleSelect1">Hak Akses</label>
						<select class="` + thisObject.classNameFormControl + `   form-control" id="ad_role_wx_ad_role_uu">` +
				htmlOptionsRole +
				`</select>
					</div>
	
						
						</form>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button id="`+ thisObject.idButtonFormSubmit + `" type="button" class="btn btn-primary">Save changes</button>
						</div>
					</div>
				</div>
			</div>
		</div>`;

		HelperService.InsertReplaceFormModal(thisObject.idFormModal, html);

		// jika bukan new record, ambil data-data dibawah untuk select options

		HelperService.InjectViewModelToForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.userCM);
		thisObject.PageEventHandler();
		});




	}


	PageEventHandler() {
		let thisObject = this;

		$('#' + thisObject.idButtonFormSubmit).off('click');
		$('#' + thisObject.idButtonFormSubmit).on('click', function () {
			thisObject.Save();

		});

		$('#' + thisObject.idFormModal).modal('show');


	}


}
