class PaymentPage {

	/**
	 * 
	 * @param {*} paymentType  // 'APP' (AP Payment)  ARR (AR Receipt)
	 * @param {*} idFragmentContent  // IdControl untuk meletakkan View Payment ex>. #fragmentContent
	 */
	constructor(paymentType, idFragmentContent) {

		this.titlePage = '';

		this.paymentType = paymentType;
		this.componentModel = new PaymentComponentModel();
		this.labelForm = '';
		this.labelNoDocument = '';
		this.labelVendorOrCustomer = '';
		this.labelNoOrder = '';
		this.labelNoInout = '';

		this.idTable = 'wx_payment_table_';
		this.idFragmentContent = idFragmentContent;
		this.paymentService = new PaymentService();
		this.metaDataTable = new MetaDataTable();
		this.datatableReference = {}; // buat reference datatable
		this.idShowForm = 'wx-btn-show-form-payment';
		this.idFormFragmentContent = "wx_payment_form_content";
		this.idButtonRowStatusDialog = "bttn_row_status";
		this.idButtonPrintDialog = "bttn_print_dialog";
		this.idDialogBox = 'status-dialog-box';
		this.idDialogPrint = 'print-dialog-box';
		this.idDialogForm = 'c_payment_form_wx_';
		this.labelInvoiceReference = 'No Invoice Vendor';// No PO Vendor
		this.labelNoDocument = 'No Dokumen';
		this.labelAkunBank = 'Akun Penerimaan';
		this.classDialogStatus = 'c_order_page_bttn_show_dialog_status';
		if (paymentType === 'APP') {

			this.labelAkunBank = 'Akun Pembayaran';
			this.titlePage = 'Pembayaran Vendor';

			this.labelVendorOrCustomer = 'Vendor';
			this.labelNoOrder = 'No Purchase Order';
			this.labelNoInout = 'No Material Receipt';
			this.metaDataTable.otherFilters.push(' AND c_doctype.docbasetype = \'APP\'');
		}

		if (paymentType === 'ARR') {

			//	this.c_order_wx_c_doctype_id = 2001; // SO
			this.titlePage = 'Terima Pembayaran Customer';
			this.labelAkunBank = 'Akun Penerimaan';
			this.labelVendorOrCustomer = 'Customer';
			this.labelNoOrder = 'No Sales Order';
			this.labelNoInout = 'No Surat Jalan';
			this.metaDataTable.otherFilters.push(' AND c_doctype.docbasetype = \'ARR\'');

		}

		this.columnsDataTable =
			[

				{ title: this.labelNoDocument, data: 'c_payment_wx_documentno' },
				{ title: this.labelNoOrder, data: 'c_order_wx_documentno' },
				{ title: 'No Invoice', data: 'c_invoice_wx_documentno' },
				{ title: this.labelInvoiceReference, data: 'c_invoice_wx_invoicereference' },
				{ title: this.labelVendorOrCustomer, data: 'c_bpartner_wx_name' },
				{ title: this.labelAkunBank, data: 'c_bankaccount_wx_name' },
				{ title: 'Grand Total' },				
				{ title: "Status", data: 'c_payment_wx_docstatus' },
				{ title: "Tanggal", data: 'c_payment_wx_dateacct' },
				{ title: 'Action' }
			];

		// jika ARR maka delete invoiceReference karena isinya adalah no invoice dari vendor
		if (paymentType === 'ARR') {

			// buang column labelInvoiceReference, pada index 3
			this.columnsDataTable.splice(3, 1);
		}
		this.Init();

	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery


		let allHtml = `
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head kt-portlet__head--lg">
			<div  class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
			<i class="kt-font-brand flaticon2-line-chart"></i>
			</span>
			<h3 class="kt-portlet__head-title">
		 	`+ thisObject.titlePage + `
			</h3>
			</div>
			<div  class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-wrapper">
			<div class="kt-portlet__head-actions">
			<button type="button" class="` + thisObject.idShowForm + ` btn btn-bold btn-label-brand btn-sm"  name="new_record" >
			<i class="la la-plus"></i>New Record</button>	
			
			</div>
			</div>
			</div>
			</div>
			<div class="kt-portlet__body" >
			<table class="table table-striped- table-bordered table-hover table-checkable" id="`+ thisObject.idTable + `">
			
			</thead>
			</table>
			<div id="`+ thisObject.idFormFragmentContent + `"></div>
			</div>
		</div>
		`;


		this.metaDataTable.ad_org_uu = localStorage.getItem('ad_org_uu');

		$('#' + thisObject.idFragmentContent).html(allHtml);


		var table = $('#' + thisObject.idTable);
		let columnDefDatatable = [
			{
				targets: -1,
				title: 'Actions',
				orderable: false,
				render: function (data, type, full, meta) {


					let buttonEdit = `<button id="bttn_row_edit` + full.c_payment_wx_c_payment_uu + `" name="` + full.c_payment_wx_c_payment_uu + `" type="button"  class="` + thisObject.idShowForm + ` btn btn-outline-hover-info btn-elevate btn-icon"><i class="la la-edit"></i></button>`;
					let buttonStatus = `<button id="` + thisObject.idButtonRowStatusDialog + full.c_payment_wx_c_payment_uu + `" value="` + full.c_payment_wx_documentno + `" name="` + full.c_payment_wx_c_payment_uu + `" type="button" class="` + thisObject.classDialogStatus + ` btn btn-outline-hover-info btn-elevate btn-icon"> <i class="la la-flag-o"></i></button>`;
					// jika 
					let htmlAction = '';
					if (full.c_payment_wx_docstatus === 'CLOSED') {
						htmlAction = '';
					}


					if (full.c_payment_wx_docstatus === 'REVERSED') {
						htmlAction = '';
					}

					if (full.c_payment_wx_docstatus === 'COMPLETED') {
						htmlAction = buttonStatus;
					}
					if (full.c_payment_wx_docstatus === 'DRAFT') {
						htmlAction = buttonEdit + buttonStatus;
					}


					return htmlAction;
				},
			},
			{
				targets: -2,
				title: 'Tanggal',
				orderable: false,
				render: function (data, type, full, meta) {
					return HelperService.ConvertTimestampToLocateDateString(full.c_payment_wx_dateacct);
				},
			},
			{
				targets: -4,
				title: 'Jumlah',
				orderable: false,
				render: function (data, type, full, meta) {


					return new Intl.NumberFormat().format(full.c_payment_wx_payamt);
				},
			},
			{
				targets: -5,
				title: thisObject.labelAkunBank,
				orderable: false,
				render: function (data, type, full, meta) {


					return full.c_bankaccount_wx_name + ' - ' + full.c_bankaccount_wx_accountno;
				},
			},

		];


		thisObject.paymentService.FindAll(thisObject.metaDataTable).then(function (paymentCMTList) {
			// begin first table
			thisObject.datatableReference = table.DataTable({
				responsive: true,
				data: paymentCMTList,
				scrollX: true,
				columns: thisObject.columnsDataTable,
				columnDefs: columnDefDatatable,
				"order": [[1, 'asc']],
				rowCallback: function (row, data, index) {
					//console.log('rowCallback');
				}

			});

			// attach event handler on the page

			thisObject.EventHandler();

		});

	}

	EventHandler() {
		let thisObject = this;

		$('.' + thisObject.idShowForm).off("click"); //diremove sebelumnya kalau, ada , karena bisa double
		$('.' + thisObject.idShowForm).on("click", function (e) {

			let attrName = $(this).attr('name');
			let paymentComponentModel = new PaymentComponentModel();
			paymentComponentModel.c_payment.documentno = new Date().getTime();
			paymentComponentModel.c_payment.docstatus = 'DRAFT';
			if (attrName !== 'new_record') {
				let dataEdited = thisObject.datatableReference.row($('#bttn_row_edit' + attrName).parents('tr')).data();
				paymentComponentModel = HelperService.ConvertRowDataToViewModel(dataEdited);
			}
			let paymentFormPage = new PaymentForm(paymentComponentModel, thisObject.datatableReference, thisObject.paymentType);

			//$('#' + thisObject.idFormModal).modal('show');

		});

		$('.' + thisObject.classDialogStatus).off('click');
		$('.' + thisObject.classDialogStatus).on('click', function (e) {
			let uuid = $(this).attr('name');
			let paymentCMT = thisObject.datatableReference.row($('#' + thisObject.idButtonRowStatusDialog + uuid).parents('tr')).data();

			let processDocumentFuction = function (docstatus) {

				return thisObject.UpdateStatusDialogBox(docstatus, uuid);
			};

			let dialogboxstatusWidget = new DialogBoxStatusWidget('Set Status Payment', paymentCMT.c_payment_wx_docstatus, processDocumentFuction);


		});
	
		thisObject.datatableReference.off('draw');
		thisObject.datatableReference.on('draw', function () {
			thisObject.EventHandler();
		});



	}

	

	async	UpdateStatusDialogBox(docStatus, uuid) {
		let thisObject = this;
		let insertSuccess = 0;

		try {

			let paymentCMT = thisObject.datatableReference.row($('#' + thisObject.idButtonRowStatusDialog + uuid).parents('tr')).data();
			paymentCMT.c_payment_wx_docstatus = docStatus;
			insertSuccess += await thisObject.paymentService.UpdateStatus(paymentCMT);

			if (insertSuccess) {
				let rowDataList = await thisObject.paymentService.FindPaymentComponentModelByCPaymentUu(paymentCMT.c_payment_wx_c_payment_uu);

				let rowData = {};

				if (rowDataList.length > 0) {
					rowData = rowDataList[0];
					if (docStatus === 'DELETED') {

						thisObject.datatableReference
							.row($('#' + thisObject.idButtonRowStatusDialog + uuid).parents('tr'))
							.remove()
							.draw();
					} else {

					
						thisObject.datatableReference
							.row($('#' + thisObject.idButtonRowStatusDialog + uuid).parents('tr'))
							.data(rowData).draw();

					}
				}

			}
		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.UpdateStatusDialogBox.name, error);
			return insertSuccess;
		}

		return insertSuccess;

	}


}

