class MaterialTransactionsPage {

	constructor(idFragmentContent) {

		this.viewModel = new WarehouseViewModel();

		this.metaDataTable = new MetaDataTable();


		this.datatableReference = {}; // buat reference datatable

		this.materialTransactionsService = new MaterialTransactionService();
		this.factacctService = new FactAcctService();

	
		this.idTable = 'material_transactions_page_material_transactions_table';
		this.idFragmentContent = idFragmentContent;

	}



	Init() {
		let thisObject = this;
		try {
			// buat reference, karena this dalam jquery berarti element dalam jquery
			let allHtml = `
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head kt-portlet__head--lg">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon">
						<i class="kt-font-brand flaticon2-line-chart"></i>
					</span>
					<h3 class="kt-portlet__head-title">
						Transaksi Material
					</h3>
				</div>			
			</div>
			<div class="kt-portlet__body">
				<table class="table table-striped- table-bordered table-hover table-checkable" id="`+ thisObject.idTable + `">
		
				</table>
			</div>
		</div>
					
			`;


			this.metaDataTable.ad_org_uu = localStorage.getItem('ad_org_uu');
			$('#' + thisObject.idFragmentContent).html(allHtml);
			thisObject.CreateTable();



		} catch (error) {
			apiInterface.Log(this.constructor.name, this.Init.name, error);
		}


	}

	EventHandler() {
		let thisObject = this;




	}


	CreateTable() {
		let thisObject = this;
		var table = $('#' + thisObject.idTable);
		thisObject.materialTransactionsService.FindAll(thisObject.metaDataTable).then(function (materialTransactionCMTList) {

			thisObject.datatableReference = table.DataTable({
				responsive: true,
				data:materialTransactionCMTList,
				scrollX: true,
				columns: [
					{ title: 'Lokasi', data: 'm_warehouse_wx_name' },
					{ title: 'Produk', data: 'm_product_wx_name' },
					{ title: 'Jumlah', data: 'm_transaction_wx_movementqty' },
					{ title: 'Satuan', data: 'c_uom_wx_name' },
					{ title: 'Tipe Movement', data: 'm_transaction_wx_movementtype' },
					{ title: 'Dokumen', data: 'document_type' },
					{ title: 'No Dokumen', data: 'document_no' },
					{ title: 'Status', data: 'm_transaction_wx_docstatus'  },
					{ title: 'Tanggal', data: 'm_transaction_wx_updated' }
				],
				columnDefs: [
					{
						targets: -1,
						title: 'Tanggal',
						orderable: true,
						render: function (data, type, full, meta) {
							return HelperService.ConvertTimestampToLocateDateString(full.m_transaction_wx_updated);
						},
					},
				],

			});

			// update document no dan document type setelah table terbuat
			thisObject.materialTransactionsService.FeedRows(thisObject.datatableReference);


		});



	}

}
