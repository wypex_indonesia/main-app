class MovementlineForm {

	/**
	 * 
	 * @param {MovementlinePage} movementlinePage movementlinePageReference

	 */
	constructor(movementlineCM, movementlinePage) {
		this.validation = {
			m_movementline_wx_movementqty: {
				required: true
			},
			m_product_wx_name: {
				required: true
			}


		};


		this.movementlinePage = movementlinePage;

		this.movementCMT = movementlinePage.movementCMT;


		this.componentModel = movementlineCM;

		this.movementlineService = new MovementlineService();
		this.uomService = new UomService();
		this.productService = new ProductService();

		this.idFormFragmentContent = movementlinePage.idFormFragmentContent;
		this.selectedProduct = {}; // current product yang sedang dipilih, product_viewmodel_table
		this.idFormModal = 'm_movementline_form_wx_' + this.movementCMT.m_movement_wx_m_movement_uu;
		this.idFormValidation = 'm_movementline_form_validation_' + this.movementCMT.m_movement_wx_m_movement_uu;
		this.classNameFormControl = 'm-movementline-form-control';
		this.selectorClassNameFormControl = '.' + this.classNameFormControl;

		this.idWidgetSearch = 'movemenetline_form_widget_search';

		this.idButtonFormSubmit = 'm_movementline_form_submit_wx_' + this.movementCMT.m_movement_wx_m_movement_uu;

		this.datatableReference = movementlinePage.datatableReference;
		this.movementlinePage = movementlinePage;

		this.ctrlBaseUom;
		let lengthRow = this.datatableReference.data().length;
		if (!this.componentModel.m_movementline.m_movementline_uu) {
			this.componentModel.m_movementline.line = ++lengthRow;
		}
		this.Init();

	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery



		let allHtml = thisObject.GenerateFormInput();

		HelperService.InsertReplaceFormModal(thisObject.idFormModal, allHtml);



		HelperService.InjectViewModelToForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.componentModel);

		thisObject.ctrlBaseUom = $('#' + this.idFormValidation + ' .based-uom');
		this.EventHandler();
	}




	/**
	 * 
	 
	 */
	async SaveUpdate() {
		let thisObject = this;

		try {
			if (HelperService.CheckValidation(thisObject.validation)) {

				thisObject.componentModel = HelperService.GetViewModelFromForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.componentModel);

				const timeNow = new Date().getTime();
				// jika new record maka buat mlocator 	
				// harus dibuat default locator, karena locator akan dipergunakan di inventory movement
				let isNewRecord = false;
				if (!thisObject.componentModel.m_movementline.m_movementline_uu) { //new record
					isNewRecord = true;
					thisObject.componentModel.m_movementline.m_movementline_uu = HelperService.UUID();
					thisObject.componentModel.m_movementline.ad_client_uu = localStorage.getItem('ad_client_uu');
					thisObject.componentModel.m_movementline.ad_org_uu = localStorage.getItem('ad_org_uu');

					thisObject.componentModel.m_movementline.createdby = localStorage.getItem('ad_user_uu');
					thisObject.componentModel.m_movementline.updatedby = localStorage.getItem('ad_user_uu');

					thisObject.componentModel.m_movementline.created = timeNow;
					thisObject.componentModel.m_movementline.updated = timeNow;
				} else { //update record


					thisObject.componentModel.m_movementline.updatedby = localStorage.getItem('ad_user_uu');

					thisObject.componentModel.m_movementline.updated = timeNow;
				}


				thisObject.componentModel.m_movementline.m_movement_uu = thisObject.movementCMT.m_movement_wx_m_movement_uu;
				thisObject.componentModel.m_movementline.m_product_uu = thisObject.componentModel.m_product.m_product_uu;
				thisObject.componentModel.m_movementline.sync_client = null;
				thisObject.componentModel.m_movementline.process_date = null;

				let insertSuccess = 0;
				const sqlInsert = HelperService.SqlInsertOrReplaceStatement('m_movementline', thisObject.componentModel.m_movementline);

				insertSuccess += await apiInterface.ExecuteSqlStatement(sqlInsert);

				// jika sukses tersimpan 
				if (insertSuccess > 0) {


					let rowDataList = await thisObject.movementlineService.FindMovementlineCMTByMMovementlineUu(thisObject.componentModel.m_movementline.m_movementline_uu);

					let rowData = {};

					if (rowDataList.length > 0) {
						rowData = rowDataList[0];
					}
					if (!isNewRecord) {
						thisObject.movementlinePage.datatableReference.row($('#bttn_row_edit' + rowData['m_movementline_wx_m_movementline_uu']).parents('tr')).data(rowData).draw();
					} else {
						thisObject.movementlinePage.datatableReference.row.add(rowData).draw();
					}

				}
				$('#' + thisObject.idFormModal).modal('hide');

			}
		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.SaveUpdate.name, error);
		}






	}

	GenerateFormInput() {
		let thisObject = this;


		return `
	<div class="modal fade" id="` + thisObject.idFormModal + `" tabindex="-1" role="dialog"
		aria-labelledby="product-form-labelled" style="display: none;" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="product-form-labelled">Mutasi line Form</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<form id="` + thisObject.idFormValidation + `" class="kt-form">
					<div class="modal-body">
						<div class="form-group">
							<label>No</label>
							<input id="m_movementline_wx_line" type="text"
								class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp">
							<span class="form-text text-muted">Nomor urut</span>
						</div>
						<div class="form-group form-group-marginless">
							<label>Produk</label>
							<div class="input-group">
								<input disabled="disabled" type="text"
									class="` + thisObject.classNameFormControl + ` form-control" id="m_product_wx_name"
									placeholder="Cari produk">
								<div class="input-group-append">
									<span id="search_m_product" class="input-group-text"><i class="la la-search"></i></span>
								</div>
							</div>
							<input type="hidden" class="` + thisObject.classNameFormControl + `"
								id="m_product_wx_m_product_uu">
						</div>
						<div class="form-group">
							<label>Jumlah Produk yang di mutasi</label>
							<div class="input-group">
								<input id="m_movementline_wx_movementqty" type="number"
									class="` + thisObject.classNameFormControl + ` form-control"
									aria-describedby="emailHelp">
	
								<div class="input-group-prepend"><span class="input-group-text based-uom"></span></div>
							</div>
						</div>
						<div class="form-group" id="`+ thisObject.idWidgetSearch + `">
	
	
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button id="`+ thisObject.idButtonFormSubmit + `" type="button" class="btn btn-primary">Save
							changes</button>
					</div>
	
				</form>
			</div>
		</div>
	</div>
		`;
	}

	EventHandler() {
		let thisObject = this;
		$('#' + thisObject.idButtonFormSubmit).off('click');
		$('#' + thisObject.idButtonFormSubmit).on('click', function () {
			thisObject.SaveUpdate();
		});

		/** BEGIN EVENT SEARCH M_Product **************************************/

		$('#' + thisObject.idFormValidation + ' #search_m_product').on('click', function () {

			let getResult = function (selectedProduct) {
				thisObject.selectedProduct = selectedProduct;
				$('#' + thisObject.idFormValidation + ' #m_product_wx_name').val(thisObject.selectedProduct.m_product_wx_name);
				thisObject.ctrlBaseUom.text(thisObject.selectedProduct.c_uom_wx_name);
				$('#' + thisObject.idFormValidation + ' #m_product_wx_m_product_uu').val(thisObject.selectedProduct.m_product_wx_m_product_uu);

			};
			let product_search_widget = new ProductSearchWidget(thisObject.idWidgetSearch, getResult);

		});



		/** END EVENT SEARCH M_PRODUCT **************************************/

		$('.wx-fomovementt-money').mask('#,##0', { reverse: true });
		$('#' + thisObject.idFormModal).modal('show');
	}


}
