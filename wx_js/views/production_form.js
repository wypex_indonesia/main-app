class ProductionForm {

	/**
	 * 
	
	 * @param {string} idFragmentContent name id fragment content untuk meletakkan html content dari form_html
	 * @param {ProductionViewModel}
	 * @param {datatableReference} datatableReference data tabel pada production page, sebagai reference untuk mengupdate table tersebut
	 * @param {c_doctype} productiontype MMS (MM Shipment), MMR (MM Receipt)
	 */
	constructor(productionViewModel, datatableReference) {

		this.titleForm = '';

		this.viewModel = productionViewModel;

		this.labelNoDocument = '';
		this.labelVendorOrCustomer = '';
		this.labelCOrder = '';

		this.metaDataTable = new MetaDataTable();

		this.datatableReference = datatableReference; // buat reference datatable

	
		this.productionService = new ProductionService();
		this.productService = new ProductService();
		this.warehouseService = new WarehouseService();
		this.idFormModal = 'm_production_form_wx_';
		this.idFormValidation = 'm_production_form_validation_';
		this.classNameFormControl = 'm-production-form-control';
		this.selectorClassNameFormControl = '.' + this.classNameFormControl;
		this.classBaseUom = 'base-uom';
		this.idButtonFormSubmit = 'm_production_form_submit_wx_';
		this.selectorClassBaseUom = '.' + this.classBaseUom;
	
		this.idWidgetSearch = 'production_form_widget_search';
		this.prefixContent = 'productionline_page_'; // prefix content untuk child page 



		this.validation = {

			id_form: this.idFormValidation,


			m_product_wx_name: {
				required: true
			},
			m_warehouse_wx_name: {
				required: true
			}


		};




		this.Init();


		

	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery


	
		HelperService.InsertReplaceFormModal(thisObject.idFormModal,  thisObject.GenerateFormInput());

		// jika bukan new record, ambil data-data dibawah untuk select options

		HelperService.InjectViewModelToForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.viewModel);

		$('#'+thisObject.idFormValidation +' ' +  thisObject.selectorClassBaseUom).text(thisObject.viewModel.c_uom.name);
		this.EventHandler();
	}




	/**
	 * 
	 
	 */
	async Save() {
		let thisObject = this;

		let insertSuccess = 0;
		if (HelperService.CheckValidation(thisObject.validation)) {

			let firstProductionqty = numeral( thisObject.viewModel.m_production.productionqty).value();
			
			thisObject.viewModel = HelperService.GetViewModelFromForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.viewModel);
			

			insertSuccess =await thisObject.productionService.SaveUpdate(firstProductionqty, thisObject.viewModel, thisObject.datatableReference);
			// jika sukses tersimpan 
			if (insertSuccess > 0) {

				$('#' + thisObject.idFormModal).modal('hide');


			}

		}


	}

	GenerateFormInput() {
		let thisObject = this;

		return `
	<div class="modal fade" id="` + thisObject.idFormModal + `" tabindex="-1" role="dialog"
		aria-labelledby="product-form-labelled" style="display: none;" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="product-form-labelled">`+ thisObject.titleForm + `</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<form id="`+ thisObject.idFormValidation + `" class="kt-form">
					<div class="modal-body">
	
						<div class="form-group">
							<label>No Dokumen</label>
							<input disabled="disabled" id="m_production_wx_documentno" type="text"
								class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp">
							<span class="form-text text-muted">No Dokumen dibuat otomatis</span>
						</div>
						<div class="form-group form-group-marginless">
							<label>Produk</label>
	
							<div class="input-group">
								<input disabled="disabled" type="text"
									class="` + thisObject.classNameFormControl + ` form-control" id="m_product_wx_name"
									placeholder="Cari Produk  ...">
								<div class="input-group-append">
									<span id="search_m_product" class="input-group-text"><i class="la la-search"></i></span>
								</div>
							</div>
							<input type="hidden" class="` + thisObject.classNameFormControl + `"
								id="m_product_wx_m_product_uu">
						</div>
						<div class="form-group form-group-marginless">
							<label>Gudang/Toko</label>
	
							<div class="input-group">
								<input disabled="disabled" type="text"
									class="` + thisObject.classNameFormControl + ` form-control" id="m_warehouse_wx_name"
									placeholder="Cari Gudang/Toko  ...">
								<div class="input-group-append">
									<span id="search_m_warehouse" class="input-group-text"><i
											class="la la-search"></i></span>
								</div>
							</div>
							<input type="hidden" class="` + thisObject.classNameFormControl + `"
								id="m_warehouse_wx_m_warehouse_uu">
						</div>
						<div class="form-group">
							<label>Jumlah Produksi</label>
							<div class="input-group">
								<input id="m_production_wx_productionqty" type="text"
									class="` + thisObject.classNameFormControl + ` form-control"
									aria-describedby="emailHelp">
								<div class="input-group-prepend"><span class="input-group-text">/</span><span
										class="input-group-text `+ thisObject.classBaseUom+ `"></span></div>
							</div>
						</div>
	
	
						<div class="form-group">
							<label>Deskripsi</label>
							<input id="m_production_wx_description" type="text"
								class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp">
							<span class="form-text text-muted">Deskripsi</span>
						</div>
						<div class="form-group">
							<label>Dokumen Status</label>
							<input disabled="disabled" id="m_production_wx_docstatus" type="text"
								class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp">
							<span class="form-text text-muted">Status Dokumen DRAFT / COMPLETED / REVERSED</span>
						</div>
						<div class="form-group" id="`+thisObject.idWidgetSearch +`">
	
	
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button id="`+ thisObject.idButtonFormSubmit + `" type="submit" class="btn btn-primary">Save
							changes</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	`;
	}


	EventHandler() {
		let thisObject = this;

		$('#' + thisObject.idFormValidation).off('submit');
		$('#' + thisObject.idFormValidation).on('submit', function (e) {
			thisObject.Save();
			e.preventDefault();

		});


		/** BEGIN EVENT SEARCH M_Product **************************************/
		$('#' + thisObject.idFormValidation + ' #search_m_product').off('click');
		$('#' + thisObject.idFormValidation + ' #search_m_product').on('click', function () {

			let functionGetResult = function(selectedProduct){
				thisObject.selectedProduct = selectedProduct;
				$('#' + thisObject.idFormValidation + ' #m_product_wx_name').val(thisObject.selectedProduct.m_product_wx_name);

				$('#' + thisObject.idFormValidation + ' #m_product_wx_m_product_uu').val(thisObject.selectedProduct.m_product_wx_m_product_uu);
	
	
				$('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassBaseUom).text(thisObject.selectedProduct.c_uom_wx_name);
	
			};

			let productWidgetSearch = new ProductSearchWidget(thisObject.idWidgetSearch, functionGetResult);
			productWidgetSearch.Init();

		});

	

		/** END EVENT SEARCH M_PRODUCT **************************************/


		/** BEGIN EVENT SEARCH M_Warehouse **************************************/
		$('#' + thisObject.idFormValidation + ' #search_m_warehouse').off('click');
		$('#' + thisObject.idFormValidation + ' #search_m_warehouse').on('click', function (e) {

			let functionGetResult = function(mWarehouseSelected){

				$('#' + thisObject.idFormValidation + ' #m_warehouse_wx_name').val(mWarehouseSelected.m_warehouse_wx_name);
				$('#' + thisObject.idFormValidation + ' #m_warehouse_wx_m_warehouse_uu').val(mWarehouseSelected.m_warehouse_wx_m_warehouse_uu);
				

			};

			let warehouse_widget_search = new WarehouseSearchWidget(thisObject.idWidgetSearch,functionGetResult);
			warehouse_widget_search.Init();
		});

	

		/** END EVENT SEARCH M_Warehouse **************************************/

		$('#' + thisObject.idFormModal).modal('show');


	}



}
