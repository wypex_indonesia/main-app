class UomPage {

	constructor() {

		this.viewModel = new UomComponentModel();

		this.metaDataTable = new MetaDataTable();
		this.metaDataTable.ad_org_uu = localStorage.getItem('ad_org_uu');

		this.datatableReference = {}; // buat reference datatable
		this.classShowButtonForm = 'uom_page_bttn_show_form';
		this.classDeleteRow = 'uom_page_delete_row';


		this.uomService = new UomService();
	}




	Init() {
		try {
			let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery
			let allHtml = `
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head kt-portlet__head--lg">
			<div  class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
			<i class="kt-font-brand flaticon2-line-chart"></i>
			</span>
			<h3 class="kt-portlet__head-title">
			Satuan
			</h3>
			</div>
			<div  class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-wrapper">
			<div class="kt-portlet__head-actions">
			<button type="button" class="`+ thisObject.classShowButtonForm + ` btn btn-bold btn-label-brand btn-sm" name="new_record" >
			<i class="la la-plus"></i>New Record</button>			
			</div>
			</div>
			</div>
			</div>
			<div class="kt-portlet__body" >
			<table class="table table-striped- table-bordered table-hover table-checkable" id="wx_uom_table">
			
			</thead>
			</table>
			</div>
		</div>
					
		`;


			this.metaDataTable.ad_org_uu = localStorage.getItem('ad_org_uu');



			$('#wx_fragment_content').html(allHtml);


			var table = $('#wx_uom_table');

			thisObject.uomService.FindUomViewModel(thisObject.metaDataTable)
				.then(function (uomCMTList) {
					// begin first table
					thisObject.datatableReference = table.DataTable({
						responsive: true,
						data: uomCMTList,
						scrollX: true,
						columns: [
							{ title: 'Nama', data: 'c_uom_wx_name' },
							{ title: 'Simbol', data: 'c_uom_wx_uomsymbol' },
							{ title: 'Action' }
						],
						columnDefs: [
							{
								targets: -1,
								title: 'Actions',
								orderable: false,
								render: function (data, type, full, meta) {
									return `
			<button id="bttn_row_edit`+ full.c_uom_wx_c_uom_uu + `" name="` + full.c_uom_wx_c_uom_uu + `" type="button"  class="` + thisObject.classShowButtonForm + ` btn btn-outline-hover-info btn-elevate btn-icon"><i class="la la-edit"></i></button>
			<button id="bttn_row_delete`+ full.c_uom_wx_c_uom_uu + `" value="` + full.c_uom_wx_name + `" name="` + full.c_uom_wx_c_uom_uu + `" type="button"    class="` + thisObject.classDeleteRow + ` btn btn-outline-hover-info btn-elevate btn-icon"> <i class="la la-trash"></i></button>
			`;
								},
							},
						],

					});

					thisObject.EventHandler();
				});


			

		} catch (error) {
			apiInterface.Log(this.constructor.name, this.Init.name, error);
		}


	}


	EventHandler() {
		let thisObject = this;
	

		$('.' + thisObject.classDeleteRow).off('click');
		$('.' + thisObject.classDeleteRow).on('click', function () {
			let nameDeleted = $(this).val();
			let uuid = $(this).attr('name');
			let deleteWidget = new DialogBoxDeleteWidget('Hapus Data', nameDeleted, uuid, thisObject);

		});

		$('.' + thisObject.classShowButtonForm).off('click');
		$('.' + thisObject.classShowButtonForm).on('click', function (e) {

			try {
				let attrName = $(this).attr('name');

				if (attrName !== 'new_record') {

					let dataEdited = thisObject.datatableReference.row($('#bttn_row_edit' + attrName).parents('tr')).data();


					thisObject.componentModel = HelperService.ConvertRowDataToViewModel(dataEdited);

				} else {

					thisObject.componentModel = new UomComponentModel();


				}
				let uomForm = new UomForm(thisObject.componentModel, thisObject.datatableReference);

			} catch (error) {
				apiInterface.Log(thisObject.constructor.name, thisObject.EventHandler.name, error)
			}

		});

		thisObject.datatableReference.off('draw');
		thisObject.datatableReference.on('draw', function () {

			thisObject.EventHandler();
		});

	}
	
		/**
	 * Delete pada table, sebenarnya hanya menandai wx_isdeleted = 'Y'
	 * @param {*} dataObject dataObject pada datatable 
	 */
	async Delete(uuidDeleted) {
		let insertSuccess = 0;
		let thisObject = this;
		try {
			let sqlstatementDeleteShipper = HelperService.SqlDeleteStatement('c_uom', 'c_uom_uu', uuidDeleted);
			insertSuccess += await apiInterface.ExecuteSqlStatement(sqlstatementDeleteShipper);

			if (insertSuccess) {
				// delete row pada datatable
				thisObject.datatableReference
					.row($('#bttn_row_delete' + uuidDeleted).parents('tr'))
					.remove()
					.draw();
				$('#modal_info_box').modal('hide');

			}
		} catch (error) {
			apiInterface.Log(this.constructor.name, this.Delete.name, error);
		}

		return insertSuccess;
	}
}
