class ProductionPage {

	/**
	 *	 
	 * @param {*} idFragmentContent  // IdControl untuk meletakkan View Orderpage ex>. #fragmentContent
	 */
	constructor(idFragmentContent) {

		this.titlePage = 'Produksi';


		this.viewModel = new ProductionComponentModel();
		this.labelForm = '';
		this.labelNoDocument = '';

		this.idTable = 'wx_production_table_';
		this.idFragmentContent = idFragmentContent;
		this.productionService = new ProductionService();
		this.metaDataTable = new MetaDataTable();
		this.datatableReference = {}; // buat reference datatable
		this.idShowForm = 'wx-btn-show-form-production';
	
		this.idButtonRowStatusDialog = "production_page_bttn_row_status";

		this.idDialogBox = 'status-dialog-box';


		this.prefixContentChild = 'productionline_page_';

		this.classDialogStatus = 'production_page_button_dialog_status';

		this.columnsDataTable =
			[
				{
					"className": 'details-control',
					"orderable": false,
					"data": null,
					"defaultContent": '<button  type="button" class="btn btn-outline-hover-info btn-icon"><i class="la la-plus-square-o"></i></button>'
				},
				{ title: 'No Dokumen', data: 'm_production_wx_documentno' },
				{ title: 'Produk', data: 'm_product_wx_name' },
				{ title: 'Gudang/Toko', data: 'm_warehouse_wx_name' },
				{ title: 'Jumlah Produksi', data: 'm_production_wx_productionqty' },
				{ title: "Status", data: 'm_production_wx_docstatus' },
				{ title: 'Action' }
			];


		this.Init();

	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery


		let allHtml = `
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head kt-portlet__head--lg">
			<div  class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
			<i class="kt-font-brand flaticon2-line-chart"></i>
			</span>
			<h3 class="kt-portlet__head-title">
		 	`+ thisObject.titlePage + `
			</h3>
			</div>
			<div  class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-wrapper">
			<div class="kt-portlet__head-actions">
			<button type="button" class="` + thisObject.idShowForm + ` btn btn-bold btn-label-brand btn-sm"  name="new_record" >
			<i class="la la-plus"></i>New Record</button>	
			
			</div>
			</div>
			</div>
			</div>
			<div class="kt-portlet__body" >
			<table class="table table-striped- table-bordered table-hover table-checkable" id="`+ thisObject.idTable + `">
		
			</table>
	
			</div>
		</div>
		`;


		this.metaDataTable.ad_org_uu = localStorage.getItem('ad_org_uu');

		$('#' + thisObject.idFragmentContent).html(allHtml);


		var table = $('#' + thisObject.idTable);
		let columnDefDatatable = [
			{
				targets: -1,
				title: 'Actions',
				orderable: false,
				render: function (data, type, full, meta) {

					let buttonStatusHtml = `<button id="` + thisObject.idButtonRowStatusDialog + full.m_production_wx_m_production_uu + `" value="` + full.m_production_wx_documentno + `" name="` + full.m_production_wx_m_production_uu + `" type="button"    class="` + thisObject.classDialogStatus + ` btn btn-outline-hover-info btn-elevate btn-icon"> <i class="la la-flag-o"></i></button>`;
					let buttonEditHtml = `<button id="bttn_row_edit` + full.m_production_wx_m_production_uu + `" name="` + full.m_production_wx_m_production_uu + `" type="button" class="` + thisObject.idShowForm + ` btn btn-outline-hover-info btn-elevate btn-icon"><i class="la la-edit"></i></button>`;
					// jika 
					let htmlAction = '';
					if (full.m_production_wx_docstatus === 'CLOSED') {
						htmlAction = '';
					}


					if (full.m_production_wx_docstatus === 'REVERSED') {
						htmlAction = '';
					}

					if (full.m_production_wx_docstatus === 'COMPLETED') {
						htmlAction = buttonStatusHtml;

					}
					if (full.m_production_wx_docstatus === 'DRAFT') {
						htmlAction = buttonEditHtml + buttonStatusHtml;
					}


					return htmlAction;
				},
			},

		];


		thisObject.productionService.FindAll(thisObject.metaDataTable).then(function (productionCMTList) {
			// begin first table
			thisObject.datatableReference = table.DataTable({
				responsive: true,
				data: productionCMTList,
				scrollX: true,
				columns: thisObject.columnsDataTable,
				columnDefs: columnDefDatatable,
				"order": [[1, 'asc']],
				createdRow: function (row, data, index) {
					if (data.extn === '') {
						var td = $(row).find("td:first");
						td.removeClass('details-control');
					}
				},
				rowCallback: function (row, data, index) {
					//console.log('rowCallback');
				}

			});

			// attach event handler on the page

			thisObject.EventHandler();

		});

	}

	EventHandler() {
		let thisObject = this;
		// Add event listener for opening and closing details
		$('#' + thisObject.idTable + ' tbody').off('click');
		$('#' + thisObject.idTable + ' tbody').on('click', 'td.details-control', function () {
			var tr = $(this).closest('tr');
			var row = thisObject.datatableReference.row(tr);
			let productionParent = row.data();
			if (row.child.isShown()) {
				// This row is already open - close it
				row.child.hide();
				$(this).html('<button  type="button" class="btn btn-outline-hover-info btn-icon"><i class="la la-plus-square-o"></i></button>');
			}
			else {
				let idProductionlineContent = thisObject.prefixContentChild + productionParent.m_production_wx_m_production_uu;
				let productionlineFragment = `<div id="` + idProductionlineContent + `"></div>`;
				row.child(productionlineFragment).show();

				// generate orderlinePage
				let productionlinePage = new ProductionlinePage(productionParent, idProductionlineContent);
				productionlinePage.Init();

				// change menjadi button minus
				$(this).html('<button  type="button" class="btn btn-outline-hover-info btn-icon"><i class="la la-minus-square-o"></i></button>');

			}

		});

		$('.' + thisObject.idShowForm).off("click");
		$('.' + thisObject.idShowForm).on("click", function (e) {

			let attrName = $(this).attr('name');
			let productionViewModel = new ProductionComponentModel();
			productionViewModel.m_production.documentno = new Date().getTime();
			productionViewModel.m_production.docstatus = 'DRAFT';
			if (attrName !== 'new_record') {
				let dataEdited = thisObject.datatableReference.row($('#bttn_row_edit' + attrName).parents('tr')).data();
				productionViewModel = HelperService.ConvertRowDataToViewModel(dataEdited);
			}
			let productionFormPage = new ProductionForm(productionViewModel, thisObject.datatableReference, thisObject.productionType);

			//$('#' + thisObject.idFormModal).modal('show');

		});


		$('.' + thisObject.classDialogStatus).off('click');
		$('.' + thisObject.classDialogStatus).on('click', function (e) {
			let uuid = $(this).attr('name');
			let productionCMT = thisObject.datatableReference.row($('#' + thisObject.idButtonRowStatusDialog + uuid).parents('tr')).data();
			let processDocumentFuction = function (docstatus) {

				return thisObject.UpdateStatusDialogBox(docstatus, uuid);
			};
			let dialogboxstatusWidget = new DialogBoxStatusWidget('Set Status Produksi', productionCMT.m_production_wx_docstatus, processDocumentFuction);

		});

	

		thisObject.datatableReference.off('draw');
		thisObject.datatableReference.on('draw', function () {
			thisObject.EventHandler();

		});
	}

	
	async	UpdateStatusDialogBox(docStatus, uuid) {
		let thisObject = this;
		let insertSuccess = 0;

		try {

			let productionCMT = thisObject.datatableReference.row($('#' + thisObject.idButtonRowStatusDialog + uuid).parents('tr')).data();
			productionCMT.m_production_wx_docstatus = docStatus;
			insertSuccess += await thisObject.productionService.UpdateStatus(productionCMT);

			if (insertSuccess) {
				let rowDataList = await thisObject.productionService.FindProductionCMTListByMProductionUu(productionCMT.m_production_wx_m_production_uu);

				let rowData = {};

				if (rowDataList.length > 0) {
					rowData = rowDataList[0];
					if (docStatus === 'DELETED') {

						thisObject.datatableReference
							.row($('#' + thisObject.idButtonRowStatusDialog + uuid).parents('tr'))
							.remove()
							.draw();
					} else {

						if (docStatus === 'COMPLETED') {
							// cek jika ternyata child orderlinepage terbuka, maka create lagi tablenya
							if ($('#' + thisObject.prefixContentChild +  rowData.c_invoice_wx_c_invoice_uu).length) {

								let productionlinePage = new ProductionlinePage(rowData, thisObject.prefixContentChild + rowData.m_production_wx_m_production_uu);
								productionlinePage.Init();
							}
						}

						thisObject.datatableReference
							.row($('#' + thisObject.idButtonRowStatusDialog + uuid).parents('tr'))
							.data(rowData).draw();

					}
				}

			}
		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.UpdateStatusDialogBox.name, error);
			return insertSuccess;
		}

		return insertSuccess;

	}
}

