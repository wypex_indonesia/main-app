/**
 * CartlinePortlet merupakan cartline portlet pada pos_page.
 * defaultCartCM akan selalu dibuat atau dicari apakah masih ada CartCM , jika tidak ada maka cartCM harus dibuat
 * 
 */
class CartlinePortlet {

    constructor(idFragmentCartline, configPosCMT) {


        this.idCartTable = 'wx_cartline_table';
        this.idFragmentCartline = idFragmentCartline;
        this.posService = new PosService();
        this.businessPartnerService = new BusinessPartnerService();

        this.cartDTRef = {}; // buat reference datatable
        this.defaultCartCM = null;
        this.defaultPosCMT = configPosCMT;
        this.idTotaltaxlines = 'pos_c_order_wx_totaltaxlines';
        this.idGrandTotal = 'pos_c_order_wx_grandtotal';
        this.idBusinesspartnerPhone = 'c_bpartner_location_wx_phone';
        this.idBusinesspartnerName = 'c_bpartner_wx_name';
        this.idCellCartline = 'cartline-portlet-cartline';
        this.Init();



    }

    async  Init() {

        let thisObject = this;
        let ppnFoot = ``;
        if (thisObject.defaultPosCMT.c_tax_wx_rate) {
            ppnFoot = `<tr>
            <td colspan=2></td>
              <td><h4>PPN</h4></td>
              <td id="`+ thisObject.idTotaltaxlines + `"></td>
            </tr>`;
        }

        let htmlTable = `
		<div class="kt-portlet kt-portlet--mobile" >
			<div class="kt-portlet__head kt-portlet__head--lg">
				<div class="kt-portlet__head-label">
					
					<h3 id="cart_total">
					
					</h3>
				</div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-actions">
                        <div class="btn-group btn-group-lg" role="group" aria-label="Large button group">
                            <button id="cartline_portlet_checkout" type="button" class="btn btn-primary">BAYAR (ALT+E)</button>
                            <button id="cartline_portlet_print" type="button" class="btn btn-success">Print (ALT+R)</button>
                            <button id="cartline_portlet_erase_all" type="button" class="btn btn-warning">Hapus (ALT+T)</button>
                        </div>
                    </div>
				</div>
			</div>
            <div class="kt-portlet__body" >
            <form id="`+ thisObject.idFormValidation + `" class="kt-form">
                <div class="form-group row">
                    <div class="col-lg-6">
                        <label>No HP Pelanggan (ALT+A)</label>
                        <input id="`+ thisObject.idBusinesspartnerPhone + `" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
                        <span class="form-text text-muted">Masukkan nomor hp pelanggan</span>
                       
                    </div>
                    <div class="col-lg-6">
                        <label>Nama (ALT+S)</label>
                        <input id="`+ thisObject.idBusinesspartnerName + `" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
                        <span class="form-text text-muted">Nama Pelanggan</span>
                    </div>
				</div>
            </form>
                (ALT+D)
				<table class="table table-striped- table-bordered table-hover table-checkable" id="`+ thisObject.idCartTable + `">
               
                <tfoot>
                `+
            ppnFoot +
            `              
                <tr>
                <td colspan=2></td>
                <td><h4>Grand Total</h4></td>
                <td id="`+ thisObject.idGrandTotal + `"></td>
              </tr>
              </tfoot>
		</table>
		
		</div>
	</div>
		`;
        $('#' + thisObject.idFragmentCartline).html(htmlTable);

        let table = $('#' + thisObject.idCartTable);

        let cartCMTList = await thisObject.posService.FindCartCMTList();

        // set defaultCarCM, jika tidak ada create defaultCartCM 
        if (cartCMTList.length > 0) {

            thisObject.defaultCartCM = HelperService.ConvertRowDataToViewModel(cartCMTList[0]);


        } else {

            thisObject.defaultCartCM = await thisObject.CreateDefaultCartCM();

        }

        let cartlineCMTList = await thisObject.posService.FindCartlineCMTListByCOrderUu(thisObject.defaultCartCM.pos_c_order.c_order_uu);



        let columnsListCartDT =
            [

                { title: 'Harga' },
                { title: 'Jumlah' },
                { title: ' Total' }
            ];
     

        let columnDefDatatable = [
           
            {
                targets: -3,
                title: 'Harga',
                orderable: false,
                render: function (data, type, full, meta) {

                    return new Intl.NumberFormat().format(full.pos_c_orderline_wx_priceactual);

                },
            },
            {
                targets: -2,
                title: 'Jumlah',
                orderable: false,
                render: function (data, type, full, meta) {
                    return new Intl.NumberFormat().format(full.pos_c_orderline_wx_qtyentered);
                },
            },
            {
                targets: -1,
                title: 'Total',
                orderable: false,
                render: function (data, type, full, meta) {

                    return new Intl.NumberFormat().format(full.pos_c_orderline_wx_linenetamt);

                },
            },


        ];


        if (config.build === 'android') {
            columnsListCartDT.unshift({ title: 'Item' })
            columnDefDatatable.unshift(
            {
                targets: 0,
                title: 'Item',
                orderable: false,
                render: function (data, type, full, meta) {
                    let item = full.m_product_wx_sku + "<br>" + full.m_product_wx_name ; 
                    
                    return item;
    
                },
            });
        } else {
            columnsListCartDT.unshift({ title: 'Nama', data: 'm_product_wx_name' });
            columnsListCartDT.unshift({ title: 'SKU/UPC', data: 'm_product_wx_sku' });

        }

       
        // begin first table
        thisObject.cartDTRef = table.DataTable({
            createdRow: function (row, data, index) {
                //angka 2 adalah index column
                if (index === 0) {
                    $('td', row).eq(4).attr('id', thisObject.idCellCartline);
                }

            },
            keys: true,
            paging: false,
            searching: false,
            responsive: true,
            data: cartlineCMTList,
            scrollX: true,
            columns: columnsListCartDT,
            columnDefs: columnDefDatatable,
            ordering: false,


        });

        //set totaltaxlines , grandtotal jika ada cartline
        if (cartlineCMTList.length) {
            this.SetPpnNGrandTotal(thisObject.defaultCartCM.pos_c_order.totaltaxlines, thisObject.defaultCartCM.pos_c_order.grandtotal);

        }

        this.EventHandler();

    }


    EventHandler() {

        let thisObject = this;


        $('#cartline_portlet_print').off('click');
        $('#cartline_portlet_print').on('click', function () {
            thisObject.Print();
        });

        $('#cartline_portlet_erase_all').off('click');
        $('#cartline_portlet_erase_all').on('click', function () {

            thisObject.EraseAllCart();

        });



        $('#cartline_portlet_checkout').off('click');
        $('#cartline_portlet_checkout').on('click', function () {
            thisObject.OpenCheckOutForm();
        });





        $('#' + thisObject.idBusinesspartnerPhone).off('input');
        $('#' + thisObject.idBusinesspartnerPhone).on('input', function () {
            let phone = $(this).val();
            if (phone.length > 5) {
                phone = HelperService.ConvertPhoneWithAreaCode(phone, '62');
                thisObject.businessPartnerService.FindBusinessPartnerCMTListByPhone(phone).then(function (businessPartnerCMTList) {
                    if (businessPartnerCMTList.length) {
                        let businessPartnerCMT = businessPartnerCMTList[0];
                        let businessPartnerCM = HelperService.ConvertRowDataToViewModel(businessPartnerCMT);
                        //thisObject.defaultCartCM.c_bpartner = businessParnerCM.c_bpartner;
                        $('#' + thisObject.idBusinesspartnerName).val(businessPartnerCM.c_bpartner.name);

                    }


                });

            }

        });



        //  change harga dan jumlah pada cartline
        $('#' + thisObject.idCartTable + ' tbody').off('click');
        $('#' + thisObject.idCartTable + ' tbody').on('click', 'tr', function () {
            let tr = $(this);
            var indexRow = thisObject.cartDTRef.row(tr).index();
            let productPriceCMT = thisObject.cartDTRef.row(indexRow).data();

            let cartlineCM = HelperService.ConvertRowDataToViewModel(productPriceCMT);

            let cartlineForm = new CartlineForm(cartlineCM, thisObject.cartDTRef, indexRow);

        });



        thisObject.cartDTRef.off('draw');
        thisObject.cartDTRef.on('draw', function () {
            thisObject.posService.CountTotallinesAndGrandTotal(thisObject.defaultCartCM.pos_c_order.c_order_uu)
                .then(function (rowChanged) {
                    thisObject.posService.FindCartCMTByCOrderUu(thisObject.defaultCartCM.pos_c_order.c_order_uu)
                        .then(function (cartCMTList) {


                            if (cartCMTList.length) {
                                thisObject.defaultCartCM = HelperService.ConvertRowDataToViewModel(cartCMTList[0]);
                            }

                            thisObject.SetPpnNGrandTotal(thisObject.defaultCartCM.pos_c_order.totaltaxlines, thisObject.defaultCartCM.pos_c_order.grandtotal);

                            thisObject.EventHandler();

                        });
                });
        });

        thisObject.HotKeysEvent();
    }


    async AddCartline(productPriceCMT) {

        let thisObject = this;
        let lengthRow = thisObject.cartDTRef.data().length;
        let posCOrderline = new COrderline();

        posCOrderline.line = ++lengthRow;
        posCOrderline.c_orderline_uu = HelperService.UUID();
        posCOrderline.c_uom_uu = productPriceCMT.m_product_wx_c_uom_uu;
        posCOrderline.qtyentered = 1;
        posCOrderline.qtyordered = 1;
        posCOrderline.priceentered = productPriceCMT.m_productprice_wx_pricestd;
        posCOrderline.priceactual = productPriceCMT.m_productprice_wx_pricestd;
        posCOrderline.m_product_uu = productPriceCMT.m_productprice_wx_m_product_uu;
        posCOrderline.c_order_uu = this.defaultCartCM.pos_c_order.c_order_uu;
        posCOrderline.linenetamt = numeral(posCOrderline.priceentered).value() * numeral(posCOrderline.qtyentered).value();
        posCOrderline.linetaxamt = numeral(thisObject.defaultPosCMT.c_tax_wx_rate).value() / 100 * numeral(posCOrderline.linenetamt).value();
        posCOrderline.c_tax_uu = thisObject.defaultPosCMT.c_tax_wx_c_tax_uu;

        posCOrderline.ad_client_uu = localStorage.getItem('ad_client_uu');
        posCOrderline.ad_org_uu = localStorage.getItem('ad_org_uu');

        const sqlInsert = HelperService.SqlInsertOrReplaceStatement('pos_c_orderline', posCOrderline);
        let insertSuccess = 0;
        insertSuccess += await apiInterface.ExecuteSqlStatement(sqlInsert);
        let rowData = await thisObject.posService.FindCartlineCMTByCOrderlineUu(posCOrderline.c_orderline_uu);
        let cartline = rowData[0];
        thisObject.cartDTRef.row.add(cartline).draw();
        return insertSuccess;

    }

    async CreateDefaultCartCM() {
        let thisObject = this;
        let timeNow = new Date().getTime();

        let cartCM = new CartComponentModel();

        let defaultCustomerBPartnerCM = await thisObject.businessPartnerService.GetDefaultCustomerPos();


        cartCM.c_bpartner = defaultCustomerBPartnerCM.c_bpartner;

        let pos_c_order = new PosCOrder();
        pos_c_order.documentno = timeNow;
        pos_c_order.c_order_uu = HelperService.UUID();
        pos_c_order.c_bpartner_uu = cartCM.c_bpartner.c_bpartner_uu;

        cartCM.pos_c_order = pos_c_order;


        const sqlInsert = HelperService.SqlInsertOrReplaceStatement('pos_c_order', pos_c_order);
        let insertSuccess = 0;
        insertSuccess += await apiInterface.ExecuteSqlStatement(sqlInsert);

        return cartCM;

    }

    SetPpnNGrandTotal(totaltaxlines, grandtotal) {
        let thisObject = this;
        totaltaxlines = new Intl.NumberFormat().format(totaltaxlines);
        grandtotal = new Intl.NumberFormat().format(grandtotal);
        $('.dataTables_scrollFoot' + ' #' + thisObject.idTotaltaxlines).html('<h3>' + totaltaxlines + '</h3>');
        $('.dataTables_scrollFoot' + ' #' + thisObject.idGrandTotal).html('<h3>' + grandtotal + '</h3>');
        if (config.build !== 'android'){
            $('#cart_total').html("GrandTotal : " + grandtotal);
        }
        
    }

    OpenCheckOutForm() {
        let thisObject = this;
        if (thisObject.defaultCartCM.pos_c_order.grandtotal) {

            thisObject.defaultCartCM.c_bpartner.name = $('#' + thisObject.idBusinesspartnerName).val();
            thisObject.defaultCartCM.c_bpartner_location.phone = $('#' + thisObject.idBusinesspartnerPhone).val();


            let checkoutForm = new CheckoutForm(thisObject.defaultCartCM, thisObject.defaultPosCMT, thisObject);
        }

    }

    Print() {
        /*  byte[] initialize = new byte[] { 27, 64 };
         byte[] underline = new byte[] { 27, 45,1 };
         Append(initialize);
         Append(underline);
         byte[] bold = new byte[]{ 27, 'E'.ToByte(), 1 };
         Append(bold);
         Append("Test Printer");
         Append("\r");*/
        let receipt_format = new ReceiptFormatService();
        receipt_format.GetReceipt(thisObject.defaultCartCM.pos_c_order.c_order_uu).then(function (listObjPrint) {
            apiInterface.PrintPOS(listObjPrint).then(function (result) {
                if (result) {

                    let newmessageBox = new DialogBoxInfoWidget("Info", result);
                }

            });

        });

    }

    async EraseAllCart() {

        const sqlEraseAll = 'DELETE FROM pos_c_order;';
        await apiInterface.ExecuteSqlStatement(sqlEraseAll);
        const sqlEraseOrderline = 'DELETE FROM pos_c_orderline';
        await apiInterface.ExecuteSqlStatement(sqlEraseOrderline);
        this.Init();

    }

    HotKeysEvent() {
        let thisObject = this;



        hotkeys('alt+a,alt+s,alt+e,alt+r,alt+t,alt+d', function (event, handler) {
            // Prevent the default refresh event under WINDOWS system
            switch (handler.key) {
                case 'alt+a':
                    $('#' + thisObject.idBusinesspartnerPhone).focus();
                    event.preventDefault();
                    return false;
                    break;
                case 'alt+s':
                    $('#' + thisObject.idBusinesspartnerName).focus();
                    event.preventDefault();
                    return false;
                    break;
                case 'alt+e':
                    thisObject.OpenCheckOutForm();
                    event.preventDefault();
                    return false;
                    break;
                case 'alt+r':
                    thisObject.Print();
                    event.preventDefault();
                    return false;
                    break;
                case 'alt+t':
                    thisObject.EraseAllCart();
                    event.preventDefault();
                    return false;
                    break;
                case 'alt+d':
                    thisObject.cartDTRef.cell('#' + thisObject.idCellCartline).focus();
                    event.preventDefault();
                    return false;
                    break;
                default: alert(event);
            }

        });

    }







}