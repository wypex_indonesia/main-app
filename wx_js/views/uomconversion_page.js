class UomConversionPage {

	/**
	 * 
	 * @param {m_product_uu} mProductParentUu product yang akan diproduksi
	 * @param {string} idFragmentContent name id fragment content untuk meletakkan html content dari productbom_page
	 */
	constructor(mProductParentUu, idFragmentContent) {
	

		this.mProductParentUu = mProductParentUu;

		this.viewModel = new UomConversionViewModel();

		this.metaDataTable = new MetaDataTable();


		this.datatableReference = {}; // buat reference datatable

		this.uomConversionService = new UomConversionService();


		this.idFragmentContent = idFragmentContent;
		
		this.classNameFormControl = 'c-uom-conversion-form-control';
		this.selectorClassNameFormControl = '.' + this.classNameFormControl;
		this.idDialogBox = 'c_uom_conversion_dialog_box_wx_' + mProductParentUu;
		this.idTable = 'wx_uomconversion_table_' + mProductParentUu;
		this.idNewRecord = 'new_record_uomconversion_wx_' + mProductParentUu;
		this.idButtonFormSubmit = 'c_uom_conversion_form_submit_wx_' + mProductParentUu;
		this.classShowForm = 'c_uom_conversion_page_show_conversion_form';

	

	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery


		thisObject.metaDataTable.ad_org_uu = localStorage.getItem('ad_org_uu');


		let allHtml = `
		<button type="button" class="`+thisObject.classShowForm + ` btn btn-bold btn-label-brand btn-sm"  id="` + thisObject.idNewRecord + `" name="new_record">
		<i class="la la-plus"></i>New Record</button>			
			<table class="table table-striped- table-bordered table-hover table-checkable" id="`+ thisObject.idTable + `">			
			</table>
		`;



		$('#' + thisObject.idFragmentContent).html(allHtml);



		thisObject.metaDataTable.otherFilters = [' AND c_uom_conversion.m_product_uu = \'' + thisObject.mProductParentUu + '\'']

		var table = $('#' + thisObject.idTable);

		let columnDefDatatable = [
			{
				targets: -1,
				title: 'Actions',
				orderable: false,
				render: function (data, type, full, meta) {
					return `
					<button id="bttn_row_edit`+ full.c_uom_conversion_wx_c_uom_conversion_uu + `" name="` + full.c_uom_conversion_wx_c_uom_conversion_uu + `" type="button"  class="` + thisObject.classShowForm + ` btn btn-outline-hover-info btn-elevate btn-icon"><i class="la la-edit"></i></button>
					<button id="bttn_row_delete`+ full.c_uom_conversion_wx_c_uom_conversion_uu + `" value="1 ` + full.c_uomto_wx_name + ` = ` + full.c_uom_conversion_wx_dividerate + ` ` + full.c_uomfrom_wx_name + `" name="` + full.c_uom_conversion_wx_c_uom_conversion_uu + `" type="button"     class="` + thisObject.classDeleteRow + ` btn btn-outline-hover-info btn-elevate btn-icon"> <i class="la la-trash"></i></button>
					`;
				},
			},

			{
				targets: -2,
				title: 'Konversi Satuan',
				orderable: false,
				render: function (data, type, full, meta) {
					return `
					1 ` + full.c_uomto_wx_name + ` = ` + full.c_uom_conversion_wx_dividerate + ` ` + full.c_uomfrom_wx_name
						;
				},
			},



		];

		thisObject.uomConversionService.FindAll(thisObject.metaDataTable).then(function (shipperCMTList) {

			if (!$.fn.dataTable.isDataTable('#' + thisObject.idTable)) {
				thisObject.datatableReference = table.DataTable({
					searching: false,
					paging: false,
					responsive: true,
					data: shipperCMTList,
					scrollX: true,
					columns: [
						{ title: 'Konversi satuan' },
						{ title: 'Action' }
					],
					columnDefs: columnDefDatatable,
					"order": [[1, 'asc']],

				});

				// attach event handler on the page

				thisObject.EventHandler();

			}

		});
	
	}










	EventHandler() {
		let thisObject = this;

		try {
			$('.' + thisObject.classDeleteRow).off('click');
			$('.' + thisObject.classDeleteRow).on('click', function () {
				let nameDeleted = $(this).val();
				let uuid = $(this).attr('name');
				let deleteWidget = new DialogBoxDeleteWidget('Hapus Data', nameDeleted, uuid, thisObject);

			});




			$('.' + thisObject.classShowForm).off('click')
			$('.' + thisObject.classShowForm).on('click', function (e) {
				let name = $(this).attr('name');

				if (name !== 'new_record') {

					let dataEdited = thisObject.datatableReference.row($('#bttn_row_edit' + name).parents('tr')).data();


					thisObject.viewModel = HelperService.ConvertRowDataToViewModel(dataEdited);

				} else {

					thisObject.viewModel = new UomConversionViewModel();
					thisObject.viewModel.c_uom_conversion.m_product_uu = thisObject.mProductParentUu;

				}
				let uomConversionForm = new UomConversionForm(thisObject.viewModel, thisObject.datatableReference);

			});

			thisObject.datatableReference.off('draw');
			thisObject.datatableReference.on('draw', function () {
	
				thisObject.EventHandler();
			});
	



			$('.wx-format-money').mask('#,##0', { reverse: true });
		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.EventHandler.name, error);
		}






	}

	/**
	 * Delete pada table, sebenarnya hanya menandai wx_isdeleted = 'Y'
	 * @param {*} dataObject dataObject pada datatable 
	 */
	async Delete(uuidDeleted) {
		let insertSuccess = 0;
		let thisObject = this;
		try {
			let sqlstatementDeleteBankaccount = HelperService.SqlDeleteStatement('c_uom_conversion', 'c_uom_conversion_uu', uuidDeleted);
			insertSuccess += await apiInterface.ExecuteSqlStatement(sqlstatementDeleteBankaccount);

			if (insertSuccess) {
				// delete row pada datatable
				thisObject.datatableReference
					.row($('#bttn_row_delete' + uuidDeleted).parents('tr'))
					.remove()
					.draw();


			}
		} catch (error) {
			apiInterface.Log(this.constructor.name, this.Delete.name, error);
		}

		return insertSuccess;
		

	}


}
