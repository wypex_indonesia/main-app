class PrintProductBarcodePage {

    constructor(idFragmentContent) {

        this.idBttnPrint = 'wx_print_product_barcode';
        this.idTableProductBarcode = 'wx_table_print_product_barcode';
        this.idBttnAddProduct = 'wx_btn_add_product';
        this.classDelete = 'wx_btn_delete_barcodeline';

        this.idOptionSizePaper = 'wx_options_size_paper';
        this.namePaperType = 'wx_paper_type';
        this.idShowFormPrintBarcodeline = 'wx_show_form_printbarcodeline';
        this.datatableReference = {};
        this.printProductBarcodelineService = new PrintProductBarcodelineService();
        this.printProductBarcodeService = new PrintProductBarcodeService();
        this.printProductBarcodeCM = new PrintProductbarcodeComponentModel();
        this.idFragmentContent = idFragmentContent;


        this.papersizeService = new PaperSizeService();
        this.Init();
    }


    async   Init() {
        let thisObject = this;
        let printProductBarcodeCMTList = await thisObject.printProductBarcodeService.FindLastPrintProductBarcodeCMTList();

        let printProductBarcodeCMT = null;
        if (printProductBarcodeCMTList.length) {
            printProductBarcodeCMT = printProductBarcodeCMTList[0];
            thisObject.printProductBarcodeCM = HelperService.ConvertRowDataToViewModel(printProductBarcodeCMT);
        } else {
            // jika tidak ada maka langsung save printProductBarcode
            //set default paper type adhesive label
            thisObject.printProductBarcodeCM.wxt_print_productbarcode.wxt_paper_type_uu = 'ff7f5065-f35d-49af-9e65-4ff4c3c31b3f';
            thisObject.printProductBarcodeCM.wxt_print_productbarcode.wxt_paper_size_uu = '2d0a7b67-6bb8-4831-a7b0-dd017e84756e';
            thisObject.printProductBarcodeCM = await thisObject.printProductBarcodeService.SaveUpdate(thisObject.printProductBarcodeCM);

        }

        $('#' + thisObject.idFragmentContent).html(thisObject.GenerateForm());
        thisObject.GenerateOptionsPaperSize(thisObject.printProductBarcodeCM.wxt_print_productbarcode.wxt_paper_type_uu);
       
        thisObject.CreateDataTable();





    }

    InjectCMToForm() {

        try {

            let thisObject = this;
            //'ff7f5065-f35d-49af-9e65-4ff4c3c31b3f', 'Kertas Adhesive Label'
            //'41d6790d-a87d-4838-88a2-bea7b4f1c32d', 'Kertas Label Thermal
            $('input[name=\'' + thisObject.namePaperType + '\']').each(function () {
                let wxt_paper_type_uu = $(this).attr('id');
                if (wxt_paper_type_uu === thisObject.printProductBarcodeCM.wxt_print_productbarcode.wxt_paper_type_uu) {
                    $(this).prop('checked', true);
                }
            });

            $('#' + thisObject.idOptionSizePaper).val(thisObject.printProductBarcodeCM.wxt_print_productbarcode.wxt_paper_size_uu);

        } catch (error) {
            apiInterface.Log(this.constructor.name, this.InjectCMToForm.name, error);
        }



    }



    GenerateForm() {
        let html = '';
        let thisObject = this;
        try {

            html =
                `
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
              <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                          <i class="kt-font-brand flaticon2-line-chart"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                          Cetak Barcode Produk
                    </h3>
              </div>
              <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                          <div class="kt-portlet__head-actions">
                                <button id="`+ thisObject.idBttnPrint + `" type="button" class="btn btn-bold btn-label-brand btn-sm" name="print">
                                      <i class="la la-plus"></i>Cetak</button>  
                             
                          </div>
                    </div>
              </div>
        </div>
        <div class="kt-portlet__body">
              <div class="kt-section kt-section--first">
                <div class="form-group">
					<label>Pilih Tipe Kertas</label>
						<div class="row">
							<div class="col-lg-6">
								<label class="kt-option kt-option kt-option--plain">
									<span class="kt-option__control">
				    					<span class="kt-radio">
											<input id="ff7f5065-f35d-49af-9e65-4ff4c3c31b3f" type="radio" name="`+ thisObject.namePaperType + `" value="ff7f5065-f35d-49af-9e65-4ff4c3c31b3f" checked="">
											<span></span>
										</span>
									</span>
									<span class="kt-option__label">
										<span class="kt-option__head">
											<span class="kt-option__title">
											Kertas Adhesive Label
											</span>
										</span>
                                        <span class="kt-option__body">
                                        Printer yang dipergunakan inkjet, laser , dan printer pada umumnya
                                            <div class="kt-widget__media">
                                               
                                                <img src="assets/media/wx-images/tjlabel_150x100.png" alt="image">
                                            </div>
                                      
										</span>
									</span>
								</label>
                            </div>
                            <div class="col-lg-6">
								<label class="kt-option kt-option kt-option--plain">
									<span class="kt-option__control">
				    					<span class="kt-radio">
											<input id="41d6790d-a87d-4838-88a2-bea7b4f1c32d" type="radio" name="`+ thisObject.namePaperType + `" value="41d6790d-a87d-4838-88a2-bea7b4f1c32d">
											<span></span>
										</span>
									</span>
									<span class="kt-option__label">
										<span class="kt-option__head">
											<span class="kt-option__title">
											Kertas Label Thermal
											</span>
										</span>
                                        <span class="kt-option__body">
                                        Printer yang dipergunakan adalah printer thermal.
                                            <div class="kt-widget__media">
                                            <img src="assets/media/wx-images/thermal_label_150_150.png" alt="image">
                                            </div>                              
										</span>
									</span>
								</label>
							</div>								
                        </div>
                        <span class="form-text text-muted">Pilihan Kertas harus sesuai dengan printer yang
                        digunakan</span>
					</div>
                   
                    <div  class="form-group">
                   
                        <label>Pilih Ukuran Kertas</label>				
                        <select class="form-control" id="`+ thisObject.idOptionSizePaper + `"></select>
			
                    </div>
              </div>
              <button  type="button" class="`+ thisObject.idShowFormPrintBarcodeline + ` btn btn-bold btn-label-brand btn-sm" name="new_record">
              <i class="la la-plus"></i>Add</button>  
              <table class="table table-striped- table-bordered table-hover table-checkable" id="`+ thisObject.idTableProductBarcode + `">
  
  
              </table>
        </div>
  </div>
        `;
        } catch (error) {
            apiInterface.Log(this.constructor.name, this.GenerateForm.name, error);
        }


        return html;

    }

    GenerateOptionsPaperSize(wxt_paper_type_uu) {
        let thisObject = this;
        try {


            if (wxt_paper_type_uu) {

                $('#' + thisObject.idOptionSizePaper).empty();

                thisObject.papersizeService.FindByWxtPaperTypeUu(wxt_paper_type_uu)
                    .then(function (papersizeCMTList) {

                        _.forEach(papersizeCMTList, function (papersizeCMT, index) {
                            let selected = '';
                            if (index === 0) {
                                selected = 'selected';
                            }
                            $('#' + thisObject.idOptionSizePaper).append('<option value="' + papersizeCMT.wxt_paper_size_wx_wxt_paper_size_uu + '"  ' + selected + '>' + papersizeCMT.wxt_paper_size_wx_paper_size + '</option>');

                        });
                        thisObject.InjectCMToForm();
                    });

            }

        } catch (error) {
            apiInterface.Log(this.constructor.name, this.GenerateOptionsPaperSize.name, error);
        }

    }


    CreateDataTable() {

        let thisObject = this;
        let table = $('#' + thisObject.idTableProductBarcode);

        let columnDefDatatable = [];
        let columnsTable = [
            { title: 'Produk', data: 'm_product_wx_name' },
            { title: 'SKU', data: 'm_product_wx_sku' },
            { title: 'UPC', data: 'm_product_wx_upc' },
            { title: 'Barcode', data: 'wxt_print_productbarcodeline_wx_isskuupc' },
            { title: 'Jumlah', data: 'wxt_print_productbarcodeline_wx_count_print' },
            { title: 'Action' }
        ];


        columnDefDatatable.push({
            targets: -1,
            title: 'Actions',
            orderable: false,
            render: function (data, type, full, meta) {

                let htmlButton = `
						<button id="bttn_row_edit`+ full.wxt_print_productbarcodeline_wx_wxt_print_productbarcodeline_uu + `" name="` + full.wxt_print_productbarcodeline_wx_wxt_print_productbarcodeline_uu + `" type="button"  class="` + thisObject.idShowFormPrintBarcodeline + ` btn btn-outline-hover-info btn-elevate btn-icon"><i class="la la-edit"></i></button>
						<button id="bttn_row_delete`+ full.wxt_print_productbarcodeline_wx_wxt_print_productbarcodeline_uu + `" value="` + full.m_product_wx_name + `" name="` + full.wxt_print_productbarcodeline_wx_wxt_print_productbarcodeline_uu + `" type="button"    class="` + thisObject.classDelete + ` wx-row-delete btn btn-outline-hover-info btn-elevate btn-icon"> <i class="la la-trash"></i></button>
						`;

                return htmlButton;
            },
        });



        thisObject.printProductBarcodelineService.FindPrintProductBarcodelineCMTListByWxtPrintProductBarcodeUu(thisObject.printProductBarcodeCM.wxt_print_productbarcode.wxt_print_productbarcode_uu)
            .then(function (productBarcodeCMTList) {

                // begin first table
                if (!$.fn.dataTable.isDataTable('#' + thisObject.idTableProductBarcode)) {
                    thisObject.datatableReference = table.DataTable({
                        searching: false,
                        paging: false,
                        responsive: true,
                        data: productBarcodeCMTList,
                        scrollX: true,
                        columns: columnsTable,
                        columnDefs: columnDefDatatable,
                        "order": [[1, 'asc']],

                    });


                }

                thisObject.EventHandler();

            })


    }

    EventHandler() {

        let thisObject = this;

        $('#' + thisObject.idBttnPrint).off("click");
        $('#' + thisObject.idBttnPrint).on("click", function (e) {

            if (thisObject.CheckValidation) {




                thisObject.printProductBarcodeService.SaveUpdate(thisObject.printProductBarcodeCM).then(function (productBarcodeCM) {

                    thisObject.printProductBarcodeCM = productBarcodeCM;

                    thisObject.printProductBarcodelineService
                        .FindPrintProductBarcodelineCMTListByWxtPrintProductBarcodeUu(thisObject.printProductBarcodeCM.wxt_print_productbarcode.wxt_print_productbarcode_uu)
                        .then(function (productBarcodelineCMTList) {

                            if (thisObject.printProductBarcodeCM.wxt_print_productbarcode.wxt_paper_size_uu === '2d0a7b67-6bb8-4831-a7b0-dd017e84756e') {

                                let printPreviewFunction = function (idContentPrint) {
                                    let printpreviewService = new PrintPreviewService();
                                    return printpreviewService.PrintBarcodeLabel(idContentPrint, productBarcodelineCMTList);
                                };

                                let dialogPrint = new DialogBoxPrintWidget('Print Preview', printPreviewFunction);

                            } else {

                                // print dengan scroll paper


                            }

                        });






                });
            }


        });

        $('.' + thisObject.classDelete).off("click");
        $('.' + thisObject.classDelete).on("click", function (e) {

            let dialogBoxDelete = new DialogBoxDeleteWidget('Hapus Barcodeline', $(this).val(), $(this).attr('name'), thisObject);


        });



        $('.' + thisObject.idShowFormPrintBarcodeline).off("click");
        $('.' + thisObject.idShowFormPrintBarcodeline).on("click", function (e) {

            let attrName = $(this).attr('name');
            let printBarcodelineCM = new PrintProductbarcodelineComponentModel();
            if (attrName !== 'new_record') {
                let dataEdited = thisObject.datatableReference.row($('#bttn_row_edit' + attrName).parents('tr')).data();
                printBarcodelineCM = HelperService.ConvertRowDataToViewModel(dataEdited);
            }
            let orderFormPage = new PrintProductBarcodelineForm(thisObject.printProductBarcodeCM, printBarcodelineCM, thisObject.datatableReference);

            $('#' + thisObject.idFormModal).modal('show');

        });

        $('input[name=\'' + thisObject.namePaperType + '\']').off('change');
        $('input[name=\'' + thisObject.namePaperType + '\']').on('change', function () {
            let wxt_paper_type_uu = $(this).attr('id');
            thisObject.GenerateOptionsPaperSize(wxt_paper_type_uu);

        });

        thisObject.datatableReference.off('draw');
        thisObject.datatableReference.on('draw', function () {
            thisObject.EventHandler();
        });
    }

    Delete(uuid) {
        let thisObject = this;
        let sqlDelete = 'DELETE FROM wxt_print_productbarcodeline WHERE wxt_print_productbarcodeline_uu = \'' + uuid + '\'';
        let insertSuccess = 0;
        apiInterface.ExecuteSqlStatement(sqlDelete).then(function (rowChanged) {
            insertSuccess += rowChanged;
            if (insertSuccess) {
                // delete row pada datatable
                thisObject.datatableReference
                    .row($('#bttn_row_delete' + uuid).parents('tr'))
                    .remove()
                    .draw();

            }

        });

    }



    CheckValidation() {
        let thisObject = this;
        let isValid = true;
        let wxt_paper_type_uu = $("input[name='" + thisObject.namePaperType + "']:checked").val();
        thisObject.printProductBarcodeCM.wxt_print_productbarcode.wxt_paper_type_uu = wxt_paper_type_uu;

        let wxt_paper_size_uu = $('#' + thisObject.idOptionSizePaper).val();
        if (!wxt_paper_size_uu) {
            $('#' + thisObject.idOptionSizePaper).addClass('is-invalid');
            $('#' + thisObject.idOptionSizePaper).get(0).scrollIntoView();
            isValid = false;
        } else {
            $('#' + thisObject.idOptionSizePaper).removeClass('is-invalid');
            isValid = true;
            thisObject.printProductBarcodeCM.wxt_print_productbarcode.wxt_paper_size_uu = wxt_paper_size_uu;
        }



    }

}