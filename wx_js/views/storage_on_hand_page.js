class StorageOnHandPage {

	constructor(idFragmentContent) {

		this.viewModel = new StorageOnHandComponentModel();

		this.metaDataTable = new MetaDataTable();

		this.datatableReference = {}; // buat reference datatable

		this.storageonhandService = new StorageOnHandService();


		this.idTable = 'storage_on_hand_page_storage_on_hand_table';
		this.idFragmentContent = idFragmentContent;

	}



	Init() {
		let thisObject = this;
		try {
			// buat reference, karena this dalam jquery berarti element dalam jquery
			let allHtml = `
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head kt-portlet__head--lg">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon">
						<i class="kt-font-brand flaticon2-line-chart"></i>
					</span>
					<h3 class="kt-portlet__head-title">
					 	Laporan Stock
					</h3>
				</div>			
			</div>
			<div class="kt-portlet__body">
				<table class="table table-striped- table-bordered table-hover table-checkable" id="`+ thisObject.idTable + `">
		
				</table>
			</div>
		</div>
					
			`;


			this.metaDataTable.ad_org_uu = localStorage.getItem('ad_org_uu');
			$('#' + thisObject.idFragmentContent).html(allHtml);
			thisObject.CreateTable();



		} catch (error) {
			apiInterface.Log(this.constructor.name, this.Init.name, error);
		}


	}

	EventHandler() {
		let thisObject = this;




	}


	CreateTable() {
		let thisObject = this;
		var table = $('#' + thisObject.idTable);
		thisObject.storageonhandService.FindAll(thisObject.metaDataTable).then(function (materialTransactionCMTList) {

			thisObject.datatableReference = table.DataTable({
				responsive: true,
				data: materialTransactionCMTList,
				scrollX: true,
				columns: [
					{ title: 'Lokasi', data: 'm_warehouse_wx_name' },
					{ title: 'Produk', data: 'm_product_wx_name' },
					{ title: 'Jumlah', data: 'm_storageonhand_wx_qtyonhand' },
					{ title: 'Satuan', data: 'c_uom_wx_name' }
				]

			});


		});



	}

}
