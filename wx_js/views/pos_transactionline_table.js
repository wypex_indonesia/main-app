class PosTransactionlineTable {

	/**
	 * 
	 * @param {OrderViewModelTable} posTransactionCMT product yang akan diproduksi
	 * @param {string} idFragmentContent name id fragment content untuk meletakkan html content dari productbom_page
	 */
	constructor(posTransactionCMT, idFragmentContent) {

		this.posTransactionCMT = posTransactionCMT;


		this.viewModel = new PosTransactionlineComponentModel();

		this.metaDataTable = new MetaDataTable();


		this.datatableReference = {}; // buat reference datatable

		this.pos_transactionline_service = new PosTransactionlineService();


		this.idFragmentContent = idFragmentContent;


		this.idTable = 'wx_postransactionline_table_' + posTransactionCMT.pos_transaction_wx_pos_transaction_uu;
		this.idNewRecord = 'new_record_postransactionline_wx_' + posTransactionCMT.pos_transaction_wx_pos_transaction_uu;
		this.idGenerateItemRetur = 'postransactionline_page_generate_item_retur' + posTransactionCMT.pos_transaction_wx_pos_transaction_uu;
		this.classGenerateItemRetur = 'postransactionline_page_generate_item';

		this.idShowFormOrderline = 'wx-btn-show-form-postransactionline';

		this.classDeleteRow = 'postransactionline_page_delete_row';
	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery


		thisObject.metaDataTable.ad_org_uu = localStorage.getItem('ad_org_uu');

		let allHtml =
	
			`<table class="table table-striped- table-bordered table-hover table-checkable" id="` + thisObject.idTable + `">			
			</table>
		`;




		$('#' + thisObject.idFragmentContent).html(allHtml);


		thisObject.CreateDataTable();


	}


	PageEventHandler() {
		let thisObject = this;




		thisObject.datatableReference.off('draw');
		thisObject.datatableReference.on('draw', function () {
			
			thisObject.PageEventHandler();

		});
	}



	CreateDataTable() {

		let thisObject = this;

		var table = $('#' + thisObject.idTable);

		let columnDefDatatable = [];
		let columnsTable = [
			{ title: 'Nama Akun', data: 'c_bankaccount_wx_name' },
			{ title: 'Open Amount'  },
			{ title: 'Close Amount' },
			{ title: 'Aktual Amount' },
		
		];
		columnDefDatatable.push({
			targets: -1,
			title: 'Aktual Amount',
			orderable: false,
			render: function (data, type, full, meta) {
				let linenetamt = new Intl.NumberFormat().format(full.pos_transactionline_wx_actual_amount);

				return linenetamt;
			},
		});
		columnDefDatatable.push({
			targets: -2,
			title: 'Close Amount',
			orderable: false,
			render: function (data, type, full, meta) {
				let linenetamt = new Intl.NumberFormat().format(full.pos_transactionline_wx_close_amount);

				return linenetamt;
			},
		});
		columnDefDatatable.push({
			targets: -3,
			title: 'Open Amount',
			orderable: false,
			render: function (data, type, full, meta) {
				let linenetamt = new Intl.NumberFormat().format(full.pos_transactionline_wx_open_amount);

				return linenetamt;
			},
		});

	

		thisObject.pos_transactionline_service.FindPosTransactionlineCMTListByPosTransactionUu(thisObject.posTransactionCMT.pos_transaction_wx_pos_transaction_uu)
			.then(function (postransactionlineCMTList) {
				// begin first table
				if (!$.fn.dataTable.isDataTable('#' + thisObject.idTable)) {
					thisObject.datatableReference = table.DataTable({
						searching: false,
						paging: false,
						responsive: true,
						data: postransactionlineCMTList,
						scrollX: true,
						columns: columnsTable,
						columnDefs: columnDefDatatable,
						"order": [[0, 'desc']],

					});

					thisObject.PageEventHandler();

				}

			});

	}
}
