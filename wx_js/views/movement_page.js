/**
 *Mutasi Barang
 * Transaction hanya pada MaterialTransaction 
 * MovementFrom	Minus	M_Movementline
 * MovementTo	Plus	M_Movementline

 */
class MovementPage {

	/**
	 * 	
	 * @param {*} idFragmentContent  // IdControl untuk meletakkan View Orderpage ex>. #fragmentContent
	 */
	constructor(idFragmentContent) {

		this.titlePage = 'Mutasi Barang';


		this.viewModel = new MovementComponentModel();
		this.labelForm = '';


		this.idTable = 'wx_movement_table_';
		this.idFragmentContent = idFragmentContent;
		this.movementService = new MovementService();
		this.metaDataTable = new MetaDataTable();
		this.datatableReference = {}; // buat reference datatable
		this.idShowForm = 'wx-btn-show-form-movement';
		this.idFormFragmentContent = "wx_movement_form_content";
		this.idButtonRowStatusDialog = "bttn_row_status";

		this.classDialogStatus = 'movement-page-status-dialog-box';

		this.labelNoDocument = 'No Dokumen';
		this.prefixContentChild = 'movementline_page_';


		this.columnsDataTable =
			[
				{
					"className": 'details-control',
					"orderable": false,
					"data": null,
					"defaultContent": '<button  type="button" class="btn btn-outline-hover-info btn-icon"><i class="la la-plus-square-o"></i></button>'
				},
				{ title: this.labelNoDocument, data: 'm_movement_wx_documentno' },
				{ title: 'Dari', data: 'm_warehouse_from_wx_name' },
				{ title: 'Ke', data: 'm_warehouse_to_wx_name' },
				{ title: "Status", data: 'm_movement_wx_docstatus' },
				{ title: "Tanggal", data: 'm_movement_wx_movementdate' },
				{ title: 'Action' }
			];

		this.Init();

	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery


		let allHtml = `
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head kt-portlet__head--lg">
			<div  class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
			<i class="kt-font-brand flaticon2-line-chart"></i>
			</span>
			<h3 class="kt-portlet__head-title">
		 	`+ thisObject.titlePage + `
			</h3>
			</div>
			<div  class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-wrapper">
			<div class="kt-portlet__head-actions">
			<button type="button" class="` + thisObject.idShowForm + ` btn btn-bold btn-label-brand btn-sm"  name="new_record" >
			<i class="la la-plus"></i>New Record</button>	
			
			</div>
			</div>
			</div>
			</div>
			<div class="kt-portlet__body" >
			<table class="table table-striped- table-bordered table-hover table-checkable" id="`+ thisObject.idTable + `">
			
		
			</table>
		
		</div>
		`;


		this.metaDataTable.ad_org_uu = localStorage.getItem('ad_org_uu');

		//	allHtml += HelperService.DialogBox(thisObject.idDialogBox, 'Set Status Dokumen');



		$('#' + thisObject.idFragmentContent).html(allHtml);


		var table = $('#' + thisObject.idTable);
		let columnDefDatatable = [
			{
				targets: -1,
				title: 'Actions',
				orderable: false,
				render: function (data, type, full, meta) {


					let buttonEdit = `<button id="bttn_row_edit` + full.m_movement_wx_m_movement_uu + `" name="` + full.m_movement_wx_m_movement_uu + `" type="button"  class="` + thisObject.idShowForm + ` btn btn-outline-hover-info btn-elevate btn-icon"><i class="la la-edit"></i></button>`;
					let buttonStatus = `<button id="` + thisObject.idButtonRowStatusDialog + full.m_movement_wx_m_movement_uu + `" value="` + full.m_movement_wx_documentno + `" name="` + full.m_movement_wx_m_movement_uu + `" type="button" class="` + thisObject.classDialogStatus + ` btn btn-outline-hover-info btn-elevate btn-icon"> <i class="la la-flag-o"></i></button>`;

					// jika 
					let htmlAction = '';
					if (full.m_movement_wx_docstatus === 'CLOSED') {
						htmlAction = '';
					}


					if (full.m_movement_wx_docstatus === 'REVERSED') {
						htmlAction = '';
					}

					if (full.m_movement_wx_docstatus === 'COMPLETED') {
						htmlAction = buttonStatus;
					}
					if (full.m_movement_wx_docstatus === 'DRAFT') {
						htmlAction = buttonEdit + buttonStatus;
					}


					return htmlAction;
				},
			},
			{
				targets: -2,
				title: 'Tanggal',
				orderable: false,
				render: function (data, type, full, meta) {
					return HelperService.ConvertTimestampToLocateDateString(full.m_movement_wx_movementdate);
				},
			},


		];

		thisObject.movementService.FindAll(thisObject.metaDataTable).then(function (movementCMTList) {
			// begin first table
			thisObject.datatableReference = table.DataTable({
				responsive: true,
				data: movementCMTList,
				scrollX: true,
				columns: thisObject.columnsDataTable,
				columnDefs: columnDefDatatable,
				"order": [[1, 'asc']],
				createdRow: function (row, data, index) {
					if (data.extn === '') {
						var td = $(row).find("td:first");
						td.removeClass('details-control');
					}
				},
				rowCallback: function (row, data, index) {
					//console.log('rowCallback');
				}

			});

			thisObject.EventHandler();


		});

	}

	EventHandler() {
		let thisObject = this;
		// Add event listener for opening and closing details
		$('#' + thisObject.idTable + ' tbody').off('click');
		$('#' + thisObject.idTable + ' tbody').on('click', 'td.details-control', function () {
			var tr = $(this).closest('tr');
			var row = thisObject.datatableReference.row(tr);
			let movementParent = row.data();
			if (row.child.isShown()) {
				// This row is already open - close it
				row.child.hide();
				$(this).html('<button  type="button" class="btn btn-outline-hover-info btn-icon"><i class="la la-plus-square-o"></i></button>');
			}
			else {
				let idMovementdetailContent = thisObject.prefixContentChild + movementParent.m_movement_wx_m_movement_uu;
				let movementlineFragment = `<div id="` + idMovementdetailContent + `"></div>`;
				row.child(movementlineFragment).show();

				// generate orderlinePage
				let movementlinePage = new MovementlinePage(movementParent, idMovementdetailContent);
				movementlinePage.Init();


				// add reference to listReferenceOrderlinePage, agar nanti bisa dipanggil lagi di orderpage, untuk membuat

				// change menjadi button minus
				$(this).html('<button  type="button" class="btn btn-outline-hover-info btn-icon"><i class="la la-minus-square-o"></i></button>');

			}

		});

		$('.' + thisObject.classDialogStatus).off('click');
		$('.' + thisObject.classDialogStatus).on('click', function (e) {
			let uuid = $(this).attr('name');
			let movementCMT = thisObject.datatableReference.row($('#' + thisObject.idButtonRowStatusDialog + uuid).parents('tr')).data();

			let processDocumentFuction = function (docstatus) {

				return thisObject.UpdateStatusDialogBox(docstatus, uuid);
			};

			let dialogboxstatusWidget = new DialogBoxStatusWidget('Set Status Mutasi Barang', movementCMT.m_movement_wx_docstatus, processDocumentFuction);


		});

		$('.' + thisObject.idShowForm).off("click");
		$('.' + thisObject.idShowForm).on("click", function (e) {

			let attrName = $(this).attr('name');
			let movementCM = new MovementComponentModel();
			movementCM.m_movement.documentno = new Date().getTime();
			movementCM.m_movement.docstatus = 'DRAFT';
			if (attrName !== 'new_record') {
				let dataEdited = thisObject.datatableReference.row($('#bttn_row_edit' + attrName).parents('tr')).data();
				movementCM = HelperService.ConvertRowDataToViewModel(dataEdited);
			}
			let movementForm = new MovementForm(movementCM, thisObject.datatableReference);

			//$('#' + thisObject.idFormModal).modal('show');

		});


		



		thisObject.datatableReference.off('draw');
		thisObject.datatableReference.on('draw', function () {
			thisObject.EventHandler();

		});



	}






	async	UpdateStatusDialogBox(docStatus, uuid) {
		let thisObject = this;
		let insertSuccess = 0;

		try {

			let movementCMT = thisObject.datatableReference.row($('#' + thisObject.idButtonRowStatusDialog + uuid).parents('tr')).data();
			movementCMT.m_movement_wx_docstatus = docStatus;
			insertSuccess += await thisObject.movementService.UpdateStatus(movementCMT);

			if (insertSuccess) {
				let rowDataList = await thisObject.movementService.FindMovementCMByMMovementUu(movementCMT.m_movement_wx_m_movement_uu);

				let rowData = {};

				if (rowDataList.length > 0) {
					rowData = rowDataList[0];
					if (docStatus === 'DELETED') {

						thisObject.datatableReference
							.row($('#' + thisObject.idButtonRowStatusDialog + uuid).parents('tr'))
							.remove()
							.draw();
					} else {

						if (docStatus === 'COMPLETED') {
							// cek jika ternyata child orderlinepage terbuka, maka create lagi tablenya
							if ($('#' + thisObject.prefixContentChild + rowData.m_inventory_wx_m_inventory_uu).length) {

								let movementlinePage = new MovementlinePage(rowData, thisObject.prefixContentChild + rowData.m_movement_wx_m_movement_uu);
								movementlinePage.Init();
							}
						}

						thisObject.datatableReference
							.row($('#' + thisObject.idButtonRowStatusDialog + uuid).parents('tr'))
							.data(rowData).draw();

					}
				}

			}
		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.UpdateStatusDialogBox.name, error);
			return insertSuccess;
		}



		return insertSuccess;




	}




}

