/**
 * qty = barang sesuai dengan uom yang tertulis
 * inventoryqty = barang sesuai dengan uom based uom
 */
class InventorylinePage {

	/**
	 * 
	 * @param {inventoryCMT} inventoryCMT parent InvoiceComponentModelTable
	 * @param {string} idFragmentContent name id fragment content untuk meletakkan html content dari productbom_page
	 */
	constructor(inventoryCMT, idFragmentContent) {

		this.inventoryCMT = inventoryCMT;


		this.viewModel = new InventorylineComponentModel();

		this.metaDataTable = new MetaDataTable();
		this.productService = new ProductService();
		this.storageOnHandService = new StorageOnHandService();

		this.datatableReference = {}; // buat reference datatable

		this.inventorylineService = new InventorylineService();

		//	this.inventorylineForm;

		this.idFragmentContent = idFragmentContent;

		this.idTable = 'wx_inventoryline_table_' + inventoryCMT.m_inventory_wx_m_inventory_uu;
		this.idInputNewInventoryline = 'new_record_inventoryline_wx_' + inventoryCMT.m_inventory_wx_m_inventory_uu;

		this.idFormFragmentContent = "wx_inventoryline_form_content";
		this.idShowFormMovementline = 'wx-btn-show-form-inventoryline';
		this.classNameFormControl = 'm-inventoryline-form-control' + inventoryCMT.m_inventory_wx_m_inventory_uu;
		this.classDeleteRow = 'inventoryline_page_delete_row';
		this.productSearchDialogBox = null;
	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery


		thisObject.metaDataTable.ad_org_uu = localStorage.getItem('ad_org_uu');

		let inputSearchNewRecordHtml = '';
		if (thisObject.inventoryCMT.m_inventory_wx_docstatus === 'DRAFT') {
			inputSearchNewRecordHtml = `
			<div class="form-group ">
					<label>Cari Produk Untuk Stock Opname</label>
					 <div class="kt-input-icon kt-input-icon--left">
						<input type="text" class="form-control" placeholder="Search..." id="`+ thisObject.idInputNewInventoryline + `">
							<span class="kt-input-icon__icon kt-input-icon__icon--left">
								<span><i class="la la-search"></i></span>
							</span>
						</div>
				<span class="form-text text-muted">Ketik sku/upc/ nama produk yang akan di stock opname</span>
			</div>
			
			`;
		}

		let allHtml =
			inputSearchNewRecordHtml +
			`<table class="table table-striped- table-bordered table-hover table-checkable" id="` + thisObject.idTable + `">			
			</table>`;


		$('#' + thisObject.idFragmentContent).html(allHtml);


		//	thisObject.inventorylineForm = new MovementlineForm(thisObject);

		thisObject.CreateDataTable();
		// attach event handler on the page

	}







	/**
 * Delete pada table, sebenarnya hanya menandai wx_isdeleted = 'Y'
 * @param {*} dataObject dataObject pada datatable 
 */
	async Delete(uuiddeleted) {
		let thisObject = this;
		let insertSuccess = 0;
		try {
			let sqlstatementDeleteBankaccount = HelperService.SqlDeleteStatement('m_inventoryline', 'm_inventoryline_uu', uuiddeleted);

			insertSuccess += await apiInterface.ExecuteSqlStatement(sqlstatementDeleteBankaccount);

			if (insertSuccess) {

				thisObject.datatableReference
					.row($('#bttn_row_delete' + uuiddeleted).parents('tr'))
					.remove()
					.draw();
			}
		} catch (error) {
			apiInterface.Log(thisObject.constructor.name, thisObject.Delete.name, error);
		}

		return insertSuccess;
	}


	EventHandler() {
		let thisObject = this;



		/**
		 * 1. Cari di product Table 
		 *  1.1 jika ditemukan hanya 1 maka
		 * 	 	cari di m_storageonhand,
		 * 		1.1.1 Jika ditermukan di m_storage onhand, maka ambil storageonhand
		 * 			  dan masukkan ke dalam qtybook pada new inventoryline
		 * 		1.2.1 Jika tidak ditemukan di m_storage onhand, 
		 * 			 create new inventoryline 
		 *  1.2 Jika ditemukan lebih dari 1 mka
		 *  show form list product
		 */
		$('#' + thisObject.idInputNewInventoryline).off("input");
		$('#' + thisObject.idInputNewInventoryline).on("input", function (e) {
			let searchValue = $(this).val();
			if (searchValue.length >= 3) {
				let isSearching = true;

				if (isSearching) {
					let metaDataTableSearch = new MetaDataTable();
					metaDataTableSearch.ad_org_uu = localStorage.getItem('ad_org_uu');
					metaDataTableSearch.otherFilters = [' AND m_product.name LIKE  \'%' + searchValue + '%\'', ' OR m_product.sku LIKE  \'%' + searchValue + '%\'', ' OR m_product.upc LIKE  \'%' + searchValue + '%\''];
					let rows = thisObject.productService.FindAll(metaDataTableSearch).then(function (rows) {
						if (rows.length > 0) {

							if (rows.length === 1) {

								let productCMT = rows[0];
								thisObject.inventorylineService.CreateNewInventoryline(productCMT.m_product_wx_m_product_uu,
									thisObject.inventoryCMT, thisObject.datatableReference).then(function (inserted) {
										$('#' + thisObject.idInputNewInventoryline).val('');


									});


							} else {
								searchValue = $('#' + thisObject.idInputNewInventoryline).val();
								let functionPostSelectedProduct = function(productCMT){

									return thisObject.PostSelectedProductFunction(productCMT);
								};
								thisObject.productSearchDialogBox = new ProductSearchDialogBox(searchValue, functionPostSelectedProduct);

							}

						}


					});

					isSearching = false;

				}
			}



		});

		$('.' + thisObject.classNameFormControl).off("input");
		$('.' + thisObject.classNameFormControl).on("input", function (e) {
			let m_inventoryline_uu = $(this).attr('name');
			thisObject.inventorylineService.UpdateQtyCount(m_inventoryline_uu, $(this).val()).then(function (inserted) {


			});
		});



		$('.' + thisObject.classDeleteRow).off('click');
		$('.' + thisObject.classDeleteRow).on('click', function () {
			let nameDeleted = $(this).val();
			let uuid = $(this).attr('name');
			let deleteWidget = new DialogBoxDeleteWidget('Hapus Data', nameDeleted, uuid, thisObject);

		});



		thisObject.datatableReference.off('draw');
		thisObject.datatableReference.on('draw', function () {
			thisObject.EventHandler();

		});



	}

	CreateDataTable() {

		let thisObject = this;

		thisObject.metaDataTable.otherFilters = [' AND m_inventoryline.m_inventory_uu = \'' + thisObject.inventoryCMT.m_inventory_wx_m_inventory_uu + '\''];

		var table = $('#' + thisObject.idTable);

		let columnDefDatatable = [];
		let columnsTable = [
			{ title: 'No', data: 'm_inventoryline_wx_line' },
			{ title: 'SKU', data: 'm_product_wx_sku' },
			{ title: 'Produk', data: 'm_product_wx_name' },
			{ title: 'Satuan', data: 'c_uom_wx_name' },
			{ title: 'Jumlah Stock', data: 'm_inventoryline_wx_qtybook' },
			{ title: 'Jumlah Opname', data: 'm_inventoryline_wx_qtycount' }
		];

		if (thisObject.inventoryCMT.m_inventory_wx_docstatus === 'DRAFT') {
			columnsTable.push({ title: 'Action' });
			columnDefDatatable.push({
				targets: -1,
				title: 'Action',
				orderable: false,
				render: function (data, type, full, meta) {

					let htmlbuttonDelete = '';
					if (thisObject.inventoryCMT.m_inventory_wx_docstatus === 'DRAFT') {

						htmlbuttonDelete = `					
						<button id="bttn_row_delete`+ full.m_inventoryline_wx_m_inventoryline_uu + `" value="` + full.m_product_wx_name + `" name="` + full.m_inventoryline_wx_m_inventoryline_uu + `" type="button" class="` + thisObject.classDeleteRow + ` btn btn-outline-hover-info btn-elevate btn-icon"> <i class="la la-trash"></i></button>
						`;
					}


					return htmlbuttonDelete;
				},
			});

			columnDefDatatable.push({
				targets: -2,
				title: 'Jumlah Opname',
				orderable: false,
				render: function (data, type, full, meta) {
					let qtycount = 0;
					if (full.m_inventoryline_wx_qtycount) {
						qtycount = numeral(full.m_inventoryline_wx_qtycount).value();
					}

					let inputQtyCount = new Intl.NumberFormat().format(qtycount);

					if (thisObject.inventoryCMT.m_inventory_wx_docstatus === 'DRAFT') {
						inputQtyCount = `
						<input id="input_`+ full.m_inventoryline_wx_m_inventoryline_uu + `" name="` + full.m_inventoryline_wx_m_inventoryline_uu + `" type="number"   class="` +
							thisObject.classNameFormControl + `" value="` + qtycount + `" form-control" aria-describedby="emailHelp" >
						`;

					}


					return inputQtyCount;
				},
			});



		}





		thisObject.inventorylineService.FindInventorylineCMTListByMInventoryUu(thisObject.inventoryCMT.m_inventory_wx_m_inventory_uu)
			.then(function (inventorylineCMTList) {
				// begin first table
				if (!$.fn.dataTable.isDataTable('#' + thisObject.idTable)) {
					thisObject.datatableReference = table.DataTable({
						searching: false,
						paging: false,
						responsive: true,
						data: inventorylineCMTList,
						scrollX: true,
						columns: columnsTable,
						columnDefs: columnDefDatatable,
						"order": [[1, 'asc']],

					});


				}
				thisObject.EventHandler();
			})


	}

	PostSelectedProductFunction(productCMT) {
		let thisObject = this;

		thisObject.inventorylineService.CreateNewInventoryline(productCMT.m_product_wx_m_product_uu,
			thisObject.inventoryCMT,
			thisObject.datatableReference).then(function (insertSuccess) {

				if (insertSuccess) {
					//$('#' + thisObject.idFormModal).modal('hide');
					thisObject.productSearchDialogBox.HideDialog();
				}

			});



	}



}
