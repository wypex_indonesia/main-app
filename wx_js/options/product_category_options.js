class ProductCategoryOptions {

    constructor(classNameFormControl) {

        this.classNameFormControl = classNameFormControl;
    }

   async Html() {
        let thisObject = this;
        let formGroup = "";
        try {
        	let productCategoryService = new ProductCategoryService();
            let metaDataResponseTableProductCategory = new MetaDataTable();
            metaDataResponseTableProductCategory.ad_org_uu = localStorage.getItem('ad_org_uu');
            let options = await productCategoryService.FindProductCategoryViewModel(metaDataResponseTableProductCategory);
       
            let htmlOptionsUom = '';
            _.forEach(options, function (uomObject) {
                htmlOptionsUom += '<option value="' + uomObject.m_product_category_wx_m_product_category_uu + '">' + uomObject.m_product_category_wx_name + '</option>';
            });

            formGroup = ` 
            <div class="form-group">
				<label for="exampleSelect1">Pilih Kategori Produk</label>
				<select class="`+ thisObject.classNameFormControl + ` product-form-control  form-control" id="m_product_category_wx_m_product_category_uu">`+ htmlOptionsUom + `</select>
            </div>
            `;
            
        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.Html.name, error);
        }

        return formGroup;
    }

}