class ShipperOptions {

    constructor(classNameFormControl, idNameControl) {

        this.classNameFormControl = classNameFormControl;

        this.idNameControl = 'm_shipper_wx_m_shipper_uu';
        if (idNameControl) {

            this.idNameControl = idNameControl;
        }

    }

    async Html() {
        let thisObject = this;
        let formGroup = "";
        try {
           let selectHtml = await thisObject.GenerateControlOptions();

            formGroup = ` 
            <div class="form-group">
				<label for="exampleSelect1">Pilih Kurir</label>`+selectHtml +`				
            </div>
            `;

        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.Html.name, error);
        }

        return formGroup;
    }

    /**
     * DIpisah hanya control options, karena ada page yang membutuhkan hanya GenerateControlOptions
     */
    async GenerateControlOptions() {
        let thisObject = this;
        let selectHtml = '';
        try {
            let shipperService = new ShipperService();

            let metaDataTableShipper = new MetaDataTable();
            metaDataTableShipper.ad_org_uu = localStorage.getItem('ad_org_uu');
    
          
            let options = await  shipperService.FindAll(metaDataTableShipper);
            let htmlOptionsShipper = '';
			_.forEach(options, function (shipperObject) {
				htmlOptionsShipper += '<option value="' + shipperObject.m_shipper_wx_m_shipper_uu + '">' + shipperObject.m_shipper_wx_name + '</option>';
			});


            selectHtml = `             
				<select class="`+ thisObject.classNameFormControl + ` product-form-control  form-control" id="` + thisObject.idNameControl + `">` + htmlOptionsShipper + `</select>
             `;

        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.Html.name, error);
        }

        return selectHtml;

    }

}