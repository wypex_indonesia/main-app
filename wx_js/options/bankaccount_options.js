class BankaccountOptions {

    /**
     * 
     * @param {*} classNameFormControl disamakan dengan classnameformcontrol pada host form, agar dapat diretrieve value nya di form
     * @param {*} idNameControl  optional default  'c_bankaccount_wx_c_bankaccount_uu'
     * @param {*} typebank C = Bank, B = Cash, jika null berarti semua bank dan petty cash
     * @param {*} label Pilih Bank
     */
    constructor(classNameFormControl, idNameControl, typebank, label) {

        this.classNameFormControl = classNameFormControl;
        this.typebank = '';
        this.idNameControl = 'c_bankaccount_wx_c_bankaccount_uu';
        this.label = 'Pilih Bank';

        if (typebank){
            this.typebank = typebank;
        }

        if (label){
            this.label = label;

        }
        if (idNameControl) {

            this.idNameControl = idNameControl;
        }

    }

    async Html() {
        let thisObject = this;
        let formGroup = "";
        try {
            let selectHtml = await thisObject.GenerateControlOptions();

            formGroup = ` 
            <div class="form-group">
				<label for="exampleSelect1">`+thisObject.label+`</label>`+ selectHtml + `				
            </div>
            `;

        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.Html.name, error);
        }

        return formGroup;
    }

    /**
     * DIpisah hanya control options, karena ada page yang membutuhkan hanya GenerateControlOptions
     */
    async GenerateControlOptions() {
        let thisObject = this;
        let selectHtml = '';
        try {
            let bankaccountService = new BankaccountService();
            let metadata = new MetaDataTable();
            metadata.ad_org_uu = localStorage.getItem('ad_org_uu');
            if (thisObject.typebank) {

                metadata.otherFilters.push(' AND c_bankaccount.bankaccounttype = \'' + thisObject.typebank + '\'');
            }

            let options = await bankaccountService.FindAll(metadata);

            let htmlOptionsUom = '';
            _.forEach(options, function (bankaccountObject) {
                htmlOptionsUom += '<option value="' + bankaccountObject.c_bankaccount_wx_c_bankaccount_uu + '">' + bankaccountObject.c_bankaccount_wx_name + '</option>';
            });

            selectHtml = `             
				<select class="`+ thisObject.classNameFormControl + ` kt-selectpicker product-form-control  form-control" id="` + thisObject.idNameControl + `">` + htmlOptionsUom + `</select>
             `;

        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.Html.name, error);
        }

        return selectHtml;

    }

}