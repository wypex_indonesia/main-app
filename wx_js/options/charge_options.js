class ChargeOptions {

    /**
     * 
     * @param {*} classNameFormControl disamakan dengan classnameformcontrol pada host form, agar dapat diretrieve value nya di form
     * @param {*} idNameControl  optional default  'c_bankaccount_wx_c_bankaccount_uu'
    
     */
    constructor(classNameFormControl, idNameControl) {

        this.classNameFormControl = classNameFormControl;
       
        this.idNameControl = 'c_charge_wx_c_charge_uu';
        if (idNameControl) {

            this.idNameControl = idNameControl;
        }

    }

    async Html() {
        let thisObject = this;
        let formGroup = "";
        try {
            let selectHtml = await thisObject.GenerateControlOptions();

            formGroup = ` 
            <div class="form-group">
				<label for="exampleSelect1">Pilih Pengeluaran</label>`+ selectHtml + `				
            </div>
            `;

        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.Html.name, error);
        }

        return formGroup;
    }

    /**
     * DIpisah hanya control options, karena ada page yang membutuhkan hanya GenerateControlOptions
     */
    async GenerateControlOptions() {
        let thisObject = this;
        let selectHtml = '';
        try {
            let chargeService = new ChargeService();
          
            let options = await chargeService.FindAll();

            let htmlOptionsUom = '';
            _.forEach(options, function (chargeObject) {
                htmlOptionsUom += '<option value="' + chargeObject.c_charge_wx_c_charge_uu + '">' + chargeObject.c_charge_wx_name + '</option>';
            });

            selectHtml = `             
				<select class="`+ thisObject.classNameFormControl + ` product-form-control  form-control" id="` + thisObject.idNameControl + `">` + htmlOptionsUom + `</select>
             `;

        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.Html.name, error);
        }

        return selectHtml;

    }

}