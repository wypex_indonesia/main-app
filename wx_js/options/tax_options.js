class TaxOptions {

    constructor(classNameFormControl, idNameControl) {

        this.classNameFormControl = classNameFormControl;

        this.idNameControl = 'c_tax_wx_c_tax_uu';
        if (idNameControl) {

            this.idNameControl = idNameControl;
        }

    }

    async Html() {
        let thisObject = this;
        let formGroup = "";
        try {
           let selectHtml = await thisObject.GenerateControlOptions();

            formGroup = ` 
            <div class="form-group">
				<label for="exampleSelect1">Pilih Tax</label>`+selectHtml +`				
            </div>
            `;

        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.Html.name, error);
        }

        return formGroup;
    }

    /**
     * DIpisah hanya control options, karena ada page yang membutuhkan hanya GenerateControlOptions
     */
    async GenerateControlOptions() {
        let thisObject = this;
        let selectHtml = '';
        try {
            let taxService = new TaxService();

            let metaDataTableShipper = new MetaDataTable();
            metaDataTableShipper.ad_org_uu = localStorage.getItem('ad_org_uu');
    
          
            let options = await  taxService.FindAll(metaDataTableShipper);
            let htmlOptionsTax = '';
            _.forEach(options, function (taxObject, index) {
                let selected = '';
                if (index === 0) {
                    selected = 'selected';
                }
                htmlOptionsTax += '<option value="' + taxObject.c_tax_wx_c_tax_uu + '" data-rate="'+taxObject.c_tax_wx_rate + '" ' + selected + '>' + taxObject.c_tax_wx_name + '</option>';
            });


            selectHtml = `             
				<select class="`+ thisObject.classNameFormControl + ` product-form-control  form-control" id="` + thisObject.idNameControl + `">` + htmlOptionsTax + `</select>
             `;

        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.Html.name, error);
        }

        return selectHtml;

    }

}