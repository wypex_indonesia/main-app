class DoctypeOptions {

    constructor(classNameFormControl, idNameControl, docbaseType, labelPilih) {

        this.docbaseType = docbaseType;
        this.classNameFormControl = classNameFormControl;
        this.labelPilih = 'Tipe Order';

        if (labelPilih){
            this.labelPilih = 'Tipe Order';

        }

        this.idNameControl = 'c_doctype_wx_c_doctype_id';
        if (idNameControl) {

            this.idNameControl = idNameControl;
        }

    }

    async Html() {
        let thisObject = this;
        let formGroup = "";
        try {
           let selectHtml = await thisObject.GenerateControlOptions();

            formGroup = ` 
            <div class="form-group">
				<label for="exampleSelect1">`+thisObject.labelPilih + `</label>`+selectHtml +`				
            </div>
            `;

        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.Html.name, error);
        }

        return formGroup;
    }

    /**
     * DIpisah hanya control options, karena ada page yang membutuhkan hanya GenerateControlOptions
     */
    async GenerateControlOptions() {
        let thisObject = this;
        let selectHtml = '';
        try {
            let doctypeService = new  DoctypeService();
          
            let options = await doctypeService.FindAll(thisObject.docbaseType);
            let htmlOptionsCDoctype = '';
			_.forEach(options, function (doctypeObject) {
				htmlOptionsCDoctype += '<option value="' + doctypeObject.c_doctype_wx_c_doctype_id + '">' + doctypeObject.c_doctype_wx_name + '</option>';
			});
            selectHtml = `             
                <select class="`+ thisObject.classNameFormControl + ` product-form-control  form-control" id="` + thisObject.idNameControl + `">` 
                +
                htmlOptionsCDoctype + `</select>
             `;

        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.Html.name, error);
        }

        return selectHtml;

    }

}