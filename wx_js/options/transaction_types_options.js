class TransactionTypesOptions {

    /**
     * 
     * @param {*} classNameFormControl disamakan dengan classnameformcontrol pada host form, agar dapat diretrieve value nya di form
     * @param {*} idNameControl  optional default  'c_bankaccount_wx_c_bankaccount_uu'
  
     * @param {*} label Pilih Transaksi
     */
    constructor(classNameFormControl, idNameControl, label) {

        this.classNameFormControl = classNameFormControl;

        this.idNameControl = 'transaction_type';
        this.label = 'Pilih Transasi';


        if (label) {
            this.label = label;

        }
        if (idNameControl) {

            this.idNameControl = idNameControl;
        }

    }

    async Html() {
        let thisObject = this;
        let formGroup = "";
        try {
            let selectHtml = await thisObject.GenerateControlOptions();

            formGroup = ` 
            <div class="form-group">
				<label for="exampleSelect1">`+ thisObject.label + `</label>` + selectHtml + `				
            </div>
            `;

        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.Html.name, error);
        }

        return formGroup;
    }

    /**
     * DIpisah hanya control options, karena ada page yang membutuhkan hanya GenerateControlOptions
     */
    async GenerateControlOptions() {
        let thisObject = this;
        let selectHtml = '';
        try {

            let options = [{ transaction_type: 'transfer_inter_bankaccount', transaction_name: 'Transfer Inter Bankaccount' },
            {transaction_type:'manual_journal', transaction_name:'Manual Journal'},
            {transaction_type:'modal_transaction', transaction_name:'Penyetoran/Pengambilan Modal Usaha'}       
        ];

            let htmlOptionsUom = '';
            _.forEach(options, function (transaction_type_object, index) {

                let selected = '';
                if (index === 0){
                    selected =  'selected';
                }
                htmlOptionsUom += '<option value="' + transaction_type_object.transaction_type + '" '+ selected + ' >' + transaction_type_object.transaction_name + '</option>';
            });

            selectHtml = `             
				<select class="`+ thisObject.classNameFormControl + ` kt-selectpicker product-form-control  form-control" id="` + thisObject.idNameControl + `">` + htmlOptionsUom + `</select>
             `;

        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.Html.name, error);
        }

        return selectHtml;

    }

}