class ReportFinancialTypesOptions {

    /**
     * 
     * @param {*} classNameFormControl disamakan dengan classnameformcontrol pada host form, agar dapat diretrieve value nya di form
     * @param {*} idNameControl  optional default  'c_bankaccount_wx_c_bankaccount_uu'
  
     * @param {*} label Pilih Transaksi
     */
    constructor(classNameFormControl, idNameControl, label) {

        this.classNameFormControl = classNameFormControl;

        this.idNameControl = 'report_type';
        this.label = 'Pilih Laporan';


        if (label) {
            this.label = label;

        }
        if (idNameControl) {

            this.idNameControl = idNameControl;
        }

    }

    async Html() {
        let thisObject = this;
        let formGroup = "";
        try {
            let selectHtml = await thisObject.GenerateControlOptions();

            formGroup = ` 
            <div class="form-group">
				<label for="exampleSelect1">`+ thisObject.label + `</label>` + selectHtml + `				
            </div>
            `;

        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.Html.name, error);
        }

        return formGroup;
    }

    /**
     * DIpisah hanya control options, karena ada page yang membutuhkan hanya GenerateControlOptions
     */
    async GenerateControlOptions() {
        let thisObject = this;
        let selectHtml = '';
        try {

            let options = [{ report_type: 'rugi_laba', report_name: 'Rugi Laba' },
            {report_type:'balance_sheet', report_name:'Neraca'}
            //,
           // {report_type:'cash_flow', report_name:'Arus Kas'}       
        ];

            let htmlOptionsUom = '';
            _.forEach(options, function (report, index) {

                let selected = '';
                if (index === 0){
                    selected =  'selected';
                }
                htmlOptionsUom += '<option value="' + report.report_type + '" '+ selected + ' >' + report.report_name + '</option>';
            });

            selectHtml = `             
				<select class="`+ thisObject.classNameFormControl + ` kt-selectpicker product-form-control  form-control" id="` + thisObject.idNameControl + `">` + htmlOptionsUom + `</select>
             `;

        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.Html.name, error);
        }

        return selectHtml;

    }

}