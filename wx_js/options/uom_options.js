class UomOptions {

    constructor(classNameFormControl, idNameControl) {

        this.classNameFormControl = classNameFormControl;

        this.idNameControl = 'c_uom_wx_c_uom_uu';
        if (idNameControl) {

            this.idNameControl = idNameControl;
        }

    }

    async Html() {
        let thisObject = this;
        let formGroup = "";
        try {
           let selectHtml = await thisObject.GenerateControlOptions();

            formGroup = ` 
            <div class="form-group">
				<label for="exampleSelect1">Pilih Satuan</label>`+selectHtml +`				
            </div>
            `;

        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.Html.name, error);
        }

        return formGroup;
    }

    /**
     * DIpisah hanya control options, karena ada page yang membutuhkan hanya GenerateControlOptions
     */
    async GenerateControlOptions() {
        let thisObject = this;
        let selectHtml = '';
        try {
            let uomService = new UomService();
            let metaDataResponseTableUom = new MetaDataTable();
            metaDataResponseTableUom.ad_org_uu = localStorage.getItem('ad_org_uu');
            let options = await uomService.FindUomViewModel(metaDataResponseTableUom);

            let htmlOptionsUom = '';
            _.forEach(options, function (uomObject) {
                htmlOptionsUom += '<option value="' + uomObject.c_uom_wx_c_uom_uu + '">' + uomObject.c_uom_wx_name + '  ' + uomObject.c_uom_wx_uomsymbol + '</option>';
            });

            selectHtml = `             
				<select class="`+ thisObject.classNameFormControl + ` product-form-control  form-control" id="` + thisObject.idNameControl + `">` + htmlOptionsUom + `</select>
             `;

        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.Html.name, error);
        }

        return selectHtml;

    }

}