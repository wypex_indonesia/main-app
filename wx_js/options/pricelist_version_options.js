class PricelistversionOptions {

    constructor(classNameFormControl) {

        this.classNameFormControl = classNameFormControl;
    }

   async Html() {
        let thisObject = this;
        let formGroup = "";
        try {
            let metaDataTable = new MetaDataTable();
           metaDataTable.ad_org_uu = localStorage.getItem('ad_org_uu');
            let pricelistVersionService = new PricelistVersionService();

            let options = await pricelistVersionService.FindAll(metaDataTable);
       
            let htmlOptions = '';
            _.forEach(options, function (pricelistObject) {
                htmlOptions += '<option value="' + pricelistObject.m_pricelist_version_wx_m_pricelist_version_uu + '">' + pricelistObject.m_pricelist_version_wx_name + '</option>';
            });

            formGroup = `
        <div class="form-group">
			<label for="exampleSelect1">Pricelist Version</label>
				<select class="`+ thisObject.classNameFormControl + `  form-control" id="m_pricelist_version_wx_m_pricelist_version_uu">` +
                htmlOptions +
                `</select>
		</div>
        
        `;
        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.Html.name, error);
        }

        return formGroup;
    }

}