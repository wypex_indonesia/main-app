class ProductTypeOptions {

    constructor(classNameFormControl) {

        this.classNameFormControl = classNameFormControl;
    }

   async Html() {
        let thisObject = this;
        let formGroup = "";
        try {
            let 	productService = new ProductService();

            let options = await productService.FindProducttypeViewModel();
       
            let htmlOptions = '';
          
            _.forEach(options, function (producttypeObject) {
                htmlOptions += '<option value="' + producttypeObject.wypex_producttype_wx_producttype + '">' + producttypeObject.wypex_producttype_wx_name + '</option>';
            });
            formGroup = `
            <div class="form-group">
				<label for="exampleSelect1">Tipe Produk</label>
				<select class="`+ thisObject.classNameFormControl + ` product-form-control  form-control" id="wypex_producttype_wx_producttype">`+ htmlOptions + `</select>
				
			</div>        
    
        
        `;
        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.Html.name, error);
        }

        return formGroup;
    }

}