class PricelistOptions {

    constructor(classNameFormControl) {

        this.classNameFormControl = classNameFormControl;
    }

   async Html() {
        let thisObject = this;
        let formGroup = "";
        try {
            let pricelistService = new PricelistService();

            let options = await pricelistService.FindAll();
       
            let htmlOptions = '';
            _.forEach(options, function (pricelistObject) {
                htmlOptions += '<option value="' + pricelistObject.m_pricelist_wx_m_pricelist_uu + '">' + pricelistObject.m_pricelist_wx_name + '</option>';
            });

            formGroup = `
        <div class="form-group">
			<label for="exampleSelect1">Tipe Pricelist</label>
				<select class="`+ thisObject.classNameFormControl + `  form-control" id="m_pricelist_wx_m_pricelist_uu">` +
                htmlOptions +
                `</select>
		</div>
        
        `;
        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.Html.name, error);
        }

        return formGroup;
    }

}