class PettycashPage {

	constructor() {
		this.validation = {

			c_bankaccount_wx_name: {
				required: true
			}

		};
		this.viewModel = new BankaccountViewModel();

		this.metaDataTable = new MetaDataTable();

		this.metaDataTable.ad_org_uu = localStorage.getItem('ad_org_uu');
		this.metaDataTable.otherFilters.push('AND bankaccounttype = \'B\'');
		this.datatableReference = {}; // buat reference datatable

		this.bankaccountService = new BankaccountService();
	}



	Init() {
		try {
			let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery
			let allHtml = `
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__head kt-portlet__head--lg">
				<div  class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
				<i class="kt-font-brand flaticon2-line-chart"></i>
				</span>
				<h3 class="kt-portlet__head-title">
				Akun Petty Cash
				</h3>
				</div>
				<div  class="kt-portlet__head-toolbar">
				<div class="kt-portlet__head-wrapper">
				<div class="kt-portlet__head-actions">
				<button type="button" class="btn btn-bold btn-label-brand btn-sm" name="new_record" data-toggle="modal" data-target="#bankaccount-form">
				<i class="la la-plus"></i>New Record</button>			
				</div>
				</div>
				</div>
				</div>
				<div class="kt-portlet__body" >
				<table class="table table-striped- table-bordered table-hover table-checkable" id="wx_bankaccount_table">
				
				</thead>
				</table>
				</div>
			</div>
			<div class="modal fade" id="bankaccount-form" tabindex="-1" role="dialog" aria-labelledby="bankaccount-form-labelled" style="display: none;" aria-hidden="true">
			
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="bankaccount-form-labelled" >Akun Bank Form</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												</button>
											</div>
											<div class="modal-body">
												<form id="bankaccount-form-validation" class="kt-form">			
						
													<div class="form-group">
														<label>Nama Petty Cash</label>
														<input id="c_bankaccount_wx_name" type="text"   class="bankaccount-form-control form-control" aria-describedby="emailHelp" placeholder="Masukkan nama petty cash">
														<span class="form-text text-muted">Masukkan nama petty cash</span>
													</div>
													<div class="form-group">
														<label>No Akun</label>
														<input id="c_bankaccount_wx_accountno" type="text" value="" class="bankaccount-form-control form-control" aria-describedby="emailHelp" placeholder="Masukkan nomor akun">
														<span class="form-text text-muted">Masukkan nomor akun.</span>
													</div>
													
												</form>
											<div class="modal-footer">
												<button  type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
												<button id='bankaccount-form-submit' type="button" class="btn btn-primary">Save changes</button>
											</div>
										</div>
									</div>
								</div>
							</div>			
			`;


			this.metaDataTable.ad_org_uu = localStorage.getItem('ad_org_uu');

			allHtml += HelperService.HtmlInfoBox();

			$('#wx_fragment_content').html(allHtml);


			var table = $('#wx_bankaccount_table');

			thisObject.bankaccountService.FindAll(thisObject.metaDataTable).then(function (bankaccountCMTList) {
				// begin first table
				thisObject.datatableReference = table.DataTable({
					responsive: true,
					data: bankaccountCMTList,
					scrollX: true,
					columns: [
						{ title: 'Nama Petty Cash', data: 'c_bankaccount_wx_name' },
						{ title: 'Nomor Akun', data: 'c_bankaccount_wx_accountno' },
						{ title: 'Action' }
					],
					columnDefs: [
						{
							targets: -1,
							title: 'Actions',
							orderable: false,
							render: function (data, type, full, meta) {
								return `
					<button id="bttn_row_edit`+ full.c_bankaccount_wx_c_bankaccount_uu + `" name="` + full.c_bankaccount_wx_c_bankaccount_uu + `" type="button" data-toggle="modal" data-target="#bankaccount-form" class="btn btn-outline-hover-info btn-elevate btn-icon"><i class="la la-edit"></i></button>
					<button id="bttn_row_delete`+ full.c_bankaccount_wx_c_bankaccount_uu + `" name="` + full.c_bankaccount_wx_c_bankaccount_uu + `" type="button" data-toggle="modal" data-target="#modal_info_box"   class="wx-row-delete btn btn-outline-hover-info btn-elevate btn-icon"> <i class="la la-trash"></i></button>
					`;
							},
						},
					],

				});

			});


			$('#bankaccount-form-submit').off('click');
			$('#bankaccount-form-submit').on('click', function () {
				thisObject.SaveUpdate();

			});

			$('#bankaccount-form').off('show.bs.modal');
			$('#bankaccount-form').on('show.bs.modal', function (e) {

				HelperService.RemoveInvalidControl('.bankaccount-form-control');
				if (e.relatedTarget.name !== 'new_record') {

					let dataEdited = thisObject.datatableReference.row($('#bttn_row_edit' + e.relatedTarget.name).parents('tr')).data();


					thisObject.viewModel = HelperService.ConvertRowDataToViewModel(dataEdited);

				} else {

					thisObject.viewModel = new BankaccountViewModel();

				}

				HelperService.InjectViewModelToForm('.bankaccount-form-control', thisObject.viewModel);
			});

			$('#modal_info_box').off('show.bs.modal');
			$('#modal_info_box').on('show.bs.modal', function (e) {

				if (e.relatedTarget.name) {

					let dataDeleted = thisObject.datatableReference.row($('#bttn_row_delete' + e.relatedTarget.name).parents('tr')).data();

					let contentModalInfo = "<p>Apakah anda akan menghapus " + dataDeleted.c_bankaccount_wx_name + " ? </p>";
					$("#content_modal_info_box").html(contentModalInfo);
					$("#ok_info_box").attr('name', e.relatedTarget.name);
				}
			});
			$('#ok_info_box').off('click');
			$('#ok_info_box').on('click', function () {
				let uuid = $(this).attr('name');
				let dataDeleted = thisObject.datatableReference.row($('#bttn_row_delete' + uuid).parents('tr')).data();

				let insertSuccess = thisObject.Delete(dataDeleted);

				if (insertSuccess) {
					// delete row pada datatable
					thisObject.datatableReference
						.row($('#bttn_row_delete' + $(this).attr('name')).parents('tr'))
						.remove()
						.draw();
					$('#modal_info_box').modal('hide');

				}

			});


		} catch (error) {
			apiInterface.Log(this.constructor.name, this.Init.name, error);
		}



	}

	/**
	 * 
	
	 */
	SaveUpdate() {
		let thisObject = this;

		try {
			if (HelperService.CheckValidation(thisObject.validation)) {
				$(".bankaccount-form-control").each(function () {
					let id = $(this).attr('id');
					let arrayId = id.split("_wx_");
					thisObject.viewModel[arrayId[0]][arrayId[1]] = $(this).val();	// masukkan value

				});


				const timeNow = new Date().getTime();
				// jika new record maka buat mlocator 	
				// harus dibuat default locator, karena locator akan dipergunakan di inventory movement
				let isNewRecord = false;
				if (!thisObject.viewModel.c_bankaccount.c_bankaccount_uu) { //new record
					isNewRecord = true;
					thisObject.viewModel.c_bankaccount.c_bankaccount_uu = HelperService.UUID();
					thisObject.viewModel.c_bankaccount.ad_client_uu = localStorage.getItem('ad_client_uu');
					thisObject.viewModel.c_bankaccount.ad_org_uu = localStorage.getItem('ad_org_uu');

					thisObject.viewModel.c_bankaccount.createdby = localStorage.getItem('ad_user_uu');
					thisObject.viewModel.c_bankaccount.updatedby = localStorage.getItem('ad_user_uu');

					thisObject.viewModel.c_bankaccount.created = timeNow;
					thisObject.viewModel.c_bankaccount.updated = timeNow;
				} else { //update record


					thisObject.viewModel.c_bankaccount.updatedby = localStorage.getItem('ad_user_uu');

					thisObject.viewModel.c_bankaccount.updated = timeNow;
				}

				thisObject.viewModel.c_bankaccount.sync_client = null;
				thisObject.viewModel.c_bankaccount.processdate = null;

				thisObject.viewModel.c_bankaccount.bankaccounttype = 'B';// for checking account, bank account
				thisObject.viewModel.c_bankaccount.c_bank_id = 1000000; // untuk bank 

				let insertSuccess = 0;
				const sqlInsertBankaccount = HelperService.SqlInsertOrReplaceStatement('c_bankaccount', thisObject.viewModel.c_bankaccount);

				insertSuccess += apiInterface.ExecuteSqlStatement(sqlInsertBankaccount);

				// jika sukses tersimpan 
				if (insertSuccess > 0) {

					let rowData = HelperService.ConvertViewModelToRow(thisObject.viewModel);

					if (!isNewRecord) {

						thisObject.datatableReference.row($('#bttn_row_edit' + rowData['c_bankaccount_wx_c_bankaccount_uu']).parents('tr')).data(rowData).draw();
					} else {

						thisObject.datatableReference.row.add(rowData).draw();
					}


				}
				$('#bankaccount-form').modal('hide');

			}



		} catch (error) {
			apiInterface.Log(this.constructor.name, this.SaveUpdate.name, error);
		}



	}

	/**
	 * Delete pada table, sebenarnya hanya menandai wx_isdeleted = 'Y'
	 * @param {*} dataObject dataObject pada datatable 
	 */
	Delete(dataObject) {
		let insertSuccess = 0;
		let sqlstatementDeleteBankaccount = HelperService.SqlDeleteStatement('c_bankaccount', 'c_bankaccount_uu', dataObject.c_bankaccount_wx_c_bankaccount_uu);
		insertSuccess += apiInterface.ExecuteSqlStatement(sqlstatementDeleteBankaccount);

		return insertSuccess;
	}

}
