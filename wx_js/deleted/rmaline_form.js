class RmalineForm {

	/**
	 * 
	 * @param {RmalinePage} rmalinePage rmalinePageReference

	 */
	constructor(rmalineComponentModel, rmalinePage) {
		this.validation = {
			m_rmaline_wx_qtyentered: {
				required: true
			},


		};


		this.rmalinePage = rmalinePage;

		this.rmaComponentModelTable = rmalinePage.rmaComponentModelTable;


		this.componentModel = rmalineComponentModel;

		this.rmalineService = new RmalineService();
		this.uomService = new UomService();
		this.productService = new ProductService();
		this.taxService = new TaxService();
		this.invoicelineService = new InvoicelineService();
		this.idFormFragmentContent = rmalinePage.idFormFragmentContent;
		this.selectedProduct = {}; // current product yang sedang dipilih, product_viewmodel_table
		this.idFormModal = 'm_rmaline_form_wx_' + this.rmaComponentModelTable.c_invoice_wx_c_invoice_uu;
		this.idFormValidation = 'm_rmaline_form_validation_' + this.rmaComponentModelTable.c_invoice_wx_c_invoice_uu;
		this.classNameFormControl = 'm-rmaline-form-control';
		this.selectorClassNameFormControl = '.' + this.classNameFormControl;


		this.idButtonFormSubmit = 'm_rmaline_form_submit_wx_' + this.rmaComponentModelTable.c_invoice_wx_c_invoice_uu;

		this.table_uomconversion_viewmodel = [];
		this.table_tax_viewmodel = [];
		this.datatableReference = rmalinePage.datatableReference;
		this.rmalinePage = rmalinePage;
		this.inputPriceactual;
		this.inputPriceentered;
		this.inputLinenetamt;
		this.inputTaxamt;
		this.inputLinetotalamt;
		this.inputMovementQty;
		this.ctrlSelectedUom;
		this.ctrlBaseUom;



		this.Init();

	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery



		let allHtml = thisObject.GenerateFormInput();

		$('#' + thisObject.idFormFragmentContent).html(allHtml);
		thisObject.inputPriceactual = $('#' + this.idFormValidation + ' #m_rmaline_wx_priceactual');
		thisObject.inputPriceentered = $('#' + this.idFormValidation + ' #m_rmaline_wx_priceentered');
		thisObject.inputLinenetamt = $('#' + this.idFormValidation + ' #m_rmaline_wx_linenetamt');
		thisObject.inputTaxamt = $('#' + this.idFormValidation + ' #m_rmaline_wx_taxamt');
		thisObject.inputLinetotalamt = $('#' + this.idFormValidation + ' #m_rmaline_wx_linetotalamt');
		thisObject.inputMovementQty = $('#' + this.idFormValidation + ' #m_rmaline_wx_movementqty');
		thisObject.ctrlSelectedUom = $('#' + this.idFormValidation + ' .selected-uom');
		thisObject.ctrlBaseUom = $('#' + this.idFormValidation + ' .base-uom');


		this.PageEventHandler();
		HelperService.InjectViewModelToForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.componentModel);
		thisObject.ctrlSelectedUom.text(thisObject.componentModel.c_uom.name);

	}




	/**
	 * 
	
	 */
	SaveUpdate() {
		let thisObject = this;


		if (HelperService.CheckValidation(thisObject.validation)) {

			thisObject.componentModel = HelperService.GetViewModelFromForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.componentModel);

			const timeNow = new Date().getTime();
			// jika new record maka buat mlocator 	
			// harus dibuat default locator, karena locator akan dipergunakan di inventory movement
			let isNewRecord = false;
			if (!thisObject.componentModel.m_rmaline.m_rmaline_uu) { //new record
				isNewRecord = true;
				thisObject.componentModel.m_rmaline.m_rmaline_uu = HelperService.UUID();
				thisObject.componentModel.m_rmaline.ad_client_uu = localStorage.getItem('ad_client_uu');
				thisObject.componentModel.m_rmaline.ad_org_uu = localStorage.getItem('ad_org_uu');

				thisObject.componentModel.m_rmaline.createdby = localStorage.getItem('ad_user_uu');
				thisObject.componentModel.m_rmaline.updatedby = localStorage.getItem('ad_user_uu');

				thisObject.componentModel.m_rmaline.created = timeNow;
				thisObject.componentModel.m_rmaline.updated = timeNow;
			} else { //update record


				thisObject.componentModel.m_rmaline.updatedby = localStorage.getItem('ad_user_uu');

				thisObject.componentModel.m_rmaline.updated = timeNow;
			}



			thisObject.componentModel.m_rmaline.sync_client = null;
			thisObject.componentModel.m_rmaline.process_date = null;

			let insertSuccess = 0;
			const sqlInsert = HelperService.SqlInsertOrReplaceStatement('m_rmaline', thisObject.componentModel.m_rmaline);

			return new Promise(function (resolve, reject) {
				apiInterface.ExecuteSqlStatement(sqlInsert)
					.then(function (rowChanged) {
						insertSuccess += rowChanged;
						if (insertSuccess > 0) {


							thisObject.rmalineService.FindRmalineComponentModelByMRmalineUu(thisObject.componentModel.m_rmaline.m_rmaline_uu)
								.then(function (rowDataList) {
									let rowData = {};

									if (rowDataList.length > 0) {
										rowData = rowDataList[0];
									}

									thisObject.rmalinePage.datatableReference.row($('#bttn_row_edit' + rowData['m_rmaline_wx_m_rmaline_uu']).parents('tr')).data(rowData).draw();
									thisObject.rmalinePage.PageEventHandler(); // harus ditrigger lagi karena , on click nya seperti hilang, msih tidak tahu kenapa

									$('#' + thisObject.idFormModal).modal('hide');
									resolve(insertSuccess);
								});


						} else {
							resolve(insertSuccess);
						}




					});

				// jika sukses tersimpan 

			});


		}




	}

	GenerateFormInput() {
		let thisObject = this;

		let metaDataTableTax = new MetaDataTable();
		metaDataTableTax.ad_org_uu = localStorage.getItem('ad_org_uu');

		return `<div class="modal fade" id="` + thisObject.idFormModal + `" tabindex="-1" role="dialog" aria-labelledby="product-form-labelled"
		style="display: none;" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="product-form-labelled">Retur line Form</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">`+
			thisObject.GenerateForm() +
			`</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button id="`+ thisObject.idButtonFormSubmit + `" type="button" class="btn btn-primary">Save changes</button>
				</div>
			</div>
		</div>
	</div>`;
	}

	PageEventHandler() {
		let thisObject = this;
		$('#' + thisObject.idButtonFormSubmit).off('click');
		$('#' + thisObject.idButtonFormSubmit).on('click', function () {
			thisObject.SaveUpdate();
		});
		$('#' + thisObject.idFormValidation + ' #m_rmaline_wx_qtyentered').off('change');
		$('#' + thisObject.idFormValidation + ' #m_rmaline_wx_qtyentered').on('change', function () {
			thisObject.calculateQtyBasedUomConversion();

			thisObject.calculateLinenetamt();

			thisObject.calculateLinetaxamt();
			thisObject.calculateLinetotalamt();
		});

		$('.wx-format-money').mask('#,##0', { reverse: true });
		$('#' + thisObject.idFormModal).modal('show');
	}

	/**
	 * Menghitung quantity berdasarkan selectedUom dan cUomConversion.
	 * c_invoice_wx_qtyentered => quantity berdasarkan selectedUom
	 * c_invoice_wx_qtinvoiced => quantity berdasarkan basedUom
	 * 
	 * rumus : m_rmaline_wx_qtyentered/m_rmaline_wx_movementqty =  c_invoice_wx_qtyentered/c_invoice_wx_qtinvoiced
	 * m_rmaline_wx_movementqty = m_rmaline_wx_qtyentered * c_invoice_wx_qtinvoiced/c_invoice_wx_qtyentered
	 */
	calculateQtyBasedUomConversion() {

		let thisObject = this;

		let qtyentered = $('#' + thisObject.idFormValidation + ' #m_rmaline_wx_qtyentered').val();

		let movementQty = qtyentered * thisObject.componentModel.c_invoiceline.qtyinvoiced / thisObject.componentModel.c_invoiceline.qtyentered;

		thisObject.inputMovementQty.val(movementQty);

	}


	/**PriceActual price berdasarkan basedUom, priceactual /uom
		Jadi tidak perlu dihitung lagi. Karena priceactual invoice akan sama dengan priceactual pada rmaline,
		karena priceactual adalah harga dari produk per baseduom_
	
	
	 * Menghitung total jumlah, 
	 */
	calculateLinenetamt() {
		let thisObject = this;
		let priceActual = numeral(thisObject.inputPriceactual.val()).value();
		let movementQty = thisObject.inputMovementQty.val();
		let linenetamt = priceActual * movementQty;
		thisObject.inputLinenetamt.val(linenetamt);

	}


	/**
 * Menghitung total jumlah, 
 */
	calculateLinetaxamt() {
		let thisObject = this;
		let linenetamt = numeral(thisObject.inputLinenetamt.val()).value();


		let rate = thisObject.componentModel.c_tax.rate;
		let linetaxamt = 0;
		if (rate) {
			linetaxamt = rate / 100 * linenetamt;

		}

		thisObject.inputTaxamt.val(linetaxamt);




	}

	calculateLinetotalamt() {
		let thisObject = this;
		let linenetamt = numeral(thisObject.inputLinenetamt.val()).value();
		let taxamt = numeral(thisObject.inputTaxamt.val()).value();

		let linetotalamt = linenetamt + taxamt;
		thisObject.inputLinetotalamt.val(linetotalamt);




	}


	GenerateForm() {
		let thisObject = this;
		return `<form id="` + thisObject.idFormValidation + `" class="kt-form">
		<div class="form-group">
			<label>No</label>
			<input id="m_rmaline_wx_line" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
			<span class="form-text text-muted">Nomor urut</span>
		</div>
		<div class="form-group form-group-marginless">
			<label>Produk</label>
			<input disabled="disabled" type="text" class="` + thisObject.classNameFormControl + ` form-control" id="m_product_wx_name" >					
			
			<input type="hidden" class="` + thisObject.classNameFormControl + `" id="m_product_wx_m_product_uu">
		</div>			
		<div class="form-group">
			<label>Jumlah Produk yang di retur</label>
			<div class="input-group">
				<input id="m_rmaline_wx_qtyentered" type="number"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >				
				<input hidden id="m_rmaline_wx_movementqty" type="number"   class="` + thisObject.classNameFormControl + `" aria-describedby="emailHelp" >				
				<div class="input-group-prepend"><span class="input-group-text selected-uom" ></span></div>
			</div>				
		</div>
		<div class="form-group">
			<label>Jumlah Produk dalam invoice</label>
			<div class="input-group">
				<input id="m_rmaline_wx_qtyinvoiceline" disabled="disabled" class="` + thisObject.classNameFormControl + ` form-control" placeholder="Jumlah Produk dalam invoice" type="number" >
				<div class="input-group-prepend"><span class="input-group-text selected-uom"></span></div>
			</div>				
		</div>
	
			<input type="hidden" id="m_rmaline_wx_priceentered" type="text"   class="` + thisObject.classNameFormControl + ` wx-format-money  form-control" aria-describedby="emailHelp" >
			<input type="hidden" id="m_rmaline_wx_priceactual" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
			<input type="hidden" id="m_rmaline_wx_c_uom_uu" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
			<input type="hidden" class="` + thisObject.classNameFormControl + `" id="c_tax_wx_c_tax_uu">		
			<input type="hidden"  id="m_rmaline_wx_linenetamt" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
			<input type="hidden"  id="m_rmaline_wx_taxamt" type="text"   class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp" >
		
		<div class="form-group">
			<label>Total</label>
			<input disabled="disabled" id="m_rmaline_wx_linetotalamt" type="text"   class="` + thisObject.classNameFormControl + ` wx-format-money form-control" aria-describedby="emailHelp" >
			<span class="form-text text-muted">Jumlah Total</span>
		</div>
	</form>`;


	}




}
