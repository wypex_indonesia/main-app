class RolelineForm {

	/**
	 *	 
	 * @param {RolelineComponentModel} rolelineCM
	 * @param {datatableReference} datatableReference data tabel pada inout page, sebagai reference untuk mengupdate table tersebut	 
	 * 
	 */
	constructor(rolelineCM, datatableReference) {

		this.titleForm = 'Roleline Form';

		this.rolelineCM = rolelineCM;


		this.metaDataTable = new MetaDataTable();

		this.datatableReference = datatableReference; // buat reference datatable


		this.rolelineService = new RolelineService();
		this.roletypeWidget = null;
		
		this.idFormModal = 'wxt_roleline_form_wx_';

		this.selectorClassNameFormControl = '.' + this.classNameFormControl;

		this.idButtonFormSubmit = 'wxt_roleline_form_submit_wx_';

		this.prefixContent = 'roleline_page_'; // prefix content untuk child page 



		this.Init();




	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery

				// nanti class_name yang lain semisal purchase order atau apa, karena setiap 
		// module mungkin beda penanganannya
	
		if (thisObject.rolelineCM.wxt_roleline.class_name === 'SideMenu') {

			thisObject.roletypeWidget = new RoleTypeMenuWidget();

		}

		HelperService.InsertReplaceFormModal(thisObject.idFormModal,thisObject.GenerateFormModalRoleline());
	
		thisObject.roletypeWidget.InjectToForm(thisObject.rolelineCM.wxt_roleline.data);

		this.PageEventHandler();
	}




	/**
	 * 

	 */
	Save() {
		let thisObject = this;

		let insertSuccess = 0;

		insertSuccess = thisObject.roletypeWidget.SaveUpdate(thisObject.rolelineCM, thisObject.datatableReference);
		// jika sukses tersimpan 
		if (insertSuccess > 0) {

			$('#' + thisObject.idFormModal).modal('hide');


		}




	}





	PageEventHandler() {
		let thisObject = this;

		$('#' + thisObject.idButtonFormSubmit).on('click', function () {
			thisObject.Save();

		});


		$('#' + thisObject.idFormModal).modal('show');


	}


	GenerateFormModalRoleline() {
		let thisObject = this;
	
		let htmlForm = 	thisObject.roletypeWidget.GenerateForm();

		return `
	<div class="modal fade" id="` + thisObject.idFormModal + `" tabindex="-1" role="dialog" aria-labelledby="product-form-labelled"
		style="display: none;" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="product-form-labelled">`+ thisObject.titleForm + `</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">`+
				htmlForm+ `

					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button id="`+ thisObject.idButtonFormSubmit + `" type="button" class="btn btn-primary">Save changes</button>
					</div>
				</div>
			</div>
		</div>
	</div>`;
	}



}
