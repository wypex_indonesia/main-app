var apiInterface = {

    ExecuteSqlStatement: function (sqlStatement) {

        let insertSuccess = 0;
        try {
            if (config.build === 'devel') {

                let sessionSqlStatement = sessionStorage.getItem("sqlstatement");
                sessionSqlStatement += '\n' + sqlStatement;
                sessionStorage.setItem("sqlstatement", sessionSqlStatement);

                insertSuccess = dbSqlite.exec(sqlStatement);

                insertSuccess = 1;
            } else {
                insertSuccess = WypexAndroid.ExecuteSqlStatement(sqlStatement);
            }
        } catch (error) {
            apiInterface.Log('ApiInterface', 'ExecuteSqlStatement', error + '/n' + sqlStatement);
        }

        return insertSuccess;
    },

    ExecuteBulkSqlStatement: function (listSqlStatementJson) {
        if (config.build === 'devel') {
            return 1;
        } else {
            return WypexAndroid.ExecuteBulkSqlStatement(JSON.stringify(listSqlStatementJson));
        }
    },

  

    Query: function (metaDataQueryJson) {

        let sqlStatement = this.GetSqlStatement(metaDataQueryJson);
        let returnRows = [];
        try {
            if (config.build === 'devel') {
                console.log(sqlStatement);
                if (dbSqlite) {

                    let stmt = dbSqlite.prepare(sqlStatement);
                    stmt.bind();
                    let rows = [];
                    while (stmt.step()) {
                        let row = stmt.getAsObject();
                        rows.push(row);
                    }
                    returnRows = rows;

                } else {
                    returnRows = metaDataQueryJson.dump;

                }


            } else {
                let result = WypexAndroid.Query(sqlStatement);
                if (result) {
                    returnRows = JSON.parse(result);
                }

            }
        } catch (error) {
            apiInterface.Log('ApiInterface', 'Query', error + '/n' + sqlStatement);

        }

        return returnRows;

    },


    DirectQuerySqlStatement: function (sqlStatement) {
        let returnRows = [];
        try {
            if (config.build === 'devel') {
                let sessionSqlStatement = sessionStorage.getItem("sqlstatement");
                sessionSqlStatement += '\n' + sqlStatement;
                sessionStorage.setItem("sqlstatement", sessionSqlStatement);
                console.log(sqlStatement);
                if (dbSqlite) {

                    let stmt = dbSqlite.prepare(sqlStatement);
                    stmt.bind();
                    let rows = [];
                    while (stmt.step()) {
                        let row = stmt.getAsObject();
                        rows.push(row);
                    }
                    returnRows = rows;

                } else {
                    returnRows = metaDataQueryJson.dump;

                }


            } else {
                let result = WypexAndroid.Query(sqlStatement);
                if (result) {
                    returnRows = JSON.parse(result);
                }

            }
        } catch (error) {
            apiInterface.Log('ApiInterface', 'DirectQuerySqlStatement', error + '/n' + sqlStatement);
        }

        return returnRows;

    },

    GetSqlStatement: function (metaDataQueryJson) {
        let listTemplateColumnAndAlias = [];
        let sqlStatement = '';

        try {
            _.forEach(metaDataQueryJson.metaDataColumnList, function (metacolumn) {

                let arrayString = metacolumn.split('.');
                let tableName = arrayString[0];
                let columnName = arrayString[1];
                let alias = tableName + "_wx_" + columnName;
                listTemplateColumnAndAlias.push(metacolumn + "  " + alias);
                sqlStatement = "SELECT " + listTemplateColumnAndAlias.join(',') + metaDataQueryJson.sqlQueryFrom;
            });
        } catch (error) {
            apiInterface.Log('ApiInterface', 'GetSqlStatement', error + '/n' + sqlStatement);
        }


        return sqlStatement;
    },

    Log: function (className, functionName, message) {
        let errorMessage = 'Time : ' + new Date().getTime() + ' Class : ' + className + ' Function : ' + functionName + ' Message : ' + message;

        console.error(errorMessage);

    }
};

var dbSqlite;
var configSqlite = {
    locateFile: (filename, prefix) => {
        console.log(`prefix is : ${prefix}`);
        return `./vendor_js/${filename}`;
    }
};

initSqlJs(configSqlite).then(function (SQL) {

    var xhr = new XMLHttpRequest();
    xhr.open('GET', './vendor_js/wypex.db', true);
    xhr.responseType = 'arraybuffer';

    xhr.onload = function (e) {
        var uInt8Array = new Uint8Array(this.response);
        dbSqlite = new SQL.Database(uInt8Array);
        let mainPage = new MainPage();

        // contents is now [{columns:['col1','col2',...], values:[[first row], [second row], ...]}]
        //        dbSqlite.exec("INSERT OR REPLACE INTO m_product_bom(m_product_bom_id,ad_client_id,ad_org_id,isactive,created,createdby,updated,updatedby,line,m_product_id,m_productbom_id,bomqty,description,bomtype,m_product_bom_uu,ad_client_uu,ad_org_uu,m_parttype_uu,m_product_uu,m_productbom_uu,wx_isdeleted,process_date,sync_client,wx_updated_on_server) VALUES (null,null,null,null,1582006958852,'65820726-d556-4567-986e-cc485d3ed85d',1582006958852,'65820726-d556-4567-986e-cc485d3ed85d','1',null,null,'12',null,null,'78cc4099-1fd6-47c6-a43c-4e6bc920af60','a38e1948-c85f-449c-908b-99079e373169','49507fb1-815a-4a8b-af56-fc3ab49e51b5',null,'1e4cabb1-be7d-4330-a945-cfa85caf03df',null,'N',null,null,null)");
        //      INSERT OR REPLACE INTO m_product_bom(m_product_bom_id,ad_client_id,ad_org_id,isactive,created,createdby,updated,updatedby,line,m_product_id,m_productbom_id,bomqty,description,bomtype,m_product_bom_uu,ad_client_uu,ad_org_uu,m_parttype_uu,m_product_uu,m_productbom_uu,wx_isdeleted,process_date,sync_client,wx_updated_on_server) VALUES (null,null,null,null,1582008961062,'65820726-d556-4567-986e-cc485d3ed85d',1582008961062,'65820726-d556-4567-986e-cc485d3ed85d','2',null,null,'1',null,null,'497a4297-85fd-4c51-afea-9a4733dbf892','a38e1948-c85f-449c-908b-99079e373169','49507fb1-815a-4a8b-af56-fc3ab49e51b5',null,'1e4cabb1-be7d-4330-a945-cfa85caf03df',null,'N',null,null,null);

        //   var contents = dbSqlite.exec("SELECT * FROM ad_org");
        //  console.dir(contents);
    };
    xhr.send();
});

