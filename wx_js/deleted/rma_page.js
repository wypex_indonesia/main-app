/**
 * Return material dari customer , akan menambah produk pada stock dan product asset
 * material  transaction 
 * factacct transaction
 */
class RmaPage {

	/**
	 * 	
	 * @param {*} idFragmentContent  // IdControl untuk meletakkan View Orderpage ex>. #fragmentContent
	 */
	constructor(idFragmentContent) {

		this.titlePage = 'Retur Customer';


		this.viewModel = new RmaComponentModel();
		this.labelForm = '';

		this.labelVendorOrCustomer = 'Customer';
		this.labelNoOrder = '';
		this.labelNoInout = '';
		this.createNewOrderRetur = false;
		this.idTable = 'wx_rma_table_';
		this.idFragmentContent = idFragmentContent;
		this.rmaService = new RmaService();
		this.metaDataTable = new MetaDataTable();
		this.datatableReference = {}; // buat reference datatable
		this.idShowForm = 'wx-btn-show-form-rma';
		this.idFormFragmentContent = "wx_rma_form_content";
		this.idButtonRowStatusDialog = "bttn_row_status";
		this.idButtonCreateOrderDialog = "bttn_create_order_dialog";
		this.idDialogBox = 'status-dialog-box';
		this.idDialogCreateOrder = 'create-order-dialog-box';
		//	this.idDialogPrint = 'print-dialog-box';
		//	this.idDialogForm = 'm_rma_form_wx_';
		this.labelInvoice = 'No Invoice';// No PO Vendor
		this.labelNoDocument = 'No Dokumen';
		this.prefixContentChild = 'rmaline_page_';


		this.columnsDataTable =
			[
				{
					"className": 'details-control',
					"orderable": false,
					"data": null,
					"defaultContent": '<button  type="button" class="btn btn-outline-hover-info btn-icon"><i class="la la-plus-square-o"></i></button>'
				},
				{ title: this.labelNoDocument, data: 'm_rma_wx_documentno' },
				{ title: this.labelInvoice, data: 'c_invoice_wx_documentno' },
				{ title: this.labelVendorOrCustomer, data: 'c_bpartner_wx_name' },
				{ title: 'Toko/Gudang', data: 'm_warehouse_wx_name' },
				{ title: "Status", data: 'm_rma_wx_docstatus' },
				{ title: 'Action' }
			];

		this.Init();

	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery


		let allHtml = `
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head kt-portlet__head--lg">
			<div  class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
			<i class="kt-font-brand flaticon2-line-chart"></i>
			</span>
			<h3 class="kt-portlet__head-title">
		 	`+ thisObject.titlePage + `
			</h3>
			</div>
			<div  class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-wrapper">
			<div class="kt-portlet__head-actions">
			<button type="button" class="` + thisObject.idShowForm + ` btn btn-bold btn-label-brand btn-sm"  name="new_record" >
			<i class="la la-plus"></i>New Record</button>	
			
			</div>
			</div>
			</div>
			</div>
			<div class="kt-portlet__body" >
			<table class="table table-striped- table-bordered table-hover table-checkable" id="`+ thisObject.idTable + `">
			
			</thead>
			</table>
			<div id="`+ thisObject.idFormFragmentContent + `"></div>
			</div>
		</div>
		`;


		this.metaDataTable.ad_org_uu = localStorage.getItem('ad_org_uu');

		allHtml += HelperService.DialogBox(thisObject.idDialogBox, 'Set Status Dokumen');
		allHtml += HelperService.DialogBox(thisObject.idDialogCreateOrder, 'Buat Retur Sales Order');



		$('#' + thisObject.idFragmentContent).html(allHtml);


		var table = $('#' + thisObject.idTable);
		let columnDefDatatable = [
			{
				targets: -1,
				title: 'Actions',
				orderable: false,
				render: function (data, type, full, meta) {


					let buttonEdit = `<button id="bttn_row_edit` + full.m_rma_wx_m_rma_uu + `" name="` + full.m_rma_wx_m_rma_uu + `" type="button"  class="` + thisObject.idShowForm + ` btn btn-outline-hover-info btn-elevate btn-icon"><i class="la la-edit"></i></button>`;
					let buttonStatus = `<button id="` + thisObject.idButtonRowStatusDialog + full.m_rma_wx_m_rma_uu + `" value="` + full.m_rma_wx_documentno + `" name="` + full.m_rma_wx_m_rma_uu + `" type="button" data-toggle="modal" data-target="#` + thisObject.idDialogBox + `"   class="wx-row-delete btn btn-outline-hover-info btn-elevate btn-icon"> <i class="la la-flag-o"></i></button>`;
					let buttonCreateOrder = `<button id="` + thisObject.idButtonCreateOrderDialog + full.m_rma_wx_m_rma_uu + `" value="` + full.m_rma_wx_documentno + `"  name="` + full.m_rma_wx_m_rma_uu + `" type="button" data-toggle="modal" data-target="#` + thisObject.idDialogCreateOrder + `"   class=" btn btn-outline-hover-info btn-elevate btn-icon"> <i class="la la-cart-plus"></i></button>`;

					// jika 
					let htmlAction = '';
					if (full.m_rma_wx_docstatus === 'CLOSED') {
						htmlAction = '';
					}


					if (full.m_rma_wx_docstatus === 'REVERSED') {
						htmlAction = '';
					}

					if (full.m_rma_wx_docstatus === 'COMPLETED') {
						htmlAction = buttonCreateOrder;
					}
					if (full.m_rma_wx_docstatus === 'DRAFT') {
						htmlAction = buttonEdit + buttonStatus;
					}


					return htmlAction;
				},
			},


		];

		thisObject.rmaService.FindAll(thisObject.metaDataTable).then(function (rmaCMTLIst) {
			thisObject.datatableReference = table.DataTable({
				responsive: true,
				data: rmaCMTLIst,
				scrollX: true,
				columns: thisObject.columnsDataTable,
				columnDefs: columnDefDatatable,
				"order": [[1, 'asc']],
				createdRow: function (row, data, index) {
					if (data.extn === '') {
						var td = $(row).find("td:first");
						td.removeClass('details-control');
					}
				},
				rowCallback: function (row, data, index) {
					//console.log('rowCallback');
				}

			});

			// attach event handler on the page

			thisObject.PageEventHandler();


		});
		// begin first table

	}

	PageEventHandler() {
		let thisObject = this;
		// Add event listener for opening and closing details
		$('#' + thisObject.idTable + ' tbody').off('click', 'td.details-control');
		$('#' + thisObject.idTable + ' tbody').on('click', 'td.details-control', function () {
			var tr = $(this).closest('tr');
			var row = thisObject.datatableReference.row(tr);
			let rmaParent = row.data();
			if (row.child.isShown()) {
				// This row is already open - close it
				row.child.hide();
				$(this).html('<button  type="button" class="btn btn-outline-hover-info btn-icon"><i class="la la-plus-square-o"></i></button>');
			}
			else {
				let idRmadetailContent = thisObject.prefixContentChild + rmaParent.m_rma_wx_m_rma_uu;
				let rmalineFragment = `<div id="` + idRmadetailContent + `"></div>`;
				row.child(rmalineFragment).show();

				// generate orderlinePage
				let rmalinePage = new RmalinePage(rmaParent, idRmadetailContent);
				rmalinePage.Init();


				// add reference to listReferenceOrderlinePage, agar nanti bisa dipanggil lagi di orderpage, untuk membuat

				// change menjadi button minus
				$(this).html('<button  type="button" class="btn btn-outline-hover-info btn-icon"><i class="la la-minus-square-o"></i></button>');

			}

		});

		$('.' + thisObject.idShowForm).off("click");
		$('.' + thisObject.idShowForm).on("click", function (e) {

			let attrName = $(this).attr('name');
			let rmaComponentModel = new RmaComponentModel();
			rmaComponentModel.m_rma.documentno = new Date().getTime();
			rmaComponentModel.m_rma.docstatus = 'DRAFT';
			if (attrName !== 'new_record') {
				let dataEdited = thisObject.datatableReference.row($('#bttn_row_edit' + attrName).parents('tr')).data();
				rmaComponentModel = HelperService.ConvertRowDataToViewModel(dataEdited);
			}
			let rmaForm = new RmaForm(thisObject.idFormFragmentContent, rmaComponentModel, thisObject.datatableReference);

			//$('#' + thisObject.idFormModal).modal('show');

		});


		/**
		 * SET STATUS DOCUMENT DIALOG BOX
		 */
		$('#' + thisObject.idDialogBox).off('show.bs.modal');
		$('#' + thisObject.idDialogBox).on('show.bs.modal', function (e) {

			if (e.relatedTarget.name) {

				let rmaComponentModelTable = thisObject.datatableReference.row($('#' + thisObject.idButtonRowStatusDialog + e.relatedTarget.name).parents('tr')).data();

				let optionsStatus = thisObject.GenerateDocStatusDialogHtml(rmaComponentModelTable);


				//let contentModalInfo = "<p>Apakah anda akan menghapus dokumen nomor " + e.relatedTarget.value + " ? </p>";
				$('#' + thisObject.idDialogBox + " #content_modal_info_box").html(optionsStatus);
				$('#' + thisObject.idDialogBox + " #ok_info_box").attr('name', e.relatedTarget.name);
			}
		});


		/**
		 * Jika docstatus bukan deleted, maka redraw the row pada table
		 * Jika docstatus completed , dan childpage terbuka maka generate childpage lagi
		 */
		$('#' + thisObject.idDialogBox + ' #ok_info_box').off('click');
		$('#' + thisObject.idDialogBox + ' #ok_info_box').on('click', function () {
			let docStatus = $("input[name='" + thisObject.idRadioDocStatus + "']:checked").val();
			let insertSuccess = 0;
			let uuid = $(this).attr('name');
			let rmaComponentModelTable = thisObject.datatableReference.row($('#' + thisObject.idButtonRowStatusDialog + uuid).parents('tr')).data();
			rmaComponentModelTable.m_rma_wx_docstatus = docStatus;
			thisObject.rmaService.UpdateStatus(rmaComponentModelTable)
				.then(function (rowChanged) {
					insertSuccess += rowChanged;
					if (insertSuccess) {
						thisObject.rmaService.FindRmaComponentModelByMRmaUu(rmaComponentModelTable.m_rma_wx_m_rma_uu)
							.then(function (rowDataList) {

								let rowData = {};

								if (rowDataList.length > 0) {
									rowData = rowDataList[0];
									if (docStatus === 'DELETED') {

										thisObject.datatableReference
											.row($('#' + thisObject.idButtonRowStatusDialog + $(this).attr('name')).parents('tr'))
											.remove()
											.draw();
									} else {

										if (docStatus === 'COMPLETED') {
											// cek jika ternyata child orderlinepage terbuka, maka create lagi tablenya
											if ($('#' + thisObject.prefixContentChild + rowData.m_rma_wx_m_rma_uu).length) {

												let rmalinePage = new RmalinePage(rowData, thisObject.prefixContentChild + rowData.m_rma_wx_m_rma_uu);
												rmalinePage.Init();
											}
										}

										thisObject.datatableReference
											.row($('#' + thisObject.idButtonRowStatusDialog + $(this).attr('name')).parents('tr'))
											.data(rowData).draw();

									}
								}


								$('#' + thisObject.idDialogBox).modal('hide');


							});

					}
				});



		});

		$('#' + thisObject.idDialogCreateOrder).off('show.bs.modal');
		$('#' + thisObject.idDialogCreateOrder).on('show.bs.modal', function (e) {
			// set false, untuk function salesOrderPage Init. karena ternyata, salesOrderPage Init(), terbuka dulu sebelum modal hide.
			thisObject.createNewOrderRetur = false;
			if (e.relatedTarget.name) {

				let rmaComponentModelTable = thisObject.datatableReference.row($('#' + thisObject.idButtonCreateOrderDialog + e.relatedTarget.name).parents('tr')).data();

				//let optionsStatus = thisObject.GenerateDocStatusDialogHtml(rmaComponentModelTable);


				let contentModalInfo = "<p>Anda akan membuat dokumen sales order untuk retur barang dari dokumen retur : " + rmaComponentModelTable.m_rma_wx_documentno + " </p>";
				$('#' + thisObject.idDialogCreateOrder + " #content_modal_info_box").html(contentModalInfo);
				$('#' + thisObject.idDialogCreateOrder + " #ok_info_box").attr('name', e.relatedTarget.name);
			}
		});

		/**
		 * Create Order Retur
		 */
		$('#' + thisObject.idDialogCreateOrder + ' #ok_info_box').off('click');
		$('#' + thisObject.idDialogCreateOrder + ' #ok_info_box').on('click', function () {
			let m_rma_uu = $(this).attr('name');
			thisObject.rmaService.FindRmaComponentModelByMRmaUu(m_rma_uu).then(function (rmaCMTList) {
				let rmaCMT = rmaCMTList[0];
				let orderService = new OrderService();
				orderService.CreateNewSalesOrderFromRma(rmaCMT).then(function () {
					$('#' + thisObject.idDialogCreateOrder).modal('hide');
					// open OrderPage
					thisObject.createNewOrderRetur = true;

				})



			});


		});

		$('#' + thisObject.idDialogCreateOrder).off('hidden.bs.modal');
		$('#' + thisObject.idDialogCreateOrder).on('hidden.bs.modal', function (e) {
			if (thisObject.createNewOrderRetur) {
				// open OrderPage
				let saleorderPage = new OrderPage('SO', 'wx_fragment_content');
				saleorderPage.Init();

			}


		});


		thisObject.datatableReference.off('draw');
		thisObject.datatableReference.on('draw', function () {
			thisObject.PageEventHandler();

		});



	}






	/**
	 * Generate document status html content
	 * @param {*} rmaComponentModelTable 
	 */
	GenerateDocStatusDialogHtml(rmaComponentModelTable) {
		let thisObject = this;
		thisObject.idRadioDocStatus = 'm_rma_wx_docstatus_' + rmaComponentModelTable.m_rma_wx_m_rma_uu;

		let optionsStatusHtml = HelperService.GenerateOptionStatusDialogHtml(rmaComponentModelTable.m_rma_wx_docstatus, thisObject.idRadioDocStatus);

		return optionsStatusHtml;


	}










}

