class RmaForm {

	/**
	 * 
	
	 * @param {string} idFragmentContent name id fragment content untuk meletakkan html content dari form_html
	 * @param {RmaComponentModel}
	 * @param {datatableReference} datatableReference data tabel pada inout page, sebagai reference untuk mengupdate table tersebut
	 * @param {c_doctype} inouttype MMS (MM Shipment), MMR (MM Receipt)
	 */
	constructor(idFragmentContent, rmaComponentModel, datatableReference) {

		this.titleForm = 'Retur Barang';

		this.componentModel = rmaComponentModel;

		this.labelVendorOrCustomer = 'Nama Customer';

		this.c_doctype_id = 0;

		//this.labelPoReference = 'No PO Vendor';// No PO Vendor
		this.invoiceReferenceHtml = '';
		this.labelNoDocument = 'No Dokumen';
		this.labelNoInvoice = 'No Invoice';


		this.metaDataTable = new MetaDataTable();

		this.datatableReference = datatableReference; // buat reference datatable

		this.rmaService = new RmaService();

		this.invoiceService = new InvoiceService();
		this.warehouseService = new WarehouseService();
		this.bankaccountService = new BankaccountService();
		this.idFragmentContent = idFragmentContent;
		this.idFormModal = 'm_rma_form_wx_';
		this.idFormValidation = 'm_rma_form_validation_';
		this.classNameFormControl = 'm-rma-form-control';
		this.selectorClassNameFormControl = '.' + this.classNameFormControl;
		this.idDialogBox = 'm_rma_dialog_box_wx_';

		this.idNewRecord = 'new_record_rma_wx_';
		this.idButtonFormSubmit = 'm_rma_form_submit_wx_';
		this.idButtonRowStatusDialog = 'bttn_row_status'; // button row status pada datatable order page
		this.idRadioDocStatus = '';// idGroupRadio Status pada document status dialog
		this.idButtonGenerateItemBarang = 'bttn_generate_rmaline';
		this.prefixContent = 'rmaline_page_'; // prefix content untuk child page 
		//this.labelNoInout = '';
		this.c_doctype_id = 7000; // tukar barang
		this.c_docbasetypeFilterInvoice = 'ARI';

		this.idWidgetSearch = 'rma_form_widget_search';
		this.validation = {

			id_form: this.idFormValidation,


			c_invoice_wx_documentno: {
				required: true
			},

			m_warehouse_wx_name: {
				required: true
			}



		};




		this.Init();


		this.PageEventHandler();

	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery



		let allHtml = thisObject.GenerateFormInput();

		$('#' + thisObject.idFragmentContent).html(allHtml);

		// jika bukan new record, ambil data-data dibawah untuk select options

		HelperService.InjectViewModelToForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.componentModel);

	}




	/**
	 * 
	
	 */
	Save() {
		let thisObject = this;

		let insertSuccess = 0;
		if (HelperService.CheckValidation(thisObject.validation)) {

			thisObject.componentModel = HelperService.GetViewModelFromForm('#' + thisObject.idFormValidation + ' ' + thisObject.selectorClassNameFormControl, thisObject.componentModel);
			thisObject.componentModel.c_doctype.c_doctype_id = thisObject.c_doctype_id;

			insertSuccess = thisObject.rmaService.SaveUpdate(thisObject.componentModel, thisObject.datatableReference);
			// jika sukses tersimpan 
			if (insertSuccess > 0) {

				$('#' + thisObject.idFormModal).modal('hide');


			}

		}


	}

	GenerateFormInput() {
		let thisObject = this;


		return `
	<div class="modal fade" id="` + thisObject.idFormModal + `" tabindex="-1" role="dialog"
		aria-labelledby="product-form-labelled" style="display: none;" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="product-form-labelled">`+ thisObject.titleForm + `</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<form id="`+ thisObject.idFormValidation + `" class="kt-form">
					<div class="modal-body">
	
						<div class="form-group">
							<label>No Dokumen</label>
							<input disabled="disabled" id="m_rma_wx_documentno" type="text"
								class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp">
							<span class="form-text text-muted">No Dokumen dibuat otomatis</span>
						</div>
						<div class="form-group form-group-marginless">
							<label>`+ thisObject.labelNoInvoice + `</label>
	
							<div class="input-group">
								<input disabled="disabled" type="text"
									class="` + thisObject.classNameFormControl + ` form-control"
									id="c_invoice_wx_documentno" placeholder="Cari ` + thisObject.labelNoInvoice + `">
								<div class="input-group-append">
									<span id="search_c_invoice" class="input-group-text"><i class="la la-search"></i></span>
								</div>
							</div>
							<input type="hidden" class="` + thisObject.classNameFormControl + `"
								id="c_invoice_wx_c_invoice_uu">
						</div>
	
						<div class="form-group">
							<label>`+ thisObject.labelVendorOrCustomer + `</label>
							<input disabled="disabled" id="c_bpartner_wx_name" type="text"
								class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp">
							<span class="form-text text-muted"></span>
							<input type="hidden" class="` + thisObject.classNameFormControl + `"
								id="c_bpartner_wx_c_bpartner_uu">
	
						</div>
	
						<div class="form-group form-group-marginless">
							<label>Lokasi Gudang / Toko Retur</label>
							<div class="input-group">
								<input disabled="disabled" type="text"
									class="` + thisObject.classNameFormControl + ` form-control" id="m_warehouse_wx_name"
									placeholder="Cari Gudang / Toko ...">
								<div class="input-group-append">
									<span id="search_m_warehouse" class="input-group-text"><i
											class="la la-search"></i></span>
								</div>
								<input type="hidden" class="` + thisObject.classNameFormControl + `"
									id="m_warehouse_wx_m_warehouse_uu">
							</div>
						</div>
						<div class="form-group">
							<label>Deskripsi</label>
							<input id="m_rma_wx_description" type="text"
								class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp">
							<span class="form-text text-muted">Deskripsi</span>
						</div>
						<div class="form-group">
							<label>Dokumen Status</label>
							<input disabled="disabled" id="m_rma_wx_docstatus" type="text"
								class="` + thisObject.classNameFormControl + ` form-control" aria-describedby="emailHelp">
							<span class="form-text text-muted">Status Dokumen DRAFT / COMPLETED / REVERSED</span>
						</div>
						<div class="form-group" id="`+ thisObject.idWidgetSearch + `">
	
	
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button id="`+ thisObject.idButtonFormSubmit + `" type="submit" class="btn btn-primary">Save
							changes</button>
					</div>
				</form>
	
			</div>
		</div>
	</div>
	
	`;
	}


	PageEventHandler() {
		let thisObject = this;
		$('#' + thisObject.idButtonFormSubmit).off('submit');
		$('#' + thisObject.idButtonFormSubmit).on('submit', function (e) {
			thisObject.Save();
			e.preventDefault();

		});

		/** BEGIN EVENT SEARCH M_Product **************************************/
		$('#' + thisObject.idFormValidation + ' #search_c_invoice').off('click');
		$('#' + thisObject.idFormValidation + ' #search_c_invoice').on('click', function () {

			let functionGetResult = function (invoiceComponentModelTable) {
				$('#' + thisObject.idFormValidation + ' #c_invoice_wx_c_invoice_uu').val(invoiceComponentModelTable.c_invoice_wx_c_invoice_uu);

				$('#' + thisObject.idFormValidation + ' #c_invoice_wx_documentno').val(invoiceComponentModelTable.c_invoice_wx_documentno);

				$('#' + thisObject.idFormValidation + ' #c_bpartner_wx_name').val(invoiceComponentModelTable.c_bpartner_wx_name);

				$('#' + thisObject.idFormValidation + ' #c_bpartner_wx_c_bpartner_uu').val(invoiceComponentModelTable.c_bpartner_wx_c_bpartner_uu);


			};

			let invoice_search_widget = new InvoiceSearchWidget(thisObject.idWidgetSearch, functionGetResult, 'Invoice');
			invoice_search_widget.Init();

		});




		/** BEGIN EVENT SEARCH M_Warehouse **************************************/
		$('#' + thisObject.idFormValidation + ' #search_m_warehouse').off('click');
		$('#' + thisObject.idFormValidation + ' #search_m_warehouse').on('click', function () {
			let functionGetResult = function (mWarehouseSelected) {

				$('#' + thisObject.idFormValidation + ' #m_warehouse_wx_name').val(mWarehouseSelected.m_warehouse_wx_name);
				$('#' + thisObject.idFormValidation + ' #m_warehouse_wx_m_warehouse_uu').val(mWarehouseSelected.m_warehouse_wx_m_warehouse_uu);

			};
			let warehouse_search_widget = new WarehouseSearchWidget(thisObject.idWidgetSearch, functionGetResult);
			warehouse_search_widget.Init();
		});





		/** END EVENT SEARCH M_Warehouse **************************************/



		$('#' + thisObject.idFormModal).modal('show');


	}



}
