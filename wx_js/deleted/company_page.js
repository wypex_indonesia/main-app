class CompanyPage {

	constructor() {
		this.Validation = { ad_org_wx_name: { required: true } };

		this.ViewModel = { ad_org: new AdOrg(), ad_orginfo: new AdOrginfo(), c_location: new CLocation() };


		this.Dump = [{ "ad_org_wx_ad_client_uu": "a38e1948-c85f-449c-908b-99079e373169", "ad_org_wx_ad_org_uu": "57f2bccb-751e-449c-b6f7-5487bd40d302", "ad_org_wx_created": "1580885386889", "ad_org_wx_name": "lapaxa", "ad_org_wx_updated": "1580885462590", "ad_org_wx_updatedby": "65820726-d556-4567-986e-cc485d3ed85d", "ad_org_wx_wx_isdeleted": "N", "c_location_wx_ad_client_uu": "a38e1948-c85f-449c-908b-99079e373169", "c_location_wx_ad_org_uu": "57f2bccb-751e-449c-b6f7-5487bd40d302", "c_location_wx_address1": "Era Mas 2000", "c_location_wx_c_location_uu": "5bc3cf79-c20e-4f10-b66d-28d8e4b55d26", "c_location_wx_city": "213123", "c_location_wx_created": "1580885386889", "c_location_wx_postal": "213123", "c_location_wx_regionname": "121231232", "c_location_wx_updated": "1580885462590", "c_location_wx_updatedby": "65820726-d556-4567-986e-cc485d3ed85d", "c_location_wx_wx_isdeleted": "N", "ad_orginfo_wx_ad_client_uu": "a38e1948-c85f-449c-908b-99079e373169", "ad_orginfo_wx_ad_org_uu": "57f2bccb-751e-449c-b6f7-5487bd40d302", "ad_orginfo_wx_ad_orginfo_uu": "23eb31b2-26e1-4b0a-8f00-83c990635362", "ad_orginfo_wx_c_location_uu": "5bc3cf79-c20e-4f10-b66d-28d8e4b55d26", "ad_orginfo_wx_created": "1580885386889", "ad_orginfo_wx_createdby": "65820726-d556-4567-986e-cc485d3ed85d", "ad_orginfo_wx_email": "231312", "ad_orginfo_wx_fax": "231", "ad_orginfo_wx_phone": "321312", "ad_orginfo_wx_phone2": "3123", "ad_orginfo_wx_taxid": "21312312", "ad_orginfo_wx_updated": "1580885462590", "ad_orginfo_wx_updatedby": "65820726-d556-4567-986e-cc485d3ed85d", "ad_orginfo_wx_wx_isdeleted": "N" }];



	}

	FindAdOrg() {


		const sqlTemplate = '  FROM ad_org ' +
			' INNER JOIN ad_orginfo ON ad_orginfo.ad_org_uu = ad_org.ad_org_uu ' +
			' INNER JOIN c_location ON c_location.c_location_uu = ad_orginfo.c_location_uu ';

		const listMetaColumn = _.union(metaColumn.ad_org, metaColumn.c_location, metaColumn.ad_orginfo);

		let varMetaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, dumpGlobal.Organization);
		//	metaDataQuery.sqlQueryFrom = sqlTemplate;



		//	metaDataQuery.metaDataColumnList = listMetaColumn;



		// 1. request count row
		//	const metaDataQueryCount = new MetaDataQuery();
		//	metaDataQueryCount.sqlQueryFrom = sqlTemplate;

		//	const listMetaColumnCount = [];
		//	listMetaColumnCount.push(new MetaTableTotalColumn('COUNT(m_warehouse.m_warehouse_uu) '));

		//	metaDataQueryCount.metaDataColumnList = listMetaColumnCount;

		// 2. request list row

		return apiInterface.Query(varMetaDataQuery);
	}




	SaveUpdate() {


		let thisObject = this;
		let shipperService = new ShipperService();
		if (HelperService.CheckValidation(this.Validation)) {
			$(".organization-form-control").each(function () {
				let id = $(this).attr('id');
				let arrayId = id.split("_wx_");
				thisObject.ViewModel[arrayId[0]][arrayId[1]] = $(this).val();	// masukkan value

			});

			const timeNow = new Date().getTime();


			let adRoleOwner;
			let adUserRoles;
			let warehouseDefault;
			let cLocationDefault;
			let mLocator;
			let satuanEach;
			let bankDefault;
			let pettyCashDefault;
			let tax10;
			let tax0;
			let isNewRecord = false;
			if (!this.ViewModel.ad_org.ad_org_uu) { //new record
				isNewRecord = true;
				this.ViewModel.ad_org.ad_org_uu = HelperService.UUID();
				this.ViewModel.ad_org.ad_client_uu = localStorage.getItem('ad_client_uu');
				HelperService.Set('ad_org_uu', this.ViewModel.ad_org.ad_org_uu);
				// Create AdRole dalam idempiere sekaligus membuat ad_user_roles, sehingga tidak perlu membuat
				// web service ad_user_roles
				adRoleOwner = new AdRole();

				adRoleOwner.ad_role_uu = HelperService.UUID();
				adRoleOwner.name = this.ViewModel.ad_org.ad_org_uu + '_owner';
				adRoleOwner.ad_org_uu = this.ViewModel.ad_org.ad_org_uu;
				adRoleOwner.ad_client_uu = localStorage.getItem('ad_client_uu');
				adRoleOwner.created = timeNow;
				adRoleOwner.updated = timeNow;
				adRoleOwner.createdby = localStorage.getItem('ad_user_uu');
				adRoleOwner.updatedby = localStorage.getItem('ad_user_uu');

				adUserRoles = new AdUserRoles();
				adUserRoles.ad_user_roles_uu = HelperService.UUID();
				adUserRoles.ad_client_uu = localStorage.getItem('ad_client_uu');
				adUserRoles.ad_org_uu = this.ViewModel.ad_org.ad_org_uu;
				adUserRoles.ad_role_uu = adRoleOwner.ad_role_uu;
				adUserRoles.ad_user_uu = localStorage.getItem('ad_user_uu');
				adUserRoles.created = timeNow;
				adUserRoles.updated = timeNow;
				adUserRoles.createdby = localStorage.getItem('ad_user_uu');
				adUserRoles.updatedby = localStorage.getItem('ad_user_uu');

				// CREATE default warehouse/toko

				warehouseDefault = new MWarehouse();
				warehouseDefault.m_warehouse_uu = HelperService.UUID();
				warehouseDefault.ad_client_uu = localStorage.getItem('ad_client_uu');
				warehouseDefault.ad_org_uu = this.ViewModel.ad_org.ad_org_uu;
				warehouseDefault.name = 'Default Gudang/Toko';
				warehouseDefault.created = timeNow;
				warehouseDefault.updated = timeNow;
				warehouseDefault.createdby = localStorage.getItem('ad_user_uu');
				warehouseDefault.updatedby = localStorage.getItem('ad_user_uu');
				warehouseDefault.sync_client = null;
				warehouseDefault.process_date = null;

				cLocationDefault = JSON.parse(JSON.stringify(this.ViewModel.c_location)); // clone c_location untuk warehouse
				warehouseDefault.c_location_uu = HelperService.UUID();

				mLocator = new MLocator();
				mLocator.m_locator_uu = HelperService.UUID();
				mLocator.value = warehouseDefault.name;
				mLocator.ad_client_uu = localStorage.getItem('ad_client_uu');
				mLocator.ad_org_uu = this.ViewModel.ad_org.ad_org_uu;
				mLocator.m_warehouse_uu = warehouseDefault.m_warehouse_uu;
				mLocator.isdefault = 'Y';
				mLocator.sync_client = null;
				mLocator.process_date = null;
				mLocator.created = timeNow;
				mLocator.updated = timeNow;
				mLocator.createdby = localStorage.getItem('ad_user_uu');
				mLocator.updatedby = localStorage.getItem('ad_user_uu');



				// create satuan 
				satuanEach = new CUom();
				satuanEach.c_uom_uu = HelperService.UUID();
				satuanEach.name = 'Each';
				satuanEach.uomsymbol = 'Ea';
				satuanEach.ad_client_uu = localStorage.getItem('ad_client_uu');
				satuanEach.ad_org_uu = this.ViewModel.ad_org.ad_org_uu;
				satuanEach.sync_client = null;
				satuanEach.process_date = null;
				satuanEach.created = timeNow;
				satuanEach.updated = timeNow;
				satuanEach.createdby = localStorage.getItem('ad_user_uu');
				satuanEach.updatedby = localStorage.getItem('ad_user_uu');



				// create default bankaccount
				bankDefault = new CBankaccount();
				bankDefault.c_bankaccount_uu = HelperService.UUID();
				bankDefault.name = "Bank Mandiri";
				bankDefault.accountno = "xxxxxxxx";

				bankDefault.ad_client_uu = localStorage.getItem('ad_client_uu');
				bankDefault.ad_org_uu = this.ViewModel.ad_org.ad_org_uu;

				bankDefault.createdby = localStorage.getItem('ad_user_uu');
				bankDefault.updatedby = localStorage.getItem('ad_user_uu');

				bankDefault.created = timeNow;
				bankDefault.updated = timeNow;
				bankDefault.sync_client = null;
				bankDefault.process_date = null;

				bankDefault.bankaccounttype = 'C';// for checking account, bank account
				bankDefault.c_bank_id = 1000001; // untuk bank 

				//create default pettyCashDefault
				pettyCashDefault = new CBankaccount();
				pettyCashDefault.c_bankaccount_uu = HelperService.UUID();
				pettyCashDefault.name = "Petty Cash Default";
				pettyCashDefault.accountno = "xxxxxxxx";

				pettyCashDefault.ad_client_uu = localStorage.getItem('ad_client_uu');
				pettyCashDefault.ad_org_uu = this.ViewModel.ad_org.ad_org_uu;

				pettyCashDefault.createdby = localStorage.getItem('ad_user_uu');
				pettyCashDefault.updatedby = localStorage.getItem('ad_user_uu');

				pettyCashDefault.created = timeNow;
				pettyCashDefault.updated = timeNow;
				pettyCashDefault.sync_client = null;
				pettyCashDefault.process_date = null;

				pettyCashDefault.bankaccounttype = 'B';// for checking account, bank account
				pettyCashDefault.c_bank_id = 1000000; // untuk bank 

				//create default tax 10%
				tax10 = new Tax();
				tax10.c_tax_uu = HelperService.UUID();
				tax10.name = "PPN 10%";
				tax10.rate = 10;

				tax10.ad_client_uu = localStorage.getItem('ad_client_uu');
				tax10.ad_org_uu = this.ViewModel.ad_org.ad_org_uu;

				tax10.createdby = localStorage.getItem('ad_user_uu');
				tax10.updatedby = localStorage.getItem('ad_user_uu');

				tax10.created = timeNow;
				tax10.updated = timeNow;
				tax10.sync_client = null;
				tax10.process_date = null;


				//create default tax 0%
				tax0 = new CTax();
				tax0.c_tax_uu = HelperService.UUID();
				tax0.name = "PPN 0%";
				tax0.rate = 0;

				tax0.ad_client_uu = localStorage.getItem('ad_client_uu');
				tax0.ad_org_uu = this.ViewModel.ad_org.ad_org_uu;

				tax0.createdby = localStorage.getItem('ad_user_uu');
				tax0.updatedby = localStorage.getItem('ad_user_uu');

				tax0.created = timeNow;
				tax0.updated = timeNow;
				tax0.sync_client = null;
				tax0.process_date = null;



				shipperService.CreateShipperDefault();

				this.ViewModel.ad_org.created = timeNow;
				this.ViewModel.ad_org.updated = timeNow;
			} else { //update record

				this.ViewModel.ad_org.updatedby = localStorage.getItem('ad_user_uu');
				this.ViewModel.ad_org.updated = timeNow;
			}

			this.ViewModel.ad_org.sync_client = null;
			this.ViewModel.ad_org.process_date = null;


			if (!this.ViewModel.c_location.c_location_uu) { //new record
				this.ViewModel.c_location.c_location_uu = HelperService.UUID();

				this.ViewModel.c_location.updatedby = localStorage.getItem('ad_user_uu');


				this.ViewModel.c_location.created = timeNow;
				this.ViewModel.c_location.updated = timeNow;
			} else { //update record

				this.ViewModel.c_location.updatedby = localStorage.getItem('ad_user_uu');

				this.ViewModel.c_location.updated = timeNow;
			}

			this.ViewModel.c_location.sync_client = null;
			this.ViewModel.c_location.process_date = null;
			this.ViewModel.c_location.ad_org_uu = this.ViewModel.ad_org.ad_org_uu;
			this.ViewModel.c_location.ad_client_uu = this.ViewModel.ad_org.ad_client_uu;

			if (!this.ViewModel.ad_orginfo.ad_orginfo_uu) { //new record
				this.ViewModel.ad_orginfo.ad_orginfo_uu = HelperService.UUID();


				this.ViewModel.ad_orginfo.createdby = localStorage.getItem('ad_user_uu');
				this.ViewModel.ad_orginfo.updatedby = localStorage.getItem('ad_user_uu');


				this.ViewModel.ad_orginfo.created = timeNow;
				this.ViewModel.ad_orginfo.updated = timeNow;
			} else { //update record


				this.ViewModel.ad_orginfo.updatedby = localStorage.getItem('ad_user_uu');


				this.ViewModel.ad_orginfo.updated = timeNow;
			}

			this.ViewModel.ad_orginfo.sync_client = null;
			this.ViewModel.ad_orginfo.process_date = null;
			this.ViewModel.ad_orginfo.ad_org_uu = this.ViewModel.ad_org.ad_org_uu;
			this.ViewModel.ad_orginfo.ad_client_uu = this.ViewModel.ad_org.ad_client_uu;
			this.ViewModel.ad_orginfo.c_location_uu = this.ViewModel.c_location.c_location_uu;

			let insertSuccess = 0;
			const sqlInsertAdOrg = HelperService.SqlInsertOrReplaceStatement('ad_org', this.ViewModel.ad_org, metaColumn.ad_org);

			insertSuccess += apiInterface.ExecuteSqlStatement(sqlInsertAdOrg);

			const sqlInsertLocation = HelperService.SqlInsertOrReplaceStatement('c_location', this.ViewModel.c_location, metaColumn.c_location);
			insertSuccess += apiInterface.ExecuteSqlStatement(sqlInsertLocation);

			const sqlInsertAdOrgInfo = HelperService.SqlInsertOrReplaceStatement('ad_orginfo', this.ViewModel.ad_orginfo, metaColumn.ad_orginfo);
			insertSuccess += apiInterface.ExecuteSqlStatement(sqlInsertAdOrgInfo);

			if ((isNewRecord) && (insertSuccess > 0)) {
				if (adRoleOwner) {
					const sqlInsertAdRole = HelperService.SqlInsertOrReplaceStatement('ad_role', adRoleOwner, metaColumn.ad_role);
					insertSuccess += apiInterface.ExecuteSqlStatement(sqlInsertAdRole);

				}
				if (adUserRoles) {
					const sqlInsertAdUserRole = HelperService.SqlInsertOrReplaceStatement('ad_user_roles', adUserRoles, metaColumn.ad_user_roles);
					insertSuccess += apiInterface.ExecuteSqlStatement(sqlInsertAdUserRole);
				}

				if (warehouseDefault) {
					const sqlInsertWarehouseDefault = HelperService.SqlInsertOrReplaceStatement('m_warehouse', warehouseDefault, metaColumn.m_warehouse);
					insertSuccess += apiInterface.ExecuteSqlStatement(sqlInsertWarehouseDefault);
				}

				if (cLocationDefault) {
					const sqlInsertcLocationDefault = HelperService.SqlInsertOrReplaceStatement('c_location', cLocationDefault, metaColumn.c_location);
					insertSuccess += apiInterface.ExecuteSqlStatement(sqlInsertcLocationDefault);
				}
				if (mLocator) {
					const sqlInsertmLocator = HelperService.SqlInsertOrReplaceStatement('m_locator', mLocator, metaColumn.m_locator);
					insertSuccess += apiInterface.ExecuteSqlStatement(sqlInsertmLocator);
				}

				if (satuanEach) {
					const sqlInsertsatuanEach = HelperService.SqlInsertOrReplaceStatement('c_uom', satuanEach, metaColumn.c_uom);
					insertSuccess += apiInterface.ExecuteSqlStatement(sqlInsertsatuanEach);
				}

				if (bankDefault) {
					const sqlInsertbankDefault = HelperService.SqlInsertOrReplaceStatement('c_bankaccount', bankDefault, metaColumn.c_bankaccount);
					insertSuccess += apiInterface.ExecuteSqlStatement(sqlInsertbankDefault);
				}
				if (pettyCashDefault) {
					const sqlInsertpettyCashDefault = HelperService.SqlInsertOrReplaceStatement('c_bankaccount', pettyCashDefault, metaColumn.c_bankaccount);
					insertSuccess += apiInterface.ExecuteSqlStatement(sqlInsertpettyCashDefault);
				}
				if (tax10) {
					const sqlInsertTax10 = HelperService.SqlInsertOrReplaceStatement('c_tax', tax10);
					insertSuccess += apiInterface.ExecuteSqlStatement(sqlInsertTax10);
				}
				if (tax0) {
					const sqlInsertTax0 = HelperService.SqlInsertOrReplaceStatement('c_tax', tax0);
					insertSuccess += apiInterface.ExecuteSqlStatement(sqlInsertTax0);
				}

			}


			if (insertSuccess > 0) {
				if (isNewRecord) {
					// load menu
					let sidemenu = new SideMenu('div[name="wx_side_menu"]');

					// set config untuk pertama kali
					HelperService.Set('ad_org_uu', this.ViewModel.ad_org.ad_org_uu);
				}
			}



		}

	}


	Init() {
		let thisObject = this;
		let allHtml = `<div class="row">
		<div class="col-md-12">

			<!--begin::Portlet-->
			<div class="kt-portlet">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
						 Profile Perusahaan
						</h3>
					</div>
				</div>

				<!--begin::Form-->
				<form class="kt-form">
					<div class="kt-portlet__body">
						<div class="form-group form-group-last">
							<div class="alert alert-secondary" role="alert">
								<div class="alert-icon"><i class="flaticon-warning kt-font-brand"></i></div>
								<div class="alert-text">
									Masukkan profile perusahaan.
								</div>
							</div>
						</div>
						<div class="form-group">
							<label>Nama Perusahaan</label>
							<input id="ad_org_wx_name" type="text"  class="organization-form-control form-control" aria-describedby="emailHelp" placeholder="Masukkan nama perusahaan">
							<span class="form-text text-muted">Masukkan Nama Perusahaan</span>
						</div>
						<div class="form-group">
						<label>NPWP</label>
						<input id="ad_orginfo_wx_taxid" type="text"  class="organization-form-control  form-control"  aria-describedby="emailHelp" placeholder="Masukkan NPWP jika anda memiliki">
						<span class="form-text text-muted">Masukkan NPWP jika Anda memiliki</span>
						</div>
						<div class="form-group">
						<label>Telephone/Handphone</label>
						<input id="ad_orginfo_wx_phone" type="text"  class="organization-form-control  form-control" aria-describedby="emailHelp" placeholder="Masukkan No handphone / telephone">
						<span class="form-text text-muted">Masukkan No handphone / telephone</span>
						</div>
						<div class="form-group">
						<label>Telephone/Handphone Alternatif</label>
						<input id="ad_orginfo_wx_phone2" type="text"  class="organization-form-control  form-control" aria-describedby="emailHelp" placeholder="Masukkan No handphone / telephone">
						<span class="form-text text-muted">Masukkan No handphone / telephone</span>
						</div>
						<div class="form-group">
						<label>Fax</label>
						<input id="ad_orginfo_wx_fax" type="text"  class="organization-form-control  form-control" aria-describedby="emailHelp" placeholder="Masukkan fax">
						<span class="form-text text-muted">Masukkan no fax</span>
						</div>
						<div class="form-group">
						<label>Email</label>
						<input id="ad_orginfo_wx_email" type="text"  class="organization-form-control  form-control" aria-describedby="emailHelp" placeholder="Masukkan email">
						<span class="form-text text-muted">Masukkan email</span>
						</div>
						<div class="form-group">
						<label>Alamat</label>
						<input id="c_location_wx_address1" type="text"  class="organization-form-control  form-control" aria-describedby="emailHelp" placeholder="Masukkan alamat">
						<span class="form-text text-muted">Masukkan alamat</span>
						</div>
						<div class="form-group">
						<label>Kota</label>
						<input id="c_location_wx_city" type="text"  class="organization-form-control  form-control" aria-describedby="emailHelp" placeholder="Masukkan kota">
						<span class="form-text text-muted">Masukkan kota</span>
						</div>
						<div class="form-group">
						<label>Kode Pos</label>
						<input id="c_location_wx_postal" type="text"  class="organization-form-control  form-control" aria-describedby="emailHelp" placeholder="Masukkan kode pos">
						<span class="form-text text-muted">Masukkan kode pos</span>
						</div>
						<div class="form-group">
						<label>Propinsi</label>
						<input id="c_location_wx_regionname" type="text"  class="organization-form-control  form-control" aria-describedby="emailHelp" placeholder="Masukkan propinsi">
						<span class="form-text text-muted">Masukkan propinsi</span>
						</div>
						
					</div>
					<div class="kt-portlet__foot">
						<div class="kt-form__actions">
							<button id="organization-form-submit" type="button" class="btn btn-primary">Save</button>
							
						</div>
					</div>
				</form>

				<!--end::Form-->
			</div>

			<!--end::Portlet-->

		</div>		
	</div>`;


		$('#wx_fragment_content').html(allHtml);

		thisObject.FindAdOrg().then(function () {
			if (dataAdOrg.length > 0) {
				this.ViewModel = HelperService.ConvertRowDataToViewModel(dataAdOrg[0]);
				HelperService.InjectViewModelToForm('.organization-form-control', this.ViewModel);
			}

		});

		$('#organization-form-submit').off('click');
		$('#organization-form-submit').on('click', function () {
			thisObject.SaveUpdate();

		});





	}



}