var service = {

    /**
     * Create Insert Replace Sql Statement
     * @param {*} tablename 
     * @param {*} data {column: value, column: value}
     * @param {*} metaColum 
     */
    SqlInsertOrReplaceStatement: function (tablename, data) {

        const columns = [];
        const values = [];
        _.forEach(data, (value, column) => {


            if ((value === undefined) || (value === null)) {
                columns.push(column);
                values.push('null');
            } else {
                if (typeof value === 'number') {
                    columns.push(column);
                    values.push(value);
                } else {
                    columns.push(column);
                    values.push('\'' + value + '\'');
                }

            }

        });

        const sqlInsert = 'INSERT OR REPLACE INTO ' + tablename + '(' + columns.join(',') + ') VALUES (' + values.join(',') + ');';
        return sqlInsert;

    },




    /**
 * Update wx_isdeleted Y dengan column reference 
 * m_warehouse_uu
 * @param {*} tablename 
 * @param {*} columnreference misal m_warehouse_uu
 * @param {*} uuid  
 */
    SqlDeleteStatement: function (tablename, columnreference, uuid) {

        const sqlDeleteStatementUpdate = `UPDATE ` + tablename + ` SET wx_isdeleted = 'Y', sync_client = null, process_date = null WHERE ` + columnreference + ` = '` + uuid + `';`;
        return sqlDeleteStatementUpdate;

    },

    

    UUID: function () {

        var dt = new Date().getTime();
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = (dt + Math.random() * 16) % 16 | 0;
            dt = Math.floor(dt / 16);
            return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
        return uuid;

    },

    /**
     * Convert row {nama_table_wx_nama_column : value, ...} menjadi {nameTable:{column:value}...} viewmodel , 
     * @param {*} rowData 
     */
    ConvertRowDataToViewModel(rowData) {

        let viewmodel = {};
        _.forEach(rowData, function (value, key) {

            let table_wx_column = key.split('_wx_');
            let table = table_wx_column[0];
            let column = table_wx_column[1];

            if (!viewmodel[table_wx_column[0]]) {
                viewmodel[table_wx_column[0]] = {};
            }

            viewmodel[table_wx_column[0]][table_wx_column[1]] = value;
            // viewmodel[table_wx_column[0]] = value;
        });
        return viewmodel;

    },

    /**
     * validation untuk form sebelum submit 
     * @param {*} validationObject {

		m_warehouse_wx_name :{
			required : true
		}

	}
  
    CheckValidation(validationObject) {

        let isValid = true;
        _.forEach(validationObject, function (rules, idControl) {

            _.forEach(rules, function (paramValue, paramKey) {

                switch (paramKey) {

                    case 'required':
                        if (paramValue) {
                            // cek jika value tidak ada , maka return false
                            if (!$('#' + idControl).val()) {
                                $('#' + idControl).addClass('is-invalid');
                                isValid = false;
                                $('#' + idControl).get(0).scrollIntoView();
                                return;
                            } else {
                                $('#' + idControl).removeClass('is-invalid');

                            }
                        }
                        break;

                }

            });

        });

        return isValid;

    },*/

    /**
        * validation untuk form sebelum submit 
        * @param {*} validationObject {
           id_form : '',
           m_warehouse_wx_name :{
               required : true
           }
   
       }
        */
    CheckValidation(validationObject) {

        let isValid = true;
        _.forEach(validationObject, function (rules, idControl) {
            let fieldIdControl = '';

            if (validationObject.id_form) {
                fieldIdControl = '#' + validationObject.id_form + ' ';
            }
            fieldIdControl += '#' + idControl + ' ';


            _.forEach(rules, function (paramValue, paramKey) {

                switch (paramKey) {

                    case 'required':
                        if (paramValue) {
                            // cek jika value tidak ada , maka return false
                            if (!$(fieldIdControl).val()) {
                                $(fieldIdControl).addClass('is-invalid');
                                isValid = false;
                                $(fieldIdControl).get(0).scrollIntoView();
                                return;
                            } else {
                                $(fieldIdControl).removeClass('is-invalid');

                            }
                        }
                        break;

                }

            });

        });

        return isValid;

    },

    /**
     * Setelah input invalid , ketika form dibuka lagi, kondisi masih invalid, jadi harus di clear form-control yang invalid
     * @param {*} groupNameControl 
     */
    RemoveInvalidControl(groupNameControl) {
        $(groupNameControl).each(function () {
            $(this).removeClass('is-invalid');
        });
    },

    /**
     * Convert viewmodel menjadi model row seperti database, nama_table_wx_nama_column
     * @param {*} viewmodel 
     */
    ConvertViewModelToRow(viewmodel) {
        let listColumn = {};
        _.forEach(viewmodel, function (model, nameTable) {
            _.forEach(model, function (value, column) {
                let table_wx_column = nameTable + '_wx_' + column;
                listColumn[table_wx_column] = value;

            });
        });
        return listColumn;
    },


    HtmlInfoBox() {
        let htmlBox = `<div class="modal fade" id="modal_info_box" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div id="content_modal_info_box" class="modal-body">
                
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button id="ok_info_box" type="button" class="btn btn-primary">OK</button>
                </div>
            </div>
        </div>
    </div>`;
        return htmlBox;

    },

    DialogBox(idModalDialogBox, modalTitle) {

        if(!modalTitle){
            modalTitle = 'Dialog Box';
        }

        let htmlBox = `<div class="modal fade" id="` + idModalDialogBox + `" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">`+modalTitle+`</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div id="content_modal_info_box" class="modal-body">
                
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button id="ok_info_box" type="button" class="btn btn-primary">OK</button>
                </div>
            </div>
        </div>
    </div>`;
        return htmlBox;

    },


   /* SetAppForConfig() {



        const sqlTemplate = '  FROM ad_user_uu_config_client_app ;';

        const listMetaColumn = _.union(metaColumn.ad_user_uu_config_client_app);

        let varMetaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);

        let configList = apiInterface.Query(varMetaDataQuery);


        _.forEach(configList, function (adUserConfig) {

            if (adUserConfig['ad_user_uu_config_client_app_wx_confname'] === 'APP_FOR') {
                config.app_for = adUserConfig['ad_user_uu_config_client_app_wx_confvalue'];
            }

        });


    },*/

    /**
     * Memasukkan value from database to torm
     * @param {*} classNameControl 
     * @param {*} viewmodel 
     */
    InjectViewModelToForm(classNameControl, viewModel) {

        $(classNameControl).each(function () {
            let id = $(this).attr('id');
            let arrayId = id.split("_wx_");

            if ($(this).attr('type') === "checkbox") {

                if (viewModel[arrayId[0]][arrayId[1]]) {
                    if (viewModel[arrayId[0]][arrayId[1]] === 'Y') {
                        $(this).prop('checked', true);
                    } else {
                        $(this).prop('checked', false);

                    }
                }

            } else {
                $(this).val(viewModel[arrayId[0]][arrayId[1]]);


            }


            // masukkan value

        });
    },

    GetViewModelFromForm(classNameControl, viewModel) {

        $(classNameControl).each(function () {
            let id = $(this).attr('id');
            let arrayId = id.split("_wx_");

            if ($(this).attr('type') === "checkbox") {

                if ($(this).prop('checked')) {
                    viewModel[arrayId[0]][arrayId[1]] = 'Y';
                } else {
                    viewModel[arrayId[0]][arrayId[1]] = 'N';
                }

            } else {

                let value = $(this).val();
                if ($(this).hasClass("wx-format-money")) {
                    value = value.replace(/\,/g, '');

                }

                viewModel[arrayId[0]][arrayId[1]] = value;

            }


            // masukkan value

        });

        return viewModel;
    },

    FormatNumber(numbervalue) {


        numbervalue += '';
        x = numbervalue.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;

    },

    /**
   * Fungsi ini berguna untuk meberikan tablename pada column, sehingga menjadi tablename.columnname
   * @param {*} tableNameAlias
   * @param {[]} columns [columnName, columnName]
   */
    CreateMetaTableColumn(tableNameAlias, columns) {
        let returnColumns = [];
        _.forEach(columns, function (columnName) {

            let aliasObjectColumn = tableNameAlias + "." + columnName;

            returnColumns.push(aliasObjectColumn);

        });
        return returnColumns;
    },

    /**
     * 
     * @param {*} tableNameAlias nama table di database misal c_order
     * @param {*} objectModel cukup object model misal new MWarehouse() 
     */
    CreateMetaColumns(tableNameAlias, objectModel) {
        let returnColumns = [];
        let columns = Object.keys(objectModel);
        _.forEach(columns, function (columnName) {

            let aliasObjectColumn = tableNameAlias + "." + columnName;

            returnColumns.push(aliasObjectColumn);

        });
        return returnColumns;
    },

    /**
     * Convert viewmodel to array metacolumn
     * @param {*} viewModel 
     */
    ConvertViewModelToMetaColumn(viewModel) {

        let listMetaColumn = [];
        _.forEach(viewModel, function (model, property) {
            let metaColumns = service.CreateMetaColumns(property, model);
            listMetaColumn = _.union(metaColumns, listMetaColumn);

        });

        return listMetaColumn;


    }





};










