/**
 * qtyentered = barang sesuai dengan uom yang tertulis
 * qtyinvoiced = barang sesuai dengan uom based uom
 */
class InvoicelinePage {

	/**
	 * 
	 * @param {InvoiceComponentModelTable} invoiceComponentModelTable parent InvoiceComponentModelTable
	 * @param {string} idFragmentContent name id fragment content untuk meletakkan html content dari productbom_page
	 */
	constructor(invoiceComponentModelTable, idFragmentContent) {

		this.invoiceComponentModelTable = invoiceComponentModelTable;


		this.viewModel = new InvoicelineComponentModel();

		this.metaDataTable = new MetaDataTable();


		this.datatableReference = {}; // buat reference datatable

		this.invoicelineService = new InvoicelineService();

		this.invoicelineForm ;

		this.idFragmentContent = idFragmentContent;
		this.idFormModal = 'c_invoiceline_form_wx_' + invoiceComponentModelTable.c_invoice_wx_c_invoice_uu;
		this.idFormValidation = 'c_invoiceline_form_validation_' + invoiceComponentModelTable.c_invoice_wx_c_invoice_uu;

		this.idDialogBox = 'c_invoiceline_dialog_box_wx_' + invoiceComponentModelTable.c_invoice_wx_c_invoice_uu;
		this.idTable = 'wx_invoiceline_table_' + invoiceComponentModelTable.c_invoice_wx_c_invoice_uu;
		this.idBtnGenerateInvoiceline = 'generate_record_invoiceline_wx_' + invoiceComponentModelTable.c_invoice_wx_c_invoice_uu;
		this.idButtonFormSubmit = 'c_invoiceline_form_submit_wx_' + invoiceComponentModelTable.c_invoice_wx_c_invoice_uu;

		this.idFormFragmentContent = "wx_invoiceline_form_content";
		this.idShowFormInvoiceline = 'wx-btn-show-form-invoiceline';
	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery
		

		thisObject.metaDataTable.ad_org_uu = config.ad_org_uu;

		let buttonNewRecordHtml = '';
		if (thisObject.invoiceComponentModelTable.c_invoice_wx_docstatus === 'DRAFT') {
			buttonNewRecordHtml = `<button type="button" class="btn btn-bold btn-label-brand btn-sm"  id="` + thisObject.idBtnGenerateInvoiceline + `"> <i class="la la-plus"></i>Generate Item Barang Dari No ` + thisObject.invoiceComponentModelTable.c_order_wx_documentno + `</button>`;
		}

		let allHtml =
			buttonNewRecordHtml +
			`<table class="table table-striped- table-bordered table-hover table-checkable" id="` + thisObject.idTable + `">			
			</table>
		<div id="`+ thisObject.idFormFragmentContent + `"></div>`;

		allHtml += HelperService.DialogBox(thisObject.idDialogBox);
		

		$('#' + thisObject.idFragmentContent).html(allHtml);
		
		thisObject.CreateDataTable();
		// attach event handler on the page

		thisObject.PageEventHandler();


	}






	/**
	 * Delete pada table, sebenarnya hanya menandai wx_isdeleted = 'Y'
	 * @param {*} dataObject dataObject pada datatable 
	 */
	Delete(dataObject) {
		let insertSuccess = 0;
		let sqlstatementDeleteBankaccount = HelperService.SqlDeleteStatement('c_invoiceline', 'c_invoiceline_uu', dataObject.c_invoiceline_wx_c_invoiceline_uu);
		insertSuccess += apiInterface.ExecuteSqlStatement(sqlstatementDeleteBankaccount);

		return insertSuccess;
	}




	PageEventHandler() {
		let thisObject = this;



		$('#' + thisObject.idBtnGenerateInvoiceline).on("click", function (e) {
			thisObject.invoicelineService.GenerateInvoicelineFromCOrderUu(thisObject.invoiceComponentModelTable).then(function(){

				thisObject.CreateDataTable();
			});
			
		});

		$('.' + thisObject.idShowFormInvoiceline).on("click", function (e) {

			let attrName = $(this).attr('name');
			let invoicelineComponentModel = new InvoicelineComponentModel();
			if (attrName !== 'new_record') {
				let dataEdited = thisObject.datatableReference.row($('#bttn_row_edit' + attrName).parents('tr')).data();
				invoicelineComponentModel = HelperService.ConvertRowDataToViewModel(dataEdited);
			}
			let invoicelineFormPage = new InvoicelineForm(invoicelineComponentModel, thisObject);

			$('#' + thisObject.idFormModal).modal('show');

		});		


		$('#' + thisObject.idDialogBox).on('show.bs.modal', function (e) {

			if (e.relatedTarget.name) {

				let contentModalInfo = "<p>Apakah anda akan menghapus " + e.relatedTarget.value + " ? </p>";
				$('#' + thisObject.idDialogBox + " #content_modal_info_box").html(contentModalInfo);
				$('#' + thisObject.idDialogBox + " #ok_info_box").attr('name', e.relatedTarget.name);
			}
		});

		$('#' + thisObject.idDialogBox + ' #ok_info_box').on('click', function () {
			let uuid = $(this).attr('name');
			let dataDeleted = thisObject.datatableReference.row($('#bttn_row_delete' + uuid).parents('tr')).data();

			let insertSuccess = thisObject.Delete(dataDeleted);

			if (insertSuccess) {
				// delete row pada datatable
				thisObject.datatableReference
					.row($('#bttn_row_delete' + $(this).attr('name')).parents('tr'))
					.remove()
					.draw();
				$('#' + thisObject.idDialogBox).modal('hide');

			}

		});


		$('.wx-format-money').mask('#,##0', { reverse: true });


	}

	CreateDataTable() {

		let thisObject = this;

		thisObject.metaDataTable.otherFilters = [' AND c_invoiceline.c_invoice_uu = \'' + thisObject.invoiceComponentModelTable.c_invoice_wx_c_invoice_uu + '\'']

		var table = $('#' + thisObject.idTable);

		let columnDefDatatable = [];
		let columnsTable = [
			{ title: 'No', data: 'c_invoiceline_wx_line' },
			{ title: 'Produk', data: 'm_product_wx_name' },
			{ title: 'Jumlah' },
			{ title: 'Harga' },
			{ title: 'Total Net' },
			{ title: 'Tax' },
			{ title: 'Total' }
		];


		if (thisObject.invoiceComponentModelTable.c_invoice_wx_docstatus === 'DRAFT') {
			columnsTable.push({ title: 'Action' });
			columnDefDatatable.push({
				targets: -1,
				title: 'Actions',
				orderable: false,
				render: function (data, type, full, meta) {
					return `
					<button id="bttn_row_edit`+ full.c_invoiceline_wx_c_invoiceline_uu + `" name="` + full.c_invoiceline_wx_c_invoiceline_uu + `" type="button"  class="` + thisObject.idShowFormInvoiceline + `   btn btn-outline-hover-info btn-elevate btn-icon" ><i class="la la-edit"></i></button>
					<button id="bttn_row_delete`+ full.c_invoiceline_wx_c_invoiceline_uu + `" value="` + full.m_product_wx_name + `" name="` + full.c_invoiceline_wx_c_invoiceline_uu + `" type="button" data-toggle="modal" data-target="#` + thisObject.idDialogBox + `"   class="wx-row-delete btn btn-outline-hover-info btn-elevate btn-icon"> <i class="la la-trash"></i></button>
					`;
				},
			});



		}

		columnDefDatatable.push({
			targets: 2,
			title: 'Jumlah',
			orderable: false,
			render: function (data, type, full, meta) {
				let qtyentered = new Intl.NumberFormat().format(full.c_invoiceline_wx_qtyentered);
				let uomSelected = full.c_uom_wx_name;
				return qtyentered + ' ' + uomSelected;
			},
		});

		columnDefDatatable.push({
			targets: 3,
			title: 'Harga',
			orderable: false,
			render: function (data, type, full, meta) {
				let priceentered = new Intl.NumberFormat().format(full.c_invoiceline_wx_priceentered);

				return priceentered;
			},
		});

		columnDefDatatable.push({
			targets: 4,
			title: 'Total Net',
			orderable: false,
			render: function (data, type, full, meta) {
				let linenetamt = new Intl.NumberFormat().format(full.c_invoiceline_wx_linenetamt);

				return linenetamt;
			},
		});
		columnDefDatatable.push({
			targets: 5,
			title: 'Tax',
			orderable: false,
			render: function (data, type, full, meta) {
				let taxamt = new Intl.NumberFormat().format(full.c_invoiceline_wx_taxamt);

				return taxamt;
			},
		});

		columnDefDatatable.push({
			targets: 6,
			title: 'Total',
			orderable: false,
			render: function (data, type, full, meta) {
				let totalamt = new Intl.NumberFormat().format(full.c_invoiceline_wx_linetotalamt);

				return totalamt;
			},
		});




		// begin first table
		if (!$.fn.dataTable.isDataTable('#' + thisObject.idTable)) {
			this.datatableReference = table.DataTable({
				searching: false,
				paging: false,
				responsive: true,
				data: thisObject.invoicelineService.FindInvoicelineComponentModelByCInvoiceUu(thisObject.invoiceComponentModelTable.c_invoice_wx_c_invoice_uu),
				scrollX: true,
				columns: columnsTable,
				columnDefs: columnDefDatatable,
				"order": [[1, 'asc']],

			});


		}

	}



}
