/**
 * qty = barang sesuai dengan uom yang tertulis
 * movementqty = barang sesuai dengan uom based uom
 */
class RmalinePage {

	/**
	 * 
	 * @param {rmaComponentModelTable} rmaComponentModelTable parent InvoiceComponentModelTable
	 * @param {string} idFragmentContent name id fragment content untuk meletakkan html content dari productbom_page
	 */
	constructor(rmaComponentModelTable, idFragmentContent) {

		this.rmaComponentModelTable = rmaComponentModelTable;


		this.viewModel = new RmalineComponentModel();

		this.metaDataTable = new MetaDataTable();


		this.datatableReference = {}; // buat reference datatable

		this.rmalineService = new RmalineService();

		//	this.rmalineForm;

		this.idFragmentContent = idFragmentContent;
		this.idFormModal = 'm_rmaline_form_wx_' + rmaComponentModelTable.m_rma_wx_m_rma_uu;
		this.idFormValidation = 'm_rmaline_form_validation_' + rmaComponentModelTable.m_rma_wx_m_rma_uu;

		this.idDialogBox = 'm_rmaline_dialog_box_wx_' + rmaComponentModelTable.m_rma_wx_m_rma_uu;
		this.idTable = 'wx_rmaline_table_' + rmaComponentModelTable.m_rma_wx_m_rma_uu;
		this.idBtnGenerateRmaline = 'generate_record_rmaline_wx_' + rmaComponentModelTable.m_rma_wx_m_rma_uu;
		this.idButtonFormSubmit = 'm_rmaline_form_submit_wx_' + rmaComponentModelTable.m_rma_wx_m_rma_uu;

		this.idFormFragmentContent = "wx_rmaline_form_content";
		this.idShowFormRmaline = 'wx-btn-show-form-rmaline';
	}





	Init() {
		let thisObject = this; // buat reference, karena this dalam jquery berarti element dalam jquery


		thisObject.metaDataTable.ad_org_uu = localStorage.getItem('ad_org_uu');

		let buttonNewRecordHtml = '';
		if (thisObject.rmaComponentModelTable.m_rma_wx_docstatus === 'DRAFT') {
			buttonNewRecordHtml = `<button type="button" class="btn btn-bold btn-label-brand btn-sm"  id="` + thisObject.idBtnGenerateRmaline + `"> <i class="la la-plus"></i>Generate Item Barang Dari No ` + thisObject.rmaComponentModelTable.c_invoice_wx_documentno + `</button>`;
		}

		let allHtml =
			buttonNewRecordHtml +
			`<table class="table table-striped- table-bordered table-hover table-checkable" id="` + thisObject.idTable + `">			
			</table>
		<div id="`+ thisObject.idFormFragmentContent + `"></div>`;

		allHtml += HelperService.DialogBox(thisObject.idDialogBox);


		$('#' + thisObject.idFragmentContent).html(allHtml);


		//	thisObject.rmalineForm = new RmalineForm(thisObject);

		thisObject.CreateDataTable();
		// attach event handler on the page


		thisObject.PageEventHandler();

	}






	/**
	 * Delete pada table, sebenarnya hanya menandai wx_isdeleted = 'Y'
	 * @param {*} dataObject dataObject pada datatable 
	 */
	Delete(dataObject) {
		let insertSuccess = 0;
		let sqlstatementDeleteBankaccount = HelperService.SqlDeleteStatement('m_rmaline', 'm_rmaline_uu', dataObject.m_rmaline_wx_m_rmaline_uu);
		insertSuccess += apiInterface.ExecuteSqlStatement(sqlstatementDeleteBankaccount);

		return insertSuccess;
	}




	PageEventHandler() {
		let thisObject = this;


		$('#' + thisObject.idBtnGenerateRmaline).off("click");
		$('#' + thisObject.idBtnGenerateRmaline).on("click", function (e) {
			thisObject.rmalineService.GenerateRmalineFromCInvoiceUu(thisObject.rmaComponentModelTable)
			.then(function(){

				thisObject.Init();

			});
	

		});
		$('.' + thisObject.idShowFormRmaline).off("click");
		$('.' + thisObject.idShowFormRmaline).on("click", function (e) {

			let attrName = $(this).attr('name');
			let rmalineComponentModel = new RmalineComponentModel();
			if (attrName !== 'new_record') {
				let dataEdited = thisObject.datatableReference.row($('#bttn_row_edit' + attrName).parents('tr')).data();
				rmalineComponentModel = HelperService.ConvertRowDataToViewModel(dataEdited);
			}
			let rmalineFormPage = new RmalineForm(rmalineComponentModel, thisObject);

			$('#' + thisObject.idFormModal).modal('show');

		});
		/*	$('#' + thisObject.idFormModal).off('show.bs.modal');
			$('#' + thisObject.idFormModal).on('show.bs.modal', function (e) {
	
				let attrName = e.relatedTarget.name;
				let rmalineComponentModel = new RmalineComponentModel();
				if (attrName !== 'new_record') {
					let dataEdited = thisObject.datatableReference.row($('#bttn_row_edit' + attrName).parents('tr')).data();
					rmalineComponentModel = HelperService.ConvertRowDataToViewModel(dataEdited);
				}
				thisObject.rmalineForm.BindComponentModel(rmalineComponentModel);
	
				$('#' + thisObject.idFormModal).modal('show');
	
			});	*/

		$('#' + thisObject.idDialogBox).off('show.bs.modal');
		$('#' + thisObject.idDialogBox).on('show.bs.modal', function (e) {

			if (e.relatedTarget.name) {

				let contentModalInfo = "<p>Apakah anda akan menghapus " + e.relatedTarget.value + " ? </p>";
				$('#' + thisObject.idDialogBox + " #content_modal_info_box").html(contentModalInfo);
				$('#' + thisObject.idDialogBox + " #ok_info_box").attr('name', e.relatedTarget.name);
			}
		});

		$('#' + thisObject.idDialogBox + ' #ok_info_box').off("click");
		$('#' + thisObject.idDialogBox + ' #ok_info_box').on('click', function () {
			let uuid = $(this).attr('name');
			let dataDeleted = thisObject.datatableReference.row($('#bttn_row_delete' + uuid).parents('tr')).data();

			let insertSuccess = thisObject.Delete(dataDeleted);

			if (insertSuccess) {
				// delete row pada datatable
				thisObject.datatableReference
					.row($('#bttn_row_delete' + $(this).attr('name')).parents('tr'))
					.remove()
					.draw();
				$('#' + thisObject.idDialogBox).modal('hide');

			}

		});

		thisObject.datatableReference.off('draw');
		thisObject.datatableReference.on('draw', function () {
			thisObject.PageEventHandler();

		});



		$('.wx-format-money').mask('#,##0', { reverse: true });


	}

	CreateDataTable() {

		let thisObject = this;

		thisObject.metaDataTable.otherFilters = [' AND m_rmaline.m_rma_uu = \'' + thisObject.rmaComponentModelTable.m_rma_wx_m_rma_uu + '\''];

		var table = $('#' + thisObject.idTable);

		let columnDefDatatable = [];
		let columnsTable = [
			{ title: 'No', data: 'm_rmaline_wx_line' },
			{ title: 'Produk', data: 'm_product_wx_name' },
			{ title: 'Satuan', data: 'c_uom_wx_name' },
			{ title: 'Jumlah Retur', data: 'm_rmaline_wx_qtyentered' },
			{ title: 'Jumlah Invoice', data: 'm_rmaline_wx_qtyinvoiceline' },
			{ title: 'Total' },

		];
		columnDefDatatable.push({
			targets: 5,
			title: 'Total',
			orderable: false,
			render: function (data, type, full, meta) {
				let totalamt = new Intl.NumberFormat().format(full.m_rmaline_wx_linetotalamt);

				return totalamt;
			},
		});

		if (thisObject.rmaComponentModelTable.m_rma_wx_docstatus === 'DRAFT') {
			columnsTable.push({ title: 'Action' });
			columnDefDatatable.push({
				targets: -1,
				title: 'Actions',
				orderable: false,
				render: function (data, type, full, meta) {
					return `
					<button id="bttn_row_edit`+ full.m_rmaline_wx_m_rmaline_uu + `" name="` + full.m_rmaline_wx_m_rmaline_uu + `" type="button" data-toggle="modal" data-target="#` + thisObject.idFormModal + `"  class="` + thisObject.idShowFormRmaline + `   btn btn-outline-hover-info btn-elevate btn-icon" ><i class="la la-edit"></i></button>
					<button id="bttn_row_delete`+ full.m_rmaline_wx_m_rmaline_uu + `" value="` + full.m_product_wx_name + `" name="` + full.m_rmaline_wx_m_rmaline_uu + `" type="button" data-toggle="modal" data-target="#` + thisObject.idDialogBox + `"   class="wx-row-delete btn btn-outline-hover-info btn-elevate btn-icon"> <i class="la la-trash"></i></button>
					`;
				},
			});



		}




		thisObject.rmalineService.FindRmalineComponentModelByMRmaUu(thisObject.rmaComponentModelTable.m_rma_wx_m_rma_uu)
			.then(function (rmaCMTList) {
				// begin first table
				if (!$.fn.dataTable.isDataTable('#' + thisObject.idTable)) {
					this.datatableReference = table.DataTable({
						searching: false,
						paging: false,
						responsive: true,
						data: rmaCMTList,
						scrollX: true,
						columns: columnsTable,
						columnDefs: columnDefDatatable,
						"order": [[1, 'asc']],

					});


				}


			});


	}



}
