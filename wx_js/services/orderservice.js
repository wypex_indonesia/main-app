
class OrderService {
    constructor() {

    }

    /**
	 * 
	 * @param {*} metaDataResponseTable 
	 */
    FindAll(metaDataResponseTable) {


        let otherFilters = '';
        if (metaDataResponseTable.otherFilters.length > 0) {

            _.forEach(metaDataResponseTable.otherFilters, function (value) {

                otherFilters += ' ' + value + ' ';
            });

        }

        // let metaColumnCOrder = HelperService.CreateMetaTableColumn("c_order", Object.keys(new COrder()));
        //   let metaColumnUomto = HelperService.CreateMetaTableColumn("c_uomto", Object.keys(new CUom()));
        //  let metaColumnCUomConversion = HelperService.CreateMetaTableColumn("c_uom_conversion", Object.keys(new CUomConversion()));

        const sqlTemplate = '  FROM c_order ' +
            ' INNER JOIN c_bpartner ON c_order.c_bpartner_uu = c_bpartner.c_bpartner_uu ' +
            ' INNER JOIN m_warehouse ON c_order.m_warehouse_uu  = m_warehouse.m_warehouse_uu ' +
            ' INNER JOIN c_bankaccount ON c_order.c_bankaccount_uu = c_bankaccount.c_bankaccount_uu ' +
            ' INNER JOIN c_doctype ON c_order.c_doctype_id = c_doctype.c_doctype_id ' +
            ' INNER JOIN c_bpartner_location ON c_bpartner.c_bpartner_uu  = c_bpartner_location.c_bpartner_uu ' +
            ' INNER JOIN m_shipper ON m_shipper.m_shipper_uu  = c_order.m_shipper_uu ' +
            ' WHERE c_order.wx_isdeleted = \'N\' ' + otherFilters + ' ORDER BY c_order.updated ASC ';


        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new OrderViewModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }

    FindOrderViewModelByCOrderUu(c_order_uu) {

        const sqlTemplate = '  FROM c_order ' +
            ' INNER JOIN c_bpartner ON c_order.c_bpartner_uu = c_bpartner.c_bpartner_uu ' +
            ' INNER JOIN m_warehouse ON c_order.m_warehouse_uu  = m_warehouse.m_warehouse_uu ' +
            ' INNER JOIN c_bankaccount ON c_order.c_bankaccount_uu = c_bankaccount.c_bankaccount_uu ' +
            ' INNER JOIN c_doctype ON c_order.c_doctype_id = c_doctype.c_doctype_id ' +
            ' INNER JOIN c_bpartner_location ON c_bpartner.c_bpartner_uu  = c_bpartner_location.c_bpartner_uu ' +
            ' INNER JOIN m_shipper ON m_shipper.m_shipper_uu  = c_order.m_shipper_uu ' +
            ' WHERE c_order.c_order_uu = \'' + c_order_uu + '\'';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new OrderViewModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }

    async  UpdateStatus(orderViewModelTable) {
        let insertSuccess = 0;
        let sqlstatement = '';
        let thisObject = this;
        if (orderViewModelTable.c_order_wx_docstatus === 'DELETED') {
            sqlstatement = HelperService.SqlDeleteStatement('c_order', 'c_order_uu', orderViewModelTable.c_order_wx_c_order_uu);

            return new Promise(function (resolve, reject) {

                apiInterface.ExecuteSqlStatement(sqlstatement).then(function (inserted) {
                    insertSuccess += inserted;
                    resolve(insertSuccess);
                });

            }).then(function (inserted) {

                return insertSuccess;
            });


        } else {
            /// set totallines and grandtotal ketika status completed

            return new Promise(function (resolve, reject) {
                if ((orderViewModelTable.c_order_wx_docstatus === 'COMPLETED') || (orderViewModelTable.c_order_wx_docstatus === 'DRAFT')) {

                    thisObject.CountTotallinesAndGrandTotal(orderViewModelTable.c_order_wx_c_order_uu).then(function (inserted) {
                        insertSuccess += inserted;
                        resolve(insertSuccess);
                    });
                } else {
                    resolve(insertSuccess);
                }


            }).then(function (rowChanged) {

                // let inserted = rowChanged;
                sqlstatement = HelperService.SqlUpdateStatusStatement('c_order', 'c_order_uu', orderViewModelTable.c_order_wx_c_order_uu, orderViewModelTable.c_order_wx_docstatus);
                return new Promise(function (resolve, reject) {
                    apiInterface.ExecuteSqlStatement(sqlstatement).then(function (inserted) {
                        insertSuccess += inserted;
                        resolve(insertSuccess);
                    });

                });


            }).then(function (rowChanged) {
                return new Promise(function (resolve, reject) {
                    thisObject.FindOrderViewModelByCOrderUu(orderViewModelTable.c_order_wx_c_order_uu).then(function (orderCMTList) {

                        resolve(orderCMTList);
                    });

                });


            }).then(function (orderCMTList) {
                let orderCMT = null;

                return new Promise(function (resolve, reject) {
                    if (orderCMTList.length) {
                        orderCMT = orderCMTList[0];
                    }
                    resolve(orderCMT);

                });


            }).then(function (orderCMT) {

                return new Promise(function (resolve, reject) {
                    thisObject.RunProcessDocument(orderCMT).then(function (result) {

                        resolve(result);

                    });

                });

            }).then(function (lastResult) {

                return lastResult;

            });

        }
    }

    /**
     * Dipergunakan ketika closing POS, untuk mendapatkan jumlah sales pada saat opendate
     * @param {*} c_pos_uu 
     * @param {*} open_date 
     * @param {*} close_date
     */
    FindCurrentSalesBankaccountByCPosUu(c_pos_uu, open_date, close_date) {

        const sql = 'SELECT c_bankaccount.name bankaccount, c_bankaccount.c_bankaccount_uu, SUM(c_order.grandtotal) totalsales FROM c_order INNER JOIN c_bankaccount ' +
            'ON c_order.c_bankaccount_uu = c_bankaccount.c_bankaccount_uu WHERE c_order.c_pos_uu = \'' + c_pos_uu + '\' ' + 
            ' WHERE c_order.dateacct >= '+open_date+' AND  c_order.dateacct <= '+ close_date+' GROUP BY bankaccount ;';

        return apiInterface.DirectQuerySqlStatement(sql);

    }

    async CountTotallinesAndGrandTotal(cOrderUu) {
        let setTotallinesStatement = 'UPDATE  c_order SET sync_client = null, process_date = null, totallines =' +
            '(select SUM(linenetamt) FROM c_orderline where c_orderline.wx_isdeleted = \'N\' AND c_orderline.c_order_uu =\'' +
            cOrderUu + '\') WHERE c_order.c_order_uu = \'' + cOrderUu + '\';';

        let setGrandTotalStatement = 'UPDATE c_order SET sync_client = null, process_date = null, grandtotal =' +
            '(select SUM(linenetamt + linetaxamt) FROM c_orderline where c_orderline.wx_isdeleted = \'N\' AND c_orderline.c_order_uu =\'' +
            cOrderUu + '\') WHERE c_order.c_order_uu = \'' + cOrderUu + '\';';


        let setTotalTaxLines = 'UPDATE c_order SET sync_client = null, process_date = null, totaltaxlines =' +
            '(select SUM(linetaxamt) FROM c_orderline where c_orderline.wx_isdeleted = \'N\' AND c_orderline.c_order_uu =\'' +
            cOrderUu + '\') WHERE c_order.c_order_uu = \'' + cOrderUu + '\';';


        //let insertSuccess = 0;


        let listSqlStatement = [setTotallinesStatement, setGrandTotalStatement, setTotalTaxLines];

        return apiInterface.ExecuteBulkSqlStatement(listSqlStatement);

    }

    /**
     * Run Process Document sesuai dengan docstatus :
     * 1. Draft -> Tidak ada Action
     * 2. COMPLETED -> with direct order 
     *  2.1. Create MInout 
     *  2.2. Create MInoutline
     *  2.3. Create MInvoice
     *  2.4. Create MInvoiceline
     * 3. COMPLETED -> with PO tidak ada action 
     * @param {*} orderViewModelTable 
     */
    async RunProcessDocument(orderViewModelTable) {
        let thisObject = this;
        let factacctService = new FactAcctService();
        let elementValueService = new ElementValueService();
        let costService = new CostService();
        let materialTransaction = new MaterialTransactionService();
        let locatorService = new LocatorService();

        let locatorCMTList = await locatorService.FindLocatorComponentModelByMWarehouseUu(orderViewModelTable.c_order_wx_m_warehouse_uu);
        let locatorCMT = locatorCMTList[0];


        try {
            if (orderViewModelTable.c_order_wx_docstatus === 'COMPLETED') {


                // pembelian langsung
                if (orderViewModelTable.c_order_wx_c_doctype_id === 1000) {

                    return thisObject.DirectPurchaseOrder(orderViewModelTable);


                } else if (orderViewModelTable.c_order_wx_c_doctype_id === 1001) {
                    // pembelian dengan PO
                    return new Promise(function (resolve, reject) {
                        resolve(1);// kaerna pembelian dengan PO tidak ada process document

                    });

                    // Penjualan langsung 2000 dan Penjualan Langsung Retur 2002
                } else if ((orderViewModelTable.c_order_wx_c_doctype_id === 2000) || (orderViewModelTable.c_order_wx_c_doctype_id === 2002)) {

                    return thisObject.DirectSalesOrder(orderViewModelTable);
                } else {
                    return new Promise(function (resolve, reject) {
                        resolve(1);

                    });

                } /*else if () {
                    // Penjualan dengan retur 
                    /**
                     * Factacct
                     * c_orderline	51100 – CoGS Product	14120-Product Asset
                        c_order	    (DR)12110 – Accounts Receivable Trade (Grand Total)	
                        c_orderline	(CR) 41000 – Trade Revenue (linenetamt)
                        c_orderline	(CR) 21610 – Tax Due (taxamt)
                        c_order	    11100 – Checking In Transfer	11130 - Checking Unallocated Receipt
                        c_bankstatementline	11100 – Checking Account	111110 – Checking In Transfer
                    *
                    Material Trasaction
                    Customer Return Material	Plus	C_orderline
                    Customer Out	Minus	C_orderline
     
                     * 
                     */


                //1. Materialtransaction 
                //2. Factacct dari Invoice sampai payment
                //3 Buat bankstatement dan bankstatementline
                //4 Buat factacct bankstatementline

                //   c_order	    12110 – Accounts Receivable Trade (Grand Total)	
                // insertSuccess += await factacctService.sqlInsertFactAcct(elementValueService.ElementValueList().AccountReceivableTrade, 'c_order',
                //    orderViewModelTable.c_order_wx_c_order_uu, orderViewModelTable.c_order_wx_grandtotal, null);
                //   c_order	    11100 – Checking In Transfer	11130 - Checking Unallocated Receipt
                //DEBIT
                //insertSuccess += await factacctService.sqlInsertFactAcct(elementValueService.ElementValueList().CheckingInTransfer, 'c_order',
                //    orderViewModelTable.c_order_wx_c_order_uu, orderViewModelTable.c_order_wx_grandtotal, null);
                //CREDIT
                //insertSuccess += await factacctService.sqlInsertFactAcct(elementValueService.ElementValueList().CheckingUnallocatedReceipt, 'c_order',
                //    orderViewModelTable.c_order_wx_c_order_uu, null, orderViewModelTable.c_order_wx_grandtotal);


                /*let orderlineService = new OrderlineService();
                orderlineService.FindOrderlineViewModelByCOrderUu(orderViewModelTable.c_order_wx_c_order_uu).then(function (orderlineCMTList) {

                    _.forEach(orderlineCMTList, function (orderlineCMT) {
                        // c_orderline	51100 – CoGS Product	14120-Product Asset
                        //find costprice
                        costService.GetProductAsset(orderlineCMT.c_orderline_wx_m_product_uu,
                            elementValueService.CostElement().LastPO, orderlineCMT.c_orderline_wx_qtyordered).then(function (productAsset) {

                                let tradeRevenue = orderlineCMT.c_orderline_wx_linenetamt;
                                let taxDue = orderlineCMT.orderline_wx_linetaxamt;
                                let movementType;
                                let isOut = true;
                                if (orderlineCMT.c_orderline_wx_isreturn === 'Y') {
                                    isOut = false;
                                    //tradeRevenue dibuat positif, karena negatif karena retur
                                    if (tradeRevenue < 0) {
                                        tradeRevenue = tradeRevenue * -1;
                                    }

                                    if (taxDue < 0) {
                                        taxDue = taxDue * -1;
                                    }
                                    movementType = elementValueService.MovementType().RmaReceipt;

                                    //DR
                                    factacctService.sqlInsertFactAcct(elementValueService.ElementValueList().ProductAsset, 'c_orderline',
                                        orderlineCMT.c_orderline_wx_c_orderline_uu, productAsset, null);

                                    // c_orderline	(CR) 41000 – Trade Revenue (linenetamt)
                                    // c_orderline	(CR) 21610 – Tax Due (taxamt)
                                    factacctService.sqlInsertFactAcct(elementValueService.ElementValueList().TradeRevenue, 'c_orderline',
                                        orderlineCMT.c_orderline_wx_c_orderline_uu, tradeRevenue, null);
                                    if (taxDue) {
                                        factacctService.sqlInsertFactAcct(elementValueService.ElementValueList().TaxDue, 'c_orderline',
                                            orderlineCMT.c_orderline_wx_c_orderline_uu, taxDue, null);
                                    }
                                    //CR                   

                                    factacctService.sqlInsertFactAcct(elementValueService.ElementValueList().CoGSProduct, 'c_orderline',
                                        orderlineCMT.c_orderline_wx_c_orderline_uu, null, productAsset);

                                } else {
                                    // bukan retur 
                                    isOut = true;
                                    movementType = elementValueService.MovementType().CustomerOut;
                                    //DR
                                    factacctService.sqlInsertFactAcct(elementValueService.ElementValueList().CoGSProduct, 'c_orderline',
                                        orderlineCMT.c_orderline_wx_c_orderline_uu, productAsset, null);
                                    //CR                   
                                    factacctService.sqlInsertFactAcct(elementValueService.ElementValueList().ProductAsset, 'c_orderline',
                                        orderlineCMT.c_orderline_wx_c_orderline_uu, null, productAsset);

                                    // c_orderline	(CR) 41000 – Trade Revenue (linenetamt)
                                    // c_orderline	(CR) 21610 – Tax Due (taxamt)
                                    factacctService.sqlInsertFactAcct(elementValueService.ElementValueList().TradeRevenue, 'c_orderline',
                                        orderlineCMT.c_orderline_wx_c_orderline_uu, null, tradeRevenue);
                                    if (taxDue) {
                                        factacctService.sqlInsertFactAcct(elementValueService.ElementValueList().TaxDue, 'c_orderline',
                                            orderlineCMT.c_orderline_wx_c_orderline_uu, null, taxDue);
                                    }



                                }


                                materialTransaction.sqlInsertMTransaction(movementType, locatorCMT.m_locator_wx_m_locator_uu, 'c_orderline', orderlineCMT.c_orderline_wx_c_orderline_uu, orderlineCMT.c_orderline_wx_m_product_uu, orderlineCMT.c_orderline_wx_qtyordered, isOut);



                            });


                    });

                });


                let bankstatementService = new BankstatementService();
                let bankstatementlineService = new BankstatementlineService();

                let bankstatement = new CBankstatement();

                bankstatement.c_bankstatement_uu = HelperService.UUID();
                bankstatement.c_bankaccount_uu = orderViewModelTable.c_order_wx_c_bankaccount_uu;
                bankstatement.docstatus = 'COMPLETED';
                bankstatementService.SaveUpdate(bankstatement);

                let bankstatementline = new CBankstatementline();
                bankstatementline.c_bankstatementline_uu = HelperService.UUID();
                bankstatementline.c_bankstatement_uu = bankstatement.c_bankstatement_uu;
                bankstatementline.c_order_uu = orderViewModelTable.c_order_wx_c_order_uu;

                bankstatementline.stmtamt = orderViewModelTable.c_order_wx_grandtotal;

                bankstatementlineService.SaveUpdate(bankstatementline);

                bankstatementService.RunProcessDocument(bankstatement.c_bankstatement_uu, orderViewModelTable.c_order_wx_c_doctype_id, bankstatement.docstatus);
            }*/

            } else if (orderViewModelTable.c_order_wx_docstatus === 'REVERSED') {

                // pembelian langsung 1000 // 2000 penjualan langsung
                if ((orderViewModelTable.c_order_wx_c_doctype_id === 1000) || (orderViewModelTable.c_order_wx_c_doctype_id === 2000)) {
                    return thisObject.DirectReversedOrder(orderViewModelTable);

                } else {

                    return new Promise(function (resolve, reject) {
                        resolve(1);

                    });
                }


            } else {
                // untuk document draft dan closed
                return new Promise(function (resolve, reject) {

                    resolve(1);

                });
            }
        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.RunProcessDocument.name, error);
        }









    }





    DirectReversedOrder(orderCMT) {

        let inoutService = new InoutService();
        let invoiceService = new InvoiceService();
        let paymentService = new PaymentService();
        let insertSuccess = 0;
        let dataflow = {};

        try {
            return new Promise(function (resolve, reject) {
                Promise.all([inoutService.FindInoutViewModelByCOrderUu(orderCMT.c_order_wx_c_order_uu),
                invoiceService.FindInvoiceComponentModelByCOrderUu(orderCMT.c_order_wx_c_order_uu),
                paymentService.FindPaymentComponentModelByCOrderUu(orderCMT.c_order_wx_c_order_uu)])
                    .then(function (resultArray) {


                        let inoutCMTList = resultArray[0];
                        let inoutCMT = inoutCMTList[0];
                        inoutCMT.m_inout_wx_docstatus = 'REVERSED';

                        dataflow.inout_cmt = inoutCMT;

                        let invoiceCMTList = resultArray[1];
                        let invoiceCMT = invoiceCMTList[0];
                        invoiceCMT.c_invoice_wx_docstatus = 'REVERSED';
                        dataflow.invoice_cmt = invoiceCMT;

                        let paymentCMTList = resultArray[2];
                        let paymentCMT = paymentCMTList[0];
                        paymentCMT.c_payment_wx_docstatus = 'REVERSED';

                        dataflow.payment_cmt = paymentCMT;

                        resolve(dataflow);

                    });


            }).then(function (dataflow) {

                return new Promise(function (resolve, reject) {

                    inoutService.UpdateStatus(dataflow.inout_cmt).then(function (inserted) {

                        insertSuccess += inserted;
                        resolve(dataflow);
                    });

                });

            }).then(function (dataflow) {

                return new Promise(function (resolve, reject) {

                    invoiceService.UpdateStatus(dataflow.invoice_cmt).then(function (inserted) {

                        insertSuccess += inserted;
                        resolve(dataflow);
                    });



                });

            }).then(function (dataflow) {

                return new Promise(function (resolve, reject) {

                    paymentService.UpdateStatus(dataflow.payment_cmt).then(function (inserted) {

                        insertSuccess += inserted;
                        resolve(insertSuccess);
                    });

                });

            });
        } catch (error) {
            apiInterface.Log(this.constructor.name, this.DirectReversedOrder.name, error);
            return new Promise(function (resolve, reject) {
                resolve(insertSuccess);
            });
        }





    }

    async  DirectSalesOrder(orderCMT) {
        let inout_doctype_id = 3000;
        let invoice_doc_type_id = 6000;
        let payment_doc_type_id = 6001;
        return this.DirectOrder(orderCMT, inout_doctype_id, invoice_doc_type_id, payment_doc_type_id);

    }

    async  DirectPurchaseOrder(orderCMT) {
        let inout_doctype_id = 4000;
        let invoice_doc_type_id = 5000;
        let payment_doc_type_id = 5001;
        return this.DirectOrder(orderCMT, inout_doctype_id, invoice_doc_type_id, payment_doc_type_id);
    }

    DirectOrder(orderCMT, inout_doctype_id, invoice_doc_type_id, payment_doc_type_id) {

        let dataflow = { insert_success: 0, m_inout: null, insert_sql: '', inout_cmt: null, order_cm: null };
        let inoutService = new InoutService();
        let inoutlineService = new InoutlineService();
        let invoiceService = new InvoiceService();
        let invoicelineService = new InvoicelineService();
        let paymentService = new PaymentService();
        return new Promise(function (resolve, reject) {

            let inoutViewModel = new InoutViewModel();

            let m_inout = new MInout();
            m_inout.m_inout_uu = HelperService.UUID();
            m_inout.documentno = new Date().getTime();
            m_inout.docstatus = 'COMPLETED';

            let doctype = new CDoctype();
            doctype.c_doctype_id = inout_doctype_id;

            let orderCM = HelperService.ConvertRowDataToViewModel(orderCMT);
            inoutViewModel.c_order = orderCM.c_order;
            inoutViewModel.m_inout = m_inout;
            inoutViewModel.m_inout.dateacct = orderCM.c_order.dateacct;
            inoutViewModel.c_bpartner = orderCM.c_bpartner;
            inoutViewModel.c_bpartner_location = orderCM.c_bpartner_location;
            inoutViewModel.m_warehouse = orderCM.m_warehouse;
            inoutViewModel.m_shipper = orderCM.m_shipper;
            inoutViewModel.c_doctype = doctype;
            let insertSql = inoutService.GetInsertSqlStatement(inoutViewModel);
            dataflow.m_inout = m_inout;
            dataflow.insert_sql = insertSql;
            dataflow.order_cm = orderCM;

            resolve(dataflow);
        }).then(function (dataflow) {

            return new Promise(function (resolve, reject) {

                apiInterface.ExecuteSqlStatement(dataflow.insert_sql).then(function (insertSuccess) {

                    dataflow['insert_success'] += insertSuccess;
                    resolve(dataflow);

                });

            });


        }).then(function (dataflow) {

            return new Promise(function (resolve, reject) {

                inoutService.FindInoutViewModelByMInoutUu(dataflow.m_inout.m_inout_uu).then(function (inoutCMTList) {
                    let inoutCMT = inoutCMTList[0];
                    dataflow.inout_cmt = inoutCMT;
                    resolve(dataflow);
                });

            });


        }).then(function (dataflow) {

            return new Promise(function (resolve, reject) {

                inoutlineService.GenerateInoutlineFromCOrderUu(dataflow.inout_cmt).then(function (insert_success) {
                    dataflow['insert_success'] += insert_success;
                    resolve(dataflow);

                });

            });


        }).then(function (dataflow) {

            return new Promise(function (resolve, reject) {

                inoutService.RunProcessDocument(dataflow.inout_cmt).then(function (insert_success) {

                    dataflow.insert_success += insert_success;
                    resolve(dataflow);
                });

            });


        }).then(function (dataflow) {

            return new Promise(function (resolve, reject) {

                ///UPDATE STATUS INVOICE
                let c_doctype_invoice = new CDoctype();
                c_doctype_invoice.c_doctype_id = invoice_doc_type_id;

                let invoiceCM = new InvoiceComponentModel();
                let c_invoice = new CInvoice();
                c_invoice.c_invoice_uu = HelperService.UUID();
                c_invoice.documentno = new Date().getTime();
                c_invoice.docstatus = 'COMPLETED';
                c_invoice.dateacct = dataflow.order_cm.c_order.dateacct;
                invoiceCM.c_invoice = c_invoice;
                invoiceCM.c_bpartner = dataflow.order_cm.c_bpartner;
                invoiceCM.c_bpartner_location = dataflow.order_cm.c_bpartner_location;
                invoiceCM.c_order = dataflow.order_cm.c_order;
                invoiceCM.c_doctype = c_doctype_invoice;

                dataflow.invoice_cm = invoiceCM;
                invoiceService.SaveUpdate(invoiceCM).then(function (insert_success) {

                    dataflow.insert_success += insert_success;
                    resolve(dataflow);

                });



            });

        }).then(function (dataflow) {
            return new Promise(function (resolve, reject) {

                invoiceService.FindInvoiceComponentModelByCInvoiceUu(dataflow.invoice_cm.c_invoice.c_invoice_uu).then(function (invoiceCMTList) {


                    let invoiceCMT = invoiceCMTList[0];
                    dataflow.invoice_cmt = invoiceCMT;
                    resolve(dataflow);

                });

            });



        }).then(function (dataflow) {

            return new Promise(function (resolve, reject) {
                invoicelineService.GenerateInvoicelineFromCOrderUu(dataflow.invoice_cmt).then(function (inserted) {
                    dataflow.insert_success += inserted;
                    resolve(dataflow);

                });

            });



        }).then(function (dataflow) {


            return new Promise(function (resolve, reject) {

                invoiceService.CountAllTotal(dataflow.invoice_cm.c_invoice.c_invoice_uu).then(function (insert_success) {

                    dataflow.insert_success += insert_success;
                    resolve(dataflow);

                });

            });

        }).then(function (dataflow) {


            //data Invoice cmt diambil lagi karena sudah diupdate grandtotal
            return new Promise(function (resolve, reject) {

                invoiceService.FindInvoiceComponentModelByCInvoiceUu(dataflow.invoice_cm.c_invoice.c_invoice_uu).then(function (invoiceCMTList) {


                    let invoiceCMT = invoiceCMTList[0];
                    dataflow.invoice_cmt = invoiceCMT;
                    resolve(dataflow);

                });

            });
        }).then(function (dataflow) {


            return new Promise(function (resolve, reject) {
                invoiceService.RunProcessDocument(dataflow.invoice_cmt).then(function (inserted) {
                    dataflow.insert_success += inserted;
                    dataflow.invoice_cm = HelperService.ConvertRowDataToViewModel(dataflow.invoice_cmt);
                    resolve(dataflow);
                });

            });


        }).then(function (dataflow) {

            return new Promise(function (resolve, reject) {


                let paymentCM = new PaymentComponentModel();

                let c_doctypePayment = new CDoctype();
                c_doctypePayment.c_doctype_id = payment_doc_type_id; //Account Receivable Receipt

                let c_payment = new CPayment();
                c_payment.documentno = new Date().getTime();
                c_payment.docstatus = 'COMPLETED';
                c_payment.c_payment_uu = HelperService.UUID();
                c_payment.dateacct = dataflow.order_cm.c_order.dateacct;

                paymentCM.c_payment = c_payment;
                paymentCM.c_order = dataflow.invoice_cm.c_order;
                paymentCM.c_bpartner = dataflow.invoice_cm.c_bpartner;
                paymentCM.c_bpartner_location = dataflow.invoice_cm.c_bpartner_location;
                paymentCM.c_invoice = dataflow.invoice_cm.c_invoice;
                paymentCM.c_bankaccount = dataflow.order_cm.c_bankaccount;
                paymentCM.c_doctype = c_doctypePayment;

                let sqlPayment = paymentService.GetSqlInsertStatement(paymentCM);
                apiInterface.ExecuteSqlStatement(sqlPayment).then(function (inserted) {

                    dataflow.insert_success += inserted;
                    dataflow.payment_cm = paymentCM;
                    resolve(dataflow);

                });


            });

        }).then(function (dataflow) {

            return new Promise(function (resolve, reject) {

                paymentService.FindPaymentComponentModelByCPaymentUu(dataflow.payment_cm.c_payment.c_payment_uu)
                    .then(function (paymentCMTList) {
                        let paymentCMT = paymentCMTList[0];

                        dataflow.payment_cmt = paymentCMT;
                        resolve(dataflow);
                    })

            });


        }).then(function (dataflow) {

            return new Promise(function (resolve, reject) {
                paymentService.RunProcessDocument(dataflow.payment_cmt).then(function (inserted) {

                    dataflow.insert_success += inserted;
                    resolve(dataflow);

                });


            });


        });


    }



    /**
     * 
     
     */
    async    SaveUpdate(viewModel, datatableReference) {
        const timeNow = new Date().getTime();
        let insertSuccess = 0;
        let isNewRecord = false;
        let thisObject = this;

        try {
            if (!viewModel.c_order.c_order_uu) { //new record
                isNewRecord = true;
                viewModel.c_order.c_order_uu = HelperService.UUID();
                viewModel.c_order.ad_client_uu = localStorage.getItem('ad_client_uu');
                viewModel.c_order.ad_org_uu = localStorage.getItem('ad_org_uu');

                viewModel.c_order.createdby = localStorage.getItem('ad_user_uu');
                viewModel.c_order.updatedby = localStorage.getItem('ad_user_uu');

                viewModel.c_order.created = timeNow;
                viewModel.c_order.updated = timeNow;
            } else { //update record


                viewModel.c_order.updatedby = localStorage.getItem('ad_user_uu');

                viewModel.c_order.updated = timeNow;
            }

            viewModel.c_order.dateordered = timeNow;
            viewModel.c_order.m_warehouse_uu = viewModel.m_warehouse.m_warehouse_uu;
            viewModel.c_order.c_bpartner_uu = viewModel.c_bpartner.c_bpartner_uu;
            viewModel.c_order.c_bankaccount_uu = viewModel.c_bankaccount.c_bankaccount_uu;
            viewModel.c_order.c_doctype_id = viewModel.c_doctype.c_doctype_id;
            viewModel.c_order.m_shipper_uu = viewModel.m_shipper.m_shipper_uu;



            viewModel.c_order.sync_client = null;
            viewModel.c_order.process_date = null;


            const sqlInsertProduct = HelperService.SqlInsertOrReplaceStatement('c_order', viewModel.c_order);

            insertSuccess += await apiInterface.ExecuteSqlStatement(sqlInsertProduct);
            insertSuccess += await thisObject.CountTotallinesAndGrandTotal(viewModel.c_order.c_order_uu);
            // jika sukses tersimpan 
            if (insertSuccess > 0) {

                if (datatableReference) {


                    let rowDataList = await thisObject.FindOrderViewModelByCOrderUu(viewModel.c_order.c_order_uu);

                    let rowData = {};

                    if (rowDataList.length > 0) {
                        rowData = rowDataList[0];
                    }

                    if (!isNewRecord) {

                        datatableReference.row($('#bttn_row_edit' + rowData['c_order_wx_c_order_uu']).parents('tr')).data(rowData).draw();
                    } else {

                        datatableReference.row.add(rowData).draw();
                    }
                }


            }
        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.SaveUpdate.name, error);
        }


        return insertSuccess;


    }

    /**
     * Hanya update tanpa draw karena ketika orderline update, maka orderpagejuga harus terupdate
     * terutama jumlahnya linenetamt dll
     * @param {*} datatableReference 
     * @param {*} c_order_uu 
     */
    async UpdateTableRowWithoutDraw(datatableReference, c_order_uu, orderPage) {
        let thisObject = this;
        let dataInTable = datatableReference.data();
        let orderCMTList = await thisObject.FindOrderViewModelByCOrderUu(c_order_uu);
        let indexFound = _.findIndex(dataInTable, function (dataRow) { return dataRow.c_order_wx_c_order_uu === c_order_uu; });
        let orderCMT = orderCMTList[0];
        let updateDataCell = ['c_order_wx_totallines', 'c_order_wx_totaltaxlines', 'c_order_wx_grandtotal'];

        //datatableReference.context[0].aoColumns[0].data
        if (indexFound >= 0) {
            //update row
            datatableReference.row($('#' + c_order_uu)).data(orderCMT);
            //ambil tdArray dalam tr
            let tdArray = $('tr#' + c_order_uu).children();
            //ambil definisi column di datatable
            let columnsInDataTable = datatableReference.context[0].aoColumns;
            _.forEach(updateDataCell, function (column) {

                let indexColumn = _.findIndex(columnsInDataTable, function (columnInDt) { return columnInDt.data === column; });
                let value = new Intl.NumberFormat().format(orderCMT[column]);
                $(tdArray[indexColumn]).html(value);


            });
            // let a = $('tr#b2e58f33-0af2-41bd-b74b-ccebb046de5a').children(); $(a[5]).html('test');
            orderPage.EventHandler();
        }

    }

    /**
       * Membuat SalesOrder dari document RMA
       * // create c_order
       * // create orderline dari rmaline
       * @param {RmaComponentModelTable} rmaCMT 
       */
    CreateNewSalesOrderFromRma(rmaCMT) {

        let thisObject = this;
        const timeNow = new Date().getTime();
        let insertSuccess = 0;
        let orderModel = new COrder();
        let bankaccountService = new BankaccountService();
        let metadataBankaccount = new MetaDataTable();
        metadataBankaccount.ad_org_uu = localStorage.getItem('ad_org_uu');

        let bankaccountList = bankaccountService.FindAll(metadataBankaccount);
        let bankaccountCMT = bankaccountList[0];

        let shipperService = new ShipperService();
        let shipperList = shipperService.FindAll(metadataBankaccount);
        let shipperCMT = shipperList[0];

        orderModel.c_order_uu = HelperService.UUID();
        orderModel.ad_client_uu = localStorage.getItem('ad_client_uu');
        orderModel.ad_org_uu = localStorage.getItem('ad_org_uu');

        orderModel.createdby = localStorage.getItem('ad_user_uu');
        orderModel.updatedby = localStorage.getItem('ad_user_uu');

        orderModel.created = timeNow;
        orderModel.updated = timeNow;


        orderModel.dateordered = timeNow;
        orderModel.documentno = new Date().getTime();
        orderModel.m_warehouse_uu = rmaCMT.m_warehouse_wx_m_warehouse_uu;
        orderModel.c_bpartner_uu = rmaCMT.c_bpartner_wx_c_bpartner_uu;
        orderModel.c_bankaccount_uu = bankaccountCMT.c_bankaccount_wx_c_bankaccount_uu;
        orderModel.c_doctype_id = 2002; // penjualan dengan retur
        orderModel.m_shipper_uu = shipperCMT.m_shipper_wx_m_shipper_uu;
        orderModel.docstatus = 'DRAFT';

        //orderModel.m_pricelist_id = 1000002; //sales order pricelist
        orderModel.m_pricelist_uu = 'c90fbb80-589b-4943-b1eb-e22beb25178a';

        orderModel.sync_client = null;
        orderModel.process_date = null;


        const sqlInsertProduct = HelperService.SqlInsertOrReplaceStatement('c_order', orderModel);

        return new Promise(function (resolve, reject) {
            apiInterface.ExecuteSqlStatement(sqlInsertProduct)
                .then(function (rowChanged) {
                    insertSuccess += rowChanged;
                    // jika sukses tersimpan 
                    if (insertSuccess > 0) {


                        let rmalineService = new RmalineService();

                        rmalineService.FindRmalineComponentModelByMRmaUu(rmaCMT.m_rma_wx_m_rma_uu)
                            .then(function (rmalineCMTList) {

                                let promiseArray = [];
                                _.forEach(rmalineCMTList, function (rmalineCMT, index) {
                                    let orderline = new COrderline();


                                    orderline.c_orderline_uu = HelperService.UUID();
                                    orderline.ad_client_uu = localStorage.getItem('ad_client_uu');
                                    orderline.ad_org_uu = localStorage.getItem('ad_org_uu');
                                    orderline.line = ++index;
                                    orderline.createdby = localStorage.getItem('ad_user_uu');
                                    orderline.updatedby = localStorage.getItem('ad_user_uu');

                                    orderline.created = timeNow;
                                    orderline.updated = timeNow;

                                    orderline.c_order_uu = orderModel.c_order_uu;
                                    orderline.m_product_uu = rmalineCMT.m_product_wx_m_product_uu;
                                    orderline.c_uom_uu = rmalineCMT.c_uom_wx_c_uom_uu;
                                    orderline.c_tax_uu = rmalineCMT.c_tax_wx_c_tax_uu;
                                    orderline.priceentered = rmalineCMT.m_rmaline_wx_priceentered;
                                    orderline.priceactual = rmalineCMT.m_rmaline_wx_priceactual;
                                    orderline.qtyordered = rmalineCMT.m_rmaline_wx_movementqty;
                                    orderline.qtyentered = rmalineCMT.m_rmaline_wx_qtyentered;
                                    orderline.linenetamt = rmalineCMT.m_rmaline_wx_linenetamt * -1;
                                    orderline.linetaxamt = rmalineCMT.m_rmaline_wx_taxamt * -1;
                                    orderline.isreturn = 'Y';
                                    orderline.sync_client = null;
                                    orderline.process_date = null;
                                    const sqlInsertStatement = HelperService.SqlInsertOrReplaceStatement('c_orderline', orderline);

                                    let promiseFunction = function () {
                                        return apiInterface.ExecuteSqlStatement(sqlInsertStatement);
                                    }

                                    promiseArray.push(promiseFunction);

                                });

                                Promise.all(promiseArray).then(function (resultArray) {

                                    thisObject.CountTotallinesAndGrandTotal(orderModel.c_order_uu);

                                })




                            });




                    }
                });


        });





    }



}