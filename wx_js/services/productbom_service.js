
class ProductBomService {

    constructor() {
    }



    /**
	 * Find All warehouse dengan bentuk warehouseviewmodel
	 * @param {*} metaDataResponseTable 
	 */
    FindAll(metaDataResponseTable) {
        let metaDataQuery;
        try {
            let otherFilters = '';
            if (metaDataResponseTable.otherFilters.length > 0) {

                _.forEach(metaDataResponseTable.otherFilters, function (value) {

                    otherFilters += ' ' + value + ' ';
                });

            }

            //  searchFilter += ' AND m_product_bom.m_product_uu = \'' + mProductUu + '\'';
            // 1. List ProductParent
            const sqlTemplate = '  FROM m_product_bom ' +
                ' INNER JOIN m_product ON m_product.m_product_uu =  m_product_bom.m_productbom_uu ' +
                ' INNER JOIN c_uom ON c_uom.c_uom_uu  = m_product.c_uom_uu ' +
                ' WHERE m_product_bom.wx_isdeleted = \'N\' ' + otherFilters + ' ORDER BY m_product_bom.line ASC ';



            const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new ProductBomViewModel());

            metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);

        } catch (error) {
            apiInterface.Log(this.constructor.name, this.FindAll.name, error);
        }



        return apiInterface.Query(metaDataQuery);
    }

    /**
     * Find ProductBomCM by Product Parent UUID
     * @param {*} m_product_uu 
     */
    FindProductBomCMByMProductUu(m_product_uu) {

        let metaDataQuery;
        try {

            const sqlTemplate = '  FROM m_product_bom ' +
                ' INNER JOIN m_product ON m_product.m_product_uu = m_product_bom.m_productbom_uu ' +
                ' INNER JOIN c_uom ON c_uom.c_uom_uu = m_product.c_uom_uu ' +
                ' WHERE m_product_bom.m_product_uu = \'' + m_product_uu + '\'';


            const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new ProductBomViewModel());

            metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);
        } catch (error) {
            apiInterface.Log(this.constructor.name, this.FindProductBomCMByMProductUu.name, error);
        }

        return apiInterface.Query(metaDataQuery);
    }

    FindProductBomViewModelByMProductBomUu(m_product_bom_uu) {

        let metaDataQuery;
        try {

            const sqlTemplate = '  FROM m_product_bom ' +
                ' INNER JOIN m_product ON m_product.m_product_uu = m_product_bom.m_productbom_uu ' +
                ' INNER JOIN c_uom ON c_uom.c_uom_uu = m_product.c_uom_uu ' +
                ' WHERE m_product_bom.m_product_bom_uu = \'' + m_product_bom_uu + '\'';


            const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new ProductBomViewModel());

            metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);
        } catch (error) {
            apiInterface.Log(this.constructor.name, this.FindProductBomViewModelByMProductBomUu.name, error);
        }

        return apiInterface.Query(metaDataQuery);
    }

    /**
	 * 
	
	 */
    async SaveUpdate(viewModel, datatableReference) {
        const timeNow = new Date().getTime();
        let insertSuccess = 0;
        let isNewRecord = false;
        let thisObject = this;

        try {

            if (!viewModel.m_product_bom.m_product_bom_uu) { //new record
                isNewRecord = true;
                viewModel.m_product_bom.m_product_bom_uu = HelperService.UUID();
                viewModel.m_product_bom.ad_client_uu = localStorage.getItem('ad_client_uu');
                viewModel.m_product_bom.ad_org_uu = localStorage.getItem('ad_org_uu');

                viewModel.m_product_bom.createdby = localStorage.getItem('ad_user_uu');
                viewModel.m_product_bom.updatedby = localStorage.getItem('ad_user_uu');

                viewModel.m_product_bom.created = timeNow;
                viewModel.m_product_bom.updated = timeNow;
            } else { //update record


                viewModel.m_product_bom.updatedby = localStorage.getItem('ad_user_uu');

                viewModel.m_product_bom.updated = timeNow;
            }


            viewModel.m_product_bom.m_productbom_uu = viewModel.m_product.m_product_uu;
            viewModel.m_product_bom.sync_client = null;
            viewModel.m_product_bom.process_date = null;


            const sqlInsertProductBom = HelperService.SqlInsertOrReplaceStatement('m_product_bom', viewModel.m_product_bom);

            insertSuccess += await apiInterface.ExecuteSqlStatement(sqlInsertProductBom);

            // jika sukses tersimpan 
            if (insertSuccess > 0) {

                let rowDataList = await thisObject.FindProductBomViewModelByMProductBomUu(viewModel.m_product_bom.m_product_bom_uu);

                let rowData = {};

                if (rowDataList.length > 0) {
                    rowData = rowDataList[0];
                }

                if (!isNewRecord) {

                    datatableReference.row($('#bttn_row_edit' + rowData['m_product_bom_wx_m_product_bom_uu']).parents('tr')).data(rowData).draw();
                } else {

                    datatableReference.row.add(rowData).draw();
                }

             

            }


        } catch (error) {
            apiInterface.Log(this.constructor.name, this.SaveUpdate.name, error);
            return insertSuccess;
        }


        return insertSuccess;




    }




}