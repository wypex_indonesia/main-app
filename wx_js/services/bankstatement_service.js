/**
 * API  (AP Invoice) 5000
 * ARI (AR Invoice)  6000
 */
class BankstatementService {
    constructor() {

    }


    FindBankStatementCMTListByCBankstatementUu(c_bankstatement_uu) {
        const sqlTemplate = '  FROM c_bankstatement ' +
            ' INNER JOIN c_bankaccount ON c_bankstatement.c_bankaccount_uu = c_bankaccount.c_bankaccount_uu ' +
            ' WHERE c_bankstatement.c_bankstatement_uu = \'' + c_bankstatement_uu + '\'';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new BankstatementComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);



    }


    /**
     * 
     * @param {CBankstatement} bankstatement 
    
     */
    SaveUpdate(bankstatement) {
        const timeNow = new Date().getTime();
        let thisObject = this;
        let isNewRecord = false;
        let insertSuccess = 0;
        let sqlInsert = '';
        try {
            if (!bankstatement.c_bankstatement_uu) { //new record
                isNewRecord = true;
            }
            sqlInsert = thisObject.GetSqlInsertStatement(bankstatement);



        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.SaveUpdate.name, error);
            return new Promise(function (resolve, reject) { resolve(insertSuccess); });
        }

        return apiInterface.ExecuteSqlStatement(sqlInsert);
    }

    GetSqlInsertStatement(bankstatement) {
        const timeNow = new Date().getTime();
        let thisObject = this;

        let sqlInsert = '';
        try {
            if (!bankstatement.c_bankstatement_uu) { //new record

                bankstatement.c_bankstatement_uu = HelperService.UUID();
                bankstatement.ad_client_uu = localStorage.getItem('ad_client_uu');;
                bankstatement.ad_org_uu = localStorage.getItem('ad_org_uu');

                bankstatement.createdby = localStorage.getItem('ad_user_uu');
                bankstatement.updatedby = localStorage.getItem('ad_user_uu');

                bankstatement.created = timeNow;
                bankstatement.updated = timeNow;
            } else { //update record


                bankstatement.updatedby = localStorage.getItem('ad_user_uu');

                bankstatement.updated = timeNow;
            }

            if (!bankstatement.ad_client_uu){
                bankstatement.ad_client_uu = localStorage.getItem('ad_client_uu');;
                bankstatement.ad_org_uu = localStorage.getItem('ad_org_uu');

            }

            if (!bankstatement.created){
                bankstatement.created = timeNow;
                bankstatement.createdby = localStorage.getItem('ad_user_uu');
            }

            bankstatement.sync_client = null;
            bankstatement.process_date = null;
            sqlInsert = HelperService.SqlInsertOrReplaceStatement('c_bankstatement', bankstatement);



        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.GetSqlInsertStatement.name, error);
        }

        return sqlInsert;

    }

    /**
     * Create BankStatement , SaveUpdate dan RunProcess Document
     * @param {*} c_bankaccount_uu 
     * @param {*} c_order_uu 
     * @param {*} stmtamt  statementamount 
     * @param {*} c_doctype_id 6001
     * @param {*} docstatus 
    
     */
    CreateBankStatement(c_bankaccount_uu, c_order_uu, c_payment_uu, c_invoice_uu, stmtamt, c_doctype_id, docstatus, dateacct) {
        let bankstatement = new CBankstatement();
        let thisObject = this;
        if (!dateacct) {
            dateacct = new Date().getTime();
        }


        return new Promise(function (resolve, reject) {

            let bankaccountService = new BankaccountService();
            bankaccountService.GetBankaccountCMTByCBankaccountUu(c_bankaccount_uu).then(function (bankaccountCMTList) {
                let bankaccountCMT = bankaccountCMTList[0];
                resolve(bankaccountCMT);

            });


        }).then(function (bankaccountCMT) {

            bankstatement.dateacct = dateacct;
            bankstatement.c_bankstatement_uu = HelperService.UUID();
            bankstatement.c_bankaccount_uu = c_bankaccount_uu;
            bankstatement.docstatus = docstatus;

            let bankstatementline = new CBankstatementline();
            bankstatementline.c_bankstatementline_uu = HelperService.UUID();
            bankstatementline.c_bankstatement_uu = bankstatement.c_bankstatement_uu;
            bankstatementline.c_order_uu = c_order_uu;
            bankstatementline.c_payment_uu = c_payment_uu;
            bankstatementline.c_invoice_uu = c_invoice_uu;
            bankstatementline.stmtamt = stmtamt;
            bankstatementline.dateacct = dateacct;
            bankstatementline.asset_element = bankaccountCMT.c_bankaccount_wx_asset_element;
            bankstatementline.transit_element = bankaccountCMT.c_bankaccount_wx_transit_element;

            let bankstatementLineService = new BankstatementlineService();

            return new Promise(function (resolve, reject) {
                let sqlInsertBankstatement = thisObject.GetSqlInsertStatement(bankstatement);
                let sqlInsertBankstatementline = bankstatementLineService.GetInsertSqlStatement(bankstatementline);
                apiInterface.ExecuteBulkSqlStatement([sqlInsertBankstatement, sqlInsertBankstatementline]).then(function (inserted) {

                    resolve(inserted);


                })
            });


        }).then(function (insertSuccess) {

            return new Promise(function (resolve, reject) {
                thisObject.RunProcessDocument(bankstatement.c_bankstatement_uu, c_doctype_id, docstatus, c_bankaccount_uu).then(function (inserted) {

                    insertSuccess += inserted;
                    resolve(insertSuccess);

                })

            });


        });


    }





    /**
     * Run Process Document sesuai dengan docstatus :
     * 1. Draft -> Tidak ada Action
     * docbasetype :
     * AR Invoice
     * 
     * AP Invoice
     * 2. COMPLETED
     *  2.2. FactAcct 
     * 3. REVERSED
     *  2.1. FactAcct -> Reversed
     * 
     * @param {CBankstatement} bankstatement
     */
    RunProcessDocument(c_bankstatement_uu, c_doctype_id, c_bankstatement_wx_docstatus, c_bankaccount_uu) {
        let thisObject = this;
        let insertSuccess = 0;
        try {
            let factacctService = new FactAcctService();
            let bankstatementlineService = new BankstatementlineService();
            let dataflow = {};
            return new Promise(function (resolve, reject) {
                bankstatementlineService.FindOnlyBankstatementlineByCBankstatementUu(c_bankstatement_uu)
                    .then(function (bankstatementlineComponentModelTableList) {
                        let sqlInsertArray = [];
                        let factacctUuList = [];

                        _.forEach(bankstatementlineComponentModelTableList, function (bankstatementlineComponentTable) {


                            if (c_bankstatement_wx_docstatus === 'COMPLETED') {
                                sqlInsertArray = sqlInsertArray.concat(thisObject.GetBankstatementCompletedSqlInsertStatement(bankstatementlineComponentTable, c_doctype_id));
                            }

                            if (c_bankstatement_wx_docstatus === 'REVERSED') {
                                sqlInsertArray = sqlInsertArray.concat(thisObject.GetBankstatementReversedSqlInsertStatement(bankstatementlineComponentTable));

                            }

                        });

                        dataflow.bankstatementline_cmtlist = bankstatementlineComponentModelTableList;
                        dataflow.sqlinsert_array = sqlInsertArray;
                        dataflow.fact_acct_list = factacctUuList;
                        resolve(dataflow);
                    });



            }).then(function (dataflow) {

                return new Promise(function (resolve, reject) {
                    apiInterface.ExecuteBulkSqlStatement(dataflow.sqlinsert_array).then(function (inserted) {
                        insertSuccess += inserted;
                        resolve(dataflow);

                    });

                });

            });/*.then(function (dataflow) {

                //ambil semua fact_acct_list yang terjadi pada transaksi dengan checking account element
                // untuk ditambahkan atau dikurangi dengan current balance
                return new Promise(function (resolve, reject) {
                    let elementValue = new ElementValueService();

                    let promiseArray = [];

                    _.forEach(dataflow.bankstatementline_cmtlist, function (bankstatementline_cmt) {

                        promiseArray.push(factacctService.FindFactAcct(bankstatementline_cmt.c_bankstatementline_wx_c_bankstatementline_uu,
                            elementValue.ElementValueList().CheckingAccount.value, c_bankstatement_wx_docstatus));

                    });

                    let fact_acct_list = [];
                    Promise.all(promiseArray).then(function (resultArray) {


                        _.forEach(resultArray, function (result) {
                            fact_acct_list = fact_acct_list.concat(result);

                        });
                        dataflow.fact_acct_list = fact_acct_list;
                        resolve(dataflow);
                    });


                });


            }).then(function (dataflow) {
                return new Promise(function (resolve, reject) {
                    let sqlInsertArray = [];
                    _.forEach(dataflow.fact_acct_list, function (fact_acct) {


                        let amt = 0;
                        if (fact_acct.fact_acct_wx_amtacctdr) {

                            amt = numeral(fact_acct.fact_acct_wx_amtacctdr).value();

                        }

                        if (fact_acct.fact_acct_wx_amtacctcr) {
                            amt = numeral(fact_acct.fact_acct_wx_amtacctcr).value() * -1;
                        }


                        let sqlUpdateCurrentBalance = 'UPDATE c_bankaccount SET currentbalance = (SELECT (' + amt +
                            ' + currentbalance) FROM c_bankaccount WHERE c_bankaccount_uu = \'' + c_bankaccount_uu
                            + '\') WHERE c_bankaccount_uu = \'' + c_bankaccount_uu + '\';';
                        sqlInsertArray.push(sqlUpdateCurrentBalance);

                    })

                    apiInterface.ExecuteBulkSqlStatement(sqlInsertArray).then(function (inserted) {

                        insertSuccess += inserted;

                        resolve(insertSuccess);

                    });



                });


            });*/

        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.RunProcessDocument, error);
        }





    }

    /**
     * 
     * @param {*} c_payment_uu 
     * @param {*} c_doctype_id , doctype Id semisal invoice_wx_c_doctype_id, order_wx_c_doctype_id
     * @param {*} set_docstatus , set doctstatus semisal COMPLETED ATAU REVERSED
     */
    SetStatusAndRunProcess(c_payment_uu, c_doctype_id, set_docstatus) {
        let thisObject = this;
        let bankstatementlineService = new BankstatementlineService();
        let insertSuccess = 0;
        return new Promise(function (resolve, reject) {


            bankstatementlineService.FindBankstatementlineComponentModelByCPaymentUu(c_payment_uu)
                .then(function (bankstatementlineCMTList) {

                    let bankstatementlineCMT = bankstatementlineCMTList[0];
                    let bankstatementlineCM = HelperService.ConvertRowDataToViewModel(bankstatementlineCMT);
                    let bankstatement = bankstatementlineCM.c_bankstatement;
                    resolve(bankstatement);
                });


        }).then(function (bankstatement) {

            return new Promise(function (resolve, reject) {

                bankstatement.docstatus = set_docstatus;
                thisObject.SaveUpdate(bankstatement).then(function (rowChanged) {
                    insertSuccess += rowChanged;
                    resolve(bankstatement);

                });
            });
        }).then(function (bankstatement) {


            return new Promise(function (resolve, reject) {

                thisObject.RunProcessDocument(bankstatement.c_bankstatement_uu, c_doctype_id, set_docstatus, bankstatement.c_bankaccount_uu).then(function (inserted) {

                    insertSuccess += inserted;
                    resolve(insertSuccess);


                });


            });

        });



    }


    /**    Jika Sales Order 11100 – Checking Account	111110 – Checking In Transfer
        *  Jika Purchase Order 111110 – Checking In Transfer	11100 – Checking Account
        */
    BankstatementCompleted(bankstatementlineComponentModelTable, c_doctype_id) {


        let thisObject = this;
        let sqlInsertArray = thisObject.GetBankstatementCompletedSqlInsertStatement(bankstatementlineComponentModelTable, c_doctype_id);

        return apiInterface.ExecuteBulkSqlStatement(sqlInsertArray);

    }


    /**
     * Return array sqlInsertStatement agar dapat dipergunakan pada proses yang lain, karena kalau loop jika function new promise masih belum bisa
     * kecuali pakai bluebird library
     * @param {*} bankstatementlineComponentModelTable 
     * @param {*} c_doctype_id 
    
     */
    GetBankstatementCompletedSqlInsertStatement(bankstatementlineComponentModelTable, c_doctype_id) {
        let elementValueService = new ElementValueService();
        let factacctService = new FactAcctService();
        let checkingAccountCrDocType = [5000, 5001, 5002, 10002, 10004]; //APP,API, AP Expense , Transfer Out Internal Antar Bank/Cash, pengambilan modal usaha
        let checkingAccountDrDocType = [6000, 6001, 2002, 10001, 10003]; //ARP,ARR, CRM, Transfer In Internal Antar Bank/Cash, penyetoran modal usaha
        let uuid = bankstatementlineComponentModelTable.c_bankstatementline_wx_c_bankstatementline_uu;
        let amt = bankstatementlineComponentModelTable.c_bankstatementline_wx_stmtamt;
        let thisObject = this;
        let sqlInsertArray = [];
        try {
            if (checkingAccountDrDocType.includes(c_doctype_id)) {
                //DR
                let sqlInsertDr = factacctService.SqlInsertFactAcctStatement(bankstatementlineComponentModelTable.c_bankstatementline_wx_asset_element,
                    'c_bankstatementline', uuid, amt, null, bankstatementlineComponentModelTable.c_bankstatementline_wx_dateacct);



                //CR
                let sqlInsertCr = factacctService.SqlInsertFactAcctStatement(bankstatementlineComponentModelTable.c_bankstatementline_wx_transit_element,
                    'c_bankstatementline', uuid, null, amt, bankstatementlineComponentModelTable.c_bankstatementline_wx_dateacct);


                sqlInsertArray.push(sqlInsertCr, sqlInsertDr);


            }

            if (checkingAccountCrDocType.includes(c_doctype_id)) {
                //DR
                let sqlInsertDr = factacctService.SqlInsertFactAcctStatement(bankstatementlineComponentModelTable.c_bankstatementline_wx_transit_element, 'c_bankstatementline',
                    uuid, amt, null, bankstatementlineComponentModelTable.c_bankstatementline_wx_dateacct);


                //CR
                let sqlInsertCr = factacctService.SqlInsertFactAcctStatement(bankstatementlineComponentModelTable.c_bankstatementline_wx_asset_element, 'c_bankstatementline',
                    uuid, null, amt, bankstatementlineComponentModelTable.c_bankstatementline_wx_dateacct);



                sqlInsertArray.push(sqlInsertCr, sqlInsertDr);


            }
        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.GetBankstatementCompletedSqlInsertStatement.name, error);
        }


        return sqlInsertArray;
    }


    BankstatementReversed(bankstatementlineComponentModelTable) {
        let thisObject = this;
        let sqlInsertArray = thisObject.GetBankstatementReversedSqlInsertStatement(bankstatementlineComponentModelTable);
        return apiInterface.ExecuteBulkSqlStatement(sqlInsertArray);
    }

    GetBankstatementReversedSqlInsertStatement(bankstatementlineComponentModelTable) {
       
        let factacctService = new FactAcctService();
        let insertSuccess = 0;
        let sqlInsertCheckingInTransfer = factacctService.sqlInsertReverseFactAcctStatement('c_bankstatementline', bankstatementlineComponentModelTable.c_bankstatementline_wx_c_bankstatementline_uu,
            bankstatementlineComponentModelTable.c_bankstatementline_wx_transit_element, bankstatementlineComponentModelTable.c_bankstatementline_wx_dateacct);
        let sqlInsertCheckingAccount = factacctService.sqlInsertReverseFactAcctStatement('c_bankstatementline', bankstatementlineComponentModelTable.c_bankstatementline_wx_c_bankstatementline_uu,
            bankstatementlineComponentModelTable.c_bankstatementline_wx_asset_element, bankstatementlineComponentModelTable.c_bankstatementline_wx_dateacct);
        let sqlInsertArray = [];
        sqlInsertArray.push(sqlInsertCheckingAccount, sqlInsertCheckingInTransfer);
        return sqlInsertArray;


    }


}