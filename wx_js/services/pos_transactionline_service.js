

class PosTransactionlineService {
    constructor() { }



    FindPosTransactionlineCashAccountCMTList(pos_transaction_uu, c_bankaccount_uu) {

        const sqlTemplate = '  FROM pos_transactionline ' +
            ' INNER JOIN pos_transaction ON pos_transactionline.pos_transaction_uu = pos_transaction.pos_transaction_uu ' +
            ' INNER JOIN c_bankaccount ON c_bankaccount.c_bankaccount_uu  = pos_transactionline.c_bankaccount_uu ' +
            ' WHERE pos_transactionline.wx_isdeleted = \'N\' ' +
            ' AND pos_transactionline.pos_transaction_uu = \'' + pos_transaction_uu + '\' ' +
            ' AND pos_transactionline.c_bankaccount_uu = \'' + c_bankaccount_uu + '\' ;';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new PosTransactionlineComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }


    FindPosTransactionlineCMTListByPosTransactionUu(pos_transaction_uu) {

        const sqlTemplate = '  FROM pos_transactionline ' +
            ' INNER JOIN pos_transaction ON pos_transactionline.pos_transaction_uu = pos_transaction.pos_transaction_uu ' +
            ' INNER JOIN c_bankaccount ON c_bankaccount.c_bankaccount_uu  = pos_transactionline.c_bankaccount_uu ' +
            ' WHERE pos_transactionline.wx_isdeleted = \'N\'  AND pos_transactionline.pos_transaction_uu = \'' + pos_transaction_uu + '\' ;';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new PosTransactionlineComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }


    /**
	 * 
	
	 */
    async    SaveUpdate(posTransactionlineCM) {

        let insertSuccess = 0;
        let isNewRecord = false;
        let thisObject = this;

        try {
            if (!posTransactionlineCM.pos_transactionline.pos_transactioneline_uu) { //new record
                isNewRecord = true;
            }

            let sqlInsert = thisObject.GetSqlInsertStatement(posTransactionlineCM.pos_transactionline);

            insertSuccess += await apiInterface.ExecuteSqlStatement(sqlInsert);


        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.SaveUpdate.name, error);
        }


        return insertSuccess;

    }







    GetSqlInsertStatement(pos_transactionline) {

        const timeNow = new Date().getTime();


        let thisObject = this;
        let sqlInsert = '';
        try {
            if (!pos_transactionline.pos_transactionline_uu) { //new record

                pos_transactionline.pos_transactionline_uu = HelperService.UUID();
                pos_transactionline.ad_client_uu = localStorage.getItem('ad_client_uu');
                pos_transactionline.ad_org_uu = localStorage.getItem('ad_org_uu');

                pos_transactionline.createdby = localStorage.getItem('ad_user_uu');
                pos_transactionline.updatedby = localStorage.getItem('ad_user_uu');

                pos_transactionline.created = timeNow;
                pos_transactionline.updated = timeNow;
            } else { //update record


                pos_transactionline.updatedby = localStorage.getItem('ad_user_uu');

                pos_transactionline.updated = timeNow;
            }

            pos_transactionline.open_amount = numeral(pos_transactionline.open_amount).value();
            pos_transactionline.close_amount = numeral(pos_transactionline.close_amount).value();
            pos_transactionline.actual_amount = numeral(pos_transactionline.actual_amount).value();

            pos_transactionline.sync_client = null;
            pos_transactionline.process_date = null;


            sqlInsert = HelperService.SqlInsertOrReplaceStatement('pos_transactionline', pos_transactionline);

        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.SaveUpdate.name, error);
        }


        return sqlInsert;


    }



}