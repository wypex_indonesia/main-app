
class StorageOnHandService {

    constructor() { }
    /**
   * Find All warehouse dengan bentuk warehouseviewmodel
   * @param {*} metaDataResponseTable 
   */
    FindAll(metaDataResponseTable) {


        let searchFilter = '';
        if (metaDataResponseTable.search !== '') {
            searchFilter += ' AND ' + metaDataResponseTable.searchField + '  LIKE    \'%' + metaDataResponseTable.search + '%\'';
        }

        let limit = '';
        if ((metaDataResponseTable.page !== null) && (metaDataResponseTable.perpage !== null)) {

            limit = ' LIMIT ' + metaDataResponseTable.page + ',' + metaDataResponseTable.perpage;

        }

        const sqlTemplate = '  FROM m_storageonhand ' +
            ' INNER JOIN m_locator  ON m_storageonhand.m_locator_uu = m_locator.m_locator_uu ' +
            ' INNER JOIN m_warehouse ON m_locator.m_warehouse_uu = m_warehouse.m_warehouse_uu ' +
            ' INNER JOIN m_product ON m_product.m_product_uu = m_storageonhand.m_product_uu ' +
            ' INNER JOIN c_uom ON c_uom.c_uom_uu = m_product.c_uom_uu ' +
            ' WHERE m_warehouse.ad_org_uu = \'' + metaDataResponseTable.ad_org_uu +
            '\'' + searchFilter + ' ORDER BY m_product.name DESC ' + limit;

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new StorageOnHandComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }
    /**
        * Find All warehouse dengan bentuk warehouseviewmodel
        * @param {*} metaDataResponseTable 
        */
    FindStorageonhandCMTByMWarehouseUu(m_warehouse_uu) {

        const sqlTemplate = '  FROM m_storageonhand ' +
            ' INNER JOIN m_locator  ON m_storageonhand.m_locator_uu = m_locator.m_locator_uu ' +
            ' INNER JOIN m_warehouse ON m_locator.m_warehouse_uu = m_warehouse.m_warehouse_uu ' +
            ' INNER JOIN m_product ON m_product.m_product_uu = m_storageonhand.m_product_uu ' +
            ' INNER JOIN c_uom ON c_uom.c_uom_uu = m_product.c_uom_uu ' +
            ' WHERE m_warehouse.m_warehouse_uu = \'' + m_warehouse_uu + '\' ';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new StorageOnHandComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);


        return apiInterface.Query(metaDataQuery);
    }



    /**
	 * Find All warehouse dengan bentuk warehouseviewmodel
	 * @param {*} metaDataResponseTable 
	 */
    FindStorageonhandCMTByMWarehouseUu(m_warehouse_uu) {

        const sqlTemplate = '  FROM m_storageonhand ' +
            ' INNER JOIN m_locator  ON m_storageonhand.m_locator_uu = m_locator.m_locator_uu ' +
            ' INNER JOIN m_warehouse ON m_locator.m_warehouse_uu = m_warehouse.m_warehouse_uu ' +
            ' INNER JOIN m_product ON m_product.m_product_uu = m_storageonhand.m_product_uu ' +
            ' INNER JOIN c_uom ON c_uom.c_uom_uu = m_product.c_uom_uu ' +
            ' WHERE m_warehouse.m_warehouse_uu = \'' + m_warehouse_uu + '\' ';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new StorageOnHandComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);


        return apiInterface.Query(metaDataQuery);
    }


    GetStorageonhandCMTList(m_locator_uu, m_product_uu) {

        const sqlTemplate = '  FROM m_storageonhand ' +
            ' INNER JOIN m_locator  ON m_storageonhand.m_locator_uu = m_locator.m_locator_uu ' +
            ' INNER JOIN m_warehouse ON m_locator.m_warehouse_uu = m_warehouse.m_warehouse_uu ' +
            ' INNER JOIN m_product ON m_product.m_product_uu = m_storageonhand.m_product_uu ' +
            ' INNER JOIN c_uom ON c_uom.c_uom_uu = m_product.c_uom_uu ' +
            ' WHERE m_storageonhand.m_locator_uu = \'' + m_locator_uu + '\' AND  m_storageonhand.m_product_uu = \'' + m_product_uu + '\' ';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new StorageOnHandComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);

        let returnMStorageonhandModel = null;


        return apiInterface.Query(metaDataQuery);
        /*let storageonhandCMTList = apiInterface.Query(metaDataQuery);
        if (storageonhandCMTList.length > 0) {
            let storageOnhandCM = HelperService.ConvertRowDataToViewModel(storageonhandCMTList[0]);
            returnMStorageonhandModel = storageOnhandCM.m_storageonhand;
        }

        return returnMStorageonhandModel;*/

    }

    SaveUpdate(m_locator_uu, m_product_uu, movementqty) {
        const timeNow = new Date().getTime();

        let thisObject = this;
        let m_storageonhand = new MStorageonhand();
        let dataFlow = { m_storageonhand: null, insert_success: 0 };
        return new Promise(function (resolve, reject) {
            thisObject.GetStorageonhandCMTList(m_locator_uu, m_product_uu)
                .then(function (storageonhandCMTList) {

                    if (storageonhandCMTList.length) {
                        let  m_storageonhandCMT = storageonhandCMTList[0];
                        let mStorageOnHandCM =  HelperService.ConvertRowDataToViewModel(m_storageonhandCMT);
                        m_storageonhand =  mStorageOnHandCM.m_storageonhand;
                    }
                    dataFlow.m_storageonhand = m_storageonhand;


                    resolve(dataFlow);

                });


        }).then(function (dataFlow) {

            return new Promise(function (resolve, reject) {

                if (!dataFlow.m_storageonhand.m_storageonhand_uu) { //new record

                    dataFlow.m_storageonhand.m_storageonhand_uu = HelperService.UUID();
                    dataFlow.m_storageonhand.ad_client_uu = localStorage.getItem('ad_client_uu');
                    dataFlow.m_storageonhand.ad_org_uu = localStorage.getItem('ad_org_uu');

                    dataFlow.m_storageonhand.createdby = localStorage.getItem('ad_user_uu');
                    dataFlow.m_storageonhand.updatedby = localStorage.getItem('ad_user_uu');

                    dataFlow.m_storageonhand.created = timeNow;
                    dataFlow.m_storageonhand.updated = timeNow;
                } else { //update record


                    dataFlow.m_storageonhand.updatedby = localStorage.getItem('ad_user_uu');

                    dataFlow.m_storageonhand.updated = timeNow;
                }

                dataFlow.m_storageonhand.qtyonhand += movementqty;

                dataFlow.m_storageonhand.m_product_uu =  m_product_uu;
                dataFlow.m_storageonhand.m_locator_uu = m_locator_uu;

                dataFlow.m_storageonhand.sync_client = null;
                dataFlow.m_storageonhand.process_date = null;

                let insertSuccess = 0;
                const sqlInsert = HelperService.SqlInsertOrReplaceStatement('m_storageonhand', dataFlow.m_storageonhand);

                dataFlow.sql_insert = sqlInsert;
                resolve(dataFlow);

            });


        }).then(function (dataFlow) {


            return new Promise(function (resolve, reject) {

                apiInterface.ExecuteSqlStatement(dataFlow.sql_insert).then(function (inserted) {

                    resolve(inserted);

                });

            });


        });







    }


}