
class ProductCategoryService {

    constructor() { }
    /**
         * Find All warehouse dengan bentuk warehouseviewmodel
         * @param {*} metaDataResponseTable 
         */
    FindAll(metaDataResponseTable) {
        let metaDataQuery;
        try {
            let searchFilter = '';
            if (metaDataResponseTable.search !== '') {
                searchFilter += ' AND ' + metaDataResponseTable.searchField + '%' + metaDataResponseTable.search + '%';
            }

            const sqlTemplate = '  FROM m_product_category ' +
                ' WHERE m_product_category.wx_isdeleted = \'N\' AND m_product_category.ad_org_uu = \'' + metaDataResponseTable.ad_org_uu + '\'' + searchFilter + ' ORDER BY m_product_category.name DESC ';

            const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new ProductCategoryViewModel());
            metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);


        } catch (error) {
            apiInterface.Log(this.constructor.name, this.FindAll.name, error);
        }


        return apiInterface.Query(metaDataQuery);
    }


    FindProductCategoryViewModel(metaDataResponseTable) {

        let metaDataQuery;
        try {

            let searchFilter = '';
            if (metaDataResponseTable.search !== '') {
                searchFilter += ' AND ' + metaDataResponseTable.searchField + '%' + metaDataResponseTable.search + '%';
            }

            const sqlTemplate = '  FROM m_product_category ' +
                ' WHERE m_product_category.wx_isdeleted = \'N\' AND m_product_category.ad_org_uu = \'' + metaDataResponseTable.ad_org_uu + '\'' + searchFilter + ' ORDER BY m_product_category.name DESC ';


            const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new ProductCategoryViewModel());


            metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);

        } catch (error) {
            apiInterface.Log(this.constructor.name, this.ProductCategoryService.name, error);
        }


        return apiInterface.Query(metaDataQuery);


    }
    FindProductCategoryCMTListByMProductCategoryUu(m_product_category_uu) {

        let metaDataQuery;
        try {

            let searchFilter = '';


            const sqlTemplate = '  FROM m_product_category ' +
                ' WHERE m_product_category.m_product_category_uu = \'' + m_product_category_uu + '\'';


            const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new ProductCategoryViewModel());


            metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);

        } catch (error) {
            apiInterface.Log(this.constructor.name, this.ProductCategoryService.name, error);
        }


        return apiInterface.Query(metaDataQuery);


    }
    /**
	 * 
	
	 */
    async	SaveUpdate(productCategoryCM, datatableReference) {
        const timeNow = new Date().getTime();
        let insertSuccess = 0;
        let isNewRecord = false;
        let thisObject = this;

        try {
            /* if (HelperService.CheckValidation(thisObject.validation)) {
                 $(".productcategory-form-control").each(function () {
                     let id = $(this).attr('id');
                     let arrayId = id.split("_wx_");
                     thisObject.viewModel[arrayId[0]][arrayId[1]] = $(this).val();	// masukkan value
 
                 });*/


            if (!productCategoryCM.m_product_category.m_product_category_uu) { //new record
                isNewRecord = true;
                productCategoryCM.m_product_category.m_product_category_uu = HelperService.UUID();
                productCategoryCM.m_product_category.ad_client_uu = localStorage.getItem('ad_client_uu');
                productCategoryCM.m_product_category.ad_org_uu = localStorage.getItem('ad_org_uu');

                productCategoryCM.m_product_category.createdby = localStorage.getItem('ad_user_uu');
                productCategoryCM.m_product_category.updatedby = localStorage.getItem('ad_user_uu');

                productCategoryCM.m_product_category.created = timeNow;
                productCategoryCM.m_product_category.updated = timeNow;
            } else { //update record


                productCategoryCM.m_product_category.updatedby = localStorage.getItem('ad_user_uu');

                productCategoryCM.m_product_category.updated = timeNow;
            }

            productCategoryCM.m_product_category.sync_client = null;
            productCategoryCM.m_product_category.process_date = null;


            const sqlInsertProductCategory = HelperService.SqlInsertOrReplaceStatement('m_product_category', productCategoryCM.m_product_category);

            insertSuccess += await apiInterface.ExecuteSqlStatement(sqlInsertProductCategory);

            // jika sukses tersimpan 
            if (insertSuccess > 0) {
                if (datatableReference) {
                    let rowDataList = await thisObject.FindProductCategoryCMTListByMProductCategoryUu(productCategoryCM.m_product_category.m_product_category_uu);
                    let rowData = {};

                    if (rowDataList.length > 0) {
                        rowData = rowDataList[0];
                    }

                    if (!isNewRecord) {

                        datatableReference.row($('#bttn_row_edit' + rowData['m_product_category_wx_m_product_category_uu']).parents('tr')).data(rowData).draw();
                    } else {

                        datatableReference.row.add(rowData).draw();
                    }



                }

            }


        } catch (error) {
            apiInterface.Log(this.constructor.name, this.SaveUpdate.name, error);
        }

        return insertSuccess;


    }
}
