
class PosTransactionService {
    constructor() {

    }

    /**
	 * 
	 * @param {*} metaDataResponseTable 
	 */
    FindAll(metaDataResponseTable) {


        let otherFilters = '';
        if (metaDataResponseTable.otherFilters.length > 0) {

            _.forEach(metaDataResponseTable.otherFilters, function (value) {

                otherFilters += ' ' + value + ' ';
            });

        }

        const sqlTemplate = '  FROM pos_transaction ' +
            ' WHERE pos_transaction.wx_isdeleted = \'N\' ' + otherFilters + ' ORDER BY pos_transaction.open_date DESC ';


        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new PosTransactionComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }

    /**
     * Find PosTransaction yang statusnya bukan closed
     * return PosTransactionCMTList
     */
    FindOutstandingPosTransaction(){

        const sqlTemplate = '  FROM pos_transaction ' +
        ' WHERE pos_transaction.wx_isdeleted = \'N\' AND pos_transaction.docstatus != \'CLOSED\' ORDER BY pos_transaction.open_date DESC ';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new PosTransactionComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }

    FindPosTransactionCMTListByPosTransactionUu(pos_transaction_uu) {

        const sqlTemplate = '  FROM pos_transaction ' +
            ' WHERE pos_transaction.pos_transaction_uu = \'' + pos_transaction_uu + '\'';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new PosTransactionComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }

    

    /**
     * 
     
     */
    async    SaveUpdate(viewModel) {
        const timeNow = new Date().getTime();
        let insertSuccess = 0;
        let isNewRecord = false;
        let thisObject = this;

        try {
            if (!viewModel.pos_transaction.pos_transaction_uu) { //new record
                isNewRecord = true;
                viewModel.pos_transaction.pos_transaction_uu = HelperService.UUID();
                viewModel.pos_transaction.ad_client_uu = localStorage.getItem('ad_client_uu');
                viewModel.pos_transaction.ad_org_uu = localStorage.getItem('ad_org_uu');

                viewModel.pos_transaction.createdby = localStorage.getItem('ad_user_uu');
                viewModel.pos_transaction.updatedby = localStorage.getItem('ad_user_uu');

                viewModel.pos_transaction.created = timeNow;
                viewModel.pos_transaction.updated = timeNow;
            } else { //update record


                viewModel.pos_transaction.updatedby = localStorage.getItem('ad_user_uu');

                viewModel.pos_transaction.updated = timeNow;
            }

         
            viewModel.pos_transaction.sync_client = null;
            viewModel.pos_transaction.process_date = null;


            const sqlInsertProduct = HelperService.SqlInsertOrReplaceStatement('pos_transaction', viewModel.pos_transaction);

            insertSuccess += await apiInterface.ExecuteSqlStatement(sqlInsertProduct);
          
        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.SaveUpdate.name, error);
        }


        return insertSuccess;


    }

    

}