class EcsPosService {

    constructor() { }

    initializePrint(){

        return {command:[27,64]};
    }

    horizontalTab(){
return {command:[9]};

    }

    separator() {

        let start = [27, 33, 1];
        let character = [];
        for (let i=0; i < 62; i++) {
            character.push(45);
        }
        let end = [27, 33, 0, 10];

        let separator = start.concat(character);
        separator = separator.concat(end);
        return { command: separator };

    }

    /**
     * 
     * @param {*} printerModeOn  nilai nya 0 atau 1, tidak tahu artinya juga,
     */
    bold(printerModeOn) {
        let modeOn = 1;
        if (printerModeOn) {
            modeOn = printerModeOn;
        }
        return { command: [27, 69, modeOn] };
    }

    /**
    * 
    * @param {*} printerModeOn  nilai nya 0 atau 1, tidak tahu artinya juga,
    */
    underline(printerModeOn) {
        let modeOn = 1;
        if (printerModeOn) {
            modeOn = printerModeOn;
        }
        return { command: [27, 45, modeOn] };
    }

    font_normal() {

        return { command: [29, 33, 0] };

    }
    font_doubleWidth2() {

        return { command: [29, 33, 16] };
    }

    font_doubleWidth3() {

        return { command: [29, 33, 32] };
    }

    alignment_left() {
        return { command: [27, 97, 0] };
    }
    alignment_center() {
        return { command: [27, 97, 1] };
    }

    alignment_right() {
        return { command: [27, 97, 2] };
    }

    text(value) {

        return { text: value };
    }

    lineFeeds(n){
        let feeds = [10];
        if (n){
            for(let i = 0; i < n; i++){
                feeds.push(10);
            }
        }
       

        return {command: feeds};
    }

    fullCut(){

        return {command:[29,86,103]};
    }

    partialCut(){

        return {command:[29,86,103]};
    }


}