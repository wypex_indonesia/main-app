class ChargeService {
    constructor() { }

    /**
 * Find All warehouse dengan bentuk warehouseviewmodel
 * @param {*} metaDataResponseTable 
 */
    FindAll() {


        const sqlTemplate = '  FROM c_charge ORDER BY c_charge.name ASC ';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new ChargeComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }



}