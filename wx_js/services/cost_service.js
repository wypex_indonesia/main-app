/**
 * Table MCost untuk menghitung cost setiap product
 */
class CostService {

    constructor() { }



    /**
   * Save MCost memiliki metode yang berbeda berdasarkan costelement
   * 1. CostElement -> Last PO, hanya satu dalam MCost
   * 2. Overhead -> Overhead, bisa lebih dari satu
   * 3. Labor -> bisa banyak
   * @param {*} m_cost 
   */
    SaveUpdate(m_cost) {
        const timeNow = new Date().getTime();
        let thisObject = this;
        //  let elementValueService = new ElementValueService();
        let insertSuccess = 0;

        try {

            return new Promise(function (resolve, reject) {

                // cari cost   existing product untuk LastPO
                // jika ada , maka update currentcostprice saja
                thisObject.FindCostByMProductUuAndCostElement(m_cost.m_product_uu, m_cost.m_costelement).then(function (listcostViewModelTable) {

                    if (listcostViewModelTable.length) {
                        let costViewModelTable = listcostViewModelTable[0];
                        let costViewModel = HelperService.ConvertRowDataToViewModel(costViewModelTable);
                        let currentcostpriceUpdated = m_cost.currentcostprice;
                        m_cost = costViewModel.m_cost;
                        m_cost.currentcostprice = currentcostpriceUpdated;
                    }

                    if (!m_cost.m_cost_uu) { //new record

                        m_cost.m_cost_uu = HelperService.UUID();
                        m_cost.ad_client_uu = localStorage.getItem('ad_client_uu');
                        m_cost.ad_org_uu = localStorage.getItem('ad_org_uu');

                        m_cost.createdby = localStorage.getItem('ad_user_uu');
                        m_cost.updatedby = localStorage.getItem('ad_user_uu');

                        m_cost.created = timeNow;
                        m_cost.updated = timeNow;
                    } else { //update record


                        m_cost.updatedby = localStorage.getItem('ad_user_uu');

                        m_cost.updated = timeNow;
                    }

                    m_cost.sync_client = null;
                    m_cost.process_date = null;


                    const sqlInsert = HelperService.SqlInsertOrReplaceStatement('m_cost', m_cost);


                    apiInterface.ExecuteSqlStatement(sqlInsert).then(function (rowChanged) {
                        insertSuccess += rowChanged;

                        resolve(insertSuccess);
                    });

                });



            });

        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.SaveUpdate.name, error);
            return new Promise(function (resolve, reject) {
                resolve(insertSuccess);

            });
        }


    }

  

    /**
     * Mendapatkan cost terakhir dari 
     * @param {*} m_product_uu 
     */
    FindByMProductUuAndCostElement(m_product_uu, m_costelement) {



            const sqlTemplate = '  FROM m_cost ' +
                ' INNER JOIN m_product ON m_product.m_product_uu = m_cost.m_product_uu ' +
                ' WHERE m_cost.wx_isdeleted = \'N\' AND m_cost.m_costelement = \'' + m_costelement + '\' AND  m_product.m_product_uu = \'' + m_product_uu + '\' ORDER BY m_cost.updated DESC';

            const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new CostViewModel());

            let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


            return apiInterface.Query(metaDataQuery);


        }


    FindCostByMProductUuAndCostElement(m_product_uu, m_costelement) {
            const sqlTemplate = '  FROM m_cost ' +
                ' INNER JOIN m_product ON m_product.m_product_uu = m_cost.m_product_uu ' +
                ' WHERE m_cost.wx_isdeleted = \'N\' AND m_cost.m_costelement = \'' + m_costelement + '\' AND   m_cost.m_product_uu = \'' + m_product_uu + '\' ';

            const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new CostViewModel());

            let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


            return apiInterface.Query(metaDataQuery);

        }

    /*  GetCostCMTByMProductUu(m_product_uu) {
         const sqlTemplate = '  FROM m_cost ' +
             ' INNER JOIN m_product ON m_product.m_product_uu = m_cost.m_product_uu ' +
             ' WHERE m_cost.wx_isdeleted = \'N\' AND m_cost.m_costelement = \'Last PO\' AND   m_cost.m_product_uu = \'' + m_product_uu + '\' ';
 
         const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new CostViewModel());
 
         let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);
 
         let costCMTList = apiInterface.Query(metaDataQuery);
         let costCMT = null;
         if (costCMTList.length) {
             costCMT = costCMTList[0];
         }
 
         return costCMT;
 
     }*/

    /**
     * Get total cost dari semua cost element pada sebuah product 
     * @param {*} m_product_uu 
     * @return double totalcost
     */
    GetTotalCostByMProductUu(m_product_uu) {
            const sqlTemplate = 'SELECT SUM(m_cost.currentcostprice) currentcostprice FROM m_cost WHERE m_cost.wx_isdeleted = \'N\' AND m_cost.m_product_uu = \'' + m_product_uu + '\'; ';



            return new Promise(function (resolve, reject) {
                apiInterface.DirectQuerySqlStatement(sqlTemplate).then(function (totalCostList) {

                    let currentcostprice = 0.0;
                    if (totalCostList.length) {
                        let totalCost = totalCostList[0];
                        currentcostprice = totalCost.currentcostprice;
                    }
                    resolve(currentcostprice);


                });


            });


        }

    GetProductAsset(m_product_uu, m_costelement, quantity) {

            let thisObject = this;
            return new Promise(function (resolve, reject) {

                thisObject.FindCostByMProductUuAndCostElement(m_product_uu, m_costelement).then(function (listMCost) {
                    let mcostCMT = listMCost[0];

                    resolve(quantity * mcostCMT.m_cost_wx_currentcostprice);


                });


            });

        }

}