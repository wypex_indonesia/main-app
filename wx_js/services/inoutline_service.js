

class InoutlineService {
    constructor() { }


    /**
	 * 
	 * @param {*} metaDataResponseTable 
	 */
    FindAll(metaDataResponseTable) {


        let otherFilters = '';
        if (metaDataResponseTable.otherFilters.length > 0) {

            _.forEach(metaDataResponseTable.otherFilters, function (value) {

                otherFilters += ' ' + value + ' ';
            });

        }

        const sqlTemplate = '  FROM m_inoutline ' +
            ' INNER JOIN m_inout ON m_inoutline.m_inout_uu = m_inout.m_inout_uu ' +
            ' INNER JOIN m_product ON m_product.m_product_uu  = m_inoutline.m_product_uu ' +
            ' INNER JOIN c_uom ON m_inoutline.c_uom_uu = c_uom.c_uom_uu ' +
            ' INNER JOIN c_uom as c_uom_based ON c_uom_based.c_uom_uu = m_product.c_uom_uu ' +
            ' INNER JOIN c_orderline ON c_orderline.c_orderline_uu = m_inoutline.c_orderline_uu ' +
            ' INNER JOIN m_locator ON m_locator.m_locator_uu  = m_inoutline.m_locator_uu ' +
            ' WHERE m_inoutline.wx_isdeleted = \'N\' ' + otherFilters + ' ORDER BY m_inoutline.updated ASC ';



        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new InoutlineViewModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);

    }

    FindInoutlineViewModelByMInoutlineUu(m_inoutline_uu) {

        const sqlTemplate = '  FROM m_inoutline ' +
            ' INNER JOIN m_inout ON m_inoutline.m_inout_uu = m_inout.m_inout_uu ' +
            ' INNER JOIN m_product ON m_product.m_product_uu  = m_inoutline.m_product_uu ' +
            ' INNER JOIN c_uom ON m_inoutline.c_uom_uu = c_uom.c_uom_uu ' +
            ' INNER JOIN c_uom as c_uom_based ON c_uom_based.c_uom_uu = m_product.c_uom_uu ' +
            ' INNER JOIN c_orderline ON c_orderline.c_orderline_uu = m_inoutline.c_orderline_uu ' +
            ' INNER JOIN m_locator ON m_locator.m_locator_uu  = m_inoutline.m_locator_uu ' +
            ' WHERE m_inoutline.m_inoutline_uu = \'' + m_inoutline_uu + '\'';



        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new InoutlineViewModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }

    FindInoutlineViewModelByMInoutUu(m_inout_uu) {

        const sqlTemplate = ' FROM m_inoutline ' +
            ' INNER JOIN m_inout ON m_inoutline.m_inout_uu = m_inout.m_inout_uu ' +
            ' INNER JOIN m_product ON m_product.m_product_uu  = m_inoutline.m_product_uu ' +
            ' INNER JOIN c_uom ON m_inoutline.c_uom_uu = c_uom.c_uom_uu ' +
            ' INNER JOIN c_uom as c_uom_based ON c_uom_based.c_uom_uu = m_product.c_uom_uu ' +
            ' INNER JOIN c_orderline ON c_orderline.c_orderline_uu = m_inoutline.c_orderline_uu ' +
            ' INNER JOIN m_locator ON m_locator.m_locator_uu  = m_inoutline.m_locator_uu ' +
            ' WHERE m_inoutline.wx_isdeleted = \'N\' AND m_inoutline.m_inout_uu = \'' + m_inout_uu + '\'';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new InoutlineViewModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }


    FindInoutlineViewModelByCOrderUu(c_order_uu) {

        const sqlTemplate = '  FROM m_inoutline ' +
            ' INNER JOIN m_inout ON m_inoutline.m_inout_uu = m_inout.m_inout_uu ' +
            ' INNER JOIN m_product ON m_product.m_product_uu  = m_inoutline.m_product_uu ' +
            ' INNER JOIN c_uom ON m_inoutline.c_uom_uu = c_uom.c_uom_uu ' +
            ' INNER JOIN c_orderline ON c_orderline.c_orderline_uu = m_inoutline.c_orderline_uu ' +
            ' INNER JOIN m_locator ON m_locator.m_locator_uu  = m_inoutline.m_locator_uu ' +
            ' WHERE m_inoutline.m_inoutline_uu = \'' + m_inoutline_uu + '\'';



        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new InoutlineViewModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }


    GenerateInoutlineFromCOrderUu(inoutViewModelTable) {
        let thisObject = this;
        let orderlineService = new OrderlineService();
        let locatorService = new LocatorService();
        let timeNow = new Date().getTime();
        let objDataFlow = {};
        return new Promise(function (resolve, reject) {
            orderlineService.FindOrderlineViewModelByCOrderUu(inoutViewModelTable.m_inout_wx_c_order_uu)
                .then(function (orderlineViewModelTableList) {
                    objDataFlow['orderline_cmt_list'] = orderlineViewModelTableList;
                    resolve(objDataFlow);
                });
        }).then(function (objDataFlow) {

            return new Promise(function (resolve, reject) {
                locatorService.FindLocatorComponentModelByMWarehouseUu(inoutViewModelTable.m_inout_wx_m_warehouse_uu)
                    .then(function (locatorComponentModelTableList) {
                        objDataFlow['locator_cmt_list'] = locatorComponentModelTableList;
                        resolve(objDataFlow);
                    });
            });
        }).then(function (objDataFlow) {


            return new Promise(function (resolve, reject) {
                let mLocatorComponentModelTable = {};
                if (objDataFlow['locator_cmt_list'].length) {
                    mLocatorComponentModelTable = objDataFlow['locator_cmt_list'][0];
                }
                let sqlInsertArray = [];
                _.forEach(objDataFlow['orderline_cmt_list'], function (objectViewModelTable, index) {

                  
                    let inoutline = new MInoutline();

                    inoutline.isreturn = objectViewModelTable.c_orderline_wx_isreturn;


                    inoutline.m_inoutline_uu = HelperService.UUID();
                    inoutline.ad_client_uu = localStorage.getItem('ad_client_uu');
                    inoutline.ad_org_uu = localStorage.getItem('ad_org_uu');
                    inoutline.line = ++index;
                    inoutline.m_product_uu = objectViewModelTable.c_orderline_wx_m_product_uu;
                    inoutline.c_uom_uu = objectViewModelTable.c_orderline_wx_c_uom_uu;
                    inoutline.qtyentered = objectViewModelTable.c_orderline_wx_qtyentered;
                    inoutline.movementqty = objectViewModelTable.c_orderline_wx_qtyordered;
                    inoutline.m_inout_uu = inoutViewModelTable.m_inout_wx_m_inout_uu;
                    inoutline.m_locator_uu = mLocatorComponentModelTable.m_locator_wx_m_locator_uu;
                    inoutline.c_orderline_uu = objectViewModelTable.c_orderline_wx_c_orderline_uu;
                    inoutline.created = timeNow;
                    inoutline.updated = timeNow;
                    inoutline.updatedby = localStorage.getItem('ad_user_uu');
                    inoutline.createdby = localStorage.getItem('ad_user_uu');
                    inoutline.sync_client = null;
                    inoutline.process_date = null;
                   

                    const sqlInsert = thisObject.GetRawInsertSqlStatement(inoutline);

                    sqlInsertArray.push(sqlInsert);

                   
                });
                resolve(sqlInsertArray);
            });
        }).then(function (sqlInsertArray) {

            return new Promise(function (resolve, reject) {
                apiInterface.ExecuteBulkSqlStatement(sqlInsertArray).then(function (inserted) {
                    resolve(inserted);

                });

            });
        }).then(function (finalResult) {

            //jumlah inserted yang success
            return finalResult;

        });

    }


    /**
     * Save Update harus merefer pada OrderViewModelTable
     * @param {MInoutlineModel} inoutline
     * @param {*} datatableReference dataTableReferemce MInout datatable
     */
    SaveUpdate(inoutline, datatableReference) {
        let thisObject = this;
        const timeNow = new Date().getTime();



        inoutline.updatedby = localStorage.getItem('ad_user_uu');

        inoutline.updated = timeNow;

        inoutline.sync_client = null;
        inoutline.process_date = null;

        let insertSuccess = 0;
        const sqlInsert = thisObject.GetRawInsertSqlStatement(inoutline);


        return new Promise(function (resolve, reject) {
            apiInterface.ExecuteSqlStatement(sqlInsert).then(function (rowChanged) {
                insertSuccess = rowChanged;
                if (insertSuccess > 0) {


                    thisObject.FindInoutlineViewModelByMInoutlineUu(inoutline.m_inoutline_uu)
                        .then(function (rowDataList) {

                            let rowData = {};

                            if (rowDataList.length > 0) {
                                rowData = rowDataList[0];
                            }

                            datatableReference.row($('#bttn_row_edit' + rowData['m_inoutline_wx_m_inoutline_uu']).parents('tr')).data(rowData).draw();

                            resolve(insertSuccess);

                        });




                } else {
                    resolve(insertSuccess);
                }

            });


        });
    }

    /**
     * Semua fungsi yang akan mengenerate sql insert statemnent untuk inoutline, harus menggunakan fungsi ini
     * karena isreturn , movementqty harus negatif
     * @param {*} m_inoutline 
     */
    GetRawInsertSqlStatement(m_inoutline){

        // maka cek apakah sudah negatif, kalau sudah negatif, tidak perlu diubah
        if (m_inoutline.isreturn === 'Y'){
            if (m_inoutline.movementqty > 0){
                m_inoutline.movementqty = m_inoutline.movementqty * -1;
            }
        }

        return HelperService.SqlInsertOrReplaceStatement('m_inoutline', m_inoutline);


    }


}