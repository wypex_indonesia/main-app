class ReceiptFormatService {


    constructor() { }

    GetReceipt(pos_c_order_uu) {
        let ecs_pos_service = new EcsPosService();

        let listObjPrint = [];

        return new Promise(function (resolve, reject) {
            let pos_service = new PosService();
            let organization_service = new OrganizationService();
            let promiseArray = [];
            promiseArray.push(organization_service.FindOrganizationComponentModelByAdOrgUu(localStorage.getItem("ad_org_uu")));
            promiseArray.push(pos_service.FindCartCMTByCOrderUu(pos_c_order_uu));
            promiseArray.push(pos_service.FindCartlineCMTListByCOrderUu(pos_c_order_uu));

            Promise.all(promiseArray).then(function (resultArray) {

                resolve(resultArray);


            });


        }).then(function (resultArray) {

            return new Promise(function (resolve, reject) {

                let adOrgCMTList = resultArray[0];
                let adOrgCMT = adOrgCMTList[0];

                let cartCMTList = resultArray[1];
                let cartCMT = cartCMTList[0];

                let cartlineCMTList = resultArray[2];

                let adUserService = new UserService();
                adUserService.FindUserCMTListByAdUserUu(cartCMT.pos_c_order_wx_updatedby).then(function (userCMTList) {
                    let userCMT = userCMTList[0];

                    listObjPrint.push(ecs_pos_service.initializePrint());
                    listObjPrint.push(ecs_pos_service.lineFeeds(3));
                    listObjPrint.push(ecs_pos_service.bold());
                    listObjPrint.push(ecs_pos_service.alignment_center());
                    //name organization
                    listObjPrint.push(ecs_pos_service.text(adOrgCMT.ad_org_wx_name));
                    listObjPrint.push(ecs_pos_service.lineFeeds());
                    listObjPrint.push(ecs_pos_service.text(adOrgCMT.c_location_wx_address1));
                    listObjPrint.push(ecs_pos_service.lineFeeds());
                    listObjPrint.push(ecs_pos_service.text(adOrgCMT.c_location_wx_city + ", " + adOrgCMT.c_location_wx_regionname + " " + adOrgCMT.c_location_wx_postal));
                    listObjPrint.push(ecs_pos_service.lineFeeds());

                    listObjPrint.push(ecs_pos_service.separator());

                    // header c_order

                    listObjPrint.push(ecs_pos_service.font_normal());
                    listObjPrint.push(ecs_pos_service.alignment_left());
                    listObjPrint.push(ecs_pos_service.lineFeeds());
                    listObjPrint.push(ecs_pos_service.text("No Receipt : " + cartCMT.pos_c_order_wx_documentno));
                    listObjPrint.push(ecs_pos_service.lineFeeds());
                    //tanggal jam
                    listObjPrint.push(ecs_pos_service.text("Tanggal : " + HelperService.ConvertTimestampToLocateDateTimeString(full.fact_acct_wx_updated)));
                    listObjPrint.push(ecs_pos_service.lineFeeds());

                    
                    listObjPrint.push(ecs_pos_service.text("Kasir : " + userCMT.ad_user_wx_name));

                    listObjPrint.push(ecs_pos_service.lineFeeds());
                    listObjPrint.push(ecs_pos_service.separator());


                    /**  ITEM DETAIL  */
                    _.forEach(cartlineCMTList, function(cartlineCMT){
                        if (cartlineCMT.pos_c_orderline_wx_upc){
                            let upcNamabarang = cartlineCMT.pos_c_orderline_wx_upc + " " + cartlineCMT.pos_c_orderline_wx_name;
                            listObjPrint.push(ecs_pos_service.text(upcNamabarang));
                            listObjPrint.push(ecs_pos_service.lineFeeds());
                        }else{
                            let skuNamabarang = cartlineCMT.pos_c_orderline_wx_sku + " " + cartlineCMT.pos_c_orderline_wx_name;
                            listObjPrint.push(ecs_pos_service.text(upcNamabarang));
                            listObjPrint.push(ecs_pos_service.lineFeeds());
                        }
                        let item = cartlineCMT.pos_c_orderline_wx_qtyentered + " " + cartlineCMT.pos_c_orderline_wx_priceentered + " " + cartlineCMT.pos_c_orderline_wx_linenetamt;
                        listObjPrint.push(ecs_pos_service.text(item));
                      
                        listObjPrint.push(ecs_pos_service.lineFeeds());

                    });

                    


                    /** TOTAL FOOTER */
                    listObjPrint.push(ecs_pos_service.separator());
                    //jika ada PPN
                    if (cartCMT.pos_c_order_wx_totaltaxlines){

                        let totalLines = "TOTAL     " + cartCMT.pos_c_order_wx_totallines;
                        listObjPrint.push(ecs_pos_service.text(totalLines));
                        listObjPrint.push(ecs_pos_service.lineFeeds());

                        let ppn = "PPN      " + cartCMT.pos_c_order_wx_totaltaxlines;                      
                        listObjPrint.push(ecs_pos_service.text(ppn));
                        listObjPrint.push(ecs_pos_service.lineFeeds());                    
                    }

                    let grandTotal = "Grand Total      " + cartCMT.pos_c_order_wx_grandtotal;                      
                    listObjPrint.push(ecs_pos_service.text(grandTotal));
                    listObjPrint.push(ecs_pos_service.lineFeeds());    
                    listObjPrint.push(ecs_pos_service.lineFeeds());    
                    listObjPrint.push(ecs_pos_service.lineFeeds());    
                    listObjPrint.push(ecs_pos_service.lineFeeds());    
                    listObjPrint.push(ecs_pos_service.lineFeeds());    

                    resolve(listObjPrint);



                });





            });

        });


    }

}