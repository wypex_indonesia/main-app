/**
 * APP  (AP Payment) 5001
 * ARR (AR Receipt)  6001
 */
class PaymentService {
    constructor() {

    }

    /**
	 * 
	 * @param {*} metaDataResponseTable 
	 */
    FindAll(metaDataResponseTable) {


        let otherFilters = '';
        if (metaDataResponseTable.otherFilters.length > 0) {

            _.forEach(metaDataResponseTable.otherFilters, function (value) {

                otherFilters += ' ' + value + ' ';
            });

        }

        const sqlTemplate = '  FROM c_payment ' +
            ' INNER JOIN c_bpartner ON c_payment.c_bpartner_uu = c_bpartner.c_bpartner_uu ' +
            ' INNER JOIN c_invoice ON c_payment.c_invoice_uu  = c_invoice.c_invoice_uu ' +
            ' INNER JOIN c_bankaccount ON c_payment.c_bankaccount_uu  = c_bankaccount.c_bankaccount_uu ' +
            ' INNER JOIN c_order ON c_order.c_order_uu = c_payment.c_order_uu ' +
            ' INNER JOIN c_doctype ON c_payment.c_doctype_id = c_doctype.c_doctype_id ' +
            ' WHERE c_payment.wx_isdeleted = \'N\' ' + otherFilters + ' ORDER BY c_payment.documentno DESC ';


        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new PaymentComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);


        return apiInterface.Query(metaDataQuery);
    }

    FindPaymentComponentModelByCPaymentUu(c_payment_uu) {

        const sqlTemplate = '  FROM c_payment ' +
            ' INNER JOIN c_bpartner ON c_payment.c_bpartner_uu = c_bpartner.c_bpartner_uu ' +
            ' INNER JOIN c_invoice ON c_payment.c_invoice_uu  = c_invoice.c_invoice_uu ' +
            ' INNER JOIN c_bankaccount ON c_payment.c_bankaccount_uu  = c_bankaccount.c_bankaccount_uu ' +
            ' INNER JOIN c_order ON c_order.c_order_uu = c_payment.c_order_uu ' +
            ' INNER JOIN c_doctype ON c_payment.c_doctype_id = c_doctype.c_doctype_id ' +
            ' WHERE c_payment.c_payment_uu = \'' + c_payment_uu + '\' ';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new PaymentComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);


        return apiInterface.Query(metaDataQuery);
    }

    FindPaymentComponentModelByCOrderUu(c_order_uu) {

        const sqlTemplate = '  FROM c_payment ' +
            ' INNER JOIN c_bpartner ON c_payment.c_bpartner_uu = c_bpartner.c_bpartner_uu ' +
            ' INNER JOIN c_invoice ON c_payment.c_invoice_uu  = c_invoice.c_invoice_uu ' +
            ' INNER JOIN c_bankaccount ON c_payment.c_bankaccount_uu  = c_bankaccount.c_bankaccount_uu ' +
            ' INNER JOIN c_order ON c_order.c_order_uu = c_payment.c_order_uu ' +
            ' INNER JOIN c_doctype ON c_payment.c_doctype_id = c_doctype.c_doctype_id ' +
            ' WHERE c_payment.c_order_uu = \'' + c_order_uu + '\' ';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new PaymentComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);


        return apiInterface.Query(metaDataQuery);
    }


    FindExpenseCMT(metaDataResponseTable) {


        let otherFilters = '';
        if (metaDataResponseTable.otherFilters.length > 0) {

            _.forEach(metaDataResponseTable.otherFilters, function (value) {

                otherFilters += ' ' + value + ' ';
            });

        }

        const sqlTemplate = '  FROM c_payment ' +
            ' INNER JOIN c_bankaccount ON c_payment.c_bankaccount_uu  = c_bankaccount.c_bankaccount_uu ' +
            ' INNER JOIN c_charge ON c_charge.c_charge_uu = c_payment.c_charge_uu ' +
            ' INNER JOIN c_doctype ON c_payment.c_doctype_id = c_doctype.c_doctype_id ' +
            ' WHERE c_payment.wx_isdeleted = \'N\' ' + otherFilters + ' ORDER BY c_payment.documentno DESC ';


        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new ExpenseComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);


        return apiInterface.Query(metaDataQuery);
    }

    FindExpenseCMByCPaymentUu(c_payment_uu) {

        const sqlTemplate = '  FROM c_payment ' +
            ' INNER JOIN c_bankaccount ON c_payment.c_bankaccount_uu  = c_bankaccount.c_bankaccount_uu ' +
            ' INNER JOIN c_charge ON c_charge.c_charge_uu = c_payment.c_charge_uu ' +
            ' INNER JOIN c_doctype ON c_payment.c_doctype_id = c_doctype.c_doctype_id ' +
            ' WHERE c_payment.c_payment_uu = \'' + c_payment_uu + '\' ';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new ExpenseComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);


        return apiInterface.Query(metaDataQuery);
    }


    UpdateStatus(componentModelTable) {
        let insertSuccess = 0;
        let sqlstatement = '';
        let thisObject = this;

        try {
            if (componentModelTable.c_payment_wx_docstatus === 'DELETED') {
                sqlstatement = HelperService.SqlDeleteStatement('c_payment', 'c_payment_uu', componentModelTable.c_payment_wx_c_payment_uu);

            } else {


                sqlstatement = HelperService.SqlUpdateStatusStatement('c_payment', 'c_payment_uu',
                    componentModelTable.c_payment_wx_c_payment_uu, componentModelTable.c_payment_wx_docstatus);
            }

            return new Promise(function (resolve, reject) {

                apiInterface.ExecuteSqlStatement(sqlstatement)
                    .then(function (rowChanged) {
                        insertSuccess += rowChanged;

                        resolve(insertSuccess);

                    });



            }).then(function (rowChanged) {
                return new Promise(function (resolve, reject) {

                    if (insertSuccess) {
                        thisObject.RunProcessDocument(componentModelTable).then(function (inserted) {

                            insertSuccess += inserted;
                            resolve(insertSuccess);

                        });
                    } else {
                        resolve(insertSuccess);
                    }



                });


            });
        } catch (error) {
            apiInterface.Log(this.constructor.name, this.UpdateStatus.name, error);
        }


    }

    /**
     * 
     * @param {*} paymentComponentModel 
     * @param {*} datatableReference dataTableReferemce CInvoice datatable
     */
    SaveUpdate(paymentComponentModel, datatableReference) {

        let thisObject = this;
        let isNewRecord = false;
        let insertSuccess = 0;

        try {

            if (!paymentComponentModel.c_payment.c_payment_uu) { //new record
                isNewRecord = true;
            }

            const sqlInsert = thisObject.GetSqlInsertStatement(paymentComponentModel);

            return new Promise(function (resolve, reject) {

                apiInterface.ExecuteSqlStatement(sqlInsert).then(function (rowChanged) {
                    insertSuccess += rowChanged;
                    if (insertSuccess > 0) {

                        if (datatableReference) {
                            thisObject.FindPaymentComponentModelByCPaymentUu(paymentComponentModel.c_payment.c_payment_uu)
                                .then(function (rowDataList) {
                                    let rowData = {};

                                    if (rowDataList.length > 0) {
                                        rowData = rowDataList[0];
                                    }

                                    if (!isNewRecord) {

                                        datatableReference.row($('#bttn_row_edit' + rowData['c_payment_wx_c_payment_uu']).parents('tr')).data(rowData).draw();
                                    } else {

                                        datatableReference.row.add(rowData).draw();
                                    }
                                    resolve(insertSuccess);

                                });


                        }

                        resolve(insertSuccess);
                    }

                });


            });


        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.SaveUpdate.name, error);
            return new Promise(function (resolve, reject) {
                resolve(insertSuccess);

            });
        }


    }

    GetSqlInsertStatement(paymentComponentModel) {
        const timeNow = new Date().getTime();

        let sqlInsert = '';

        let thisObject = this;
        try {
            if (!paymentComponentModel.c_payment.c_payment_uu) { //new record

                paymentComponentModel.c_payment.c_payment_uu = HelperService.UUID();
                paymentComponentModel.c_payment.ad_client_uu = localStorage.getItem('ad_client_uu');
                paymentComponentModel.c_payment.ad_org_uu = localStorage.getItem('ad_org_uu');

                paymentComponentModel.c_payment.createdby = localStorage.getItem('ad_user_uu');
                paymentComponentModel.c_payment.updatedby = localStorage.getItem('ad_user_uu');

                paymentComponentModel.c_payment.created = timeNow;
                paymentComponentModel.c_payment.updated = timeNow;
            } else { //update record


                paymentComponentModel.c_payment.updatedby = localStorage.getItem('ad_user_uu');

                paymentComponentModel.c_payment.updated = timeNow;
            }


            paymentComponentModel.c_payment.c_order_uu = paymentComponentModel.c_order.c_order_uu;
            paymentComponentModel.c_payment.c_bpartner_uu = paymentComponentModel.c_bpartner.c_bpartner_uu;
            paymentComponentModel.c_payment.c_invoice_uu = paymentComponentModel.c_invoice.c_invoice_uu;
            paymentComponentModel.c_payment.c_bankaccount_uu = paymentComponentModel.c_bankaccount.c_bankaccount_uu;
            paymentComponentModel.c_payment.c_doctype_id = paymentComponentModel.c_doctype.c_doctype_id;
            paymentComponentModel.c_payment.payamt = paymentComponentModel.c_invoice.grandtotal;
            paymentComponentModel.c_payment.sync_client = null;
            paymentComponentModel.c_payment.process_date = null;
            sqlInsert = HelperService.SqlInsertOrReplaceStatement('c_payment', paymentComponentModel.c_payment);

        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.GetSqlInsert.name, error);
        }

        return sqlInsert;


    }

    /**
   * 
   * @param {ExpenseComponentModel} expenseComponentModel 
   * @param {*} datatableReference dataTableReferemce CInvoice datatable
   */
    SaveUpdateExpenseCM(expenseComponentModel, datatableReference) {
        let thisObject = this;
        const timeNow = new Date().getTime();
        let insertSuccess = 0;
        let isNewRecord = false;

        try {
            if (!expenseComponentModel.c_payment.c_payment_uu) { //new record
                isNewRecord = true;
                expenseComponentModel.c_payment.c_payment_uu = HelperService.UUID();
                expenseComponentModel.c_payment.ad_client_uu = localStorage.getItem('ad_client_uu');
                expenseComponentModel.c_payment.ad_org_uu = localStorage.getItem('ad_org_uu');

                expenseComponentModel.c_payment.createdby = localStorage.getItem('ad_user_uu');
                expenseComponentModel.c_payment.updatedby = localStorage.getItem('ad_user_uu');

                expenseComponentModel.c_payment.created = timeNow;
                expenseComponentModel.c_payment.updated = timeNow;
            } else { //update record


                expenseComponentModel.c_payment.updatedby = localStorage.getItem('ad_user_uu');

                expenseComponentModel.c_payment.updated = timeNow;
            }


            expenseComponentModel.c_payment.c_bankaccount_uu = expenseComponentModel.c_bankaccount.c_bankaccount_uu;
            expenseComponentModel.c_payment.c_doctype_id = expenseComponentModel.c_doctype.c_doctype_id;
            expenseComponentModel.c_payment.c_charge_uu = expenseComponentModel.c_charge.c_charge_uu;
            expenseComponentModel.c_payment.sync_client = null;
            expenseComponentModel.c_payment.process_date = null;


            const sqlInsert = HelperService.SqlInsertOrReplaceStatement('c_payment', expenseComponentModel.c_payment);

            return new Promise(function (resolve, reject) {

                apiInterface.ExecuteSqlStatement(sqlInsert)
                    .then(function (rowChanged) {

                        insertSuccess += rowChanged;
                        if (insertSuccess > 0) {

                            if (datatableReference) {
                                thisObject.FindExpenseCMByCPaymentUu(expenseComponentModel.c_payment.c_payment_uu)
                                    .then(function (rowDataList) {

                                        let rowData = {};

                                        if (rowDataList.length > 0) {
                                            rowData = rowDataList[0];
                                        }

                                        if (!isNewRecord) {

                                            datatableReference.row($('#bttn_row_edit' + rowData['c_payment_wx_c_payment_uu']).parents('tr')).data(rowData).draw();
                                        } else {

                                            datatableReference.row.add(rowData).draw();
                                        }
                                        resolve(insertSuccess);

                                    });
                            } else {
                                resolve(insertSuccess);

                            }




                        }



                    });


            });

        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.SaveUpdateExpenseCM.name, error);
            return new Promise(function (resolve, reject) { resolve(0); });
        }





    }



    /**
     * Run Process Document sesuai dengan docstatus :
     * 1. Draft -> Tidak ada Action
     * docbasetype :
     * AP Payment
     * 
     * AP Invoice
     * 2. COMPLETED
     *  2.2. FactAcct 
     * 3. REVERSED
     *  2.1. FactAcct -> Reversed
     * 
     * @param {*} componentModelTable , paymentCMT atau expenseCMT
     */
    RunProcessDocument(componentModelTable) {
        let factacctService = new FactAcctService();
        // let bankstatementService = new BankstatementService();
        //  let bankstatementlineService = new BankstatementlineService();
        let thisObject = this;
        try {
            if (componentModelTable.c_payment_wx_docstatus === 'COMPLETED') {


                if (componentModelTable.c_doctype_wx_docbasetype === 'APP') {
                    return thisObject.APPaymentCompleted(componentModelTable);
                } else if (componentModelTable.c_doctype_wx_docbasetype === 'ARR') {
                    return thisObject.ARReceiptCompleted(componentModelTable);
                } else if (componentModelTable.c_doctype_wx_c_doctype_id === 5002) {
                    // AP Expense
                    return thisObject.APExpenseCompleted(componentModelTable);
                } else {
                    return new Promise(function (resolve, reject) {

                        resolve(1);
                    });
                }


            } else if (componentModelTable.c_payment_wx_docstatus === 'REVERSED') {

                if (componentModelTable.c_doctype_wx_docbasetype === 'APP') {
                    return thisObject.APPaymentReversed(componentModelTable);
                } else if (componentModelTable.c_doctype_wx_docbasetype === 'ARR') {

                    return thisObject.ARReceiptReversed(componentModelTable);
                } else if (componentModelTable.c_doctype_wx_c_doctype_id === 5002) {

                    return thisObject.APExpenseReversed(componentModelTable);
                } else {
                    return new Promise(function (resolve, reject) {
                        resolve(1);

                    });

                }


            } else {
                // draft and closed document
                return new Promise(function (resolve, reject) {

                    resolve(1);
                });


            }
        } catch (error) {
            apiInterface.Log(this.constructor.name, this.RunProcessDocument.name, error);
        }

    }

    APPaymentReversed(paymentComponentModelTable) {
        let elementValueService = new ElementValueService();
        let factacctService = new FactAcctService();
        let thisObject = this;
        // REVERSE PAYMENT SELECTION
        let insertSuccess = 0;
        let sqlInsertPaymentSelection = factacctService.sqlInsertReverseFactAcctStatement('c_payment', paymentComponentModelTable.c_payment_wx_c_payment_uu, elementValueService.ElementValueList().PaymentSelection.value);

        // CHECKING IN TRANSFER
        let sqlInsertCheckingInTransfer = factacctService.sqlInsertReverseFactAcctStatement('c_payment', paymentComponentModelTable.c_payment_wx_c_payment_uu, paymentComponentModelTable.c_bankaccount_wx_transit_element);


        return new Promise(function (resolve, reject) {

            apiInterface.ExecuteBulkSqlStatement([sqlInsertPaymentSelection, sqlInsertCheckingInTransfer]).then(function (inserted) {
                insertSuccess += inserted;
                resolve(insertSuccess);
            });

        }).then(function () {

            return new Promise(function (resolve, reject) {
                let bankstatementService = new BankstatementService();
                bankstatementService.SetStatusAndRunProcess(paymentComponentModelTable.c_payment_wx_c_payment_uu,
                    paymentComponentModelTable.c_doctype_wx_c_doctype_id, 'REVERSED').then(function (inserted) {
                        insertSuccess += inserted;
                        resolve(insertSuccess);

                    });

            });

        });


    }







    ARReceiptReversed(paymentComponentModelTable) {
        let elementValueService = new ElementValueService();
        let factacctService = new FactAcctService();
        let thisObject = this;
        let insertSuccess = 0;

        // CHECKING IN TRANSFER 

        let sqlInsertPaymentSelection = factacctService.sqlInsertReverseFactAcctStatement('c_payment', paymentComponentModelTable.c_payment_wx_c_payment_uu,paymentComponentModelTable.c_bankaccount_wx_transit_element);

        //Checking Unallocated Receipt
        let sqlInsertCheckingInTransfer = factacctService.sqlInsertReverseFactAcctStatement('c_payment', paymentComponentModelTable.c_payment_wx_c_payment_uu, elementValueService.ElementValueList().CheckingUnallocatedReceipt.value);


        return new Promise(function (resolve, reject) {

            apiInterface.ExecuteBulkSqlStatement([sqlInsertPaymentSelection, sqlInsertCheckingInTransfer]).then(function (inserted) {
                insertSuccess += inserted;
                resolve(insertSuccess);
            });

        }).then(function () {

            return new Promise(function (resolve, reject) {
                let bankstatementService = new BankstatementService();
                bankstatementService.SetStatusAndRunProcess(paymentComponentModelTable.c_payment_wx_c_payment_uu,
                    paymentComponentModelTable.c_doctype_wx_c_doctype_id, 'REVERSED').then(function (inserted) {
                        insertSuccess += inserted;
                        resolve(insertSuccess);

                    });

            });

        });


    }



    /**
      * Create FactAcct berdasarkan APP Completed
      */
    ARReceiptCompleted(paymentCMT) {


        let thisObject = this;
        let elementValueService = new ElementValueService();
        let factacctService = new FactAcctService();
        let insertSuccess = 0;
        try {
            let sqlInsertDr = factacctService.SqlInsertFactAcctStatement(paymentCMT.c_bankaccount_wx_transit_element,
                'c_payment', paymentCMT.c_payment_wx_c_payment_uu, paymentCMT.c_payment_wx_payamt, null);

            let sqlInsertCr = factacctService.sqlInsertFactAcctStatement(elementValueService.ElementValueList().CheckingUnallocatedReceipt,
                'c_payment', paymentCMT.c_payment_wx_c_payment_uu, null, paymentCMT.c_payment_wx_payamt);

            return new Promise(function (resolve, reject) {
                apiInterface.ExecuteBulkSqlStatement([sqlInsertDr, sqlInsertCr]).then(function (rowChanged) {
                    insertSuccess += rowChanged;
                    resolve(insertSuccess);
                });
            }).then(function () {
                return new Promise(function (resolve, reject) {
                    let bankstatementService = new BankstatementService();
                    bankstatementService.CreateBankStatement(paymentCMT.c_payment_wx_c_bankaccount_uu,
                        paymentCMT.c_payment_wx_c_order_uu, paymentCMT.c_payment_wx_c_payment_uu,
                        paymentCMT.c_payment_wx_c_invoice_uu, paymentCMT.c_payment_wx_payamt,
                        paymentCMT.c_payment_wx_c_doctype_id, 'COMPLETED', paymentCMT.c_payment_wx_dateacct, false).then(function (inserted) {

                            insertSuccess += inserted;
                            resolve(insertSuccess);

                        });

                });

            });
        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.ARReceiptCompleted.name, error);
        }

    }

    /**
  * Create FactAcct berdasarkan APP Completed
  */
    APPaymentCompleted(paymentCMT) {
        let thisObject = this;
        let insertSuccess = 0;
        let elementValueService = new ElementValueService();
        let factacctService = new FactAcctService();

        try {
            let sqlInsertDr = factacctService.sqlInsertFactAcctStatement(elementValueService.ElementValueList().PaymentSelection,
                'c_payment', paymentCMT.c_payment_wx_c_payment_uu, paymentCMT.c_payment_wx_payamt, null);

            let sqlInsertCr = factacctService.SqlInsertFactAcctStatement(paymentCMT.c_bankaccount_wx_transit_element,
                'c_payment', paymentCMT.c_payment_wx_c_payment_uu, null, paymentCMT.c_payment_wx_payamt);

            return new Promise(function (resolve, reject) {
                apiInterface.ExecuteBulkSqlStatement([sqlInsertDr, sqlInsertCr]).then(function (rowChanged) {
                    insertSuccess += rowChanged;
                    resolve(insertSuccess);
                });
            }).then(function () {
                return new Promise(function (resolve, reject) {
                    let bankstatementService = new BankstatementService();
                    bankstatementService.CreateBankStatement(paymentCMT.c_payment_wx_c_bankaccount_uu,
                        paymentCMT.c_payment_wx_c_order_uu, paymentCMT.c_payment_wx_c_payment_uu,
                        paymentCMT.c_payment_wx_c_invoice_uu, paymentCMT.c_payment_wx_payamt,
                        paymentCMT.c_payment_wx_c_doctype_id, 'COMPLETED',paymentCMT.c_payment_wx_dateacct, true).then(function (inserted) {

                            insertSuccess += inserted;
                            resolve(insertSuccess);

                        });

                });

            });
        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.APPaymentCompleted.name, error);
        }



    }



    APExpenseCompleted(expenseCMT) {
        let thisObject = this;
        let insertSuccess = 0;
        let elementValueService = new ElementValueService();
        let factacctService = new FactAcctService();

        let mapExpenseElementValue = elementValueService.ExpenseMapElement();
        let elementValueObject = mapExpenseElementValue[expenseCMT.c_charge_wx_name];
        let uuid = expenseCMT.c_payment_wx_c_payment_uu;

        try {
            //DR
            let insertSqlDr = factacctService.sqlInsertFactAcctStatement(elementValueObject, 'c_payment', uuid, expenseCMT.c_payment_wx_payamt, null);

            //CR
            let insertSqlCrChecking = factacctService.SqlInsertFactAcctStatement(expenseCMT.c_bankaccount_wx_transit_element, 'c_payment', uuid, null, expenseCMT.c_payment_wx_payamt);


            return new Promise(function (resolve, reject) {
                apiInterface.ExecuteBulkSqlStatement([insertSqlDr, insertSqlCrChecking]).then(function (rowChanged) {
                    insertSuccess += rowChanged;
                    resolve(insertSuccess);
                });

            }).then(function (insertSuccess) {

                return new Promise(function (resolve, reject) {
                    let bankstatementService = new BankstatementService();
                    bankstatementService.CreateBankStatement(expenseCMT.c_payment_wx_c_bankaccount_uu,
                        null, expenseCMT.c_payment_wx_c_payment_uu,
                        null, expenseCMT.c_payment_wx_payamt,
                        expenseCMT.c_payment_wx_c_doctype_id, 'COMPLETED',expenseCMT.c_payment_wx_dateacct,true).then(function (inserted) {

                            insertSuccess += inserted;
                            resolve(insertSuccess);

                        });


                });

            });


        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.APExpenseCompleted.name, error);
        }

    }

    /**
     * c_doctype_id = 5002 , AP EXPENSE
     * @param {*} expenseCMT 
     */
    APExpenseReversed(expenseCMT) {
        let elementValueService = new ElementValueService();
        let factacctService = new FactAcctService();

        let mapExpenseElementValue = elementValueService.ExpenseMapElement();
        let elementValueObject = mapExpenseElementValue[expenseCMT.c_charge_wx_name];
        let uuid = expenseCMT.c_payment_wx_c_payment_uu;
        let insertSuccess = 0;

        // Expense Account
        let sqlExpense = factacctService.sqlInsertReverseFactAcctStatement('c_payment', uuid, elementValueObject.value);

        // Checking In Transfer
        let sqlCheckingInTransfer = factacctService.sqlInsertReverseFactAcctStatement('c_payment', uuid, expenseCMT.c_bankaccount_wx_transit_element);


        return new Promise(function (resolve, reject) {

            apiInterface.ExecuteBulkSqlStatement([sqlExpense, sqlCheckingInTransfer]).then(function (inserted) {
                insertSuccess += inserted;
                resolve(insertSuccess);
            });

        }).then(function () {

            return new Promise(function (resolve, reject) {
                let bankstatementService = new BankstatementService();
                bankstatementService.SetStatusAndRunProcess(expenseCMT.c_payment_wx_c_payment_uu,
                    expenseCMT.c_doctype_wx_c_doctype_id, 'REVERSED').then(function (inserted) {
                        insertSuccess += inserted;
                        resolve(insertSuccess);

                    });

            });

        });

    }





}