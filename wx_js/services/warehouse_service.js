class WarehouseService {
    constructor() { }

    /**
 * Find All warehouse dengan bentuk warehouseviewmodel
 * @param {*} metaDataResponseTable 
 */
    FindAll(metaDataResponseTable) {


        let searchFilter = '';
        if (metaDataResponseTable.search !== '') {
            searchFilter += ' AND ' + metaDataResponseTable.searchField + '  LIKE    \'%' + metaDataResponseTable.search + '%\'';
        }

        let limit  = '';
        if ((metaDataResponseTable.page !== null) && (metaDataResponseTable.perpage !== null)){

            limit = ' LIMIT ' + metaDataResponseTable.page + ',' + metaDataResponseTable.perpage;

        }

        const sqlTemplate = '  FROM m_warehouse ' +
            ' INNER JOIN c_location ON c_location.c_location_uu = m_warehouse.c_location_uu ' +
            ' WHERE m_warehouse.wx_isdeleted = \'N\' AND m_warehouse.ad_org_uu = \'' + metaDataResponseTable.ad_org_uu +
            '\'' + searchFilter + ' ORDER BY m_warehouse.updated DESC ' + limit;

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new WarehouseViewModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }

    FindWarehouseViewModelByMWarehouseUu(m_warehouse_uu) {

        const sqlTemplate = '  FROM m_warehouse ' +
            ' INNER JOIN c_location ON c_location.c_location_uu = m_warehouse.c_location_uu ' +
            ' WHERE m_warehouse.m_warehouse_uu = \'' + m_warehouse_uu + '\'';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new WarehouseViewModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }



    /**
     * 
     * @param {*} warehouseCM 
     * @param {*} datatableReference dataTableReferemce CInvoice datatable
     */
    async SaveUpdate(warehouseCM, datatableReference) {
        const timeNow = new Date().getTime();

        let thisObject = this;
        let isNewRecord = false;
        let mLocator = null;
        if (!warehouseCM.m_warehouse.m_warehouse_uu) { //new record
            isNewRecord = true;
            warehouseCM.m_warehouse.m_warehouse_uu = HelperService.UUID();
            warehouseCM.m_warehouse.ad_client_uu = localStorage.getItem('ad_client_uu');
            warehouseCM.m_warehouse.ad_org_uu = localStorage.getItem('ad_org_uu');

            warehouseCM.m_warehouse.createdby = localStorage.getItem('ad_user_uu');
            warehouseCM.m_warehouse.updatedby = localStorage.getItem('ad_user_uu');

            warehouseCM.m_warehouse.created = timeNow;
            warehouseCM.m_warehouse.updated = timeNow;

            mLocator = new MLocator();
            mLocator.m_locator_uu = HelperService.UUID();
            mLocator.value = warehouseCM.m_warehouse.name;
            mLocator.ad_client_uu = localStorage.getItem('ad_client_uu');
            mLocator.ad_org_uu = localStorage.getItem('ad_org_uu');
            mLocator.m_warehouse_uu = warehouseCM.m_warehouse.m_warehouse_uu;
            mLocator.isdefault = 'Y';
            mLocator.sync_client = null;
            mLocator.process_date = null;
            mLocator.created = timeNow;
            mLocator.updated = timeNow;
            mLocator.createdby = localStorage.getItem('ad_user_uu');
            mLocator.updatedby = localStorage.getItem('ad_user_uu');
        } else { //update record


            warehouseCM.m_warehouse.updatedby = localStorage.getItem('ad_user_uu');

            warehouseCM.m_warehouse.updated = timeNow;
        }



        if (!warehouseCM.c_location.c_location_uu) {
            warehouseCM.c_location.c_location_uu = HelperService.UUID();
            warehouseCM.c_location.ad_client_uu = localStorage.getItem('ad_client_uu');
            warehouseCM.c_location.ad_org_uu = localStorage.getItem('ad_org_uu');

            warehouseCM.c_location.createdby = localStorage.getItem('ad_user_uu');
            warehouseCM.c_location.updatedby = localStorage.getItem('ad_user_uu');

            warehouseCM.c_location.created = timeNow;
            warehouseCM.c_location.updated = timeNow;
        } else {


            warehouseCM.c_location.updatedby = localStorage.getItem('ad_user_uu');

            warehouseCM.c_location.updated = timeNow;
        }

        warehouseCM.c_location.sync_client = null;
        warehouseCM.c_location.process_date = null;

        warehouseCM.m_warehouse.sync_client = null;
        warehouseCM.m_warehouse.process_date = null;


        warehouseCM.m_warehouse.c_location_uu = warehouseCM.c_location.c_location_uu;

        let insertSuccess = 0;
        const sqlInsert = HelperService.SqlInsertOrReplaceStatement('m_warehouse', warehouseCM.m_warehouse);
        const sqlInsertCLocation = HelperService.SqlInsertOrReplaceStatement('c_location', warehouseCM.c_location);

        insertSuccess += await apiInterface.ExecuteSqlStatement(sqlInsert);
        insertSuccess += await apiInterface.ExecuteSqlStatement(sqlInsertCLocation);

        if (mLocator) {
            const sqlInsertMLocator = HelperService.SqlInsertOrReplaceStatement('m_locator', mLocator);

            insertSuccess += await apiInterface.ExecuteSqlStatement(sqlInsertMLocator);
        }

        if (insertSuccess > 0) {

            if (datatableReference) {
                let rowDataList = await thisObject.FindWarehouseViewModelByMWarehouseUu(warehouseCM.m_warehouse.m_warehouse_uu);

                let rowData = {};

                if (rowDataList.length > 0) {
                    rowData = rowDataList[0];
                }

                if (!isNewRecord) {

                    datatableReference.row($('#bttn_row_edit' + rowData['m_warehouse_wx_m_warehouse_uu']).parents('tr')).data(rowData).draw();
                } else {

                    datatableReference.row.add(rowData).draw();
                }



            }



        }
        return insertSuccess;
        /*return new Promise(function (resolve, reject) {

            Promise.all([apiInterface.ExecuteSqlStatement(sqlInsert),
            apiInterface.ExecuteSqlStatement(sqlInsertCLocation)])
                .then(function (resultArray) {
                    insertSuccess = resultArray.length;

                    if (mLocator) {
                        const sqlInsertMLocator = HelperService.SqlInsertOrReplaceStatement('m_locator', mLocator);

                        apiInterface.ExecuteSqlStatement(sqlInsertMLocator).then(function (rowChanged) {
                            insertSuccess += rowChanged;
                        })
                    }

                    if (insertSuccess > 0) {

                        if (datatableReference) {
                            thisObject.FindWarehouseViewModelByMWarehouseUu(warehouseCM.m_warehouse.m_warehouse_uu)
                                .then(function (rowDataList) {
                                    let rowData = {};

                                    if (rowDataList.length > 0) {
                                        rowData = rowDataList[0];
                                    }

                                    if (!isNewRecord) {

                                        datatableReference.row($('#bttn_row_edit' + rowData['m_warehouse_wx_m_warehouse_uu']).parents('tr')).data(rowData).draw();
                                    } else {

                                        datatableReference.row.add(rowData).draw();
                                    }
                                    resolve(insertSuccess);
                                });


                        }



                    }else{
                        resolve(insertSuccess);
                    }






                });

        });*/




    }

}