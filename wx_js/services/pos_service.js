class PosService {


    FindSearchProductCMTList(m_pricelist_version_uu, metadatatable) {
        let filters = '';
        let limit = '';
        if (metadatatable.page && metadatatable.perpage) {
            limit = ` LIMIT ` + metadatatable.page + ',' + metadatatable.perpage;
        }


        if (metadatatable.search) {
            filters = ` m_product_wx_name LIKE  \'%` + metadatatable.search + `%\'  OR m_product_wx_sku LIKE  \'%` + metadatatable.search + '%\' OR m_product_wx_upc LIKE  \'%' + metadatatable.search + '%\'';
        }


        const sqlTemplate = '  FROM m_productprice ' +
            ' INNER JOIN m_product ON m_product.m_product_uu =  m_productprice.m_product_uu ' +
            ' WHERE m_productprice.wx_isdeleted = \'N\' AND m_productprice.m_pricelist_version_uu = \'' + m_pricelist_version_uu + '\'  ORDER BY m_product.name ASC ';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new SearchPosProductComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);
        let sqlStatement = apiInterface.GetSqlStatement(metaDataQuery);

        let sqlStatementRun = '';

        if (filters) {
            sqlStatementRun = 'SELECT * FROM (' + sqlStatement + ') WHERE ' + filters + ' ORDER BY m_product_wx_name ASC ' + limit;

        } else {

            sqlStatementRun = sqlStatement + limit;
        }

        return apiInterface.DirectQuerySqlStatement(sqlStatementRun);
    }


    GetDefaultCPosCMT() {

        const sqlTemplate = '  FROM c_pos ' +
            ' INNER JOIN m_warehouse ON m_warehouse.m_warehouse_uu =  c_pos.m_warehouse_uu ' +
            ' INNER JOIN c_bankaccount ON c_bankaccount.c_bankaccount_uu =  c_pos.c_bankaccount_uu ' +
            ' INNER JOIN c_tax ON c_tax.c_tax_uu =  c_pos.c_tax_uu ' +
            ' INNER JOIN m_shipper ON m_shipper.m_shipper_uu =  c_pos.m_shipper_uu ' +
            ' INNER JOIN m_pricelist_version ON m_pricelist_version.m_pricelist_version_uu =  c_pos.m_pricelist_version_uu ';


        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new PosConfigComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }

    FindCartCMTList() {



        const sqlTemplate = '  FROM pos_c_order ' +
            ' INNER JOIN c_bpartner ON pos_c_order.c_bpartner_uu = c_bpartner.c_bpartner_uu ' +
            ' INNER JOIN c_bpartner_location ON pos_c_order.c_bpartner_uu = c_bpartner.c_bpartner_uu ' +
            ' WHERE pos_c_order.wx_isdeleted = \'N\'  ORDER BY pos_c_order.updated ASC ';


        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new CartComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);


    }

    FindCartCMTByCOrderUu(c_order_uu) {



        const sqlTemplate = '  FROM pos_c_order ' +
            ' INNER JOIN c_bpartner ON pos_c_order.c_bpartner_uu = c_bpartner.c_bpartner_uu ' +
            ' INNER JOIN c_bpartner_location ON pos_c_order.c_bpartner_uu = c_bpartner.c_bpartner_uu ' +
            ' WHERE pos_c_order.c_order_uu = \'' + c_order_uu + '\'';


        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new CartComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);


    }

    FindCartlineCMTListByCOrderUu(c_order_uu) {

        const sqlTemplate = '  FROM pos_c_orderline ' +
            ' INNER JOIN pos_c_order ON pos_c_orderline.c_order_uu = pos_c_order.c_order_uu ' +
            ' INNER JOIN m_product ON m_product.m_product_uu  = pos_c_orderline.m_product_uu ' +
            ' INNER JOIN c_uom ON pos_c_orderline.c_uom_uu = c_uom.c_uom_uu ' +
            ' WHERE pos_c_orderline.wx_isdeleted = \'N\'  AND pos_c_orderline.c_order_uu = \'' + c_order_uu + '\' ORDER BY pos_c_orderline.line ASC ';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new CartlineComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }

    FindCartlineCMTByCOrderlineUu(c_orderline_uu) {

        const sqlTemplate = '  FROM pos_c_orderline ' +
            ' INNER JOIN pos_c_order ON pos_c_orderline.c_order_uu = pos_c_order.c_order_uu ' +
            ' INNER JOIN m_product ON m_product.m_product_uu  = pos_c_orderline.m_product_uu ' +
            ' INNER JOIN c_uom ON pos_c_orderline.c_uom_uu = c_uom.c_uom_uu ' +
            ' WHERE  pos_c_orderline.c_orderline_uu = \'' + c_orderline_uu + '\' ';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new CartlineComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }

    CountTotallinesAndGrandTotal(cOrderUu) {
        let setTotallinesStatement = 'UPDATE  pos_c_order SET sync_client = null, process_date = null, totallines =' +
            '(select SUM(linenetamt) FROM pos_c_orderline where pos_c_orderline.c_order_uu =\'' +
            cOrderUu + '\') WHERE pos_c_order.c_order_uu = \'' + cOrderUu + '\';';

        let setGrandTotalStatement = 'UPDATE pos_c_order SET sync_client = null, process_date = null, grandtotal =' +
            '(select SUM(linenetamt + linetaxamt) FROM pos_c_orderline where pos_c_orderline.wx_isdeleted = \'N\' AND pos_c_orderline.c_order_uu =\'' +
            cOrderUu + '\') WHERE pos_c_order.c_order_uu = \'' + cOrderUu + '\';';


        let setTotalTaxLines = 'UPDATE pos_c_order SET sync_client = null, process_date = null, totaltaxlines =' +
            '(select SUM(linetaxamt) FROM pos_c_orderline where pos_c_orderline.wx_isdeleted = \'N\' AND pos_c_orderline.c_order_uu =\'' +
            cOrderUu + '\') WHERE pos_c_order.c_order_uu = \'' + cOrderUu + '\';';


        let insertSuccess = 0;

        return new Promise(function (resolve, reject) {
            Promise.all([apiInterface.ExecuteSqlStatement(setTotallinesStatement),
            apiInterface.ExecuteSqlStatement(setGrandTotalStatement),
            apiInterface.ExecuteSqlStatement(setTotalTaxLines)]).then(function (resultArray) {
                insertSuccess += resultArray[0];
                insertSuccess += resultArray[1];
                insertSuccess += resultArray[2];

                resolve(insertSuccess);

            });



        });


    }

    /**
     * 1. Move pos_c_order to c_order
     * 2. Move pos_c_orderline to c_orderline
     * 3. Run Process Direct Sales Order
     * 4. Print to thermal
     * @param {*} pos_c_order_uu 
     */
    RunPosPayment(pos_c_order_uu, cartlinePortlet) {
        try {
            //1. move pos_c_order to c_order
            let timeNow = new Date().getTime();
            let insertSuccess = 0;
            let orderService = new OrderService();

            let promiseArray = [];
            let movePosCOrder = 'INSERT OR REPLACE INTO c_order(ad_client_uu, ad_org_uu,' +
                ' c_bpartner_uu, c_bpartner_location_uu,c_doctype_id, c_order_uu,documentno, c_pos_uu,grandtotal,' +
                ' c_bankaccount_uu, totallines, totaltaxlines, pos_payment_amount, pos_charge_amount, m_warehouse_uu, ' +
                ' m_shipper_uu, docstatus,  updated, created, createdby, updatedby, sync_client, process_date, dateacct )' +
                ' SELECT ad_client_uu, ad_org_uu,' +
                ' c_bpartner_uu, c_bpartner_location_uu,c_doctype_id, c_order_uu,documentno, c_pos_uu,grandtotal,' +
                ' c_bankaccount_uu, totallines, totaltaxlines, pos_payment_amount, pos_charge_amount, m_warehouse_uu, ' +
                ' m_shipper_uu,\'COMPLETED\', ' + timeNow + ',' + timeNow + ',\'' + localStorage.getItem('ad_user_uu') + '\',\'' +
                localStorage.getItem('ad_user_uu') + '\', NULL, NULL, dateacct ' +
                ' FROM pos_c_order WHERE c_order_uu = \'' + pos_c_order_uu + '\';';

            promiseArray.push(apiInterface.ExecuteSqlStatement(movePosCOrder));

            let movePosCOrderline = 'INSERT OR REPLACE INTO c_orderline(ad_client_uu, ad_org_uu,' +
                ' c_order_uu, c_orderline_uu, c_tax_uu, c_uom_uu, line, linenetamt, m_product_uu, ' +
                ' priceentered, priceactual,qtyordered, qtyentered, linetaxamt, created,updated, createdby, updatedby, sync_client, process_date) ' +
                ' SELECT ad_client_uu, ad_org_uu,' +
                ' c_order_uu, c_orderline_uu, c_tax_uu, c_uom_uu, line, linenetamt, m_product_uu, ' +
                ' priceentered, priceactual,qtyordered, qtyentered, linetaxamt,' + timeNow + ',' + timeNow + ',\'' +
                localStorage.getItem('ad_user_uu') + '\',\'' + localStorage.getItem('ad_user_uu') + '\', NULL, NULL FROM pos_c_orderline WHERE c_order_uu = \'' + pos_c_order_uu + '\';';
            promiseArray.push(apiInterface.ExecuteSqlStatement(movePosCOrderline));

            return new Promise(function (resolve, reject) {

                Promise.all(promiseArray).then(function (inserted) {
                    insertSuccess += inserted;
                    resolve(insertSuccess)
                });
            }).then(function (insertSuccess) {
                return new Promise(function (resolve, reject) {
             orderService.FindOrderViewModelByCOrderUu(pos_c_order_uu).then(function(orderCMTList){

                        let orderCMT = orderCMTList[0];
                        resolve(orderCMT);

                    });
                   
                });


            }).then(function (orderCMT) {

                return new Promise(function (resolve, reject) {

                    orderService.RunProcessDocument(orderCMT).then(function (dataflow) {

                        resolve(insertSuccess);

                    });

                });



            }).then(function () {


                return new Promise(function (resolve, reject) {

                    //PRINT TO POS PRINTER
                    cartlinePortlet.Print();
                    resolve(1);

                });

            }).then(function () {
                return new Promise(function (resolve, reject) {

                    //EraseAllCart
                    cartlinePortlet.EraseAllCart().then(function () {

                        resolve(1);
                    });

                });

            });

            // print to POS printer


        } catch (err) {
            apiInterface.Log(this.constructor.name, this.RunPosPayment.name, err);
        }


    }






}