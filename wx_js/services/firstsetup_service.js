class FirstSetupService {

    constructor() { }

    async SaveUpdate(organizationCM, userCM) {

        let thisObject = this;
        try {



            let shipperService = new ShipperService();
            let organizationService = new OrganizationService();
            let userService = new UserService();

            // cek jika ini adalah new record, karena ad_user_uu , ad_org_uu, ad_client_uu harus dibuat bersama-sama

            let isNewRecord = false;
            let insertSuccess = 0;


            if (!organizationCM.ad_org.ad_org_uu) {
                isNewRecord = true;
                //set config karena config dipergunakan disemua nya
                localStorage.setItem('ad_user_uu', HelperService.UUID());
                localStorage.setItem('ad_org_uu', HelperService.UUID());
                localStorage.setItem('ad_client_uu', HelperService.UUID());

                let roleCM = new RoleComponentModel();
                roleCM.ad_role.ad_org_uu = localStorage.getItem('ad_org_uu');
                roleCM.ad_role.ad_client_uu = localStorage.getItem('ad_client_uu');
                roleCM.ad_role.created = new Date().getTime();
                roleCM.ad_role.createdby = localStorage.getItem('ad_user_uu');
                roleCM.ad_role.name = 'Administrator';
                roleCM.ad_role.ad_role_uu = HelperService.UUID();

                let roleService = new RoleService();

                insertSuccess = await roleService.SaveUpdate(roleCM);



                userCM.ad_user.ad_org_uu = localStorage.getItem('ad_org_uu');
                userCM.ad_user.ad_client_uu = localStorage.getItem('ad_client_uu');
                userCM.ad_user.created = new Date().getTime();
                userCM.ad_user.ad_user_uu = localStorage.getItem('ad_user_uu');
                userCM.ad_user.createdby = localStorage.getItem('ad_user_uu');

                userCM.ad_role = roleCM.ad_role;


                // CREATE WAREHOUSE DEFAULT

                let warehouseDefault = new MWarehouse();
                warehouseDefault.name = 'Kantor Pusat';

                let warehouseCM = new WarehouseViewModel();
                warehouseCM.m_warehouse = warehouseDefault;
                warehouseCM.c_location = organizationCM.c_location;

                let warehouseService = new WarehouseService();


                insertSuccess += await warehouseService.SaveUpdate(warehouseCM);

                let uom = new CUom();
                uom.name = 'Each';
                uom.uomsymbol = 'Ea';
                let uomCM = new UomComponentModel();
                uomCM.c_uom = uom;
                let uomService = new UomService();

                insertSuccess += await uomService.SaveUpdate(uomCM);

                let bankaccountCM = new BankaccountViewModel();
                bankaccountCM.c_bankaccount.name = 'Default Bank';
                bankaccountCM.c_bankaccount.accountno = "xxxxxxxx";
                bankaccountCM.c_bankaccount.bankaccounttype = 'C';// for checking account, bank account
                bankaccountCM.c_bankaccount.c_bank_id = 1000001; // untuk bank 

                let bankaccountService = new BankaccountService();


                insertSuccess += await bankaccountService.SaveUpdate(bankaccountCM);



                let pettyCashDefault = new BankaccountViewModel();
                pettyCashDefault.c_bankaccount.name = 'Petty Cash Default';
                pettyCashDefault.c_bankaccount.accountno = "xxxxxxxx";
                pettyCashDefault.c_bankaccount.bankaccounttype = 'B';// for checking account, bank account
                pettyCashDefault.c_bankaccount.c_bank_id = 1000000; // untuk bank 


                insertSuccess += await bankaccountService.SaveUpdate(pettyCashDefault);


                let taxService = new TaxService();

                let tax10 = new CTax();
                tax10.name = "PPN 10%";
                tax10.rate = 10;

                let tax10CM = new TaxComponentModel();
                tax10CM.c_tax = tax10;

                insertSuccess += await taxService.SaveUpdate(tax10CM);



                let tax0CM = new TaxComponentModel();

                let tax0 = new CTax();
                tax0.name = "PPN 0%";
                tax0.rate = 0;

                tax0CM.c_tax = tax0;


                insertSuccess += await taxService.SaveUpdate(tax0CM);


                //create default pricelistversion

                let pricelistversionService = new PricelistVersionService();
                let defaultPricelistHargaJual = new PricelistVersionViewModel();
                defaultPricelistHargaJual.m_pricelist_version.name = 'Default Versi Harga Jual';
                defaultPricelistHargaJual.m_pricelist.m_pricelist_uu = 'c90fbb80-589b-4943-b1eb-e22beb25178a';


                insertSuccess += await pricelistversionService.SaveUpdate(defaultPricelistHargaJual);





                let defaultPricelistHargaBeli = new PricelistVersionViewModel();
                defaultPricelistHargaBeli.m_pricelist_version.name = 'Default Versi Harga Beli';
                defaultPricelistHargaBeli.m_pricelist.m_pricelist_uu = 'fd61186c-6ade-4e33-9db8-fb81b82cef5b';


                insertSuccess += await pricelistversionService.SaveUpdate(defaultPricelistHargaBeli);


                let shipperService = new ShipperService();

                insertSuccess += await shipperService.CreateShipperDefault();

                apiInterface.Debug("create shipper default");



            }



            insertSuccess += await organizationService.SaveUpdate(organizationCM);




            insertSuccess += await userService.SaveUpdate(userCM);


            /* return new Promise(function (resolve, reject) {
 
                 organizationService.SaveUpdate(organizationCM).then(function (rowChanged1) {
                     insertSuccess += rowChanged1;
 
                     userService.SaveUpdate(userCM).then(function (rowChanged2) {
                         insertSuccess += rowChanged2;
 
                         resolve(insertSuccess);
                     });
 
                 });
 
 
             });*/

            return insertSuccess;

        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.SaveUpdate.name, error);
            return 0;

        }

    }


}