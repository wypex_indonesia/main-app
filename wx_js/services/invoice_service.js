/**
 * API  (AP Invoice) 5000
 * ARI (AR Invoice)  6000
 */
class InvoiceService {
    constructor() {

    }

    /**
	 * 
	 * @param {*} metaDataResponseTable 
	 */
    FindAll(metaDataResponseTable) {


        let otherFilters = '';
        if (metaDataResponseTable.otherFilters.length > 0) {

            _.forEach(metaDataResponseTable.otherFilters, function (value) {

                otherFilters += ' ' + value + ' ';
            });

        }

        const sqlTemplate = '  FROM c_invoice ' +
            ' INNER JOIN c_bpartner ON c_invoice.c_bpartner_uu = c_bpartner.c_bpartner_uu ' +
            ' INNER JOIN c_bpartner_location ON c_bpartner.c_bpartner_uu  = c_bpartner_location.c_bpartner_uu ' +
            ' INNER JOIN c_order ON c_order.c_order_uu = c_invoice.c_order_uu ' +
            ' INNER JOIN c_doctype ON c_invoice.c_doctype_id = c_doctype.c_doctype_id ' +
            ' WHERE c_invoice.wx_isdeleted = \'N\' ' + otherFilters + ' ORDER BY c_invoice.documentno DESC ';


        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new InvoiceComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);


        return apiInterface.Query(metaDataQuery);
    }

    FindInvoiceComponentModelByCInvoiceUu(c_invoice_uu) {

        const sqlTemplate = '  FROM c_invoice ' +
            ' INNER JOIN c_bpartner ON c_invoice.c_bpartner_uu = c_bpartner.c_bpartner_uu ' +
            ' INNER JOIN c_bpartner_location ON c_bpartner.c_bpartner_uu  = c_bpartner_location.c_bpartner_uu ' +
            ' INNER JOIN c_order ON c_order.c_order_uu = c_invoice.c_order_uu ' +
            ' INNER JOIN c_doctype ON c_invoice.c_doctype_id = c_doctype.c_doctype_id ' +
            ' WHERE c_invoice.c_invoice_uu = \'' + c_invoice_uu + '\' ';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new InvoiceComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);


        return apiInterface.Query(metaDataQuery);
    }

    FindInvoiceComponentModelByCOrderUu(c_order_uu) {

        const sqlTemplate = '  FROM c_invoice ' +
            ' INNER JOIN c_bpartner ON c_invoice.c_bpartner_uu = c_bpartner.c_bpartner_uu ' +
            ' INNER JOIN c_bpartner_location ON c_bpartner.c_bpartner_uu  = c_bpartner_location.c_bpartner_uu ' +
            ' INNER JOIN c_order ON c_order.c_order_uu = c_invoice.c_order_uu ' +
            ' INNER JOIN c_doctype ON c_invoice.c_doctype_id = c_doctype.c_doctype_id ' +
            ' WHERE c_invoice.c_order_uu = \'' + c_order_uu + '\' ';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new InvoiceComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);


        return apiInterface.Query(metaDataQuery);
    }

    UpdateStatus(invoiceCMT) {
        let insertSuccess = 0;
        let thisObject = this;
        let sqlstatement = '';

        try {
            if (invoiceCMT.c_invoice_wx_docstatus === 'DELETED') {
                sqlstatement = HelperService.SqlDeleteStatement('c_invoice', 'c_invoice_uu', invoiceCMT.c_invoice_wx_c_invoice_uu);

                return new Promise(function (resolve, reject) {

                    apiInterface.ExecuteSqlStatement(sqlstatement).then(function (rowChanged) {
                        insertSuccess += rowChanged;
                        resolve(insertSuccess);
                    });

                });


            } else {

                return new Promise(function (resolve, reject) {
                    if ((invoiceCMT.c_invoice_wx_docstatus === 'COMPLETED') || (invoiceCMT.c_invoice_wx_docstatus === 'DRAFT')) {

                        thisObject.CountAllTotal(invoiceCMT.c_invoice_wx_c_invoice_uu).then(function (rowChanged) {
                            insertSuccess += rowChanged;
                            resolve(insertSuccess);

                        });

                    } else {

                        resolve(insertSuccess);
                    }
                }).then(function () {

                    return new Promise(function (resolve, reject) {
                        let sqlstatementUpdateStatus = HelperService.SqlUpdateStatusStatement('c_invoice', 'c_invoice_uu',
                            invoiceCMT.c_invoice_wx_c_invoice_uu, invoiceCMT.c_invoice_wx_docstatus);

                        apiInterface.ExecuteSqlStatement(sqlstatementUpdateStatus).then(function (rowChanged) {

                            insertSuccess += rowChanged;
                            resolve(insertSuccess);
                        });

                    });
                }).then(function () {

                    return new Promise(function (resolve, reject) {
                        thisObject.RunProcessDocument(invoiceCMT).then(function (rowChanged) {

                            insertSuccess += rowChanged;
                            resolve(insertSuccess);
                        });


                    });


                });

            }
        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.UpdateStatus.name, error);
        }



    }

    /**
     * 
     * @param {*} invoiceComponentModel 
     * @param {*} datatableReference dataTableReferemce CInvoice datatable
     */
    SaveUpdate(invoiceComponentModel, datatableReference) {

        let insertSuccess = 0;
        let isNewRecord = false;
        let thisObject = this;
        try {
            if (!invoiceComponentModel.c_invoice.c_invoice_uu) { //new record
                isNewRecord = true;

            }

            let sqlInsert = thisObject.GetInsertSqlStatement(invoiceComponentModel);

            return new Promise(function (resolve, reject) {

                apiInterface.ExecuteSqlStatement(sqlInsert).then(function (rowChanged) {
                    insertSuccess += rowChanged;
                    if (insertSuccess > 0) {

                        if (datatableReference) {
                            thisObject.FindInvoiceComponentModelByCInvoiceUu(invoiceComponentModel.c_invoice.c_invoice_uu)
                                .then(function (rowDataList) {

                                    let rowData = {};

                                    if (rowDataList.length > 0) {
                                        rowData = rowDataList[0];
                                    }

                                    if (!isNewRecord) {

                                        datatableReference.row($('#bttn_row_edit' + rowData['c_invoice_wx_c_invoice_uu']).parents('tr')).data(rowData).draw();
                                    } else {

                                        datatableReference.row.add(rowData).draw();
                                    }
                                    resolve(insertSuccess);

                                });
                        }
                        resolve(insertSuccess);



                    } else {
                        resolve(insertSuccess);
                    }


                });


            });
        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.SaveUpdate.name, error);
        }


    }


    GetInsertSqlStatement(invoiceComponentModel) {

        const timeNow = new Date().getTime();

        let isNewRecord = false;

        let sqlInsert = '';
        let thisObject = this;
        try {
            if (!invoiceComponentModel.c_invoice.c_invoice_uu) { //new record
                isNewRecord = true;
                invoiceComponentModel.c_invoice.c_invoice_uu = HelperService.UUID();
                invoiceComponentModel.c_invoice.ad_client_uu = localStorage.getItem('ad_client_uu');
                invoiceComponentModel.c_invoice.ad_org_uu = localStorage.getItem('ad_org_uu');

                invoiceComponentModel.c_invoice.createdby = localStorage.getItem('ad_user_uu');
                invoiceComponentModel.c_invoice.updatedby = localStorage.getItem('ad_user_uu');

                invoiceComponentModel.c_invoice.created = timeNow;
                invoiceComponentModel.c_invoice.updated = timeNow;
            } else { //update record


                invoiceComponentModel.c_invoice.updatedby = localStorage.getItem('ad_user_uu');

                invoiceComponentModel.c_invoice.updated = timeNow;
            }

            // AP Invoice
            if (invoiceComponentModel.c_doctype_wx_docbasetype === 'API') {

                invoiceComponentModel.c_invoice.issotrx = 'Y';
            }

            // AR Invoice
            if (invoiceComponentModel.c_doctype_wx_docbasetype === 'ARI') {

                invoiceComponentModel.c_invoice.issotrx = 'N';
            }


            invoiceComponentModel.c_invoice.c_order_uu = invoiceComponentModel.c_order.c_order_uu;
            invoiceComponentModel.c_invoice.c_bpartner_uu = invoiceComponentModel.c_bpartner.c_bpartner_uu;
            invoiceComponentModel.c_invoice.c_bpartner_location_uu = invoiceComponentModel.c_bpartner_location.c_bpartner_location_uu;
            invoiceComponentModel.c_invoice.c_doctype_id = invoiceComponentModel.c_doctype.c_doctype_id;
            invoiceComponentModel.c_invoice.sync_client = null;
            invoiceComponentModel.c_invoice.process_date = null;


            sqlInsert = HelperService.SqlInsertOrReplaceStatement('c_invoice', invoiceComponentModel.c_invoice);


        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.GetInsertSqlStatement.name, error);
        }

        return sqlInsert;
    }

    /**
     * COUNT totallines, totaltaxlines, grandtotal
     * @param {*} c_invoice_uu 
     */
    CountAllTotal(c_invoice_uu) {
        let setTotallinesStatement = 'UPDATE  c_invoice SET sync_client = null, process_date = null, totallines =' +
            '(select SUM(linenetamt) FROM c_invoiceline where c_invoiceline.wx_isdeleted = \'N\' AND c_invoiceline.c_invoice_uu =\'' +
            c_invoice_uu + '\') WHERE c_invoice.c_invoice_uu = \'' + c_invoice_uu + '\';';

        let setGrandTotalStatement = 'UPDATE c_invoice SET sync_client = null, process_date = null, grandtotal =' +
            '(select SUM(linenetamt + taxamt) FROM c_invoiceline where c_invoiceline.wx_isdeleted = \'N\' AND c_invoiceline.c_invoice_uu =\'' +
            c_invoice_uu + '\') WHERE c_invoice.c_invoice_uu= \'' + c_invoice_uu + '\';';


        let setTotalTaxLines = 'UPDATE c_invoice SET sync_client = null, process_date = null, totaltaxlines =' +
            '(select SUM(taxamt) FROM c_invoiceline where c_invoiceline.wx_isdeleted = \'N\' AND c_invoiceline.c_invoice_uu =\'' +
            c_invoice_uu + '\') WHERE c_invoice.c_invoice_uu = \'' + c_invoice_uu + '\';';


        let insertSuccess = 0;

        return new Promise(function (resolve, reject) {

            apiInterface.ExecuteBulkSqlStatement([setTotallinesStatement, setGrandTotalStatement, setTotalTaxLines]).then(function (inserted) {
                insertSuccess += inserted;
                resolve(insertSuccess);


            });




        });


    }



    /**
     * Run Process Document sesuai dengan docstatus :
     * 1. Draft -> Tidak ada Action
     * docbasetype :
     * AR Invoice
     * 
     * AP Invoice
     * 2. COMPLETED
     *  2.2. FactAcct 
     * 3. REVERSED
     *  2.1. FactAcct -> Reversed
     * 
     * @param {*} invoiceComponentModelTable 
     */
    RunProcessDocument(invoiceComponentModelTable) {

        let thisObject = this;
        let factacctService = new FactAcctService();

        if (invoiceComponentModelTable.c_invoice_wx_docstatus === 'COMPLETED') {

            // SET MCost pada tiap-tiap product dengan m_costelement Last PO
            // dengan mendapatkan priceactual dari c_orderline, karena price menggunakan basedUom
            // product asset value =  priceactual * qtyOrdered 
            // dan create accounting transaction 
            if (invoiceComponentModelTable.c_doctype_wx_docbasetype === 'API') {
                return thisObject.APInvoiceCompleted(invoiceComponentModelTable);
            } else if (invoiceComponentModelTable.c_doctype_wx_docbasetype === 'ARI') {
                return thisObject.ARInvoiceCompleted(invoiceComponentModelTable);

            } else {
                return new Promise(function (resolve, reject) {
                    resolve(1);

                });

            }



        } else if (invoiceComponentModelTable.c_invoice_wx_docstatus === 'REVERSED') {

            if (invoiceComponentModelTable.c_doctype_wx_docbasetype === 'API') {
                return thisObject.APInvoiceReversed(invoiceComponentModelTable);
            } else if (invoiceComponentModelTable.c_doctype_wx_docbasetype === 'ARI') {

                return thisObject.ARInvoiceReversed(invoiceComponentModelTable);
            } else {
                return new Promise(function (resolve, reject) {
                    resolve(1);

                });

            }
        } else {

            // untuk DRAFT DAN CLOSED DOCUMENT
            return new Promise(function (resolve, reject) {
                resolve(1);

            });

        }


    }

    APInvoiceReversed(invoiceCMT) {
        let thisObject = this;
        let factacctService = new FactAcctService();
        let elementValueService = new ElementValueService();
        let invoicelineService = new InvoicelineService();
        let sqlInsertArray = [];
        let reverseAccountPayableTradeStatement = factacctService.sqlInsertReverseFactAcctStatement('c_invoice', invoiceCMT.c_invoice_wx_c_invoice_uu, elementValueService.ElementValueList().AccountPayableTrade.value);

        sqlInsertArray.push(reverseAccountPayableTradeStatement);
        let insertSuccess = 0;



        return new Promise(function (resolve, reject) {
            invoicelineService.FindInvoicelineComponentModelByCInvoiceUu(invoiceCMT.c_invoice_wx_c_invoice_uu)
                .then(function (invoicelineCMTList) {

                    _.forEach(invoicelineCMTList, function (invoicelineCMT) {

                        // NOT INVOICED RECEIPT
                        let sqlInsertReversedNotInvoiceReceipt = factacctService.sqlInsertReverseFactAcctStatement('c_invoiceline', invoicelineCMT.c_invoiceline_wx_c_invoiceline_uu, elementValueService.ElementValueList().NotInvoicedReceipt.value);
                        sqlInsertArray.push(sqlInsertReversedNotInvoiceReceipt);

                        if (invoicelineCMT.c_invoiceline_wx_taxamt) {
                            // TAX CREDIT A/R
                            let sqlInsertReversedTaxCredit = factacctService.sqlInsertReverseFactAcctStatement('c_invoiceline', invoicelineCMT.c_invoiceline_wx_c_invoiceline_uu, elementValueService.ElementValueList().TaxCreditAR.value);
                            sqlInsertArray.push(sqlInsertReversedTaxCredit);
                        }

                    });

                    resolve(sqlInsertArray);
                });



        }).then(function (sqlInsertArray) {

            return new Promise(function (resolve, reject) {

                apiInterface.ExecuteBulkSqlStatement(sqlInsertArray).then(function (rowChanged) {

                    insertSuccess += rowChanged;

                    resolve(insertSuccess);

                });



            });


        });



    }


    /**
       * Create FactAcct berdasarkan API Completed
       */
    ARInvoiceCompleted(invoiceCMT) {

        let thisObject = this;
        let factacctService = new FactAcctService();
        let invoicelineService = new InvoicelineService();
        let elementValueService = new ElementValueService();
        let dataflow = { sql_array: [] };
        return new Promise(function (resolve, reject) {
            invoicelineService.FindInvoicelineComponentModelByCInvoiceUu(invoiceCMT.c_invoice_wx_c_invoice_uu)
                .then(function (invoicelineCMTList) {

                    _.forEach(invoicelineCMTList, function (invoicelineCMT) {

                        let insertSqlCr = factacctService.sqlInsertFactAcctStatement(elementValueService.ElementValueList().TradeRevenue,
                            'c_invoiceline', invoicelineCMT.c_invoiceline_wx_c_invoiceline_uu, null, invoicelineCMT.c_invoiceline_wx_linenetamt);
                        dataflow.sql_array.push(insertSqlCr);

                        // cek jika ada 21610 – Tax Due (taxamt)
                        if (invoicelineCMT.c_invoiceline_wx_taxamt) {
                            let insertSqlCrTax = factacctService.sqlInsertFactAcctStatement(elementValueService.ElementValueList().TaxDue,
                                'c_invoiceline', invoicelineCMT.c_invoiceline_wx_c_invoiceline_uu, null, invoicelineCMT.c_invoiceline_wx_taxamt);
                            dataflow.sql_array.push(insertSqlCrTax);

                        }

                    });

                    let insertSqlDr = factacctService.sqlInsertFactAcctStatement(elementValueService.ElementValueList().AccountReceivableTrade,
                        'c_invoice', invoiceCMT.c_invoice_wx_c_invoice_uu, invoiceCMT.c_invoice_wx_grandtotal, null);
                    dataflow.sql_array.push(insertSqlDr);




                    resolve(dataflow);


                });


        }).then(function (dataflow) {

            return new Promise(function (resolve, reject) {

                apiInterface.ExecuteBulkSqlStatement(dataflow.sql_array).then(function (inserted) {

                    resolve(inserted);

                });



            });

        });


    }


    ARInvoiceReversed(invoiceCMT) {
        let thisObject = this;
        let factacctService = new FactAcctService();
        let elementValueService = new ElementValueService();
        let invoicelineService = new InvoicelineService();
        let sqlInsertArray = [];
        //Account Receivable trade reversed
        let reverseAccountPayableTradeStatement = factacctService.sqlInsertReverseFactAcctStatement('c_invoice', invoiceCMT.c_invoice_wx_c_invoice_uu, elementValueService.ElementValueList().AccountReceivableTrade.value);

        sqlInsertArray.push(reverseAccountPayableTradeStatement);
        let insertSuccess = 0;

        return new Promise(function (resolve, reject) {
            invoicelineService.FindInvoicelineComponentModelByCInvoiceUu(invoiceCMT.c_invoice_wx_c_invoice_uu)
                .then(function (invoicelineCMTList) {

                    _.forEach(invoicelineCMTList, function (invoicelineCMT) {

                        // Trade Revenue
                        let sqlInsertReversedNotInvoiceReceipt = factacctService.sqlInsertReverseFactAcctStatement('c_invoiceline', invoicelineCMT.c_invoiceline_wx_c_invoiceline_uu, elementValueService.ElementValueList().TradeRevenue.value);
                        sqlInsertArray.push(sqlInsertReversedNotInvoiceReceipt);

                        if (invoicelineCMT.c_invoiceline_wx_taxamt) {
                            // TAX Due
                            let sqlInsertReversedTaxCredit = factacctService.sqlInsertReverseFactAcctStatement('c_invoiceline', invoicelineCMT.c_invoiceline_wx_c_invoiceline_uu, elementValueService.ElementValueList().TaxDue.value);
                            sqlInsertArray.push(sqlInsertReversedTaxCredit);
                        }

                    });

                    resolve(sqlInsertArray);
                });



        }).then(function (sqlInsertArray) {

            return new Promise(function (resolve, reject) {

                apiInterface.ExecuteBulkSqlStatement(sqlInsertArray).then(function (rowChanged) {

                    insertSuccess += rowChanged;

                    resolve(insertSuccess);

                });



            });


        });


    }




    /**
       * Create FactAcct berdasarkan API Completed
       */
    APInvoiceCompleted(invoiceCMT) {

        let thisObject = this;
        let factacctService = new FactAcctService();
        let invoicelineService = new InvoicelineService();
        let elementValueService = new ElementValueService();
        let dataflow = { sql_array: [] };
        return new Promise(function (resolve, reject) {
            invoicelineService.FindInvoicelineComponentModelByCInvoiceUu(invoiceCMT.c_invoice_wx_c_invoice_uu)
                .then(function (invoicelineCMTList) {

                    _.forEach(invoicelineCMTList, function (invoicelineCMT) {

                        let insertSqlDr = factacctService.sqlInsertFactAcctStatement(elementValueService.ElementValueList().NotInvoicedReceipt,
                            'c_invoiceline', invoicelineCMT.c_invoiceline_wx_c_invoiceline_uu, invoicelineCMT.c_invoiceline_wx_linenetamt, null);
                        dataflow.sql_array.push(insertSqlDr);

                        // cek jika ada tax
                        if (invoicelineCMT.c_invoiceline_wx_taxamt) {
                            let insertSqlDrTax = factacctService.sqlInsertFactAcctStatement(elementValueService.ElementValueList().TaxCreditAR,
                                'c_invoiceline', invoicelineCMT.c_invoiceline_wx_c_invoiceline_uu, invoicelineCMT.c_invoiceline_wx_taxamt, null);
                            dataflow.sql_array.push(insertSqlDrTax);

                        }

                    });

                    let insertSqlCr = factacctService.sqlInsertFactAcctStatement(elementValueService.ElementValueList().AccountPayableTrade,
                        'c_invoice', invoiceCMT.c_invoice_wx_c_invoice_uu, null, invoiceCMT.c_invoice_wx_grandtotal);
                    dataflow.sql_array.push(insertSqlCr);




                    resolve(dataflow);


                });


        }).then(function (dataflow) {

            return new Promise(function (resolve, reject) {

                apiInterface.ExecuteBulkSqlStatement(dataflow.sql_array).then(function (inserted) {

                    resolve(inserted);

                });



            });

        });


    }




}