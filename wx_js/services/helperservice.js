class HelperService {

    /**
* Update wx_isdeleted Y dengan column reference 
* m_warehouse_uu
* @param {*} tablename 
* @param {*} columnreference misal m_warehouse_uu
* @param {*} uuid  
* @param {*} docstatus DRAFT , COMPLETED, REVERSED, CLOSED
*/
    static SqlUpdateStatusStatement(tablename, columnreference, uuid, docstatus) {

        const sqlUpdateStatement = `UPDATE ` + tablename + ` SET docstatus = '` + docstatus + `'  , sync_client = null, process_date = null WHERE ` + columnreference + ` = '` + uuid + `';`;
        return sqlUpdateStatement;

    }

    static SqlUpdateStatement(tablename, data, column_uu) {
        const sets = [];

        let filter = '';
        _.forEach(data, (value, column) => {


            if ((value === undefined) || (value === null)) {

            } else {
                if (column !== column_uu) {
                    if (typeof value === 'number') {
                        let set = column + ' = ' + value;
                        sets.push(set);
                    } else {
                        let set = column + ' = \'' + value + '\'';
                        sets.push(set);
                    }

                } else {
                    filter = ' WHERE ' + column_uu + ' = ' + '\'' + value + '\'';

                }


            }

        });

        let sqlUpdate = '';
        if (filter) {
            sqlUpdate = 'UPDATE ' + tablename + ' SET ' + sets.join(',') + filter;
        }


        return sqlUpdate;

    }

    /**
    * Create Insert Replace Sql Statement
    * @param {*} tablename 
    * @param {*} data {column: value, column: value}
   
    */
    static SqlInsertOrReplaceStatement(tablename, data) {

        const columns = [];
        const values = [];
        _.forEach(data, (value, column) => {


            if ((value === undefined) || (value === null)) {
                columns.push(column);
                values.push('null');
            } else {
                if (typeof value === 'number') {
                    columns.push(column);
                    values.push(value);
                } else {
                    columns.push(column);
                    value = value.replace(/'/g, "''");
                    values.push('\'' + value + '\'');
                }

            }

        });

        const sqlInsert = 'INSERT OR REPLACE INTO ' + tablename + '(' + columns.join(',') + ') VALUES (' + values.join(',') + ');';
        return sqlInsert;

    }




    /**
 * Update wx_isdeleted Y dengan column reference 
 * m_warehouse_uu
 * @param {*} tablename 
 * @param {*} columnreference misal m_warehouse_uu
 * @param {*} uuid  
 */
    static SqlDeleteStatement(tablename, columnreference, uuid) {

        const sqlDeleteStatementUpdate = `UPDATE ` + tablename + ` SET wx_isdeleted = 'Y', sync_client = null, process_date = null WHERE ` + columnreference + ` = '` + uuid + `';`;
        return sqlDeleteStatementUpdate;

    }



    static UUID() {

        var dt = new Date().getTime();
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = (dt + Math.random() * 16) % 16 | 0;
            dt = Math.floor(dt / 16);
            return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
        return uuid;

    }

    /**
    * Convert row {nama_table_wx_nama_column : value, ...} menjadi {nameTable:{column:value}...} viewmodel , 
    * @param {*} rowData 
    */
    static ConvertRowDataToViewModel(rowData) {

        let viewmodel = {};
        _.forEach(rowData, function (value, key) {

            let table_wx_column = key.split('_wx_');
            let table = table_wx_column[0];
            let column = table_wx_column[1];

            if (!viewmodel[table_wx_column[0]]) {
                viewmodel[table_wx_column[0]] = {};
            }

            viewmodel[table_wx_column[0]][table_wx_column[1]] = value;
            // viewmodel[table_wx_column[0]] = value;
        });
        return viewmodel;

    }



    /**
       * validation untuk form sebelum submit 
       * @param {*} validationObject {
          id_form : '',
          m_warehouse_wx_name :{
              required : true
          }
  
      }
       */
    static CheckValidation(validationObject) {

        let isValid = true;
        _.forEach(validationObject, function (rules, idControl) {
            let fieldIdControl = '';

            if (validationObject.id_form) {
                fieldIdControl = '#' + validationObject.id_form + ' ';
            }
            fieldIdControl += '#' + idControl + ' ';


            _.forEach(rules, function (paramValue, paramKey) {

                switch (paramKey) {

                    case 'required':
                        if (paramValue) {
                            // cek jika value tidak ada , maka return false
                            if (!$(fieldIdControl).val()) {
                                $(fieldIdControl).addClass('is-invalid');
                                isValid = false;
                                $(fieldIdControl).get(0).scrollIntoView();
                                return;
                            } else {
                                $(fieldIdControl).removeClass('is-invalid');

                            }
                        }
                        break;

                }

            });

        });

        return isValid;

    }

    /**
    * Setelah input invalid , ketika form dibuka lagi, kondisi masih invalid, jadi harus di clear form-control yang invalid
    * @param {*} groupNameControl 
    */
    static RemoveInvalidControl(groupNameControl) {
        $(groupNameControl).each(function () {
            $(this).removeClass('is-invalid');
        });
    }

    /**
    * Convert viewmodel menjadi model row seperti database, nama_table_wx_nama_column
    * @param {*} viewmodel 
    */
    static ConvertViewModelToRow(viewmodel) {
        let listColumn = {};
        _.forEach(viewmodel, function (model, nameTable) {
            _.forEach(model, function (value, column) {
                let table_wx_column = nameTable + '_wx_' + column;
                listColumn[table_wx_column] = value;

            });
        });
        return listColumn;
    }

    

    static ConvertTimestampToLocateDateString(timestamp) {
        if (timestamp) {

            let locateDate = new Date(timestamp);
            var options = { year: 'numeric', month: 'long', day: 'numeric' };
            return locateDate.toLocaleDateString('id-ID', options);
        } else {
            return '';
        }


    }

    static ConvertTimestampToLocateDateTimeString(timestamp) {
        if (timestamp) {

            let locateDate = new Date(timestamp);
            var options = { year: 'numeric', month: 'long', day: 'numeric' };
            let dateFormat = locateDate.toLocaleDateString('id-ID', options);
            let timeFormat = locateDate.toLocaleTimeString();
            return dateFormat + ' ' + timeFormat;
        } else {
            return '';
        }


    }

    static ConvertPhoneWithAreaCode(phone, areacode) {
        let returnPhone = phone.trim();
        try {
            if (returnPhone.charAt(0) === "0") {
                returnPhone = areacode + returnPhone.substr(1);
            }
        } catch (err) {

            apiInterface.Log('HelperService', this.ConvertPhoneWithAreaCode.name, err);
        }


        return returnPhone;
    }

    static HtmlInfoBox() {
        let htmlBox = `<div class="modal fade" id="modal_info_box" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div id="content_modal_info_box" class="modal-body">
                
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button id="ok_info_box" type="button" class="btn btn-primary">OK</button>
                </div>
            </div>
        </div>
    </div>`;
        return htmlBox;

    }

    static DialogBox(idModalDialogBox, modalTitle) {

        if (!modalTitle) {
            modalTitle = 'Dialog Box';
        }

        let htmlBox = `<div class="modal fade" id="` + idModalDialogBox + `" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">`+ modalTitle + `</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div id="content_modal_info_box" class="modal-body">
                
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button id="ok_info_box" type="button" class="btn btn-primary">OK</button>
                </div>
            </div>
        </div>
    </div>`;
        return htmlBox;

    }


    /* static SetAppForConfig() {
 
 
 
         const sqlTemplate = '  FROM ad_user_uu_config_client_app ;';
 
         const listMetaColumn = _.union(metaColumn.ad_user_uu_config_client_app);
 
         let varMetaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);
 
         let configList = apiInterface.Query(varMetaDataQuery);
 
 
         _.forEach(configList, function (adUserConfig) {
 
             if (adUserConfig['ad_user_uu_config_client_app_wx_confname'] === 'APP_FOR') {
                 config.app_for = adUserConfig['ad_user_uu_config_client_app_wx_confvalue'];
             }
 
         });
 
 
     }*/

    /**
     * Memasukkan value from database to torm
     * @param {*} classNameControl 
     * @param {*} viewmodel 
     */
    static InjectViewModelToForm(selectorClassNameControl, viewModel) {

        $(selectorClassNameControl).each(function () {
            let id = $(this).attr('id');
            let arrayId = id.split("_wx_");

            if ($(this).attr('type') === "checkbox") {

                if (viewModel[arrayId[0]][arrayId[1]]) {
                    if (viewModel[arrayId[0]][arrayId[1]] === 'Y') {
                        $(this).prop('checked', true);
                    } else {
                        $(this).prop('checked', false);

                    }
                }

            } else if ($(this).hasClass('datepicker')) {
                let timestamp = viewModel[arrayId[0]][arrayId[1]];
                if (timestamp) {
                    $(this).datepicker('update', new Date(timestamp));
                } else {
                    $(this).datepicker('update', new Date());
                }
            } else {
                $(this).val(viewModel[arrayId[0]][arrayId[1]]);
            }


            // masukkan value

        });
    }

    static GetViewModelFromForm(classNameControl, viewModel) {

        try {
            $(classNameControl).each(function () {
                let id = $(this).attr('id');
                let arrayId = id.split("_wx_");

                if ($(this).attr('type') === "checkbox") {

                    if ($(this).prop('checked')) {
                        viewModel[arrayId[0]][arrayId[1]] = 'Y';
                    } else {
                        viewModel[arrayId[0]][arrayId[1]] = 'N';
                    }

                } else {

                    let value = $(this).val();
                    if ($(this).hasClass("wx-format-money")) {
                        value = value.replace(/\,/g, '');
                    }

                    if ($(this).hasClass('datepicker')) {
                        let stringDate = value.split('/');
                        let currentDate = new Date();
                        let mm = numeral(stringDate[0]).value() - 1; // javascript month start from 0
                        let dd = numeral(stringDate[1]).value();
                        let yy = numeral(stringDate[2]).value();
                        let hh = currentDate.getUTCHours();
                        let MM = currentDate.getUTCMinutes();
                        let ss = currentDate.getUTCSeconds();
                        let timestamp = Date.UTC(yy, mm, dd, hh, MM, ss)
                        value = timestamp;
                    }

                    if (viewModel[arrayId[0]]) {
                        viewModel[arrayId[0]][arrayId[1]] = value;
                    }



                }


                // masukkan value

            });

        } catch (err) {

            apiInterface.Log('HelperService', this.GetViewModelFromForm.name, err);
        }

        return viewModel;
    }


    /**
     * 
     * @param {*} periode 
     * return MM YYYY => July 2020
     */
    static ConvertPeriodeToLabel(periode) {
        let locateDate = this.ConvertPeriodeToDate(periode);
        var options = { year: 'numeric', month: 'long', day: 'numeric' };
        let dateFormat = locateDate.toLocaleDateString('id-ID', options);

        let dateArray = dateFormat.split(' ');
        return dateArray[1] + ' ' + dateArray[2];


    }

    static ConvertPeriodeToDate(periode) {

        let periodeArray = periode.split('-');
        let month = numeral(periodeArray[0]).value();
        let year = periodeArray[1];

        return new Date(year, month, 0);

    }

    /**
     * 
     * @param {*} periode dengan forma mm-yyyy
     * return {start_timestamp: xxxxxx, end_timestamp:}
     */
    static ConvertPeriodeToStartimeToEndTime(periode) {
        let periodeArray = periode.split('-');
        let month = numeral(periodeArray[0]).value();
        let year = periodeArray[1];
        let date = this.ConvertPeriodeToDate(periode);
        let daysInMonth = date.getDate();


        month = month - 1; //untuk mendapatkan utc timestamp, month harus dikurangin 1 karena js month dimulai dari 0

        let start_timestamp = Date.UTC(year, month, 1, 0, 0, 0);
        let end_timestamp = Date.UTC(year, month, daysInMonth, 0, 0, 0);

        return { 'start_timestamp': start_timestamp, 'end_timestamp': end_timestamp };


    }

    /**
     * 
     * @param {*} timestamp 
     * return {month: month, year: year}
     */
    static ConvertTimeStampToPeriode(timestamp) {

        let date = new Date(timestamp);
        let month = date.getUTCMonth();
        let year = date.getUTCFullYear();

        return { month: month, year: year };
    }

    /**
  * 
  * @param {*} timestamp 
  * return mm-yyyy
  */
    static ConvertTimeStampToPeriodeName(timestamp) {

        let date = new Date(timestamp);
        let month = date.getUTCMonth() + 1;
        let year = date.getUTCFullYear();
        if (month < 10) {
            month = '0' + month;
        }
        let periodeName = month + '-' + year;
        return periodeName;
    }

    /**
     * Mendapatkan object element value yang berisi amount
     * 
     * @param {*} idName ex: parent_value
     * @param {*} valueFilter ex:
     * @param {*} dataTotalSummary dataTotalSummary dalam format tree
     */
    static GetListObjectFromHierarchy(idname, valueFilter, account_type, dataTotalSummary) {



        var i = 0, found, result = [];

        for (; i < dataTotalSummary.length; i++) {
            if ((dataTotalSummary[i][idname] === valueFilter) && (dataTotalSummary[i]['account_type'] === account_type)) {
                result.push(dataTotalSummary[i]);
            } else if (_.isArray(dataTotalSummary[i].children)) {
                found = this.GetListObjectInSummaryTotalElement(idname, valueFilter, account_type, dataTotalSummary[i].children);
                if (found.length) {
                    result = result.concat(found);
                }
            }
        }

        return result;

    }

    /**
   * Mendapatkan object element value yang berisi amount
   * 
   * @param {*} idName ex: parent_value
   * @param {*} valueFilter ex:
   * @param {*} dataTotalSummary dataTotalSummary dalam format tree
   */
    static GetListObjectInSummaryTotalElement(idname, valueFilter, account_type, dataTotalSummary) {


        var i = 0, result = [];

        for (; i < dataTotalSummary.length; i++) {
            if ((dataTotalSummary[i][idname] === valueFilter) && (dataTotalSummary[i]['account_type'] === account_type)) {
                result.push(dataTotalSummary[i]);
            }
        }

        return _.uniqBy(result, 'value');

    }

    static GetNodeByValue(node) {

        if (node.children.length > 0) {
            // node['amount'] = 0;
            //node.value = 0;
            for (var i = 0; i < node.children.length; i++) {
                node.amount += this.GetNodeByValue(node.children[i]);
                // node.value += SumUpAmount(node.children[i]);
            }
        }



        return node.amount;
    }

    /*static FormatNumber(numbervalue) {


        numbervalue += '';
        x = numbervalue.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;

    }*/

    /**
     * Merubah value number 100000000000 => 1000.0000.0000
     * @param {*} value 
     */
    static ConvertValueNumberToValueDot(value) {
        return value.toString().match(/.{1,4}/g).join('.');
    }
    /**
   * Fungsi ini berguna untuk meberikan tablename pada column, sehingga menjadi tablename.columnname
   * @param {*} tableNameAlias
   * @param {[]} columns [columnName, columnName]
   */
    static CreateMetaTableColumn(tableNameAlias, columns) {
        let returnColumns = [];
        _.forEach(columns, function (columnName) {

            let aliasObjectColumn = tableNameAlias + "." + columnName;

            returnColumns.push(aliasObjectColumn);

        });
        return returnColumns;
    }

    /**
     * 
     * @param {*} tableNameAlias nama table di database misal c_order
     * @param {*} objectModel cukup object model misal new MWarehouse() 
     */
    static CreateMetaColumns(tableNameAlias, objectModel) {
        let returnColumns = [];
        let columns = Object.keys(objectModel);
        _.forEach(columns, function (columnName) {

            let aliasObjectColumn = tableNameAlias + "." + columnName;

            returnColumns.push(aliasObjectColumn);

        });
        return returnColumns;
    }

    /**
     * Convert viewmodel to array metacolumn
     * @param {*} viewModel 
     */
    static ConvertViewModelToMetaColumn(viewModel) {
        let thisObject = this;
        let listMetaColumn = [];
        _.forEach(viewModel, function (model, property) {
            let metaColumns = thisObject.CreateMetaColumns(property, model);
            listMetaColumn = _.union(metaColumns, listMetaColumn);

        });

        return listMetaColumn;
    }

    static GetInitialName(name) {

        let firstWordArray = name.split(' ');

        let initial = '';
        _.forEach(firstWordArray, function (word) {
            initial += word.charAt(0);

        });

        return initial;

    }

    /**
     * Append form modal pada wx_fragment_content
     */
    static InsertReplaceFormModal(idFormModal, formModalHtml) {
        if ($('#wx_fragment_content' + ' #' + idFormModal).length) {
            $('#wx_fragment_content' + ' #' + idFormModal).replaceWith(formModalHtml);

        } else {
            $('#wx_fragment_content').append(formModalHtml);
        }


    }

    static IsPeriodExpired(date_acct) {
        let start_timestamp = numeral(localStorage.getItem('start_timestamp')).value();
       // let end_timestamp = numeral(localStorage.getItem('end_timestamp')).value();
        let date_acct_numeric = numeral(date_acct).value();

        if (date_acct_numeric < start_timestamp) {
            //
            return true;
        }else{
            return false;
        }


    }



    /*
    static SetConfig(key,value){
        let configString = localStorage.getItem('config');
        let config = {};
        if (configString){
            config = JSON.parse(configString);
        }

        config[key] = value;
        localStorage.setItem('config', JSON.stringify(config));

    }

    static GetConfig(key){
        let configString = localStorage.getItem('config');
        let config = null;
        if (configString){
            config = JSON.parse(configString);
        }

        if (config){
            return config[key];
        }else{
            return null;
        }

    }*/
    /**
      * Generate document status html content
      * @param {*} docstatus
      * @param {*} idRadioDocStatus 
     
       static GenerateOptionStatusDialogHtml(docstatus, idRadioDocStatus) {
   
           // thisObject.idRadioDocStatus = 'c_order_wx_docstatus_' + orderViewModelTable.c_order_wx_c_order_uu;
   
           let statusDraftObject = { status: 'DRAFT', body: 'Simpan dokumen dengan status draft. Dokumen masih dapat dimodifikasi.' };
           let statusCompletedObject = { status: 'COMPLETED', body: 'Simpan dokumen dengan status completed. Dokumen tidak dapat dimodifikasi. Dan akan membuat transaksi record.' };
           let statusDeletedObject = { status: 'DELETED', body: 'Simpan dokumen dengan status deleted. Dokumen akan dihapus.' };
           let statusReversedObject = { status: 'REVERSED', body: 'Dokumen akan dibatalkan dan dibuatkan jurnal balik, jika ada transaksi sebelumnya yang terbentuk.' };
           let statusClosedObject = { status: 'CLOSED', body: 'Dokumen telah closed dan tidak dapat dibatalkan.' };
   
           let statusRow = [];
           switch (docstatus) {
   
               case 'DRAFT':
                   statusRow.push(statusDraftObject, statusCompletedObject, statusDeletedObject);
                   break;
               default:
                   statusRow.push(statusReversedObject, statusClosedObject);
                   break;
           }
   
           let rowHtml = '';
           _.forEach(statusRow, function (objectStatus, index) {
               let checked = '';
               if (index === 0) {
                   checked = 'checked';
               }
               let templateHtml = `
          <div class="row">
              <div class="col-lg-12">
                  <label class="kt-option">
                      <span class="kt-option__control">
                          <span class="kt-radio kt-radio--bold kt-radio--brand">
                              <input type="radio" name="`+ idRadioDocStatus + `" value="` + objectStatus.status + `" ` + checked + `>
                              <span></span>
                          </span>
                      </span>
                      <span class="kt-option__label">
                          <span class="kt-option__head">
                              <span class="kt-option__title">
                              `+ objectStatus.status + `
                              </span>								
                          </span>
                          <span class="kt-option__body">
                          `+ objectStatus.body + `
                          </span>
                      </span>
                  </label>
              </div>
          </div>`;
               rowHtml += templateHtml;
           });
   
   
           return rowHtml;
   
   
       } */


}