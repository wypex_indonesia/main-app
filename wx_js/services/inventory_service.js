/**

 */
class InventoryService {
    constructor() {

    }

    /**
	 * 
	 * @param {*} metaDataResponseTable 
	 */
    FindAll(metaDataResponseTable) {


        let otherFilters = '';
        if (metaDataResponseTable.otherFilters.length > 0) {

            _.forEach(metaDataResponseTable.otherFilters, function (value) {

                otherFilters += ' ' + value + ' ';
            });

        }

        const sqlTemplate = '  FROM m_inventory ' +
            ' INNER JOIN m_warehouse ON m_inventory.m_warehouse_uu = m_warehouse.m_warehouse_uu ' +
            ' INNER JOIN c_doctype ON m_inventory.c_doctype_id = c_doctype.c_doctype_id ' +
            ' WHERE m_inventory.wx_isdeleted = \'N\' ' + otherFilters + ' ORDER BY m_inventory.documentno DESC ';


        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new InventoryComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);


        return apiInterface.Query(metaDataQuery);
    }

    FindInventoryCMTListByMInventoryUu(m_inventory_uu) {

        const sqlTemplate = '  FROM m_inventory ' +
            ' INNER JOIN m_warehouse ON m_inventory.m_warehouse_uu = m_warehouse.m_warehouse_uu ' +
            ' INNER JOIN c_doctype ON m_inventory.c_doctype_id = c_doctype.c_doctype_id ' +
            ' WHERE m_inventory.m_inventory_uu = \'' + m_inventory_uu + '\' ';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new InventoryComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);



        return apiInterface.Query(metaDataQuery);
    }

    GetInventoryCMTByMInventoryUu(m_inventory_uu) {

        const sqlTemplate = '  FROM m_inventory ' +
            ' INNER JOIN m_warehouse ON m_inventory.m_warehouse_uu = m_warehouse.m_warehouse_uu ' +
            ' INNER JOIN c_doctype ON m_inventory.c_doctype_id = c_doctype.c_doctype_id ' +
            ' WHERE m_inventory.m_inventory_uu = \'' + m_inventory_uu + '\' ';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new InventoryComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);

        return new Promise(function (resolve, reject) {
            let rowDataList = apiInterface.Query(metaDataQuery);
            let dataCMT = null;
            if (rowDataList.length > 0) {
                dataCMT = rowDataList[0];
            }

            resolve(dataCMT);


        });

    }

    UpdateStatus(inventoryComponentModelTable) {
        let insertSuccess = 0;
        let sqlstatement = '';
        let thisObject = this;
        try {
            if (inventoryComponentModelTable.m_inventory_wx_docstatus === 'DELETED') {
                sqlstatement = HelperService.SqlDeleteStatement('m_inventory', 'm_inventory_uu', inventoryComponentModelTable.m_inventory_wx_m_inventory_uu);

            } else {

                //  this.CountAllTotal(inventoryComponentModelTable.m_inventory_wx_m_inventory_uu);
                sqlstatement = HelperService.SqlUpdateStatusStatement('m_inventory', 'm_inventory_uu',
                    inventoryComponentModelTable.m_inventory_wx_m_inventory_uu, inventoryComponentModelTable.m_inventory_wx_docstatus);
            }


            return new Promise(function (resolve, reject) {
                apiInterface.ExecuteSqlStatement(sqlstatement)
                    .then(function (rowChanged) {

                        insertSuccess += rowChanged

                        resolve(insertSuccess);
                    });



            }).then(function () {

                return new Promise(function (resolve, reject) {
                    if (insertSuccess) {
                        thisObject.RunProcessDocument(inventoryComponentModelTable).then(function (inserted) {
                            insertSuccess += inserted;
                            resolve(insertSuccess);

                        });
                    }




                });

            });
        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.UpdateStatus.name, error);
        }


    }

    /**
     * 
     * @param {*} inventoryComponentModel 
     * @param {*} datatableReference dataTableReferemce CInvoice datatable
     */
    async  SaveUpdate(inventoryComponentModel, datatableReference) {
        const timeNow = new Date().getTime();
        let thisObject = this;
        let isNewRecord = false;
        let insertSuccess = 0;
        try {
            if (!inventoryComponentModel.m_inventory.m_inventory_uu) { //new record
                isNewRecord = true;
                inventoryComponentModel.m_inventory.m_inventory_uu = HelperService.UUID();
                inventoryComponentModel.m_inventory.ad_client_uu = localStorage.getItem('ad_client_uu');
                inventoryComponentModel.m_inventory.ad_org_uu = localStorage.getItem('ad_org_uu');

                inventoryComponentModel.m_inventory.createdby = localStorage.getItem('ad_user_uu');
                inventoryComponentModel.m_inventory.updatedby = localStorage.getItem('ad_user_uu');

                inventoryComponentModel.m_inventory.created = timeNow;
                inventoryComponentModel.m_inventory.updated = timeNow;
            } else { //update record


                inventoryComponentModel.m_inventory.updatedby = localStorage.getItem('ad_user_uu');

                inventoryComponentModel.m_inventory.updated = timeNow;
            }

            inventoryComponentModel.m_inventory.m_warehouse_uu = inventoryComponentModel.m_warehouse.m_warehouse_uu;
            inventoryComponentModel.m_inventory.c_doctype_id = inventoryComponentModel.c_doctype.c_doctype_id;
            inventoryComponentModel.m_inventory.sync_client = null;
            inventoryComponentModel.m_inventory.process_date = null;


            const sqlInsert = HelperService.SqlInsertOrReplaceStatement('m_inventory', inventoryComponentModel.m_inventory);

            insertSuccess += await apiInterface.ExecuteSqlStatement(sqlInsert);
            if (insertSuccess > 0) {

                if (datatableReference) {

                    let rowDataList = await thisObject.FindInventoryCMTListByMInventoryUu(inventoryComponentModel.m_inventory.m_inventory_uu);
                    let rowData = rowDataList[0];
                    if (!isNewRecord) {

                        datatableReference.row($('#bttn_row_edit' + rowData['m_inventory_wx_m_inventory_uu']).parents('tr')).data(rowData).draw();
                    } else {

                        datatableReference.row.add(rowData).draw();
                    }
                }



            }
        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.SaveUpdate.name, error);
            return insertSuccess;
        }
        return insertSuccess;

    }



    /**
     * Run Process Document sesuai dengan docstatus :
     * 1. Draft -> Tidak ada Action
     * docbasetype :
     * AR Invoice
     * 
     * AP Invoice
     * 2. COMPLETED
     *  2.2. FactAcct 
     * 3. REVERSED
     *  2.1. FactAcct -> Reversed
     * 
     * @param {*} inventoryComponentModelTable 
     */
    RunProcessDocument(inventoryCMT) {
        let thisObject = this;
        let insertSuccess = 0;
        let elementValueService = new ElementValueService();
        let materialTransactionService = new MaterialTransactionService();
        let inventorylineService = new InventorylineService();
        let mLocatorService = new LocatorService();
        let dataflow = {};
        try {
            if (inventoryCMT.m_inventory_wx_docstatus === 'COMPLETED') {
                return new Promise(function (resolve, reject) {

                    Promise.all([mLocatorService.FindLocatorComponentModelByMWarehouseUu(inventoryCMT.m_warehouse_wx_m_warehouse_uu),
                    inventorylineService.FindInventorylineCMTListByMInventoryUu(inventoryCMT.m_inventory_wx_m_inventory_uu)])
                        .then(function (resultArray) {

                            let inventoryCMTList = resultArray[0];
                            let inventoryCMT = inventoryCMTList[0];

                            let inventorylineCMTList = resultArray[1];
                            dataflow.inventory_cmt = inventoryCMT;
                            dataflow.inventoryline_cmtlist = inventorylineCMTList;
                            resolve(dataflow);
                        });
                }).then(function (dataflow) {
                    return new Promise(function (resolve, reject) {

                        let sqlInsertArray = [];
                        let promiseArray = [];
                        _.forEach(dataflow.inventoryline_cmtlist, function (inventorylineCMT) {
                            let sqlArray = thisObject.MMInventoryCompletedSqlStatement(inventorylineCMT);
                            sqlInsertArray = sqlInsertArray.concat(sqlArray);

                            // jika hasil perhitungan lebih besar daripada stock, maka isOut = false;
                            /// karena  artinya barang bertambah
                            let isOut = true;
                            let inventoryShrinkage = (inventorylineCMT.m_inventoryline_wx_qtycount - inventorylineCMT.m_inventoryline_wx_qtybook);
                            if (inventoryShrinkage > 0) {
                                isOut = false;
                            }

                            if (inventoryShrinkage < 0) {
                                isOut = true;
                                inventoryShrinkage = inventoryShrinkage * -1;
                            }

                            promiseArray.push(materialTransactionService.sqlInsertMTransaction(elementValueService.MovementType().StockOpname,
                                dataflow.inventory_cmt.m_locator_wx_m_locator_uu, 'm_inventoryline', inventorylineCMT.m_inventoryline_wx_m_inventoryline_uu,
                                inventorylineCMT.m_product_wx_m_product_uu, inventoryShrinkage, isOut));

                        });

                        promiseArray.push(apiInterface.ExecuteBulkSqlStatement(sqlInsertArray));
                        Promise.all(promiseArray).then(function (resultArray) {
                            _.forEach(resultArray, function (inserted) {
                                insertSuccess += inserted;

                            });
                            resolve(insertSuccess);
                        });


                    });

                });

            }
        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.RunProcessDocument.name, error);
            return new Promise(function (resolve, reject) { resolve(0) });
        }


    }






    /**
    * Rumus perhitungan shrinkage adalah quantity count – stock saat ini * cogs product, 
    * jadi jika stock -(5) maka perhitungan akan 5 -(-5) * cogs, 
    * jika hasil pengurangan hasil count dan stock adalah minus, 
    * maka debit credit akan berubah juga, DR 56100 – Inventory Shrinkage, dan CR 14120 Product Asset
    * @param {*} inventoryCMT 
    */
    MMInventoryCompletedSqlStatement(inventorylineCMT) {
        let thisObject = this;

        let elementValueService = new ElementValueService();
        let factacctService = new FactAcctService();

        let currentCostprice = inventorylineCMT.m_inventoryline_wx_currentcostprice;
        let qtycount = inventorylineCMT.m_inventoryline_wx_qtycount;
        let qtybook = inventorylineCMT.m_inventoryline_wx_qtybook;
        let inventoryShrinkage = (qtycount - qtybook) * currentCostprice;

        let sqlInsertArray = [];

        try {
            // jika plus maka DR ( Product Asset) CR(Inventory Shrinkage) 
            if (inventoryShrinkage > 0) {

                let sqlProductAsset = factacctService.sqlInsertFactAcctStatement(elementValueService.ElementValueList().ProductAsset, 'm_inventoryline',
                    inventorylineCMT.m_inventoryline_wx_m_inventoryline_uu, inventoryShrinkage, null);

                let sqlInventoryShrinkage = factacctService.sqlInsertFactAcctStatement(elementValueService.ElementValueList().InventoryShrinkage, 'm_inventoryline',
                    inventorylineCMT.m_inventoryline_wx_m_inventoryline_uu, null, inventoryShrinkage);
                sqlInsertArray.push(sqlProductAsset, sqlInventoryShrinkage);
            }

            // jika minus maka DR(Inventory Shrinkage) CR(Product Asset)
            if (inventoryShrinkage < 0) {
                inventoryShrinkage = inventoryShrinkage * -1;
                let sqlInventoryShrinkage = factacctService.sqlInsertFactAcctStatement(elementValueService.ElementValueList().InventoryShrinkage, 'm_inventoryline',
                    inventorylineCMT.m_inventoryline_wx_m_inventoryline_uu, inventoryShrinkage, null);

                let sqlProductAsset = factacctService.sqlInsertFactAcctStatement(elementValueService.ElementValueList().ProductAsset, 'm_inventoryline',
                    inventorylineCMT.m_inventoryline_wx_m_inventoryline_uu, null, inventoryShrinkage);
                sqlInsertArray.push(sqlInventoryShrinkage, sqlProductAsset);
            }

        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.MMInventoryCompleted.name, error);
        }

        return sqlInsertArray;

    }




}