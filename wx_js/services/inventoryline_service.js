

class InventorylineService {
    constructor() { }


    /**
	 * 
	 * @param {*} metaDataResponseTable 
	 */
    FindAll(metaDataResponseTable) {


        let otherFilters = '';
        if (metaDataResponseTable.otherFilters.length > 0) {

            _.forEach(metaDataResponseTable.otherFilters, function (value) {

                otherFilters += ' ' + value + ' ';
            });

        }

        const sqlTemplate = '  FROM m_inventoryline ' +
            ' INNER JOIN m_inventory ON m_inventory.m_inventory_uu = m_inventoryline.m_inventory_uu ' +
            ' INNER JOIN m_product ON m_product.m_product_uu  = m_inventoryline.m_product_uu ' +
            ' INNER JOIN c_uom ON c_uom.c_uom_uu = m_product.c_uom_uu ' +
            ' WHERE m_inventoryline.wx_isdeleted = \'N\' ' + otherFilters + ' ORDER BY m_inventoryline.line ASC ';



        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new InventorylineComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);

    }

    FindInventorylineCMTListByMInventoryUu(m_inventory_uu) {

        const sqlTemplate = '  FROM m_inventoryline ' +
            ' INNER JOIN m_inventory ON m_inventory.m_inventory_uu = m_inventoryline.m_inventory_uu ' +
            ' INNER JOIN m_product ON m_product.m_product_uu  = m_inventoryline.m_product_uu ' +
            ' INNER JOIN c_uom ON c_uom.c_uom_uu = m_product.c_uom_uu ' +
            ' WHERE m_inventoryline.wx_isdeleted = \'N\' AND m_inventoryline.m_inventory_uu = \'' + m_inventory_uu + '\' ORDER BY m_inventoryline.line ASC';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new InventorylineComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }


    FindInventorylineCMTListByMInventorylineUu(m_inventoryline_uu) {

        const sqlTemplate = '  FROM m_inventoryline ' +
            ' INNER JOIN m_inventory ON m_inventory.m_inventory_uu = m_inventoryline.m_inventory_uu ' +
            ' INNER JOIN m_product ON m_product.m_product_uu  = m_inventoryline.m_product_uu ' +
            ' INNER JOIN c_uom ON c_uom.c_uom_uu = m_product.c_uom_uu ' +
            ' WHERE m_inventoryline.m_inventoryline_uu = \'' + m_inventoryline_uu + '\'';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new InventorylineComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }




    /**
     * Save Update harus merefer pada OrderViewModelTable
     * @param {MInventorylineModel} invetoryline
     * @param {*} datatableReference dataTableReferemce MInventoryline datatable
     */
    async  SaveUpdate(m_inventoryline, datatableReference) {
        const timeNow = new Date().getTime();
        let thisObject = this;
        let isNewRecord = false;
        let insertSuccess = 0;
        try {
            if (!m_inventoryline.m_inventoryline_uu) { //new record
                isNewRecord = true;
                m_inventoryline.m_inventoryline_uu = HelperService.UUID();
                m_inventoryline.ad_client_uu = localStorage.getItem('ad_client_uu');
                m_inventoryline.ad_org_uu = localStorage.getItem('ad_org_uu');

                m_inventoryline.createdby = localStorage.getItem('ad_user_uu');
                m_inventoryline.updatedby = localStorage.getItem('ad_user_uu');

                m_inventoryline.created = timeNow;
                m_inventoryline.updated = timeNow;
            } else { //update record


                m_inventoryline.updatedby = localStorage.getItem('ad_user_uu');

                m_inventoryline.updated = timeNow;
            }

            m_inventoryline.sync_client = null;
            m_inventoryline.process_date = null;


            const sqlInsert = HelperService.SqlInsertOrReplaceStatement('m_inventoryline', m_inventoryline);
            insertSuccess += await apiInterface.ExecuteSqlStatement(sqlInsert);

            if (datatableReference) {
                if (insertSuccess > 0) {

                    let rowDataList = await thisObject.FindInventorylineCMTListByMInventorylineUu(m_inventoryline.m_inventoryline_uu);
                    let rowData = null;
                    if (rowDataList.length) {
                        rowData = rowDataList[0];


                        if (!isNewRecord) {

                            datatableReference.row($('#bttn_row_edit' + rowData['m_inventoryline_wx_m_inventoryline_uu']).parents('tr')).data(rowData).draw();
                        } else {

                            datatableReference.row.add(rowData).draw();
                        }

                    }


                }
            }

        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.SaveUpdate.name, error);
            return new Promise(function (resolve, reject) { resolve(0); })
        }

        return insertSuccess;

    }

    UpdateQtyCount(m_inventoryline_uu, qtycount) {

        qtycount = numeral(qtycount).value();
        let ad_user_uu = localStorage.getItem('ad_user_uu');
        let timeNow = new Date().getTime();

        if (qtycount === null) {
            qtycount = 0;
        }

        const sqlTemplate = 'UPDATE m_inventoryline SET qtycount = ' + qtycount + ', updated = ' + timeNow + ' , updatedby = \'' + ad_user_uu + '\' WHERE m_inventoryline_uu = \'' + m_inventoryline_uu + '\';';


        return apiInterface.ExecuteSqlStatement(sqlTemplate);

    }

    /**
     * Buat new inventoryline, sekaligus set qtybook pada storageonhand
     */
    CreateNewInventoryline(m_product_uu, inventoryCMT, datatableInventoryline) {
        let thisObject = this;

        try {

            let newInventoryline = new MInventoryline();
            newInventoryline.m_product_uu = m_product_uu;
            newInventoryline.m_inventory_uu = inventoryCMT.m_inventory_wx_m_inventory_uu;
            newInventoryline.currentcostprice = 0.0;

            let costService = new CostService();
            let locatorService = new LocatorService();
            let storageonhandService = new StorageOnHandService();

            let dataflow = {};
            return new Promise(function (resolve, reject) {
                Promise.all([costService.GetTotalCostByMProductUu(m_product_uu),
                locatorService.FindLocatorComponentModelByMWarehouseUu(inventoryCMT.m_inventory_wx_m_warehouse_uu)])
                    .then(function (resultArray) {

                        let totalCost = resultArray[0];
                        let locatorCMTList = resultArray[1];
                        let locatorCMT = locatorCMTList[0];

                        dataflow.total_cost = totalCost;
                        dataflow.locator_cmt = locatorCMT;
                        resolve(dataflow);
                    });


            }).then(function (dataflow) {

                return new Promise(function (resolve, reject) {

                    storageonhandService.GetStorageonhandCMTList(dataflow.locator_cmt.m_locator_wx_m_locator_uu, m_product_uu)
                        .then(function (storageonhandCMTList) {
                            let lengthRow = datatableInventoryline.data().length;

                            newInventoryline.line = ++lengthRow;

                            // jika ada set m_inventoryline 
                            if (storageonhandCMTList.length) {
                                let storageonhandCMT = storageonhandCMTList[0];
                                newInventoryline.qtybook = storageonhandCMT.m_storageonhand_wx_qtyonhand;
                            } else {
                                //jika tidak ada set 0 pada newInventoryline
                                newInventoryline.qtybook = 0.0;
                            }
                            newInventoryline.currentcostprice = dataflow.total_cost;
                            resolve(newInventoryline);

                        });

                });
            }).then(function () {

                return new Promise(function (resolve, reject) {
                    thisObject.SaveUpdate(newInventoryline, datatableInventoryline).then(function (inserted) {
                        $('#input_' + newInventoryline.m_inventoryline_uu).focus();
                        resolve(inserted);

                    });


                });



            });




        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.CreateNewInventoryline.name, error);
            return new Promise(function (resolve, reject) { resolve(0); });
        }




    }




}