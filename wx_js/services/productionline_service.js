

class ProductionlineService {
    constructor() { }


    /**
	 * 
	 * @param {*} metaDataResponseTable 
	 */
    FindAll(metaDataResponseTable) {


        let otherFilters = '';
        if (metaDataResponseTable.otherFilters.length > 0) {

            _.forEach(metaDataResponseTable.otherFilters, function (value) {

                otherFilters += ' ' + value + ' ';
            });

        }

        const sqlTemplate = '  FROM m_productionline ' +
            ' INNER JOIN m_production ON m_production.m_production_uu = m_productionline.m_production_uu ' +
            ' INNER JOIN m_product ON m_product.m_product_uu  = m_productionline.m_product_uu ' +
            ' INNER JOIN c_uom ON c_uom.c_uom_uu = m_product.c_uom_uu ' +
            ' WHERE m_productionline.wx_isdeleted = \'N\' ' + otherFilters + ' ORDER BY m_productionline.line ASC ';



        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new ProductionlineComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);

    }

    FindProductionlineCMTListByMProductionUu(m_production_uu) {

        const sqlTemplate = '  FROM m_productionline ' +
            ' INNER JOIN m_production ON m_production.m_production_uu = m_productionline.m_production_uu ' +
            ' INNER JOIN m_product ON m_product.m_product_uu  = m_productionline.m_product_uu ' +
            ' INNER JOIN c_uom ON c_uom.c_uom_uu = m_product.c_uom_uu ' +
            ' WHERE m_productionline.wx_isdeleted = \'N\' AND m_productionline.m_production_uu = \'' + m_production_uu + '\' ORDER BY m_productionline.line ASC';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new ProductionlineComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }


    FindProductionlineCMTListByMProductionlineUu(m_productionline_uu) {

        const sqlTemplate = '  FROM m_productionline ' +
            ' INNER JOIN m_production ON m_production.m_production_uu = m_productionline.m_production_uu ' +
            ' INNER JOIN m_product ON m_product.m_product_uu  = m_productionline.m_product_uu ' +
            ' INNER JOIN c_uom ON c_uom.c_uom_uu = m_product.c_uom_uu ' +
            ' WHERE m_productionline.m_productionline_uu = \'' + m_productionline_uu + '\'';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new ProductionlineComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }



    SaveUpdate(productionlineCM, datatableProductionline) {
        let thisObject = this;
        let inventorylineService = new InventorylineService();
        let isNewRecord = false;
        let insertSuccess = 0;
        if (!productionlineCM.m_productionline.m_productionline_uu) { //new record
            isNewRecord = true;
        }
        let sqlInsertStatement = thisObject.GenerateInsertSqlStatement(productionlineCM.m_productionline);

        return new Promise(function (resolve, reject) {

            apiInterface.ExecuteSqlStatement(sqlInsertStatement).then(function (inserted) {

                insertSuccess += inserted;
                resolve(insertSuccess);

            });

        }).then(function (insertSuccess) {


            return new Promise(function (resolve, reject) {
                if (insertSuccess > 0) {

                    if (datatableProductionline) {

                        inventorylineService.FindInventorylineCMTListByMInventorylineUu(m_productionline.m_productionline_uu)
                            .then(function (rowDataList) {

                                let rowData = null;
                                if (rowDataList.length) {
                                    rowData = rowDataList[0];
                                    if (!isNewRecord) {

                                        datatableProductionline.row($('#bttn_row_edit' + rowData['m_productionline_wx_m_productionline_uu']).parents('tr')).data(rowData).draw();
                                    } else {

                                        datatableProductionline.row.add(rowData).draw();
                                    }
                                }
                                resolve(insertSuccess);
                            });

                    } else {
                        resolve(insertSuccess);
                    }
                } else {

                    resolve(insertSuccess);
                }


            });


        });

    }


    /**
     * Save Update harus merefer pada OrderViewModelTable
     * @param {ProductionlineComponentModel} productionlineCM
     * @param {*} datatableReference dataTableReferemce MInventoryline datatable
     */
    GenerateInsertSqlStatement(m_productionline) {
        const timeNow = new Date().getTime();

        let isNewRecord = false;
        if (!m_productionline.m_productionline_uu) { //new record
            isNewRecord = true;
            m_productionline.m_productionline_uu = HelperService.UUID();
            m_productionline.ad_client_uu = localStorage.getItem('ad_client_uu');
            m_productionline.ad_org_uu = localStorage.getItem('ad_org_uu');

            m_productionline.createdby = localStorage.getItem('ad_user_uu');
            m_productionline.updatedby = localStorage.getItem('ad_user_uu');

            m_productionline.created = timeNow;
            m_productionline.updated = timeNow;
        } else { //update record


            m_productionline.updatedby = localStorage.getItem('ad_user_uu');

            m_productionline.updated = timeNow;
        }


        m_productionline.sync_client = null;
        m_productionline.process_date = null;


        const sqlInsert = HelperService.SqlInsertOrReplaceStatement('m_productionline', m_productionline);



        return sqlInsert;

    }

    /**
     * Update semua production line , ketika ada update productionqty di production.
     *  rumus : BomQtylast = qtyProductionLast/qtyProductionfirst * bomQtyfirst;
     * @param {*} comparisonQtylastAndQtyFirst perbandingan productionQtyLast/productionQtyFirst
     * @param {*} m_production_uu 
     */
    UpdateMovementQty(comparisonQtylastAndQtyFirst, m_production_uu) {

        let thisObject = this;
        let insertSuccess = 0;


        return new Promise(function (resolve, reject) {

            thisObject.FindProductionlineCMTListByMProductionUu(m_production_uu)
                .then(function (productionlineCMTList) {
                    let sqlUpdateArray = [];
                    _.forEach(productionlineCMTList, function (productionlineCMT) {
                        let bomQtylast = comparisonQtylastAndQtyFirst * productionlineCMT.m_productionline_wx_movementqty;
                        const sqlTemplate = 'UPDATE m_productionline SET movementqty = ' + bomQtylast + ' WHERE m_productionline_uu = \'' + productionlineCMT.m_productionline_wx_m_productionline_uu + '\';';

                        sqlUpdateArray.push(sqlTemplate);

                    });
                    resolve(sqlUpdateArray);

                });


        }).then(function (sqlUpdateArray) {

            return new Promise(function (resolve, reject) {

                apiInterface.ExecuteBulkSqlStatement(sqlUpdateArray).then(function (inserted) {
                    insertSuccess += inserted;
                    resolve(insertSuccess);

                });


            });

        });




    }

    /**
     * Buat new inventoryline, sekaligus set qtybook pada storageonhand
     */
    GenerateProductionline(productionCMT, lengthRow) {
        let thisObject = this;
        let productBomService = new ProductBomService();
        let insertSuccess = 0;
        return new Promise(function (resolve, reject) {

            productBomService.FindProductBomCMByMProductUu(productionCMT.m_product_wx_m_product_uu)
                .then(function (productBomCMTList) {

                    let sqlInsertArray = [];
                    _.forEach(productBomCMTList, function (productBomCMT) {
                        let productionline = new MProductionline();
                        productionline.line = ++lengthRow;
                        productionline.m_production_uu = productionCMT.m_production_wx_m_production_uu;
                        productionline.m_product_uu = productBomCMT.m_product_bom_wx_m_productbom_uu;
                        productionline.movementqty = productionCMT.m_production_wx_productionqty * productBomCMT.m_product_bom_wx_bomqty;


                        sqlInsertArray.push(thisObject.GenerateInsertSqlStatement(productionline));

                    })
                    resolve(sqlInsertArray);

                });



        }).then(function (sqlInsertArray) {

            return new Promise(function (resolve, reject) {
                apiInterface.ExecuteBulkSqlStatement(sqlInsertArray).then(function (inserted) {

                    insertSuccess += inserted;
                    resolve(insertSuccess);

                });


            });

        });


    }



}




