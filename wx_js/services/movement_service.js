
class MovementService {
    constructor() {

    }

    /**
	 * 
	 * @param {*} metaDataResponseTable 
	 */
    FindAll(metaDataResponseTable) {


        let otherFilters = '';
        if (metaDataResponseTable.otherFilters.length > 0) {

            _.forEach(metaDataResponseTable.otherFilters, function (value) {

                otherFilters += ' ' + value + ' ';
            });

        }

        const sqlTemplate = '  FROM m_movement ' +
            ' INNER JOIN m_warehouse m_warehouse_from ON m_movement.m_warehouse_from_uu = m_warehouse_from.m_warehouse_uu ' +
            ' INNER JOIN m_warehouse m_warehouse_to ON m_movement.m_warehouse_to_uu = m_warehouse_to.m_warehouse_uu ' +
            ' INNER JOIN c_doctype ON m_movement.c_doctype_id = c_doctype.c_doctype_id ' +
            ' WHERE m_movement.wx_isdeleted = \'N\' ' + otherFilters + ' ORDER BY m_movement.documentno DESC ';


        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new MovementComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);


        return apiInterface.Query(metaDataQuery);
    }

    FindMovementCMByMMovementUu(m_movement_uu) {

        const sqlTemplate = '  FROM m_movement ' +
            ' INNER JOIN m_warehouse m_warehouse_from ON m_movement.m_warehouse_from_uu = m_warehouse_from.m_warehouse_uu ' +
            ' INNER JOIN m_warehouse m_warehouse_to ON m_movement.m_warehouse_to_uu = m_warehouse_to.m_warehouse_uu ' +
            ' INNER JOIN c_doctype ON m_movement.c_doctype_id = c_doctype.c_doctype_id ' +
            ' WHERE m_movement.m_movement_uu = \'' + m_movement_uu + '\' ';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new MovementComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);


        return apiInterface.Query(metaDataQuery);
    }

    UpdateStatus(movementCMT) {
        let insertSuccess = 0;
        let sqlstatement = '';
        let thisObject = this;
        if (movementCMT.m_movement_wx_docstatus === 'DELETED') {
            sqlstatement = HelperService.SqlDeleteStatement('m_movement', 'm_movement_uu', movementCMT.m_movement_wx_m_movement_uu);

        } else {


            sqlstatement = HelperService.SqlUpdateStatusStatement('m_movement', 'm_movement_uu',
                movementCMT.m_movement_wx_m_movement_uu, movementCMT.m_movement_wx_docstatus);
        }

        return new Promise(function (resolve, reject) {
            apiInterface.ExecuteSqlStatement(sqlstatement)
                .then(function (rowChanged) {

                    insertSuccess += rowChanged;
                    if (insertSuccess) {
                        thisObject.RunProcessDocument(movementCMT);
                    }


                    resolve(insertSuccess);


                });




        });
    }

    /**
     * 
     * @param {*} movementCM 
     * @param {*} datatableReference dataTableReferemce CInvoice datatable
     */
    SaveUpdate(movementCM, datatableReference) {
        const timeNow = new Date().getTime();
        let thisObject = this;
        let isNewRecord = false;

        try {
            if (!movementCM.m_movement.m_movement_uu) { //new record
                isNewRecord = true;
                movementCM.m_movement.m_movement_uu = HelperService.UUID();
                movementCM.m_movement.ad_client_uu = localStorage.getItem('ad_client_uu');
                movementCM.m_movement.ad_org_uu = localStorage.getItem('ad_org_uu');

                movementCM.m_movement.createdby = localStorage.getItem('ad_user_uu');
                movementCM.m_movement.updatedby = localStorage.getItem('ad_user_uu');

                movementCM.m_movement.created = timeNow;
                movementCM.m_movement.updated = timeNow;
            } else { //update record


                movementCM.m_movement.updatedby = localStorage.getItem('ad_user_uu');

                movementCM.m_movement.updated = timeNow;
            }


            movementCM.m_movement.m_warehouse_from_uu = movementCM.m_warehouse_from.m_warehouse_uu;
            movementCM.m_movement.m_warehouse_to_uu = movementCM.m_warehouse_to.m_warehouse_uu;
            movementCM.m_movement.c_doctype_id = movementCM.c_doctype.c_doctype_id;
            movementCM.m_movement.sync_client = null;
            movementCM.m_movement.process_date = null;

            let insertSuccess = 0;
            const sqlInsert = HelperService.SqlInsertOrReplaceStatement('m_movement', movementCM.m_movement);


            return new Promise(function (resolve, reject) {
                apiInterface.ExecuteSqlStatement(sqlInsert).then(function (rowChanged) {
                    insertSuccess += rowChanged;
                    if (insertSuccess > 0) {


                        thisObject.FindMovementCMByMMovementUu(movementCM.m_movement.m_movement_uu)
                            .then(function (rowDataList) {

                                let rowData = {};

                                if (rowDataList.length > 0) {
                                    rowData = rowDataList[0];
                                }

                                if (datatableReference) {

                                    if (!isNewRecord) {

                                        datatableReference.row($('#bttn_row_edit' + rowData['m_movement_wx_m_movement_uu']).parents('tr')).data(rowData).draw();
                                    } else {

                                        datatableReference.row.add(rowData).draw();
                                    }

                                }

                                resolve(insertSuccess);


                            });




                    } else {

                        resolve(insertSuccess);
                    }



                });



            });
        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.SaveUpdate.name, error);
        }



    }





    /**
     * Run Process Document sesuai dengan docstatus :
     * 1. Draft -> Tidak ada Action
     * 2. COMPLETED
     *  1. Ambil rmaline
     *  1. MTransaction 
     * MovementFrom	Minus	M_Movementline
       MovementTo	Plus	M_Movementline

   t   
     * 3. REVERSED
     *  2.1. MTransaction -> Reversed
     * 
     * @param {*} movementCMT 
   */
    RunProcessDocument(movementCMT) {
        let thisObject = this;
        let insertSuccess = 0;
        let elementValueService = new ElementValueService();
        let materialTransactionService = new MaterialTransactionService();
        let movementlineService = new MovementlineService;
        let locatorService = new LocatorService();
        let dataflow = {};

        try {

            if ((movementCMT.m_movement_wx_docstatus === 'COMPLETED') || (movementCMT.m_movement_wx_docstatus === 'REVERSED')) {
                return new Promise(function (resolve, reject) {
                    Promise.all([locatorService.FindLocatorComponentModelByMWarehouseUu(movementCMT.m_warehouse_from_wx_m_warehouse_uu),
                    locatorService.FindLocatorComponentModelByMWarehouseUu(movementCMT.m_warehouse_to_wx_m_warehouse_uu),
                    movementlineService.FindMovementlineCMTByMMovementUu(movementCMT.m_movement_wx_m_movement_uu)]).then(function (resultArray) {
                        let movementfromCMTList = resultArray[0];
                        let movementtoCMTList = resultArray[1];
                        let movementlineCMTList = resultArray[2];
                        let movementfromCMT = movementfromCMTList[0];
                        let movementtoCMT = movementtoCMTList[0];
                        dataflow.movement_from_cmt = movementfromCMT;
                        dataflow.movement_to_cmt = movementtoCMT;
                        dataflow.movementline_cmt_list = movementlineCMTList;
                        resolve(dataflow);

                    });

                }).then(function (dataflow) {

                    return new Promise(function (resolve, reject) {

                        let promiseArray = [];
                        let movementTypeFrom = elementValueService.MovementType().MovementFrom;
                        let movementTypeTo = elementValueService.MovementType().MovementTo;

                        _.forEach(dataflow.movementline_cmt_list, function (movementlineCMT) {

                            if (movementCMT.m_movement_wx_docstatus === 'COMPLETED') {
                                // MOVEMENT FROM
                                promiseArray.push(materialTransactionService.sqlInsertMTransaction(movementTypeFrom,
                                    dataflow.movement_from_cmt.m_locator_wx_m_locator_uu, 'm_movementline',
                                    movementlineCMT.m_movementline_wx_m_movementline_uu, movementlineCMT.m_movementline_wx_m_product_uu,
                                    movementlineCMT.m_movementline_wx_movementqty, true));
                                //MOVEMENT_TO

                                promiseArray.push(materialTransactionService.sqlInsertMTransaction(movementTypeTo,
                                    dataflow.movement_to_cmt.m_locator_wx_m_locator_uu, 'm_movementline',
                                    movementlineCMT.m_movementline_wx_m_movementline_uu, movementlineCMT.m_movementline_wx_m_product_uu,
                                    movementlineCMT.m_movementline_wx_movementqty, false));


                            }

                            if (movementCMT.m_movement_wx_docstatus === 'REVERSED') {

                                promiseArray.push(materialTransactionService.sqlInsertReverseMTransaction(elementValueService.MovementType().MovementFromReverse,
                                    movementTypeFrom, 'm_movementline', movementlineCMT.m_movementline_wx_m_movementline_uu));

                                // REVERSED MOVEMENT_TO

                                promiseArray.push(materialTransactionService.sqlInsertReverseMTransaction(elementValueService.MovementType().MovementToReverse,
                                    movementTypeTo, 'm_movementline', movementlineCMT.m_movementline_wx_m_movementline_uu));

                            }


                        });
                        Promise.all(promiseArray).then(function (resultArray) {
                            _.forEach(resultArray, function (inserted) {
                                insertSuccess += inserted;

                            });
                            resolve(insertSuccess);

                        });

                    });

                });
            } else {
                return new Promise(function (resolve, reject) {
                    resolve(1);

                });


            }

        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.RunProcessDocument.name, error);
            {
                return new Promise(function (resolve, reject) {
                    resolve(0);

                });

            }
        }
    }
}