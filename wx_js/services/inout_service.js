/**
 * MMS (MM Shipment) 3000
 * MMR (MM Receipt)  4000
 */
class InoutService {
    constructor() {

    }

    /**
	 * 
	 * @param {*} metaDataResponseTable 
	 */
    FindAll(metaDataResponseTable) {


        let otherFilters = '';
        if (metaDataResponseTable.otherFilters.length > 0) {

            _.forEach(metaDataResponseTable.otherFilters, function (value) {

                otherFilters += ' ' + value + ' ';
            });

        }

        const sqlTemplate = '  FROM m_inout ' +
            ' INNER JOIN c_bpartner ON m_inout.c_bpartner_uu = c_bpartner.c_bpartner_uu ' +
            ' INNER JOIN c_bpartner_location ON c_bpartner.c_bpartner_uu  = c_bpartner_location.c_bpartner_uu ' +
            ' INNER JOIN c_order ON c_order.c_order_uu = m_inout.c_order_uu ' +
            ' INNER JOIN m_shipper ON m_shipper.m_shipper_uu = m_inout.m_shipper_uu ' +
            ' INNER JOIN m_warehouse ON m_inout.m_warehouse_uu  = m_warehouse.m_warehouse_uu ' +
            ' INNER JOIN c_doctype ON m_inout.c_doctype_id = c_doctype.c_doctype_id ' +
            ' WHERE m_inout.wx_isdeleted = \'N\' ' + otherFilters + ' ORDER BY m_inout.updated ASC ';


        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new InoutViewModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);


        return apiInterface.Query(metaDataQuery);
    }

    FindInoutViewModelByMInoutUu(m_inout_uu) {

        const sqlTemplate = '  FROM m_inout ' +
            ' INNER JOIN c_bpartner ON m_inout.c_bpartner_uu = c_bpartner.c_bpartner_uu ' +
            ' INNER JOIN c_bpartner_location ON c_bpartner.c_bpartner_uu  = c_bpartner_location.c_bpartner_uu ' +
            ' INNER JOIN c_order ON c_order.c_order_uu = m_inout.c_order_uu ' +
            ' INNER JOIN m_shipper ON m_shipper.m_shipper_uu = m_inout.m_shipper_uu ' +
            ' INNER JOIN m_warehouse ON m_inout.m_warehouse_uu  = m_warehouse.m_warehouse_uu ' +
            ' INNER JOIN c_doctype ON m_inout.c_doctype_id = c_doctype.c_doctype_id ' +
            ' WHERE m_inout.m_inout_uu = \'' + m_inout_uu + '\' ';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new InoutViewModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }

    FindInoutViewModelByCOrderUu(c_order_uu) {

        const sqlTemplate = '  FROM m_inout ' +
            ' INNER JOIN c_bpartner ON m_inout.c_bpartner_uu = c_bpartner.c_bpartner_uu ' +
            ' INNER JOIN c_bpartner_location ON c_bpartner.c_bpartner_uu  = c_bpartner_location.c_bpartner_uu ' +
            ' INNER JOIN c_order ON c_order.c_order_uu = m_inout.c_order_uu ' +
            ' INNER JOIN m_shipper ON m_shipper.m_shipper_uu = m_inout.m_shipper_uu ' +
            ' INNER JOIN m_warehouse ON m_inout.m_warehouse_uu  = m_warehouse.m_warehouse_uu ' +
            ' INNER JOIN c_doctype ON m_inout.c_doctype_id = c_doctype.c_doctype_id ' +
            ' WHERE m_inout.c_order_uu = \'' + c_order_uu + '\' ';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new InoutViewModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }


    UpdateStatus(inoutViewModelTable) {
        let insertSuccess = 0;
        let sqlstatement = '';
        let thisObject = this;
        if (inoutViewModelTable.m_inout_wx_docstatus === 'DELETED') {
            sqlstatement = HelperService.SqlDeleteStatement('m_inout', 'm_inout_uu', inoutViewModelTable.m_inout_wx_m_inout_uu);

        } else {

            sqlstatement = HelperService.SqlUpdateStatusStatement('m_inout', 'm_inout_uu',
                inoutViewModelTable.m_inout_wx_m_inout_uu, inoutViewModelTable.m_inout_wx_docstatus);
        }

        return new Promise(function (resolve, reject) {

            apiInterface.ExecuteSqlStatement(sqlstatement)
                .then(function (rowChanged) {
                    insertSuccess += rowChanged;

                    resolve(insertSuccess);
                });



        }).then(function (insertSuccess) {

            return new Promise(function (resolve, reject) {

                if (insertSuccess) {
                    thisObject.RunProcessDocument(inoutViewModelTable).then(function (inserted) {
                        insertSuccess += inserted;

                        resolve(insertSuccess);
                    });
                } else {

                    resolve(insertSuccess);
                }


            });


        });

    }

    /**
     * Save Update harus merefer pada OrderViewModelTable
     * @param {*} inoutViewModel 
     * @param {*} datatableReference dataTableReferemce MInout datatable
     */
    async SaveUpdate(inoutViewModel, datatableReference) {
        const timeNow = new Date().getTime();
        let insertSuccess = 0;
        let isNewRecord = false;

        let thisObject = this;
        try {
            if (!inoutViewModel.m_inout.m_inout_uu) { //new record
                isNewRecord = true;
            }

            const sqlInsert = thisObject.GetInsertSqlStatement(inoutViewModel);

            insertSuccess += await apiInterface.ExecuteSqlStatement(sqlInsert);

            if (insertSuccess > 0) {

                if (datatableReference) {
                    let rowDataList = await thisObject.FindInoutViewModelByMInoutUu(inoutViewModel.m_inout.m_inout_uu);

                    let rowData = {};

                    if (rowDataList.length > 0) {
                        rowData = rowDataList[0];
                    }

                    if (!isNewRecord) {

                        datatableReference.row($('#bttn_row_edit' + rowData['m_inout_wx_m_inout_uu']).parents('tr')).data(rowData).draw();
                    } else {

                        datatableReference.row.add(rowData).draw();
                    }


                }
            }

        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.SaveUpdate.name, error);
            return insertSuccess;
        }
        return insertSuccess;

    }


    GetInsertSqlStatement(inoutViewModel) {
        const timeNow = new Date().getTime();

        let isNewRecord = false;
        let sqlInsert = '';
        let thisObject = this;
        try {
            if (!inoutViewModel.m_inout.m_inout_uu) { //new record
                isNewRecord = true;
                inoutViewModel.m_inout.m_inout_uu = HelperService.UUID();
                inoutViewModel.m_inout.ad_client_uu = localStorage.getItem('ad_client_uu');
                inoutViewModel.m_inout.ad_org_uu = localStorage.getItem('ad_org_uu');

                inoutViewModel.m_inout.createdby = localStorage.getItem('ad_user_uu');
                inoutViewModel.m_inout.updatedby = localStorage.getItem('ad_user_uu');

                inoutViewModel.m_inout.created = timeNow;
                inoutViewModel.m_inout.updated = timeNow;
            } else { //update record


                inoutViewModel.m_inout.updatedby = localStorage.getItem('ad_user_uu');

                inoutViewModel.m_inout.updated = timeNow;
            }

            // MM Shipment
            if (inoutViewModel.c_doctype_wx_c_doctype_id === 3000) {
                inoutViewModel.m_inout.movementtype = 'C-';
                inoutViewModel.m_inout.issotrx = 'Y';
            }

            // MM Receipt
            if (inoutViewModel.c_doctype_wx_c_doctype_id === 4000) {
                inoutViewModel.m_inout.movementtype = 'V+';
                inoutViewModel.m_inout.issotrx = 'N';
            }


            inoutViewModel.m_inout.c_order_uu = inoutViewModel.c_order.c_order_uu;
            inoutViewModel.m_inout.c_bpartner_uu = inoutViewModel.c_bpartner.c_bpartner_uu;
            inoutViewModel.m_inout.c_bpartner_location_uu = inoutViewModel.c_bpartner_location.c_bpartner_location_uu;
            inoutViewModel.m_inout.m_warehouse_uu = inoutViewModel.m_warehouse.m_warehouse_uu;
            inoutViewModel.m_inout.m_shipper_uu = inoutViewModel.m_shipper.m_shipper_uu;
            inoutViewModel.m_inout.c_doctype_id = inoutViewModel.c_doctype.c_doctype_id;
            inoutViewModel.m_inout.sync_client = null;
            inoutViewModel.m_inout.process_date = null;


            sqlInsert = HelperService.SqlInsertOrReplaceStatement('m_inout', inoutViewModel.m_inout);



        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.SaveUpdate.name, error);

        }
        return sqlInsert;




    }

    /**
     * Run Process Document sesuai dengan docstatus :
     *  
     * 1. Draft -> Tidak ada Action
     * docbasetype :
     * 
     * COMPLETED 
     * MM Shipment -> Action MaterialOutCompleted
     * 
     * MM Receive -> MaterialReceiptCompleted
     * 
     * REVERSED
     * MM Shipment -> MaterialOutReversed
     * 
     * MM Receive -> MaterialReceiveReversed
     * 
     * 
     * @param {*} inoutViewModelTable 
     */
    RunProcessDocument(inoutViewModelTable) {
        let thisObject = this;
        let inoutlineService = new InoutlineService();
        let costService = new CostService();
        let factacctService = new FactAcctService();
        let elementValueService = new ElementValueService();
        let materialTransactionService = new MaterialTransactionService();
        let insertSuccess = 0;

        try {
            if (inoutViewModelTable.m_inout_wx_docstatus === 'COMPLETED') {

                // SET MCost pada tiap-tiap product dengan m_costelement Last PO
                // dengan mendapatkan priceactual dari c_orderline, karena price menggunakan basedUom
                // product asset value =  priceactual * qtyOrdered 
                // dan create accounting transaction 
                if (inoutViewModelTable.c_doctype_wx_docbasetype === 'MMR') {

                    return new Promise(function (resolve, reject) {

                        inoutlineService.FindInoutlineViewModelByMInoutUu(inoutViewModelTable.m_inout_wx_m_inout_uu).then(function (inoutlineViewModelTable) {

                            resolve(inoutlineViewModelTable);
                        })


                    }).then(function (inoutlineViewModelTable) {

                        return new Promise(function (resolve, reject) {

                            let promiseArray = [];
                            _.forEach(inoutlineViewModelTable, function (row) {
                                let mCost = new MCost();
                                mCost.currentcostprice = row.c_orderline_wx_priceactual;
                                mCost.m_product_uu = row.m_inoutline_wx_m_product_uu;
                                mCost.m_costelement = elementValueService.CostElement().LastPO;

                                let productAssetValue = mCost.currentcostprice * row.m_inoutline_wx_movementqty;

                                promiseArray.push(costService.SaveUpdate(mCost));
                                promiseArray.push(thisObject.MaterialReceiptCompleted(productAssetValue, row.m_inoutline_wx_m_inoutline_uu));
                                promiseArray.push(materialTransactionService.MaterialReceiptCompleted(row))
                            });

                            Promise.all(promiseArray).then(function (resultArray) {

                                _.forEach(resultArray, function (inserted) {
                                    insertSuccess += inserted;

                                });
                                resolve(insertSuccess);

                            });

                        });


                    });

                } else if (inoutViewModelTable.c_doctype_wx_docbasetype === 'MMS') {


                    let dataflow = {};
                    return new Promise(function (resolve, reject) {
                        inoutlineService.FindInoutlineViewModelByMInoutUu(inoutViewModelTable.m_inout_wx_m_inout_uu)
                            .then(function (inoutlineViewModelTable) {
                                dataflow.inout_cmt_list = inoutlineViewModelTable;
                                resolve(dataflow);
                            });



                    }).then(function (dataflow) {
                        return new Promise(function (resolve, reject) {
                            let promiseArray = [];
                            _.forEach(dataflow.inout_cmt_list, function (row) {
                                promiseArray.push(costService.FindByMProductUuAndCostElement(row.m_product_wx_m_product_uu, elementValueService.CostElement().LastPO));

                            });


                            Promise.all(promiseArray).then(function (arrayCostCMTList) {
                                dataflow.array_cost_cmt_list = arrayCostCMTList;
                                resolve(dataflow);
                            });


                        });


                    }).then(function (dataflow) {

                        return new Promise(function (resolve, reject) {
                            let promiseArray = [];
                            let i = 0;
                            for (i = 0; i < dataflow.inout_cmt_list.length; i++) {

                                let costViewModelTableList = dataflow.array_cost_cmt_list[i];
                                let inoutCMT = dataflow.inout_cmt_list[i];
                                // cek jika product tidak memiliki cost price, maka set currentcostprice
                                let currentcostprice = 0;
                                if (costViewModelTableList.length) {
                                    let costViewModelTable = costViewModelTableList[0];
                                    currentcostprice = costViewModelTable.m_cost_wx_currentcostprice;
                                }


                                let productAssetValue = currentcostprice * inoutCMT.m_inoutline_wx_movementqty;
                                promiseArray.push(thisObject.MaterialOutCompleted(productAssetValue, inoutCMT));

                            }

                            Promise.all(promiseArray).then(function (resultArray) {

                                _.forEach(resultArray, function (inserted) {
                                    insertSuccess += inserted;

                                });
                                resolve(insertSuccess);
                            });

                        });




                    });



                } else {
                    return new Promise(function (resolve, reject) {

                        resolve(1);
                    });

                }

                /**
                 * 
                 *  DOC STATUS REVERSED *  *         *            */


            } else if (inoutViewModelTable.m_inout_wx_docstatus === 'REVERSED') {

                if (inoutViewModelTable.c_doctype_wx_docbasetype === 'MMR') {

                    return new Promise(function (resolve, reject) {
                        inoutlineService.FindInoutlineViewModelByMInoutUu(inoutViewModelTable.m_inout_wx_m_inout_uu)
                            .then(function (inoutlineViewModelTable) {
                                resolve(inoutlineViewModelTable);
                            })


                    }).then(function (inoutlineViewModelTable) {

                        let promiseArray = [];

                        return new Promise(function (resolve, reject) {
                            _.forEach(inoutlineViewModelTable, function (row) {

                                promiseArray.push(thisObject.MaterialReceiptReversed(row.m_inoutline_wx_m_inoutline_uu));
                                promiseArray.push(materialTransactionService.MaterialReceiptReversed(row.m_inoutline_wx_m_inoutline_uu));
                            });

                            Promise.all(promiseArray).then(function (resultArray) {
                                _.forEach(resultArray, function (inserted) {
                                    insertSuccess += inserted;

                                });
                                resolve(insertSuccess);

                            });


                        });


                    });


                } else if (inoutViewModelTable.c_doctype_wx_docbasetype === 'MMS') {

                    return new Promise(function (resolve, reject) {
                        inoutlineService.FindInoutlineViewModelByMInoutUu(inoutViewModelTable.m_inout_wx_m_inout_uu)
                            .then(function (inoutlineViewModelTable) {
                                resolve(inoutlineViewModelTable);
                            })


                    }).then(function (inoutlineViewModelTable) {

                        let promiseArray = [];
                        return new Promise(function (resolve, reject) {
                            _.forEach(inoutlineViewModelTable, function (row) {

                                promiseArray.push(thisObject.MaterialOutReversed(row.m_inoutline_wx_m_inoutline_uu));
                                // promiseArray.push(materialTransactionService.MaterialReceiptReversed(row.m_inoutline_wx_m_inoutline_uu));
                            });

                            Promise.all(promiseArray).then(function (resultArray) {
                                _.forEach(resultArray, function (inserted) {
                                    insertSuccess += inserted;

                                });
                                resolve(insertSuccess);

                            });

                        });

                    });

                } else {

                    return new Promise(function (resolve, reject) {
                        resolve(1);

                    });

                }
            } else {
                //STATUS document bisa draft atau closed
                return new Promise(function (resolve, reject) {
                    resolve(insertSuccess);

                });


            }
        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.RunProcessDocument.name, error);
        }



    }

    /**
     * Create FactAcct berdasarkan material completed
     * Create Material Transaction material out completed
     */
    MaterialOutCompleted(productAssetValue, inoutlineCMT) {
        let factacctService = new FactAcctService()
        let materialTransactionService = new MaterialTransactionService();
        let thisObject = this;
        let insertSuccess = 0;

        let elementValueService = new ElementValueService();

        try {

            // jika barang tersebut adalah ITEM, maka buat transaction product asset dan material transaction
            if (inoutlineCMT.m_product_wx_producttype === 'I') {
                return new Promise(function (resolve, reject) {
                    // Create FactAcct berdasarkan material completed



                    let sqlInsertDr = factacctService.sqlInsertFactAcctStatement(elementValueService.ElementValueList().CoGSProduct,
                        'm_inoutline', inoutlineCMT.m_inoutline_wx_m_inoutline_uu, productAssetValue, null);

                    let sqlInsertCr = factacctService.sqlInsertFactAcctStatement(elementValueService.ElementValueList().ProductAsset,
                        'm_inoutline', inoutlineCMT.m_inoutline_wx_m_inoutline_uu, null, productAssetValue);

                    apiInterface.ExecuteBulkSqlStatement([sqlInsertDr, sqlInsertCr]).then(function (inserted) {

                        insertSuccess += inserted;
                        resolve(insertSuccess);
                    });


                }).then(function () {

                    return new Promise(function (resolve, reject) {

                        materialTransactionService.sqlInsertMTransaction(elementValueService.MovementType().CustomerOut,
                            inoutlineCMT.m_inoutline_wx_m_locator_uu, 'm_inoutline',
                            inoutlineCMT.m_inoutline_wx_m_inoutline_uu,
                            inoutlineCMT.m_inoutline_wx_m_product_uu,
                            inoutlineCMT.m_inoutline_wx_movementqty, true).then(function (inserted) {

                                insertSuccess += inserted;
                                resolve(insertSuccess);

                            });

                    });



                })

            }else{

                return new Promise(function(resolve, reject){

                    resolve(1);

                });


            }


        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.MaterialOutCompleted.name, error);
            return new Promise(function (resolve, reject) { resolve(insertSuccess); });
        }



    }

    /**
    * Dibalik dari MaterialReceiptCompleted
    */
    MaterialOutReversed(m_inoutline_uu) {
        let elementValueService = new ElementValueService();
        let factacctService = new FactAcctService();
        let materialTransactionService = new MaterialTransactionService();
        let insertSuccess = 0;


        return new Promise(function (resolve, reject) {

            //reverseCoGS
            let cogsStatement = factacctService.sqlInsertReverseFactAcctStatement('m_inoutline', m_inoutline_uu, elementValueService.ElementValueList().CoGSProduct.value);

            //reverseProductAsset
            let producAssetStatement = factacctService.sqlInsertReverseFactAcctStatement('m_inoutline', m_inoutline_uu, elementValueService.ElementValueList().ProductAsset.value);

            apiInterface.ExecuteBulkSqlStatement([cogsStatement, producAssetStatement]).then(function (inserted) {
                insertSuccess += inserted;
                resolve(insertSuccess);
            });


        }).then(function () {

            return new Promise(function (resolve, reject) {

                materialTransactionService.sqlInsertReverseMTransaction(elementValueService.MovementType().CustomerOutReverse,
                    elementValueService.MovementType().CustomerOut, 'm_inoutline', m_inoutline_uu).then(function (inserted) {

                        insertSuccess += inserted;
                        resolve(insertSuccess);
                    });

            });


        });



    }

     /**
     * Create FactAcct berdasarkan material completed
     */
    MaterialReceiptCompleted(productAssetValue, m_inoutline_uu) {

        let thisObject = this;
        let factacctService = new FactAcctService();
        //DEBIT 
        let elementValueService = new ElementValueService();
        let sqlInsertDr = factacctService.sqlInsertFactAcctStatement(elementValueService.ElementValueList().ProductAsset,
            'm_inoutline', m_inoutline_uu, productAssetValue, null);

        let sqlInsertCr = factacctService.sqlInsertFactAcctStatement(elementValueService.ElementValueList().NotInvoicedReceipt,
            'm_inoutline', m_inoutline_uu, null, productAssetValue);


        return apiInterface.ExecuteBulkSqlStatement([sqlInsertDr, sqlInsertCr]);


    }

    /**
     * Dibalik dari MaterialReceiptCompleted
     */
    MaterialReceiptReversed(m_inoutline_uu) {
        let elementValueService = new ElementValueService();
        let factacctService = new FactAcctService();

        let sqlInsertNotInvoicedReceipt = factacctService.sqlInsertReverseFactAcctStatement('m_inoutline', m_inoutline_uu, elementValueService.ElementValueList().NotInvoicedReceipt.value);

        let sqlInsertProductAsset = factacctService.sqlInsertReverseFactAcctStatement('m_inoutline', m_inoutline_uu, elementValueService.ElementValueList().ProductAsset.value);

        return apiInterface.ExecuteBulkSqlStatement([sqlInsertNotInvoicedReceipt, sqlInsertProductAsset]);
    }




}