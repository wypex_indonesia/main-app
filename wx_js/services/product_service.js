
class ProductService {

    constructor() { }

    FindProducttypeViewModel() {
        let metaDataQuery;
        try {
            const sqlTemplate = '  FROM wypex_producttype order by wypex_producttype.name;';

            const listMetaColumn = HelperService.ConvertViewModelToMetaColumn({ wypex_producttype: new WypexProducttype() });

            metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);


        } catch (error) {
            apiInterface.Log(this.constructor.name, this.FindProducttypeViewModel.name, error);
        }

        return apiInterface.Query(metaDataQuery);

    }

    /**
	 * Find All warehouse dengan bentuk warehouseviewmodel
	 * @param {*} metaDataResponseTable 
	 */
    FindAll(metaDataResponseTable) {

        let metaDataQuery;
        try {

            let searchFilter = '';
            if (metaDataResponseTable.search !== '') {
                searchFilter += ' AND ' + metaDataResponseTable.searchField + '%' + metaDataResponseTable.search + '%';
            }

            let limit  = '';
            if ((metaDataResponseTable.page !== null) && (metaDataResponseTable.perpage !== null)){
    
                limit = ' LIMIT ' + metaDataResponseTable.page + ',' + metaDataResponseTable.perpage;
    
            }

            let otherFilters = '';
            _.forEach(metaDataResponseTable.otherFilters, function (filter) {

                otherFilters += filter;

            });

            const sqlTemplate = '  FROM m_product ' +
                ' INNER JOIN m_product_category ON m_product_category.m_product_category_uu = m_product.m_product_category_uu ' +
                ' INNER JOIN c_uom ON c_uom.c_uom_uu = m_product.c_uom_uu ' +
                ' INNER JOIN wypex_producttype ON wypex_producttype.producttype = m_product.producttype ' +
                ' WHERE m_product.wx_isdeleted = \'N\' AND m_product.ad_org_uu = \'' + metaDataResponseTable.ad_org_uu + '\'' + searchFilter + otherFilters + ' ORDER BY m_product.updated DESC ' + limit;


            const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new ProductViewModel());

            metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);

        } catch (error) {
            apiInterface.Log(this.constructor.name, this.FindAll.name, error);
            return new Promise(function (resolve, reject) {
                resolve([]);
            });
        }

        return apiInterface.Query(metaDataQuery);
    }

    FindProductViewModelByMProductUu(m_product_uu) {
        let metaDataQuery;
        try {
            const sqlTemplate = '  FROM m_product ' +
                ' INNER JOIN m_product_category ON m_product_category.m_product_category_uu = m_product.m_product_category_uu ' +
                ' INNER JOIN c_uom ON c_uom.c_uom_uu = m_product.c_uom_uu ' +
                ' INNER JOIN wypex_producttype ON wypex_producttype.producttype = m_product.producttype ' +
                ' WHERE m_product.m_product_uu = \'' + m_product_uu + '\'';


            const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new ProductViewModel());

            metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);

        } catch (error) {
            apiInterface.Log(this.constructor.name, this.FindProductViewModelByMProductUu.name, error);
            return new Promise(function (resolve, reject) {
                resolve([]);
            });
        }


        return apiInterface.Query(metaDataQuery);
    }

    /**
	 * 
	
	 */
    async SaveUpdate(productCM, datatableReference) {
        const timeNow = new Date().getTime();
        let insertSuccess = 0;
        let isNewRecord = false;
        let thisObject = this;

        try {


            if (!productCM.m_product.m_product_uu) { //new record
                isNewRecord = true;
                productCM.m_product.m_product_uu = HelperService.UUID();
                productCM.m_product.ad_client_uu = localStorage.getItem('ad_client_uu');
                productCM.m_product.ad_org_uu = localStorage.getItem('ad_org_uu');

                productCM.m_product.createdby = localStorage.getItem('ad_user_uu');
                productCM.m_product.updatedby = localStorage.getItem('ad_user_uu');

                productCM.m_product.created = timeNow;
                productCM.m_product.updated = timeNow;
            } else { //update record


                productCM.m_product.updatedby = localStorage.getItem('ad_user_uu');

                productCM.m_product.updated = timeNow;
            }

            productCM.m_product.m_product_uu = productCM.m_product.m_product_uu;
            productCM.m_product.m_product_category_uu = productCM.m_product_category.m_product_category_uu;
            productCM.m_product.producttype = productCM.wypex_producttype.producttype;

            // E Biaya, S jasa
            if ((productCM.m_product.producttype === 'E') || (productCM.m_product.producttype === 'S')) {
                productCM.m_product.ispurchased = 'N';
                productCM.m_product.isbom = 'N';
                productCM.m_product.isstocked = 'N'
                if (productCM.m_product.producttype === 'E') {
                    productCM.m_product.issold = 'N';
                }
            }



            productCM.m_product.c_uom_uu = productCM.c_uom.c_uom_uu;
            productCM.m_product.sync_client = null;
            productCM.m_product.process_date = null;

            const sqlInsertProduct = HelperService.SqlInsertOrReplaceStatement('m_product', productCM.m_product);
            insertSuccess += await apiInterface.ExecuteSqlStatement(sqlInsertProduct);

            if (insertSuccess > 0) {

                if (datatableReference) {
                    let rowDataList = await thisObject.FindProductViewModelByMProductUu(productCM.m_product.m_product_uu);
                    let rowData = {};

                    if (rowDataList.length > 0) {
                        rowData = rowDataList[0];
                    }

                    if (!isNewRecord) {

                        datatableReference.row($('#bttn_row_edit' + rowData['m_product_wx_m_product_uu']).parents('tr')).data(rowData).draw();
                    } else {

                        datatableReference.row.add(rowData).draw();
                    }


                }



            }




        } catch (error) {
            apiInterface.Log(this.constructor.name, this.SaveUpdate.name, error);
            return insertSuccess;
        }


        return insertSuccess;



    }




}