class PrintPreviewService {

    constructor() { }

    GetSizeDialog(idPreviewContent) {
        let witdhObjectPreview = 0;
        if ($('#' + idPreviewContent)) {
            witdhObjectPreview = $('#' + idPreviewContent)[0].clientWidth;
            witdhObjectPreview = witdhObjectPreview * 0.9; //potong 10%
        }
        let size = '';
        if (witdhObjectPreview) {
            let height = witdhObjectPreview * 0.5 + witdhObjectPreview;
            size = 'width="' + witdhObjectPreview + '" height = "' + height + '"';
        }

        return size;
    }

    async   HeaderPrintPreview(idPreviewContent, ad_org_uu, c_bpartner_uu, documentno, titlePrint, shipOrReceiptFrom, docPdf) {

        let thisObject = this;
        let htmlTemplate;

        try {

            let idCanvas = 'barcodeCanvas';
            let htmlWaiting = `<canvas hidden id="` + idCanvas + `"></canvas>
            <div class="kt-spinner kt-spinner--md kt-spinner--danger"></div>`;


            $('#' + idPreviewContent).html(htmlWaiting);
            let organizationService = new OrganizationService();
            let businesspartnerService = new BusinessPartnerService();

            let organizationCMTList = await organizationService.FindOrganizationComponentModelByAdOrgUu(ad_org_uu);
            let organizationCMT = organizationCMTList[0];

            let businesspartnerCMTList = await businesspartnerService.FindBusinesspartnerViewModelByCBpartnerUu(c_bpartner_uu);
            let businesspartnerCMT = businesspartnerCMTList[0];
            // The return value is the canvas element
            let canvas = bwipjs.toCanvas(idCanvas, {
                bcid: 'code128',       // Barcode type
                text: documentno,    // Text to encode
                scale: 3,               // 3x scaling factor
                height: 10,              // Bar height, in millimeters
                includetext: true,            // Show human-readable text
                textxalign: 'center',        // Always good to set this
            });




            let barcodeString = canvas.toDataURL(
                'image/png');
            const optionsSuratJalan = { align: 'left' };
            docPdf.setFontSize(25);

            docPdf.text(titlePrint, 20, 20, optionsSuratJalan);
            docPdf.setFontSize(11);
            docPdf.text('No Document', 150, 20);
            docPdf.addImage(barcodeString, 'PNG', 120, 22, 80, 20);
            // docPdf.text('Barcode', 270, 20);
            docPdf.setFontSize(12);
            docPdf.text(thisObject.SetValue(organizationCMT.ad_org_wx_name), 20, 50);
            docPdf.text(thisObject.SetValue(organizationCMT.c_location_wx_address1), 20, 54);
            docPdf.text(thisObject.SetValue(organizationCMT.c_location_wx_city) + ', ' + thisObject.SetValue(organizationCMT.c_location_wx_regionname), 20, 58);
            docPdf.text(thisObject.SetValue(organizationCMT.ad_orginfo_wx_phone), 20, 62);

            docPdf.text(thisObject.SetValue(shipOrReceiptFrom), 136, 50);
            docPdf.text(thisObject.SetValue(businesspartnerCMT.c_bpartner_wx_name), 136, 54);
            docPdf.text(thisObject.SetValue(businesspartnerCMT.c_location_wx_address1), 136, 58);
            docPdf.text(thisObject.SetValue(businesspartnerCMT.c_location_wx_city) + ', ' + thisObject.SetValue(businesspartnerCMT.c_location_wx_regionname), 136, 62);
            docPdf.text(thisObject.SetValue(businesspartnerCMT.c_bpartner_location_wx_phone), 136, 66);


        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.HeaderPrintPreview.name, error);
        }



    }

    async PrintPreviewOrder(idPreviewContent, orderCMT) {
        let thisObject = this;
        let htmlTemplate;

        try {


            let orderlineService = new OrderlineService();
            let listOrderlineCMT = await orderlineService.FindOrderlineViewModelByCOrderUu(orderCMT.c_order_wx_c_order_uu);

            let titlePrint = 'PurchaseOrder';
            let shipOrReceiptFrom = 'Kirim ke';
            //  shipping
            if (orderCMT.c_doctype_wx_docbasetype !== 'PO') {
                titlePrint = 'Sales Order';
                shipOrReceiptFrom = 'Terima dari';
            }

            const docPdf = new jsPDF();

            await thisObject.HeaderPrintPreview(idPreviewContent, orderCMT.c_order_wx_ad_org_uu,
                orderCMT.c_order_wx_c_bpartner_uu, orderCMT.c_order_wx_documentno, titlePrint, shipOrReceiptFrom, docPdf);

            // shipping
            if (orderCMT.c_doctype_wx_docbasetype === 'SO') {
                docPdf.autoTable({
                    startY: 90,
                    head: [['Order Date', 'Order Number', 'PO Number']],
                    body: [
                        [HelperService.ConvertTimestampToLocateDateString(orderCMT.c_order_wx_dateordered), orderCMT.c_order_wx_documentno, orderCMT.c_order_wx_poreference]

                    ]
                });
            } else {
                docPdf.autoTable({
                    startY: 90,
                    head: [['Order Date', 'Order Number']],
                    body: [
                        [HelperService.ConvertTimestampToLocateDateString(orderCMT.c_order_wx_dateordered), orderCMT.c_order_wx_documentno]

                    ]
                });
            }



            const listOrderline = [];
            _.forEach(listOrderlineCMT, (orderlineCMT) => {
                let linetaxamt = Intl.NumberFormat().format(orderlineCMT.c_orderline_wx_linetaxamt);
                let linenetamt = Intl.NumberFormat().format(orderlineCMT.c_orderline_wx_linenetamt);
                const orderlineRow = [orderlineCMT.m_product_wx_sku, orderlineCMT.m_product_wx_name, orderlineCMT.c_uom_wx_name, orderlineCMT.c_orderline_wx_qtyentered, linetaxamt, linenetamt];
                listOrderline.push(orderlineRow);

            });
            let totalTaxlines = new Intl.NumberFormat().format(orderCMT.c_order_wx_totaltaxlines);
            let totalLines = new Intl.NumberFormat().format(orderCMT.c_order_wx_totallines);
            let footerTotal = ['', '', '', 'Total : ', totalTaxlines, totalLines];
            listOrderline.push(footerTotal);

            let grandTotal = new Intl.NumberFormat().format(orderCMT.c_order_wx_grandtotal);
            let footerGrandTotal = ['', '', '', 'Grand Total', { content: grandTotal, colSpan: 2, styles: { fontStyle: 'bold', halign: 'center', fontSize: 12 } }];
            listOrderline.push(footerGrandTotal);

            docPdf.autoTable({
                startY: 108,
                head: [['SKU', 'Item', 'Satuan', 'Jumlah', 'Tax', 'Net']],
                body: listOrderline
            });

            // const optionsNote = { align: 'right' };
            // catatan
            // docPdf.text('Please contact Customer Service at Phone with any questions', 100, 234, optionsNote);


            const pdfDataString = docPdf.output('datauristring');
            const arrayPdfDataString = pdfDataString.split(';');
            let pdfString = arrayPdfDataString[0] + ';' + arrayPdfDataString[2]; // kalau mau langsung diprint kirim ke host form

            htmlTemplate = '<object ' + thisObject.GetSizeDialog(idPreviewContent) + '  data="' + arrayPdfDataString[0] + ';' + arrayPdfDataString[2] + '" type="application/pdf" ></object>';


            $('#' + idPreviewContent).html(htmlTemplate);



        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.PrintPreviewOrder.name, error);
        }


    }


    async PrintPreviewInout(idPreviewContent, inoutCMT) {
        let thisObject = this;
        let htmlTemplate;

        try {

            let inoutlineService = new InoutlineService();

            let listInoutlineCMT = await inoutlineService.FindInoutlineViewModelByMInoutUu(inoutCMT.m_inout_wx_m_inout_uu);

            let titlePrint = 'Surat Jalan';
            let shipOrReceiptFrom = 'Kirim ke';
            //  shipping
            if (inoutCMT.m_inout_wx_c_doctype_id !== 3000) {
                titlePrint = 'Material Receipt';
                shipOrReceiptFrom = 'Terima dari';
            }

            const docPdf = new jsPDF();
            await thisObject.HeaderPrintPreview(idPreviewContent,
                inoutCMT.m_inout_wx_ad_org_uu,
                inoutCMT.m_inout_wx_c_bpartner_uu,
                inoutCMT.m_inout_wx_documentno, titlePrint, shipOrReceiptFrom, docPdf);

            // shipping
            if (inoutCMT.m_inout_wx_c_doctype_id === 3000) {
                docPdf.autoTable({
                    startY: 90,
                    head: [['Order Date', 'Order Number', 'PO Number']],
                    body: [
                        [HelperService.ConvertTimestampToLocateDateString(inoutCMT.c_order_wx_dateordered), inoutCMT.c_order_wx_documentno, inoutCMT.m_inout_wx_poreference]

                    ]
                });
            } else {
                docPdf.autoTable({
                    startY: 90,
                    head: [['Order Date', 'Order Number']],
                    body: [
                        [HelperService.ConvertTimestampToLocateDateString(inoutCMT.c_order_wx_dateordered), inoutCMT.c_order_wx_documentno]

                    ]
                });
            }


            const listInoutline = [];
            _.forEach(listInoutlineCMT, (inoutlineCMT) => {
                const inoutlineRow = [inoutlineCMT.m_product_wx_sku, inoutlineCMT.m_product_wx_name, inoutlineCMT.c_uom_wx_name, inoutlineCMT.m_inoutline_wx_qtyentered];
                listInoutline.push(inoutlineRow);

            });

            docPdf.autoTable({
                startY: 108,
                head: [['SKU', 'Item', 'Satuan', 'Jumlah']],
                body: listInoutline
            });

            // const optionsNote = { align: 'right' };
            // catatan
            // docPdf.text('Please contact Customer Service at Phone with any questions', 100, 234, optionsNote);


            const pdfDataString = docPdf.output('datauristring');
            const arrayPdfDataString = pdfDataString.split(';');
            let pdfString = arrayPdfDataString[0] + ';' + arrayPdfDataString[2];

            htmlTemplate = '<object ' + thisObject.GetSizeDialog(idPreviewContent) + '  data="' + arrayPdfDataString[0] + ';' + arrayPdfDataString[2] + '" type="application/pdf" ></object>';


            $('#' + idPreviewContent).html(htmlTemplate);




        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.PrintPreviewInout.name, error);
        }


    }



    async PrintPreviewInvoice(idPreviewContent, invoiceCMT) {

        let thisObject = this;
        let htmlTemplate;

        try {

            let invoicelineService = new InvoicelineService();

            let listInvoicelineCMT = await invoicelineService.FindInvoicelineComponentModelByCInvoiceUu(invoiceCMT.c_invoice_wx_c_invoice_uu);



            let titlePrint = 'Invoice';
            let shipOrReceiptFrom = 'To';
            //  shipping
            if (invoiceCMT.c_doctype_wx_docbasetype !== 'ARI') {
                titlePrint = 'Invoice Vendor';
                shipOrReceiptFrom = 'Terima dari';
            }

            const docPdf = new jsPDF();
            await thisObject.HeaderPrintPreview(idPreviewContent,
                invoiceCMT.c_invoice_wx_ad_org_uu,
                invoiceCMT.c_invoice_wx_c_bpartner_uu,
                invoiceCMT.c_invoice_wx_documentno, titlePrint, shipOrReceiptFrom, docPdf);

            // shipping
            if (invoiceCMT.c_doctype_wx_docbasetype === 'ARI') {
                docPdf.autoTable({
                    startY: 90,
                    head: [['Order Date', 'Invoice Number', 'PO Number']],
                    body: [
                        [HelperService.ConvertTimestampToLocateDateString(invoiceCMT.c_order_wx_dateordered), invoiceCMT.c_invoice_wx_documentno, invoiceCMT.c_order_wx_poreference]

                    ]
                });
            } else {
                docPdf.autoTable({
                    startY: 90,
                    head: [['Order Date', 'Invoice Number']],
                    body: [
                        [HelperService.ConvertTimestampToLocateDateString(invoiceCMT.c_order_wx_dateordered), invoiceCMT.c_invoice_wx_documentno]

                    ]
                });
            }


            const listInvoiceline = [];
            _.forEach(listInvoicelineCMT, (invoicelineComponentModelTable) => {
                let taxamt = Intl.NumberFormat().format(invoicelineComponentModelTable.c_invoiceline_wx_taxamt);
                let linenetamt = Intl.NumberFormat().format(invoicelineComponentModelTable.c_invoiceline_wx_linenetamt);
                const invoicelineRow = [invoicelineComponentModelTable.c_invoiceline_wx_line, invoicelineComponentModelTable.m_product_wx_sku, invoicelineComponentModelTable.m_product_wx_name, invoicelineComponentModelTable.c_uom_wx_name, invoicelineComponentModelTable.c_invoiceline_wx_qtyentered, taxamt, linenetamt];
                listInvoiceline.push(invoicelineRow);

            });
            let totalTaxlines = new Intl.NumberFormat().format(invoiceCMT.c_invoice_wx_totaltaxlines);
            let totalLines = new Intl.NumberFormat().format(invoiceCMT.c_invoice_wx_totallines);
            let footerTotal = ['', '', '', '', 'Total : ', totalTaxlines, totalLines];
            listInvoiceline.push(footerTotal);

            let grandTotal = new Intl.NumberFormat().format(invoiceCMT.c_invoice_wx_grandtotal);
            let footerGrandTotal = ['', '', '', '', 'Grand Total', { content: grandTotal, colSpan: 2, styles: { fontStyle: 'bold', halign: 'center', fontSize: 12 } }];
            listInvoiceline.push(footerGrandTotal);

            docPdf.autoTable({
                startY: 108,
                head: [['No', 'SKU', 'Item', 'Satuan', 'Jumlah', 'Tax', 'Net']],
                body: listInvoiceline
            });

            const pdfDataString = docPdf.output('datauristring');
            const arrayPdfDataString = pdfDataString.split(';');
            let pdfString = arrayPdfDataString[0] + ';' + arrayPdfDataString[2];

            htmlTemplate = '<object ' + thisObject.GetSizeDialog(idPreviewContent) + '  data="' + arrayPdfDataString[0] + ';' + arrayPdfDataString[2] + '" type="application/pdf" ></object>';


            $('#' + idPreviewContent).html(htmlTemplate);



        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.PrintPreviewInvoice.name, error);
        }


    }


    PrintBarcodeLabel(idPreviewContent, productBarcodelineCMTList) {
        let thisObject = this;


        let htmlWaiting = ` <div class="kt-spinner kt-spinner--md kt-spinner--danger"></div>`;

        $('#' + idPreviewContent).html(htmlWaiting);

        let printBarcodelineCMTList = [];
        _.forEach(productBarcodelineCMTList, function (productBarcodelineCMT, index) {

            let idCanvas = 'barcodeCanvas_' + productBarcodelineCMT.wxt_print_productbarcodeline_wx_wxt_print_productbarcodeline_uu;
            let canvasHtml = `<canvas hidden id="` + idCanvas + `"></canvas>`;
            $('#' + idPreviewContent).append(canvasHtml);
            let skuOrUpc = '';
            if (productBarcodelineCMT.wxt_print_productbarcodeline_wx_isskuupc === 'sku') {
                skuOrUpc = productBarcodelineCMT.m_product_wx_sku;
            } else {

                skuOrUpc = productBarcodelineCMT.m_product_wx_upc;
            }



            let canvas = bwipjs.toCanvas(idCanvas, {
                bcid: 'qrcode',       // Barcode type
                text: skuOrUpc,    // Text to encode
                scale: 1,              // 3x scaling factor                         
                includetext: true,            // Show human-readable text
                textxalign: 'center',        // Always good to set this
            });

            let barcodeString = canvas.toDataURL(
                'image/png');

            productBarcodelineCMT.barcode_string = barcodeString;
            printBarcodelineCMTList.push(productBarcodelineCMT);

        });



        // hanya uuntuk paper no. 107, 18 x 50 mm label adhesive



        let firstGapY = 5; //mm
        let gapY = 2;
        let betweenGap = 5;
        let gapX = 2;
        let fistQrX = 5;
        let fistTextX = 10;
        let firstTextSkuY = 10;
        let firstTextProdY = 14;
        let firstQrY = 2;
        let widthLabel = 50;
        let heightLabel = 18; //ukuran
        let widthQr = 15;
        let heightQr = 15;


        var docPdf = new jsPDF({ format: [165, 210], unit: 'mm' });
        docPdf.setFontSize(8);

        let arrayBarcode = [];

        let rowsBarcode = [];


        // initial data
        /*  for (i = 1; i <= 30; i++) {
  
              if (i % 3 === 0) {
                  arrayBarcode.push(barcodeString);
                  rowsBarcode.push(arrayBarcode.slice(1));//clone array , agar tidak merefer object yang sama
                  arrayBarcode = [];
              } else {
                  arrayBarcode.push(barcodeString);
              }
  
          }*/
        let x = 0;
        let y = 0;
        let i = 0;

        _.forEach(printBarcodelineCMTList, function (productBarcodelineCMT) {
            let count_print = numeral(productBarcodelineCMT.wxt_print_productbarcodeline_wx_count_print).value();

            let skuOrUpc = '';
            if (productBarcodelineCMT.wxt_print_productbarcodeline_wx_isskuupc === 'sku') {
                skuOrUpc = productBarcodelineCMT.m_product_wx_sku;
            } else {

                skuOrUpc = productBarcodelineCMT.m_product_wx_upc;
            }

            let nameProduct = productBarcodelineCMT.m_product_wx_name;
            let barcodeString = productBarcodelineCMT.barcode_string;
            for (i = 0; i < count_print; i++) {
                if ((y !== 0) && (y === 10)) {
                    docPdf.addPage([165, 210]);
                    y = 0;
                }
                if (x === 3) {
                    x = 0;
                }


                let space = (heightLabel + gapY) * y;

                if (x === 0) {
                    // label x0y0
                    docPdf.text(skuOrUpc, gapX + widthQr + fistTextX, firstGapY + firstTextSkuY + space);
                    docPdf.text(nameProduct, gapX + widthQr + fistTextX, firstGapY + firstTextProdY + space);
                    docPdf.addImage(barcodeString, 'PNG', gapX + fistQrX, firstGapY + firstQrY + space, widthQr, heightQr);
                }

                if (x === 1) {
                    //label x1y0
                    docPdf.text(skuOrUpc, gapX + widthLabel + betweenGap + widthQr + fistTextX, firstGapY + firstTextSkuY + space);
                    docPdf.text(nameProduct, gapX + widthLabel + betweenGap + widthQr + fistTextX, firstGapY + firstTextProdY + space);
                    docPdf.addImage(barcodeString, 'PNG', gapX + widthLabel + betweenGap + fistQrX, firstGapY + firstQrY + space, widthQr, heightQr);

                }

                if (x === 2) {

                    //label x2y0
                    docPdf.text(skuOrUpc, gapX + widthLabel + betweenGap + widthLabel + betweenGap + widthQr + fistTextX, firstGapY + firstTextSkuY + space);
                    docPdf.text(nameProduct, gapX + widthLabel + betweenGap + widthLabel + betweenGap + widthQr + fistTextX, firstGapY + firstTextProdY + space);
                    docPdf.addImage(barcodeString, 'PNG', gapX + widthLabel + betweenGap + widthLabel + betweenGap + fistQrX, firstGapY + firstQrY + space, widthQr, heightQr);
                    y++;
                }
                x++;

            }


        });







        const pdfDataString = docPdf.output('datauristring');
        const arrayPdfDataString = pdfDataString.split(';');
        let pdfString = arrayPdfDataString[0] + ';' + arrayPdfDataString[2];

        let htmlTemplate = '<object ' + thisObject.GetSizeDialog(idPreviewContent) + '   data="' + arrayPdfDataString[0] + ';' + arrayPdfDataString[2] + '" type="application/pdf" ></object>';

        let base64StringWithBase64 = arrayPdfDataString[2]; // base64,xxxxxxxx
        let base64StringArray = base64StringWithBase64.split(',');


        $('#' + idPreviewContent).html(htmlTemplate);

    }

    HeaderReportFinancial(docPdf, stringArray) {


        let listHeaderRow = [];
        _.forEach(stringArray, function (stringText) {

            //  let xOffset = (docPdf.internal.pageSize.width / 2) - (docPdf.getStringUnitWidth(text) * docPdf.internal.getFontSize() / 2); 
            listHeaderRow.push([
                { content: stringText, styles: { halign: 'center', fillColor: null, linewidth: 0 } }
            ]);

        })
        docPdf.autoTable({
            startY: 10,
            body: listHeaderRow
        });


    }


    /**
     * Membuat hierarchy akun, 
     * untuk parent index 0, 1, bold dan tidak ada amount ditampilkan
     * untuk index lebih dari 1, jika 0 tidak ditampilkan
     * @param {*} objectTree object parent dengan children semisal, akun Aktiva dan childrennya
     */
    CreateHierarchyRowAkun(objectTree, rowsPdf, i) {
        let thisObject = this;

        if (i === 0) {
            let amount = new Intl.NumberFormat().format(objectTree.amount);
            let space = '';
            rowsPdf.push([{ content: space + objectTree.value + ' ' + objectTree.name, styles: { fontStyle: 'bold', halign: 'left' }, colspan: 2 }]);
            ++i;
        }

        let childrens = objectTree.children;
        _.forEach(childrens, function (akun) {
            let amount = new Intl.NumberFormat().format(akun.amount);
            let space = '';
            for (let j = 0; j < i; j++) {
                space += ' ';

            }


            if (i === 1) {
                rowsPdf.push([{ content: space + akun.value + ' ' + akun.name, styles: { fontStyle: 'bold', halign: 'left' }, colspan: 2 }]);
            } else {
                if (akun.amount !== 0) {
                    rowsPdf.push([{ content: space + akun.value + ' ' + akun.name, colspan: 2 }, amount]);
                }

            }

            if (akun.children.length) {
                thisObject.CreateHierarchyRowAkun(akun, rowsPdf, ++i);
            }


        });

        return rowsPdf;
    }


    /**
     * 
     * @param {*} listAkun 
     * @param {*} mainAkun mainAkun atau parentAkun 
     * @param {*} labelTotalSummary 
     * return {total_amount: totalamount, row_html: rowhtml}
     */
    CreateRowAkun(listAkun, mainAkun, labelTotalSummary) {

        let thisObject = this;

        let rowsPdf = [];
        // row main akun
        rowsPdf.push([{ content: mainAkun.value + ' ' + mainAkun.name, colspan: 2 }]);

        let listRowHtml = '';
        let totalSummary = 0;
        _.forEach(listAkun, function (akun) {
            totalSummary += numeral(akun.amount).value();
            let amount = new Intl.NumberFormat().format(akun.amount);

            rowsPdf.push([{ content: '  ' + akun.value + ' ' + akun.name, colspan: 2 }, amount]);

        });

        let totalSummaryRow = thisObject.CreateTotalSummaryRow(totalSummary, labelTotalSummary);

        rowsPdf.push(totalSummaryRow);

        let returnRow = { total_amount: totalSummary, rows_pdf: rowsPdf };

        return returnRow;

    }

    CreateTotalSummaryRow(totalAmount, labelTotalSummary) {
        let thisObject = this;
        let rowsPdf = null;
        let formatTotalSummary = new Intl.NumberFormat().format(totalAmount);

        return [{ content: labelTotalSummary, colspan: 2, styles: { fontStyle: 'bold', halign: 'left' } }, formatTotalSummary];

    }


    /*PrintPreviewReport(idPreviewContent, docPdf, headerStringArray) {

        let thisObject = this;

        let htmlTemplate;
        let htmlWaiting = ` <div class="kt-spinner kt-spinner--md kt-spinner--danger"></div>`;

        $('#' + idPreviewContent).html(htmlWaiting);

        try {
            thisObject.HeaderReportFinancial(docPdf, headerStringArray);

            const pdfDataString = docPdf.output('datauristring');
            const arrayPdfDataString = pdfDataString.split(';');

            let htmlTemplate = '<object ' + thisObject.GetSizeDialog(idPreviewContent) + '   data="' + arrayPdfDataString[0] + ';' + arrayPdfDataString[2] + '" type="application/pdf" ></object>';


            $('#' + idPreviewContent).html(htmlTemplate);

        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.PrintPreviewReport.name, error);
        }


    }*/
    PrintPreviewReport(idPreviewContent, docPdf) {

        let thisObject = this;

        let htmlTemplate;
        let htmlWaiting = ` <div class="kt-spinner kt-spinner--md kt-spinner--danger"></div>`;

        $('#' + idPreviewContent).html(htmlWaiting);

        try {

            const pdfDataString = docPdf.output('datauristring');
            const arrayPdfDataString = pdfDataString.split(';');

            let htmlTemplate = '<object ' + thisObject.GetSizeDialog(idPreviewContent) + '   data="' + arrayPdfDataString[0] + ';' + arrayPdfDataString[2] + '" type="application/pdf" ></object>';


            $('#' + idPreviewContent).html(htmlTemplate);

        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.PrintPreviewReport.name, error);
        }


    }

    SetValue(valueToPrint) {

        if (valueToPrint) {
            return valueToPrint;
        } else {
            return '';
        }

    }


    /*   PrintPreviewInout(idPreviewContent, inoutCMT) {
           let thisObject = this;
           let htmlTemplate;
   
           try {
   
               let idCanvas = 'barcodeCanvas';
               let htmlWaiting = `<canvas hidden id="` + idCanvas + `"></canvas>
               <div class="kt-spinner kt-spinner--md kt-spinner--danger"></div>`;
   
               let witdhObjectPreview = 0;
               if ($('#' + idPreviewContent)) {
                   witdhObjectPreview = $('#' + idPreviewContent)[0].clientWidth;
                   witdhObjectPreview = witdhObjectPreview * 0.9; //potong 10%
               }
               $('#' + idPreviewContent).html(htmlWaiting);
               let organizationService = new OrganizationService();
               let businesspartnerService = new BusinessPartnerService();
               let inoutlineService = new InoutlineService();
   
               Promise.all([organizationService.FindOrganizationComponentModelByAdOrgUu(inoutCMT.m_inout_wx_ad_org_uu),
               businesspartnerService.FindBusinesspartnerViewModelByCBpartnerUu(inoutCMT.c_bpartner_wx_c_bpartner_uu),
               inoutlineService.FindInoutlineViewModelByMInoutUu(inoutCMT.m_inout_wx_m_inout_uu)]).then(function (resultArray) {
   
                   let organizationCMTList = resultArray[0];
                   let organizationCMT = organizationCMTList[0];
   
                   let businesspartnerCMTList = resultArray[1];;
                   let businesspartnerCMT = businesspartnerCMTList[0];
   
                   let listInoutlineCMT = resultArray[2];
   
                   // The return value is the canvas element
                   let canvas = bwipjs.toCanvas(idCanvas, {
                       bcid: 'code128',       // Barcode type
                       text: inoutCMT.m_inout_wx_documentno,    // Text to encode
                       scale: 3,               // 3x scaling factor
                       height: 10,              // Bar height, in millimeters
                       includetext: true,            // Show human-readable text
                       textxalign: 'center',        // Always good to set this
                   });
   
   
   
   
                   let barcodeString = canvas.toDataURL(
                       'image/png');
   
                   //   $("object").replaceWith('<object data="' + doc.output('datauristring') + '"></object>');
                   let titlePrint = 'Surat Jalan';
                   let shipOrReceiptFrom = 'Kirim ke';
                   //  shipping
                   if (inoutCMT.m_inout_wx_c_doctype_id !== 3000) {
                       titlePrint = 'Material Receipt';
                       shipOrReceiptFrom = 'Terima dari';
                   }
   
                   const docPdf = new jsPDF();
                   const optionsSuratJalan = { align: 'left' };
                   docPdf.setFontSize(25);
   
   
   
   
                   docPdf.text(titlePrint, 20, 20, optionsSuratJalan);
                   docPdf.setFontSize(11);
                   docPdf.text('No Document', 150, 20);
                   docPdf.addImage(barcodeString, 'PNG', 120, 22, 80, 20);
                   // docPdf.text('Barcode', 270, 20);
                   docPdf.setFontSize(12);
                   docPdf.text(thisObject.SetValue(organizationCMT.ad_org_wx_name), 20, 50);
                   docPdf.text(thisObject.SetValue(organizationCMT.c_location_wx_address1), 20, 54);
                   docPdf.text(thisObject.SetValue(organizationCMT.c_location_wx_city) + ', ' + thisObject.SetValue(organizationCMT.c_location_wx_regionname), 20, 58);
                   docPdf.text(thisObject.SetValue(organizationCMT.ad_orginfo_wx_phone), 20, 62);
   
                   docPdf.text(shipOrReceiptFrom, 136, 50);
                   docPdf.text(businesspartnerCMT.c_bpartner_wx_name, 136, 54);
                   docPdf.text(businesspartnerCMT.c_location_wx_address1, 136, 58);
                   docPdf.text(businesspartnerCMT.c_location_wx_city + ', ' + businesspartnerCMT.c_location_wx_regionname, 136, 62);
                   docPdf.text(businesspartnerCMT.c_bpartner_location_wx_phone, 136, 66);
   
                   // shipping
                   if (inoutCMT.m_inout_wx_c_doctype_id === 3000) {
                       docPdf.autoTable({
                           startY: 90,
                           head: [['Order Date', 'Order Number', 'PO Number']],
                           body: [
                               [HelperService.ConvertTimestampToLocateDateString(inoutCMT.c_order_wx_dateordered), inoutCMT.c_order_wx_documentno, inoutCMT.m_inout_wx_poreference]
   
                           ]
                       });
                   } else {
                       docPdf.autoTable({
                           startY: 90,
                           head: [['Order Date', 'Order Number']],
                           body: [
                               [HelperService.ConvertTimestampToLocateDateString(inoutCMT.c_order_wx_dateordered), inoutCMT.c_order_wx_documentno]
   
                           ]
                       });
                   }
   
   
                   const listInoutline = [];
                   _.forEach(listInoutlineCMT, (inoutlineCMT) => {
                       const inoutlineRow = [inoutlineCMT.m_product_wx_sku, inoutlineCMT.m_product_wx_name, inoutlineCMT.c_uom_wx_name, inoutlineCMT.m_inoutline_wx_qtyentered];
                       listInoutline.push(inoutlineRow);
   
                   });
   
                   docPdf.autoTable({
                       startY: 108,
                       head: [['SKU', 'Item', 'Satuan', 'Jumlah']],
                       body: listInoutline
                   });
   
                   // const optionsNote = { align: 'right' };
                   // catatan
                   // docPdf.text('Please contact Customer Service at Phone with any questions', 100, 234, optionsNote);
   
   
                   const pdfDataString = docPdf.output('datauristring');
                   const arrayPdfDataString = pdfDataString.split(';');
                   let pdfString = arrayPdfDataString[0] + ';' + arrayPdfDataString[2];
                   let widthObject = '';
                   if (witdhObjectPreview) {
                       let height = witdhObjectPreview * 0.5 + witdhObjectPreview;
                       widthObject = 'width="' + witdhObjectPreview + '" height = "' + height + '"';
                   }
                   htmlTemplate = '<object ' + widthObject + '  data="' + arrayPdfDataString[0] + ';' + arrayPdfDataString[2] + '" type="application/pdf" ></object>';
   
   
                   $('#' + idPreviewContent).html(htmlTemplate);
   
   
   
               });
   
   
           } catch (error) {
               apiInterface.Log(thisObject.constructor.name, thisObject.PrintPreviewInout.name, error);
           }
   
   
       }
   
   
   
       PrintPreviewInvoice(idPreviewContent, invoiceCMT) {
   
           let thisObject = this;
           let htmlTemplate;
   
           try {
   
               let idCanvas = 'barcodeCanvas';
               let htmlWaiting = `<canvas hidden id="` + idCanvas + `"></canvas>
               <div class="kt-spinner kt-spinner--md kt-spinner--danger"></div>`;
   
               let witdhObjectPreview = 0;
               if ($('#' + idPreviewContent)) {
                   witdhObjectPreview = $('#' + idPreviewContent)[0].clientWidth;
                   witdhObjectPreview = witdhObjectPreview * 0.9; //potong 10%
               }
               $('#' + idPreviewContent).html(htmlWaiting);
               let organizationService = new OrganizationService();
               let businesspartnerService = new BusinessPartnerService();
               let invoicelineService = new InvoicelineService();
               Promise.all([organizationService.FindOrganizationComponentModelByAdOrgUu(invoiceCMT.c_invoice_wx_ad_org_uu),
               businesspartnerService.FindBusinesspartnerViewModelByCBpartnerUu(invoiceCMT.c_bpartner_wx_c_bpartner_uu),
               invoicelineService.FindInvoicelineComponentModelByCInvoiceUu(invoiceCMT.c_invoice_wx_c_invoice_uu)
               ]).then(function (resultArray) {
                   let organizationCMTList = resultArray[0];
                   let organizationCMT = organizationCMTList[0];
   
                   let businesspartnerCMTList = resultArray[1];
                   let businesspartnerCMT = businesspartnerCMTList[0];
   
                   let listInvoicelineCMT = resultArray[2];
   
   
                   // The return value is the canvas element
                   let canvas = bwipjs.toCanvas(idCanvas, {
                       bcid: 'code128',       // Barcode type
                       text: invoiceCMT.c_invoice_wx_documentno,    // Text to encode
                       scale: 3,               // 3x scaling factor
                       height: 10,              // Bar height, in millimeters
                       includetext: true,            // Show human-readable text
                       textxalign: 'center',        // Always good to set this
                   });
   
   
   
   
                   let barcodeString = canvas.toDataURL(
                       'image/png');
   
   
                   let titlePrint = 'Invoice';
                   // let shipOrReceiptFrom = 'To';
                   //  shipping
                   if (invoiceCMT.c_doctype_wx_docbasetype !== 'ARI') {
                       titlePrint = 'Invoice Vendor';
                       //   shipOrReceiptFrom = 'Terima dari';
                   }
   
                   const docPdf = new jsPDF();
                   const optionsSuratJalan = { align: 'left' };
                   docPdf.setFontSize(25);
   
                   docPdf.text(titlePrint, 20, 20, optionsSuratJalan);
                   docPdf.setFontSize(11);
                   docPdf.text('No Document', 150, 20);
                   docPdf.addImage(barcodeString, 'PNG', 120, 22, 80, 20);
                   // docPdf.text('Barcode', 270, 20);
                   docPdf.setFontSize(12);
                   docPdf.text(thisObject.SetValue(organizationCMT.ad_org_wx_name), 20, 50);
                   docPdf.text(thisObject.SetValue(organizationCMT.c_location_wx_address1), 20, 54);
                   docPdf.text(thisObject.SetValue(organizationCMT.c_location_wx_city) + ', ' + thisObject.SetValue(organizationCMT.c_location_wx_regionname), 20, 58);
                   docPdf.text(thisObject.SetValue(organizationCMT.ad_orginfo_wx_phone), 20, 62);
   
                   //  docPdf.text(shipOrReceiptFrom, 136, 50);
                   docPdf.text(businesspartnerCMT.c_bpartner_wx_name, 136, 54);
                   docPdf.text(businesspartnerCMT.c_location_wx_address1, 136, 58);
                   docPdf.text(businesspartnerCMT.c_location_wx_city + ', ' + businesspartnerCMT.c_location_wx_regionname, 136, 62);
                   docPdf.text(businesspartnerCMT.c_bpartner_location_wx_phone, 136, 66);
   
                   // shipping
                   if (invoiceCMT.c_doctype_wx_docbasetype === 'ARI') {
                       docPdf.autoTable({
                           startY: 90,
                           head: [['Order Date', 'Invoice Number', 'PO Number']],
                           body: [
                               [HelperService.ConvertTimestampToLocateDateString(invoiceCMT.c_order_wx_dateordered), invoiceCMT.c_invoice_wx_documentno, invoiceCMT.c_order_wx_poreference]
   
                           ]
                       });
                   } else {
                       docPdf.autoTable({
                           startY: 90,
                           head: [['Order Date', 'Invoice Number']],
                           body: [
                               [HelperService.ConvertTimestampToLocateDateString(invoiceCMT.c_order_wx_dateordered), invoiceCMT.c_invoice_wx_documentno]
   
                           ]
                       });
                   }
   
   
                   const listInvoiceline = [];
                   _.forEach(listInvoicelineCMT, (invoicelineComponentModelTable) => {
                       let taxamt = Intl.NumberFormat().format(invoicelineComponentModelTable.c_invoiceline_wx_taxamt);
                       let linenetamt = Intl.NumberFormat().format(invoicelineComponentModelTable.c_invoiceline_wx_linenetamt);
                       const invoicelineRow = [invoicelineComponentModelTable.c_invoiceline_wx_line, invoicelineComponentModelTable.m_product_wx_sku, invoicelineComponentModelTable.m_product_wx_name, invoicelineComponentModelTable.c_uom_wx_name, invoicelineComponentModelTable.c_invoiceline_wx_qtyentered, taxamt, linenetamt];
                       listInvoiceline.push(invoicelineRow);
   
                   });
                   let totalTaxlines = new Intl.NumberFormat().format(invoiceCMT.c_invoice_wx_totaltaxlines);
                   let totalLines = new Intl.NumberFormat().format(invoiceCMT.c_invoice_wx_totallines);
                   let footerTotal = ['', '', '', '', 'Total : ', totalTaxlines, totalLines];
                   listInvoiceline.push(footerTotal);
   
                   let grandTotal = new Intl.NumberFormat().format(invoiceCMT.c_invoice_wx_grandtotal);
                   let footerGrandTotal = ['', '', '', '', 'Grand Total', { content: grandTotal, colSpan: 2, styles: { fontStyle: 'bold', halign: 'center', fontSize: 12 } }];
                   listInvoiceline.push(footerGrandTotal);
   
                   docPdf.autoTable({
                       startY: 108,
                       head: [['No', 'SKU', 'Item', 'Satuan', 'Jumlah', 'Tax', 'Net']],
                       body: listInvoiceline
                   });
   
                   const pdfDataString = docPdf.output('datauristring');
                   const arrayPdfDataString = pdfDataString.split(';');
                   let pdfString = arrayPdfDataString[0] + ';' + arrayPdfDataString[2];
                   let widthObject = '';
                   if (witdhObjectPreview) {
                       let height = witdhObjectPreview * 0.5 + witdhObjectPreview;
                       widthObject = 'width="' + witdhObjectPreview + '" height = "' + height + '"';
                   }
                   htmlTemplate = '<object ' + widthObject + '  data="' + arrayPdfDataString[0] + ';' + arrayPdfDataString[2] + '" type="application/pdf" ></object>';
   
   
                   $('#' + idPreviewContent).html(htmlTemplate);
   
   
   
               });
   
           } catch (error) {
               apiInterface.Log(thisObject.constructor.name, thisObject.PrintPreviewInvoice.name, error);
           }
   
   
       }*/



}