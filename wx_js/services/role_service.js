
class RoleService {

    constructor() { }


    FindAll(metaDataResponseTable) {

        let searchFilter = '';

        let metaDataQuery;
        try {

            if (metaDataResponseTable.search !== '') {
                searchFilter += ' AND ' + metaDataResponseTable.searchField + 'LIKE \'%' + metaDataResponseTable.search + '%\'';
            }

            let otherFilters = '';
            if (metaDataResponseTable.otherFilters.length > 0) {

                _.forEach(metaDataResponseTable.otherFilters, function (value) {

                    otherFilters += ' ' + value + ' ';
                });

            }

            const sqlTemplate = '  FROM ad_role ' +
                ' WHERE ad_role.wx_isdeleted = \'N\' AND ad_role.ad_org_uu = \'' + metaDataResponseTable.ad_org_uu + '\'' + otherFilters + searchFilter + ' ORDER BY ad_role.name DESC ';

            const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new RoleComponentModel());

            metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);

        } catch (error) {
            apiInterface.Log(this.constructor.name, this.FindAll.name, error);
        }

        return apiInterface.Query(metaDataQuery);

    }



    FindRoleCMTListByAdRoleUu(ad_role_uu) {
        let metaDataQuery;
        try {

            const sqlTemplate = '  FROM ad_role ' +
                ' WHERE ad_role.ad_role_uu = \'' + ad_role_uu + '\'';


            const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new RoleComponentModel());

            metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);
        } catch (error) {
            apiInterface.Log(this.constructor.name, this.FindRoleCMTListByAdRoleUu.name, error);
        }



        return apiInterface.Query(metaDataQuery);


    }

    /**
   * Save Update harus merefer pada OrderViewModelTable
   * @param {RoleComponentModel} roleCM 
   * @param {*} datatableReference dataTableReferemce MInout datatable
   */
    async SaveUpdate(roleCM, datatableReference) {
        const timeNow = new Date().getTime();
        let insertSuccess = 0;
        let isNewRecord = false;
        try {
            if (!roleCM.ad_role.ad_role_uu) { //new record
                isNewRecord = true;
                roleCM.ad_role.ad_role_uu = HelperService.UUID();
                roleCM.ad_role.ad_client_uu = localStorage.getItem('ad_client_uu');
                roleCM.ad_role.ad_org_uu = localStorage.getItem('ad_org_uu');

                roleCM.ad_role.createdby = localStorage.getItem('ad_user_uu');
                roleCM.ad_role.updatedby = localStorage.getItem('ad_user_uu');

                roleCM.ad_role.created = timeNow;
                roleCM.ad_role.updated = timeNow;
            } else { //update record


                roleCM.ad_role.updatedby = localStorage.getItem('ad_user_uu');

                roleCM.ad_role.updated = timeNow;
            }

            roleCM.ad_role.sync_client = null;
            roleCM.ad_role.process_date = null;


            const sqlInsert = HelperService.SqlInsertOrReplaceStatement('ad_role', roleCM.ad_role);

            let rowChanged = await apiInterface.ExecuteSqlStatement(sqlInsert);
            insertSuccess += rowChanged;
            if (insertSuccess > 0) {

                if (datatableReference) {
                    let rowDataList = await thisObject.FindRoleCMTListByAdRoleUu(roleCM.ad_role.ad_role_uu);

                    let rowData = {};

                    if (rowDataList.length > 0) {
                        rowData = rowDataList[0];
                    }

                    if (!isNewRecord) {

                        datatableReference.row($('#bttn_row_edit' + rowData['ad_role_wx_ad_role_uu']).parents('tr')).data(rowData).draw();
                    } else {

                        datatableReference.row.add(rowData).draw();
                    }



                }
            }

            return insertSuccess;

            /* return new Promise(function (resolve, reject) {
 
                 apiInterface.ExecuteSqlStatement(sqlInsert)
                     .then(function (rowChanged) {
 
                         insertSuccess += rowChanged;
                         if (insertSuccess > 0) {
 
                             if (datatableReference) {
                                 thisObject.FindRoleCMTListByAdRoleUu(roleCM.ad_role.ad_role_uu)
                                     .then(function (rowDataList) {
                                         let rowData = {};
 
                                         if (rowDataList.length > 0) {
                                             rowData = rowDataList[0];
                                         }
 
                                         if (!isNewRecord) {
 
                                             datatableReference.row($('#bttn_row_edit' + rowData['ad_role_wx_ad_role_uu']).parents('tr')).data(rowData).draw();
                                         } else {
 
                                             datatableReference.row.add(rowData).draw();
                                         }
 
 
                                     });
 
                                     resolve(insertSuccess);
 
 
                             }
                         }else{
                             resolve(insertSuccess)
                         }
                     });
 
 
 
             });*/

        } catch (error) {
            apiInterface.Log(this.constructor.name, this.SaveUpdate.name, error);
            return new Promise(function (resolve, reject) {
                resolve(0);
            });
        }



    }

    GetSqlStatement() {



    }



}