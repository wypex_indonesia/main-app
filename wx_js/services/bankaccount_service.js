
class BankaccountService {

    constructor() { }

    /**
	 * Find All warehouse dengan bentuk warehouseviewmodel
	 * @param {*} metaDataResponseTable 
	 */
    FindAll(metaDataResponseTable) {
        let metaDataQuery;
        try {
            let searchFilter = '';
            if (metaDataResponseTable.search !== '') {
                searchFilter += ' AND ' + metaDataResponseTable.searchField + 'LIKE \'%' + metaDataResponseTable.search + '%\'';
            }

            let otherFilters = '';
            if (metaDataResponseTable.otherFilters.length > 0) {

                _.forEach(metaDataResponseTable.otherFilters, function (value) {

                    otherFilters += ' ' + value + ' ';
                });

            }

            const sqlTemplate = '  FROM c_bankaccount ' +
                ' WHERE c_bankaccount.wx_isdeleted = \'N\' AND c_bankaccount.ad_org_uu = \'' + metaDataResponseTable.ad_org_uu + '\'' + otherFilters + searchFilter + ' ORDER BY c_bankaccount.name DESC ';

            const listMetaColumn = HelperService.ConvertViewModelToMetaColumn({ c_bankaccount: new CBankaccount() });

            metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        } catch (err) {
            apiInterface.Log(this.constructor.name, this.FindAll.name, err);

        }

        return apiInterface.Query(metaDataQuery);
    }

    FindPettyCash(metaDataResponseTable) {

        let searchFilter = '';
        let metaDataQuery;
        try {
            if (metaDataResponseTable.search !== '') {
                searchFilter += ' AND ' + metaDataResponseTable.searchField + 'LIKE \'%' + metaDataResponseTable.search + '%\'';
            }

            let otherFilters = '';
            if (metaDataResponseTable.otherFilters.length > 0) {

                _.forEach(metaDataResponseTable.otherFilters, function (value) {

                    otherFilters += ' ' + value + ' ';
                });

            }

            const sqlTemplate = '  FROM c_bankaccount ' +
                ' WHERE c_bankaccount.bankaccounttype = \'B\' AND c_bankaccount.wx_isdeleted = \'N\' AND c_bankaccount.ad_org_uu = \'' + metaDataResponseTable.ad_org_uu + '\'' + otherFilters + searchFilter + ' ORDER BY c_bankaccount.name DESC ';

            const listMetaColumn = HelperService.ConvertViewModelToMetaColumn({ c_bankaccount: new CBankaccount() });

            metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);

        } catch (err) {

            apiInterface.Log(this.constructor.name, this.FindPettyCash.name, err);

        }



        return apiInterface.Query(metaDataQuery);

    }

    FindAllBankAccountCMTList() {

        let metaDataQuery;

        try {
            const sqlTemplate = '  FROM c_bankaccount ' +
                ' WHERE c_bankaccount.bankaccounttype = \'C\' AND c_bankaccount.wx_isdeleted = \'N\'  ORDER BY c_bankaccount.name ASC ';

            const listMetaColumn = HelperService.ConvertViewModelToMetaColumn({ c_bankaccount: new CBankaccount() });

            metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);

        } catch (err) {

            apiInterface.Log(this.constructor.name, this.FindAllBankAccountCMTList.name, err);
        }


        return apiInterface.Query(metaDataQuery);

    }

    GetBankaccountCMTByCBankaccountUu(c_bankaccount_uu) {
        let metaDataQuery;
        try {
            const sqlTemplate = '  FROM c_bankaccount ' +
                ' WHERE c_bankaccount.c_bankaccount_uu = \'' + c_bankaccount_uu + '\'';

            let bankaccountCM = { c_bankaccount: new CBankaccount() };
            const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(bankaccountCM);

            metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);

        } catch (err) {

            apiInterface.Log(this.constructor.name, this.GetBankaccountCMTByCBankaccountUu.name, err);
        }


        return apiInterface.Query(metaDataQuery);


    }

    /**
    * Tiap bank account akan selalu mempunyai c_elementvalue 
    * dan tiap bank account mempunyai 2 elementvalue : 
    * 1. Cari transaction_code = bank di table c_elementvalue, akan menjadi parent element (Bank IDR)
    * 1. Parent semisal Bank Mandiri 1111.2100.0001
    * 2. Child akan menjadi Bank IDR - Mandiri dalam transit 1111.2100.0002 , dan parent_value 1111.2100.0001
    * 
    * 2. Cari transaction_code = cash_pos di table c_elementvalue, akan menjadi parent element (Kas Point Of Sales)
    * 1 Parent semisal Pos001 1111.1300.0001
    * 2.  Child akan menjadi Pos - 001 dalam transit  1111.1300.0002 , dan parent_value  1111.1300.0001
    * @param {BankaccountViewModel} bankaccountCM 
    * @param {*} datatableReference dataTableReferemce CInvoice datatable
    */
    async  SaveUpdate(bankaccountCM, datatableReference) {
        const timeNow = new Date().getTime();

        let thisObject = this;
        let isNewRecord = false;
        let insertSuccess = 0;

        try {
            if (!bankaccountCM.c_bankaccount.c_bankaccount_uu) { //new record
                isNewRecord = true;
                bankaccountCM.c_bankaccount.c_bankaccount_uu = HelperService.UUID();
                bankaccountCM.c_bankaccount.ad_client_uu = localStorage.getItem('ad_client_uu');;
                bankaccountCM.c_bankaccount.ad_org_uu = localStorage.getItem('ad_org_uu');

                bankaccountCM.c_bankaccount.createdby = localStorage.getItem('ad_user_uu');
                bankaccountCM.c_bankaccount.updatedby = localStorage.getItem('ad_user_uu');

                bankaccountCM.c_bankaccount.created = timeNow;
                bankaccountCM.c_bankaccount.updated = timeNow;

            } else { //update record


                bankaccountCM.c_bankaccount.updatedby = localStorage.getItem('ad_user_uu');

                bankaccountCM.c_bankaccount.updated = timeNow;
            }



            bankaccountCM.c_bankaccount.sync_client = null;
            bankaccountCM.c_bankaccount.process_date = null;

            let sqlInsertArray = [];
            //CREATE NEW JIKA BELUM ADA
            if (!bankaccountCM.c_bankaccount.asset_element) {
                let elementValueService = new ElementValueService();

                let assetElementValue = new CElementvalue();
                let transitElementValue = new CElementvalue();
             
                let elementValueCMTList = null;
                let transaction_code = '';
                //cash 
                if (bankaccountCM.c_bankaccount.bankaccounttype === 'B') {
                    transaction_code = 'cash_pos';
                                
                }
                //bank
                if (bankaccountCM.c_bankaccount.bankaccounttype === 'C') {
                    transaction_code = 'bank';                   
                }

                elementValueCMTList = await elementValueService.FindChildFromTransactionCode(transaction_code);     

                let elementValueCMT = null;
                let parent_value = '';
                 //jika ada maka ambil paling akhir,
                 if (elementValueCMTList.length) {
                   elementValueCMT = elementValueCMTList[elementValueCMTList.length - 1];
                   parent_value = elementValueCMT.c_elementvalue_wx_parent_value;
                   
                } else {
                    // jika tidak ada maka create new elementvalue
                    let elementValueParentCMTList = await elementValueService.FindElementByTransactionCode(transaction_code);
                    elementValueCMT = elementValueParentCMTList[0];
                    parent_value = elementValueCMT.c_elementvalue_wx_value;

                }

                let value = numeral(elementValueCMT.c_elementvalue_wx_value.split('.').join('')).value();
                let assetvalue = value + 1;
                let transitvalue = assetvalue + 1;

                assetElementValue.name = bankaccountCM.c_bankaccount.name + ' ' + bankaccountCM.c_bankaccount.accountno + ' - Asset';
                assetElementValue.value = HelperService.ConvertValueNumberToValueDot(assetvalue);
                assetElementValue.parent_value = parent_value;
                assetElementValue.account_type = 'Asset';

                transitElementValue.name = bankaccountCM.c_bankaccount.name + ' ' + bankaccountCM.c_bankaccount.accountno + ' - Dalam Transit';
                transitElementValue.value = HelperService.ConvertValueNumberToValueDot(transitvalue);
                transitElementValue.parent_value = parent_value;
                transitElementValue.account_type = 'Asset';

                bankaccountCM.c_bankaccount.asset_element = assetElementValue.value;
                bankaccountCM.c_bankaccount.transit_element = transitElementValue.value;
                insertSuccess += await elementValueService.SaveUpdate(assetElementValue);
                insertSuccess += await elementValueService.SaveUpdate(transitElementValue);
                
            }


            const sqlInsert = HelperService.SqlInsertOrReplaceStatement('c_bankaccount', bankaccountCM.c_bankaccount);
            insertSuccess += await apiInterface.ExecuteSqlStatement(sqlInsert);

            if (insertSuccess > 0) {

                if (datatableReference) {
                    let rowDataList = await thisObject.GetBankaccountCMTByCBankaccountUu(bankaccountCM.c_bankaccount.c_bankaccount_uu);
                    let rowData = {};

                    if (rowDataList.length > 0) {
                        rowData = rowDataList[0];
                    }

                    if (!isNewRecord) {

                        datatableReference.row($('#bttn_row_edit' + rowData['c_bankaccount_wx_c_bankaccount_uu']).parents('tr')).data(rowData).draw();
                    } else {

                        datatableReference.row.add(rowData).draw();
                    }




                }





            }
            return insertSuccess;
            /* return new Promise(function (resolve, reject) {
                 .then(function (rowChanged) {
 
                     insertSuccess += rowChanged;
                     if (insertSuccess > 0) {
 
                         if (datatableReference) {
                             thisObject.GetBankaccountCMTByCBankaccountUu(bankaccountCM.c_bankaccount.c_bankaccount_uu).then(function (rowDataList) {
                                 let rowData = {};
 
                                 if (rowDataList.length > 0) {
                                     rowData = rowDataList[0];
                                 }
 
                                 if (!isNewRecord) {
 
                                     datatableReference.row($('#bttn_row_edit' + rowData['c_bankaccount_wx_c_bankaccount_uu']).parents('tr')).data(rowData).draw();
                                 } else {
 
                                     datatableReference.row.add(rowData).draw();
                                 }
 
                                 resolve(insertSuccess);
 
                             });
                         }
 
                         resolve(insertSuccess);
 
 
 
                     }
 
 
                 });
 
 
 
             });*/



        } catch (err) {

            apiInterface.Log(this.constructor.name, this.SaveUpdate.name, err);
            return new Promise(function (resolve, reject) {
                resolve(insertSuccess);
            });

        }



    }



}