

class BankstatementlineService {
    constructor() { }

    /**
       * Find All warehouse dengan bentuk warehouseviewmodel
       * @param {*} metaDataResponseTable 
       */
    FindAll(metaDataResponseTable) {


        let searchFilter = '';
        if (metaDataResponseTable.search !== '') {
            searchFilter += ' AND ' + metaDataResponseTable.searchField + '  LIKE    \'%' + metaDataResponseTable.search + '%\'';
        }

        let limit = '';
        if ((metaDataResponseTable.page !== null) && (metaDataResponseTable.perpage !== null)) {

            limit = ' LIMIT ' + metaDataResponseTable.page + ',' + metaDataResponseTable.perpage;

        }

        const sqlTemplate = '  FROM c_bankstatementline ' +
            ' INNER JOIN c_payment ON c_payment.c_payment_uu  = c_bankstatementline.c_payment_uu ' +
            ' INNER JOIN c_bankstatement ON c_bankstatement.c_bankstatement_uu = c_bankstatementline.c_bankstatement_uu ' +
            ' WHERE  c_bankstatementline.ad_org_uu = \'' + metaDataResponseTable.ad_org_uu +
            '\'' + searchFilter + ' ORDER BY c_bankstatementline.updated DESC ' + limit;

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new BankstatementlineComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }

    FindBankstatementlineComponentModelByCBankstatementUu(c_bankstatement_uu) {

        const sqlTemplate = '  FROM c_bankstatementline ' +
            ' INNER JOIN c_payment ON c_payment.c_payment_uu  = c_bankstatementline.c_payment_uu ' +
            ' INNER JOIN c_bankstatement ON c_bankstatement.c_bankstatement_uu = c_bankstatementline.c_bankstatement_uu ' +
            ' WHERE c_bankstatementline.wx_isdeleted = \'N\' AND c_bankstatementline.c_bankstatement_uu = \'' + c_bankstatement_uu + '\' ORDER BY c_bankstatementline.updated ASC';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new BankstatementlineComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }

    FindOnlyBankstatementlineByCBankstatementUu(c_bankstatement_uu) {

        const sqlTemplate = '  FROM c_bankstatementline ' +
            ' WHERE c_bankstatementline.wx_isdeleted = \'N\' AND c_bankstatementline.c_bankstatement_uu = \'' + c_bankstatement_uu + '\' ORDER BY c_bankstatementline.updated ASC';

        let customCM = { c_bankstatementline: new CBankstatementline() };

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(customCM);

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }



    FindBankstatementlineComponentModelByCPaymentUu(c_payment_uu) {

        const sqlTemplate = '  FROM c_bankstatementline ' +
            ' INNER JOIN c_payment ON c_payment.c_payment_uu  = c_bankstatementline.c_payment_uu ' +
            ' INNER JOIN c_bankstatement ON c_bankstatement.c_bankstatement_uu = c_bankstatementline.c_bankstatement_uu ' +
            ' WHERE c_bankstatementline.wx_isdeleted = \'N\' AND c_bankstatementline.c_payment_uu = \'' + c_payment_uu + '\' ORDER BY c_bankstatementline.updated ASC';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new BankstatementlineComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }


    /**
     * Save Update harus merefer pada OrderViewModelTable
     * @param {CBankstatementline} bankstatementline
     */
    SaveUpdate(bankstatementline) {
        const timeNow = new Date().getTime();

        let isNewRecord = false;
        let insertSuccess = 0;
        let thisObject = this;
        try {


            const sqlInsert = thisObject.GetInsertSqlStatement(bankstatementline);

            return apiInterface.ExecuteSqlStatement(sqlInsert);



        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.SaveUpdate.name, error);
            return new Promise(function (resolve, reject) {

                resolve(insertSuccess);
            });
        }



    }

    GetInsertSqlStatement(bankstatementline) {
        const timeNow = new Date().getTime();

        let isNewRecord = false;
        let insertSuccess = 0;
        let thisObject = this;
        let sqlInsert = '';
        try {
            if (!bankstatementline.c_bankstatementline_uu) { //new record
                isNewRecord = true;
                bankstatementline.c_bankstatementline_uu = HelperService.UUID();
                bankstatementline.ad_client_uu = localStorage.getItem('ad_client_uu');;
                bankstatementline.ad_org_uu = localStorage.getItem('ad_org_uu');

                bankstatementline.createdby = localStorage.getItem('ad_user_uu');
                bankstatementline.updatedby = localStorage.getItem('ad_user_uu');

                bankstatementline.created = timeNow;
                bankstatementline.updated = timeNow;
            } else { //update record


                bankstatementline.updatedby = localStorage.getItem('ad_user_uu');

                bankstatementline.updated = timeNow;
            }

            if (!bankstatementline.ad_client_uu){
                bankstatementline.ad_client_uu = localStorage.getItem('ad_client_uu');;
                bankstatementline.ad_org_uu = localStorage.getItem('ad_org_uu');

            }

            if (!bankstatementline.created){
                bankstatementline.created = timeNow;
                bankstatementline.createdby = localStorage.getItem('ad_user_uu');
            }



            bankstatementline.sync_client = null;
            bankstatementline.process_date = null;


            sqlInsert = HelperService.SqlInsertOrReplaceStatement('c_bankstatementline', bankstatementline);
        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.SaveUpdate.name, error);
        }

        return sqlInsert;
    }



}