class PeriodService {

    constructor() { }


    FindPeriodByName(period_name){

        const sqlTemplate = 'SELECT name, start_timestamp, end_timestamp FROM c_period WHERE name = \''+ period_name + '\';';
       
        return apiInterface.DirectQuerySqlStatement(sqlTemplate);


    }

    /**
     * Find period and previous period, 
     * return periodList , dengan index 0 nextPeriod, dan index 1 previous Period
     */
    FindPeriodAndPreviousByName(nex_period_name){

        const sqlTemplate = 'SELECT name, start_timestamp, end_timestamp FROM c_period '+
            ' WHERE name = \''+ nex_period_name + '\' ORDER BY start_timestamp DESC LIMIT 0,2 ;';
            return apiInterface.DirectQuerySqlStatement(sqlTemplate);

    }


    /**
     * 1. Check apakah ada yang period yang open, ambil start_timestamp dan end_timestamp
     *  1.1 Jika ada, check currentTimestamp, apakah ada diantara start_timestamp and end_timestamp
     *      1.1.1 Jika diantara range timestamp  maka, return the period
     *      1.1.2 Jika tidak diantara range timestamp, maka create new period , getMonth and getYear , 
     *            and set start_timestamp and end_timestamp, set isopen = 'Y', dan yang lain set isopen = 'N'.
     *  1.2. Jika tidak ada, create new period, set isopen= 'Y', start_timestamp and end_timestamp
     * 
     * return namePeriod
     */
    FindAndCreatePeriodOpen() {
        let thisObject = this;
        const sql = 'SELECT name, start_timestamp, end_timestamp FROM c_period WHERE isopen = \'Y\';';
        return new Promise(function (resolve, reject) {

            apiInterface.DirectQuerySqlStatement(sql).then(function (c_period_list) {
                let returnPeriode = null;

                let currentTimestamp = new Date().getTime();

                if (c_period_list.length) {
                    let c_period = c_period_list[0];
                    //1.1.1
                    if ((currentTimestamp >= c_period.start_timestamp) && (currentTimestamp <= c_period.end_timestamp)) {
                        returnPeriode = c_period;
                    }
                }

                resolve(returnPeriode);


            });

        }).then(function (returnPeriode) {

            return new Promise(function (resolve, reject) {
                if (returnPeriode) {
                    resolve(returnPeriode);
                } else {
                    thisObject.SaveUpdate(new CPeriod()).then(function (c_periode) {
                        resolve(c_periode);

                    });
                }
            });

        });


    }

    /**
     * 
     * @param {*} warehouseCM 
        return c_period
     */
    async SaveUpdate(c_period) {
        const timeNow = new Date().getTime();

        let thisObject = this;
        let isNewRecord = false;
        let insertSuccess = 0;

        try {
            if (!c_period.c_period_uu) { //new record
                isNewRecord = true;
                c_period.c_period_uu = HelperService.UUID();
                c_period.ad_client_uu = localStorage.getItem('ad_client_uu');
                c_period.ad_org_uu = localStorage.getItem('ad_org_uu');

                c_period.createdby = localStorage.getItem('ad_user_uu');
                c_period.updatedby = localStorage.getItem('ad_user_uu');

                c_period.created = timeNow;
                c_period.updated = timeNow;


            } else { //update record


                c_period.updatedby = localStorage.getItem('ad_user_uu');

                c_period.updated = timeNow;
            }

            c_period.isopen = 'Y';
            if (!c_period.name) {
                c_period.name = HelperService.ConvertTimeStampToPeriodeName(timeNow);
            }

            let startEndTime = HelperService.ConvertPeriodeToStartimeToEndTime(c_period.name);
            c_period.start_timestamp = startEndTime.start_timestamp;
            c_period.end_timestamp = startEndTime.end_timestamp;


            const sqlInsert = HelperService.SqlInsertOrReplaceStatement('c_period', c_period);
            let listSql = [];

            const sqlSetCloseOtherPeriodeStatement = 'UPDATE c_period SET isopen = \'N\' WHERE c_period_uu != \'' + c_period.c_period_uu + '\';';

            listSql.push(sqlInsert);
            listSql.push(sqlSetCloseOtherPeriodeStatement);
            insertSuccess = await apiInterface.ExecuteBulkSqlStatement(listSql);
        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.SaveUpdate.name, error);
        }

        return c_period;
    }
}