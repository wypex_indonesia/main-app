
class PaperSizeService {

    constructor() { }


    FindByWxtPaperTypeUu(wxt_paper_type_uu) {


        let metaDataQuery;
        try {

           
            const sqlTemplate = '  FROM wxt_paper_size ' +
            ' INNER JOIN wxt_paper_type ON wxt_paper_type.wxt_paper_type_uu  = wxt_paper_size.wxt_paper_type_uu ' +
            ' WHERE wxt_paper_size.wxt_paper_type_uu = \''+wxt_paper_type_uu + '\'' +
            ' ORDER BY wxt_paper_size.paper_size ASC';

            const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new PaperSizeComponentModel());

            metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);

        } catch (error) {
            apiInterface.Log(this.constructor.name, this.FindByWxtPaperTypeUu.name, error);
        }

        return apiInterface.Query(metaDataQuery);

    }





}