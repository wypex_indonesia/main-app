
class PricelistVersionService {

    constructor() { }
    /**
     * 
     * @param {*} m_pricelist_version_uu 
     */
    FindPricelistVersionViewModelByMPricelistVersionUu(m_pricelist_version_uu) {

        let metaDataQuery;
        try {
            const sqlTemplate = '  FROM m_pricelist_version ' +
                ' INNER JOIN m_pricelist ON m_pricelist.m_pricelist_uu = m_pricelist_version.m_pricelist_uu ' +
                ' WHERE m_pricelist_version.wx_isdeleted = \'N\' AND m_pricelist_version.m_pricelist_version_uu = \'' + m_pricelist_version_uu + '\'';

            const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new PricelistVersionViewModel());

            metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);

        } catch (error) {
            apiInterface.Log(this.constructor.name, this.PricelistVersionService.name, error);
        }


        return apiInterface.Query(metaDataQuery);

    }


    /**
	 * Find All warehouse dengan bentuk warehouseviewmodel
	 * @param {*} metaDataResponseTable 
	 */
   async  FindAll(metaDataResponseTable) {

        let metaDataQuery;
        try {
            let searchFilter = '';
            if (metaDataResponseTable.search !== '') {
                searchFilter += ' AND ' + metaDataResponseTable.searchField + '%' + metaDataResponseTable.search + '%';
            }

            let otherFilters = '';
            if (metaDataResponseTable.otherFilters.length > 0) {
                otherFilters += metaDataResponseTable.otherFilters.join(' ');
            }

            let rolelineService = new RolelineService();
            let notInClauseSql = await rolelineService.GetNotInClauseSql('MPricelist');

            let filterRole = '';
            if (notInClauseSql) {
                filterRole = ' AND m_pricelist.m_pricelist_uu NOT IN (' + notInClauseSql + ') ';
            }

            const sqlTemplate = '  FROM m_pricelist_version ' +
                ' INNER JOIN m_pricelist ON m_pricelist.m_pricelist_uu = m_pricelist_version.m_pricelist_uu ' +
                ' WHERE m_pricelist_version.wx_isdeleted = \'N\' AND m_pricelist_version.ad_org_uu = \'' + metaDataResponseTable.ad_org_uu + '\'' + filterRole + otherFilters + ' ORDER BY m_pricelist_version.updated DESC ';

            const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new PricelistVersionViewModel());

            metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);
            return apiInterface.Query(metaDataQuery);



        } catch (error) {
            apiInterface.Log(this.constructor.name, this.FindAll.name, error);
            return new Promise(function(resolve,reject){

                resolve([]);
            });
        }





    }

    async SaveUpdate(pricelistVersionCM, datatableReference) {
        let thisObject = this;
        let insertSuccess = 0;
        try {



            const timeNow = new Date().getTime();
            // jika new record maka buat mlocator 	
            // harus dibuat default locator, karena locator akan dipergunakan di inventory movement
            let isNewRecord = false;
            if (!pricelistVersionCM.m_pricelist_version.m_pricelist_version_uu) { //new record
                isNewRecord = true;
                pricelistVersionCM.m_pricelist_version.m_pricelist_version_uu = HelperService.UUID();
                pricelistVersionCM.m_pricelist_version.ad_client_uu = localStorage.getItem('ad_client_uu');
                pricelistVersionCM.m_pricelist_version.ad_org_uu = localStorage.getItem('ad_org_uu');

                pricelistVersionCM.m_pricelist_version.createdby = localStorage.getItem('ad_user_uu');
                pricelistVersionCM.m_pricelist_version.updatedby = localStorage.getItem('ad_user_uu');

                pricelistVersionCM.m_pricelist_version.created = timeNow;
                pricelistVersionCM.m_pricelist_version.updated = timeNow;
            } else { //update record


                pricelistVersionCM.m_pricelist_version.updatedby = localStorage.getItem('ad_user_uu');

                pricelistVersionCM.m_pricelist_version.updated = timeNow;
            }

            pricelistVersionCM.m_pricelist_version.m_pricelist_uu = pricelistVersionCM.m_pricelist.m_pricelist_uu;
            pricelistVersionCM.m_pricelist_version.sync_client = null;
            pricelistVersionCM.m_pricelist_version.process_date = null;


            const sqlInsertPricelistVersion = HelperService.SqlInsertOrReplaceStatement('m_pricelist_version', pricelistVersionCM.m_pricelist_version);
            insertSuccess += await apiInterface.ExecuteSqlStatement(sqlInsertPricelistVersion);




            // jika sukses tersimpan 
            if (insertSuccess > 0) {

                if (datatableReference) {


                    let rowDataList = await thisObject.FindPricelistVersionViewModelByMPricelistVersionUu(pricelistVersionCM.m_pricelist_version.m_pricelist_version_uu);

                    let rowData = {};

                    if (rowDataList.length > 0) {
                        rowData = rowDataList[0];
                    }

                    if (!isNewRecord) {

                        datatableReference.row($('#bttn_row_edit' + rowData['m_pricelist_version_wx_m_pricelist_version_uu']).parents('tr')).data(rowData).draw();
                    } else {

                        datatableReference.row.add(rowData).draw();
                    }


                }



            }



        } catch (error) {
            apiInterface.Log(this.constructor.name, this.SaveUpdate.name, error);
            return insertSuccess;
        }

        return insertSuccess;



    }


}