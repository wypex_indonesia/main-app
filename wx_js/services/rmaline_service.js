

class RmalineService {
    constructor() { }


    /**
	 * 
	 * @param {*} metaDataResponseTable 
	 */
    FindAll(metaDataResponseTable) {


        let otherFilters = '';
        if (metaDataResponseTable.otherFilters.length > 0) {

            _.forEach(metaDataResponseTable.otherFilters, function (value) {

                otherFilters += ' ' + value + ' ';
            });

        }

        const sqlTemplate = '  FROM m_rmaline ' +
            ' INNER JOIN m_rma ON m_rma.m_rma_uu = m_rmaline.m_rma_uu ' +
            ' INNER JOIN c_invoiceline ON c_invoiceline.c_invoiceline_uu = m_rmaline.c_invoiceline_uu ' +
            ' INNER JOIN m_product ON m_product.m_product_uu  = m_rmaline.m_product_uu ' +
            ' INNER JOIN c_uom ON m_rmaline.c_uom_uu = c_uom.c_uom_uu ' +
            ' INNER JOIN c_tax ON c_invoiceline.c_tax_uu  = c_tax.c_tax_uu ' +
            ' WHERE m_rmaline.wx_isdeleted = \'N\' ' + otherFilters + ' ORDER BY m_rmaline.line ASC ';



        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new RmalineComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);

    }

    FindRmalineComponentModelByMRmalineUu(m_rmaline_uu) {


        const sqlTemplate = '  FROM m_rmaline ' +
            ' INNER JOIN m_rma ON m_rma.m_rma_uu = m_rmaline.m_rma_uu ' +
            ' INNER JOIN c_invoiceline ON c_invoiceline.c_invoiceline_uu = m_rmaline.c_invoiceline_uu ' +
            ' INNER JOIN m_product ON m_product.m_product_uu  = m_rmaline.m_product_uu ' +
            ' INNER JOIN c_uom ON m_rmaline.c_uom_uu = c_uom.c_uom_uu ' +
            ' INNER JOIN c_tax ON c_invoiceline.c_tax_uu  = c_tax.c_tax_uu ' +
            ' WHERE m_rmaline.m_rmaline_uu = \'' + m_rmaline_uu + '\'';



        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new RmalineComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }

    FindRmalineComponentModelByMRmaUu(m_rma_uu) {


        const sqlTemplate = '  FROM m_rmaline ' +
            ' INNER JOIN m_rma ON m_rma.m_rma_uu = m_rmaline.m_rma_uu ' +
            ' INNER JOIN c_invoiceline ON c_invoiceline.c_invoiceline_uu = m_rmaline.c_invoiceline_uu ' +
            ' INNER JOIN m_product ON m_product.m_product_uu  = m_rmaline.m_product_uu ' +
            ' INNER JOIN c_uom ON m_rmaline.c_uom_uu = c_uom.c_uom_uu ' +
            ' INNER JOIN c_tax ON c_invoiceline.c_tax_uu  = c_tax.c_tax_uu ' +
            ' WHERE m_rmaline.wx_isdeleted = \'N\' AND m_rmaline.m_rma_uu = \'' + m_rma_uu + '\' ORDER BY m_rmaline.line ASC';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new RmalineComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }


    GenerateRmalineFromCInvoiceUu(rmaComponentModelTable) {
        let invoicelineService = new InvoicelineService();
        let locatorService = new LocatorService();


        return new Promise(function (resolve, reject) {

            invoicelineService.FindInvoicelineComponentModelByCInvoiceUu(rmaComponentModelTable.c_invoice_wx_c_invoice_uu)
                .then(function (invoicelineViewModelTable) {
                    locatorService.FindLocatorComponentModelByMWarehouseUu(rmaComponentModelTable.m_warehouse_wx_m_warehouse_uu)
                        .then(function (mLocatorTableList) {
                            let mLocatorComponentModel = mLocatorTableList[0];
                            let timeNow = new Date().getTime();
                            let promiseArray = [];
                            _.forEach(invoicelineViewModelTable, function (objectViewModelTable, index) {

                                let rmaline = new MRmaline();
                                rmaline.m_rma_uu = rmaComponentModelTable.m_rma_wx_m_rma_uu;
                                rmaline.m_rmaline_uu = HelperService.UUID();
                                rmaline.line = ++index;
                                rmaline.m_product_uu = objectViewModelTable.c_invoiceline_wx_m_product_uu;
                                rmaline.c_uom_uu = objectViewModelTable.c_invoiceline_wx_c_uom_uu;
                                rmaline.qtyentered = objectViewModelTable.c_invoiceline_wx_qtyentered;
                                rmaline.qtyinvoiceline = objectViewModelTable.c_invoiceline_wx_qtyentered;
                                rmaline.c_invoiceline_uu = objectViewModelTable.c_invoiceline_wx_c_invoiceline_uu;
                                rmaline.linenetamt = objectViewModelTable.c_invoiceline_wx_linenetamt;
                                rmaline.priceentered = objectViewModelTable.c_invoiceline_wx_priceentered;
                                rmaline.priceactual = objectViewModelTable.c_invoiceline_wx_priceactual;
                                rmaline.taxamt = objectViewModelTable.c_invoiceline_wx_taxamt;
                                rmaline.linetotalamt = objectViewModelTable.c_invoiceline_wx_linetotalamt;
                                rmaline.c_tax_uu = objectViewModelTable.c_tax_wx_c_tax_uu;
                                rmaline.m_locator_uu = mLocatorComponentModel.m_locator_wx_m_locator_uu;
                                rmaline.created = timeNow;
                                rmaline.updated = timeNow;
                                rmaline.updatedby = localStorage.getItem('ad_user_uu');
                                rmaline.createdby = localStorage.getItem('ad_user_uu');
                                rmaline.sync_client = null;
                                rmaline.process_date = null;

                                const sqlInsert = HelperService.SqlInsertOrReplaceStatement('m_rmaline', rmaline);
                                let promiseFunction = function () {

                                    return apiInterface.ExecuteSqlStatement(sqlInsert);

                                }

                                promiseArray.push(promiseFunction);


                            });

                            Promise.all(promiseArray).then(function (resultArray) {

                                insertSuccess = resultArray.length;

                                resolve(insertSuccess);

                            });


                        });



                });



        });



    }

    /**
     * Save Update harus merefer pada OrderViewModelTable
     * @param {CInvoicelineModel} invoiceline
     * @param {*} datatableReference dataTableReferemce MInout datatable
     */
    SaveUpdate(rmaline, datatableReference) {
        const timeNow = new Date().getTime();

        rmaline.updatedby = localStorage.getItem('ad_user_uu');

        rmaline.updated = timeNow;

        rmaline.sync_client = null;
        rmaline.process_date = null;

        let insertSuccess = 0;
        const sqlInsert = HelperService.SqlInsertOrReplaceStatement('m_rmaline', rmaline);

        return new Promise(function (resolve, reject) {
            apiInterface.ExecuteSqlStatement(sqlInsert)
                .then(function (rowChanged) {

                    insertSuccess += rowChanged;
                    if (insertSuccess > 0) {


                        thisObject.FindRmalineComponentModelByMRmalineUu(rmaline.m_rmaline_uu)
                            .then(function (rowDataList) {

                                let rowData = {};

                                if (rowDataList.length > 0) {
                                    rowData = rowDataList[0];
                                }



                                datatableReference.row($('#bttn_row_edit' + rowData['m_rmaline_wx_m_rmaline_uu']).parents('tr')).data(rowData).draw();

                                resolve(insertSuccess);

                            });



                    } else {
                        resolve(insertSuccess);

                    }


                });


        });


    }


}