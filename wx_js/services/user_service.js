
class UserService {

    constructor() { }


    FindAll(metaDataResponseTable) {
        let metaDataQuery;
        let searchFilter = '';
        try {
            if (metaDataResponseTable.search !== '') {
                searchFilter += ' AND ' + metaDataResponseTable.searchField + ' LIKE \'%' + metaDataResponseTable.search + '%\'';
            }

            let otherFilters = '';
            if (metaDataResponseTable.otherFilters.length > 0) {

                _.forEach(metaDataResponseTable.otherFilters, function (value) {

                    otherFilters += ' ' + value + ' ';
                });

            }

            const sqlTemplate = '  FROM ad_user ' +
                ' INNER JOIN ad_user_roles ON ad_user_roles.ad_user_uu = ad_user.ad_user_uu ' +
                ' INNER JOIN ad_role ON ad_user_roles.ad_role_uu = ad_role.ad_role_uu ' +
                ' WHERE ad_user.wx_isdeleted = \'N\' AND ad_user.ad_org_uu = \'' + metaDataResponseTable.ad_org_uu + '\'' + otherFilters + searchFilter + ' ORDER BY ad_user.name ASC ';

            const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new UserComponentModel());

            metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        } catch (err) {
            apiInterface.Log(this.constructor.name, this.FindAll.name, err);

        }


        return apiInterface.Query(metaDataQuery);

    }

    FindUserCMTListByPhone(phone) {

        phone = HelperService.ConvertPhoneWithAreaCode(phone, '62');
        let metaDataQuery;
        try {
            const sqlTemplate = '  FROM ad_user ' +
                ' INNER JOIN ad_user_roles ON ad_user_roles.ad_user_uu = ad_user.ad_user_uu ' +
                ' INNER JOIN ad_role ON ad_user_roles.ad_role_uu = ad_role.ad_role_uu ' +
                ' WHERE ad_user.phone = \'' + phone + '\'';


            const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new UserComponentModel());

            metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);

        } catch (err) {
            apiInterface.Log(this.constructor.name, this.FindUserCMTListByPhone.name, err);
        }


        return apiInterface.Query(metaDataQuery);


    }



    FindUserCMTListByAdUserUu(ad_user_uu) {
        let metaDataQuery;
        try {
            const sqlTemplate = '  FROM ad_user ' +
                ' INNER JOIN ad_user_roles ON ad_user_roles.ad_user_uu = ad_user.ad_user_uu ' +
                ' INNER JOIN ad_role ON ad_user_roles.ad_role_uu = ad_role.ad_role_uu ' +
                ' WHERE ad_user.ad_user_uu = \'' + ad_user_uu + '\'';


            const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new UserComponentModel());

            metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        } catch (err) {

            apiInterface.Log(this.constructor.name, this.FindUserCMTListByAdUserUu.name, err);
        }


        return apiInterface.Query(metaDataQuery);


    }

    /**
   * Save Update harus merefer pada OrderViewModelTable
   * @param {UserComponentModel} userCM 
   * @param {*} datatableReference dataTableReferemce MInout datatable
   */
  async  SaveUpdate(userCM, datatableReference) {
        const timeNow = new Date().getTime();
        let insertSuccess = 0;
        let isNewRecord = false;
        let thisObject = this;
        try {
            if (!userCM.ad_user.ad_user_uu) { //new record
                isNewRecord = true;
                userCM.ad_user.ad_user_uu = HelperService.UUID();
                userCM.ad_user.ad_client_uu = localStorage.getItem('ad_client_uu');
                userCM.ad_user.ad_org_uu = localStorage.getItem('ad_org_uu');

                userCM.ad_user.createdby = localStorage.getItem('ad_user_uu');
                userCM.ad_user.updatedby = localStorage.getItem('ad_user_uu');

                userCM.ad_user.created = timeNow;
                userCM.ad_user.updated = timeNow;
            } else { //update record


                userCM.ad_user.updatedby = localStorage.getItem('ad_user_uu');

                userCM.ad_user.updated = timeNow;
            }




            if (!userCM.ad_user_roles.ad_user_roles_uu) { //new record
                isNewRecord = true;
                userCM.ad_user_roles.ad_user_roles_uu = HelperService.UUID();
                userCM.ad_user_roles.ad_client_uu = localStorage.getItem('ad_client_uu');
                userCM.ad_user_roles.ad_org_uu = localStorage.getItem('ad_org_uu');

                userCM.ad_user_roles.createdby = localStorage.getItem('ad_user_uu');
                userCM.ad_user_roles.updatedby = localStorage.getItem('ad_user_uu');

                userCM.ad_user_roles.created = timeNow;
                userCM.ad_user_roles.updated = timeNow;
            } else { //update record


                userCM.ad_user_roles.updatedby = localStorage.getItem('ad_user_uu');

                userCM.ad_user_roles.updated = timeNow;
            }

            userCM.ad_user.sync_client = null;
            userCM.ad_user.process_date = null;
            userCM.ad_user.phone = HelperService.ConvertPhoneWithAreaCode(userCM.ad_user.phone, '62');



            userCM.ad_user_roles.sync_client = null;
            userCM.ad_user_roles.process_date = null;

            userCM.ad_user_roles.ad_user_uu = userCM.ad_user.ad_user_uu;
            userCM.ad_user_roles.ad_role_uu = userCM.ad_role.ad_role_uu;



            // cek jika exist maka pakai update, karena ada password user yang tidak boleh diupdate



            const sqlInsertAdUser = HelperService.SqlInsertOrReplaceStatement('ad_user', userCM.ad_user);
            const sqlInsertAdUserRoles = HelperService.SqlInsertOrReplaceStatement('ad_user_roles', userCM.ad_user_roles);

            insertSuccess += await apiInterface.ExecuteSqlStatement(sqlInsertAdUser);
            insertSuccess +=  await apiInterface.ExecuteSqlStatement(sqlInsertAdUserRoles);

            if (insertSuccess > 0) {

                if (datatableReference) {
                    let rowDataList =  await thisObject.FindUserCMTListByAdUserUu(userCM.ad_user.ad_user_uu);
                      
                            let rowData = {};

                            if (rowDataList.length > 0) {
                                rowData = rowDataList[0];
                            }

                            if (!isNewRecord) {

                                datatableReference.row($('#bttn_row_edit' + rowData['ad_user_wx_ad_user_uu']).parents('tr')).data(rowData).draw();
                            } else {

                                datatableReference.row.add(rowData).draw();
                            }

                     


                }


            }

            return insertSuccess;


           /* return new Promise(function (resolve, reject) {

                Promise.all([apiInterface.ExecuteSqlStatement(sqlInsertAdUser),
                apiInterface.ExecuteSqlStatement(sqlInsertAdUserRoles)])
                    .then(function (resultArray) {
                        insertSuccess = resultArray.length;

                        if (insertSuccess > 0) {

                            if (datatableReference) {
                                thisObject.FindUserCMTListByAdUserUu(userCM.ad_user.ad_user_uu)
                                    .then(function (rowDataList) {

                                        let rowData = {};

                                        if (rowDataList.length > 0) {
                                            rowData = rowDataList[0];
                                        }

                                        if (!isNewRecord) {

                                            datatableReference.row($('#bttn_row_edit' + rowData['ad_user_wx_ad_user_uu']).parents('tr')).data(rowData).draw();
                                        } else {

                                            datatableReference.row.add(rowData).draw();
                                        }

                                        resolve(insertSuccess);
                                    });



                            }


                        }else{
                            resolve(insertSuccess);
                        }


                    });



            });*/


        } catch (err) {
            apiInterface.Log(thisObject.constructor.name, thisObject.FindUserCMTListByAdUserUu.name, err);
        }



    }



}