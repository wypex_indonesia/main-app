
class PricelistService {

    constructor() { }

    async FindAll() {
        let rolelineService = new RolelineService();

        let notInClauseSql = await rolelineService.GetNotInClauseSql('MPricelist');


        let filterRole = '';
        if (notInClauseSql) {
            filterRole = ' WHERE m_pricelist_uu NOT IN (' + notInClauseSql + ') ';
        }
        const sqlTemplate = '  FROM m_pricelist  ' + filterRole;
        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn({ m_pricelist: new MPricelist() });

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);


        return apiInterface.Query(metaDataQuery);


    }


}
