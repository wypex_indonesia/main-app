class ElementValueService {

    constructor() { }

    ElementValueList() {
        return {
            ProductAsset: { value: '1115.1100.0000', name: 'Persediaan Barang Dagangan', accountType: 'Asset' },
            TaxCreditAR: { value: '1117.2010.0000', name: 'PPN Masukan Dibayar Dimuka', accountType: 'Asset' },
            AccountReceivableTrade: { value: '1113.1110.0000', name: 'Piutang Penjualan Barang', accountType: 'Asset' },
            NotInvoicedReceipt: { value: '2110.2130.0000', name: 'Terima Barang Belum Difaktur', accountType: 'Liability' },
            AccountPayableTrade: { value: '2110.2110.0000', name: 'Hutang Dagang', accountType: 'Liability' },
            PaymentSelection: { value: '2110.2140.0000', name: 'Seleksi Pembayaran', accountType: 'Liability' },
            CheckingInTransfer: { value: '1111.2112.0000', name: 'Bank IDR Dalam Transit', accountType: 'Asset' },
            CheckingAccount: { value: '1111.2110.0000', name: 'Bank IDR', accountType: 'Asset' },
            CoGSProduct: { value: '5101.0000.0000', name: 'HPP Produk', accountType: 'Expense' },
            TradeRevenue: { value: '4100.1000.0000', name: 'Penjualan Barang Usaha', accountType: 'Revenue' },
            TaxDue: { value: '2110.4110.0000', name: 'PPN keluaran', accountType: 'Liability' },
            CheckingUnallocatedReceipt: { value: '1111.9010.0000', name: 'Penerimaan Belum Dialokasi BANK IDR', accountType: 'Asset' },
            InventoryShrinkage: { value: '5105.0000.0000', name: 'Selisih Persediaan', accountType: 'Expense' },
            RentExpense: { value: '6160.1900.0000', name: 'Biaya Sewa Lainnya', accountType: 'Expense' },
            TransportaionExpense: { value: '6190.1150.0000', name: 'Biaya Angkut & Packing', accountType: 'Expense' },
            SalariesExpense: { value: '6110.0000.0000', name: 'Biaya Karyawan', accountType: 'Expense' },
            TelephoneExpense: { value: '6140.1140.0000', name: 'Biaya Handphone', accountType: 'Expense' },
            OtherExpense: { value: '6190.1900.0000', name: 'Biaya Umum Lain-lain', accountType: 'Expense' },
            ListrikExpense: { value: '6150.1110.0000', name: 'Biaya Listrik', accountType: 'Expense' },
            ShippingExpense: { value: '6120.1190.0000', name: 'Biaya Trucking/Ekspedisi', accountType: 'Expense' }
        }


        /*return {
            ProductAsset: { value: '14120-Product Asset', accountType: 'Asset' },
            TaxCreditAR: { value: '12610-Tax Credit A/R', accountType: 'Asset' },
            AccountReceivableTrade: { value: '12110-Accounts Receivable Trade', accountType: 'Asset' },
            NotInvoicedReceipt: { value: '21190-Not Invoiced Receipt', accountType: 'Liability' },
            AccountPayableTrade: { value: '21100-Account Payable Trade', accountType: 'Liability' },
            PaymentSelection: { value: '21300-Payment Selection', accountType: 'Liability' },
            CheckingInTransfer: { value: '11110-Checking In Transfer', accountType: 'Liability' },
            CheckingAccount: { value: '11100-Checking Account', accountType: 'Asset' },
            CoGSProduct: { value: '51100-CoGS Product', accountType: 'Expense' },
            TradeRevenue: { value: '41000-Trade Revenue', accountType: 'Revenue' },
            TaxDue: { value: '21610-Tax Due', accountType: 'Liability' },
            CheckingUnallocatedReceipt: { value: '11130-Checking Unallocated Receipt', accountType: 'Asset' },
            InventoryShrinkage: { value: '56100-Inventory Shrinkage', accountType: 'Asset' },
            RentExpense: { value: '61100-Rent Expense', accountType: 'Expense' },
            TransportaionExpense: { value: '68100-Transportation Expense', accountType: 'Expense' },
            SalariesExpense: { value: '60130-Salaries Expense', accountType: 'Expense' },
            TelephoneExpense: { value: '63100-Telephone Expense', accountType: 'Expense' },
            OtherExpense: { value: '78900-Other Expense', accountType: 'Expense' },
            ListrikExpense: { value: '72200-Listrik', accountType: 'Expense' },
            ShippingExpense: { value: '73300-Shipping Expenses', accountType: 'Expense' }
        };*/

    }

    ExpenseMapElement() {

        return {
            "Transportasi": { value: '6190.1150.0000', name: 'Biaya Angkut & Packing', accountType: 'Expense' },
            "Listrik": { value: '6150.1110.0000', name: 'Biaya Listrik', accountType: 'Expense' },
            "Gaji Karyawan": { value: '6110.0000.0000', name: 'Biaya Karyawan', accountType: 'Expense' },
            "Sewa Tempat Usaha": { value: '6160.1900.0000', name: 'Biaya Sewa Lainnya', accountType: 'Expense' },
            "Telekomunikasi": { value: '6140.1140.0000', name: 'Biaya Handphone', accountType: 'Expense' },
            "Shipping": { value: '6120.1190.0000', name: 'Biaya Trucking/Ekspedisi', accountType: 'Expense' },
            "Lain-lain": { value: '6190.1900.0000', name: 'Biaya Umum Lain-lain', accountType: 'Expense' }
        };


    }

    MovementType() {
        return {
            VendorReceipt: 'Vendor Receipt',
            VendorReceiptReverse: 'Vendor Receipt Reverse',
            CustomerOut: 'Customer Out',
            CustomerOutReverse: 'Customer Out Reverse',
            RmaReceipt: 'Customer Return Material',
            MovementFrom: 'Movement From',
            MovementFromReverse: 'Movement From Reverse',
            MovementTo: 'Movement To',
            MovementToReverse: 'Movement To Reverse',
            StockOpname: 'Stock Opname',
            Production: 'Production',
            ProductionReverse: 'Production Reverse'
        };

    }

    CostElement() {
        return {
            LastPO: 'Last PO',
            LastProduction: 'Last Production'
        };
    }


    FindAll() {




        const sqlTemplate = 'SELECT value, name, parent_value, value || \' \' || name AS text, transaction_code FROM c_elementvalue ' +
            '  ORDER BY c_elementvalue.value ASC ';


        // const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new ElementvalueComponentModel());

        // let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);


        return apiInterface.DirectQuerySqlStatement(sqlTemplate);
    }

    /* let objHierarchy = 'data' : 
    

    [
        {
            "value": 10000000, "name": "AKTIVA", "text": "10000000 AKTIVA", "children": [
                {
                    "value": 11000000, "name": "AKTIVA Lancar", "text": "11000000 AKTIVA Lancar", "children": [
                        { "value": 11110000, "name": "KAS DAN SETARA KAS", "text": "11110000 KAS DAN SETARA KAS", "children": [] }
                    ]
                },
                { "value": 11110000, "name": "AKTIVA Lancar", "text": "11000000 AKTIVA Lancar" }
            ]
        }
    ]*/
    /**
     * Penjelasan code ada di 
     * https://medium.com/@Kivylius/building-a-hierarchical-tree-from-a-list-explained-in-detail-4c4ed60d1f1c
     * @param {*} flatList 
     * @param {*} idKey 
     * @param {*} parentKey 
     * @param {*} childrenKey 
     */
    CreateHierarchyElementValue(flatList, idKey, parentKey, childrenKey) {


        const tree = [];
        const childrenOf = {};
        flatList.forEach((item) => {
            const { [idKey]: id, [parentKey]: parentId = 0 } = item;
            childrenOf[id] = childrenOf[id] || [];
            item[childrenKey] = childrenOf[id];
            parentId
                ? (
                    childrenOf[parentId] = childrenOf[parentId] || []
                ).push(item)
                : tree.push(item);
        });
        return tree;

    }

    SumUpAmount(node) {
        // internal nodes get their total from children

        if (node.amount === undefined) {
            node.amount = (numeral(node.dr).value() - numeral(node.cr).value());
        }


        if (node.children.length > 0) {
            // node['amount'] = 0;
            //node.value = 0;
            for (var i = 0; i < node.children.length; i++) {
                node.amount += this.SumUpAmount(node.children[i]);
                // node.value += SumUpAmount(node.children[i]);
            }
        }


        //return node.value;
        //let amount_dr_cr = (numeral(node.dr).value() - numeral(node.cr).value());

        return node.amount;
    }

    ConvertHierarchyToFlat(tree, resultFlatArray) {
        let thisObject = this;

        _.forEach(tree, function (obj) {
            resultFlatArray.push(obj);
            if (obj.children.length) {
                thisObject.ConvertHierarchyToFlat(obj.children, resultFlatArray);
            }


        });

        return _.uniqBy(resultFlatArray, 'value');

    }

    async  SaveUpdate(c_elementvalue) {
        const timeNow = new Date().getTime();

        let thisObject = this;
        let isNewRecord = false;
        let insertSuccess = 0;

        try {
            if (!c_elementvalue.c_elementvalue_uu) { //new record
                isNewRecord = true;
                c_elementvalue.c_elementvalue_uu = HelperService.UUID();
                c_elementvalue.ad_client_uu = localStorage.getItem('ad_client_uu');;
                c_elementvalue.ad_org_uu = localStorage.getItem('ad_org_uu');

                c_elementvalue.createdby = localStorage.getItem('ad_user_uu');
                c_elementvalue.updatedby = localStorage.getItem('ad_user_uu');

                c_elementvalue.created = timeNow;
                c_elementvalue.updated = timeNow;

            } else { //update record


                c_elementvalue.updatedby = localStorage.getItem('ad_user_uu');

                c_elementvalue.updated = timeNow;
            }



            c_elementvalue.sync_client = null;
            c_elementvalue.process_date = null;


            const sqlInsert = HelperService.SqlInsertOrReplaceStatement('c_elementvalue', c_elementvalue);
            insertSuccess += await apiInterface.ExecuteSqlStatement(sqlInsert);

            return insertSuccess;

        } catch (err) {

            apiInterface.Log(this.constructor.name, this.SaveUpdate.name, err);
            return new Promise(function (resolve, reject) {
                resolve(insertSuccess);
            });

        }



    }

    /**
     * Fungsi ini untuk mencari child dari parent yang memiliki flag trnsaction_Code, dan saat ini dipergunakan
     * untuk mencari bank dan cash_pos, child element, 
     * @param {*} transaction_code 
     */
    FindChildFromTransactionCode(transaction_code) {

      
        let metaDataQuery;
        try {


            const sqlTemplate = '  FROM c_elementvalue ' +
                ' WHERE parent_value = (SELECT value FROM c_elementvalue WHERE transaction_code = \'' + transaction_code + '\') ORDER BY c_elementvalue.value ASC';

            const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new ElementvalueComponentModel());

            metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);

        } catch (err) {

            apiInterface.Log(this.constructor.name, this.FindPettyCash.name, err);

        }
        return apiInterface.Query(metaDataQuery);

    }

    FindElementByTransactionCode(transaction_code) {
        let metaDataQuery;
        try {


            const sqlTemplate = '  FROM c_elementvalue ' +
                ' WHERE c_elementvalue.transaction_code = \'' + transaction_code + '\'';

            const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new ElementvalueComponentModel());

            metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);

        } catch (err) {

            apiInterface.Log(this.constructor.name, this.FindPettyCash.name, err);

        }
        return apiInterface.Query(metaDataQuery);

    }



}