
class RmaService {
    constructor() {

    }

    /**
	 * 
	 * @param {*} metaDataResponseTable 
	 */
    FindAll(metaDataResponseTable) {


        let otherFilters = '';
        if (metaDataResponseTable.otherFilters.length > 0) {

            _.forEach(metaDataResponseTable.otherFilters, function (value) {

                otherFilters += ' ' + value + ' ';
            });

        }

        const sqlTemplate = '  FROM m_rma ' +
            ' INNER JOIN c_bpartner ON m_rma.c_bpartner_uu = c_bpartner.c_bpartner_uu ' +
            ' INNER JOIN m_warehouse ON m_rma.m_warehouse_uu = m_warehouse.m_warehouse_uu ' +
            ' INNER JOIN c_invoice ON c_invoice.c_invoice_uu = m_rma.c_invoice_uu ' +
            ' INNER JOIN c_doctype ON m_rma.c_doctype_id = c_doctype.c_doctype_id ' +
            ' WHERE m_rma.wx_isdeleted = \'N\' ' + otherFilters + ' ORDER BY m_rma.documentno DESC ';


        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new RmaComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);


        return apiInterface.Query(metaDataQuery);
    }

    FindRmaComponentModelByMRmaUu(m_rma_uu) {

        const sqlTemplate = '  FROM m_rma ' +
            ' INNER JOIN c_bpartner ON m_rma.c_bpartner_uu = c_bpartner.c_bpartner_uu ' +
            ' INNER JOIN m_warehouse ON m_rma.m_warehouse_uu = m_warehouse.m_warehouse_uu ' +
            ' INNER JOIN c_invoice ON c_invoice.c_invoice_uu = m_rma.c_invoice_uu ' +
            ' INNER JOIN c_doctype ON m_rma.c_doctype_id = c_doctype.c_doctype_id ' +
            ' WHERE m_rma.m_rma_uu = \'' + m_rma_uu + '\' ';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new RmaComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);


        return apiInterface.Query(metaDataQuery);
    }

    UpdateStatus(rmaComponentModelTable) {
        let insertSuccess = 0;
        let sqlstatement = '';
        if (rmaComponentModelTable.m_rma_wx_docstatus === 'DELETED') {
            sqlstatement = HelperService.SqlDeleteStatement('m_rma', 'm_rma_uu', rmaComponentModelTable.m_rma_wx_m_rma_uu);

        } else {


            sqlstatement = HelperService.SqlUpdateStatusStatement('m_rma', 'm_rma_uu',
                rmaComponentModelTable.m_rma_wx_m_rma_uu, rmaComponentModelTable.m_rma_wx_docstatus);
        }

        return new Promise(function (resolve, reject) {

            apiInterface.ExecuteSqlStatement(sqlstatement)
                .then(function (rowChanged) {

                    insertSuccess += rowChanged;
                    resolve(insertSuccess);
                });



        });

    }

    /**
     * 
     * @param {*} rmaComponentModel 
     * @param {*} datatableReference dataTableReferemce CInvoice datatable
     */
    SaveUpdate(rmaComponentModel, datatableReference) {
        const timeNow = new Date().getTime();
        let thisObject = this;
        let isNewRecord = false;
        if (!rmaComponentModel.m_rma.m_rma_uu) { //new record
            isNewRecord = true;
            rmaComponentModel.m_rma.m_rma_uu = HelperService.UUID();
            rmaComponentModel.m_rma.ad_client_uu = localStorage.getItem('ad_client_uu');
            rmaComponentModel.m_rma.ad_org_uu = localStorage.getItem('ad_org_uu');

            rmaComponentModel.m_rma.createdby = localStorage.getItem('ad_user_uu');
            rmaComponentModel.m_rma.updatedby = localStorage.getItem('ad_user_uu');

            rmaComponentModel.m_rma.created = timeNow;
            rmaComponentModel.m_rma.updated = timeNow;
        } else { //update record


            rmaComponentModel.m_rma.updatedby = localStorage.getItem('ad_user_uu');

            rmaComponentModel.m_rma.updated = timeNow;
        }


        rmaComponentModel.m_rma.c_invoice_uu = rmaComponentModel.c_invoice.c_invoice_uu;
        rmaComponentModel.m_rma.c_bpartner_uu = rmaComponentModel.c_bpartner.c_bpartner_uu;
        rmaComponentModel.m_rma.m_warehouse_uu = rmaComponentModel.m_warehouse.m_warehouse_uu;
        rmaComponentModel.m_rma.c_doctype_id = rmaComponentModel.c_doctype.c_doctype_id;
        rmaComponentModel.m_rma.sync_client = null;
        rmaComponentModel.m_rma.process_date = null;

        let insertSuccess = 0;
        const sqlInsert = HelperService.SqlInsertOrReplaceStatement('m_rma', rmaComponentModel.m_rma);

        return new Promise(function (resolve, reject) {
            apiInterface.ExecuteSqlStatement(sqlInsert)
                .then(function (rowChanged) {
                    insertSuccess += rowChanged;
                    if (insertSuccess > 0) {


                        thisObject.FindRmaComponentModelByMRmaUu(rmaComponentModel.m_rma.m_rma_uu)
                            .then(function (rowDataList) {
                                let rowData = {};

                                if (rowDataList.length > 0) {
                                    rowData = rowDataList[0];
                                }

                                if (!isNewRecord) {

                                    datatableReference.row($('#bttn_row_edit' + rowData['m_rma_wx_m_rma_uu']).parents('tr')).data(rowData).draw();
                                } else {

                                    datatableReference.row.add(rowData).draw();
                                }

                                resolve(insertSuccess);

                            });



                    } else {
                        resolve(insertSuccess);
                    }


                })



        });


    }





    /**
     * Run Process Document sesuai dengan docstatus :
     * 1. Draft -> Tidak ada Action
     * 2. COMPLETED
     *  1. Ambil rmaline
     *  1. MTransaction dan factacct Minout
     *  2. Factacct Invoiceline
     *  3. Factacct Payment 
     *  4. Factacct BankStatement   
     * 3. REVERSED
     *  2.1. FactAcct -> Reversed
     * 
     * @param {*} rmaComponentModelTable 
   
    RunProcessDocument(rmaComponentModelTable) {

    
        let factacctService = new FactAcctService();
     
        let materialTransactionService = new MaterialTransactionService();
        let rmalineService = new RmalineService();
        if (rmaComponentModelTable.m_rma_wx_docstatus === 'COMPLETED') {

            let rmalineComponentModelTableList = rmalineService.FindRmalineComponentModelByMRmaUu(rmaComponentModelTable.m_rma_wx_m_rma_uu);
            _.forEach(rmalineComponentModelTableList, function(rmalineComponentModelTable){
                materialTransactionService.RmaReceiptCompleted(rmalineComponentModelTable);
                factacctService.RmaCompleted(rmalineComponentModelTableList);
            });

           


        }

       // if (rmaComponentModelTable.m_rma_wx_docstatus === 'REVERSED') {

      //  }


    }  */




}