

class InvoicelineService {
    constructor() { }


    /**
	 * 
	 * @param {*} metaDataResponseTable 
	 */
    FindAll(metaDataResponseTable) {


        let otherFilters = '';
        if (metaDataResponseTable.otherFilters.length > 0) {

            _.forEach(metaDataResponseTable.otherFilters, function (value) {

                otherFilters += ' ' + value + ' ';
            });

        }

        const sqlTemplate = '  c_invoiceline ' +
            ' INNER JOIN c_invoice ON c_invoiceline.c_invoice_uu = c_invoice.c_invoice_uu ' +
            ' INNER JOIN m_product ON m_product.m_product_uu  = c_invoiceline.m_product_uu ' +
            ' INNER JOIN c_uom ON c_invoiceline.c_uom_uu = c_uom.c_uom_uu ' +
            ' INNER JOIN c_uom as c_uom_based ON c_uom_based.c_uom_uu = m_product.c_uom_uu ' +
            ' INNER JOIN c_orderline ON c_orderline.c_orderline_uu = c_invoiceline.c_orderline_uu ' +
            ' INNER JOIN c_tax ON c_invoiceline.c_tax_uu  = c_tax.c_tax_uu ' +
            ' WHERE c_invoiceline.wx_isdeleted = \'N\' ' + otherFilters + ' ORDER BY c_invoiceline.line ASC ';



        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new InvoicelineComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);

    }

    FindInvoicelineComponentModelByCInvoicelineUu(m_invoiceline_uu) {

        const sqlTemplate = '  FROM c_invoiceline ' +
            ' INNER JOIN c_invoice ON c_invoiceline.c_invoice_uu = c_invoice.c_invoice_uu ' +
            ' INNER JOIN m_product ON m_product.m_product_uu  = c_invoiceline.m_product_uu ' +
            ' INNER JOIN c_uom ON c_invoiceline.c_uom_uu = c_uom.c_uom_uu ' +
            ' INNER JOIN c_uom as c_uom_based ON c_uom_based.c_uom_uu = m_product.c_uom_uu ' +
            ' INNER JOIN c_orderline ON c_orderline.c_orderline_uu = c_invoiceline.c_orderline_uu ' +
            ' INNER JOIN c_tax ON c_invoiceline.c_tax_uu  = c_tax.c_tax_uu ' +
            ' WHERE c_invoiceline.c_invoiceline_uu = \'' + m_invoiceline_uu + '\'';



        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new InvoicelineComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }

    FindInvoicelineCMTListByListCInvoicelineUu(list_c_invoiceline_uu) {


        let arrayStringCInvoicelineUu = [];
        _.forEach(list_c_invoiceline_uu, function (c_invoiceline_uu) {

            let c_invoiceline_uu_string = '\'' + c_invoiceline_uu + '\'';
            arrayStringCInvoicelineUu.push(c_invoiceline_uu_string);
        });

        let inClause = '(' + arrayStringCInvoicelineUu.join(',') + ')';
        const sqlTemplate = '  FROM c_invoiceline ' +
            ' INNER JOIN c_invoice ON c_invoiceline.c_invoice_uu = c_invoice.c_invoice_uu ' +
            ' INNER JOIN m_product ON m_product.m_product_uu  = c_invoiceline.m_product_uu ' +
            ' INNER JOIN c_uom ON c_invoiceline.c_uom_uu = c_uom.c_uom_uu ' +
            ' INNER JOIN c_uom as c_uom_based ON c_uom_based.c_uom_uu = m_product.c_uom_uu ' +
            ' INNER JOIN c_orderline ON c_orderline.c_orderline_uu = c_invoiceline.c_orderline_uu ' +
            ' INNER JOIN c_tax ON c_invoiceline.c_tax_uu  = c_tax.c_tax_uu ' +
            ' WHERE c_invoiceline.c_invoiceline_uu IN ' + inClause;



        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new InvoicelineComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }

    FindInvoicelineComponentModelByCInvoiceUu(c_invoice_uu) {

        const sqlTemplate = '  FROM c_invoiceline ' +
            ' INNER JOIN c_invoice ON c_invoiceline.c_invoice_uu = c_invoice.c_invoice_uu ' +
            ' INNER JOIN m_product ON m_product.m_product_uu  = c_invoiceline.m_product_uu ' +
            ' INNER JOIN c_uom ON c_invoiceline.c_uom_uu = c_uom.c_uom_uu ' +
            ' INNER JOIN c_uom as c_uom_based ON c_uom_based.c_uom_uu = m_product.c_uom_uu ' +
            ' INNER JOIN c_orderline ON c_orderline.c_orderline_uu = c_invoiceline.c_orderline_uu ' +
            ' INNER JOIN c_tax ON c_invoiceline.c_tax_uu  = c_tax.c_tax_uu ' +
            ' WHERE c_invoiceline.wx_isdeleted = \'N\' AND c_invoiceline.c_invoice_uu = \'' + c_invoice_uu + '\' ORDER BY c_invoiceline.line ASC';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new InvoicelineComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }


    GenerateInvoicelineFromCOrderUu(invoiceComponentModelTable) {
        let thisObject = this;
        let orderlineService = new OrderlineService();
        let timeNow = new Date().getTime();
        let dataFlow = {};

        return new Promise(function (resolve, reject) {
            orderlineService.FindOrderlineViewModelByCOrderUu(invoiceComponentModelTable.c_order_wx_c_order_uu)
                .then(function (orderlineCMTList) {

                    dataFlow.orderline_cmt_list = orderlineCMTList;
                    resolve(dataFlow);
                });
        }).then(function (dataFlow) {
            return new Promise(function (resolve, reject) {
                let sqlInsertArray = [];
                _.forEach(dataFlow.orderline_cmt_list, function (objectViewModelTable, index) {

                    let invoiceline = new CInvoiceline();
                    invoiceline.c_invoice_uu = invoiceComponentModelTable.c_invoice_wx_c_invoice_uu;
                    invoiceline.c_invoiceline_uu = HelperService.UUID();
                    invoiceline.line = ++index;
                    invoiceline.m_product_uu = objectViewModelTable.c_orderline_wx_m_product_uu;
                    invoiceline.c_uom_uu = objectViewModelTable.c_orderline_wx_c_uom_uu;
                    invoiceline.qtyentered = objectViewModelTable.c_orderline_wx_qtyentered;
                    invoiceline.qtyinvoiced = objectViewModelTable.c_orderline_wx_qtyordered;
                    invoiceline.c_orderline_uu = objectViewModelTable.c_orderline_wx_c_orderline_uu;
                    invoiceline.linenetamt = objectViewModelTable.c_orderline_wx_linenetamt;
                    invoiceline.priceentered = objectViewModelTable.c_orderline_wx_priceentered;
                    invoiceline.priceactual = objectViewModelTable.c_orderline_wx_priceactual;
                    invoiceline.taxamt = objectViewModelTable.c_orderline_wx_linetaxamt;
                    invoiceline.linetotalamt = numeral(objectViewModelTable.c_orderline_wx_linenetamt).value()
                        + numeral(objectViewModelTable.c_orderline_wx_linetaxamt).value();
                    invoiceline.c_tax_uu = objectViewModelTable.c_tax_wx_c_tax_uu;
                    invoiceline.created = timeNow;
                    invoiceline.updated = timeNow;
                    invoiceline.updatedby = localStorage.getItem('ad_user_uu');
                    invoiceline.createdby = localStorage.getItem('ad_user_uu');
                    invoiceline.sync_client = null;
                    invoiceline.process_date = null;
                    invoiceline.isreturn = objectViewModelTable.c_orderline_wx_isreturn;

                    const sqlInsert = thisObject.GetRawInsertSqlStatement(invoiceline);
                    sqlInsertArray.push(sqlInsert);

                });

                apiInterface.ExecuteBulkSqlStatement(sqlInsertArray).then(function (insertSuccess) {
                    dataFlow.insert_success = insertSuccess;
                    resolve(dataFlow);
                });

            });


        }).then(function (dataFlow) {

            return new Promise(function (resolve, reject) {
                let invoice_service = new InvoiceService();
                invoice_service.CountAllTotal(invoiceComponentModelTable.c_invoice_wx_c_invoice_uu).then(function (inserted) {
                    dataFlow.insert_success += inserted;
                    resolve(dataFlow.insert_success);

                });

            });


        });



    }

    /**
     * Save Update harus merefer pada OrderViewModelTable
     * @param {CInvoicelineModel} invoiceline
     * @param {*} datatableReference dataTableReferemce MInout datatable
     */
    SaveUpdate(invoiceline, datatableReference) {
        let thisObject = this;
        let insertSuccess = 0;
        const timeNow = new Date().getTime();
        try {
            invoiceline.updatedby = localStorage.getItem('ad_user_uu');

            invoiceline.updated = timeNow;

            invoiceline.sync_client = null;
            invoiceline.process_date = null;


            const sqlInsert = thisObject.GetRawInsertSqlStatement(invoiceline);

            return new Promise(function (resolve, reject) {


                apiInterface.ExecuteSqlStatement(sqlInsert).then(function (rowChanged) {
                    insertSuccess += rowChanged;
                    if (insertSuccess > 0) {


                        thisObject.FindInvoicelineComponentModelByCInvoicelineUu(invoiceline.c_invoiceline_uu)
                            .then(function (rowDataList) {

                                let rowData = {};

                                if (rowDataList.length > 0) {
                                    rowData = rowDataList[0];
                                }

                                datatableReference.row($('#bttn_row_edit' + rowData['c_invoiceline_wx_c_invoiceline_uu']).parents('tr')).data(rowData).draw();

                                let invoice_service = new InvoiceService();
                                invoice_service.CountAllTotal(invoiceline.c_invoice_uu);

                                resolve(insertSuccess);
                            });


                    } else {
                        resolve(insertSuccess);
                    }
                });





            });

        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.SaveUpdate.name, error);
            return new Promise(function (resolve, reject) {

                resolve(insertSuccess);
            });
        }




    }


    /**
   * Semua fungsi yang akan mengenerate sql insert statemnent untuk invoiceline, harus menggunakan fungsi ini
   * karena isreturn , linenetamt, taxamt, grandtotal  harus negatif
   * @param {*} c_invoiceline 
   */
    GetRawInsertSqlStatement(c_invoiceline) {

        // maka cek apakah sudah negatif, kalau sudah negatif, tidak perlu diubah
        if (c_invoiceline.isreturn === 'Y') {
            if (c_invoiceline.linenetamt > 0) {
                c_invoiceline.linenetamt = numeral(c_invoiceline.linenetamt).value() * -1;
            }
            if (c_invoiceline.taxamt > 0) {
                c_invoiceline.taxamt = numeral(c_invoiceline.taxamt).value() * -1;
            }
            if (c_invoiceline.linetotalamt > 0) {
                c_invoiceline.linetotalamt = numeral(c_invoiceline.linetotalamt).value() * -1;
            }
        }

        return HelperService.SqlInsertOrReplaceStatement('c_invoiceline', c_invoiceline);


    }


}