/**

 */
class ProductionService {
    constructor() {

    }

    /**
	 * 
	 * @param {*} metaDataResponseTable 
	 */
    FindAll(metaDataResponseTable) {


        let otherFilters = '';
        if (metaDataResponseTable.otherFilters.length > 0) {

            _.forEach(metaDataResponseTable.otherFilters, function (value) {

                otherFilters += ' ' + value + ' ';
            });

        }

        const sqlTemplate = '  FROM m_production ' +
            ' INNER JOIN m_warehouse ON m_production.m_warehouse_uu = m_warehouse.m_warehouse_uu ' +
            ' INNER JOIN m_product ON m_product.m_product_uu = m_production.m_product_uu ' +
            ' INNER JOIN c_uom ON c_uom.c_uom_uu = m_product.c_uom_uu ' +
            ' WHERE m_production.wx_isdeleted = \'N\' ' + otherFilters + ' ORDER BY m_production.documentno DESC ';


        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new ProductionComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);


        return apiInterface.Query(metaDataQuery);
    }

    FindProductionCMTListByMProductionUu(m_production_uu) {

        const sqlTemplate = '  FROM m_production ' +
            ' INNER JOIN m_warehouse ON m_production.m_warehouse_uu = m_warehouse.m_warehouse_uu ' +
            ' INNER JOIN m_product ON m_product.m_product_uu = m_production.m_product_uu ' +
            ' INNER JOIN c_uom ON c_uom.c_uom_uu = m_product.c_uom_uu ' +
            ' WHERE m_production.m_production_uu = \'' + m_production_uu + '\' ';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new ProductionComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);
        let rowDataList = apiInterface.Query(metaDataQuery);


        return rowDataList;
    }

    GetProductionCMTByMProductionUu(m_production_uu) {

        const sqlTemplate = '  FROM m_production ' +
            ' INNER JOIN m_warehouse ON m_production.m_warehouse_uu = m_warehouse.m_warehouse_uu ' +
            ' INNER JOIN m_product ON m_product.m_product_uu = m_production.m_product_uu ' +
            ' INNER JOIN c_uom ON c_uom.c_uom_uu = m_product.c_uom_uu ' +
            ' WHERE m_production.m_production_uu = \'' + m_production_uu + '\' ';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new ProductionComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);
        let rowDataList = apiInterface.Query(metaDataQuery);
        let dataCMT = null;
        if (rowDataList.length > 0) {
            dataCMT = rowDataList[0];
        }

        return dataCMT;
    }

    UpdateStatus(productionCMT) {
        let insertSuccess = 0;
        let sqlstatement = '';
        let thisObject = this;
        if (productionCMT.m_production_wx_docstatus === 'DELETED') {
            sqlstatement = HelperService.SqlDeleteStatement('m_production', 'm_production_uu', productionCMT.m_production_wx_m_production_uu);

        } else {

            sqlstatement = HelperService.SqlUpdateStatusStatement('m_production', 'm_production_uu',
                productionCMT.m_production_wx_m_production_uu, productionCMT.m_production_wx_docstatus);
        }

        return new Promise(function (resolve, reject) {

            apiInterface.ExecuteSqlStatement(sqlstatement)
                .then(function (rowChanged) {
                    insertSuccess += rowChanged
                    if (insertSuccess) {
                        thisObject.RunProcessDocument(productionCMT).then(function (inserted) {
                            insertSuccess += inserted;
                            resolve(insertSuccess);
                        });
                    } else {

                        resolve(insertSuccess);
                    }



                });


        });

    }

    /**
     * 
     * JIka firstProductionqty tidak sama dengan productionCM atau artinya ada update, maka
     * productionline harus diupdate 
     * rumus : Bomlast = qtyLast/qtyfirst * bomfirst;
     * 
     * @param {*} firstProductionqty , productionQty, ketika belum diupdate, gunanya untuk menghitung productionline 
     * @param {*} productionCM 
     * @param {*} datatableReference dataTableReferemce CInvoice datatable
     */
    async SaveUpdate(firstProductionqty, productionCM, datatableReference) {
        let thisObject = this;
        const timeNow = new Date().getTime();
        let insertSuccess = 0;

        let isNewRecord = false;

        try {
            if (!productionCM.m_production.m_production_uu) { //new record
                isNewRecord = true;
                productionCM.m_production.m_production_uu = HelperService.UUID();
                productionCM.m_production.ad_client_uu = localStorage.getItem('ad_client_uu');
                productionCM.m_production.ad_org_uu = localStorage.getItem('ad_org_uu');

                productionCM.m_production.createdby = localStorage.getItem('ad_user_uu');
                productionCM.m_production.updatedby = localStorage.getItem('ad_user_uu');

                productionCM.m_production.created = timeNow;
                productionCM.m_production.updated = timeNow;
            } else { //update record


                productionCM.m_production.updatedby = localStorage.getItem('ad_user_uu');

                productionCM.m_production.updated = timeNow;
            }


            productionCM.m_production.c_doctype_id = 9000; // document production
            productionCM.m_production.m_product_uu = productionCM.m_product.m_product_uu;
            productionCM.m_production.m_warehouse_uu = productionCM.m_warehouse.m_warehouse_uu;

            productionCM.m_production.sync_client = null;
            productionCM.m_production.process_date = null;


            const sqlInsert = HelperService.SqlInsertOrReplaceStatement('m_production', productionCM.m_production);

            insertSuccess += await apiInterface.ExecuteSqlStatement(sqlInsert);

            if ((firstProductionqty) && (productionCM.m_production.productionqty)) {
                let productionlineService = new ProductionlineService();
                await productionlineService.UpdateMovementQty(productionCM.m_production.productionqty / firstProductionqty, productionCM.m_production.m_production_uu);
            }

            if (insertSuccess > 0) {


                let rowDataList = await thisObject.FindProductionCMTListByMProductionUu(productionCM.m_production.m_production_uu);
                let rowData = rowDataList[0];

                if (!isNewRecord) {

                    datatableReference.row($('#bttn_row_edit' + rowData['m_production_wx_m_production_uu']).parents('tr')).data(rowData).draw();
                } else {

                    datatableReference.row.add(rowData).draw();
                }


            }

        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.SaveUpdate.name, error);
        }

        return insertSuccess;

    }





    /**
     * Run Process Document sesuai dengan docstatus :
     * 1. Draft -> Tidak ada Action
     * docbasetype :
     * AR Invoice
     * 
     * AP Invoice
     * 2. COMPLETED
     *  2.2. FactAcct 
     * 3. REVERSED
     *  2.1. FactAcct -> Reversed
     * 
     * @param {*} productionCMT 
     */
    RunProcessDocument(productionCMT) {
        let thisObject = this;


        if (productionCMT.m_production_wx_docstatus === 'COMPLETED') {
            return thisObject.ProductionCompleted(productionCMT);

        }



        if (productionCMT.m_production_wx_docstatus === 'REVERSED') {

            return thisObject.ProductionReversed(productionCMT);



        }



    }


    ProductionReversed(productionCMT){


        let thisObject = this;
        let insertSuccess = 0;
        let elementValueService = new ElementValueService();
        let materialTransactionService = new MaterialTransactionService();
        let productionlineService = new ProductionlineService();
       

        let dataflow = {};
        return new Promise(function (resolve, reject) {

          
            productionlineService.FindProductionlineCMTListByMProductionUu(productionCMT.m_production_wx_m_production_uu)
            .then(function (productionlineCMTList) {

               
                dataflow.productionline_cmt_list = productionlineCMTList;
                resolve(dataflow);
            });


        }).then(function (dataflow) {

            return new Promise(function (resolve, reject) {

              
                let promiseArray = [];
                _.forEach(dataflow.productionline_cmt_list, function (productionlineCMT) {
                   
                    promiseArray.push(materialTransactionService.sqlInsertReverseMTransaction(elementValueService.MovementType().ProductionReverse,
                    elementValueService.MovementType().Production, 'm_productionline', productionlineCMT.m_productionline_wx_m_productionline_uu));
                   
                });

          
                // stock plus
                promiseArray.push(materialTransactionService.sqlInsertReverseMTransaction(elementValueService.MovementType().ProductionReverse,
                elementValueService.MovementType().Production, 'm_production', productionCMT.m_production_wx_m_production_uu));
               


                Promise.all(promiseArray).then(function (resultSuccess) {

                    _.forEach(resultSuccess, function (inserted) {
                        insertSuccess += inserted;

                    });
                    resolve(insertSuccess);

                });



            });


        });


    }


    ProductionCompleted(productionCMT) {
        let thisObject = this;
        let insertSuccess = 0;
        let elementValueService = new ElementValueService();
        let materialTransactionService = new MaterialTransactionService();
        let productionlineService = new ProductionlineService();
        let mLocatorService = new LocatorService();
        let factacctService = new FactAcctService();
        let costService = new CostService();


        let dataflow = {};
        return new Promise(function (resolve, reject) {

            Promise.all([mLocatorService.FindLocatorComponentModelByMWarehouseUu(productionCMT.m_warehouse_wx_m_warehouse_uu),
            productionlineService.FindProductionlineCMTListByMProductionUu(productionCMT.m_production_wx_m_production_uu)
            ]).then(function (resultArray) {

                let locatorCMTList = resultArray[0];
                let locatorCMT = locatorCMTList[0];
                let productionlineCMTList = resultArray[1];

                dataflow.locator_cmt = locatorCMT;
                dataflow.productionline_cmt_list = productionlineCMTList;
                resolve(dataflow);
            });


        }).then(function (dataflow) {

            return new Promise(function (resolve, reject) {

                let mapTotalCostMProductUu = {};
                let promiseArray = [];
                _.forEach(dataflow.productionline_cmt_list, function (productionlineCMT) {


                    promiseArray.push(costService.GetTotalCostByMProductUu(productionlineCMT.m_productionline_wx_m_product_uu));

                });

                Promise.all(promiseArray).then(function (resultArray) {

                    _.forEach(resultArray, function (result, i) {

                        let productionlineCMT = dataflow.productionline_cmt_list[i];
                        mapTotalCostMProductUu[productionlineCMT.m_productionline_wx_m_product_uu] = result;

                    });

                    dataflow.map_m_product_uu_totalcost = mapTotalCostMProductUu;
                    resolve(dataflow);
                });


            });


        }).then(function (dataflow) {

            return new Promise(function (resolve, reject) {

                let sqlInsertArray = [];
                let totalCoGSProduction = 0.0;
                let promiseArray = [];
                _.forEach(dataflow.productionline_cmt_list, function (productionlineCMT) {
                    let productAssetOne = dataflow.map_m_product_uu_totalcost[productionlineCMT.m_productionline_wx_m_product_uu];
                    let totalProductAsset = numeral(productAssetOne).value() * numeral(productionlineCMT.m_productionline_wx_movementqty).value();
                    //CR PRODUCT ASSET 
                    //transaksi production dalam fact_acct masih belum clear, ketika ada componen jasa bagaimana
                    // harus di cek di idempiere dulu, sementara tidak usah dibuatkan jurnal
                   /* let sqlInsertCR = factacctService.sqlInsertFactAcctStatement(elementValueService.ElementValueList().ProductAsset, 'm_productionline',
                        productionlineCMT.m_productionline_wx_m_productionline_uu, null, totalProductAsset);

                    sqlInsertArray.push(sqlInsertCR);*/

                    //stock minus
                    promiseArray.push(materialTransactionService.sqlInsertMTransaction(elementValueService.MovementType().Production,
                        dataflow.locator_cmt.m_locator_wx_m_locator_uu, 'm_productionline', productionlineCMT.m_productionline_wx_m_productionline_uu,
                        productionlineCMT.m_product_wx_m_product_uu, productionlineCMT.m_productionline_wx_movementqty, true));
                    totalCoGSProduction += numeral(totalProductAsset).value();
                });

                dataflow.total_cogs_production = totalCoGSProduction;


                // stock plus
                promiseArray.push(materialTransactionService.sqlInsertMTransaction(elementValueService.MovementType().Production,
                    dataflow.locator_cmt.m_locator_wx_m_locator_uu, 'm_production', productionCMT.m_production_wx_m_production_uu,
                    productionCMT.m_product_wx_m_product_uu, productionCMT.m_production_wx_productionqty, false));


                //DR ProductAsset
               /* let insertSqlDR = factacctService.sqlInsertFactAcctStatement(elementValueService.ElementValueList().ProductAsset, 'm_production', productionCMT.m_production_wx_m_production_uu, totalCoGSProduction, null);
                sqlInsertArray.push(insertSqlDR);

                promiseArray.push(apiInterface.ExecuteBulkSqlStatement(sqlInsertArray));*/

                Promise.all(promiseArray).then(function (resultSuccess) {

                    _.forEach(resultSuccess, function (inserted) {
                        insertSuccess += inserted;

                    });
                    resolve(insertSuccess);

                });



            });


        }).then(function (dataflow) {

            return new Promise(function (resolve, reject) {


                /**
                 * COGS Product pada production adalah total COGS Productionline / productionqty
                 * Set m_cost pada production
                 */


                let m_cost = new MCost();
                let productAsset = dataflow.total_cogs_production;
                let CogsEndProduct = numeral(productAsset).value() / numeral(productionCMT.m_production_wx_productionqty).value();


                m_cost.currentCostprice = CogsEndProduct;
                m_cost.m_costelement = elementValueService.CostElement().LastProduction;
                // save m_cost
                costService.SaveUpdate(m_cost).then(function (inserted) {

                    insertSuccess += inserted;
                    resolve(insertSuccess);
                });





            });

        });



    }




}