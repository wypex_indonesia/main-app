class ShipperService {
    constructor() { }

    /**
 * Find All warehouse dengan bentuk warehouseviewmodel
 * @param {*} metaDataResponseTable 
 */
    FindAll(metaDataResponseTable) {


        let searchFilter = '';
        if (metaDataResponseTable.search !== '') {
            searchFilter += ' AND ' + metaDataResponseTable.searchField + '  LIKE    \'%' + metaDataResponseTable.search + '%\'';
        }

        const sqlTemplate = '  FROM m_shipper ' +
            ' WHERE m_shipper.wx_isdeleted = \'N\' AND m_shipper.ad_org_uu = \'' + metaDataResponseTable.ad_org_uu + '\'' + searchFilter + ' ORDER BY m_shipper.updated DESC ';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new ShipperComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }

    FindShipperComponentModelByMShipperUu(m_shipper_uu) {

        const sqlTemplate = '  FROM m_shipper ' +
            ' WHERE  m_shipper.m_shipper_uu = \'' + m_shipper_uu + '\'';


        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new ShipperComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }

    /**
     * Create Shipper pada saat AdOrg Dibuat baru
     * @param {*} ad_org_uu 
     */
    async  CreateShipperDefault() {

        let thisObject = this;

        try {
            let shipperDefault = ["Gojek", "Grab", "SiCepat", "J&T Express", "Lion Parcel", "Ninja Express", "Paxel",
                "Wahana Express", "JNE Express", "TIKI", "Alfa Trex", "Pos Indonesia", "RPX", "Lain - lain", "Pickup"];

            let insertSuccess = 0;

            let bulkSqlStatement = [];
            _.forEach(shipperDefault, function (element) {
                let shipperComponentModel = new ShipperComponentModel();
                shipperComponentModel.m_shipper.name = element;
                let insertSqlStatement = thisObject.GetInsertUpdateSqlStatement(shipperComponentModel);
                bulkSqlStatement.push(insertSqlStatement);

            });

            insertSuccess += await apiInterface.ExecuteBulkSqlStatement(bulkSqlStatement);
            apiInterface.Debug("create shipper default CreateShipperDefault");

            return insertSuccess;
        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.CreateShipperDefault.name, error)
            return 0;
        }




    }



    /**
* Save Update harus merefer pada OrderViewModelTable
* @param {*} shipperComponentModel 
 
*/
    async  SaveUpdate(shipperComponentModel, datatableReference) {
        let thisObject = this;
        let insertSuccess = 0;
        try {

            let isNewRecord = false;
            if (!shipperComponentModel.m_shipper.m_shipper_uu) {
                isNewRecord = true;
            }

            let sqlInsertShipper = thisObject.GetInsertUpdateSqlStatement(shipperComponentModel);

            insertSuccess += await apiInterface.ExecuteSqlStatement(sqlInsertShipper);


            // jika sukses tersimpan 
            if (insertSuccess > 0) {

                if (datatableReference) {

                    let shipperCMTList = await thisObject.FindShipperComponentModelByMShipperUu(shipperComponentModel.m_shipper.m_shipper_uu);

                    let rowData = shipperCMTList[0];
                    if (!isNewRecord) {

                        datatableReference.row($('#bttn_row_edit' + rowData['m_shipper_wx_m_shipper_uu']).parents('tr')).data(rowData).draw();
                    } else {

                        datatableReference.row.add(rowData).draw();
                    }
                    $('#shipper-form').modal('hide');

                }



            }


            return insertSuccess;



        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.SaveUpdate.name, error);
        }

    }

    GetInsertUpdateSqlStatement(shipperComponentModel) {
        let thisObject = this;
        try {
            const timeNow = new Date().getTime();
            let isNewRecord = false;

            if (!shipperComponentModel.m_shipper.m_shipper_uu) {
                isNewRecord = true;
                shipperComponentModel.m_shipper.m_shipper_uu = HelperService.UUID();
                shipperComponentModel.m_shipper.ad_client_uu = localStorage.getItem('ad_client_uu');
                shipperComponentModel.m_shipper.ad_org_uu = localStorage.getItem('ad_org_uu');

                shipperComponentModel.m_shipper.createdby = localStorage.getItem('ad_user_uu');
                shipperComponentModel.m_shipper.updatedby = localStorage.getItem('ad_user_uu');


                shipperComponentModel.m_shipper.created = timeNow;
                shipperComponentModel.m_shipper.updated = timeNow;
            } else { //update record

                shipperComponentModel.m_shipper.updatedby = localStorage.getItem('ad_user_uu');


                shipperComponentModel.m_shipper.updated = timeNow;
            }

            shipperComponentModel.m_shipper.sync_client = null;
            shipperComponentModel.m_shipper.process_date = null;


            return HelperService.SqlInsertOrReplaceStatement('m_shipper', shipperComponentModel.m_shipper);

        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.GetInsertUpdateSqlStatement.name, error);
        }


    }


}