

class RolelineService {
    constructor() { }


    /**
	 * 
	 * @param {*} metaDataResponseTable 
	 */
    FindAll(metaDataResponseTable) {

        let metaDataQuery;
        try {


            const sqlTemplate = '  FROM wxt_roleline ' +
                ' INNER JOIN ad_role ON ad_role.ad_role_uu = wxt_roleline.ad_role_uu ' +
                ' WHERE wxt_roleline.wx_isdeleted = \'N\' ' +
                ' ORDER BY wxt_roleline.class_name ASC ';



            const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new RolelineComponentModel());

            metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);

        } catch (error) {
            apiInterface.Log(this.constructor.name, this.FindAll.name, error);
        }


        return apiInterface.Query(metaDataQuery);

    }

    FindRolelineCMTListByAdRoleUu(ad_role_uu) {
        let thisObject = this;
        let metaDataQuery;
        try {
            const sqlTemplate = '  FROM wxt_roleline ' +
                ' INNER JOIN ad_role ON ad_role.ad_role_uu = wxt_roleline.ad_role_uu ' +
                ' WHERE wxt_roleline.wx_isdeleted = \'N\' AND wxt_roleline.ad_role_uu = \'' + ad_role_uu + '\'';

            const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new RolelineComponentModel());

            metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);


        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.FindRolelineCMTListByAdRoleUu.name, ad_role_uu);
        }

        return apiInterface.Query(metaDataQuery);
    }

    FindRolelineCMTListByAdRoleUuAndClassName(ad_role_uu, class_name) {
        let metaDataQuery;
        try {
            const sqlTemplate = '  FROM wxt_roleline ' +
                ' INNER JOIN ad_role ON ad_role.ad_role_uu = wxt_roleline.ad_role_uu ' +
                ' WHERE wxt_roleline.ad_role_uu = \'' + ad_role_uu + '\' AND wxt_roleline.class_name = \'' + class_name + '\'';

            const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new RolelineComponentModel());

            metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);


        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.FindRolelineCMTListByAdRoleUu.name, ad_role_uu);
        }

        return apiInterface.Query(metaDataQuery);
    }

    FindRolelineCMTListByWxtRolelineUu(wxt_roleline_uu) {

        let metaDataQuery;


        try {

            const sqlTemplate = '  FROM wxt_roleline ' +
                ' INNER JOIN ad_role ON ad_role.ad_role_uu = wxt_roleline.ad_role_uu ' +
                ' WHERE wxt_roleline.wxt_roleline_uu = \'' + wxt_roleline_uu + '\'';

            const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new RolelineComponentModel());

            metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);

        } catch (error) {
            apiInterface.Log(this.constructor.name, this.FindRolelineCMTListByAdRoleUu.name, ad_role_uu);
        }
        return apiInterface.Query(metaDataQuery);
    }


    /**
     * Save Update harus merefer pada OrderViewModelTable
     * @param {RolelineComponentModel} rolelineCM
     * @param {*} datatableReference dataTableReferemce MInout datatable
     */
    SaveUpdate(rolelineCM, datatableReference) {
        let thisObject = this;
        const timeNow = new Date().getTime();

        let isNewRecord = false;
        if (!rolelineCM.wxt_roleline.wxt_roleline_uu) { //new record
            isNewRecord = true;
            rolelineCM.wxt_roleline.wxt_roleline_uu = HelperService.UUID();
            rolelineCM.wxt_roleline.ad_client_uu = localStorage.getItem('ad_client_uu');
            rolelineCM.wxt_roleline.ad_org_uu = localStorage.getItem('ad_org_uu');

            rolelineCM.wxt_roleline.createdby = localStorage.getItem('ad_user_uu');
            rolelineCM.wxt_roleline.updatedby = localStorage.getItem('ad_user_uu');

            rolelineCM.wxt_roleline.created = timeNow;
            rolelineCM.wxt_roleline.updated = timeNow;
        } else { //update record


            rolelineCM.wxt_roleline.updatedby = localStorage.getItem('ad_user_uu');

            rolelineCM.wxt_roleline.updated = timeNow;
        }

        rolelineCM.wxt_roleline.ad_role_uu = rolelineCM.ad_role.ad_role_uu;

        let insertSuccess = 0;
        const sqlInsert = HelperService.SqlInsertOrReplaceStatement('wxt_roleline', rolelineCM.wxt_roleline);

        return new Promise(function (resolve, reject) {
            apiInterface.ExecuteSqlStatement(sqlInsert)
                .then(function (rowChanged) {
                    insertSuccess += rowChanged;
                    if (insertSuccess > 0) {


                        thisObject.FindRolelineCMTListByWxtRolelineUu(rolelineCM.wxt_roleline.wxt_roleline_uu)
                            .then(function (rowDataList) {

                                let rowData = {};

                                if (rowDataList.length > 0) {
                                    rowData = rowDataList[0];
                                }

                                if (!isNewRecord) {
                                    datatableReference.row($('#bttn_row_edit' + rowData['wxt_roleline_wx_wxt_roleline_uu']).parents('tr')).data(rowData).draw();
                                } else {

                                    datatableReference.row.add(rowData).draw();
                                }

                            });





                    }
                });



        });



    }

    /**
     * Mendapatkan Not IN clause dalam roleline, akan menjadi 'xxxxxx','yyyyyy' ...
     */
    async GetNotInClauseSql(class_name) {

        let thisObject = this;

        let rolelineCMTList = await thisObject.FindRolelineCMTListByAdRoleUuAndClassName(localStorage.getItem('ad_role_uu'), class_name);




        let rolelineCMT = null;
        if (rolelineCMTList.length) {
            rolelineCMT = rolelineCMTList[0];
        }

        let filterRole = '';
        if (rolelineCMT) {
            let roleException = JSON.parse(rolelineCMT.wxt_roleline_wx_data);
            let notInArray = [];
            _.forEach(roleException, function (wypex_ad_menu_uu) {
                notInArray.push('\'' + wypex_ad_menu_uu + '\'');

            });

            if (notInArray.length) {
                let notInString = notInArray.join(',');
                filterRole = notInString;
            }

        }

        return filterRole;



    }


}