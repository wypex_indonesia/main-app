class UomConversionService {
    constructor() {
    }

    /**
	 * Find All warehouse dengan bentuk warehouseviewmodel
	 * @param {*} metaDataResponseTable 
	 */
    FindAll(metaDataResponseTable) {


        let otherFilters = '';
        if (metaDataResponseTable.otherFilters.length > 0) {

            _.forEach(metaDataResponseTable.otherFilters, function (value) {

                otherFilters += ' ' + value + ' ';
            });

        }

        let metaColumnUomfrom = HelperService.CreateMetaTableColumn("c_uomfrom", Object.keys(new CUom()));
        let metaColumnUomto = HelperService.CreateMetaTableColumn("c_uomto", Object.keys(new CUom()));
        let metaColumnCUomConversion = HelperService.CreateMetaTableColumn("c_uom_conversion", Object.keys(new CUomConversion()));

        const sqlTemplate = '  FROM c_uom_conversion ' +
            ' INNER JOIN c_uom as c_uomfrom ON c_uom_conversion.c_uom_uu = c_uomfrom.c_uom_uu ' +
            ' INNER JOIN c_uom as c_uomto ON c_uom_conversion.c_uom_to_uu = c_uomto.c_uom_uu ' +
            ' WHERE c_uom_conversion.wx_isdeleted = \'N\' ' + otherFilters + ' ORDER BY c_uom_conversion.updated ASC ';


        const listMetaColumn = _.union(metaColumnCUomConversion, metaColumnUomto, metaColumnUomfrom);

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }

    FindUomConversionViewModelByCUomConversionUu(c_uom_conversion_uu) {

        const sqlTemplate = ' FROM c_uom_conversion ' +
            ' INNER JOIN c_uom as c_uomfrom ON c_uom_conversion.c_uom_uu = c_uomfrom.c_uom_uu ' +
            ' INNER JOIN c_uom as c_uomto ON c_uom_conversion.c_uom_to_uu = c_uomto.c_uom_uu ' +
            ' WHERE c_uom_conversion.c_uom_conversion_uu = \'' + c_uom_conversion_uu + '\'';

        let metaColumnUomfrom = HelperService.CreateMetaTableColumn("c_uomfrom", Object.keys(new CUom()));
        let metaColumnUomto = HelperService.CreateMetaTableColumn("c_uomto", Object.keys(new CUom()));
        let metaColumnCUomConversion = HelperService.CreateMetaTableColumn("c_uom_conversion", Object.keys(new CUomConversion()));

        const listMetaColumn = _.union(metaColumnCUomConversion, metaColumnUomto, metaColumnUomfrom);

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }

    /**
     * 
     * @param {*} m_product_uu 
     * @param {*} c_uom_uu cum yang lebih kecil
     * @param {*} c_uom_to_uu cuom yang lebih besar
     */
    FindUomConversionViewModelByMProductUuAndCUomUuAndCUomToUu(m_product_uu, c_uom_uu, c_uom_to_uu) {

        const sqlTemplate = ' FROM c_uom_conversion ' +
            ' INNER JOIN c_uom as c_uomfrom ON c_uom_conversion.c_uom_uu = c_uomfrom.c_uom_uu ' +
            ' INNER JOIN c_uom as c_uomto ON c_uom_conversion.c_uom_to_uu = c_uomto.c_uom_uu ' +
            ' WHERE c_uom_conversion.m_product_uu = \'' + m_product_uu + '\' AND c_uom_conversion.c_uom_to_uu = \'' +
            c_uom_to_uu + '\' AND c_uom_conversion.c_uom_uu = \'' +
            c_uom_uu + '\'';

        let metaColumnUomfrom = HelperService.CreateMetaTableColumn("c_uomfrom", Object.keys(new CUom()));
        let metaColumnUomto = HelperService.CreateMetaTableColumn("c_uomto", Object.keys(new CUom()));
        let metaColumnCUomConversion = HelperService.CreateMetaTableColumn("c_uom_conversion", Object.keys(new CUomConversion()));

        const listMetaColumn = _.union(metaColumnCUomConversion, metaColumnUomto, metaColumnUomfrom);

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }

    /**
	 * 
	
	 */
    async SaveUpdate(viewModel, datatableReference) {
        const timeNow = new Date().getTime();
        let insertSuccess = 0;
        let isNewRecord = false;
        let thisObject = this;


        try {
            if (!viewModel.c_uom_conversion.c_uom_conversion_uu) { //new record
                isNewRecord = true;
                viewModel.c_uom_conversion.c_uom_conversion_uu = HelperService.UUID();
                viewModel.c_uom_conversion.ad_client_uu = localStorage.getItem('ad_client_uu');
                viewModel.c_uom_conversion.ad_org_uu = localStorage.getItem('ad_org_uu');

                viewModel.c_uom_conversion.createdby = localStorage.getItem('ad_user_uu');
                viewModel.c_uom_conversion.updatedby = localStorage.getItem('ad_user_uu');

                viewModel.c_uom_conversion.created = timeNow;
                viewModel.c_uom_conversion.updated = timeNow;
            } else { //update record


                viewModel.c_uom_conversion.updatedby = localStorage.getItem('ad_user_uu');

                viewModel.c_uom_conversion.updated = timeNow;
            }


            viewModel.c_uom_conversion.c_uom_uu = viewModel.c_uomfrom.c_uom_uu;
            viewModel.c_uom_conversion.c_uom_to_uu = viewModel.c_uomto.c_uom_uu;
            viewModel.c_uom_conversion.sync_client = null;
            viewModel.c_uom_conversion.process_date = null;


            const sqlInsert = HelperService.SqlInsertOrReplaceStatement('c_uom_conversion', viewModel.c_uom_conversion);

            insertSuccess += await apiInterface.ExecuteSqlStatement(sqlInsert);

            if (insertSuccess > 0) {

                if (datatableReference) {
                    let rowDataList = await thisObject.FindUomConversionViewModelByCUomConversionUu(viewModel.c_uom_conversion.c_uom_conversion_uu);
                    let rowData = {};


                    if (rowDataList.length > 0) {
                        rowData = rowDataList[0];
                    }

                    if (!isNewRecord) {

                        datatableReference.row($('#bttn_row_edit' + rowData['c_uom_conversion_wx_c_uom_conversion_uu']).parents('tr')).data(rowData).draw();
                    } else {

                        datatableReference.row.add(rowData).draw();
                    }

                }


            }

        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.SaveUpdate.name, error);
            return insertSuccess;
        }


        return insertSuccess;





    }




}


