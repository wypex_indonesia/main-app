


class ProductpriceService {

    constructor() {
    }


    /**
	 * Find All warehouse dengan bentuk warehouseviewmodel
	 * @param {*} metaDataResponseTable 
	 */
    FindAll(metaDataResponseTable) {

        let metaDataQuery;
        try {
            let otherFilters = '';
            if (metaDataResponseTable.otherFilters.length > 0) {

                _.forEach(metaDataResponseTable.otherFilters, function (value) {

                    otherFilters += ' ' + value + ' ';
                });

            }

            let rolelineService = new RolelineService();
            return rolelineService.GetNotInClauseSql('MPricelist').then(function (notInClauseSql) {

                let filterRole = '';
                if (notInClauseSql) {
                    filterRole = ' AND m_pricelist.m_pricelist_uu NOT IN (' + notInClauseSql + ') ';
                }

                const sqlTemplate = '  FROM m_productprice ' +
                    ' INNER JOIN m_product ON m_product.m_product_uu =  m_productprice.m_product_uu ' +
                    ' INNER JOIN m_pricelist_version ON m_pricelist_version.m_pricelist_version_uu  = m_productprice.m_pricelist_version_uu ' +
                    ' INNER JOIN m_pricelist ON m_pricelist.m_pricelist_uu  = m_pricelist_version.m_pricelist_uu ' +
                    ' WHERE m_productprice.wx_isdeleted = \'N\' ' + filterRole + otherFilters + ' ORDER BY m_productprice.updated ASC ';



                const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new ProductpriceViewModel());

                metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);
                return apiInterface.Query(metaDataQuery);

            });


        } catch (error) {
            apiInterface.Log(this.constructor.name, this.FindAll.name, error);
        }



    }

    FindProductpriceViewModelByMProductpriceUu(m_productprice_uu) {

        let metaDataQuery;
        try {
            const sqlTemplate = '  FROM m_productprice ' +
                ' INNER JOIN m_product ON m_product.m_product_uu =  m_productprice.m_product_uu ' +
                ' INNER JOIN m_pricelist_version ON m_pricelist_version.m_pricelist_version_uu  = m_productprice.m_pricelist_version_uu ' +
                ' INNER JOIN m_pricelist ON m_pricelist.m_pricelist_uu  = m_pricelist_version.m_pricelist_uu ' +
                ' WHERE m_productprice.m_productprice_uu = \'' + m_productprice_uu + '\'';
            const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new ProductpriceViewModel());

            metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);

        } catch (error) {
            apiInterface.Log(this.constructor.name, this.FindProductpriceViewModelByMProductpriceUu.name, error);
        }

        return apiInterface.Query(metaDataQuery);
    }

	/**
	 * 
	
	 */
    async SaveUpdate(viewModel, datatableReference) {
        const timeNow = new Date().getTime();
        let insertSuccess = 0;
        let isNewRecord = false;
        let thisObject = this;

        try {

            if (!viewModel.m_productprice.m_productprice_uu) { //new record
                isNewRecord = true;
                viewModel.m_productprice.m_productprice_uu = HelperService.UUID();
                viewModel.m_productprice.ad_client_uu = localStorage.getItem('ad_client_uu');
                viewModel.m_productprice.ad_org_uu = localStorage.getItem('ad_org_uu');

                viewModel.m_productprice.createdby = localStorage.getItem('ad_user_uu');
                viewModel.m_productprice.updatedby = localStorage.getItem('ad_user_uu');

                viewModel.m_productprice.created = timeNow;
                viewModel.m_productprice.updated = timeNow;
            } else { //update record


                viewModel.m_productprice.updatedby = localStorage.getItem('ad_user_uu');

                viewModel.m_productprice.updated = timeNow;
            }

          
            if (!viewModel.m_productprice.pricelimit){
                viewModel.m_productprice.pricelimit = viewModel.m_productprice.pricelist;
            }
            if (!viewModel.m_productprice.pricestd){
                viewModel.m_productprice.pricestd = viewModel.m_productprice.pricelist;
            }

            viewModel.m_productprice.m_pricelist_version_uu = viewModel.m_pricelist_version.m_pricelist_version_uu;
            viewModel.m_productprice.sync_client = null;
            viewModel.m_productprice.process_date = null;

            const sqlInsert = HelperService.SqlInsertOrReplaceStatement('m_productprice', viewModel.m_productprice);
            insertSuccess += await apiInterface.ExecuteSqlStatement(sqlInsert);


            // jika sukses tersimpan 
            if (insertSuccess > 0) {

                if (datatableReference) {
                    let rowDataList = await thisObject.FindProductpriceViewModelByMProductpriceUu(viewModel.m_productprice.m_productprice_uu);

                    let rowData = {};

                    if (rowDataList.length > 0) {
                        rowData = rowDataList[0];
                    }

                    if (!isNewRecord) {

                        datatableReference.row($('#bttn_row_edit' + rowData['m_productprice_wx_m_productprice_uu']).parents('tr')).data(rowData).draw();
                    } else {

                        datatableReference.row.add(rowData).draw();
                    }

                }

            }

        } catch (error) {
            apiInterface.Log(this.constructor.name, this.SaveUpdate.name, error);
            return insertSuccess;
        }


        return insertSuccess;


    }




}

