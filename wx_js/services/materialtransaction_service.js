class MaterialTransactionService {
    constructor() { }



    /**
       * Feed rows secara synchronize karena datanya banyak, dan memiliki relation table yang berbeda-beda
       * @param {*} metaDataResponseTable 
       * @param {*} datatableReference 
       */
    FeedRows(datatableReference) {
        let thisObject = this;
        let factacctService = new FactAcctService();
        let mTransactionCMTList = datatableReference.rows().data();
        let columnsInDataTable = datatableReference.context[0].aoColumns;
        let indexDocumentNo = _.findIndex(columnsInDataTable, function (columnInDt) { return columnInDt.data === 'document_no'; });
        let indexDocumentType = _.findIndex(columnsInDataTable, function (columnInDt) { return columnInDt.data === 'document_type'; });

        _.forEach(mTransactionCMTList, function (mTransactionCMT,i) {

            factacctService.GetDetailDocument(mTransactionCMT.m_transaction_wx_tablename, mTransactionCMT.m_transaction_wx_tableuuid).then(function (results) {
                let result = results[0];
                mTransactionCMT.document_no = result.document_no;
                mTransactionCMT.document_type = result.document_type;
              //  datatableReference.cell(i, indexDocumentType).data(result.document_type);
               // datatableReference.cell(i, indexDocumentNo).data(result.document_no);
                datatableReference.row(i).data(mTransactionCMT);

            })

        });

    }



    /**
     * Find All warehouse dengan bentuk warehouseviewmodel
     * @param {*} metaDataResponseTable 
     */
    FindAll(metaDataResponseTable) {


        let searchFilter = '';
        if (metaDataResponseTable.search !== '') {
            searchFilter += ' AND ' + metaDataResponseTable.searchField + '  LIKE    \'%' + metaDataResponseTable.search + '%\'';
        }

        let limit = '';
        if ((metaDataResponseTable.page !== null) && (metaDataResponseTable.perpage !== null)) {

            limit = ' LIMIT ' + metaDataResponseTable.page + ',' + metaDataResponseTable.perpage;

        }

        

        const sqlTemplate = 'SELECT m_warehouse.name  m_warehouse_wx_name, m_product.name m_product_wx_name, ' +
            ' m_transaction.movementqty m_transaction_wx_movementqty, c_uom.name c_uom_wx_name, ' + 
            ' m_transaction.movementtype m_transaction_wx_movementtype, \'\' document_type, \'\' document_no, ' +
            ' m_transaction.docstatus m_transaction_wx_docstatus, m_transaction.updated m_transaction_wx_updated, '+
            ' m_transaction.tablename m_transaction_wx_tablename, m_transaction.tableuuid m_transaction_wx_tableuuid ' +
            '  FROM m_transaction ' +
            ' INNER JOIN m_locator  ON m_transaction.m_locator_uu = m_locator.m_locator_uu ' +
            ' INNER JOIN m_warehouse ON m_locator.m_warehouse_uu = m_warehouse.m_warehouse_uu ' +
            ' INNER JOIN m_product ON m_product.m_product_uu = m_transaction.m_product_uu ' +
            ' INNER JOIN c_uom ON c_uom.c_uom_uu = m_product.c_uom_uu ' +
            ' WHERE  m_transaction.ad_org_uu = \'' + metaDataResponseTable.ad_org_uu +
            '\'' + searchFilter + ' ORDER BY m_transaction.updated DESC ' + limit;

      
        return apiInterface.DirectQuerySqlStatement(sqlTemplate);
    }





    /**
     * 
     * @param {*} movementquantity  movement quantity berdasarkan basedUom 
     * @param {*} m_product_uu 
     * @param {*} m_inoutline_uu 
     * @param {*} m_locator_uu 
     */
    // MaterialReceiptCompleted(movementquantity, m_product_uu, m_inoutline_uu, m_locator_uu) {
    MaterialReceiptCompleted(inoutlineComponentModelTable) {

        let elementValueService = new ElementValueService();
        let thisObject = this;
        let insertSuccess = 0;
        return thisObject.sqlInsertMTransaction(elementValueService.MovementType().VendorReceipt,
            inoutlineComponentModelTable.m_inoutline_wx_m_locator_uu, 'm_inoutline', inoutlineComponentModelTable.m_inoutline_wx_m_inoutline_uu,
            inoutlineComponentModelTable.m_inoutline_wx_m_product_uu, inoutlineComponentModelTable.m_inoutline_wx_movementqty, false);




    }

    /**
     * Dibalik dari MaterialReceiptCompleted
     */
    MaterialReceiptReversed(m_inoutline_uu) {
        let elementValueService = new ElementValueService();

        return this.sqlInsertReverseMTransaction(elementValueService.MovementType().VendorReceiptReverse,
            elementValueService.MovementType().VendorReceipt,
            'm_inoutline', m_inoutline_uu);



    }



    FindMTransactionCMTByMTransactionUu(m_transaction_uu) {

        const sqlTemplate = '  FROM m_transaction ' +
            ' INNER JOIN m_locator  ON m_transaction.m_locator_uu = m_locator.m_locator_uu ' +
            ' INNER JOIN m_warehouse ON m_locator.m_warehouse_uu = m_warehouse.m_warehouse_uu ' +
            ' INNER JOIN m_product ON m_product.m_product_uu = m_transaction.m_product_uu ' +
            ' INNER JOIN c_uom ON c_uom.c_uom_uu = m_product.c_uom_uu ' +
            ' WHERE m_transaction.m_transaction_uu = \'' + m_transaction_uu + '\'';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new MaterialTransactionComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);



        return apiInterface.Query(metaDataQuery);
    }


    /**      
     *  @param movementTypeFilter movementype sebagai filter untuk mencari di table yang match Customer Out
     *  @param movementtypeReverse status movement type reverse ex. Customer Out Reverse
       * @param tableName nama table m_inoutline, atau c_invoiceline,
       * @param tableUuid uuid dari row , 
       */
    sqlInsertReverseMTransaction(movementtypeReverse, movementTypeFilter, tableName, tableUuid) {
        const updated = new Date().getTime();
        const updatedby = localStorage.getItem('ad_user_uu');
        let thisObject = this;
        const m_transaction_uu = HelperService.UUID();
        let sqlStatement = 'INSERT OR REPLACE INTO m_transaction(m_transaction_uu, movementtype, movementqty, m_product_uu, ' +
            ' tablename, tableuuid, docstatus, created, updated, createdby, updatedby, ad_org_uu, ad_client_uu, m_locator_uu) ' +
            ' SELECT  \'' + m_transaction_uu + '\', \'' + movementtypeReverse + '\', movementqty * (-1), m_product_uu, ' +
            ' tablename, tableuuid, \'REVERSED\',\'' + updated + '\' , \'' + updated + '\', \'' + updatedby + '\', \'' + updatedby + '\', ad_org_uu, ad_client_uu, m_locator_uu FROM m_transaction ' +
            ' WHERE movementtype=\'' + movementTypeFilter + '\' AND docstatus = \'COMPLETED\' AND tablename = \'' + tableName + '\' AND tableuuid = \'' + tableUuid + '\' ;';

        let insertSuccess = 0;

        let dataflow = {};

        return new Promise(function (resolve, reject) {
            apiInterface.ExecuteSqlStatement(sqlStatement).then(function (inserted) {

                insertSuccess += inserted;
                resolve(insertSuccess);
            });

        }).then(function (insertSuccess) {
            return new Promise(function (resolve, reject) {
                thisObject.FindMTransactionCMTByMTransactionUu(m_transaction_uu)
                    .then(function (mTransactionCMTList) {
                        let m_transaction_cmt = null;
                        if (mTransactionCMTList.length) {
                            m_transaction_cmt = mTransactionCMTList[0];
                        }
                        dataflow.m_transaction_cmt = m_transaction_cmt;
                        resolve(dataflow);
                    });
            });

        }).then(function (dataflow) {

            return new Promise(function (resolve, reject) {
                let product_cmt = null;
                if (dataflow.m_transaction_cmt) {

                    let productService = new ProductService();
                    productService.FindProductViewModelByMProductUu(dataflow.m_transaction_cmt.m_transaction_wx_m_product_uu).then(function (productCMTList) {

                        product_cmt = productCMTList[0];
                        dataflow.product_cmt = product_cmt;
                        resolve(dataflow);
                    });

                } else {
                    dataflow.product_cmt = product_cmt;
                    resolve(dataflow);
                }

            });
        }).then(function (dataflow) {

            return new Promise(function (resolve, reject) {

                // JIKA STOCK isstocked maka masukkan ke storagonhand
                if (dataflow.product_cmt.m_product_wx_isstocked === 'Y') {
                    const storageOnHandService = new StorageOnHandService();
                    storageOnHandService.SaveUpdate(dataflow.m_transaction_cmt.m_transaction_wx_m_locator_uu,
                        dataflow.m_transaction_cmt.m_transaction_wx_m_product_uu,
                        dataflow.m_transaction_cmt.m_transaction_wx_movementqty).then(function (inserted) {

                            insertSuccess += inserted;
                            resolve(insertSuccess);

                        });

                } else {

                    resolve(insertSuccess);
                }

            });

        });

    }


    /**
     * 
     *@param {JenisMovement Type} movementTypeValue , jenis movementype value
     * @param {*} m_locator_uu 
     * @param {*} tableName tablename, bisa m_inoutline, c_orderline (jika direct sales)
     * @param {*} tableuuid 
     * @param {*} m_product_uu 
     * @param {*} movementqty 
     * @param {*} isOut // apakah out material, agar dapat dikalikan -1 pada movementqty
     */
    sqlInsertMTransaction(movementTypeValue, m_locator_uu, tableName, tableuuid, m_product_uu, movementqty, isOut, movementdate) {

        let factorMovement = 1; // 1 artinya In, sedangkan -1 artinya out, untuk dikalikan dengan movementqty
        if (isOut) {
            factorMovement = -1;
        }
        movementqty = factorMovement * movementqty;

        const timeNow = new Date().getTime();

        const mtransaction = new MTransaction();
        mtransaction.m_transaction_uu = HelperService.UUID();
        mtransaction.movementtype = movementTypeValue;

        mtransaction.m_locator_uu = m_locator_uu;
        mtransaction.docstatus = 'COMPLETED';
        mtransaction.tablename = tableName;
        mtransaction.tableuuid = tableuuid;
        mtransaction.ad_client_uu = localStorage.getItem('ad_client_uu');
        mtransaction.ad_org_uu = localStorage.getItem('ad_org_uu');

        mtransaction.createdby = localStorage.getItem('ad_user_uu');
        mtransaction.updatedby = localStorage.getItem('ad_user_uu');

        mtransaction.created = timeNow;
        mtransaction.updated = timeNow;
        mtransaction.movementqty = movementqty;
        mtransaction.m_product_uu = m_product_uu;
        mtransaction.sync_client = null;
        mtransaction.process_date = null;
        if (!movementdate){
            movementdate = timeNow;
        }

        const sqlInsert = HelperService.SqlInsertOrReplaceStatement('m_transaction', mtransaction);

        let insertSuccess = 0;
        let dataflow = {};
        return new Promise(function (resolve, reject) {
            apiInterface.ExecuteSqlStatement(sqlInsert).then(function (inserted) {

                insertSuccess += inserted;

                resolve(insertSuccess);
            })
        }).then(function (insertSuccess) {

            return new Promise(function (resolve, reject) {

                let productService = new ProductService();
                productService.FindProductViewModelByMProductUu(m_product_uu).then(function (productCMTList) {

                    let productCMT = productCMTList[0];

                    resolve(productCMT);
                });


            });




        }).then(function (productCMT) {

            return new Promise(function (resolve, reject) {

                // JIKA STOCK isstocked maka masukkan ke storagonhand
                if (productCMT.m_product_wx_isstocked === 'Y') {
                    const storageOnHandService = new StorageOnHandService();
                    storageOnHandService.SaveUpdate(m_locator_uu, m_product_uu, movementqty).then(function (inserted) {

                        insertSuccess += inserted;
                        resolve(insertSuccess);

                    });

                } else {

                    resolve(insertSuccess);
                }




            });


        });

    }





    // MaterialReceiptCompleted(movementquantity, m_product_uu, m_inoutline_uu, m_locator_uu) {
    /*  RmaReceiptCompleted(rmalineComponentModelTable) {
          const timeNow = new Date().getTime();
          let elementValueService = new ElementValueService();
          const mtransaction = new MTransaction();
          mtransaction.m_transaction_uu = HelperService.UUID();
          mtransaction.movementtype = elementValueService.MovementType().RmaReceipt;
  
          mtransaction.m_locator_uu = rmalineComponentModelTable.m_rmaline_wx_m_locator_uu;
          mtransaction.docstatus = 'COMPLETED';
          mtransaction.tablename = 'm_rmaline';
          mtransaction.table_uuid = rmalineComponentModelTable.m_rmaline_wx_m_rmaline_uu;
          mtransaction.ad_client_uu = localStorage.getItem('ad_client_uu');
          mtransaction.ad_org_uu = localStorage.getItem('ad_org_uu');
  
          mtransaction.createdby = localStorage.getItem('ad_user_uu');
          mtransaction.updatedby = localStorage.getItem('ad_user_uu');
  
          mtransaction.created = timeNow;
          mtransaction.updated = timeNow;
          mtransaction.movementqty = rmalineComponentModelTable.m_rmaline_wx_movementqty;
          mtransaction.m_product_uu = rmalineComponentModelTable.m_rmaline_wx_m_product_uu;
          mtransaction.sync_client = null;
          mtransaction.process_date = null;
  
  
          const sqlInsert = HelperService.SqlInsertOrReplaceStatement('m_transaction', mtransaction);
  
          let insertSuccess = 0;
          insertSuccess += apiInterface.ExecuteSqlStatement(sqlInsert);
          return insertSuccess;
  
      }*/
}