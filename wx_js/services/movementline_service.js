

class MovementlineService {
    constructor() { }


    /**
	 * 
	 * @param {*} metaDataResponseTable 
	 */
    FindAll(metaDataResponseTable) {


        let otherFilters = '';
        if (metaDataResponseTable.otherFilters.length > 0) {

            _.forEach(metaDataResponseTable.otherFilters, function (value) {

                otherFilters += ' ' + value + ' ';
            });

        }

        const sqlTemplate = '  FROM m_movementline ' +
            ' INNER JOIN m_movement ON m_movement.m_movement_uu = m_movementline.m_movement_uu ' +
            ' INNER JOIN m_product ON m_product.m_product_uu  = m_movementline.m_product_uu ' +
            ' INNER JOIN c_uom ON m_product.c_uom_uu = c_uom.c_uom_uu ' +
            ' WHERE m_movementline.wx_isdeleted = \'N\' ' + otherFilters + ' ORDER BY m_movementline.line ASC ';



        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new MovementlineComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);

    }

    FindMovementlineCMTByMMovementlineUu(m_movementline_uu) {


        const sqlTemplate = '  FROM m_movementline ' +
            ' INNER JOIN m_movement ON m_movement.m_movement_uu = m_movementline.m_movement_uu ' +
            ' INNER JOIN m_product ON m_product.m_product_uu  = m_movementline.m_product_uu ' +
            ' INNER JOIN c_uom ON m_product.c_uom_uu = c_uom.c_uom_uu ' +
            ' WHERE m_movementline.m_movementline_uu = \'' + m_movementline_uu + '\'';



        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new MovementlineComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }

    FindMovementlineCMTByMMovementUu(m_movement_uu) {


        const sqlTemplate = '  FROM m_movementline ' +
            ' INNER JOIN m_movement ON m_movement.m_movement_uu = m_movementline.m_movement_uu ' +
            ' INNER JOIN m_product ON m_product.m_product_uu  = m_movementline.m_product_uu ' +
            ' INNER JOIN c_uom ON m_product.c_uom_uu = c_uom.c_uom_uu ' +
            ' WHERE m_movementline.wx_isdeleted = \'N\' AND m_movementline.m_movement_uu = \'' + m_movement_uu + '\' ORDER BY m_movementline.line ASC';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new MovementlineComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }

    UpdateQtyCount(m_movementline_uu, qtycount) {



        qtycount = numeral(qtycount).value();
        let ad_user_uu = localStorage.getItem('ad_user_uu');
        let timeNow = new Date().getTime();

        if (qtycount === null) {
            qtycount = 0;
        }

        const sqlTemplate = 'UPDATE m_movementline SET movementqty = ' + qtycount + ', updated = ' + timeNow + ' , updatedby = \'' + ad_user_uu + '\'  WHERE m_movementline_uu = \'' + m_movementline_uu + '\';';




        return apiInterface.ExecuteSqlStatement(sqlTemplate);

    }
	/**
	 * 
	 
	 */
    async SaveUpdate(m_movementline, datatableReferenceMovementline) {
        let thisObject = this;
        let insertSuccess = 0;
        try {

            const timeNow = new Date().getTime();
            // jika new record maka buat mlocator 	
            // harus dibuat default locator, karena locator akan dipergunakan di inventory movement
            let isNewRecord = false;
            if (!m_movementline.m_movementline_uu) { //new record
                isNewRecord = true;
                m_movementline.m_movementline_uu = HelperService.UUID();
                m_movementline.ad_client_uu = localStorage.getItem('ad_client_uu');
                m_movementline.ad_org_uu = localStorage.getItem('ad_org_uu');

                m_movementline.createdby = localStorage.getItem('ad_user_uu');
                m_movementline.updatedby = localStorage.getItem('ad_user_uu');

                m_movementline.created = timeNow;
                m_movementline.updated = timeNow;
            } else { //update record


                m_movementline.updatedby = localStorage.getItem('ad_user_uu');

                m_movementline.updated = timeNow;
            }



            m_movementline.sync_client = null;
            m_movementline.process_date = null;


            const sqlInsert = HelperService.SqlInsertOrReplaceStatement('m_movementline', m_movementline);

            insertSuccess += await apiInterface.ExecuteSqlStatement(sqlInsert);

            // jika sukses tersimpan 
            if (insertSuccess > 0) {


                let rowDataList = await thisObject.FindMovementlineCMTByMMovementlineUu(m_movementline.m_movementline_uu);

                let rowData = {};

                if (rowDataList.length > 0) {
                    rowData = rowDataList[0];
                }
                if (!isNewRecord) {
                    datatableReferenceMovementline.row($('#bttn_row_edit' + rowData['m_movementline_wx_m_movementline_uu']).parents('tr')).data(rowData).draw();
                } else {
                    datatableReferenceMovementline.row.add(rowData).draw();
                }

            }



        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.SaveUpdate.name, error);
        }


        return insertSuccess;



    }
    CreateNewmovementline(m_product_uu, movementCMT, datatableReferenceMovementline) {
        let thisObject = this;

        try {
            let lengthRow = datatableReferenceMovementline.data().length;


            let newMovementline = new MMovementline();
            newMovementline.m_product_uu = m_product_uu;
            newMovementline.m_movement_uu = movementCMT.m_movement_wx_m_movement_uu;
            newMovementline.line = ++lengthRow;


            return new Promise(function (resolve, reject) {

                thisObject.SaveUpdate(newMovementline, datatableReferenceMovementline).then(function (inserted) {
                    $('#input_' + newMovementline.m_movementline_uu).focus();
                    resolve(inserted);

                });

            });

        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.CreateNewmovementline.name, error);
        }


    }




}