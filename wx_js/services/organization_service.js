
class OrganizationService {
    constructor() { }



    /**
    * Find All warehouse dengan bentuk warehouseviewmodel
    * @param {*} metaDataResponseTable 
    */
    FindOrganizationComponentModelByAdOrgUu(ad_org_uu) {


        const sqlTemplate = '  FROM ad_org ' +
            ' INNER JOIN ad_orginfo ON ad_orginfo.ad_org_uu = ad_org.ad_org_uu ' +
            ' INNER JOIN c_location ON c_location.c_location_uu = ad_orginfo.c_location_uu ' +
            ' WHERE ad_org.ad_org_uu = \'' + ad_org_uu + '\'';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new OrganizationComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }

    FindAdOrg() {


        const sqlTemplate = '  FROM ad_org ' +
            ' INNER JOIN ad_orginfo ON ad_orginfo.ad_org_uu = ad_org.ad_org_uu ' +
            ' INNER JOIN c_location ON c_location.c_location_uu = ad_orginfo.c_location_uu ';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new OrganizationComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }




    async SaveUpdate(organizationCM) {
        let thisObject = this;
        const timeNow = new Date().getTime();


        if (!organizationCM.ad_org.ad_org_uu) { //new record

            organizationCM.ad_org.ad_org_uu = localStorage.getItem('ad_org_uu');
            organizationCM.ad_org.ad_client_uu = localStorage.getItem('ad_client_uu');

            organizationCM.ad_org.createdby = localStorage.getItem('ad_user_uu');
            organizationCM.ad_org.updatedby = localStorage.getItem('ad_user_uu');

            organizationCM.ad_org.created = timeNow;
            organizationCM.ad_org.updated = timeNow;
        } else { //update record


            organizationCM.ad_org.updatedby = localStorage.getItem('ad_user_uu');

            organizationCM.ad_org.updated = timeNow;
        }

        organizationCM.ad_org.sync_client = null;
        organizationCM.ad_org.process_date = null;

        if (!organizationCM.c_location.c_location_uu) {
            organizationCM.c_location.c_location_uu = HelperService.UUID();
            organizationCM.c_location.ad_client_uu = organizationCM.ad_org.ad_client_uu;
            organizationCM.c_location.ad_org_uu = organizationCM.ad_org.ad_org_uu;

            organizationCM.c_location.createdby = localStorage.getItem('ad_user_uu');
            organizationCM.c_location.updatedby = localStorage.getItem('ad_user_uu');

            organizationCM.c_location.created = timeNow;
            organizationCM.c_location.updated = timeNow;
        } else {


            organizationCM.c_location.updatedby = localStorage.getItem('ad_user_uu');

            organizationCM.c_location.updated = timeNow;
        }

        organizationCM.c_location.sync_client = null;
        organizationCM.c_location.process_date = null;

        if (!organizationCM.ad_orginfo.ad_orginfo_uu) {
            organizationCM.ad_orginfo.ad_orginfo_uu = HelperService.UUID();
            organizationCM.ad_orginfo.ad_client_uu = organizationCM.ad_org.ad_client_uu;
            organizationCM.ad_orginfo.ad_org_uu = organizationCM.ad_org.ad_org_uu;

            organizationCM.ad_orginfo.createdby = localStorage.getItem('ad_user_uu');
            organizationCM.ad_orginfo.updatedby = localStorage.getItem('ad_user_uu');

            organizationCM.ad_orginfo.created = timeNow;
            organizationCM.ad_orginfo.updated = timeNow;
        } else {


            organizationCM.ad_orginfo.updatedby = localStorage.getItem('ad_user_uu');

            organizationCM.ad_orginfo.updated = timeNow;
        }
        organizationCM.ad_orginfo.sync_client = null;
        organizationCM.ad_orginfo.process_date = null;


        organizationCM.ad_orginfo.ad_org_uu = organizationCM.ad_org.ad_org_uu;
        organizationCM.ad_orginfo.c_location_uu = organizationCM.c_location.c_location_uu;

        let insertSuccess = 0;
        const sqlInsertAdOrg = HelperService.SqlInsertOrReplaceStatement('ad_org', organizationCM.ad_org);
        const sqlInsertAdOrgInfo = HelperService.SqlInsertOrReplaceStatement('ad_orginfo', organizationCM.ad_orginfo);
        const sqlInsertLocation = HelperService.SqlInsertOrReplaceStatement('c_location', organizationCM.c_location);
        insertSuccess += await apiInterface.ExecuteSqlStatement(sqlInsertAdOrg);
        insertSuccess += await apiInterface.ExecuteSqlStatement(sqlInsertAdOrgInfo);
        insertSuccess += await apiInterface.ExecuteSqlStatement(sqlInsertLocation);
        return insertSuccess;
        /*  return new Promise(function(resolve,reject){
  
              Promise.all([apiInterface.ExecuteSqlStatement(sqlInsertAdOrg),
                  apiInterface.ExecuteSqlStatement(sqlInsertAdOrgInfo),
                  apiInterface.ExecuteSqlStatement(sqlInsertLocation)
              ]).then(function(resultArray){
                  insertSuccess += resultArray[0];
  
        
                  insertSuccess += resultArray[1];
          
                
                  insertSuccess += resultArray[2];
          
                  // jika sukses tersimpan 
          
                  resolve(insertSuccess);
          
      
  
  
              });
            
  
          });*/

    }





}