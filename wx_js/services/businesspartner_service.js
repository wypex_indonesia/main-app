
class BusinessPartnerService {
    constructor() { }

    /**
	 * Find All warehouse dengan bentuk warehouseviewmodel
	 * @param {*} metaDataResponseTable 
	 */
    FindAll(metaDataResponseTable) {


        let searchFilter = '';
        if (metaDataResponseTable.search !== '') {
            searchFilter += ' AND ' + metaDataResponseTable.searchField + '  LIKE    \'%' + metaDataResponseTable.search + '%\'';
        }

        let limit = '';
        if ((metaDataResponseTable.page !== null) && (metaDataResponseTable.perpage !== null)) {

            limit = ' LIMIT ' + metaDataResponseTable.page + ',' + metaDataResponseTable.perpage;

        }

        const sqlTemplate = '  FROM c_bpartner ' +
            ' INNER JOIN c_bpartner_location ON c_bpartner_location.c_bpartner_uu = c_bpartner.c_bpartner_uu ' +
            ' INNER JOIN c_location ON c_location.c_location_uu = c_bpartner_location.c_location_uu ' +
            ' WHERE c_bpartner.wx_isdeleted = \'N\' AND c_bpartner.ad_org_uu = \'' + metaDataResponseTable.ad_org_uu + '\'' + searchFilter + ' ORDER BY c_bpartner.updated DESC ' + limit;

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new BusinesspartnerViewModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);


        return apiInterface.Query(metaDataQuery);
    }

    /**
     * return new Promise BPartnerCM
     */
    GetDefaultCustomerPos() {
        let thisObject = this;
        let metabusinessPartner = new MetaDataTable();
        metabusinessPartner.ad_org_uu = localStorage.getItem('ad_org_uu');
        metabusinessPartner.search = 'Customer';
        metabusinessPartner.searchField = 'c_bpartner.name';

        return new Promise(function (resolve, reject) {
            thisObject.FindAll(metabusinessPartner).then(function (businessPartnerList) {

                resolve(businessPartnerList);
            });

        }).then(function (businessPartnerList) {

            return new Promise(function (resolve, reject) {
                let returnBpartnerCM = null;
                if (businessPartnerList.length) {
                    let businessPartnerCMT = businessPartnerList[0];
                    returnBpartnerCM = HelperService.ConvertRowDataToViewModel(businessPartnerCMT);
                    resolve(returnBpartnerCM);
                } else {

                    thisObject.CreateDefaultCustomerForPos().then(function (returnBpartnerCM) {

                        resolve(returnBpartnerCM);
                    });

                }

            });

        });



    }

    /**
    * Find All warehouse dengan bentuk warehouseviewmodel
    * @param {*} metaDataResponseTable 
    */
    FindBusinessPartnerCMTListByPhone(phone) {



        const sqlTemplate = '  FROM c_bpartner ' +
            ' INNER JOIN c_bpartner_location ON c_bpartner_location.c_bpartner_uu = c_bpartner.c_bpartner_uu ' +
            ' INNER JOIN c_location ON c_location.c_location_uu = c_bpartner_location.c_location_uu ' +
            ' WHERE c_bpartner.wx_isdeleted = \'N\' AND c_bpartner_location.phone =\'' + phone + '\'';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new BusinesspartnerViewModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);


        return apiInterface.Query(metaDataQuery);
    }

    /**
    * Find All warehouse dengan bentuk warehouseviewmodel
    * @param {*} metaDataResponseTable 
    */
    FindBusinesspartnerViewModelByCBpartnerUu(c_bpartner_uu) {


        const sqlTemplate = '  FROM c_bpartner ' +
            ' INNER JOIN c_bpartner_location ON c_bpartner_location.c_bpartner_uu = c_bpartner.c_bpartner_uu ' +
            ' INNER JOIN c_location ON c_location.c_location_uu = c_bpartner_location.c_location_uu ' +
            ' WHERE c_bpartner.c_bpartner_uu = \'' + c_bpartner_uu + '\'';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new BusinesspartnerViewModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn);


        return apiInterface.Query(metaDataQuery);
    }


    CreateDefaultCustomerForPos() {
        let thisObject = this;
        let timeNow = new Date().getTime();
        let businesspartnerVM = new BusinesspartnerViewModel();
        let c_bpartner = new CBpartner();
        let c_bpartner_location = new CBpartnerLocation();
        let c_location = new CLocation();

        try {
            c_bpartner.name = 'Customer';
            c_bpartner.isvendor = 'N';
            c_bpartner.iscustomer = 'Y';
            c_bpartner.c_bpartner_uu = HelperService.UUID();
            c_bpartner.ad_client_uu = localStorage.getItem('ad_client_uu');;
            c_bpartner.ad_org_uu = localStorage.getItem('ad_org_uu');
    
            c_bpartner.createdby = localStorage.getItem('ad_user_uu');
            c_bpartner.updatedby = localStorage.getItem('ad_user_uu');
    
            c_bpartner.created = timeNow;
            c_bpartner.updated = timeNow;
            businesspartnerVM.c_bpartner = c_bpartner;
            businesspartnerVM.c_bpartner_location = c_bpartner_location;
            businesspartnerVM.c_location = c_location;
    
            return new Promise(function (resolve, reject) {
                thisObject.SaveUpdate(businesspartnerVM).then(function (inserted) {
                    resolve(businesspartnerVM);
                })
    
    
    
    
            });
        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.CreateDefaultCustomerForPos.name,error);
        }
       

    }

    /**
     * 
     * @param {*} businessPartnerCM 
     * @param {*} functionPostSaved optional, jika tidak ada tidak akan dieksekusi
     */
    async SaveUpdate(businessPartnerCM, datatableReference) {
        let thisObject = this;
        const timeNow = new Date().getTime();
        let insertSuccess = 0;
        let isNewRecord = false;
        try {
            // jika new record maka buat mlocator 	
            // harus dibuat default locator, karena locator akan dipergunakan di inventory movement

            if (!businessPartnerCM.c_bpartner.c_bpartner_uu) { //new record
                isNewRecord = true;
                businessPartnerCM.c_bpartner.c_bpartner_uu = HelperService.UUID();
                businessPartnerCM.c_bpartner.ad_client_uu = localStorage.getItem('ad_client_uu');;
                businessPartnerCM.c_bpartner.ad_org_uu = localStorage.getItem('ad_org_uu');

                businessPartnerCM.c_bpartner.createdby = localStorage.getItem('ad_user_uu');
                businessPartnerCM.c_bpartner.updatedby = localStorage.getItem('ad_user_uu');

                businessPartnerCM.c_bpartner.created = timeNow;
                businessPartnerCM.c_bpartner.updated = timeNow;
            } else { //update record


                businessPartnerCM.c_bpartner.updatedby = localStorage.getItem('ad_user_uu');

                businessPartnerCM.c_bpartner.updated = timeNow;
            }

            businessPartnerCM.c_bpartner.sync_client = null;
            businessPartnerCM.c_bpartner.process_date = null;

            if (!businessPartnerCM.c_location.c_location_uu) {
                businessPartnerCM.c_location.c_location_uu = HelperService.UUID();
                businessPartnerCM.c_location.ad_client_uu = localStorage.getItem('ad_client_uu');;
                businessPartnerCM.c_location.ad_org_uu = localStorage.getItem('ad_org_uu');

                businessPartnerCM.c_location.createdby = localStorage.getItem('ad_user_uu');
                businessPartnerCM.c_location.updatedby = localStorage.getItem('ad_user_uu');

                businessPartnerCM.c_location.created = timeNow;
                businessPartnerCM.c_location.updated = timeNow;
            } else {


                businessPartnerCM.c_location.updatedby = localStorage.getItem('ad_user_uu');

                businessPartnerCM.c_location.updated = timeNow;
            }

            businessPartnerCM.c_location.sync_client = null;
            businessPartnerCM.c_location.process_date = null;

            if (!businessPartnerCM.c_bpartner_location.c_bpartner_location_uu) {
                businessPartnerCM.c_bpartner_location.c_bpartner_location_uu = HelperService.UUID();
                businessPartnerCM.c_bpartner_location.ad_client_uu = localStorage.getItem('ad_client_uu');;
                businessPartnerCM.c_bpartner_location.ad_org_uu = localStorage.getItem('ad_org_uu');

                businessPartnerCM.c_bpartner_location.createdby = localStorage.getItem('ad_user_uu');
                businessPartnerCM.c_bpartner_location.updatedby = localStorage.getItem('ad_user_uu');

                businessPartnerCM.c_bpartner_location.created = timeNow;
                businessPartnerCM.c_bpartner_location.updated = timeNow;
            } else {


                businessPartnerCM.c_bpartner_location.updatedby = localStorage.getItem('ad_user_uu');

                businessPartnerCM.c_bpartner_location.updated = timeNow;
            }

            if (businessPartnerCM.c_bpartner.iscustomer === '') {
                businessPartnerCM.c_bpartner.iscustomer = 'Y';
            }

            if (businessPartnerCM.c_bpartner.isvendor === '') {
                businessPartnerCM.c_bpartner.isvendor = 'Y';
            }

            businessPartnerCM.c_bpartner_location.c_bpartner_uu = businessPartnerCM.c_bpartner.c_bpartner_uu;
            businessPartnerCM.c_bpartner_location.c_location_uu = businessPartnerCM.c_location.c_location_uu;


            businessPartnerCM.c_bpartner_location.name = businessPartnerCM.c_bpartner.name;
            businessPartnerCM.c_bpartner_location.sync_client = null;
            businessPartnerCM.c_bpartner_location.process_date = null;


            const sqlInsertBusinesspartner = HelperService.SqlInsertOrReplaceStatement('c_bpartner', businessPartnerCM.c_bpartner);
            const sqlInsertBusinesspartnerlocation = HelperService.SqlInsertOrReplaceStatement('c_bpartner_location', businessPartnerCM.c_bpartner_location);
            const sqlInsertLocation = HelperService.SqlInsertOrReplaceStatement('c_location', businessPartnerCM.c_location);


            insertSuccess += await apiInterface.ExecuteSqlStatement(sqlInsertBusinesspartner);

            insertSuccess += await apiInterface.ExecuteSqlStatement(sqlInsertBusinesspartnerlocation);

            insertSuccess += await apiInterface.ExecuteSqlStatement(sqlInsertLocation);

            if (insertSuccess) {
                if (datatableReference) {
                    let rowDataList = await thisObject.FindBusinesspartnerViewModelByCBpartnerUu(businessPartnerCM.c_bpartner.c_bpartner_uu);
                    let rowData = {};

                    if (rowDataList.length > 0) {
                        rowData = rowDataList[0];
                    }

                    if (!isNewRecord) {

                        datatableReference.row($('#bttn_row_edit' + businessPartnerCM.c_bpartner.c_bpartner_uu).parents('tr')).data(rowData).draw();
                    } else {

                        datatableReference.row.add(rowData).draw();
                    }

                }
            }

            return insertSuccess;

        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.SaveUpdate.name, error);
            return insertSuccess;
        }


    }



}