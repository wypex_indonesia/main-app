

class TaxService {
    constructor() {
    }

    FindAll(metaDataResponseTable) {
        let searchFilter = '';
        if (metaDataResponseTable.search !== '') {
            searchFilter += ' AND ' + metaDataResponseTable.searchField + '  LIKE    \'%' + metaDataResponseTable.search + '%\'';
        }

        const sqlTemplate = '  FROM c_tax ' +
            ' WHERE c_tax.wx_isdeleted = \'N\' AND c_tax.ad_org_uu = \'' + metaDataResponseTable.ad_org_uu + '\'' + searchFilter + ' ORDER BY c_tax.rate DESC ';



        const listMetaColumn = HelperService.CreateMetaColumns('c_tax', new CTax());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }

    FindTaxCMByCTaxUu(c_tax_uu) {
        const sqlTemplate = '  FROM c_tax ' +
            ' WHERE  c_tax.c_tax_uu = \'' + c_tax_uu + '\'';



        const listMetaColumn = HelperService.CreateMetaColumns('c_tax', new CTax());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);

    }


    /**
    * 
    * @param {TaxComponentModel} taxCM 
    * @param {*} datatableReference dataTableReferemce CInvoice datatable
    */
   async SaveUpdate(taxCM, datatableReference) {
        const timeNow = new Date().getTime();

        let isNewRecord = false;
        let mLocator = null;
        if (!taxCM.c_tax.c_tax_uu) { //new record
            isNewRecord = true;
            taxCM.c_tax.c_tax_uu = HelperService.UUID();
            taxCM.c_tax.ad_client_uu = localStorage.getItem('ad_client_uu');
            taxCM.c_tax.ad_org_uu = localStorage.getItem('ad_org_uu');

            taxCM.c_tax.createdby = localStorage.getItem('ad_user_uu');
            taxCM.c_tax.updatedby = localStorage.getItem('ad_user_uu');

            taxCM.c_tax.created = timeNow;
            taxCM.c_tax.updated = timeNow;

        } else { //update record


            taxCM.c_tax.updatedby = localStorage.getItem('ad_user_uu');

            taxCM.c_tax.updated = timeNow;
        }



        taxCM.c_tax.sync_client = null;
        taxCM.c_tax.process_date = null;


        let insertSuccess = 0;
        const sqlInsert = HelperService.SqlInsertOrReplaceStatement('c_tax', taxCM.c_tax);

        insertSuccess += await apiInterface.ExecuteSqlStatement(sqlInsert);
        if (insertSuccess > 0) {

            if (datatableReference) {
              let rowDataList =  await  thisObject.FindTaxCMByCTaxUu(taxCM.c_tax.c_tax_uu);

                        let rowData = {};

                        if (rowDataList.length > 0) {
                            rowData = rowDataList[0];
                        }

                        if (!isNewRecord) {

                            datatableReference.row($('#bttn_row_edit' + rowData['c_tax_wx_c_tax_uu']).parents('tr')).data(rowData).draw();
                        } else {

                            datatableReference.row.add(rowData).draw();
                        }


            }



        }
        return insertSuccess;
        /*  return new Promise(function (resolve, reject) {
  
              apiInterface.ExecuteSqlStatement(sqlInsert)
                  .then(function (rowChanged) {
                      insertSuccess += rowChanged;
  
                      if (insertSuccess > 0) {
  
                          if (datatableReference) {
                              thisObject.FindTaxCMByCTaxUu(taxCM.c_tax.c_tax_uu)
                                  .then(function (rowDataList) {
  
                                      let rowData = {};
  
                                      if (rowDataList.length > 0) {
                                          rowData = rowDataList[0];
                                      }
  
                                      if (!isNewRecord) {
  
                                          datatableReference.row($('#bttn_row_edit' + rowData['c_tax_wx_c_tax_uu']).parents('tr')).data(rowData).draw();
                                      } else {
  
                                          datatableReference.row.add(rowData).draw();
                                      }
  
                                      resolve(insertSuccess);
  
                                  });
  
  
                          }
  
  
  
                      } else {
                          resolve(insertSuccess);
                      }
  
  
                  });
  
  
  
          });*/



    }

}