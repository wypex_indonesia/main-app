
class UomService {
    constructor() { }

    FindUomViewModel(metaDataResponseTable) {
        let metaDataQuery;
        let thisObject = this;
        try {
            let searchFilter = '';
            if (metaDataResponseTable.search !== '') {
                searchFilter += ' AND ' + metaDataResponseTable.searchField + '%' + metaDataResponseTable.search + '%';
            }

            const sqlTemplate = '  FROM c_uom ' +
                ' WHERE c_uom.wx_isdeleted = \'N\' AND c_uom.ad_org_uu = \'' + metaDataResponseTable.ad_org_uu + '\'' + searchFilter + ' ORDER BY c_uom.uomsymbol ASC ';

            const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new UomComponentModel());

            metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);

        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.FindUomViewModel.name, error);
        }


        return apiInterface.Query(metaDataQuery);

    }

    FindUomCMByCUomUu(c_uom_uu) {
        let metaDataQuery;
        try {
            const sqlTemplate = '  FROM c_uom ' +
                ' WHERE c_uom.c_uom_uu = \'' + c_uom_uu + '\'';

            const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new UomComponentModel());

            metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);

        } catch (error) {
            apiInterface.Log(this.constructor.name, this.FindUomCMByCUomUu.name, error);
        }


        return apiInterface.Query(metaDataQuery);

    }

    /**
    * 
    * @param {*} uomCM 
    * @param {*} datatableReference dataTableReferemce CInvoice datatable
    */
    async  SaveUpdate(uomCM, datatableReference) {
        const timeNow = new Date().getTime();
        let insertSuccess = 0;
        let isNewRecord = false;
        let thisObject = this;
        try {
            if (!uomCM.c_uom.c_uom_uu) { //new record
                isNewRecord = true;
                uomCM.c_uom.c_uom_uu = HelperService.UUID();
                uomCM.c_uom.ad_client_uu = localStorage.getItem('ad_client_uu');
                uomCM.c_uom.ad_org_uu = localStorage.getItem('ad_org_uu');

                uomCM.c_uom.createdby = localStorage.getItem('ad_user_uu');
                uomCM.c_uom.updatedby = localStorage.getItem('ad_user_uu');

                uomCM.c_uom.created = timeNow;
                uomCM.c_uom.updated = timeNow;


            } else { //update record


                uomCM.c_uom.updatedby = localStorage.getItem('ad_user_uu');

                uomCM.c_uom.updated = timeNow;
            }



            uomCM.c_uom.sync_client = null;
            uomCM.c_uom.process_date = null;


            const sqlInsert = HelperService.SqlInsertOrReplaceStatement('c_uom', uomCM.c_uom);
            insertSuccess += await apiInterface.ExecuteSqlStatement(sqlInsert);
            if (insertSuccess > 0) {

                if (datatableReference) {
                    let rowDataList = await thisObject.FindUomCMByCUomUu(uomCM.c_uom.c_uom_uu)
                    let rowData = {};

                    if (rowDataList.length > 0) {
                        rowData = rowDataList[0];
                    }

                    if (!isNewRecord) {

                        datatableReference.row($('#bttn_row_edit' + rowData['c_uom_wx_c_uom_uu']).parents('tr')).data(rowData).draw();
                    } else {

                        datatableReference.row.add(rowData).draw();
                    }



                }

            }




        } catch (error) {
            apiInterface.Log(this.constructor.name, this.SaveUpdate.name, error);
            return insertSuccess;
        }


        return insertSuccess;

    }

}