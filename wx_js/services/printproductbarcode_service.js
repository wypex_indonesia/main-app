

class PrintProductBarcodeService {
    constructor() { }


    FindPrintProductBarcodeCMTListByWxtPrintProductbarcodeUu(wxt_print_productbarcode_uu) {

        const sqlTemplate = '  FROM wxt_print_productbarcode ' +
            ' INNER JOIN wxt_paper_type ON wxt_print_productbarcode.wxt_paper_type_uu = wxt_paper_type.wxt_paper_type_uu ' +
            ' INNER JOIN wxt_paper_size ON wxt_print_productbarcode.wxt_paper_size_uu  = wxt_paper_sizewxt_paper_size_uu ' +

            ' WHERE wxt_print_productbarcode.wxt_print_productbarcode_uu = \'' + wxt_print_productbarcode_uu + '\'';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new PrintProductbarcodeComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }

    FindLastPrintProductBarcodeCMTList() {

        const sqlTemplate = '  FROM wxt_print_productbarcode ' +
            ' INNER JOIN wxt_paper_type ON wxt_print_productbarcode.wxt_paper_type_uu = wxt_paper_type.wxt_paper_type_uu ' +
            ' INNER JOIN wxt_paper_size ON wxt_print_productbarcode.wxt_paper_size_uu  = wxt_paper_size.wxt_paper_size_uu ' +
            ' ORDER BY wxt_print_productbarcode.updated DESC LIMIT 0,1';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new PrintProductbarcodeComponentModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }

    /**
  * Save Update harus merefer pada OrderViewModelTable
  * @param {PrintProductbarcodeComponentModel} barcodeCM 
 
  */
    SaveUpdate(barcodeCM) {
        const timeNow = new Date().getTime();
        let insertSuccess = 0;
        let isNewRecord = false;
        let sqlInsert = '';
        try {
            if (!barcodeCM.wxt_print_productbarcode.wxt_print_productbarcode_uu) { //new record
                isNewRecord = true;
                barcodeCM.wxt_print_productbarcode.wxt_print_productbarcode_uu = HelperService.UUID();
                barcodeCM.wxt_print_productbarcode.ad_client_uu = localStorage.getItem('ad_client_uu');
                barcodeCM.wxt_print_productbarcode.ad_org_uu = localStorage.getItem('ad_org_uu');

                barcodeCM.wxt_print_productbarcode.createdby = localStorage.getItem('ad_user_uu');
                barcodeCM.wxt_print_productbarcode.updatedby = localStorage.getItem('ad_user_uu');

                barcodeCM.wxt_print_productbarcode.created = timeNow;
                barcodeCM.wxt_print_productbarcode.updated = timeNow;
            } else { //update record


                barcodeCM.wxt_print_productbarcode.updatedby = localStorage.getItem('ad_user_uu');

                barcodeCM.wxt_print_productbarcode.updated = timeNow;
            }
         
            barcodeCM.wxt_print_productbarcode.sync_client = null;
            barcodeCM.wxt_print_productbarcode.process_date = null;


            sqlInsert = HelperService.SqlInsertOrReplaceStatement('wxt_print_productbarcode', barcodeCM.wxt_print_productbarcode);



        } catch (error) {
            apiInterface.Log(this.constructor.name, this.SaveUpdate.name, error);
        }

        return new Promise(function (resolve, reject) {
            apiInterface.ExecuteSqlStatement(sqlInsert).then(function (inserted) {

                resolve(barcodeCM);
            });


        });


    }





}