class FactAcctService {
    constructor() { }


    /**
     * Feed rows secara synchronize karena datanya banyak, dan memiliki relation table yang berbeda-beda
     * @param {*} metaDataResponseTable 
     * @param {*} datatableReference 
   
    FeedRows(metaDataResponseTable, datatableReference) {

        let thisObject = this;
        thisObject.FindAll(metaDataResponseTable).then(function (factAcctCMTList) {

            _.forEach(factAcctCMTList, function (factAcctCMT, i) {

                thisObject.GetDetailDocument(factAcctCMT.fact_acct_wx_tablename, factAcctCMT.fact_acct_wx_tableuuid).then(function (results) {
                    let result = results[0];
                    factAcctCMT.document_no = result.document_no;
                    factAcctCMT.document_type = result.document_type;
                    datatableReference.row.add(factAcctCMT).draw();
                })

            });


        });


    }  */

    FeedRows(datatableReference) {
        let thisObject = this;

        let factAcctCMTList = datatableReference.rows().data();
        let columnsInDataTable = datatableReference.context[0].aoColumns;
        let indexDocumentNo = _.findIndex(columnsInDataTable, function (columnInDt) { return columnInDt.data === 'document_no'; });
        let indexDocumentType = _.findIndex(columnsInDataTable, function (columnInDt) { return columnInDt.data === 'document_type'; });

        _.forEach(factAcctCMTList, function (factAcctCMT, i) {

            thisObject.GetDetailDocument(factAcctCMT.fact_acct_wx_tablename, factAcctCMT.fact_acct_wx_tableuuid).then(function (results) {
                let result = results[0];
                factAcctCMT.document_no = result.document_no;
                factAcctCMT.document_type = result.document_type;
                // datatableReference.cell(i, indexDocumentType).data(result.document_type);
                // datatableReference.cell(i, indexDocumentNo).data(result.document_no);
                datatableReference.row(i).data(factAcctCMT);


            })

        });

    }

    UpdateDocumentNoAndType(factAcctCMT) {

        return new Promise(function (resolve, reject) {

            thisObject.GetDetailDocument(factAcctCMT.fact_acct_wx_tablename, factAcctCMT.fact_acct_wx_tableuuid).then(function (results) {
                let result = results[0];
                factAcctCMT.document_no = result.document_no;
                factAcctCMT.document_type = result.document_type;
                resolve(factAcctCMT);
            })

        })

    }

    /**
       * Find All warehouse dengan bentuk warehouseviewmodel
       * @param {*} metaDataResponseTable 
       */
    FindAll(metaDataResponseTable) {


        let searchFilter = '';
        if (metaDataResponseTable.search !== '') {
            searchFilter += ' AND ' + metaDataResponseTable.searchField + '  LIKE    \'%' + metaDataResponseTable.search + '%\'';
        }

        let limit = '';
        if ((metaDataResponseTable.page !== null) && (metaDataResponseTable.perpage !== null)) {

            limit = ' LIMIT ' + metaDataResponseTable.page + ',' + metaDataResponseTable.perpage;

        }



        const sqlTemplate = ' SELECT fact_acct_uu fact_acct_wx_fact_acct_uu, accounttype fact_acct_wx_accounttype, ' +
            ' c_elementvalue fact_acct_wx_c_elementvalue, tablename fact_acct_wx_tablename, tableuuid fact_acct_wx_tableuuid , ' +
            ' amtacctdr fact_acct_wx_amtacctdr, amtacctcr  fact_acct_wx_amtacctcr, ' +
            ' \'\' document_type, \'\' document_no ,  docstatus fact_acct_wx_docstatus, updated fact_acct_wx_updated  FROM fact_acct ' +
            ' WHERE fact_acct.ad_org_uu = \'' + metaDataResponseTable.ad_org_uu + '\'' + searchFilter +
            ' ORDER BY fact_acct.updated DESC ' + limit;

        //   const listMetaColumn = HelperService.ConvertViewModelToMetaColumn({ fact_acct: new FactAcct() });

        //   let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.DirectQuerySqlStatement(sqlTemplate);
    }


    /**
     * Biasa dipergunakan untuk menghitung current balance bank
     * @param {*} tableuuid 
     * @param {*} c_elementvalue 
     * @param {*} docstatus 
     */
    FindFactAcct(tableuuid, c_elementvalue, docstatus) {

        const sqlTemplate = ' SELECT fact_acct_uu fact_acct_wx_fact_acct_uu, accounttype fact_acct_wx_accounttype, ' +
            ' c_elementvalue fact_acct_wx_c_elementvalue, tablename fact_acct_wx_tablename, tableuuid fact_acct_wx_tableuuid , ' +
            ' amtacctdr fact_acct_wx_amtacctdr, amtacctcr  fact_acct_wx_amtacctcr, docstatus fact_acct_wx_docstatus, updated fact_acct_wx_updated  FROM fact_acct ' +
            ' WHERE fact_acct.tableuuid = \'' + tableuuid + '\' AND fact_acct.c_elementvalue = \'' + c_elementvalue + '\''
            + ' AND fact_acct.docstatus = \'' + docstatus + '\'';


        return apiInterface.DirectQuerySqlStatement(sqlTemplate);


    }

    /**
     * Biasa dipergunakan untuk menghitung current balance bank
     * @param {*} tableuuid 
     * @param {*} c_elementvalue 
     * @param {*} docstatus 
     */
    FindFactAcctUu(fact_acct_uu) {

        const sqlTemplate = ' SELECT fact_acct_uu fact_acct_wx_fact_acct_uu, accounttype fact_acct_wx_accounttype, ' +
            ' c_elementvalue fact_acct_wx_c_elementvalue, tablename fact_acct_wx_tablename, tableuuid fact_acct_wx_tableuuid , ' +
            ' amtacctdr fact_acct_wx_amtacctdr, amtacctcr  fact_acct_wx_amtacctcr, docstatus fact_acct_wx_docstatus, updated fact_acct_wx_updated  FROM fact_acct ' +
            ' WHERE fact_acct_uu = \'' + fact_acct_uu + '\'';


        return apiInterface.DirectQuerySqlStatement(sqlTemplate);


    }

    /**
       * 1. Cari periode di table c_period, dan beririsan dengan table summary
       * 
       * return array Promise, untuk di run secara sequentiall
       */
    GetProcessClosingPeriod() {
        let thisObject = this;
        const sql = 'SELECT c_period.name,c_period.start_timestamp, c_period.end_timestamp, summary.name AS summary_period_name FROM c_period LEFT JOIN fact_acct_summary summary ' +
            ' ON c_period.name = summary.name ORDER BY c_period.start_timestamp ASC ';
        return new Promise(function (resolve, reject) {
            apiInterface.DirectQuerySqlStatement(sql).then(function (list_period_name) {
                resolve(list_period_name);
            });

        }).then(function (list_period_name) {

            let currentTimestamp = new Date().getTime();
            return new Promise(function (resolve, reject) {
                let arrayPromise = [];
                _.forEach(list_period_name, function (period_and_summary, index) {

                    let end_timestamp = numeral(period_and_summary.end_timestamp).value();
                    //jika current timestamp lebih besar dari end_timestamp, maka create process closing periode
                    if (currentTimestamp >= end_timestamp) {
                        // jika tidak ada , atau summary.c_period_name = null, berarti belum pernah dibuat closing periodnya
                        if (!period_and_summary.summary_period_name) {
                            let previousIndex = index - 1;
                            let previous_period_name = null;
                            if (previousIndex >= 0) {
                                previous_period_name = list_period_name[previousIndex];
                            }
                            let functionSummaryElementValue = function () {

                                return thisObject.CreateAndSaveSummaryElementValue(period_and_summary.name, previous_period_name);
                            }
                            arrayPromise.push(functionSummaryElementValue);
                            //check await in loop
                        }

                    }

                });
                resolve(arrayPromise);

            });



        });
    }

    /**
     * Run process closing period secara sequentially, karena ketika user tidak pernah membuka aplikasi maka , 
     * tidak pernah closing period
     * @param {*} arrayPromise 
     */
    RunProcessClosingPeriod(arrayPromise) {
        return arrayPromise.reduce((chain, promiseFunction, currentIndex) => {
            // const func = this[action];

            return chain.then(() => promiseFunction()).then(insertSuccess => console.log(insertSuccess));
        }, Promise.resolve());
    }


    /**
     * Find FactAcct Summary dari period yang dicari dalam create report
     * 1. Jika ada period, maka 
     *     1.1. Check di fact_acct_summary, 
     *     1.1.1 Jika ada di fact_acct_summary return element_value_cmt_list
     *     1.1.2 Jika tidak ada di fact_acct_summary, 
     *          maka ambil current TotalElementValue dan previous fact_acct_summary
     * 2. Jika tidak ada period
     *      return no_period
     * @param {*} period 
     * return { data: element_value_cmt_list, }
     */
    FindFactAcctSummary(period) {
        let thisObject = this;

        return new Promise(function (resolve, reject) {
            let period_service = new PeriodService();
            period_service.FindPeriodAndPreviousByName(period).then(function (list_period) {

                resolve(list_period);

            });

        }).then(function (list_period) {

            return new Promise(function (resolve, reject) {

                if (list_period.length > 0) {
                    let next_period_name = list_period[0]['name'];
                    let previous_period_name = null;
                    if (list_period.length > 1) {
                        previous_period_name = list_period[1]['name'];

                    }

                    thisObject.GetSummaryTotalElementValue(next_period_name, previous_period_name).then(function (summary_element) {
                        resolve(summary_element);
                    });


                } else {
                    resolve([]);
                }




            })

        });
    }




    /**
     * 1. Pilih element value dan sum (Dr), SUM(cr) di table fact_acct, dengan menggunakan next_period_name
     * 2. Pilih summary elementvalue di table fact_acctz_summary dengan previous_periode, 
     * 3. Jumlahkan amount dari fact_acctz_summary ke summary element value dari fact_acct, dan hanya parent_value 10000,2000,300000, karena tipe akun
     * ini yang akan dibawa terus tiap periode
     * 
     * Get RawSummary Total Element Value dari database dan telah dijumlahkan dengan previous periode
     * , tetapi belum dijadikan hierarchy
     * raw summary akan dibuat jika currenttimestamp masih lebih besar daripada end_timestamp
     * @param {*} periode 
     */
    CreateAndSaveSummaryElementValue(next_period_name, previous_period_name) {
        let thisObject = this;
        let lastTimeStamp = HelperService.ConvertPeriodeToStartimeToEndTime(next_period_name);


        return new Promise(function (resolve, reject) {
            /*  apiInterface.DirectQuerySqlStatement(sqlTemplate).then(function (elementTotalCMTList) {
  
                  resolve(elementTotalCMTList);
              });*/
            thisObject.GetSummaryTotalElementValue(next_period_name, previous_period_name).then(function (treeSummaryElementValue) {

                let elementTotalCMTList = treeSummaryElementValue.flat(Infinity);
                resolve(elementTotalCMTList);

            });


        }).then(function (elementTotalCMTList) {

            return new Promise(function (resolve, reject) {
                // antisipasi ketika period yang pertama kali dicreate, maka previous_period_name pasti tidak ada
                if (previous_period_name) {
                    const sqlTemplate = 'SELECT value, amount FROM fact_acct_summary ' +
                        ' WHERE c_period_name = ' + previous_period_name + ' ORDER BY value ASC';
                    apiInterface.DirectQuerySqlStatement(sqlTemplate).then(function (fact_acct_summary_list) {

                        let cloneElementTotalCMTList = JSON.parse(JSON.stringify(elementTotalCMTList));
                        let typeElementFilter = ['1', '2', '3'];// asset, liability, equity
                        _.forEach(elementTotalCMTList, function (elementTotalCMT, index) {

                            if (typeElementFilter.includes(elementTotalCMT.charAt(0))) {

                                let fact_acct_summary_found = _.find(fact_acct_summary_list, function (fact_acct_summary) { return fact_acct_summary.value === elementTotalCMT.value; });
                                if (fact_acct_summary_found) {
                                    let amountElementTotalCMT = numeral(fact_acct_summary_found.amount).value() + numeral(elementTotalCMT.amount).value();
                                    cloneElementTotalCMTList[index]['amount'] = amountElementTotalCMT;
                                }
                            }

                        });

                        resolve(cloneElementTotalCMTList);

                    });


                } else {
                    resolve(elementTotalCMTList);
                }


            });

        }).then(function (cloneElementTotalCMTList) {

            return new Promise(function (resolve, reject) {

                let listSqlStatement = [];
                _.forEach(cloneElementTotalCMTList, function (element_total) {

                    let data = {
                        start_timestamp: lastTimeStamp.start_timestamp, end_timestamp: lastTimeStamp.end_timestamp,
                        value: element_total.value, fact_acct_summary_uu: HelperService.UUID(), amount: element_total.amount,
                        name: element_total.name, parent_value: element_total.parent_value, account_type: element_total.account_type, c_period_name: next_period_name
                    };

                    let sqlStatement = HelperService.SqlInsertOrReplaceStatement("fact_acct_summary", data);
                    listSqlStatement.push(sqlStatement);
                });
                apiInterface.ExecuteBulkSqlStatement(listSqlStatement).then(function (inserted) {

                    resolve(inserted);
                })

            });


        });

    }




    /**
     * 1. Get Summary total dari fact_acct dan element_value
     * 2. Get previous period di fact_acct_summary
     * 3. DIgabungkan amount di fact_acct_summary, ke fact_acct
     * @param {*} periode 
     * return element_total_CMT_list
     */
    GetSummaryTotalElementValue(periode_name, previous_period_name) {


        let objStartEndTime = HelperService.ConvertPeriodeToStartimeToEndTime(periode_name);

        const sqlTemplate = 'SELECT element.value, element.name, element.parent_value, ' +
            ' element.value || \' \' || element.name AS text , element.account_type,   acct.dr, acct.cr  FROM c_elementvalue element LEFT JOIN ' +
            ' (SELECT acct.c_elementvalue, SUM(acct.amtacctdr) AS dr, SUM(acct.amtacctcr) cr FROM fact_acct acct ' +
            '  WHERE acct.c_period_name = \'' + periode_name + '\' ' +
            ' GROUP BY acct.c_elementvalue ) acct   ON element.value = acct.c_elementvalue    ORDER BY element.value ASC ;';

        let elementValueService = new ElementValueService();

        let elementTotal = {};
        return new Promise(function (resolve, reject) {
            apiInterface.DirectQuerySqlStatement(sqlTemplate).then(function (elementTotalCMTList) {
                let treeSummaryElementValue = elementValueService.CreateHierarchyElementValue(elementTotalCMTList, "value",
                    "parent_value", "children");

                _.forEach(treeSummaryElementValue, function (objElementValue) {

                    elementValueService.SumUpAmount(objElementValue);

                });
                //  let elementTotalCMTListFlat = treeSummaryElementValue.flat(Infinity);
                let elementTotalCMTListFlat = elementValueService.ConvertHierarchyToFlat(treeSummaryElementValue, []);
                resolve(elementTotalCMTListFlat);
            });


        }).then(function (elementTotalCMTList) {

            return new Promise(function (resolve, reject) {
                // antisipasi ketika period yang pertama kali dicreate, maka previous_period_name pasti tidak ada
                if (previous_period_name) {
                    const sqlTemplate = 'SELECT value, amount FROM fact_acct_summary ' +
                        ' WHERE c_period_name = ' + previous_period_name + ' ORDER BY value ASC';
                    apiInterface.DirectQuerySqlStatement(sqlTemplate).then(function (fact_acct_summary_list) {

                        let cloneElementTotalCMTList = JSON.parse(JSON.stringify(elementTotalCMTList));
                        let typeElementFilter = ['1', '2', '3'];// asset, liability, equity
                        _.forEach(elementTotalCMTList, function (elementTotalCMT, index) {

                            if (typeElementFilter.includes(elementTotalCMT.charAt(0))) {

                                let fact_acct_summary_found = _.find(fact_acct_summary_list, function (fact_acct_summary) { return fact_acct_summary.value === elementTotalCMT.value; });
                                if (fact_acct_summary_found) {
                                    let amountElementTotalCMT = numeral(fact_acct_summary_found.amount).value() + numeral(elementTotalCMT.amount).value();
                                    cloneElementTotalCMTList[index]['amount'] = amountElementTotalCMT;
                                }
                            }

                        });

                        resolve(cloneElementTotalCMTList);

                    });


                } else {
                    resolve(elementTotalCMTList);
                }


            });

        })





    }



    GetDetailDocument(tablename, table_uuid) {
        let sql = '';
        switch (tablename) {
            case 'm_inoutline':
                sql = "SELECT m_inout.documentno document_no,c_doctype.name document_type FROM m_inoutline " +
                    " INNER JOIN m_inout ON m_inoutline.m_inout_uu = m_inout.m_inout_uu " +
                    " INNER JOIN c_doctype ON c_doctype.c_doctype_id = m_inout.c_doctype_id " +
                    " WHERE m_inoutline.m_inoutline_uu = '" + table_uuid + "';";
                break;
            case 'c_invoice':
                sql = "SELECT c_invoice.documentno document_no, c_doctype.name document_type FROM c_invoice " +
                    " INNER JOIN c_doctype ON c_doctype.c_doctype_id = c_invoice.c_doctype_id " +
                    " WHERE c_invoice.c_invoice_uu = '" + table_uuid + "';";
                break;
            case 'c_invoiceline':
                sql = "SELECT c_invoice.documentno document_no, c_doctype.name document_type FROM c_invoiceline " +
                    " INNER JOIN c_invoice ON c_invoiceline.c_invoice_uu = c_invoice.c_invoice_uu " +
                    " INNER JOIN c_doctype ON c_doctype.c_doctype_id = c_invoice.c_doctype_id " +
                    " WHERE c_invoiceline.c_invoiceline_uu = '" + table_uuid + "';";
                break;
            case 'c_payment':
                sql = "SELECT c_payment.documentno document_no, c_doctype.name document_type FROM c_payment " +
                    " INNER JOIN c_doctype ON c_doctype.c_doctype_id = c_payment.c_doctype_id " +
                    " WHERE c_payment.c_payment_uu = '" + table_uuid + "';";
                break;
            case 'c_bankstatementline':
                sql = "SELECT 'Bank/Cash Statement' document_no, 'Bank/Cash Statement' document_type ";
                break;
            case 'm_inventoryline':
                sql = "SELECT m_inventory.documentno document_no, c_doctype.name document_type FROM m_inventoryline " +
                    " INNER JOIN m_inventory ON m_inventoryline.m_inventory_uu = m_inventory.m_inventory_uu " +
                    " INNER JOIN c_doctype ON c_doctype.c_doctype_id = m_inventory.c_doctype_id " +
                    " WHERE m_inventoryline.m_inventoryline_uu = '" + table_uuid + "';";
                break;
            case 'm_movementline':
                sql = "SELECT m_movement.documentno document_no, c_doctype.name document_type FROM m_movementline " +
                    " INNER JOIN m_movement ON m_movementline.m_movement_uu = m_movement.m_movement_uu " +
                    " INNER JOIN c_doctype ON c_doctype.c_doctype_id = m_movement.c_doctype_id " +
                    " WHERE m_movementline.m_movementline_uu = '" + table_uuid + "';";
                break;
            case 'm_productionline':
                sql = "SELECT m_production.documentno document_no, c_doctype.name document_type FROM m_productionline " +
                    " INNER JOIN m_production ON m_productionline.m_production_uu = m_production.m_production_uu " +
                    " INNER JOIN c_doctype ON c_doctype.c_doctype_id = m_production.c_doctype_id " +
                    " WHERE m_productionline.m_productionline_uu = '" + table_uuid + "';";
                break;
            case 'm_production':
                sql = "SELECT m_production.documentno document_no, c_doctype.name document_type FROM m_production " +
                    " INNER JOIN c_doctype ON c_doctype.c_doctype_id = m_production.c_doctype_id " +
                    " WHERE m_production.m_production_uu = '" + table_uuid + "';";
                break;
            default:
                sql = "";
                break;

        }

        if (sql) {
            return apiInterface.DirectQuerySqlStatement(sql);

        } else {

            return new Promise(function (resolve, reject) {
                resolve([{ document_no: '', document_type: '' }]);

            });

        }



    }









    /**
     * Return material 
     * Dr                   Cr
     * 14120-Product Asset	51100 – CoGS Product
     * 
     * @param {RmalineComponentModelTable} rmaCMT 
     */
    RmaCompleted(rmalineCMT) {
        let elementValueService = new ElementValueService();
        let costService = new CostService();


        costService.FindByMProductUuAndCostElement(rmalineCMT.m_rmaline_wx_m_product_uu,
            elementValueService.CostElement().LastPO).then(function (costMProductList) {
                let costMProduct = costMProductList[0];
                let coGsProduct = costMProduct.m_cost_wx_currentcostprice * rmalineCMT.m_rmaline_wx_movementqty;
                //DR
                this.sqlInsertFactAcct(elementValueService.ElementValueList().ProductAsset,
                    'm_rmaline', rmalineCMT.m_rmaline_wx_m_rmaline_uu, coGsProduct, null);

                //CR
                this.sqlInsertFactAcct(elementValueService.ElementValueList().CoGSProduct,
                    'm_rmaline', rmalineCMT.m_rmaline_wx_m_rmaline_uu, null, coGsProduct);



            });



    }





    /**
    * 
   
    * @param tableName nama table m_inoutline, atau c_invoiceline,
    * @param tableUuid uuid dari row , 
    * @param cElementvalue cElementvalue
    */
    sqlInsertReverseFactAcct(tableName, tableUuid, cElementvalue, dateacct) {

        let sqlStatement = this.sqlInsertReverseFactAcctStatement(tableName, tableUuid, cElementvalue, dateacct);

        return apiInterface.ExecuteSqlStatement(sqlStatement);
    }

    sqlInsertReverseFactAcctStatement(tableName, tableUuid, cElementvalue, dateacct) {
        if (!dateacct) {
            dateacct = new Date().getTime();
        }
        const c_period_name = localStorage.getItem('current_period');
        const updated = new Date().getTime();
        const updatedby = localStorage.getItem('ad_user_uu');
        let sqlStatement = 'INSERT OR REPLACE INTO fact_acct(fact_acct_uu, accounttype, c_elementvalue, ' +
            ' tablename, tableuuid, docstatus, amtacctcr, amtacctdr , created, updated, createdby, updatedby, ad_org_uu, ad_client_uu,dateacct,c_period_name) ' +
            ' SELECT  \'' + HelperService.UUID() + '\', accounttype, c_elementvalue, ' +
            ' tablename, tableuuid, \'REVERSED\' , amtacctdr , amtacctcr, \'' + updated + '\' , \'' + updated + '\', \'' + updatedby + '\', \'' + updatedby + '\', ad_org_uu, ad_client_uu, ' + dateacct + ',' + c_period_name + ' FROM fact_acct ' +
            ' WHERE docstatus = \'COMPLETED\' AND tablename = \'' + tableName + '\' AND tableuuid = \'' + tableUuid + '\'  AND c_elementvalue = \'' + cElementvalue + '\' ;';

        return sqlStatement;
    }

    /**
     * Obsolete akan diganti dengan SqlInsertFactAcct
     * @param {*} elementValue 
     * @param {*} tableName 
     * @param {*} tableUuid 
     * @param {*} amtDr 
     * @param {*} amtCr 
     * @param {*} dateacct 
     * @param {*} fact_acct_uu 
     */
    sqlInsertFactAcct(elementValue, tableName, tableUuid, amtDr, amtCr, dateacct, fact_acct_uu) {

        let sqlInsertFactAcct = this.sqlInsertFactAcctStatement(elementValue, tableName, tableUuid, amtDr, amtCr, dateacct, fact_acct_uu);
        return apiInterface.ExecuteSqlStatement(sqlInsertFactAcct);

    }

    /**
     * Fungsi ini akan obsolete, diganti dengan SqlInsertFactAcctStatement
     * @param {*} elementValue {accountType:xxxx, value:yyyyy}
     * @param {*} tableName 
     * @param {*} tableUuid 
     * @param {*} amtDr 
     * @param {*} amtCr 
     * @param {*} dateacct 
     * @param {*} fact_acct_uu  optional, jika tidak ada, akan dicreate, berguna , jika ingin mendapatkan nilai uuid yang akan dimasukkan, karena returnya hanya insertSuccess, bukan uuid
     */
    sqlInsertFactAcctStatement(elementValue, tableName, tableUuid, amtDr, amtCr, dateacct, fact_acct_uu) {
        const timeNow = new Date().getTime();

        // let elementValueService = new ElementValueService();
        const factacct = new FactAcct();
        factacct.fact_acct_uu = HelperService.UUID();
        //jika ada nilai dari luar function, maka dipakai yang dari luar
        if (fact_acct_uu) {
            factacct.fact_acct_uu = fact_acct_uu;

        }

        factacct.ad_client_uu = localStorage.getItem('ad_client_uu');
        factacct.ad_org_uu = localStorage.getItem('ad_org_uu');

        factacct.createdby = localStorage.getItem('ad_user_uu');
        factacct.updatedby = localStorage.getItem('ad_user_uu');

        factacct.created = timeNow;
        factacct.updated = timeNow;

        factacct.accounttype = elementValue.accountType;
        factacct.c_elementvalue = elementValue.value;
        factacct.tablename = tableName;
        factacct.tableuuid = tableUuid;
        factacct.docstatus = 'COMPLETED';
        factacct.amtacctdr = amtDr;
        factacct.amtacctcr = amtCr;
        if (!dateacct) {
            dateacct = new Date().getTime();
        }
        factacct.dateacct = dateacct;

        factacct.c_period_name = localStorage.getItem('current_period');

        return HelperService.SqlInsertOrReplaceStatement('fact_acct', factacct);

    }

    /**
     * 
     * @param {*} c_elementvalue_wx_value 
     * @param {*} tableName 
     * @param {*} tableUuid 
     * @param {*} amtDr 
     * @param {*} amtCr 
     * @param {*} dateacct optional 
     * @param {*} fact_acct_uu optional
     */

    SqlInsertFactAcct(c_elementvalue_wx_value, tableName, tableUuid, amtDr, amtCr, dateacct, fact_acct_uu) {

        let sqlInsertFactAcct = this.SqlInsertFactAcctStatement(c_elementvalue_wx_value, tableName, tableUuid, amtDr, amtCr, dateacct, fact_acct_uu);
        return apiInterface.ExecuteSqlStatement(sqlInsertFactAcct);

    }

    /**
     * function sqlInsertFactAcctStatement akan diganti dengan fungsi ini.
     * @param {*} c_elementvalue_wx_value 
     * @param {*} tableName 
     * @param {*} tableUuid 
     * @param {*} amtDr 
     * @param {*} amtCr 
     * @param {*} dateacct optional, jika tidak ada akan di create sendiri
     * @param {*} fact_acct_uu  optional, jika tidak ada, akan dicreate, berguna , jika ingin mendapatkan nilai uuid yang akan dimasukkan, karena returnya hanya insertSuccess, bukan uuid
     */
    SqlInsertFactAcctStatement(c_elementvalue_wx_value, tableName, tableUuid, amtDr, amtCr, dateacct, fact_acct_uu) {
        const timeNow = new Date().getTime();

        // let elementValueService = new ElementValueService();
        const factacct = new FactAcct();
        factacct.fact_acct_uu = HelperService.UUID();
        //jika ada nilai dari luar function, maka dipakai yang dari luar
        if (fact_acct_uu) {
            factacct.fact_acct_uu = fact_acct_uu;
        }

        factacct.ad_client_uu = localStorage.getItem('ad_client_uu');
        factacct.ad_org_uu = localStorage.getItem('ad_org_uu');

        factacct.createdby = localStorage.getItem('ad_user_uu');
        factacct.updatedby = localStorage.getItem('ad_user_uu');

        factacct.created = timeNow;
        factacct.updated = timeNow;

        // factacct.accounttype = elementValue.accountType;
        factacct.c_elementvalue = c_elementvalue_wx_value;
        factacct.tablename = tableName;
        factacct.tableuuid = tableUuid;
        factacct.docstatus = 'COMPLETED';
        factacct.amtacctdr = amtDr;
        factacct.amtacctcr = amtCr;
        if (!dateacct) {
            dateacct = new Date().getTime();
        }
        factacct.dateacct = dateacct;
        factacct.c_period_name = localStorage.getItem('current_period');

        return HelperService.SqlInsertOrReplaceStatement('fact_acct', factacct);

    }

    /*
    
    ProcessingClosingPeriode() {
        const sql = "SELECT DISTINCT start_timestamp, end_timestamp FROM coa_record ORDER BY start_timestamp DESC ";
        const currentTimestamp = new Date().getTime();

        return new Promise(function (resolve, reject) {
            apiInterface.DirectQuerySqlStatement(sql).then(function (timestampList) {
                resolve(timestampList);
            });

        }).then(function (timestampList) {
            if (timestampList.length) {
                return new Promise(function (resolve, reject) {
                    let lastTimestampRecord = timestampList[0];

                });

            } else {
                return new Promise(function (resolve, reject) {


                });

            }
        })
    }

    async  CreateSummaryTotalElementValue(lastTimestampRecord) {
        let thisObject = this;
        const currentTimestamp = new Date().getTime();
        const periode = HelperService.ConvertTimeStampToPeriode(lastTimestampRecord);

        while (currentTimestamp > lastTimestampRecord.end_timestamp) {

            if (periode.month == 11) {
                periode.month = 0;
                periode.year += 1;
            } else {
                periode.month += 1;
            }

            let formatPeriode = periode.month + '-' + periode.year;
            lastTimestampRecord = HelperService.ConvertPeriodeToStartimeToEndTime(formatPeriode);
            if (lastTimestampRecord.end_timestamp < currentTimestamp) {

                await thisObject.CreateAndSaveSummaryElementValue(formatPeriode);

            }


        }


    }
    
    
    
    
    SaveUpdate(factAcct) {
        const timeNow = new Date().getTime();
        let thisObject = this;
        let insertSuccess = 0;
        try {
            if (!m_cost.m_cost_uu) { //new record
 
                m_cost.m_cost_uu = HelperService.UUID();
                m_cost.ad_client_uu = localStorage.getItem('ad_client_uu');
                m_cost.ad_org_uu = localStorage.getItem('ad_org_uu');
 
                m_cost.createdby = localStorage.getItem('ad_user_uu');
                m_cost.updatedby = localStorage.getItem('ad_user_uu');
 
                m_cost.created = timeNow;
                m_cost.updated = timeNow;
            } else { //update record
 
 
                m_cost.updatedby = localStorage.getItem('ad_user_uu');
 
                m_cost.updated = timeNow;
            }
 
            m_cost.sync_client = null;
            m_cost.process_date = null;
 
 
            const sqlInsert = HelperService.SqlInsertOrReplaceStatement('m_cost', m_cost);
 
            return new Promise(function (resolve, reject) {
                apiInterface.ExecuteSqlStatement(sqlInsert).then(function (rowChanged) {
 
                    insertSuccess += rowChanged;
                    resolve(insertSuccess);
 
                });
 
 
 
            });
 
        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.SaveUpdate.name, error);
            return new Promise(function (resolve, reject) {
                resolve(insertSuccess);
 
            });
        }
 
 
 
 
    }*/


    /* const timeNow = new Date().getTime();
     let insertSuccess = 0;
     let elementValueService = new ElementValueService();
     //CREDIT
     const factacctCr = new FactAcct();
 
     factacctCr.fact_acct_uu = HelperService.UUID();
     factacctCr.ad_client_uu = localStorage.getItem('ad_client_uu');
     factacctCr.ad_org_uu = localStorage.getItem('ad_org_uu');
 
     factacctCr.createdby = localStorage.getItem('ad_user_uu');
     factacctCr.updatedby = localStorage.getItem('ad_user_uu');
 
     factacctCr.created = timeNow;
     factacctCr.updated = timeNow;
 
     factacctCr.accounttype = elementValueService.ElementValueList().AccountPayableTrade.accountType;
     factacctCr.c_elementvalue = elementValueService.ElementValueList().AccountPayableTrade.value;
     factacctCr.tablename = 'c_invoice';
     factacctCr.tableuuid = invoiceComponentModelTable.c_invoice_wx_c_invoice_uu;
     factacctCr.docstatus = 'COMPLETED';
     factacctCr.amtacctcr = invoiceComponentModelTable.c_invoice_wx_grandtotal;
 
     const sqlInsertCr = HelperService.SqlInsertOrReplaceStatement('fact_acct', factacctCr);
 
     insertSuccess += apiInterface.ExecuteSqlStatement(sqlInsertCr);
     // each list of invoiceline
     let invoicelineService = new InvoicelineService();
 
     invoicelineService.FindInvoicelineComponentModelByCInvoiceUu(invoiceComponentModelTable.c_invoice_wx_c_invoice_uu)
         .then(function (invoicelineComponentModelTableList) {
 
             _.forEach(invoicelineComponentModelTableList, function (invoicelineComponentModelTable) {
                 //DEBIT 
                 // NOT INVOICED RECEIPT
                 const factacctDr = new FactAcct();
                 let elementValueService = new ElementValueService();
                 factacctDr.fact_acct_uu = HelperService.UUID();
                 factacctDr.ad_client_uu = localStorage.getItem('ad_client_uu');
                 factacctDr.ad_org_uu = localStorage.getItem('ad_org_uu');
 
                 factacctDr.createdby = localStorage.getItem('ad_user_uu');
                 factacctDr.updatedby = localStorage.getItem('ad_user_uu');
 
                 factacctDr.created = timeNow;
                 factacctDr.updated = timeNow;
 
                 factacctDr.accounttype = elementValueService.ElementValueList().NotInvoicedReceipt.accountType;
                 factacctDr.c_elementvalue = elementValueService.ElementValueList().NotInvoicedReceipt.value;
                 factacctDr.tablename = 'c_invoiceline';
                 factacctDr.tableuuid = invoicelineComponentModelTable.c_invoiceline_wx_c_invoiceline_uu;
                 factacctDr.docstatus = 'COMPLETED';
                 factacctDr.amtacctdr = invoicelineComponentModelTable.c_invoiceline_wx_linenetamt;
 
                 const sqlInsertDr = HelperService.SqlInsertOrReplaceStatement('fact_acct', factacctDr);
 
                 insertSuccess += apiInterface.ExecuteSqlStatement(sqlInsertDr);
 
                 if (invoicelineComponentModelTable.c_invoiceline_wx_taxamt) {
                     // TAX CREDIT A/R
                     const factacctDrTaxCredit = new FactAcct();
                     factacctDrTaxCredit.fact_acct_uu = HelperService.UUID();
                     factacctDrTaxCredit.ad_client_uu = localStorage.getItem('ad_client_uu');
                     factacctDrTaxCredit.ad_org_uu = localStorage.getItem('ad_org_uu');
 
                     factacctDrTaxCredit.createdby = localStorage.getItem('ad_user_uu');
                     factacctDrTaxCredit.updatedby = localStorage.getItem('ad_user_uu');
 
                     factacctDrTaxCredit.created = timeNow;
                     factacctDrTaxCredit.updated = timeNow;
 
                     factacctDrTaxCredit.accounttype = elementValueService.ElementValueList().TaxCreditAR.accountType;
                     factacctDrTaxCredit.c_elementvalue = elementValueService.ElementValueList().TaxCreditAR.value;
                     factacctDrTaxCredit.tablename = 'c_invoiceline';
                     factacctDrTaxCredit.tableuuid = invoicelineComponentModelTable.c_invoiceline_wx_c_invoiceline_uu;
                     factacctDrTaxCredit.docstatus = 'COMPLETED';
                     factacctDrTaxCredit.amtacctdr = invoicelineComponentModelTable.c_invoiceline_wx_taxamt;
 
                     const sqlInsertDrTaxCredit = HelperService.SqlInsertOrReplaceStatement('fact_acct', factacctDrTaxCredit);
 
                     insertSuccess += apiInterface.ExecuteSqlStatement(sqlInsertDrTaxCredit);
                 }
 
 
 
             });
 
         });*/


    /**
   * Create FactAcct berdasarkan material out completed
   
     MaterialOutCompleted(productAssetValue, m_inoutline_uu) {
 
 
 
         const timeNow = new Date().getTime();
 
         //DEBIT 
         const factacctDr = new FactAcct();
         let elementValueService = new ElementValueService();
         factacctDr.fact_acct_uu = HelperService.UUID();
         factacctDr.ad_client_uu = localStorage.getItem('ad_client_uu');
         factacctDr.ad_org_uu = localStorage.getItem('ad_org_uu');
 
         factacctDr.createdby = localStorage.getItem('ad_user_uu');
         factacctDr.updatedby = localStorage.getItem('ad_user_uu');
 
         factacctDr.created = timeNow;
         factacctDr.updated = timeNow;
 
         factacctDr.accounttype = elementValueService.ElementValueList().CoGSProduct.accountType;
         factacctDr.c_elementvalue = elementValueService.ElementValueList().CoGSProduct.value;
         factacctDr.tablename = 'm_inoutline';
         factacctDr.tableuuid = m_inoutline_uu;
         factacctDr.docstatus = 'COMPLETED';
         factacctDr.amtacctdr = productAssetValue;
 
         //CREDIT
         const factacctCr = new FactAcct();
 
         factacctCr.fact_acct_uu = HelperService.UUID();
         factacctCr.ad_client_uu = localStorage.getItem('ad_client_uu');
         factacctCr.ad_org_uu = localStorage.getItem('ad_org_uu');
 
         factacctCr.createdby = localStorage.getItem('ad_user_uu');
         factacctCr.updatedby = localStorage.getItem('ad_user_uu');
 
         factacctCr.created = timeNow;
         factacctCr.updated = timeNow;
 
         factacctCr.accounttype = elementValueService.ElementValueList().ProductAsset.accountType;
         factacctCr.c_elementvalue = elementValueService.ElementValueList().ProductAsset.value;
         factacctCr.tablename = 'm_inoutline';
         factacctCr.tableuuid = m_inoutline_uu;
         factacctCr.docstatus = 'COMPLETED';
         factacctCr.amtacctcr = productAssetValue;
 
         let insertSuccess = 0;
         const sqlInsertDr = HelperService.SqlInsertOrReplaceStatement('fact_acct', factacctDr);
         const sqlInsertCr = HelperService.SqlInsertOrReplaceStatement('fact_acct', factacctCr);
 
         let arraySql = [sqlInsertDr, sqlInsertCr];
 
 
         return apiInterface.ExecuteBulkSqlStatement(arraySql);
 
     }
 */
}