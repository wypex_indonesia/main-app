

class OrderlineService {
    constructor() { }


    /**
	 * 
	 * @param {*} metaDataResponseTable 
	 */
    FindAll(metaDataResponseTable) {


        let otherFilters = '';
        if (metaDataResponseTable.otherFilters.length > 0) {

            _.forEach(metaDataResponseTable.otherFilters, function (value) {

                otherFilters += ' ' + value + ' ';
            });

        }

        const sqlTemplate = '  FROM c_orderline ' +
            ' INNER JOIN c_order ON c_orderline.c_order_uu = c_order.c_order_uu ' +
            ' INNER JOIN m_product ON m_product.m_product_uu  = c_orderline.m_product_uu ' +
            ' INNER JOIN c_uom ON c_orderline.c_uom_uu = c_uom.c_uom_uu ' +
            ' INNER JOIN c_tax ON c_tax.c_tax_uu = c_orderline.c_tax_uu ' +
            ' WHERE c_orderline.wx_isdeleted = \'N\' ' + otherFilters + ' ORDER BY c_orderline.line ASC ';



        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new OrderlineViewModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);

    }

    FindOrderlineViewModelByCOrderlineUu(c_orderline_uu) {

        const sqlTemplate = '  FROM c_orderline ' +
            ' INNER JOIN c_order ON c_orderline.c_order_uu = c_order.c_order_uu ' +
            ' INNER JOIN m_product ON m_product.m_product_uu  = c_orderline.m_product_uu ' +
            ' INNER JOIN c_uom ON c_orderline.c_uom_uu = c_uom.c_uom_uu ' +
            ' INNER JOIN c_tax ON c_tax.c_tax_uu = c_orderline.c_tax_uu ' +
            ' WHERE c_orderline.c_orderline_uu = \'' + c_orderline_uu + '\'';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new OrderlineViewModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }

    FindOrderlineCMTListByCOrderlineUu(listCOrderlineUu) {
        let arrayStringCOrderlineUu = [];
        _.forEach(listCOrderlineUu, function (c_orderline_uu) {

            let c_orderline_uu_string = '\'' + c_orderline_uu + '\'';
            arrayStringCOrderlineUu.push(c_orderline_uu_string);
        });

        let inClause = '(' + arrayStringCOrderlineUu.join(',') + ')';
        const sqlTemplate = '  FROM c_orderline ' +
            ' INNER JOIN c_order ON c_orderline.c_order_uu = c_order.c_order_uu ' +
            ' INNER JOIN m_product ON m_product.m_product_uu  = c_orderline.m_product_uu ' +
            ' INNER JOIN c_uom ON c_orderline.c_uom_uu = c_uom.c_uom_uu ' +
            ' INNER JOIN c_tax ON c_tax.c_tax_uu = c_orderline.c_tax_uu ' +
            ' WHERE c_orderline.c_orderline_uu IN ' + inClause;

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new OrderlineViewModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }

    FindOrderlineViewModelByCOrderUu(c_order_uu) {

        const sqlTemplate = '  FROM c_orderline ' +
            ' INNER JOIN c_order ON c_orderline.c_order_uu = c_order.c_order_uu ' +
            ' INNER JOIN m_product ON m_product.m_product_uu  = c_orderline.m_product_uu ' +
            ' INNER JOIN c_uom ON c_orderline.c_uom_uu = c_uom.c_uom_uu ' +
            ' INNER JOIN c_tax ON c_tax.c_tax_uu = c_orderline.c_tax_uu ' +
            ' WHERE c_orderline.wx_isdeleted = \'N\'  AND c_orderline.c_order_uu = \'' + c_order_uu + '\' ORDER BY c_orderline.line ASC ';

        const listMetaColumn = HelperService.ConvertViewModelToMetaColumn(new OrderlineViewModel());

        let metaDataQuery = new MetaDataQuery(sqlTemplate, listMetaColumn, null);


        return apiInterface.Query(metaDataQuery);
    }


    /**
	 * Menghitung quantity berdasarkan selectedUom dan cUomConversion.
	 *
	 */
    CalculateQtyBasedUomConversion(cUomToUu, qtyentered, cUomConversionViewModelTable) {
        let thisObject = this;
        //let cUomUu = $('#' + thisObject.idFormValidation + ' #c_uom_wx_c_uom_uu').val();

        //let qtyentered = $('#' + thisObject.idFormValidation + ' #c_orderline_wx_qtyentered').val();
        let qtyConversion = 0;
        const cUomConversionFound = _.find(cUomConversionViewModelTable, (cUomConversion) => {
            return cUomConversion.c_uomto_wx_c_uom_uu === cUomToUu;
        });
        if (cUomConversionFound) {
            // tslint:disable-next-line:max-line-length
            qtyConversion = cUomConversionFound.c_uom_conversion_wx_dividerate * qtyentered;
        } else {

            // tslint:disable-next-line:max-line-length
            qtyConversion = qtyentered;
        }

        return qtyConversion;


        //$('#' + thisObject.idFormValidation + ' #c_orderline_wx_qtyordered').val(qtyOrdered);

    }


    /**
	 * 
	
	 */
    async    SaveUpdate(orderlineCM, datatableReference) {

        let insertSuccess = 0;
        let isNewRecord = false;
        let thisObject = this;
       // let orderService = new OrderService();
        try {
            if (!orderlineCM.c_orderline.c_orderline_uu) { //new record
                isNewRecord = true;
            }

            let sqlInsert = thisObject.GetSqlInsertStatement(orderlineCM);

            insertSuccess += await thisObject.ExecuteAndUpdateOrderSql(sqlInsert, orderlineCM.c_orderline.c_order_uu);

            // jika sukses tersimpan 
            if (insertSuccess > 0) {

                // await orderService.CountTotallinesAndGrandTotal(orderlineCM.c_orderline.c_order_uu);

                let rowDataList = await thisObject.FindOrderlineViewModelByCOrderlineUu(orderlineCM.c_orderline.c_orderline_uu);

                let rowData = {};

                if (rowDataList.length > 0) {
                    rowData = rowDataList[0];
                }

                if (!isNewRecord) {

                    datatableReference.row($('#bttn_row_edit' + rowData['c_orderline_wx_c_orderline_uu']).parents('tr')).data(rowData).draw();
                } else {

                    datatableReference.row.add(rowData).draw();
                }


            }


        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.SaveUpdate.name, error);
        }


        return insertSuccess;

    }




    /**
     * Konversi dari list c invoiceline_uu menjadi orderline 
     * @param {*} listCInvoicelineUu 
     * return orderlineCMTList
     */
    ConvertCInvoicelineUuReturToOrderline(listCInvoicelineUu, c_order_uu, line) {

        let timeNow = new Date().getTime();
        let invoicelineService = new InvoicelineService();
        let thisObject = this;
        return new Promise(function (resolve, reject) {

            invoicelineService.FindInvoicelineCMTListByListCInvoicelineUu(listCInvoicelineUu).then(function (invoicelineCMTList) {

                resolve(invoicelineCMTList);
            });

        }).then(function (invoicelineCMTList) {
            return new Promise(function (resolve, reject) {
                let sqlInsertArray = [];
                let listCOrderlineUu = [];
                _.forEach(invoicelineCMTList, function (invoicelineCMT) {
                    let c_orderline = new COrderline();
                    c_orderline.c_orderline_uu = HelperService.UUID();
                    c_orderline.line = ++line;
                    c_orderline.c_order_uu = c_order_uu;
                    c_orderline.sync_client = null;
                    c_orderline.process_date = null;
                    c_orderline.isreturn = 'Y';
                    c_orderline.m_product_uu = invoicelineCMT.c_invoiceline_wx_m_product_uu;
                    c_orderline.c_uom_uu = invoicelineCMT.c_invoiceline_wx_c_uom_uu;
                    c_orderline.qtyentered = invoicelineCMT.c_invoiceline_wx_qtyentered;
                    c_orderline.qtyordered = invoicelineCMT.c_invoiceline_wx_qtyinvoiced;
                    c_orderline.linenetamt = invoicelineCMT.c_invoiceline_wx_linenetamt;
                    c_orderline.priceentered = invoicelineCMT.c_invoiceline_wx_priceentered;
                    c_orderline.priceactual = invoicelineCMT.c_invoiceline_wx_priceactual;
                    c_orderline.linetaxamt = invoicelineCMT.c_invoiceline_wx_taxamt;
                    c_orderline.c_tax_uu = invoicelineCMT.c_invoiceline_wx_c_tax_uu;
                    c_orderline.created = timeNow;
                    c_orderline.updated = timeNow;
                    c_orderline.updatedby = localStorage.getItem('ad_user_uu');
                    c_orderline.createdby = localStorage.getItem('ad_user_uu');
                    c_orderline.ad_org_uu = localStorage.getItem('ad_org_uu');
                    c_orderline.ad_client_uu = localStorage.getItem('ad_client_uu');
                    c_orderline.sync_client = null;
                    c_orderline.process_date = null;

                    listCOrderlineUu.push(c_orderline.c_orderline_uu);

                    let sqlInsert = thisObject.GetRawInsertSqlStatement(c_orderline);
                    sqlInsertArray.push(sqlInsert);

                });

                thisObject.ExecuteAndUpdateOrderBulkSql(sqlInsertArray, c_order_uu).then(function (inserted) {
                    resolve(listCOrderlineUu);

                });



            });


        }).then(function (listCOrderlineUu) {
            return new Promise(function (resolve, reject) {

                thisObject.FindOrderlineCMTListByCOrderlineUu(listCOrderlineUu).then(function (orderlineCMTList) {

                    resolve(orderlineCMTList);

                });

            });


        });
    }



    GetSqlInsertStatement(orderlineCM) {

        const timeNow = new Date().getTime();


        let thisObject = this;
        let sqlInsert = '';
        try {
            if (!orderlineCM.c_orderline.c_orderline_uu) { //new record

                orderlineCM.c_orderline.c_orderline_uu = HelperService.UUID();
                orderlineCM.c_orderline.ad_client_uu = localStorage.getItem('ad_client_uu');
                orderlineCM.c_orderline.ad_org_uu = localStorage.getItem('ad_org_uu');

                orderlineCM.c_orderline.createdby = localStorage.getItem('ad_user_uu');
                orderlineCM.c_orderline.updatedby = localStorage.getItem('ad_user_uu');

                orderlineCM.c_orderline.created = timeNow;
                orderlineCM.c_orderline.updated = timeNow;
            } else { //update record


                orderlineCM.c_orderline.updatedby = localStorage.getItem('ad_user_uu');

                orderlineCM.c_orderline.updated = timeNow;
            }


            orderlineCM.c_orderline.m_product_uu = orderlineCM.m_product.m_product_uu;
            orderlineCM.c_orderline.c_uom_uu = orderlineCM.c_uom.c_uom_uu;
            orderlineCM.c_orderline.c_tax_uu = orderlineCM.c_tax.c_tax_uu;
            orderlineCM.c_orderline.sync_client = null;
            orderlineCM.c_orderline.process_date = null;


            sqlInsert = thisObject.GetRawInsertSqlStatement(orderlineCM.c_orderline);

        } catch (error) {
            apiInterface.Log(thisObject.constructor.name, thisObject.SaveUpdate.name, error);
        }


        return sqlInsert;


    }

    /**
   * Semua fungsi yang akan mengenerate sql insert statemnent untuk invoiceline, harus menggunakan fungsi ini
   * karena isreturn , linenetamt, taxamt, grandtotal  harus negatif
   * @param {*} c_orderline 
   */
    GetRawInsertSqlStatement(c_orderline) {

        // maka cek apakah sudah negatif, kalau sudah negatif, tidak perlu diubah
        if (c_orderline.isreturn === 'Y') {
            if (c_orderline.linenetamt > 0) {
                c_orderline.linenetamt = numeral(c_orderline.linenetamt).value() * -1;
            }
            if (c_orderline.linetaxamt > 0) {
                c_orderline.linetaxamt = numeral(c_orderline.linetaxamt).value() * -1;
            }

        }

        return HelperService.SqlInsertOrReplaceStatement('c_orderline', c_orderline);


    }

    ExecuteAndUpdateOrderSql(sqlStatement, c_order_uu) {

        try {
            return new Promise(function (resolve, reject) {

                apiInterface.ExecuteSqlStatement(sqlStatement).then(function (inserted) {
                    resolve(inserted);

                });

            }).then(function (insertSuccess) {
                return new Promise(function (resolve, reject) {

                    let orderService = new OrderService();
                    orderService.CountTotallinesAndGrandTotal(c_order_uu).then(function (inserted) {
                        insertSuccess += inserted;
                        resolve(insertSuccess);

                    });

                });

            });
        } catch (error) {
            apiInterface.Log(this.constructor.name, this.ExecuteAndUpdateOrderSql.name, error);
            return new Promise(function (resolve, reject) { resolve(0); });
        }




    }

    ExecuteAndUpdateOrderBulkSql(listSqlStatement, c_order_uu) {

        try {
            return new Promise(function (resolve, reject) {

                apiInterface.ExecuteBulkSqlStatement(listSqlStatement).then(function (inserted) {
                    resolve(inserted);

                });

            }).then(function (insertSuccess) {
                return new Promise(function (resolve, reject) {

                    let orderService = new OrderService();
                    orderService.CountTotallinesAndGrandTotal(c_order_uu).then(function (inserted) {
                        insertSuccess += inserted;
                        resolve(insertSuccess);

                    });

                });

            });
        } catch (error) {
            apiInterface.Log(this.constructor.name, this.ExecuteAndUpdateOrderBulkSql.name, error);
            return new Promise(function (resolve, reject) { resolve(0); });
        }




    }


}