var configSqlite = {
    locateFile: (filename, prefix) => {
        console.log(`prefix is : ${prefix}`);
        return `./vendor_js/${filename}`;
    }
};

var dbSqlite;
var hostname =  window.location.hostname;
var port = window.location.port;
var href = window.location.href;
//var urlNodeJs = "http://localhost:4000";

var apiInterface = {

    ExecuteSqlStatement: function (sqlStatement) {

        let insertSuccess = 0;
        try {
            if (config.build === 'web') {

                return new Promise(function (resolve, reject) {
                    let sessionSqlStatement = sessionStorage.getItem("sqlstatement");
                    sessionSqlStatement += '\n' + sqlStatement;
                    sessionStorage.setItem("sqlstatement", sessionSqlStatement);

                    dbSqlite.exec(sqlStatement);

                    resolve(1);


                });


            }
            if (config.build === 'android') {

                return new Promise(function (resolve, reject) {

                    let result = WypexAndroid.ExecuteSqlStatement(sqlStatement);
                    resolve(result);

                });

            }

            if (config.build === 'desktop') {
                return new Promise(function (resolve, reject) {

                    pywebview.api.ExecuteSqlStatement(sqlStatement).then(function (result) {

                        resolve(result);
                    });

                });

            }

            if (config.build === 'dotnet') {
                return new Promise(function (resolve, reject) {

                    dotnetJs.executeSqlStatement(sqlStatement).then(function (result) {

                        resolve(result);
                    });

                });

            }


            if (config.build === 'electron') {
                return new Promise(function (resolve, reject) {
                    let jsonData = JSON.stringify({statement : sqlStatement});
                    $.ajax({
                        method: "POST",
                        url: config.urlNodeJs + "/executesqlstatement",
                        data: jsonData,
                        crossDomain: true,
                        contentType:"application/json"
                    })
                        .done(function (result) {
                            if (result) {
                                //resolve(result);
                               
                                resolve(JSON.parse(result));
                            } else {

                                resolve(0);
                            }
                        });
                });

            }






        } catch (error) {
            apiInterface.Log('ApiInterface', 'ExecuteSqlStatement', error + '/n' + sqlStatement);
        }

        return insertSuccess;
    },

    ExecuteBulkSqlStatement: function (listSqlStatement) {
        let insertSuccess = 0;
        if (config.build === 'web') {

            return new Promise(function (resolve, reject) {

                _.forEach(listSqlStatement, function (sqlStatement) {

                    let sessionSqlStatement = sessionStorage.getItem("sqlstatement");
                    sessionSqlStatement += '\n' + sqlStatement;
                    sessionStorage.setItem("sqlstatement", sessionSqlStatement);

                    dbSqlite.exec(sqlStatement);
                    insertSuccess++;
                });
                resolve(insertSuccess);
            });
        }




        if (config.build === 'android') {

            return new Promise(function (resolve, reject) {
                if (listSqlStatement.length) {
                    let result = WypexAndroid.ExecuteBulkSqlStatement(JSON.stringify(listSqlStatement));
                    resolve(result);

                } else {
                    resolve(0);
                }


            });

        }

        if (config.build === 'desktop') {
            return new Promise(function (resolve, reject) {

                if (listSqlStatement.length) {

                    let listSqlStatementJson = JSON.stringify(listSqlStatement);
                    pywebview.api.ExecuteBulkSqlStatement(listSqlStatementJson).then(function (result) {

                        resolve(result);
                    });
                } else {

                    resolve(0);
                }


            });

        }

        if (config.build === 'dotnet') {
            return new Promise(function (resolve, reject) {

                if (listSqlStatement.length) {

                    let listSqlStatementJson = JSON.stringify(listSqlStatement);
                    dotnetJs.executeBulkSqlStatement(listSqlStatementJson).then(function (result) {

                        resolve(result);
                    });
                } else {

                    resolve(0);
                }


            });

        }

        if (config.build === 'electron') {
            return new Promise(function (resolve, reject) {
                if (listSqlStatement.length) {

                    let listSqlStatementJson = JSON.stringify(listSqlStatement);
                    $.ajax({
                        method: "POST",
                        url: config.urlNodeJs + "/executebulksqlstatement",
                        data: listSqlStatementJson,
                        crossDomain: true,
                        contentType:"application/json"
                    })
                        .done(function (result) {
                            if (result) {
                                //resolve(result);
                               
                                resolve(JSON.parse(result));
                            } else {

                                resolve(0);
                            }
                        });
                }else{
                    resolve (0);
                }

                
            });

        }





    },



    Query: function (metaDataQueryJson) {

        let sqlStatement = this.GetSqlStatement(metaDataQueryJson);
        // let sqlStatement = metaDataQueryJson;
        let returnRows = [];
        try {
            if (config.build === 'web') {
                console.log(sqlStatement);
                if (dbSqlite) {

                    return new Promise(function (resolve, reject) {
                        let stmt = dbSqlite.prepare(sqlStatement);
                        stmt.bind();
                        let rows = [];
                        while (stmt.step()) {
                            let row = stmt.getAsObject();
                            rows.push(row);
                        }
                        resolve(rows);
                    });
                } else {
                    return new Promise(function (resolve, reject) {
                        initSqlJs(configSqlite).then(function (SQL) {

                            var xhr = new XMLHttpRequest();
                            xhr.open('GET', './vendor_js/wypex.db', true);
                            xhr.responseType = 'arraybuffer';

                            xhr.onload = function (e) {
                                var uInt8Array = new Uint8Array(this.response);
                                dbSqlite = new SQL.Database(uInt8Array);
                                // return new SQL.Database(uInt8Array);
                                let stmt = dbSqlite.prepare(sqlStatement);
                                stmt.bind();
                                let rows = [];
                                while (stmt.step()) {
                                    let row = stmt.getAsObject();
                                    rows.push(row);
                                }
                                resolve(rows);

                                //let mainPage = new MainPage();

                            };
                            xhr.send();
                        });

                    });


                }



            }




            if (config.build === 'android') {

                return new Promise(function (resolve, reject) {

                    let result = WypexAndroid.Query(sqlStatement);

                    if (result) {
                        //resolve(result);
                        resolve(JSON.parse(result));
                    } else {

                        resolve(null);
                    }
                });


            }
            if (config.build === 'desktop') {

                return new Promise(function (resolve, reject) {

                    pywebview.api.Query(sqlStatement).then(function (result) {

                        if (result) {
                            //resolve(result);
                            resolve(JSON.parse(result));
                        } else {

                            resolve(null);
                        }
                    });

                });
            }

            if (config.build === 'dotnet') {

                return new Promise(function (resolve, reject) {

                    dotnetJs.query(sqlStatement).then(function (result) {

                        if (result) {
                            //resolve(result);
                            console.log(result);
                            resolve(JSON.parse(result));
                        } else {

                            resolve(null);
                        }


                    })

                });


            }

            if (config.build === 'electron') {
                return new Promise(function (resolve, reject) {
                    let jsonData = JSON.stringify({statement : sqlStatement});
                    $.ajax({
                        method: "POST",
                        url: config.urlNodeJs + "/query",
                        data: jsonData,
                        crossDomain: true,
                        contentType:"application/json"
                    })
                        .done(function (result) {
                            if (result) {
                                //resolve(result);
                               
                                resolve(result);
                            } else {

                                resolve(null);
                            }
                        });
                });

            }



        } catch (error) {
            apiInterface.Log('ApiInterface', 'Query', error + '/n' + sqlStatement);
            return new Promise(function (resolve, reject) {
                resolve(null);
            });

        }

        // return returnRows;

    },


    DirectQuerySqlStatement: function (sqlStatement) {
        let returnRows = [];
        try {
            if (config.build === 'web') {

                return new Promise(function (resolve, reject) {

                    console.log(sqlStatement);

                    let stmt = dbSqlite.prepare(sqlStatement);
                    stmt.bind();
                    let rows = [];
                    while (stmt.step()) {
                        let row = stmt.getAsObject();
                        rows.push(row);
                    }

                    resolve(rows);




                });




            }

            if (config.build === 'android') {

                return new Promise(function (resolve, reject) {

                    let result = WypexAndroid.Query(sqlStatement);

                    if (result) {
                        resolve(JSON.parse(result));

                    } else {
                        resolve(null);

                    }


                });

            }

            if (config.build === 'dotnet') {

                return new Promise(function (resolve, reject) {

                    dotnetJs.query(sqlStatement).then(function (result) {

                        if (result) {
                            //resolve(result);
                            resolve(JSON.parse(result));
                        } else {

                            resolve(null);
                        }
                    });

                });
            }

            if (config.build === 'electron') {
                return new Promise(function (resolve, reject) {

                    let jsonData = JSON.stringify({statement : sqlStatement});
                    $.ajax({
                        method: "POST",
                        url: config.urlNodeJs + "/directquerysqlstatement",
                        data: jsonData,
                        crossDomain: true,
                        contentType:"application/json"
                    })
                        .done(function (result) {
                            if (result) {
                                //resolve(result);
                               
                                resolve(result);
                            } else {

                                resolve(null);
                            }
                        });

                 
                });

            }

         /*   if (config.build === 'desktop') {

                return new Promise(function (resolve, reject) {

                    pywebview.api.Query(sqlStatement).then(function (result) {

                        if (result) {
                            //resolve(result);
                            resolve(JSON.parse(result));
                        } else {

                            resolve(null);
                        }
                    });

                });
            }*/


            

        } catch (error) {
            apiInterface.Log('ApiInterface', 'DirectQuerySqlStatement', error + '/n' + sqlStatement);
        }

        return returnRows;

    },

    GetSqlStatement: function (metaDataQueryJson) {
        let listTemplateColumnAndAlias = [];
        let sqlStatement = '';

        try {
            _.forEach(metaDataQueryJson.metaDataColumnList, function (metacolumn) {

                let arrayString = metacolumn.split('.');
                let tableName = arrayString[0];
                let columnName = arrayString[1];
                let alias = tableName + "_wx_" + columnName;
                listTemplateColumnAndAlias.push(metacolumn + "  " + alias);
                sqlStatement = "SELECT " + listTemplateColumnAndAlias.join(',') + metaDataQueryJson.sqlQueryFrom;
            });
        } catch (error) {
            apiInterface.Log('ApiInterface', 'GetSqlStatement', error + '/n' + sqlStatement);
        }


        return sqlStatement;
    },

    Log: function (className, functionName, message) {
        let errorMessage = 'Time : ' + new Date().getTime() + ' Class : ' + className + ' Function : ' + functionName + ' Message : ' + message;
        let thisObject = this;
        if (config.build === 'web') {

            console.error(errorMessage);
        }

        if (config.build === 'desktop') {
            pywebview.api.Log(errorMessage);
        }

        if (config.build === 'android') {
            WypexAndroid.Log(errorMessage);
        }

        if (config.build === 'dotnet') {
            dotnetJs.log(errorMessage);
        }


    },

    Debug: function (message) {
        let thisObject = this;
        message += "DEBUG : " + message;
        if (config.build === 'web') {

            console.warn(message);
        }

        if (config.build === 'desktop') {
            pywebview.api.Log(message);
        }

        if (config.build === 'android') {
            WypexAndroid.Log(message);
        }

        if (config.build === 'dotnet') {
            dotnetJs.log(message);
        }



    },

    PrintPOS: function (listObjPrint) {
        let returnRows = [];
        try {


            if (config.build === 'android') {

                return new Promise(function (resolve, reject) {
                    let listObjPrintJson = JSON.stringify(listObjPrint);
                    let result = WypexAndroid.PrintPOS(listObjPrintJson);

                    if (result) {
                        resolve(result);
                    } else {
                        resolve(null);

                    }


                });

            }



            if (config.build === 'dotnet') {

                return new Promise(function (resolve, reject) {

                    let listObjPrintJson = JSON.stringify(listObjPrint);
                    dotnetJs.printPOS(listObjPrintJson).then(function (result) {

                        if (result) {
                            resolve(result);
                        } else {
                            resolve(null);

                        }

                    });

                });
            }

            if (config.build === 'electron') {
                return new Promise(function (resolve, reject) {

                    let listObjPrintJson = JSON.stringify(listObjPrint);
                    $.ajax({
                        method: "POST",
                        url: config.urlNodeJs + "/posprint",
                        data: listObjPrintJson,
                        crossDomain: true,
                        contentType:"application/json"
                    })
                        .done(function (result) {
                            if (result) {
                                //resolve(result);
                               
                                resolve(result);
                            } else {

                                resolve(null);
                            }
                        });

                 
                });

            }

        } catch (error) {
            apiInterface.Log('ApiInterface', 'DirectQuerySqlStatement', error + '/n' + sqlStatement);
        }

        return returnRows;


    },
    GetLocalStorage: function () {
        let returnRows = [];
        try {

            if (config.build === 'web') {

                return new Promise(function (resolve, reject) {
                    let ad_user_uu ="";
                    if ((localStorage.getItem("ad_user_uu") !== 'null') && localStorage.getItem("ad_user_uu")){
                        
                        ad_user_uu = { ad_user_uu: localStorage.getItem('ad_user_uu') };
                    }else{
                        localStorage.removeItem("ad_user_uu");
                    }


                    if (ad_user_uu) {

                        resolve(ad_user_uu);
                    } else {
                        resolve(null);

                    }


                });

            }

            if (config.build === 'android') {

                return new Promise(function (resolve, reject) {

                    let result = WypexAndroid.GetLocalStorage();

                    if (result) {
                        let listObjStorage = JSON.parse(result); // {key:xxxx,valu:xxxx}
                        resolve(listObjStorage);
                    } else {
                        resolve(null);

                    }


                });

            }



            if (config.build === 'dotnet') {

                return new Promise(function (resolve, reject) {


                    dotnetJs.GetLocalStorage().then(function (result) {

                        if (result) {
                            let listObjStorage = JSON.parse(result); // {key:xxxx,valu:xxxx}
                            resolve(listObjStorage);
                        } else {
                            resolve(null);

                        }

                    });





                });
            }

            if (config.build === 'electron') {

                return new Promise(function (resolve, reject) {

                    let ad_user_uu ="";
                    if ((localStorage.getItem("ad_user_uu") !== 'null') && localStorage.getItem("ad_user_uu")){
                        
                        ad_user_uu = { ad_user_uu: localStorage.getItem('ad_user_uu') };
                    }else{
                        localStorage.removeItem("ad_user_uu");
                    }

                    if (ad_user_uu) {

                        resolve(ad_user_uu);
                    } else {
                        resolve(null);

                    }


                });

            }

        } catch (error) {
            apiInterface.Log('ApiInterface', 'GetLocalStorage', error + '/n' + sqlStatement);
        }

        return returnRows;


    },
    SetLocalStorage: function () {
        let returnRows = [];
        try {
            if (config.build === 'web') {

                return new Promise(function (resolve, reject) {
                    resolve(1);


                });

            }


            if (config.build === 'android') {

                return new Promise(function (resolve, reject) {
                    let objStorageAdUserUuJson = "";
                    if (localStorage.getItem("ad_user_uu")){
                        let ad_user_uuStorage = localStorage.getItem("ad_user_uu");
                       objStorageAdUserUu = { ad_user_uu: ad_user_uuStorage };
                    }
                  
                    JSON.stringify(objStorageAdUserUu);
                    WypexAndroid.SetLocalStorage(objStorageAdUserUuJson);
                    resolve(1);


                });

            }





            if (config.build === 'dotnet') {

                return new Promise(function (resolve, reject) {
                    let ad_user_uuStorage = localStorage.getItem("ad_user_uu");
                    let objStorageAdUserUu = { ad_user_uu: ad_user_uuStorage };
                    let objStorageAdUserUuJson = JSON.stringify(objStorageAdUserUu);
                    dotnetJs.SetLocalStorage(objStorageAdUserUuJson).then(function (result) {

                        resolve(1);
                    });

                });
            }

            if (config.build === 'electron') {

                return new Promise(function (resolve, reject) {
                    resolve(1);


                });

            }

        } catch (error) {
            apiInterface.Log('ApiInterface', 'DirectQuerySqlStatement', error + '/n' + sqlStatement);
        }

        return returnRows;


    }
};
